import sys
import re
import math

class _possibleElementType:
    """ This class keeps all possible element's types """

    def __init__(self):
        self.__names_type = {}
        self.__names_type["drift"]     =1
        self.__names_type["dipole"]    =2
        self.__names_type["quad"]      =3
        self.__names_type["sbend"]     =4
        self.__names_type["hkicker"]   =5
        self.__names_type["vkicker"]   =6
        self.__names_type["marker"]    =7
        self.__names_type["sextupole"] =8
        self.__names_type["monitor"]   =9
        self.__names_type["octupole"]  =10
        self.__names_type["quadrupole"]  =11
        self.__names_type["rfcavity"]  =12
        self.__names_type["solenoid"]  =13
        self.__names_type["multipole"]  =14
        self.__names_type["rbend"]  =15
        self.__names_type["rcollimator"]  =16
        self.__names_type["kicker"]   =17

    def __del__(self):
        del self.__names_type

    def getType(self,name):
        if self.__names_type.has_key(name) == 0:
            print "Error of creating lattice element."
            print "There can not be an element with name:",name
            print "Stop."
            sys.exit (0)
        return self.__names_type[name]
            
#===============================================================

class LattElement:
    """ An Arbitrary Element in the Lattice """
    
    def __init__(self,name,Tname):
        """ Create instance with name, typeName and type """
        self.__name = name    
        elMng = _possibleElementType()
        self.__type = elMng.getType(Tname)
        self.__Tname = Tname 
        self.__par = {}
        del elMng
        self.__depthLevel = 0

    def __del__(self):
        del self.__par
        del self.__type
        del self.__name
        del self.__Tname
        del self.__depthLevel

    def getName(self):
        """ Returns name of the element """
        return self.__name

    def getType(self):
        """ Returns type of the element """
        return self.__type

    def getTName(self):
        """ Returns type name of the element """
        return self.__Tname

    def addParameter(self,nameOfPar,parVal):
        self.__par[nameOfPar] = parVal

    def getParameter(self,nameOfPar):
        if self.__par.has_key(nameOfPar) == 0:
            print "class LattElement, method getParameter"
            print "The name of Element =",self.__name
            print "The type of Element =",self.__type
            print "The Element's key-val =",self.__par
            print "This Element does not have Parameter=", nameOfPar
            print "Stop."
            sys.exit (0)            
        return self.__par[nameOfPar]

    def getParameters(self):
        return self.__par

    def getElements(self):
        """ Returns list of elements (only one here) """
        elements = []
        elements.append(self)
        return elements

    def increaseDepthLevel(self):
       self.__depthLevel += 1

    def getDepthLevel(self):
        return self.__depthLevel
    
#====================================================================

class LattLine:
    """ An Arbitrary Line in the Lattice """

    def __init__(self,name):
        """ Create instance with list of lines or elements """
        self.__name = name
        self.__line = []
        self.__nItems = 0
        self.__depthLevel = 0

    def __del__(self):
        del self.__name
        del self.__line
        del self.__nItems
        del self.__depthLevel

    def getName(self):
        """ Returns name of the line """
        return self.__name

    def addItem(self,item):
        """ Adds a line or element to this line"""
        self.__line.append(item)
        self.__nItems = len(self.__line)
        item.increaseDepthLevel()

    def getElements(self):
        """ Returns list of elements """
        elements = []
        for item in self.__line:
            elems = item.getElements()
            for el in elems:
                elements.append(el)
        return elements

    def increaseDepthLevel(self):
       self.__depthLevel += 1

    def getDepthLevel(self):
        return self.__depthLevel

#====================================================================

class MADparser:
    """ MAD parser """

    def __init__(self):
        """ Create instance of the MADparser class """
        self.__vars =  {}
        self.__lines = {}
        self.__elems = {}
        self.__stringParser = _StrMADParser()

    def __del__(self):
        del self.__vars
        del self.__lines
        del self.__elems
        del self.__stringParser

    def parse(self,MADfileName):
        fl = open(MADfileName)
        str_local = ""
        for str in fl.readlines():
            str = str.lower()
            str_local = "".join([str_local,str.strip()])

            #this part deal with a comment "!" at the end of line 
            str0 = str_local            
            if str0.rfind("!") > 0:
                str_local = ""
                for i in xrange(str0.rfind("!")):
                    str_local = "".join([str_local,str0[i]])
            str_local.strip()

            #this part deal with a comment ";" at the end of line 
            str0 = str_local            
            if str0.rfind(";") > 0:
                str_local = ""
                for i in xrange(str0.rfind(";")):
                    str_local = "".join([str_local,str0[i]])
            str_local.strip()

            #this part deal with a continue sign at the end of line                
            if str_local.endswith("&"):
                str0 = str_local
                if str0.rfind("&") > 0:
                    str_local = ""
                    for i in xrange(str0.rfind("&")):
                      str_local = "".join([str_local,str0[i]])
                str_local.strip()
                continue
            else:
                #check the empty line
                if str_local == "":
                    str_local = ""
                    continue
                #check if the line is a comment
                if str.find("!") == 0:
                    str_local = ""
                    continue
                #now we have the line to parse
                self._parse_string(str_local,self.__vars,self.__lines,self.__elems)
                str_local = ""
        fl.close()

    def _parse_string(self,str,Vars,Lines,Elems):
        """ The method parses the one string """

        #Now here 5 types of string
        # 0 - unknown type of the line
        # 1 - variables calculations
        # 2 - element definition
        # 3 - MAD line definition
        # 4 - call another nested MAD file

        #Delete spaces 
        str=re.sub(r'[ ]',"",str)
        
        #Initialize string parser
        self.__stringParser.setString(str)
        
        tp = self.__stringParser.getAndDefineType()
        if tp == 0:
            #print "StrType =0 :",str
            return
        if tp == 1:
            #print "StrType =1 :",str
            self.__stringParser.parseCalcString(Vars,Elems)
        if tp == 2:
            #print "StrType =2 :",str            
            self.__stringParser.parseElemDefString(Vars,Elems)
        if tp == 3:
            #print "StrType =3 :",str            
            self.__stringParser.parseLineDefString(Vars,Lines,Elems)
        if tp == 4:
            #print "StrType =4 :",str
            fl = self.__stringParser.parseNestedFileLine()
            self.parse(fl)

    def clear(self):
        del self.__vars
        del self.__lines
        del self.__elems
        del self.__stringParser
        self.__vars  = {}
        self.__lines = {}
        self.__elems = {}
        self.__stringParser = _StrMADParser()
        
    def getLines(self):
        return self.__lines

    def getElements(self):
        return self.__elems

#====================================================================

class _StrMADParser:
    """ Parser for one line from MAD file """

    def __init__(self):
        """ Create instance of the  _StrMADParser class"""
        self.__str = ""
        self.__type = 0

    def __del__(self):
        del self.__str
        del self.__type

    def setString(self,str):
        """ Sets string for parsing """
        self.__str = str

    def getAndDefineType(self):
        """ Return type of the string """
        #Now here 5 types of string
        # 0 - unknown type of the line
        # 1 - variables calculations
        # 2 - element definition
        # 3 - MAD line definition
        # 4 - call another nested MAD file
        str = self.__str
        self.__type = 0
        t_match = re.search(r'.*:=.*',str)
        if t_match:
            self.__type = 1
        if self.__type != 1:
            t_match = re.search(r'[\w]* *:.*',str)
            if t_match:
                self.__type = 2
        t_match = re.search(r'.*:.*line.*=',str)
        if t_match:
            self.__type = 3
        t_match = re.search(r' *call *file *=',str)
        if t_match:
            self.__type = 4
        return self.__type
        
    def parseCalcString(self,Vars,Elems):
        #substitute element's parameters
        self.__str = self._substElemPar(self.__str,Elems)
        #divide string onto two parts: name of value and value
        patt = re.compile(r'(?<=:=).*')
        s_val = re.findall(patt,self.__str)
        patt = re.compile(r'.*(?=:=)')
        s_name  = re.findall(patt,self.__str)
        s_val  = s_val[0]
        s_name = s_name[0]
        Vars[s_name] = self._calcString(s_val,Vars)        

    def parseElemDefString(self,Vars,Elems):
        """ Parses a MAD's line defining an element and its keys """
        MAD_string = self.__str
        #substitute element's parameters (like a11[l]) by value
        self.__str = self._substElemPar(self.__str,Elems)
        #divide string onto three parts: name,type and key-values of element
        patt = re.compile(r'[\w]+(?=:)')
        name = re.findall(patt,self.__str)
        patt = re.compile(r'(?<=:)[\w]+?(?=\s|,)')
        tp = re.findall(patt,self.__str+",")
        patt = re.compile(r'(?<=\w),.*')
        s_key_vals = re.findall(patt,self.__str)       
        name = name[0]
        tp = tp[0]
        if Elems.has_key(name) > 0:
            print "Wrong line in the MAD file"
            print "The line:",MAD_string
            print "You tray to redefine element with name:",name
            print "It is not wise. Check MAD file."
            print "Stop."
            sys.exit (0)                       
        Elems[name]= LattElement(name,tp)
        #==================================================
        #now we have name and type, let's get key-val pairs
        #==================================================
        if len(s_key_vals) < 1:
            return
        s_key_vals = s_key_vals[0]+","
        patt = re.compile(r'(?<=,).+?(?=,)')
        s_key_vals = re.findall(patt,s_key_vals)
        for s_key_val in s_key_vals:
            patt = re.compile(r'[\w]+?(?==)')
            key =  re.findall(patt,s_key_val)
            key = key[0]
            patt = re.compile(r'(?<==).*')
            val = re.findall(patt,s_key_val)
            val = self._calcString(val[0],Vars)
            Elems[name].addParameter(key,val)


    def parseLineDefString(self,Vars,Lines,Elems):
        """ Parses the MAD file line with lattice line definition"""
        MAD_string = self.__str
        #define name of new line
        patt = re.compile(r'[\w]+(?=:)')
        name = re.findall(patt,self.__str)
        name = name[0]
        if Lines.has_key(name) > 0:
            print "Wrong line in the MAD file"
            print "The string:",MAD_string
            print "You tray to redefine MAD-line with name:",name
            print "It is not wise. Check MAD file."
            print "Stop."
            sys.exit (0)                       
        Lines[name] = LattLine(name)
        patt = re.compile(r'(?<==).*')
        s_def = re.findall(patt,self.__str)
        patt = re.compile(r'(?<=\(|,).+?(?=,|\))')
        item_names = re.findall(patt,s_def[0])
        #=========================================
        #deal with the N*name expressions
        #=========================================
        item_names_new = []
        for it in item_names:
            patt = re.compile(r'[\d]+?(?=\*)')
            n_rep = re.findall(patt,it)
            if len(n_rep) > 0:
                n_rep = int(n_rep[0])
                patt = re.compile(r'(?<=\*)[\w]+')
                s=re.findall(patt,it)
                s=s[0]
                for i in range(1,n_rep+1):
                    item_names_new.append(s)
            else:
               item_names_new.append(it)
        item_names = item_names_new
        #======================================
        for it in item_names:
            is_in_lines = 0
            is_in_elems = 0
            if Lines.has_key(it) > 0:
                is_in_lines = 1
            if Elems.has_key(it) > 0:
                is_in_elems = 1
            if is_in_lines == 1 and is_in_elems == 1:
                print "Wrong line in the MAD file"
                print "The string:",MAD_string
                print "There the lattice line and"
                print "the element with the same names:",it
                print "It is not possible. Check MAD file."
                print "Stop."
                sys.exit (0)
            if is_in_lines == 0 and is_in_elems == 0:
                print "Wrong line in the MAD file"
                print "The string:",MAD_string
                print "The lattice line or the element"
                print "should be defined before.The name:",it
                print "Check MAD file."
                print "Stop."
                sys.exit (0)
            if is_in_lines == 1:
                Lines[name].addItem(Lines[it])
            if is_in_elems == 1:
                Lines[name].addItem(Elems[it])
    #========================================================

    def parseNestedFileLine(self):
        """ Returns the name of the nested file"""
        str = self.__str
        #Define delimiter
        dl="'"
        if str.find(dl) < 0:
            dl = "\""
        ind0  = str.find(dl)
        ind0 += 1
        ind1  = str.rfind(dl)
        str_res=""
        if ind0 >= ind1 or ind0 < 0 or ind1 < 0:
            print "Wrong line in the MAD file"
            print "line Call file= defines wrong name of file format"
            print "Should be : Call file = 'name of file'"
            print "Stop."
            sys.exit (0)           
        for i in range(ind0,ind1):
            str_res = "".join([str_res,str[i]])
        return str_res
        
    def _substElemPar(self,strng,Elems):
        """ Checks the string for ELEMENT[KEY] substring """
        """ Replase this substring by its value """
        patt=re.compile(r'[\w]*\[[\w]*\]')
        s_name_key = re.findall(patt,strng)
        if len(s_name_key) == 0:
            return strng
        s_key = []
        patt=re.compile(r'(?<=\[)[\w]*(?=\])')
        for s in s_name_key:
            s1 = re.findall(patt,s)
            s1 = s1[0]
            s_key.append(s1)
        s_name = []
        patt=re.compile(r'[\w]*(?=\[)')
        for s in s_name_key:
            s1 = re.findall(patt,s)
            s1 = s1[0]
            s_name.append(s1)
        i = 0
        for s in s_name_key:
            if Elems.has_key(s_name[i]) == 0:
                print "The MAD line parser"
                print "String =",strng
                print "There is no element with name:",s_name[i]
                print "Stop."
                sys.exit (0)              
            val = Elems[s_name[i]].getParameter(s_key[i])
            s_val = str(val)
            s=re.sub(r'\[',"\[",s,1)
            s=re.sub(r'\]',"\]",s,1)
            strng = re.sub(s,s_val,strng,1)
            i += 1
        return strng

    def _calcString(self,s_var,Vars):
        """ Calculates the string s_var by using Vars dictionary """
        #replace .e by .0e
        s_var = re.sub("\.e",".0e",s_var)
        #find variable's names
        patt = re.compile(r'[\w]*')
        v0 = re.findall(patt,s_var)
        #delete empty names and starting with number
        v = []
        for s in v0:
            if (not re.search(r'^[0-9]',s)) and s != "" :
                v.append(s)
        #if nothing to substitute
        if len(v) == 0 :
            return eval(s_var)
        #make empty substitute list
        vs = []
        for s in v:
            vs.append("")
        #check the math operatons
        i = 0
        for s in v:
            if s == "sin":
                vs[i]="math.sin"
            if s == "cos":
                vs[i]="math.cos"
            if s == "tan":
                vs[i]="math.tan"
            if s == "exp":
                vs[i]="math.exp"
            if s == "log":
                vs[i]="math.log"
            if s == "acos":
                vs[i]="math.acos"
            if s == "asin":
                vs[i]="math.asin"
            if s == "atan":
                vs[i]="math.atan"
            if s == "sqrt":
                vs[i]="math.sqrt"
            i += 1
        #substitute known variables
        i = 0
        for s in v:
            if vs[i] == "":
                if Vars.has_key(s) == 0:
                    print "The MAD line parser"
                    print "String =",s_var
                    print "Undefined variable:",s
                    print "Stop."
                    sys.exit (0)
                vs[i]="Vars['"+s+"']"
            i += 1
        i = 0
        for s in v:
            s_var = re.sub(s,"&"+str(i),s_var,1)
            i += 1
        i = 0
        for s in v:
            s_var = re.sub("&"+str(i),vs[i],s_var,1)
            i += 1
        #calulate string
        r=eval(s_var)
        return r
