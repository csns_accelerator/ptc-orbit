#!/usr/bin/env python

import sys
from MADparser import LattElement, LattLine, MADparser

pars = MADparser()

pars.parse("temp/Simple_623_523.LAT")

lines = pars.getLines()

lines_names = lines.keys()

#print "All upper Lines in the MAD file:"

#for l in lines_names:
#    if lines[l].getDepthLevel() == 0:
#        print "  name=",lines[l].getName()

#===========================================================
#elems = lines[lines_names[0]].getElements()
#elems = lines["ring"].getElements()
elems = lines["l000001"].getElements()

#print "Elements in the lattice with name:",lines_names[0]
#print "Elements in the lattice with name:",lines["ring"].getName()

for el in elems:
    print el.getTName(),"  ",el.getName(),"  ",str(el.getParameters())


sys.exit(0)
