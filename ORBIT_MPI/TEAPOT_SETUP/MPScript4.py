#!/usr/bin/env python

import sys
from MADparser import LattElement, LattLine, MADparser

pars = MADparser()

pars.parse("MAD.LAT")

lines = pars.getLines()

lines_names = lines.keys()

for l in lines_names:
    if lines[l].getDepthLevel() == 0:
        print "  name=",lines[l].getName()

#===========================================================
#elems = lines[lines_names[0]].getElements()
#elems = lines["ring"].getElements()

f_out = open("TP.LAT","w")


elems = lines["l000001"].getElements()
for el in elems:
    keys = el.getParameters().keys()
    values = el.getParameters().values()
    strng = ""
    k = ["","","","","","","","","",""]
    i = 0
    for key in keys:
        k[i] = k[i] + str(key)
        i = i + 1
    i=0
    for val in values:
        strng = strng + k[i] + "  " + str(val) + "  "
        i = i + 1
    strng = strng + "sub  10  end"
    print >> f_out, el.getTName(), strng
    
elems = lines["l000002"].getElements()
for el in elems:
    keys = el.getParameters().keys()
    values = el.getParameters().values()
    strng = ""
    k = ["","","","","","","","","",""]
    i = 0
    for key in keys:
        k[i] = k[i] + str(key)
        i = i + 1
    i=0
    for val in values:
        strng = strng + k[i] + "  " + str(val) + "  "
        i = i + 1
    strng = strng + "sub  10  end"
    print >> f_out, el.getTName(), strng

elems = lines["l000003"].getElements()
for el in elems:
    keys = el.getParameters().keys()
    values = el.getParameters().values()
    strng = ""
    k = ["","","","","","","","","",""]
    i = 0
    for key in keys:
        k[i] = k[i] + str(key)
        i = i + 1
    i=0
    for val in values:
        strng = strng + k[i] + "  " + str(val) + "  "
        i = i + 1
    strng = strng + "sub  10  end"
    print >> f_out, el.getTName(), strng    

elems = lines["l000004"].getElements()
for el in elems:
    keys = el.getParameters().keys()
    values = el.getParameters().values()
    strng = ""
    k = ["","","","","","","","","",""]
    i = 0
    for key in keys:
        k[i] = k[i] + str(key)
        i = i + 1
    i=0
    for val in values:
        strng = strng + k[i] + "  " + str(val) + "  "
        i = i + 1
    strng = strng + "sub  10  end"
    print >> f_out, el.getTName(), strng

print >> f_out, "final"
f_out.close()

sys.exit(0)
