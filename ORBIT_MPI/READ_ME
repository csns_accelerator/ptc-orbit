There is the new parallel version of ORBIT - ORBIT_MPI. It uses the 
MPI library to provide communication between CPUs. Practically all of the 
SuperCode's and  C++ interfaces of the new version are the same as in the 
original ORBIT code. All modules of ORBIT dealing with the collective effects 
have a parallel implementation. The Makefile for ORBIT_MPI generates one executable 
ORBIT_MPI.

The parallel diagnostics is implemented except the tune diagnostics.
For tune diagnostics you have to use "post mortem" diagnostics 
after writing coordinates of particles on the disk and non-parallel 
scripts (number of CPUs should be 1).

How to install ORBIT_MPI.

1. Install Super Code.
We assume that you are using gcc compiler. If you are using something else
you have to modify SuperCode/Config/LINUX.mak file by replacing the line
CXX = g++
with an appropriate compiler name.

1.1 >gzip -d SuperCode_10_18_2006.tar.gz
1.2 >tar xvf SuperCode_10_18_2006.tar
1.3 >cd SuperCode
1.4 Define environment variables
>export CPU=LINUX
>export SUPERCODE_ROOT=<future installation directory>
1.5 >make configure
1.6 >make
1.7 >make install
Please, use GNU make.


2. Install FFTW
3. If you do not have MPI library, please install it.


4. Install ORBIT_MPI itself.
4.1 >gzip -d ORBIT_MPI.tar.gz
4.2 >tar xvf ORBIT_MPI.tar
4.3 >cd ORBIT_MPI
4.4 Define the folowing evironment variables and make the necessary changes
    in your .bashrc and .bash_profile:

>export CPU=LINUX
>export SUPERCODE_ROOT=<Super Code location>
>export FFTW_ROOT=<FFTW location>
>export MPI_ROOT=<MPI location>

The make file for ORBIT_MPI is written assuming that we have mpich 
implementation of MPI. If you have something else, please modify Makefile
for ORBIT_MPI by specifying the right libraries and the location of header files.
The place that should be modified:
# Add in the MPI stuff:
ifdef MPI_ROOT
  CXXSTDINCLUDES += -I$(MPI_ROOT)/include
  CXXSHELLLIBS += -L$(MPI_ROOT)/lib -lmpich
endif

4.5 >make
It will create ORBIT_MPI in ${CPU} directory.

To start ORBIT_MPI use mpirun:
>mpirun -np <Number of CPUs> <full path to ORBIT_MPI executable>/ORBIT_MPI <name of the ORBIT script>
   

5. There are several examples in the  
   ./Examples/MPI_functions and 
   ./Examples/TransSpaceCharge
   ./Examples/MPI_PartsDistr
   ./Examples/MPI_TrImp
   ./Examples/MPI_3DSC
   directories. The examples can be started by using the START_ORBIT_MPI.sh 
   shell script. Before using this bash-shell script make this script executable
   >chmod +x START_ORBIT_MPI.sh
   and define ORBIT_PATH environment variable in .bashrc file:
   export ORBIT_PATH=<path to the ORBIT_MPI directory>
   Then start it
   >./START_ORBIT_MPI.sh <name of the ORBIT's script> <number of CPUs>


#====================================================================
# All text below is for a log purposes only. 
# All changes have been already made!
#====================================================================

==========Change Log=================================================
In the parallel version of the ORBIT_MPI the next changes were made
- the module Parallel_MPI.mod (source file Parallel_MPI.cc) was added
- the new main program Main_MPI.cc was added
- the file Injection.cc was modified to provide an injection to all CPUs
- the file TSpaceCharge.cc was modified to provide a parallel capability
  for the FFTTransSC and PotentialTransSC classes.
- the files Particles.cc and Particles.mod were modified to eliminate pvm
  stuff.
- Makefile was modified to generate ORBIT and ORBIT_MPI executables
  and to add MPICH library.
- the file LSpaceCharge.cc was changed to provide a parallel capability
  for longitudinal SC calculation
- the files PartsDistributor_MPI.cc,PartsDistributor_MPI.h were added to
  distribute particles between CPUs during the parallel runs.
- the files Boundary.cc, Boundary.h were added to provide 2D space charge
  calculations for one longitudinal slice in the 3D Space Charge task.
- FFT3DSpaceCharge_MPI.cc, FFT3DSpaceCharge_MPI.h were added to provide
  3D SC calculations.
- SpaceCharge3D_MPI.cc, SpaceCharge3D_MPI.mod, SpCh3D_MPI.h provide the
  interface to the parallel 3D SC classes.
- TImpedance.cc, TImpedance.mod, TImp.h were modified to provide a parallel
  capability.
- DiagClass.h, Diagnostic.cc, Diagnostic.mod, Plots.cc, Output.cc, 
  Output.mod, Injection.cc, Particles.cc were modified to provide parallel
  diagnostics except the tune diagnostics. The status of the tune diagnostic
  is the same as before. It can not be used in parallel runs.
- Transverse Impedance module based on wake functions for resonant element
  was added, files - TransImpWF.cc,TransImpWF.h, TimpedanceWF.cc,
  TimpedanceWF.h. Main.cc and Main_MPI.cc were changed to provide loading
  for new module.
   
===========changes were made for Eagle IBM RS6000 =====================
- ./Environment/ORBIT_env_eagle_ksh was added with environments variables
  for eagle.ccs.ornl.gov (IBM RS6000)
- Makefile was modified for IBM AIX and Linux
- Injection.cc the line #include <iostream> was added
- TransMap.cc was modified to avoid conflict between double declaration of
  the l variable (lines 129 and 334)
- Plots.cc, lines #ifndef (USE_PLPLOT) #else #endif were added to plot4pp
  method to avoid complaints during compilation and linking
- Collimator.cc, some chages about #undef EPS #undef JMAX, #include <ctime>
  and pow(-1,i)
- FNALMaps.cc, disable parts of code that use FNAL libraries if the parameter
  USE_FNALMaps is not defined
- Parallel_MPI.cc, Boundary.cc, FFT3DSpaceCharge_MPI.cc, #include <iostream>
  was added
- PartDistributor_MPI.cc, nMPIsize - 1 => nMPIsize was changed to avoid memory
  allocation for 0-size array

At this moment the new parallel algorithms are implemented for the following
modules:
 TSpaceCharge (2D+1/2 space charge module with or without boundary conditions)
 LSpaceCharge (longitudinal space charge module)
 SpaceCharge3D_MPI (parallel 3D Space Charge module)
 TImpedance (Transverse Impedance module)
 TransImpWF (Transverse Impedance module based on wake functions, 
             Mike Blaskiewicz approach)


#=========Example of environment variables definition============
#you have to define the folowing environment variables
#CPU = LINUX
#SUPERCODE_ROOT = /usr1/home/shishlo/tools/SC_3
#FFTW_ROOT = /usr/local/fftw-2.1.3
#MPI_ROOT = /usr/local/mpi-1.2.4
#FORTRAN_LIBRARY =' -L/opt/intel/fc/9.1.032/lib -lifcore'
#FORTRAN_LIBRARY =' -L/opt/intel/fc/9.1.032/lib -lifcore -ldl'
#USE_PTC=YES
#endif




