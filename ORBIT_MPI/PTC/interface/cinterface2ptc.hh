
//------------------------------------------------------------------
//C interface to the subroutines and functions in interface.for
//-------------------------------------------------------------------

#ifndef PTC_C_INTERFACE_H
#define PTC_C_INTERFACE_H

//main subroutine in the Model initial code 
extern "C" void ptc_init_(const char* file_name, int length_of_name);

//get initial twiss at entrance of the ring
extern "C" void ptc_get_twiss_init_(double* betax,double* betay, 
                                    double* alphax,double* alphay,
                                    double* etax,double* etapx);

//get number of PTC ORBIT nodes, harmonic number, length of the ring, and gamma transition
extern "C" void ptc_get_ini_params_(int* nNodes, int* nHarm, double* lRing, double* gammaT);


//get synchronous particle params mass, charge, and energy
extern "C" void ptc_get_syncpart_(double* mass, int* charge, double* kin_energy);


//call ptc method to set up synchronous particle calualtions (before and after node)
//this method should be called from PTC_Map instance (TransMap.cc file, ORBIT source)
//These methods are located inside PTC (not interfaces)
//if node_index < 0 then PTC will do something inside that does not relate to tracking
extern "C" void ptc_synchronous_set_(int* node_index);
extern "C" void ptc_synchronous_after_(int* node_index);

//It reads the acceleration table into the ptc code
extern "C" void ptc_read_accel_table_(const char* file_name, int length_of_name);


//get twiss and length for a node with index
extern "C" void ptc_get_twiss_for_node_(int* node_index, double* length,
                                    double* betax,double* betay, 
                                    double* alphax,double* alphay,
                                    double* etax,double* etapx);

//track 6D coordinates through the PTC-ORBIT node
extern "C" void ptc_track_particle_(int* node_index, 
                                    double* x, double* xp, 
                                    double* y, double* yp, 
                                    double* phi, double* dE);

// reads additional ptc commands from file and execute them inside ptc 
extern "C" void ptc_script_(const char* p_in_file, int length_of_name);

//===========================================================
// This subroutine should be called before particle tracking.
//  It specifies the type of the task that will be performed
//  in ORBIT before particle tracking for particular node.
//  i_task = 0 - do not do anything
//  i_task = 1 - energy of the sync. particle changed
//===========================================================
extern "C" void  ptc_get_task_type_(int* i_node,int* i_task);

//===================================================
//It returns the lowest frequency of the RF cavities
//This will be used to transform phi to z[m] 
//===================================================
extern "C" void  ptc_get_omega_(double* X);

#endif
