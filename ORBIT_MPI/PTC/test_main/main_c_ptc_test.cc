#include <iostream>
#include <cstdlib>
#include <cstring>

#include "cinterface2ptc.hh" 

//=======================================
//MAIN METHOD
//=======================================
int main(int argc, char *argv[])
{ 
  std::cout<<"Start.\n";
 
  int length_of_name = 128;
  char file_name[length_of_name];

  if(argc >= 2){
    strncpy(file_name,argv[1],length_of_name-1);
  }
  else{
    std::cout<<"Usage $./main_test <input data file> \n";
    exit(1);
  }
  std::cout<<"File name in C++="<<file_name<<"\n";

  ptc_init_(file_name,length_of_name-1);

  std::cout<<"Stop.\n";
 
}


