//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    rcs_acc_with_sc.sc
//
// AUTHOR
//    shishlo
//
// CREATED
//    08/29/2007
//
// DESCRIPTION
//    Acceleration after injection in RCS.
//    The longitudinal Space Charge (SC) and impedances , and transverse
//    SC (2.5D) with boundary conditions are included.
//
///////////////////////////////////////////////////////////////////////////

//MPI auxiliary variables definition
Integer nRank=MPI_rank();
String sRank = "_";
MPI_String_Add_Integer(sRank,nRank);

Integer i = 0;

// Files for output:
String fSuffix = sRank;
if(nRank != 0) {
  fSuffix = fSuffix + ".tmp";
 }
 else{
  fSuffix = fSuffix + ".dat";
 }

OFstream fio("rcs_acc_with_sc" + fSuffix, ios::out);

//////////////////////////////
// Make a synchronous particle:
//////////////////////////////

Real TSync  = 0.4;     // Kinetic Energy (GeV)
Real mSync  = 1.0;     // Mass (AMU)
Real charge = 1.0;     // charge number

addSyncPart(mSync,charge,TSync);

Real GAMMA = 1.0 + TSync / (0.93827231 * mSync);
Real BETA = sqrt(1.0 - 1.0 / (GAMMA * GAMMA));


//nMaxMacroParticles will limit ORBIT injection
//This variable is global, and it was defined in Injection module.
Integer ACCELERATION_TURNS = 15330;
Injection::nMacrosPerTurn = 0;
Injection::nMaxMacroParticles = 10000;

//The initial value here is irrelevant.
//Orbit will manage this limit automatically
//This variable is globa, and it was defined in Particles module.
mainHerd = addMacroHerd(100);

//We read the macroparticles coordinates from bunch file
readParts(mainHerd, "test_bunch.dat", Injection::nMaxMacroParticles);

Injection::nMaxMacroParticles = getHerdSizeGlobal(mainHerd);

//The macrosize per macro-particle. It is important for the space-charge
//calculations.
//This variable is globa, and it was defined in Injection module
nReals_Macro = 8.3e13/(Injection::nMaxMacroParticles);

if(nRank == 0) cerr <<" Main herd is ready. \n"
		    <<" nParts ="<< Injection::nMaxMacroParticles <<"\n"
		    <<" nReals_Macro ="<< nReals_Macro <<"\n";

//////////////////////////////
// Make  a Ring
//////////////////////////////

const Integer nstepTPD = 1;
const Integer fringeD = 1;
const Integer nstepTPM = 4;
const Integer fringeM = 1;
const Integer nstepTPQ = 4;
const Integer fringeQ = 1;
const Integer nstepTPB = 10;
const Integer fringeB = 1;
const Integer nstepTPS = 4;
const Integer fringeS = 1;
const Integer nstepTPK = 4;
const Integer fringeK = 1;

buildTPlattice("../RCS_TWISS",
	       "../RCS_TEAPOT.LAT",
	       nstepTPD, fringeD,
	       nstepTPM, fringeM,
	       nstepTPQ, fringeQ,
	       nstepTPB, fringeB,
	       nstepTPS, fringeS,
	       nstepTPK, fringeK);

Ring::lRing = 348.333;
Ring::harmonicNumber = 1;
Ring::gammaTrans = 4.883456;
Ring::nuX=6.68;
Ring::nuY=6.27;

//////////////////////////////
// Add an ideal bumps and waveforms
//////////////////////////////

Real time_stop =  0.5; // msec
Real x_amp_paint = 0.;
Real y_amp_paint = 0.;

Void sqrBump()
{
   Real t_frac  = sqrt(Ring::time / time_stop);
   Bump::xIdealBump = x_amp_paint + 0.0*t_frac;
   Bump::xPIdealBump= 0.0*t_frac;
   Bump::yIdealBump = y_amp_paint + 0.0*t_frac;
   Bump::yPIdealBump= 0.0*t_frac;
}

//Bump Module: addIdealBump(Name,Position Index, Up(+1) or Down(-1), Subroutime)
//Bumps have lattice indexes 1 and 3 (Foild will be between with index=2)
addIdealBump("UpBump",   1,  1, sqrBump);
addIdealBump("DownBump", 3, -1, sqrBump);

//////////////////////////////
// Add foil node
//////////////////////////////

Real xfoilmin = -5.1;
Real xfoilmax = +5.1;
Real yfoilmin = -50.0;
Real yfoilmax = +50.0;

//    useFoilScattering = 0;  - No foil-beam interaction!
//    useFoilScattering = 1;  - scattering model from ACCSIM
//    useFoilScattering = 2;  - physics from collimation module:
//                              ionization energy loss
//                              small angle coulomb scattering
//                              nuclear elastic and inelastic scattering

useFoilScattering = 2;

//Foil Thickness (micro-gram/cm^2)
Real foilThickness = 60.;

//foil has index 2
addFoil("Foil", 2, xfoilmin, xfoilmax, yfoilmin, yfoilmax,foilThickness);

//////////////////////////////
// RF Acceleration
//////////////////////////////
Accelerate::AccelChoice = 0;

Accelerate::rhoBend = 2.946*24/(2*pi);
Real rest_m = mSync * 0.93827231; //Rest mass in GeV
Accelerate::BSynch = BETA*GAMMA*rest_m*1.0e+9/(rhoBend*2.99792458e+8);
cerr <<"BSynch [T]="<< BSynch <<"\n"; //Initial B in [T]
// BSynch = gamma*beta*m0/(rho*c)
//It should be BSynch = 0.282867635571
//              GAMMA = 1.42631546912
//              BETA  = 0.71305591521

//Read the [time, field, [voltage, phase]] table.
//First line: the number of hramonics [harm index ...]
//Field [T], Voltage [kV], Phase [rad]
getRampedBV("time_field_voltage.dat");
Real coeff_bsynch = 0.282867635571/BSynchPoints(1);

//rescale voltage (we use only 1-st harmonic here)
for(i = 1; i <= Accelerate::nTimePoints; i++){
	RFVoltPoints(1,i) = RFVoltPoints(1,i) *1.3;
	BSynchPoints(i) = coeff_bsynch*BSynchPoints(i)
	                  	*(1.0 + 0.0145*(i-1)/(1.0+Accelerate::nTimePoints));
}

cerr <<"Number of harmonics="<< nRFHarmonics <<"\n";
addRampedBAccel("Accelerate", 8367, Accelerate::InterpolateBV, nRFHarmonics, RFVolts, RFHarmNum, RFPhase);
//////////////////////////////////////////////////////////
// Add a Longitudinal Space Charge Node
//////////////////////////////////////////////////////////

//external longitudinal empedance - none
//ZImped(i) - complex values components in Ohm
//where i - mode of revolution frequency i = 1,2,3,...
ComplexVector ZImped(512);
ZImped = Complex(0.,0.);

nLongBins = 128;
Real b_a =  1.5;
Integer useAvg = 1;
Integer nMacroLSCMin = 100;
Integer useSpaceCharge = 1;

addFFTLSpaceCharge("LSC1", 8558, ZImped, b_a, useAvg, nMacroLSCMin, useSpaceCharge);

if( nRank == 0 ) {
	cerr << "===== Longitudinal Space Charge Node added ===== \n";
	cerr << "Number of long. bins ="<< nLongBins <<"\n";
	cerr << "Ratio (Pipe/Beam) Radii ="<< b_a <<"\n";
}

//////////////////////////////////////////////////////////////////
// Add a Transverse Space Charge Node Set
//////////////////////////////////////////////////////////////////

//Grid size in X and Y directions
Integer nxBins = 64;
Integer nyBins = 64;

//smoothing parameter - do not touch
Real eps = 1.e-6;

// shape = "Ellipse" BP1 is 1/2 horizontal size, BP2 - 1/2 vertical size
// shape = "Circle" BP1 - radius
// shape = "Rectangle" BP1 - minX, BP2 - maxX, BP3 - minY, BP4 - maxY
// Shape could be "Ellipse","Rectangle","Circle","None"
// If Shape == "None" boundary conditions will not be used
// Sizes in [mm]

String bShape= "Circle";
Real BP1 =  120.;
Real BP2 =  120.;
Real BP3 =  120.;
Real BP4 =  120.;

//boundary points
Integer bPoints = 64;

//number of circular modes in the boundary induced field
Integer bModes = 12;

//do not touch
Real gFact = 2.0;

//min number of macros for SC to work
Integer nMacroTSCMin = 100;

addPotentialTransSCSet(nxBins, nyBins, eps, bShape,
                       BP1,BP2,BP3,BP4,
                       bPoints, bModes, gFact,
                       nMacroTSCMin);

if( nRank == 0 ) {
	cerr << "===== Transverse Space Charge Nodes added ===== \n";
	cerr << "nxBins ="<< nxBins <<"\n";
	cerr << "nyBins ="<< nyBins <<"\n";
	cerr << "Shape="<< bShape <<"\n";
	cerr << "BP1="<< BP1 <<"\n";
	cerr << "BP2="<< BP2 <<"\n";
	cerr << "BP3="<< BP3 <<"\n";
	cerr << "BP4="<< BP4 <<"\n";
	cerr << "Boundary points ="<< bPoints <<"\n";
	cerr << "Boundary field modes="<< bModes <<"\n";
	cerr << "Min number of macroparts="<< nMacroTSCMin <<"\n";
	cerr << "============================================ \n";
}

//////////////////////////////
// Add magnetic apertures around the ring, and beam pipe in
// collimator section.
//////////////////////////////

//beam pipe radius in [mm]
Real beamPipeRadius = 200.0;
addCircAperture("Beam Pipe 100", 1001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 2001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 3001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 4001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 5001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 6001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 7001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 8001, beamPipeRadius, 0.0, 0.0, 0);

//You could use aperture and collimators (with nuclear interaction)
//see the Collimato.cc and Aperture.cc  files for explanations.
//addElliptAperture("Arc dipole", 345, 115.8, 78.7, 0.0, 0.0, 0);
//addCollimator("Scraper 1t", 1380, 0.0045,  5,  1, 3, 35.78,  0.,  0., 0., 0.);
//////////////////////////////
// Start Output:
//////////////////////////////

showStart(fio);
showErrors(fio);
fio.close();

//////////////////////////////
// do tracking
//////////////////////////////


if( nRank == 0 ) cerr <<"Start tracking. \n";

for(i = 1; i <= ACCELERATION_TURNS; i++){
  doTurn(1);
	Integer nParticles = getHerdSizeGlobal(mainHerd);
	Real  eng_kin = getSyncPartEnergy();
	if(i % 10 == 0){
	  if( nRank == 0 ) cerr <<"Turn i="<<i
		                      <<" TK[eV]="<< eng_kin*1.0e+9
													<<" nParts="<< nParticles
													<<" time[ms]="<< Ring::time
													<<"\n";
	}
}

/////////////////////////////
// Dump particles into a file
//////////////////////////////
OFstream fioBunch("bunch"+fSuffix, ios::out);
dumpPartsGlobal(mainHerd, fioBunch);
fioBunch.close();


///////////////////////////////////
// Dump lost particles into a file
///////////////////////////////////
OFstream fioLost("LostParts"+fSuffix , ios::out);
fioLost << "Number of ParticlesMissing Foil at Injection = "<< nMacrosMissedFoil << "\n\n";
dumpLostPartsGlobal(mainHerd, fioLost);
fioLost.close();

OFstream fioCollHits("Collim_Apert_Hits"+fSuffix, ios::out);
dumpCollimatorHits(mainHerd, fioCollHits);
fioCollHits<< "\n\n";
dumpApertureHits(mainHerd, fioCollHits);
fioCollHits.close();


//Stop execution
if( nRank == 0 ) cerr << "Stop.\n";
quit
