//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    rcs_injection.sc
//
// AUTHOR
//    shishlo
//
// CREATED
//    08/24/2007
//
// DESCRIPTION
//    Injection into RCS without E-Cloud and Impedances
//    User can remove Transverse SC to speed up simulations.
//    Remember that transverse impedances cannot be used with 2.5D SC
//    Only 3D Space Charge should be used for transverse instabilities.
//
///////////////////////////////////////////////////////////////////////////

//MPI auxiliary variables definition
Integer nRank=MPI_rank();
String sRank = "_";
MPI_String_Add_Integer(sRank,nRank);

// Files for output:
String fSuffix = sRank;
if(nRank != 0) {
  fSuffix = fSuffix + ".tmp";
 }
 else{
  fSuffix = fSuffix + ".dat";
 }

OFstream fio("rcs_injection" + fSuffix, ios::out);

//////////////////////////////
// Make a synchronous particle:
//////////////////////////////

Real TSync  = 0.4;     // Kinetic Energy (GeV)
Real mSync  = 1.0;     // Mass (AMU)
Real charge = 1.0;     // charge number

addSyncPart(mSync,charge,TSync);

Real GAMMA = 1.0 + TSync / (0.93827231 * mSync);
Real BETA = sqrt(1.0 - 1.0 / (GAMMA * GAMMA));


//nMaxMacroParticles will limit ORBIT injection
//This variable is global, and it was defined in Injection module.
Integer INJ_TURNS = 381;
Injection::nMacrosPerTurn = 100;
Injection::nMaxMacroParticles = INJ_TURNS*Injection::nMacrosPerTurn;

//The initial value here is irrelevant.
//Orbit will manage this limit automatically
//This variable is globa, and it was defined in ???? module.
mainHerd = addMacroHerd(100);

//We could read the macroparticles coordinates from bunch file
// if we want to. Here we do not want it.
//Integer nParticlesToRead = 20000;
//readParts(mainHerd, "test_bunch.dat", nParticlesToRead);

//How many macro-particles we have on all CPUs
//If we did not read from bunch-file and did not inject it will be 0.
Integer nParticles = getHerdSizeGlobal(mainHerd);

//The macrosize per macro-particle. It is important for the space-charge
//calculations.
//This variable is globa, and it was defined in ???? modul
nReals_Macro = 8.3e13/(Injection::nMaxMacroParticles);

if(nRank == 0) cerr <<" Main herd is ready. \n"
		    <<" nParts ="<< Injection::nMaxMacroParticles <<"\n"
		    <<" nReals_Macro ="<< nReals_Macro <<"\n"
		    <<" nParticles ="<< nParticles <<"\n"
		    <<" total macrosize ="<< nParticles*Injection::nReals_Macro <<"\n";


//////////////////////////////
// Make  a Ring
//////////////////////////////

const Integer nstepTPD = 1;
const Integer fringeD = 1;
const Integer nstepTPM = 4;
const Integer fringeM = 1;
const Integer nstepTPQ = 4;
const Integer fringeQ = 1;
const Integer nstepTPB = 10;
const Integer fringeB = 1;
const Integer nstepTPS = 4;
const Integer fringeS = 1;
const Integer nstepTPK = 4;
const Integer fringeK = 1;

buildTPlattice("../RCS_TWISS",
	       "../RCS_TEAPOT.LAT",
	       nstepTPD, fringeD,
	       nstepTPM, fringeM,
	       nstepTPQ, fringeQ,
	       nstepTPB, fringeB,
	       nstepTPS, fringeS,
	       nstepTPK, fringeK);

Ring::lRing = 348.333;
Ring::harmonicNumber = 1;
Ring::gammaTrans = 4.883456;
Ring::nuX=6.68;
Ring::nuY=6.27;

/////////////////////////////////////
// Injection params
/////////////////////////////////////
//epsXRMSInj   - "Injected beam ellipse X RMS emittance [pi mm-mrad]"
//epsYRMSInj   - "Injected beam ellipse Y RMS emittance [pi mm-mrad]"

//X(Horizontal distr)
betaXInj = 1.0; alphaXInj = 0.0;
epsXRMSInj = 6.0/pi; MXJoho = 3.;
epsXLimInj = epsXRMSInj * 2 * (MXJoho+1);
x0Inj = 0.0;   xP0Inj = 0.0;
addXInitializer("Gaussian-X",JohoXDist);

//X(Horizontal distr)
betaYInj = 1.0; alphaYInj = 0.0;
epsYRMSInj = 6.0/pi; MYJoho = 3.;
epsYLimInj  = epsYRMSInj * 2 * (MYJoho+1);
y0Inj = 0.0;   yP0Inj = 0.0;
addYInitializer("Gaussian-Y",JohoYDist);

//Longitudinal Distribution
Integer nPhiPoints = 1000;
RealVector phiV(nPhiPoints);
RealVector phiRhoV(nPhiPoints);
Real phiWidth = 0.56;

//injects two sub-bunches around pi/2 and -pi/2
Integer i = 0;
Real phi = 0.;
Real val = 0.;
for( i = 1; i <= nPhiPoints; i++){
  phi = -pi + (2.0*pi*i)/(1.0001*nPhiPoints);
	val = 0.;
	if( (phi - (-pi/2)) < pi*phiWidth/2.0){
	  if( (phi - (-pi/2)) > -pi*phiWidth/2.0){
		   val = 1.0;
		}
	}

	if( (phi - (pi/2)) < pi*phiWidth/2.0){
	  if( (phi - (pi/2)) > -pi*phiWidth/2.0){
		   val = 1.0;
		}
	}

  phiV(i) =  phi;
  phiRhoV(i) = val;
}

Integer nEnrgPoints = 200;
RealVector enrgV(nEnrgPoints);
RealVector enrgRhoV(nEnrgPoints);
Real dEovrE = 0.003*GAMMA*BETA*BETA*(0.93827231 * mSync);

// energy offset in GeV
Real energyShift = 0.0;

Real dE = 0.;
for( i = 1; i <= nEnrgPoints; i++){
  dE = -3*dEovrE + 2*3*dEovrE*i/(nEnrgPoints*1.001);
	val = exp(-dE*dE/(2.0*dEovrE*dEovrE));
  enrgV(i) =  dE;
  enrgRhoV(i) = val;
}

setupInjArbLongDistr(nPhiPoints,phiV,phiRhoV, nEnrgPoints, enrgV, enrgRhoV);
addLongInitializer("Arbitrary -L",Injection::ArbitraryLongDist);

//////////////////////////////
// Add an ideal bumps and waveforms
//////////////////////////////

Real time_stop =  0.5; // msec
Real x_amp_paint = 0.;
Real y_amp_paint = 0.;

Void sqrBump()
{
   Real t_frac  = sqrt(Ring::time / time_stop);
   Bump::xIdealBump = x_amp_paint + 0.0*t_frac;
   Bump::xPIdealBump= 0.0*t_frac;
   Bump::yIdealBump = y_amp_paint + 0.0*t_frac;
   Bump::yPIdealBump= 0.0*t_frac;
}

//Bump Module: addIdealBump(Name,Position Index, Up(+1) or Down(-1), Subroutime)
//Bumps have lattice indexes 1 and 3 (Foild will be between with index=2)
addIdealBump("UpBump",   1,  1, sqrBump);
addIdealBump("DownBump", 3, -1, sqrBump);

//////////////////////////////
// Add foil node
//////////////////////////////

Real xfoilmin = -5.1;
Real xfoilmax = +5.1;
Real yfoilmin = -50.0;
Real yfoilmax = +50.0;

//    useFoilScattering = 0;  - No foil-beam interaction!
//    useFoilScattering = 1;  - scattering model from ACCSIM
//    useFoilScattering = 2;  - physics from collimation module:
//                              ionization energy loss
//                              small angle coulomb scattering
//                              nuclear elastic and inelastic scattering

useFoilScattering = 2;

//Foil Thickness (u-g/cm^2)
Real foilThickness = 60.;

//foil has index 2
addFoil("Foil", 2, xfoilmin, xfoilmax, yfoilmin, yfoilmax,foilThickness);

//////////////////////////////
// Add RF Cavities
//////////////////////////////

//   nHarm    Number of harmonic components to use in the voltage waveform
//   voltage  Peak voltage vector [kV]
//   harmNum  Harmonic number vector for RF Voltage
//   phase    phase vector for RF voltage [rad]

  Integer nRFHarms = 2;
  RealVector volts(nRFHarms), harmNum(nRFHarms), RFPhase(nRFHarms);
	harmNum(1)  = 1.; harmNum(2)   = 2.;
  volts(1)    = 0.;  volts(2)    = 140.;
  RFPhase(1)  = 0.;  RFPhase(2)  = pi;

  addRFCavity("RF 1", 8217, nRFHarms, volts, harmNum, RFPhase);
  addRFCavity("RF 2", 8367, nRFHarms, volts, harmNum, RFPhase);
  addRFCavity("RF 3", 8557, nRFHarms, volts, harmNum, RFPhase);


//////////////////////////////////////////////////////////
// Add a Longitudinal Space Charge Node
//////////////////////////////////////////////////////////

//external longitudinal empedance - none
//ZImped(i) - complex values components in Ohm
//where i - mode of revolution frequency i = 1,2,3,...
ComplexVector ZImped(512);
ZImped = Complex(0.,0.);

nLongBins = 128;
Real b_a =  1.5;
Integer useAvg = 1;
Integer nMacroLSCMin = 1;
Integer useSpaceCharge = 1;

addFFTLSpaceCharge("LSC1", 8558, ZImped, b_a, useAvg, nMacroLSCMin, useSpaceCharge);

if( nRank == 0 ) {
	cerr << "===== Longitudinal Space Charge Node added ===== \n";
	cerr << "Number of long. bins ="<< nLongBins <<"\n";
	cerr << "Ratio (Pipe/Beam) Radii ="<< b_a <<"\n";
}

//////////////////////////////////////////////////////////////////
// Add a Transverse Space Charge Node Set
//////////////////////////////////////////////////////////////////

//Grid size in X and Y directions
Integer nxBins = 128;
Integer nyBins = 128;

//smoothing parameter - do not touch
Real eps = 1.e-6;

// shape = "Ellipse" BP1 is 1/2 horizontal size, BP2 - 1/2 vertical size
// shape = "Circle" BP1 - radius
// shape = "Rectangle" BP1 - minX, BP2 - maxX, BP3 - minY, BP4 - maxY
// Shape could be "Ellipse","Rectangle","Circle","None"
// If Shape == "None" boundary conditions will not be used
// Sizes in [mm]

String bShape= "Circle";
Real BP1 =  120.;
Real BP2 =  120.;
Real BP3 =  120.;
Real BP4 =  120.;

//boundary points
Integer bPoints = 64;

//number of circular modes in the boundary induced field
Integer bModes = 12;

//do not touch
Real gFact = 2.0;

//min number of macros for SC to work
Integer nMacroTSCMin = 1000;

addPotentialTransSCSet(nxBins, nyBins, eps, bShape,
                       BP1,BP2,BP3,BP4,
                       bPoints, bModes, gFact,
                       nMacroTSCMin);

if( nRank == 0 ) {
	cerr << "===== Transverse Space Charge Nodes added ===== \n";
	cerr << "nxBins ="<< nxBins <<"\n";
	cerr << "nyBins ="<< nyBins <<"\n";
	cerr << "Shape="<< bShape <<"\n";
	cerr << "BP1="<< BP1 <<"\n";
	cerr << "BP2="<< BP2 <<"\n";
	cerr << "BP3="<< BP3 <<"\n";
	cerr << "BP4="<< BP4 <<"\n";
	cerr << "Boundary points ="<< bPoints <<"\n";
	cerr << "Boundary field modes="<< bModes <<"\n";
	cerr << "Min number of macroparts="<< nMacroTSCMin <<"\n";
	cerr << "============================================ \n";
}
//////////////////////////////
// Add magnetic apertures around the ring, and beam pipe in
// collimator section.
//////////////////////////////

//beam pipe radius in [mm]
Real beamPipeRadius = 100.0;
addCircAperture("Beam Pipe 100", 1001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 2001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 3001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 4001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 5001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 6001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 7001, beamPipeRadius, 0.0, 0.0, 0);
addCircAperture("Beam Pipe 100", 8001, beamPipeRadius, 0.0, 0.0, 0);

//You could use aperture and collimators (with nuclear interaction)
//see the Collimato.cc and Aperture.cc  files for explanations.
//addElliptAperture("Arc dipole", 345, 115.8, 78.7, 0.0, 0.0, 0);
//addCollimator("Scraper 1t", 1380, 0.0045,  5,  1, 3, 35.78,  0.,  0., 0., 0.);
//////////////////////////////
// Start Output:
//////////////////////////////

showStart(fio);
showErrors(fio);
fio.close();

//////////////////////////////
// do tracking
//////////////////////////////
//turnToNode(mainHerd,3);

if( nRank == 0 ) cerr <<"Start tracking. \n";

for(i = 1; i <= INJ_TURNS; i++){
  doTurn(1);
	nParticles = getHerdSizeGlobal(mainHerd);
	if(i % 1 == 0){
	  if( nRank == 0 ) cerr <<"Turn i="<<i<<" nParts="<< nParticles <<" time="<< Ring::time <<" done \n";
	}
}

/////////////////////////////
// Dump particles into a file
//////////////////////////////
OFstream fioBunch("bunch"+fSuffix, ios::out);
dumpPartsGlobal(mainHerd, fioBunch);
fioBunch.close();


///////////////////////////////////
// Dump lost particles into a file
///////////////////////////////////
OFstream fioLost("LostParts"+fSuffix , ios::out);
fioLost << "Number of ParticlesMissing Foil at Injection = "<< nMacrosMissedFoil << "\n\n";
dumpLostPartsGlobal(mainHerd, fioLost);
fioLost.close();

OFstream fioCollHits("Collim_Apert_Hits"+fSuffix, ios::out);
dumpCollimatorHits(mainHerd, fioCollHits);
fioCollHits<< "\n\n";
dumpApertureHits(mainHerd, fioCollHits);
fioCollHits.close();


//Stop execution
if( nRank == 0 ) cerr << "Stop.\n";
quit
