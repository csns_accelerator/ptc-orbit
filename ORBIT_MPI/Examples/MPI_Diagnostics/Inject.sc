//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    SYNCH_8GeV.sc
//
// AUTHOR J. Holmes (jzh@ornl.gov)
//
// CREATED
//    02/15/2002
//
// MODIFIED
//
// DESCRIPTION
//    FNAL Proton Driver Synchrotron
//
///////////////////////////////////////////////////////////////////////////

// Files for output:

  String of1;
  runName = "SYNCH_8GeV";
  of1 = runName + ".out";

  OFstream fio(of1, ios::out);

//////////////////////////////
// Make a synchronous particle:
//////////////////////////////

  Real TSync = 0.600;    // Kinetic Energy (GeV)
  Real mSync = 1;        // Mass (AMU)
  Real charge = 1;       // charge number

  addSyncPart(mSync, charge, TSync);

  mainHerd = addMacroHerd(100000);

//////////////////////////////
// Make  a Ring 
//////////////////////////////

  readMADFile("Inj_RaceTrack_01.tw", "Inj_RaceTrack_01.out");
  Ring::lRing = 474.000120;
  Ring::gammaTrans = 13.975418;
  Ring::harmonicNumber = 84;  // 84 Harmonics

////////////////////////////////////////////
// Make Linac X & Y Distribution functions
////////////////////////////////////////////

  betaXInj = 14.610; alphaXInj = 1.561;
  MXJoho = 100.;
  epsXLimInj = 1.57 * (MXJoho+1)/2.; // 95% Normalized emit 3 pi
  x0Inj = 0.0; xP0Inj = 0.0;
//  x0Inj = spot at nobump; xP0Inj = spot at nobump;
  xTailFraction = 0.0; xTailFactor = 3.;
  addXInitializer("Bi-Gaussian-X", JohoXDist);

  betaYInj = 6.704; alphaYInj = -0.101;
  MYJoho = 100.;
  epsYLimInj = 1.57 * (MYJoho+1)/2.; // 95% Normalized emit 3 pi
  y0Inj = 0.0; yP0Inj = 0.0;
//  y0Inj = spot at nobump; yP0Inj = spot at nobump;
  yTailFraction = 0.0; yTailFactor = 3.;
  addYInitializer("Bi-Gaussian-Y", JohoYDist);

  phiMinInj = -180.0;
  phiMaxInj =  180.0;
  EOffset = 0.0;
  deltaEFracInj = 8.333333e-04;
  addLongInitializer("Uniform",UniformLongDist);

//////////////////////////////
// Add ideal bump nodes
//////////////////////////////

  Real xBamp = 9.10, xpBamp = 0.0, yBamp = 0.0, ypBamp = 1.68;
  Real BumpTime = 0.08978589;
  Real TimeFactor, TimeFactorc, tfract, tfractc;
  Void FNALBump()
  {
    tfract = time / BumpTime;
    tfractc = 1.0 - tfract;
    TimeFactor = pow(2.0 * tfract - tfract * tfract , 0.5);
    TimeFactorc = pow(2.0 * tfractc - tfractc * tfractc , 0.5);
    xIdealBump = xBamp * TimeFactor;
    xPIdealBump = 0.0;
    yIdealBump = 0.0;
    yPIdealBump = ypBamp * TimeFactorc;
  }

  addIdealBump("DownBump", 1, 1, FNALBump);
  addIdealBump("UpBump", 3, 0, FNALBump);

//////////////////////////////
// Add a "3-sided" foil node
//////////////////////////////

  addFoil("Foil",  2, -100.00, 100.00, -100.00, 100.00, 300.);
//  useFoilScattering = 1;
  useFoilScattering = 0;

// Inject nMacrosPerTurn particles per turn, up to 45 turns

  nMacrosPerTurn = 2000;
  nMaxMacroParticles = 45 * nMacrosPerTurn;
  nReals_Macro = 3.00e11/Real(nMaxMacroParticles);

//////////////////////////////
// Add an RF Cavity
//////////////////////////////

  Real tFactor;
  Integer nRFHarms = 1;
  RealVector volts(nRFHarms), harmNum(nRFHarms), RFPhase(nRFHarms);
  harmNum(1) = 1;  // Remember Ring::harmonicNumber
  RFPhase(1) = 0.;
  Void PDIIVolts()
  {
   tFactor = time/0.08978589;
   if(tFactor > 1.) tFactor = 1.;
   volts(1) = 0.0 + 0.0 * tFactor;
  }
  addRampedRFCavity("RF 1", 1665, nRFHarms, volts, harmNum, RFPhase, PDIIVolts);
  Accelerate::rhoBend = 19.86254;

//////////////////////////////
// Add a Longitudinal Space Charge Node
//////////////////////////////

  ComplexVector ZImped(64);
  ZImped = Complex(0.,0.);

  nLongBins = 64;
  Real b_a = 4.;
  Integer useAvg = 1;
  Integer nMacroLSCMin = 128;
  Integer useSpaceCharge = 1;
  addFFTLSpaceCharge("LSC1", 20, ZImped, b_a, useAvg,
                     nMacroLSCMin, useSpaceCharge);

///////////////////////////////////////////
// Add a Transverse Space Charge Node Set
///////////////////////////////////////////

  Integer nxBins = 128, nyBins = 128;
  Real eps = 1.e-6;   // smoothing parameter
  addFFTTransSCSet(nxBins, nyBins, eps, nMacroLSCMin);

///////////////////////////////////////////
// Add Moment and StatLat Nodes
///////////////////////////////////////////

  addMomentNode("MomentNode(0)", 7, 4, "Moments");       
  addStatLatNode("StatLatNode(0)", 8, "StatLats");       
  activateMomentNode(1);
  activateStatLatNode(1);

// Set up to histogram the distributions:

  Integer i, nBins = 120;
  RealVector xHist(nBins), yHist(nBins);

  Real xGMin, dx, yGMin, dy, phiGMin, dphi;
  xGMin = -60.0; dx = 120.0/Real(nBins);
  yGMin = -60.0; dy = 120.0/Real(nBins);
  phiGMin = -3.1415926539 / harmNum(1); dphi = -2.*phiGMin/Real(nBins);

// Emittance Output:

  nEmitBins = 100;   deltaEmitBin = 0.5;  // set up emittance bins

//////////////////////////////
// Start Output:
//////////////////////////////

  showStart(fio);

//////////////////////////////
// do some turns, and dump particles for later plots:
//////////////////////////////

 cerr << "Start Tracking\n";
  Real et;
  timerOn();

  doTurn(14);
  startTuneCalc();
  doTurn(1);
  stopTuneCalc();
  OFstream fio5("T_and_A_00015", ios::out);
  dumpTAndA(mainHerd, fio5);
  OFstream fio6("Bm_Parts_00015", ios::out);
  dumpParts(mainHerd, fio6);
  showTurnInfo(fio);
  showTurnInfo(cerr);
  OFstream fio20("Xhist_00015", ios::out);
  xHist(1) = xGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dx;
  binHorizontal(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio20 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio21("Yhist_00015", ios::out);
  xHist(1) = yGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dy;
  binVertical(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio21 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio41("Phihist_00015", ios::out);
  xHist(1) = phiGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dphi;
  binLongitudinal(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio41 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio51("Emithist_00015", ios::out);
  showEmit(fio51);
  fio5.close();
  fio6.close();
  fio20.close();
  fio21.close();
  fio41.close();
  fio51.close();
  et = elapsedTime();
  showTiming(cerr, et);  
  showTiming(fio, et);

  doTurn(14);
  startTuneCalc();
  doTurn(1);
  stopTuneCalc();
  OFstream fio7("T_and_A_00030", ios::out);
  dumpTAndA(mainHerd, fio7);
  OFstream fio8("Bm_Parts_00030", ios::out);
  dumpParts(mainHerd, fio8);
  showTurnInfo(fio);
  showTurnInfo(cerr);
  OFstream fio22("Xhist_00030", ios::out);
  xHist(1) = xGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dx;
  binHorizontal(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio22 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio23("Yhist_00030", ios::out);
  xHist(1) = yGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dy;
  binVertical(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio23 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio42("Phihist_00030", ios::out);
  xHist(1) = phiGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dphi;
  binLongitudinal(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio42 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio52("Emithist_00030", ios::out);
  showEmit(fio52);
  fio7.close();
  fio8.close();
  fio22.close();
  fio23.close();
  fio42.close();
  fio52.close();
  et = elapsedTime();
  showTiming(cerr, et);  
  showTiming(fio, et);

  doTurn(14);
  startTuneCalc();
  doTurn(1);
  stopTuneCalc();
  OFstream fio9("T_and_A_00045", ios::out);
  dumpTAndA(mainHerd, fio9);
  OFstream fio10("Bm_Parts_00045", ios::out);
  dumpParts(mainHerd, fio10);
  showTurnInfo(fio);
  showTurnInfo(cerr);
  OFstream fio24("Xhist_00045", ios::out);
  xHist(1) = xGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dx;
  binHorizontal(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio24 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio25("Yhist_00045", ios::out);
  xHist(1) = yGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dy;
  binVertical(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio25 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio43("Phihist_00045", ios::out);
  xHist(1) = phiGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dphi;
  binLongitudinal(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio43 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio53("Emithist_00045", ios::out);
  showEmit(fio53);
  fio9.close();
  fio10.close();
  fio24.close();
  fio25.close();
  fio43.close();
  fio53.close();
  et = elapsedTime();
  showTiming(cerr, et);  
  showTiming(fio, et);

  startTuneCalc();
  doTurn(1);
  stopTuneCalc();
  OFstream fio11("T_and_A_00046", ios::out);
  dumpTAndA(mainHerd, fio11);
  OFstream fio12("Bm_Parts_00046", ios::out);
  dumpParts(mainHerd, fio12);
  showTurnInfo(fio);
  showTurnInfo(cerr);
  OFstream fio26("Xhist_00046", ios::out);
  xHist(1) = xGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dx;
  binHorizontal(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio26 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio27("Yhist_00046", ios::out);
  xHist(1) = yGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dy;
  binVertical(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio27 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio44("Phihist_00046", ios::out);
  xHist(1) = phiGMin;
  for(i=2; i<= nBins; i++) xHist(i) = xHist(i-1) + dphi;
  binLongitudinal(mainHerd, xHist, yHist);
  for(i=1; i<= nBins; i++)
    fio44 << xHist(i) << "\t" << yHist(i) << "\n";
  OFstream fio54("Emithist_00046", ios::out);
  showEmit(fio54);
  fio11.close();
  fio12.close();
  fio26.close();
  fio27.close();
  fio44.close();
  fio54.close();
  et = elapsedTime();
  showTiming(cerr, et);  
  showTiming(fio, et);

  deactivateMomentNodes();
  deactivateStatLatNodes();

  cerr << "Doing Emittance\n";

  showEmit(fio);
  showTunes(fio);

  fio.close();

  quit

