//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    .sc
//
// AUTHOR A. Shishlo
//
// CREATED
//    03//28/2007
//
// MODIFIED
//
// DESCRIPTION
//    How to read file in SuperCode
//
///////////////////////////////////////////////////////////////////////////

  IFstream fio("test.dat", ios::in);
 
  Real a1,a2,a3;
  Integer i1,i2,i3;

  fio >> a1 >> a2 >> a3;
  fio >> i1 >> i2 >> i3;

  fio.close();

  cerr << " a1,a2,a3="<< a1 << " " <<  a2 << " " << a3 << "\n"; 
  cerr << " i1,i2,i3="<< i1 << " " <<  i2 << " " << i3<< "\n";

  quit
