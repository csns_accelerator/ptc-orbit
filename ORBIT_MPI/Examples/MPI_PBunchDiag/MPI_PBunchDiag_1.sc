//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_PBunchDiag_1.sc
//
// AUTHOR
//    A. Shishlo
//
// CREATED
//    01/30/2004
//
//
// DESCRIPTION
//    Script file to check the EBunchDiag module.
//    How to perform diagnostic without creation of the lattice.
//
///////////////////////////////////////////////////////////////////////////

  Integer nRank = MPI_rank();

  String runName, of1;
  runName = "EBunchDiag_1";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);

  Real TSync = 1.;
  Real mSync = 1;
  Real charge = 1;
  addSyncPart(mSync,charge,TSync);

  nMaxMacroParticles = 1000;
  Integer chunkSize = 10000;
  nReals_Macro = 1.0e14/(nMaxMacroParticles);
  mainHerd = addMacroHerd(chunkSize);
  readParts(mainHerd, "bunch.dat", nMaxMacroParticles);

  if(nRank == 0) cerr << "Done reading mainherd:\n";

  //make diagnostic node
  Integer order = 55;
  Integer zSlices = 10;
  Integer nRecordsMax = 30;
  //String diagTypeName = "LONG_DENSITY";
  //String diagTypeName = "LONG_DENSITY_FFT";
  //String diagTypeName = "CENTROID_X";
  String diagTypeName = "CENTROID_X_FFT";
  String fileName = "pBunchDiag.dat";

  Integer pBunchDiagIndex0;
  pBunchDiagIndex0 = PBunchDiagMakeNode(order,zSlices,nRecordsMax,diagTypeName,fileName);

  if(nRank == 0) cerr << "Create PBunchDiag - Done.\n";
  
  //perform diagnostics
  PBunchDiagPerform(pBunchDiagIndex0);

  if(nRank == 0) cerr << "Diganostics done.\n";

  PBunchDiagDump(pBunchDiagIndex0);

  //print only 10 biggest columns
  //PBunchDiagDumpMax(pBunchDiagIndex0);

  if(nRank == 0) cerr << "Stop.\n";

  quit

