//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_PBunchDiag_0.sc
//
// AUTHOR
//    A. Shishlo
//
// CREATED
//    01/30/2004
//
//
// DESCRIPTION
//    Script file to check the EBunchDiag module.
//
///////////////////////////////////////////////////////////////////////////

  Integer nRank = MPI_rank();

  String runName, of1;
  runName = "EBunchDiag_0";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);

  Real TSync = 1.;
  Real mSync = 1;
  Real charge = 1;
  addSyncPart(mSync,charge,TSync);

  nMaxMacroParticles = 1000;
  Integer chunkSize = 10000;
  nReals_Macro = 1.0e14/(nMaxMacroParticles);
  mainHerd = addMacroHerd(chunkSize);
  readParts(mainHerd, "bunch.dat", nMaxMacroParticles);

  if(nRank == 0) cerr << "Done reading mainherd:\n";

/////////////////
// Make a Lattice
/////////////////

  if(nRank == 0) cerr << "Create lattice :\n"; 
  String LatType = "Ring";
  Real LengthTunes = 88.;
  Real XTune = 6.40;
  Real YTune = 6.30;
  Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 88;
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);
  if(nRank == 0) cerr << "Done Creating Lattice:\n"; 


  //make diagnostic node
  Integer order = 55;
  Integer zSlices = 5;
  Integer nRecordsMax = 30;
  //String diagTypeName = "LONG_DENSITY";
  //String diagTypeName = "LONG_DENSITY_FFT";
  //String diagTypeName = "CENTROID_X";
  String diagTypeName = "CENTROID_X_FFT";
  String fileName = "pBunchDiag.dat";

  Integer pBunchDiagIndex0;
  pBunchDiagIndex0 = PBunchDiagMakeNode(order,zSlices,nRecordsMax,diagTypeName,fileName);

  if(nRank == 0) cerr << "Create PBunchDiag - Done.\n";

  //dump the structure into the output file
  showNodes(fio);

  if(nRank == 0) cerr << "Start tracking.\n";
  
  PBunchDiagStart(pBunchDiagIndex0);

  doTurn(20);

  PBunchDiagStop(pBunchDiagIndex0);

  if(nRank == 0) cerr << "Stop tracking.\n";

  PBunchDiagDump(pBunchDiagIndex0);

  //print only 10 biggest columns
  //PBunchDiagDumpMax(pBunchDiagIndex0);

  if(nRank == 0) cerr << "Stop.\n";

  quit

