//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_FeedBack.sc
//
// DESCRIPTION
//    Script file for a uniform focusing lattice and
//    one feed back element (author:Slava Danilov).
//
///////////////////////////////////////////////////////////////////////////

  //MPI stuff
  Integer nRank=MPI_rank();
  String sRank;
  MPI_String_Add_Integer(sRank,nRank);

  String runName, of1;
  runName = "FEED_BACK_MPI";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);

  Real TSync = 1.;
  Real mSync = 1;
  Real charge = 1;
  addSyncPart(mSync,charge,TSync);

  nMaxMacroParticles = 50;
  Real BeamLength = 1.00;
  nReals_Macro = 1.00e14/(nMaxMacroParticles*BeamLength);
  mainHerd = addMacroHerd(nMaxMacroParticles);
  readParts(mainHerd, "Bm_Parts_ini_0", nMaxMacroParticles);

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done reading mainherd:\n";
  }

/////////////////
// Make a Lattice
/////////////////
 
  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Create lattice :\n"; 
  }

  String LatType = "Ring";
  Real LengthTunes = 12.;
  Real XTune = 6.40;
  Real YTune = 6.30;
  Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 12;
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done Creating Lattice:\n"; 
  }

///////////////////////////////////////////
// Add Moment and StatLat Nodes
///////////////////////////////////////////

  addMomentNode("MomentNode(0)", 4, 4, "Moments");       
  addStatLatNode("StatLatNode(0)", 5, "StatLats");       
  activateMomentNode(1);
  activateStatLatNode(1);

// Set up to histogram the distributions:

    Integer i, nBins = 60;
    RealVector xHist(nBins), yHist(nBins);

    Real xGMin, dx, yGMin, dy, phiGMin, dphi;
    xGMin = -39.; dx = 78./Real(nBins);
    yGMin = -39.; dy = 78./Real(nBins);
    phiGMin = -3.1415926539; dphi = -2.*phiGMin/Real(nBins);

// Emittance Output:
    nEmitBins = 100;   deltaEmitBin = 2.;  // set up emittance bins

//////////////////////////////
// Create Feedback lattice element:
//////////////////////////////
useXdimTFB = 1;
useYdimTFB = 1;

Integer useAvg = 1;
Integer TFBNturndelay=5;
RealVector CoordinateFilter(TFBNturndelay), AngleFilter(TFBNturndelay);
AngleFilter(1)=0.001;
Integer nBinsFB = 20;
Integer indOfFB1 = 19;

addTFeedBack("TFBack1", indOfFB1 , nBinsFB ,TFBNturndelay, useAvg,
             CoordinateFilter, AngleFilter);

//////////////////////////////
// Start Output:
//////////////////////////////

  showStart(fio);

//////////////////////////////
// do some turns, and dump particles for later plots:
//////////////////////////////


  //MPI stuff
  if( nRank == 0 ) {
   cerr << "Start Tracking\n";
  }


  Real et;
  timerOn();

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "======== Start My Calculation  =========\n";
  }

  //Integer index_Nodes = 2;
  //turnToNode(mainHerd, index_Nodes);
  doTurn(10);  

  if( nRank == 0 ) {
    cerr << "======== STOP Calculation  =========\n";
  }

//-------------------------------------------------------

  String nameOfHerd = "Bm_Parts_000_MPI_";
  nameOfHerd = nameOfHerd  + sRank + ".dat";
  OFstream fio1(nameOfHerd, ios::out);

  dumpPartsGlobal(mainHerd, fio1);
  fio1.close();

  quit

