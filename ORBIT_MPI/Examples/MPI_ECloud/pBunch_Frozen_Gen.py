#!/usr/bin/env python

import math
import random
import sys


class TransverseCoordGen:
    """ Generates u and u' coordinates distributed according
        to the Gaussian with certain parameters emit_rms and
        beta: exp(-(u^2+beta^2*(u')^2)/(2*beta*emit_rms)) """
    
    def __init__(self, beta, emit_rms, u_cutoff):
        self.beta = beta
        self.emit_rms = emit_rms
        self.u_cutoff = u_cutoff
        self.hamilton_max =  u_cutoff*u_cutoff
        self.parU = math.sqrt(1./(2*beta*emit_rms))
        self.parUP = math.sqrt(beta*beta/(2*beta*emit_rms))

    def getCoords(self):
        """ Returns u and u' [mm] and [mrad] """
        res = 0
        u = 0.
        up = 0.
        while(res == 0):
            u = random.gauss(0.,math.sqrt(0.5))/self.parU
            up = random.gauss(0.,math.sqrt(0.5))/self.parUP
            if( (u*u+self.beta*self.beta*up*up) <  self.hamilton_max):
                res = 1
        return (u,up*1000.)

#====================================================================
class Function:
    """ Keeps x,y pairs """
    
    def __init__(self):
        """ Create instance """
        #----------arrays --------
        self.__x_arr  = []   
        self.__y_arr  = []
        self.maxX = - 1.0e+200
        self.minX =   1.0e+200
        self.maxY = - 1.0e+200
        self.minY =   1.0e+200
        
    def __del__(self):
        del self.__x_arr
        del self.__y_arr

    def getSize(self):
        """ Returns the number of time steps """
        return len(self.__x_arr)

    def addXY(self,x,y):
        """ Adds pair x,y """
        if( len(self.__x_arr) <= 0 ):
            self.__x_arr.append(x)
            self.__y_arr.append(y)
        else:
            if( x > self.__x_arr[len(self.__x_arr)-1]):
                self.__x_arr.append(x)
                self.__y_arr.append(y)
            elif( x < self.__x_arr[0]):
                self.__x_arr.insert(0,x)
                self.__y_arr.insert(0,y)                
            elif(x == self.__x_arr[0] or x == self.__x_arr[len(self.__x_arr)-1]):
                #print "ERROR: (0) Function can not include the same x values.! Stop"
                return
            else:
                i_min = 0
                i_max = len(self.__x_arr)-1
                while( i_min != (i_max - 1)):
                    i_avg = int((i_min + i_max)/2)
                    if(i_avg == i_min):
                        break
                    if(x == self.__x_arr[i_min] or x == self.__x_arr[i_avg] or x == self.__x_arr[i_max]):
                        #print "ERROR: (1) Function can not include the same x values.! Stop"
                        return
                    x_min = self.__x_arr[i_min]
                    x_avg = self.__x_arr[i_avg]
                    x_max = self.__x_arr[i_max]
                    if( x > x_min and x < x_avg):
                        i_max = i_avg
                    else:
                        i_min = i_avg
                self.__x_arr.insert(i_max,x)
                self.__y_arr.insert(i_max,y)
            if(x > self.maxX) : self.maxX = x
            if(x < self.minX) : self.minX = x
            if(y > self.maxY) : self.maxY = y
            if(y < self.minY) : self.minY = y

    def getFirstY(self):
        return self.__y_arr[0]
    
    def getLastY(self):
        return self.__y_arr[len(self.__x_arr)-1]        

    def getY(self,x):
        """ Returns y value for x """
        i_max = len(self.__x_arr) - 1
        if(i_max < 0): return 0.
        if( x >= self.__x_arr[i_max]):
            return self.__y_arr[i_max]
        if(x <= self.__x_arr[0]):
            return self.__y_arr[0]
        i_0 = 0
        i_1 = i_max
        while( i_1 - i_0 != 1 ):
            i_avg = (i_1 + i_0)/2
            if(x >= self.__x_arr[i_0] and x < self.__x_arr[i_avg]):
                i_1 = i_avg
            else:
                i_0 = i_avg
        coeff = (x - self.__x_arr[i_0])/(self.__x_arr[i_1] - self.__x_arr[i_0])
        y = self.__y_arr[i_0] + coeff*(self.__y_arr[i_1] - self.__y_arr[i_0])
        return y

    def getXY(self,i):
        """ Returns the (x,y) of point with index i """
        return (self.__x_arr[i],self.__y_arr[i])
    
    def getX(self,i):
        """ Returns the (x) of point with index i """
        return self.__x_arr[i]

    def getValueY(self,i):
        """ Returns the (y) of point with index i """
        return self.__y_arr[i]

    def getMaxX(self):
        """ Returns the max value of x """
        return self.maxX
    
    def getMinX(self):
        """ Returns the min value of x  """
        return self.minX
    
    def getMaxY(self):
        """ Returns the max value of y """
        return self.maxY
    
    def getMinY(self):
        """ Returns the min value of y  """
        return self.minY
    
    def clear(self):
        """ Removes old coordinates """
        self.__x_arr  = []   
        self.__y_arr  = []

    def multiply(self,coeff):
        """ Multiply all y-values by constant coefficient """
        i = 0
        for y in self.__y_arr:
            self.__y_arr[i] *= coeff
            i += 1

    def copy(self,func):
        """ Copy all data from external Function """
        self.clear()
        n = func.getSize()
        for i in range(n):
            (x,y) = func.getXY(i)
            self.addXY(x,y)

    def dump(self,file_name):
        file_out = open(file_name,"w")
        i = 0
        for x in self.__x_arr:
            y = self.__y_arr[i]
            print >>file_out,'%-10.5g  %-12.7g  %-12.7g ' % (i,x,y)
            i += 1
        file_out.close()

    def init(self,file_name):
        self.clear()
        file_in = open(file_name,"r")
        while 1:
            str = file_in.readline()
            if len(str) == 0:
                break
            str.strip()
            res = str.split()
            if(len(res) == 2):
                self.addXY(float(res[0]),float(res[1]))
            elif(len(res) == 3):
                self.addXY(float(res[1]),float(res[2]))
        file_in.close()

#----------------------------------------------------------------------------------
class LongDensDistribution:
    """ Generates the phi value distributed according external function """

    def __init__(self, phi_max, file_name):
        """ Create instance """
        self.phi_max = phi_max
        distrFunc =  Function()
        distrFunc.init(file_name)
        f_prob_int = Function()
        N = 400
        step = 2*phi_max/(N-1)
        f_prob_int.addXY( -phi_max, 0.)
        for i in range(1,N):
            (x,y) = f_prob_int.getXY(i-1)
            coeff = (x + phi_max)/(2.0*phi_max)
            f0 = distrFunc.getY(distrFunc.getMinX()+coeff*(distrFunc.getMaxX()-distrFunc.getMinX()))
            x += step
            coeff = (x + phi_max)/(2.0*phi_max)
            f1 = distrFunc.getY(distrFunc.getMinX()+coeff*(distrFunc.getMaxX()-distrFunc.getMinX()))
            f_prob_int.addXY(x, y + (f0+f1)*step/2.0)
        y_max = f_prob_int.getLastY()
        f_prob_int.multiply(1.0/y_max)
        f_inv = Function()
        for i in range(0,f_prob_int.getSize()):
            (x,y) = f_prob_int.getXY(i)
            f_inv.addXY(y,x)
        self.f_inv = f_inv
    
    def getPhiMax(self):
         return self.phi_max

    def getPhiMin(self):
         return (-self.phi_max)

    def getPhi(self):
         r = random.random()
         return self.f_inv.getY(r)


class LongDistribution:
    """ Phase and Energy Deviation Distribution.
        Energy and voltage parameters in GeV """

    def __init__(self, longDensity, Tkin, gamma_trans, harmonics):
        """ Create instance """
        self.ldd = longDensity
        self.Tkin =  Tkin
        self.gamma_trans = gamma_trans
        self.harm = harmonics
        self.mass = 0.93826
        gamma = (Tkin + self.mass)/self.mass
        self.gamma = gamma
        beta = math.sqrt(1.0 - 1.0/(gamma*gamma))
        self.beta = beta
        eta = 1.0/(gamma_trans*gamma_trans) - 1.0/(gamma*gamma)
        self.eta = eta

    def getDeltaAndPhi(self):
        res = 0
        while(res == 0):        
            delta = 0.
            phi = self.ldd.getPhi()
            return (delta,phi)

    def getEta(self):
        """ Returns slippage factor"""
        return self.eta

    def getBeta(self):
        """ Returns beta parameter"""
        return self.beta

    def getGamma(self):
        """ Returns gamma parameter"""
        return self.gamma 
#===========================================================
#========START MAIN PROGRAM=================================
#===========================================================

if len(sys.argv) != 2 :
    print "The arguments should be :  Name_of_profile_file  "
    sys.exit(1)

#Number of macro-protons
nPoints = 500000

profile_file = sys.argv[1]
bunch_file =  "psr_bunch_10.dat"

#set seed to get identical result at the end
seed = 1573172
random.seed(seed)


#------------------------
#x-coord generator
#------------------------

tune_x = 6.4
length = 90.261*1000.0
beta_x = length/(2*math.pi*tune_x)
x_rms = 10.0                        #in [mm]
emit_x = x_rms*x_rms/beta_x          # [mm*rad]
x_cutoff = 40.0

print "=== x-coordinate: ======="
print " length [mm] = ",length
print " tune =", tune_x
print " beta [mm] = ",beta_x
print " coord. rms [mm] = ",x_rms
print " emittance [mm*mrad] = ", emit_x*1000.
print " coord. cutoff [mm] = ",x_cutoff


genX = TransverseCoordGen(beta_x, emit_x, x_cutoff)

#------------------------
#y-coord generator
#------------------------
tune_y = 6.3
length = 90.261*1000.0
beta_y = length/(2*math.pi*tune_y)
y_rms = 10.                          #in [mm]
emit_y = y_rms*y_rms/beta_y   # [mm*rad]
y_cutoff = 40.0

print "=== y-coordinate: ======="
print " length [mm] = ",length
print " tune =", tune_y
print " beta [mm] = ",beta_y
print " coord. rms [mm] = ",y_rms
print " emittance [mm*mrad] = ", emit_y*1000.
print " coord. cutoff [mm] = ",y_cutoff

genY = TransverseCoordGen(beta_y, emit_y, y_cutoff)



#------------------------
#long-coord generator
#------------------------
phi_max = 3.141592*0.8
ldd = LongDensDistribution( phi_max , profile_file )

Tkin = 0.797  # GeV
#gamma_trans = 3.03975
gamma_trans = 1000000.
harm = 1

ld = LongDistribution(ldd,Tkin,gamma_trans,harm)

print "=== longitudinal distribution: ======="
print " phi_max [rad] = ",phi_max
print " Tkin  =", Tkin
print " gamma_trans = ",gamma_trans
print " harmonics = ", harm
print " beta  = ",ld.getBeta()
print " gamma = ",ld.getGamma()
print " eta   = ",ld.getEta()

f_out = open(bunch_file,"w")

x_rms = 0.
y_rms = 0.
phi_rms = 0.

for i in range(1,nPoints+1):
    (x,xp) = genX.getCoords()
    (y,yp) = genY.getCoords()
    (delta,phi) = ld.getDeltaAndPhi()
    x_rms += x*x
    y_rms += y*y
    phi_rms += phi*phi
    #print >>f_out,' %-11.5g  %-11.5g %-11.5g  %-11.5g %-11.5g %-11.5g ' % (x,xp,y,yp,phi,delta)
    print >>f_out,' %-11.5g  %-11.5g %-11.5g  %-11.5g %-11.5g 0.0 ' % (x,xp,y,yp,phi)
    if(i % 100000 == 0):
        print "i=",i
        f_out.flush()

x_rms /= nPoints
x_rms = math.sqrt(x_rms)
print "x_rms=",x_rms

y_rms /= nPoints
y_rms = math.sqrt(y_rms)
print "y_rms=",y_rms

phi_rms /= nPoints
phi_rms = math.sqrt(phi_rms)
print "phi_rms=",phi_rms

f_out.close()




