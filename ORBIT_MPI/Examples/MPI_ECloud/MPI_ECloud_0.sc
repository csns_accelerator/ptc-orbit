//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_ECloud_0.sc
//
// AUTHOR
//    A. Shishlo
//
// CREATED
//    01/22/2004
//
//
// DESCRIPTION
//    Script file to check the ECloud propagate procedure.
//    You create the uniform lattice and add one EP_Node.
//    Than you propagate p-bunch through the lattice and
//    one EP_Node at a position where you placed it. 
//
///////////////////////////////////////////////////////////////////////////

  Integer nRank = MPI_rank();

  String runName, of1;
  runName = "ECloud_0";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);

  Real TSync = 1.;
  Real mSync = 1;
  Real charge = 1;
  addSyncPart(mSync,charge,TSync);

  nMaxMacroParticles = 500000;
  Integer chunkSize = 10000;
  nReals_Macro = 1.0e14/(nMaxMacroParticles);
  mainHerd = addMacroHerd(chunkSize);
  readParts(mainHerd, "psr_bunch_10.dat", nMaxMacroParticles);

  if(nRank == 0) cerr << "Done reading mainherd:\n";

/////////////////
// Make a Lattice
/////////////////

  if(nRank == 0) cerr << "Create lattice :\n"; 
  String LatType = "Ring";
  Real LengthTunes = 88.;
  Real XTune = 6.40;
  Real YTune = 6.30;
  Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 88;
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);
  if(nRank == 0) cerr << "Done Creating Lattice:\n"; 

////////////////////////////////////////
// Make a particle distributor,
// ECloud Calculator and EP_Node
////////////////////////////////////////

  if(nRank == 0) cerr << "Create EP_Node and EP-calculator.\n";

  Integer nSlices_total = 128;
  ECloudCreatePartsDistibutor(nSlices_total);

  Integer xBins = 32;
  Integer yBins = 32;
  Integer nZ_slices = 1;
  Real xSize = 100.;
  Real ySize = 100.;
  Integer BPPoints = 52;
  String  BPShape = "Circle";
  Integer BPModes = 15;
  Integer minPBunchSize = 1;

  //make the e-cloud calculator
  ECloudCreateCalculator(xBins,yBins,nZ_slices,xSize,ySize,BPPoints,BPShape,BPModes,minPBunchSize);

  //set integration parameters
  Integer nLongSteps = 1500;
  Integer pBunchFieldUpdatesSteps = 1;
  Integer nTrackerSteps = 20;
  ECloudSetNumberLongSteps(nLongSteps);
  ECloudSetNumberPFieldUpdates(pBunchFieldUpdatesSteps);
  ECloudSetNumberStepsInSlice(nTrackerSteps);

  //Using the simplectic tracker (default 0)
  Integer useSimplecticTracker = 0;
  ECloudUseSimplecticTracker(useSimplecticTracker);

  //Will the b-bunch kick from e-cloud apply 1-yes (default) 0-no
  Integer pBunchKickApply = 1;
  ECloudSetpBunchKickApply(pBunchKickApply);

  //Defines will it be there the debugging output (default 0 - no)
  Integer debugOutput = 1;
  ECloudSetTrackerDebugOutput(debugOutput);

  //Make EP_Node
  //The length - length of the e-cloud region
  Integer orderInd = 885;
  Real length = 1000.0; // mm
  Integer npNodeIndex0 = ECloudMakeEP_Node(orderInd,length);

  //set the surface and volume electron generation probabilities
  Real surfaceGenProb = 1.0;
  Real volumeGenProb  = 0.0;
  ECloudSetElectrGenProbab(npNodeIndex0,surfaceGenProb,volumeGenProb);

  //set the electron generation parameters
  Integer newMacroPerBunch = 50000;
  Real electronsPerProton = 100.0;
  Real lossRate = 4.0e-6;
  ECloudSetElectrGenParams(npNodeIndex0,newMacroPerBunch,electronsPerProton,lossRate);

  //add boundary conditions to the solver ( default 1 - yes)
  //this will add the boundary conditions to the EP_Node
  Integer addBoundaryPotential = 1;
  ECloudSetAddBoundaryConds(npNodeIndex0, addBoundaryPotential);

  if(nRank == 0) cerr << "Create EP_Node and EP-calculator - Done.\n";

  //dump the structure into the output file
  showNodes(fio);

  if(nRank == 0) cerr << "Start tracking.\n";

  doTurn(1);

  if(nRank == 0) cerr << "Stop tracking.\n";

  String nameOfHerd = "Bm_Parts_new_";
  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".out";
  OFstream fio1(nameOfHerd, ios::out);

  dumpPartsGlobal(mainHerd, fio1);

  fio1.close();

  if(nRank == 0) cerr << "Stop.\n";

  quit

