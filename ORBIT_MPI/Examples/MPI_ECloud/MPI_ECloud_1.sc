//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_ECloud_1.sc
//
// AUTHOR
//    A. Shishlo
//
// CREATED
//    01/22/2004
//
//
// DESCRIPTION
//    A script file to check the ECloud propagate procedure
//
///////////////////////////////////////////////////////////////////////////

  Integer nRank = MPI_rank();

  Real TSync  = 0.815;
  Real mSync  = 1.0;
  Real charge = 1.0;
  addSyncPart(mSync,charge,TSync);


  //the ring definition
  lRing = 88.5493;
  harmonicNumber = 1.0;

  nMaxMacroParticles = 500000;
  Integer chunkSize = 10000;
  nReals_Macro = 3.12e+13/(nMaxMacroParticles);
  mainHerd = addMacroHerd(chunkSize);
  readParts(mainHerd, "psr_bunch_10.dat", nMaxMacroParticles);

  if(nRank == 0) cerr << "Done reading mainherd:\n";

//-------------------------------------------------------
// create ECloud Calculator
//-------------------------------------------------------

  if(nRank == 0) cerr << "Create calculator.\n";

  Integer nSlices_total = 128;
  ECloudCreatePartsDistibutor(nSlices_total);

  Integer xBins = 32;
  Integer yBins = 32;
  Integer nZ_slices = 1;
  Real xSize = 100.;
  Real ySize = 100.;
  Integer BPPoints = 52;
  String  BPShape = "Circle";
  Integer BPModes = 15;
  Integer minPBunchSize = 1;

  //make the e-cloud calculator
  ECloudCreateCalculator(xBins,yBins,nZ_slices,xSize,ySize,BPPoints,BPShape,BPModes,minPBunchSize);

  //set integration parameters
  Integer nLongSteps = 1500;
  Integer pBunchFieldUpdatesSteps = 1;
  Integer nTrackerSteps = 20;
  ECloudSetNumberLongSteps(nLongSteps);
  ECloudSetNumberPFieldUpdates(pBunchFieldUpdatesSteps);
  ECloudSetNumberStepsInSlice(nTrackerSteps);

  //Using the simplectic tracker (default 0)
  Integer useSimplecticTracker = 0;
  ECloudUseSimplecticTracker(useSimplecticTracker);

  //Will the b-bunch kick from e-cloud apply 1-yes (default) 0-no
  Integer pBunchKickApply = 1;
  ECloudSetpBunchKickApply(pBunchKickApply);

  //Defines will it be there the debugging output (default 0 - no)
  Integer debugOutput = 1;
  ECloudSetTrackerDebugOutput(debugOutput);

  if(nRank == 0) cerr << "Create calculator - Done \n";

  if(nRank == 0) cerr << "Start calculation.\n";

  //The length of the e-cloud region
  Real length = 100.0; // mm

  //=================================================
  //This will propagate p-bunch with 
  //initially empty e-bunch.
  //In this way you can add diagnostic to the EP_Node
  //and/or additional fields.
  //=================================================
  Integer orderInd = 885;
  Integer npNodeIndex0 = ECloudMakeEP_Node(orderInd,length);

  String fileName1 = "ebunch_diag.dat";
  String fileName2 = "surf_diag.dat";
  Integer turn = 1;
  Real timeStart = 300.;
  Real timeStop  = 320.;
  ECloudAddEBunchDiagToEP_Node(npNodeIndex0,turn,timeStart,fileName1);
  ECloudAddSurfaceDiagToEP_Node(npNodeIndex0,turn,timeStart,timeStop,fileName2);

  //set surface
  ECloudSetSurfaceForEP_Node(npNodeIndex0,"TITAN");

  //set the surface and volume electron generation probabilities
  Real surfaceGenProb = 1.0;
  Real volumeGenProb  = 0.0;
  ECloudSetElectrGenProbab(npNodeIndex0,surfaceGenProb,volumeGenProb);

  //set the electron generation parameters
  Integer newMacroPerBunch = 50000;
  Real electronsPerProton = 100.0;
  Real lossRate = 4.0e-6;
  ECloudSetElectrGenParams(npNodeIndex0,newMacroPerBunch,electronsPerProton,lossRate);

  //add boundary conditions to the solver ( default 1 - yes)
  //this will add the boundary conditions to the EP_Node
  Integer addBoundaryPotential = 1;
  ECloudSetAddBoundaryConds(npNodeIndex0, addBoundaryPotential);

  //set external filed in Tesla
  Real hx = 0.;
  Real hy = 0.;
  Real hz = 0.;
  ECloudAddUniformMagnField(npNodeIndex0,-length/2.0,length/2.0,hx,hy,hz);


  //start the proton and electron density monitoring
  ECloudStartProfileRecordEP_Node(npNodeIndex0,"PBUNCH_DENSITY");
  ECloudStartProfileRecordEP_Node(npNodeIndex0,"EBUNCH_DENSITY");

  //propagate through the EP_Node
  ECloudPropagateThroughEP_Node(npNodeIndex0);


  if(nRank == 0) cerr << "Stop calculation.\n";

  //dump the proton and electron density function (as f(t)) 
  ECloudDumpProfileRecordEP_Node(npNodeIndex0,"PBUNCH_DENSITY","pBunchProfile.dat");
  ECloudDumpProfileRecordEP_Node(npNodeIndex0,"EBUNCH_DENSITY","eBunchProfile.dat");

  //start the proton and electron density monitoring
  ECloudStopProfileRecordEP_Node(npNodeIndex0,"PBUNCH_DENSITY");
  ECloudStopProfileRecordEP_Node(npNodeIndex0,"EBUNCH_DENSITY");

  if(nRank == 0) cerr << "Start p-bunch dumping.\n";

  String nameOfHerd = "Bm_Parts_new_";
  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".out";
  OFstream fio1(nameOfHerd, ios::out);

  dumpPartsGlobal(mainHerd, fio1);
  fio1.close();

  if(nRank == 0) cerr << "Stop.\n";

  quit

