//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_ECloud_2.sc
//
// AUTHOR
//    A. Shishlo
//
// CREATED
//    01/22/2004
//
//
// DESCRIPTION
//    A script file to study one p-bunch passing
//    trough the e-cloud region.
//    (or to study resonance - scan over modulation frequency)
//
///////////////////////////////////////////////////////////////////////////

  Integer nRank = MPI_rank();

  Real TSync  = 0.815;
  Real mSync  = 1.0;
  Real charge = 1.0;
  addSyncPart(mSync,charge,TSync);

  lRing = 88.5493;
  harmonicNumber = 1.0;

  nMaxMacroParticles = 500000;
  Integer chunkSize = 10000;
  nReals_Macro = 3.12e+13/(nMaxMacroParticles);
  mainHerd = addMacroHerd(50000);
  readParts(mainHerd, "psr_bunch_10.dat", nMaxMacroParticles);

  if(nRank == 0) cerr << "Done reading mainherd:\n";

//-------------------------------------------------------
// create ECloud Calculator
//-------------------------------------------------------

  if(nRank == 0) cerr << "Create calculator.\n";

  Integer nSlices_total = 128;
  ECloudCreatePartsDistibutor(nSlices_total);

  Integer xBins = 32;
  Integer yBins = 32;
  Integer nZ_slices = 1;
  Real xSize = 100.;
  Real ySize = 100.;
  Integer BPPoints = 52;
  String  BPShape = "Circle";
  Integer BPModes = 15;
  Integer minPBunchSize = 1;

  //make the e-cloud calculator
  ECloudCreateCalculator(xBins,yBins,nZ_slices,xSize,ySize,BPPoints,BPShape,BPModes,minPBunchSize);

  //set integration parameters
  Integer nLongSteps = 1500;
  Integer pBunchFieldUpdatesSteps = 1;
  Integer nTrackerSteps = 20;
  ECloudSetNumberLongSteps(nLongSteps);
  ECloudSetNumberPFieldUpdates(pBunchFieldUpdatesSteps);
  ECloudSetNumberStepsInSlice(nTrackerSteps);

  //Using the simplectic tracker (default 0)
  Integer useSimplecticTracker = 0;
  ECloudUseSimplecticTracker(useSimplecticTracker);

  //Will the b-bunch kick from e-cloud apply 1-yes (default) 0-no
  Integer pBunchKickApply = 1;
  ECloudSetpBunchKickApply(pBunchKickApply);

  //Defines will it be there the debugging output (default 0 - no)
  Integer debugOutput = 1;
  ECloudSetTrackerDebugOutput(debugOutput);

  //add boundary conditions to the solver ( default 1 - yes)
  //this will add the boundary conditions to the tracker
  Integer addBoundaryPotential = 1;
  ECloudSetAddBoundaryCondsForCalc(addBoundaryPotential);

  if(nRank == 0) cerr << "Create calculator - Done \n";

  if(nRank == 0) cerr << "Start calculation.\n";

  //The length of the e-cloud region
  Real length = 100.0; // mm

  //=================================================
  // You just propagate through the Calculator
  //=================================================
  //ECloudDebugScanModulation(length);
  ECloudDebugStartCalculator(length);

  if(nRank == 0) cerr << "Stop calculation.\n";

  if(nRank == 0) cerr << "Stop.\n";

  quit

