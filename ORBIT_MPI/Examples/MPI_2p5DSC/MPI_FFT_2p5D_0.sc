//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_FFT_2p5D_0.sc
//
// DESCRIPTION
//    Script file for a uniform focusing lattice and
//    2.5D space charge and elliptical beam pipe.
//
///////////////////////////////////////////////////////////////////////////

  //MPI stuff - get rank of the CPU
  Integer nRank=MPI_rank();

  String runName, of1;
  runName = "FFT_2p5D";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);

  Real TSync = 1.;
  Real mSync = 1;
  Real charge = 1;
  addSyncPart(mSync,charge,TSync);

  nMaxMacroParticles = 1000;
  Real BeamLength = 1.00;
  nReals_Macro = 1.0e14/(nMaxMacroParticles*BeamLength);
  mainHerd = addMacroHerd(nMaxMacroParticles);
  readParts(mainHerd, "Bm_Parts_ini_0", nMaxMacroParticles);

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done reading mainherd:\n";
  }

/////////////////
// Make a Lattice
/////////////////
 
  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Create lattice :\n"; 
  }

  String LatType = "Ring";
  Real LengthTunes = 10.;
  Real XTune = 6.40;
  Real YTune = 6.30;
  Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 10;
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done Creating Lattice:\n"; 
  }

///////////////////////////////////////////
// Add a Transverse Space Charge Node Set
///////////////////////////////////////////
  
  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Start Creating 2.5 SC node:\n";
  }

  Integer nxBins = 128, nyBins = 128;
  Real eps = 1.e-6;   // smoothing parameter
  Real BP1 = 50.0;
  Real BP2 = 70.0;
  Real BP3 = -50.0;
  Real BP4 =  50.0;
  Integer bPoints = 256;
  Integer bModes = 21;
  Real gFact = 2.0;
  Integer nMacroTSCMin = 50;
  //The parameters BP1,BP2,BP3,BP4 have different meanings for different
  // shapes:
  // shape = "Ellipse" BP1 is 1/2 horizontal size, BP2 - 1/2 vertical size
  // shape = "Circle" BP1 - radius
  // shape = "Rectangle" BP1 - minX, BP2 - maxX, BP3 - minY, BP4 - maxY
  // Shape could be "Ellipse","Rectangle","Circle","None"
  // If Shape == "None" boundary conditions will not be used
  // Sizes in [mm]
  addPotentialTransSCSet(nxBins, nyBins, eps, "Ellipse",
                         BP1,BP2,BP3, BP4,
                         bPoints, bModes, gFact,
                         nMacroTSCMin);

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done Creating SC node:\n";
  }
  

//////////////////////////////
// Start Output:
//////////////////////////////

  showStart(fio);

//////////////////////////////
// do some turns, and dump particles for later use:
//////////////////////////////

  Integer nTurns = 2;

  //MPI stuff
  if( nRank == 0 ) {
   cerr << "Start Tracking\n";
  }

   Real time_start = MPI_Wtime0();

  //Integer index_Nodes = 2;
  //turnToNode(mainHerd, index_Nodes);
  doTurn(nTurns);  

   Real time_stop = MPI_Wtime0() - time_start;   

  if( nRank == 0 ) {
    cerr << "======== STOP Calculation  =========\n";
    cerr << "N turns ="<< nTurns << " Time sec="<< time_stop<< " \n";
  }

//-------------------------------------------------------

  String nameOfHerd = "Bm_Parts_final_";

  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".dat";
  OFstream fio1(nameOfHerd, ios::out);

  dumpParts(mainHerd, fio1);
  fio1.close();

  quit

