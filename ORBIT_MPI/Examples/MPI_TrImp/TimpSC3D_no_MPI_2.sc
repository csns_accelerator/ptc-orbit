//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    TimpSC3D_no_MPI_1.sc
//
// AUTHOR
//    J. Holmes (jzh@ornl.gov), S. Danilov (vux@ornl.gov) 
//
// CREATED
//    07/26/01
//
//
// DESCRIPTION
//    Script file for "analytic" transverse impedance case with 
//                    3D space charge
//
////////////////////////////////////////////////////////////////////
// DESCRIPTION
//    Halo development for Lorentz dist in uniform focusing lattice.
//    This case is nu_x = 1.1, nu_y = 1.05 and uniform displacement
//         dy = 1 mm.
//
////////////////////////////////////////////////////////////////////


///////////////////////////////
// Make a Synchronous Particle:
///////////////////////////////

  Real TSync = 1.0;    // Kinetic Energy (GeV)
  Real mSync = 1.0;      // Mass (AMU)
  Real charge = 1.0;     // charge number

  addSyncPart(mSync,charge,TSync);

//////////////////////////////
// Add a Particle Distribution
//////////////////////////////

  nMaxMacroParticles = 100;
  Real BeamLength = 1.00;
  nReals_Macro = 1.0e13/(nMaxMacroParticles*BeamLength);
  mainHerd = addMacroHerd(nMaxMacroParticles);
  readParts(mainHerd, "Bm_Parts_ini.dat", nMaxMacroParticles);

  cerr << "Done reading mainherd:\n";

  Integer nMacrosMin = 1;

 
/////////////////
// Make a Lattice
/////////////////

  String LatType = "Linac";
  Real LengthTunes = 40.0;
  Real XTune = 1.10;
  Real YTune = 1.05;
  Real RhoInv = 0.;
  //Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 40;
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);

  cerr << "The Lattice has been created:\n"; 



//////////
// Output:
//////////

  String runName, of1;
  runName = "TimpSC3D_no_MPI";
  of1 = runName + ".prt";
  OFstream fio(of1, ios::out);

  Integer ilist;

/////////////////////////////////////
// Check for the Transverse Kick Here
/////////////////////////////////////

  Ring::nuX=XTune;
  Ring::nuY=YTune;

  include "TCheckImpedance.sc"

  nLBinsTImp = 512;
  ComplexVector ZTImpedplus(nLBinsTImp/2), ZTImpedminus(nLBinsTImp/2);
  calcTCheckImped();

  Real b_a = 2.;
  Integer useAvg = 1;
  useYdimension = 1;

//////////////////////////////
//   Transverse Impedance Here
//////////////////////////////

   addFFTTImpedance("TImp1", 124, ZTImpedplus, ZTImpedminus, 
                    ZTImpedplus, ZTImpedminus, b_a, useAvg, 
                    nMacrosMin );

// boundaries for the grid

  Integer nxBins = 64, nyBins = 64, nphiBins=64;
  Real eps = 0.001;
  String BPShape = "Circle";
  Real BP1 = 55., BP2 = 0., BP3 = 0., BP4 = 0.;
  Integer BPPoints = 64, BPModes = 20;
  Real Gridfact = 2.0;


  addSpCh3D_FFTSet(nxBins, nyBins, nphiBins, eps,
                   BPShape, BP1, BP2, BP3, BP4,
                   BPPoints, BPModes, Gridfact, nMacrosMin);

  cerr << "Done Creating SC node:\n";



///////////////////////////////
// Add Moment and StatLat Nodes
///////////////////////////////

  addMomentNode("MomentNode(0)", 4, 4, "Moments");       
  addStatLatNode("StatLatNode(0)", 5, "StatLats");       
  activateMomentNode(1);
  activateStatLatNode(1);


// Set up to histogram the distributions:

    Integer i, nBins = 60;
    RealVector xHist(nBins), yHist(nBins);
    Real yGMin, dy;
    yGMin = -60.; dy = 120.0/Real(nBins);
// Emittances Output:
    nEmitBins = 60;   deltaEmitBin = 5.0;  // set up emittance bins


////////////////
// Start Output:
////////////////

  showStart(fio);


/////////////////////////////////////////////////////
// Do Some Turns, and Dump Particles for Later Plots:
/////////////////////////////////////////////////////

  cerr << "Start Tracking\n";

  Real et;
  Integer maxharm, nTurn;
  maxharm=5;

  timerOn();

//==================START=================================================

  cerr << "======== Start My Calculation  =========\n";

  Integer nTurns = 10;

  OFstream  fisHB("Herd_3Dtest_before.dat", ios::out);
  dumpParts(mainHerd, fisHB);

  Integer iTurns;

  for( iTurns = 1; iTurns <= nTurns; iTurns++){
    cout << "Turns = " <<  iTurns << "\n";
    doTurn(1);
  }

  OFstream  fisHA("Herd_3Dtest_after.dat", ios::out);
  dumpParts(mainHerd, fisHA);

  quit
