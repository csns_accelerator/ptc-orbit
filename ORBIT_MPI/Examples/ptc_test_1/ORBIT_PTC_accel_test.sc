///////////////////////////////////////////////////////////////////////
// ORBIT_PTC acceleration test srcript 
///////////////////////////////////////////////////////////////////////

//initialize PTC
InitPTC("MAIN_1_5_KIND7_1");


mainHerd = addMacroHerd(1000);

nMacrosPerTurn = 0;
nMaxMacroParticles = 100;
nReals_Macro = 3.0e11/Real(nMaxMacroParticles);
readParts(mainHerd, "bunch.dat", nMaxMacroParticles);


readAccelerationTableForPTC("ACCWAVE_40kV_280kV_350ms.DAT");


//////////////////////////////
// Start Output:
//////////////////////////////
  OFstream fio("statistics.out", ios::out);
  showStart(fio);
  fio.close();

  Integer nTurns = 100;
  Integer iTurns = 1;
  SynchronousSetPTC(-1);

  cerr << "Start tracking.\n";
  for( iTurns = 1; iTurns <= nTurns; iTurns++){
    cerr << "turn=" <<iTurns <<"\n" ;

    UpdateTwissFromPTC();

    doTurn(1);

   }

  cerr << "Stop tracking.\n";

  OFstream fio1("bunch_final.dat", ios::out);
  dumpPartsGlobal(mainHerd, fio1);
  fio1.close();

quit
