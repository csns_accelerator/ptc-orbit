//define the MPI related variables 
Integer n_CPU;
Integer n_Rank;

n_CPU  = MPI_size();
n_Rank = MPI_rank();

//////////////////////////////
// Make a synchronous particle:
//////////////////////////////

  Real TSync = 0.600;    // Kinetic Energy (GeV)
  Real mSync = 1;        // Mass (AMU)
  Real charge = 1;       // charge number

  addSyncPart(mSync, charge, TSync);

  mainHerd = addMacroHerd(5000);

  Integer nParts = 10;

  Integer i;
  Real x,xp,y,yp,dE,phi;
  Real r;

  for(i=0;i< nParts*n_Rank;i++){
   r=rand_MPI();
  }

  for(i=0;i< nParts;i++){
   x  =1.0 + 0.1*(n_Rank+1);
   xp =2.0 + 0.1*(n_Rank+1);
   y  =3.0 + 0.1*(n_Rank+1);
   yp =4.0 + 0.1*(n_Rank+1);
   dE =5.0 + 0.1*(n_Rank+1);
   phi=1.0 + 0.1*rand_MPI();
   addMacroPart2(x,xp,y,yp,dE,phi);
  }

  Integer nRank=MPI_rank();

  showHerdGlobal(mainHerd,cout);

  String nameOfHerd = "Bm_Parts_0003_";
  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".dat";
  OFstream fio1(nameOfHerd, ios::out);

  showHerdGlobal(mainHerd,fio1);

  fio1.close();


  if( nRank == 0) {
   cerr<<"debug Distribution start.\n"; 
  }

//-------------------------------------------------------
  Integer nCountLM = 10;
  Integer nSlices_total = 30;

  createPartDistr(nCountLM,nSlices_total);

  distributeParticle();
//-------------------------------------------------------
  showHerdGlobal(mainHerd,cout);


  nameOfHerd ="Bm_Parts_0003_new_";
  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".dat";
  OFstream fio2(nameOfHerd, ios::out);

  showHerdGlobal(mainHerd,fio2);
  fio2.close();

  if( nRank == 0) {
   cerr<<"debug Distribution finished .\n"; 
  }

  quit

