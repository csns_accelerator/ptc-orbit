//////////////////////////////
// Make a synchronous particle:
//////////////////////////////

  Real TSync = 0.600;    // Kinetic Energy (GeV)
  Real mSync = 1;        // Mass (AMU)
  Real charge = 1;       // charge number

  addSyncPart(mSync, charge, TSync);

  mainHerd = addMacroHerd(1000);

  Integer nParts = 183;

  cerr << "Start reading.\n";

  readParts(mainHerd,"Bm_Parts_ini_0",nParts);

  cerr << "Stop reading.\n";

  Integer nRank=MPI_rank();

  String nameOfHerd = "Bm_Parts_0004_";
  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".dat";
  OFstream fio1(nameOfHerd, ios::out);
  dumpPartsGlobal(mainHerd, fio1);
  fio1.close();

//-------------------------------------------------------
  Integer nCountLM = 10;
  Integer nSlices_total = 5;

  createPartDistr(nCountLM,nSlices_total);

  distributeParticle();
//-------------------------------------------------------
  nameOfHerd ="Bm_Parts_0004_new_";
  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".dat";
  OFstream fio2(nameOfHerd, ios::out);
  dumpPartsGlobal(mainHerd, fio2);
  fio2.close();


  quit

