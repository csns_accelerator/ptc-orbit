//define the MPI related variables 
Integer n_CPU;
Integer n_Rank;

n_CPU  = MPI_size();
n_Rank = MPI_rank();

//////////////////////////////
// Make a synchronous particle:
//////////////////////////////

  Real TSync = 0.600;    // Kinetic Energy (GeV)
  Real mSync = 1;        // Mass (AMU)
  Real charge = 1;       // charge number

  addSyncPart(mSync, charge, TSync);

  mainHerd = addMacroHerd(5000);

  Integer nParts = 5000;

  Integer i;
  Real x,xp,y,yp,dE,phi;
  Real r;

  for(i=0;i< nParts*n_Rank;i++){
   r=rand_MPI();
  }

  for(i=0;i< nParts;i++){
   x  =1.0 + 0.1*(n_Rank+1);
   xp =2.0 + 0.1*(n_Rank+1);
   y  =3.0 + 0.1*(n_Rank+1);
   yp =4.0 + 0.1*(n_Rank+1);
   dE =5.0 + 0.1*(n_Rank+1);
   phi=1.0 + 0.1*rand_MPI();
   addMacroPart2(x,xp,y,yp,dE,phi);
  }
  
  String nameOfHerd = "Bm_Parts_0002_";
  Integer nRank=MPI_rank();

  MPI_String_Add_Integer(nameOfHerd,nRank);
  OFstream fio1(nameOfHerd, ios::out);

  dumpParts(mainHerd, fio1);
  fio1.close();

//-------------------------------------------------------
  Integer nCountLM = 10;
  Integer nSlices_total = 30;

  createPartDistr(nCountLM,nSlices_total);

  distributeParticle();
//-------------------------------------------------------

  //cerr<<"Runk="<< nRank <<" finished particle distributing.\n"; 

  nameOfHerd = "Bm_Parts_0002_new_";

  MPI_String_Add_Integer(nameOfHerd,nRank);
  OFstream fio1(nameOfHerd, ios::out);

  dumpParts(mainHerd, fio1);
  fio1.close();


  quit

