//////////////////////////////
// Make a synchronous particle:
//////////////////////////////

  Real TSync = 0.600;    // Kinetic Energy (GeV)
  Real mSync = 1;        // Mass (AMU)
  Real charge = 1;       // charge number

  addSyncPart(mSync, charge, TSync);

  mainHerd = addMacroHerd(1000);

  Integer nParts = 10;

  cerr << "Start reading.\n";

  readParts(mainHerd,"Bm_Parts_ini_0",nParts);

  cerr << "Stop reading.\n";
  

  String nameOfHerd = "Bm_Parts_0001_";
  Integer nRank=MPI_rank();

  MPI_String_Add_Integer(nameOfHerd,nRank);
  OFstream fio1(nameOfHerd, ios::out);

  dumpParts(mainHerd, fio1);
  fio1.close();

//-------------------------------------------------------
  Integer nCountLM = 10;
  Integer nSlices_total = 5;

  createPartDistr(nCountLM,nSlices_total);

  distributeParticle();
//-------------------------------------------------------

  nameOfHerd = "Bm_Parts_0001_new_";

  MPI_String_Add_Integer(nameOfHerd,nRank);
  OFstream fio1(nameOfHerd, ios::out);

  dumpParts(mainHerd, fio1);
  plotHorizontal(mainHerd);
  fio1.close();


//  quit

