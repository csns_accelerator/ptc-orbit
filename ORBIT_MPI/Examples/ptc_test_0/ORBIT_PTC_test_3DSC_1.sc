//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//   ORBIT_PTC_test_3DSC_1.sc
//
// DESCRIPTION
//    Script file for a PTC lattice and 3DSC calculation
//    3D space charge and circular beam pipe.
//
///////////////////////////////////////////////////////////////////////////

  //MPI stuff
  Integer nRank=MPI_rank();

  String runName, of1;
  runName = "ptc_3DSC";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);
 
 //initialize PTC
  InitPTC("ptc_data_test_0.txt");

  nMaxMacroParticles = 1000;
  nReals_Macro = 1.00e14/(nMaxMacroParticles);
  mainHerd = addMacroHerd(nMaxMacroParticles);
  readParts(mainHerd, "Bm_Parts_ini_0", nMaxMacroParticles);

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done reading mainherd:\n";
  }

//-------------------------------------------------------
// boundaries and 3DSC integrator
//-------------------------------------------------------
  Integer nMacrosMin = 1;
  Integer nxBins = 64, nyBins = 64, nphiBins=24;
  Real eps = 0.001;
  String BPShape = "Circle";
  Real BP1 = 110., BP2 = 110.;
  Integer BPPoints = 64, BPModes = 20;
  Real Gridfact = 2.0; 

  addSpCh3D_FFTSet_MPI(nxBins, nyBins, nphiBins, eps,
                       BPShape, BP1, BP2,
                       BPPoints, BPModes, Gridfact, nMacrosMin);


  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done Creating SC node:\n";
  }


//////////////////////////////
// Start Output:
//////////////////////////////

  showStart(fio);

//////////////////////////////
// do some turns, and dump particles for later plots:
//////////////////////////////


  //MPI stuff
  if( nRank == 0 ) {
   cerr << "Start Tracking\n";
  }



   //Integer index_Nodes = 2;
   //turnToNode(mainHerd, index_Nodes);
   doTurn(10);  

  if( nRank == 0 ) {
    cerr << "======== STOP Calculation  =========\n";
  }

//-------------------------------------------------------

  String nameOfHerd = "Bm_Parts_000_final_";

  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".dat";
  if( nRank == 0 ) {
    nameOfHerd = "Bm_Parts_final.dat";
  }
  OFstream fio1(nameOfHerd, ios::out);

  dumpPartsGlobal(mainHerd, fio1);
  fio1.close();

  quit

