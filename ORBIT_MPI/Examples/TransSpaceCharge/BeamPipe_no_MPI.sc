//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    SYNCH_8GeV.sc
//
// AUTHOR J. Holmes (jzh@ornl.gov)
//
// CREATED
//    02/15/2002
//
// MODIFIED
//
// DESCRIPTION
//    FNAL Proton Driver Synchrotron
//
///////////////////////////////////////////////////////////////////////////

// Files for output:

  String of1;
  runName = "SYNCH_8GeV";
  of1 = runName + ".out";

  OFstream fio(of1, ios::out);

//////////////////////////////
// Make a synchronous particle:
//////////////////////////////

  Real TSync = 0.600;    // Kinetic Energy (GeV)
  Real mSync = 1;        // Mass (AMU)
  Real charge = 1;       // charge number

  addSyncPart(mSync, charge, TSync);

  mainHerd = addMacroHerd(1000);

//////////////////////////////
// Make  a Ring 
//////////////////////////////

  readMADFile("Inj_RaceTrack_01.tw", "Inj_RaceTrack_01.out");
  Ring::lRing = 474.000120;
  Ring::gammaTrans = 13.975418;
  Ring::harmonicNumber = 84;  // 84 Harmonics

////////////////////////////////////////////
// Make Linac X & Y Distribution functions
////////////////////////////////////////////

  betaXInj = 14.610; alphaXInj = 1.561;
  MXJoho = 100.;
  epsXLimInj = 1.57 * (MXJoho+1)/2.; // 95% Normalized emit 3 pi
  x0Inj = 0.0; xP0Inj = 0.0;
//  x0Inj = spot at nobump; xP0Inj = spot at nobump;
  xTailFraction = 0.0; xTailFactor = 3.;
  addXInitializer("Bi-Gaussian-X", JohoXDist);

  betaYInj = 6.704; alphaYInj = -0.101;
  MYJoho = 100.;
  epsYLimInj = 1.57 * (MYJoho+1)/2.; // 95% Normalized emit 3 pi
  y0Inj = 0.0; yP0Inj = 0.0;
//  y0Inj = spot at nobump; yP0Inj = spot at nobump;
  yTailFraction = 0.0; yTailFactor = 3.;
  addYInitializer("Bi-Gaussian-Y", JohoYDist);

  phiMinInj = -180.0;
  phiMaxInj =  180.0;
  EOffset = 0.0;
  deltaEFracInj = 8.333333e-04;
  addLongInitializer("Uniform",UniformLongDist);

//////////////////////////////
// Add ideal bump nodes
//////////////////////////////

  Real xBamp = 9.10, xpBamp = 0.0, yBamp = 0.0, ypBamp = 1.68;
  Real BumpTime = 0.08978589;
  Real TimeFactor, TimeFactorc, tfract, tfractc;
  Void FNALBump()
  {
    tfract = time / BumpTime;
    tfractc = 1.0 - tfract;
    TimeFactor = pow(2.0 * tfract - tfract * tfract , 0.5);
    TimeFactorc = pow(2.0 * tfractc - tfractc * tfractc , 0.5);
    xIdealBump = xBamp * TimeFactor;
    xPIdealBump = 0.0;
    yIdealBump = 0.0;
    yPIdealBump = ypBamp * TimeFactorc;
  }

  addIdealBump("DownBump", 1, 1, FNALBump);
  addIdealBump("UpBump", 3, 0, FNALBump);

//////////////////////////////
// Add a "3-sided" foil node
//////////////////////////////

  addFoil("Foil",  2, -100.00, 100.00, -100.00, 100.00, 300.);
//  useFoilScattering = 1;
  useFoilScattering = 0;

// Inject nMacrosPerTurn particles per turn, up to 45 turns

  nMacrosPerTurn = 100;
  nMaxMacroParticles = 45 * nMacrosPerTurn;
  nReals_Macro = 3.00e11/Real(nMaxMacroParticles);

//////////////////////////////
// Add an RF Cavity
//////////////////////////////

  Real tFactor;
  Integer nRFHarms = 1;
  RealVector volts(nRFHarms), harmNum(nRFHarms), RFPhase(nRFHarms);
  harmNum(1) = 1;  // Remember Ring::harmonicNumber
  RFPhase(1) = 0.;
  Void PDIIVolts()
  {
   tFactor = time/0.08978589;
   if(tFactor > 1.) tFactor = 1.;
   volts(1) = 0.0 + 0.0 * tFactor;
  }
  addRampedRFCavity("RF 1", 1665, nRFHarms, volts, harmNum, RFPhase, PDIIVolts);
  Accelerate::rhoBend = 19.86254;

//////////////////////////////
// Add a Longitudinal Space Charge Node
//////////////////////////////
//
//  ComplexVector ZImped(64);
//  ZImped = Complex(0.,0.);
//
//  nLongBins = 64;
//  Real b_a = 4.;
//  Integer useAvg = 1;
//  Integer nMacroLSCMin = 128;
//  Integer useSpaceCharge = 1;
//  addFFTLSpaceCharge("LSC1", 20, ZImped, b_a, useAvg,
//                     nMacroLSCMin, useSpaceCharge);
//
///////////////////////////////////////////
// Add a Transverse Space Charge Node Set
///////////////////////////////////////////

  Integer nxBins = 128, nyBins = 128;
  Real eps = 1.e-6;   // smoothing parameter
  Real rxMin = -76.2;
  Real rxMax = 76.2;
  Real ryMin = -50.8;
  Real ryMax = 50.8;
  Integer bPoints = 256;
  Integer bModes = 21;
  Real gFact = 2.0;
  Integer nMacroTSCMin = 50;
  addPotentialTransSCSet(nxBins, nyBins, eps, "Rectangle",
                         rxMin, rxMax, ryMin, ryMax,
                         bPoints, bModes, gFact,
                         nMacroTSCMin);


///////////////////////////////////////////
// Add Moment and StatLat Nodes
///////////////////////////////////////////

  addMomentNode("MomentNode(0)", 7, 4, "Moments");       
  addStatLatNode("StatLatNode(0)", 8, "StatLats");       
  activateMomentNode(1);
  activateStatLatNode(1);

// Set up to histogram the distributions:

  Integer i, nBins = 120;
  RealVector xHist(nBins), yHist(nBins);

  Real xGMin, dx, yGMin, dy, phiGMin, dphi;
  xGMin = -60.0; dx = 120.0/Real(nBins);
  yGMin = -60.0; dy = 120.0/Real(nBins);
  phiGMin = -3.1415926539 / harmNum(1); dphi = -2.*phiGMin/Real(nBins);

// Emittance Output:

  nEmitBins = 100;   deltaEmitBin = 0.5;  // set up emittance bins

//////////////////////////////
// Start Output:
//////////////////////////////

  showStart(fio);

//////////////////////////////
// do some turns, and dump particles for later plots:
//////////////////////////////
  cerr << "Start tracking.\n";

  Integer n_Turns = 5;

  timerOn();
  doTurn(1);
  startTuneCalc();
  doTurn(1);
  stopTuneCalc();

  doTurn(1);
  doTurn(1);
  doTurn(1);

  cerr << "Turn number "<< n_Turns <<" done.\n";

  String nameOfHerd = "Bm_Parts_0005";
  OFstream fio6(nameOfHerd, ios::out);
  dumpParts(mainHerd, fio6);
  fio6.close();

  quit

