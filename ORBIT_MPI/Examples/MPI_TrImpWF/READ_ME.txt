There are two examples of using Transverse imedance elements
that are based on Wake Functions. 

=====================
For developers only.
=====================

1. TimpWF_MPI_1.sc
///////////////////////////////////////////////////////////
// A Herd with the step-function shape is propagated through
// one Transverse Impedance resonant element.
///////////////////////////////////////////////////////////

2. TimpWF_MPI_2.sc
///////////////////////////////////////////////////////////
// A Herd with the step-function shape is propagated through
// one Transverse Impedance resonant element after 4 previous
// herds with the same distribution.
///////////////////////////////////////////////////////////

