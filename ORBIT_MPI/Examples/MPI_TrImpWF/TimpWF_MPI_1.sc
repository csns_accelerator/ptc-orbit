///////////////////////////////////////////////////////////
// A Herd with the step-function shape is propagated through
// one Transverse Impedance resonant element.
///////////////////////////////////////////////////////////

cerr <<"Start \n";

String sRank;
Integer nRank=MPI_rank();
Integer nSize = MPI_size();
MPI_String_Add_Integer(sRank,nRank);

///////////////////////////////
// Make a Synchronous Particle:
///////////////////////////////

  Real TSync = 1.0;      // Kinetic Energy (GeV)
  Real mSync = 1.0;      // Mass (AMU)
  Real charge = 1.0;     // charge number

  Real beta = 0.875026; //this value is true for TSync = 1.0 and mSync = 1.0

  addSyncPart(mSync,charge,TSync);

  Real c_light = 2.99792458e+8; 

  nMaxMacroParticles = 1000;
  nReals_Macro = 1.0e13/(nMaxMacroParticles);
  mainHerd = addMacroHerd(nMaxMacroParticles);

/////////////////
// Make a Lattice
/////////////////

  String LatType = "Ring";
  Real LengthTunes = 100.0;
  Real XTune = 1.10;
  Real YTune = 1.05;
  Real RhoInv = 0.;
  Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 10;
cerr <<"Start \n";
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);
cerr <<"Start \n";
  Ring::nuX=XTune;
  Ring::nuY=YTune;

  if(nRank == 0){
    cerr << "The Lattice has been created:\n"; 
  }


//////////////////////////////
// Add a Particle Distribution
//////////////////////////////

  Real x,xp,y,yp,phi,dE;
  Real PI = 3.1415926;
  Real phi_min = -0.5*PI;
  Real phi_max =  0.5*PI;
  Integer n_step = nMaxMacroParticles;
  Real phi_step = (phi_max - phi_min)/(n_step -1);
  Integer i;

  Integer i_start,i_stop;
  Integer i_remind;
  i_remind = nMaxMacroParticles % nSize;
  Integer nMaxMacroParticlesLocal = nMaxMacroParticles/nSize;

  if( nRank < i_remind ){
    nMaxMacroParticlesLocal = nMaxMacroParticlesLocal + 1;
    i_start = nRank*nMaxMacroParticlesLocal + 1;
  }
  else{
    i_start = nRank*nMaxMacroParticlesLocal + i_remind + 1;
  }
  i_stop = i_start + nMaxMacroParticlesLocal -1;

  for( i = i_start; i <= i_stop; i++){
   x = 1000.0;
   y = 1000.0;
   xp = 0.;
   yp = 0.;
   dE = 0.;
   phi = phi_min + (i-1)*phi_step;
   addMacroPart2(x,xp,y,yp,dE,phi);
  }

  if(nRank == 0){
   cerr << "The mainherd has been created:\n";
  }
//////////
// Output:
//////////

  String runName, of1;
  runName = "TimpWF_MPI";
  of1 = runName + ".prt";
  OFstream fio(of1, ios::out);

//////////////////////////////
//   Transverse Impedance Here
//////////////////////////////

Integer orderTImpWF = 0;
Integer nBinsTImpWF = 100;
String  axisTImpWF  = "x";

Integer indexTImpWF;
indexTImpWF = addTImpedanceWF(orderTImpWF,nBinsTImpWF);

Real R_timp =10000000.0;
Real Q_timp =5.0;
Real F_timp = 8.0*1000000;
addResElemToTImpedanceWF(indexTImpWF, axisTImpWF, R_timp, Q_timp, F_timp);

Real RevFreq = beta*c_light/Ring::lRing;
Real T_timpWF = 1.0/RevFreq;

if(nRank == 0){
 cerr<<"Resonant frequency [Hz] ="<< F_timp <<"\n";
 cerr<<"R ="<< R_timp <<" [Ohm/(m*m)]\n";
 cerr<<"Q ="<< Q_timp <<" \n";
 cerr<<"===============================\n";

 cerr<<"t_min="<<(phi_min/PI)*(-T_timpWF/2.0)<<"\n";
 cerr<<"t_min="<<(phi_min/PI)*(T_timpWF/2.0)<<"\n";

 cerr<<"Ring length [m] ="<<Ring::lRing<<"\n";
 cerr<<"Revolution frequency [Hz] ="<< RevFreq <<"\n";
 cerr<<"Revolution period [sec] ="<< T_timpWF <<"\n";
 cerr<<"MacroSize ="<<nReals_Macro<<"\n";
}

////////////////
// Start Output:
////////////////

showStart(fio);


/////////////////////////////////////////////////////
// Do Some Turns, and Dump Particles for Later Plots:
/////////////////////////////////////////////////////

if(nRank == 0){
 cerr << "Start Tracking\n";
}

String nameOfHerdIn = "herd_test_1_in_";
nameOfHerdIn = nameOfHerdIn + sRank + ".dat";
OFstream  fisHAIn(nameOfHerdIn , ios::out);
dumpPartsGlobal(mainHerd, fisHAIn);
  
//doTurn(1);
TransImpWFpropagate(indexTImpWF,T_timpWF);

String nameOfHerdOut = "herd_test_1_out_";
nameOfHerdOut  = nameOfHerdOut  + sRank + ".dat";
OFstream  fisHAOut (nameOfHerdOut , ios::out);
dumpPartsGlobal(mainHerd, fisHAOut );

quit
