///////////////////////////////////////////////////////////////////////
// ORBIT_MPI test srcript for MPI_test_my function
///////////////////////////////////////////////////////////////////////

Integer n_CPU;
Integer n_Rank;
Integer i_mpi;
String name_of_CPU;

i_mpi = MPI_Initialized0();

n_CPU  = MPI_size();
n_Rank = MPI_rank();
MPI_Get_CPU_name(name_of_CPU);

cout <<"CPU="<<name_of_CPU<<" rank="<<n_Rank<<" size="<<n_CPU<<" initialized="<<i_mpi<<"\n";


//MPI_test_1_my();


//Test of conjunction Integer and the String
String str="test_";
cout << "before string ="<<str<<"\n";
MPI_String_Add_Integer(str, n_Rank);
str=str+"_";
MPI_String_Add_Integer(str, n_CPU);
str=str+"_.out";
cout << "after string  ="<<str<<"\n";

quit
