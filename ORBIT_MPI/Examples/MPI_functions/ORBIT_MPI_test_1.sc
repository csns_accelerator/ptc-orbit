///////////////////////////////////////////////////////////////////////
// ORBIT_MPI test srcript 
///////////////////////////////////////////////////////////////////////
// Description of the implemented MPI functions and procedures
//
// MPI_size() - total number of CPUs
// MPI_rank() - rank of the current CPU (0..(MPI_size()-1))
// MPI_Get_CPU_name - name of the curent CPU
// MPI_Initialized0 - 1 if MPI_init has been called (0-otherwise)
// MPI_Allreduce_MAX_Integer - finds the maximal integer among all CPUs
// MPI_Allreduce_SUM_Integer - finds the sum of integers among all CPUs
// MPI_Allreduce_MAX_Real    - finds the maximal Real among all CPUs
// MPI_Allreduce_SUM_Real    - finds the sum of Reals among all CPUs
// MPI_Barrier0  - synchronizes all tasks 
// MPI_Wtime0 - returns time from the beginning in seconds
////////////////////////////////////////////////////////////////////////
Integer n_CPU;
Integer n_Rank;
Integer i_mpi;
String name_of_CPU;

Real time_start;
Real time_stop;

Integer i;
Integer i_max;
Integer i_sum;
Real d;
Real d_max;
Real d_sum;

i_mpi = MPI_Initialized0();

n_CPU  = MPI_size();
n_Rank = MPI_rank();
MPI_Get_CPU_name(name_of_CPU);

cout <<"CPU="<<name_of_CPU<<" rank="<<n_Rank<<" size="<<n_CPU<<" initialized="<<i_mpi<<"\n";

time_start = MPI_Wtime0();

i = n_Rank;

i_max = MPI_Allreduce_MAX_Integer(i);
i_sum = MPI_Allreduce_SUM_Integer(i);

d = 1.0*n_Rank;

d_max = MPI_Allreduce_MAX_Real(d);
d_sum = MPI_Allreduce_SUM_Real(d);

if( n_Rank == 0 ){
 cout <<"i_max="<<i_max<<" should be equal to maximal rank"<<"\n";
 cout <<"i_sum="<<i_sum<<" should be equal to sum of ranks"<<"\n";
 cout <<"d_max="<<d_max<<" should be equal to maximal rank"<<"\n";
 cout <<"d_sum="<<d_sum<<" should be equal to sum of ranks"<<"\n";
}


time_stop = MPI_Wtime0();

Real t = time_stop - time_start;

t = MPI_Allreduce_MAX_Real(t);

if( n_Rank == 0 ){

cout <<"time ="<<t<<" sec.\n";

}


quit
