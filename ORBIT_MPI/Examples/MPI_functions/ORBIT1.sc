Integer n_CPU;
Integer n_Rank;
String name_of_CPU;

n_CPU  = MPI_size();
n_Rank = MPI_rank();
MPI_Get_CPU_name(name_of_CPU);

cout <<"CPU="<<name_of_CPU<<" rank="<<n_Rank<<" size="<<n_CPU<<"\n";


  Real TSync = 1000.;  
  Real mSync = 1;    
  Real charge = 1;     

  addSyncPart(mSync,charge,TSync);

//////////////////////////////
// Make some Macro Particles:
//////////////////////////////

 mainHerd = addMacroHerd(1000);


  Real x,xp,y,yp;

  x = 1.;xp = 0., y=0., yp=1.;
  addMacroPart(x,xp,y,yp);

  x = 2.;xp = -1., y=1., yp=-1.;
  addMacroPart(x,xp,y,yp);

//////////////////////////////
//
// Make  some Ring Nodes "by hand"
// Yukkk!, You'll rarely want to do this, but if you
// want to you can.
//
//////////////////////////////

RealMatrix R(6,6);
R(1,1) = 1.371014; R(1,2) = -11.99692; R(1,6) =1.49099e-4;
R(2,1) =-9.506066e-2; R(2,2) = 1.561206; R(2,6) =  -2.135459e-5;
R(3,3) = -0.6030482; R(3,4) = -6.575110;
R(4,3) = 8.183674e-2; R(4,4) =  -0.7659661;
R(5,1) = 1.510395e-5; R(5,2) = 2.341438e-5; R(5,6) = 4.540528;


  addTransferMatrix("Foil2Cav",10,R,   15.928, 5.045, 
    -1.979, 0.747, 0., 0., 102.555,"lineElem");

R(1,1) = 2.089929; R(1,2) =  6.828369; R(1,6) = 8.431313e-5;
R(2,1) = 0.3062153; R(2,2) = 1.478974; R(2,6) =  -6.101376e-7;
R(3,3) = 0.565041; R(3,4) = 1.109158;
R(4,3) =  -0.3041498; R(4,4) =1.172746;
R(5,2) =  -2.709311e-5; R(5,6) = -0.1288632e-4; R(5,6) = 4.540528;

  addTransferMatrix("Cav2End", 20,R,  9.263, 9.261, 
    1.3639, -1.3630, 0., 0., 118.133,"lineElem");

//////////////////////////////
// Add a foil
//////////////////////////////

  addFoil("Foil", 1, 0., 10., 0., 10., 300.);


//////////////////////////////
// show Nodes
//////////////////////////////

  showNodes(cout);
  showTransMatrix(cout);

//////////////////////////////
// do a turn:
//////////////////////////////

   doTurn(1);

//////////////////////////////
// show MacroParticles:
//////////////////////////////

  showHerd(mainHerd, cout);

/////////////////////////////////////////////////
// Do some turns, store hit coordinates and plot
/////////////////////////////////////////////////

 Integer i, nT=10;
 RealVector xHit(nT), xpHit(nT), yHit(nT), ypHit(nT);

 for (i=1; i<=nT; i++)


{
  doTurn(1);
  xHit(i) = xVal(1);
  xpHit(i) = xpVal(1);
  yHit(i) = yVal(1);
  ypHit(i) = ypVal(1);

 }
 

// Number of foil hits/particle :

cerr << "hits/particle = " << Real(nFoilHits())/Real(nMacroParticles) << "\n";

quit
