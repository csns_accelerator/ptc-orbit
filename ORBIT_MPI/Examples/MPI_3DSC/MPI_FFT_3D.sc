//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_FFT_3D.sc
//
// AUTHOR
//    J. Holmes (jzh@ornl.gov)
//
// CREATED
//    07/19/2001
//
//
// DESCRIPTION
//    Script file for a coasting beam case with uniform focusing and
//    3D space charge and circular beam pipe.
//
///////////////////////////////////////////////////////////////////////////


  String runName, of1;
  runName = "FFT_3D";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);

  Real TSync = 1.;
  Real mSync = 1;
  Real charge = 1;
  addSyncPart(mSync,charge,TSync);

  nMaxMacroParticles = 10;
  Real BeamLength = 1.00;
  nReals_Macro = 1.00e14/(nMaxMacroParticles*BeamLength);
  mainHerd = addMacroHerd(nMaxMacroParticles);
  readParts(mainHerd, "Herd_3Dtest_before.dat", nMaxMacroParticles);
  cerr << "Done reading mainherd:\n";

/////////////////
// Make a Lattice
/////////////////

  cerr << "Create lattice :\n"; 
  String LatType = "Ring";
  Real LengthTunes = 248.;
  Real XTune = 6.40;
  Real YTune = 6.30;
  Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 248;
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);
  cerr << "Done Creating Lattice:\n"; 


  String nameOfHerd = "Bm_Parts_0001_MPI_";
  Integer nRank=MPI_rank();
  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".out";
  OFstream fio1(nameOfHerd, ios::out);

  dumpParts(mainHerd, fio1);
  fio1.close();
//-------------------------------------------------------
// boundaries and 3DSC integrator
//-------------------------------------------------------
  Integer nCountLM = 10;
  Integer nSlices_total = 25;

  Integer minBunchSize = 1;
  Integer xBins = 64;
  Integer yBins = 64;
  Real xSize = 110.;
  Real ySize = 110.;
  Integer BPPoints = 64;
  Integer BPShape = 1;
  Integer BPModes = 20;
  Real eps = 0.001;



  Real length = 0.5;

  createPartDistr(nCountLM,nSlices_total);
  createBoundary(xBins, yBins, xSize, ySize, BPPoints, BPShape, BPModes, eps);
  createFFT3DSpaceCharge_MPI(xBins,yBins, nSlices_total, minBunchSize);

  cerr << "Instances are created.\n";

  propagateFFT3DSpaceCharge_MPI(length);

//-------------------------------------------------------

  nameOfHerd = "Bm_Parts_0001_MPI_new_";
  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".out";
  OFstream fio1(nameOfHerd, ios::out);

  dumpParts(mainHerd, fio1);
  fio1.close();

  cerr << "Stop.\n";
  quit

