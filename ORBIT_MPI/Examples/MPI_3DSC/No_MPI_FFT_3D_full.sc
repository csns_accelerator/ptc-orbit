//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    No_MPI_FFT_3D_full.sc
//
// DESCRIPTION
//    Script file for a uniform focusing and
//    3D space charge and circular beam pipe.
//
///////////////////////////////////////////////////////////////////////////


  String runName, of1;
  runName = "FFT_3D";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);

  Real TSync = 1.;
  Real mSync = 1;
  Real charge = 1;
  addSyncPart(mSync,charge,TSync);

  nMaxMacroParticles = 1000;
  Real BeamLength = 1.00;
  nReals_Macro = 1.00e14/(nMaxMacroParticles*BeamLength);
  mainHerd = addMacroHerd(nMaxMacroParticles);
  readParts(mainHerd, "Bm_Parts_ini_0", nMaxMacroParticles);
  cerr << "Done reading mainherd:\n";

/////////////////
// Make a Lattice
/////////////////

  cerr << "Create lattice :\n"; 
  String LatType = "Ring";
  Real LengthTunes = 12.;
  Real XTune = 6.40;
  Real YTune = 6.30;
  Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 12;
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);

  cerr << "Done Creating Lattice:\n"; 

// boundaries for the grid

  Integer nMacrosMin = 1;
  Integer nxBins = 64, nyBins = 64, nphiBins=24;
  Real eps = 0.001;
  String BPShape = "Circle";
  Real BP1 = 55., BP2 = 0., BP3 = 0., BP4 = 0.;
  Integer BPPoints = 64, BPModes = 20;
  Real Gridfact = 2.0;

  addSpCh3D_FFTSet(nxBins, nyBins, nphiBins, eps,
                   BPShape, BP1, BP2, BP3, BP4,
                   BPPoints, BPModes, Gridfact, nMacrosMin);

  cerr << "Done Creating SC node:\n";


///////////////////////////////////////////
// Add Moment and StatLat Nodes
///////////////////////////////////////////

  addMomentNode("MomentNode(0)", 4, 4, "Moments");       
  addStatLatNode("StatLatNode(0)", 5, "StatLats");       
  activateMomentNode(1);
  activateStatLatNode(1);

// Set up to histogram the distributions:

    Integer i, nBins = 60;
    RealVector xHist(nBins), yHist(nBins);

    Real xGMin, dx, yGMin, dy, phiGMin, dphi;
    xGMin = -39.; dx = 78./Real(nBins);
    yGMin = -39.; dy = 78./Real(nBins);
    phiGMin = -3.1415926539; dphi = -2.*phiGMin/Real(nBins);

// Emittance Output:
    nEmitBins = 100;   deltaEmitBin = 2.;  // set up emittance bins


//////////////////////////////
// Start Output:
//////////////////////////////

  showStart(fio);

//////////////////////////////
// do some turns, and dump particles for later plots:
//////////////////////////////

 cerr << "Start Tracking\n";
  Real et;
  timerOn();

  cerr << "======== Start My Calculation  =========\n";
//Integer index_Nodes = 2;
//turnToNode(mainHerd, index_Nodes);
doTurn(10);

  OFstream  fisHA("Bm_Parts_000_no_MPI.dat", ios::out);
  dumpParts(mainHerd, fisHA);

  cerr << "======== STOP Calculation  =========\n";

  quit


//==================STOP==================================================

