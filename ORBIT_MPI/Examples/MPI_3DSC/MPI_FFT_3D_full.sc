//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MPI_FFT_3D_full.sc
//
// DESCRIPTION
//    Script file for a uniform focusing and
//    3D space charge and circular beam pipe.
//
///////////////////////////////////////////////////////////////////////////

  //MPI stuff
  Integer nRank=MPI_rank();

  String runName, of1;
  runName = "FFT_3D";
  of1 = runName + ".prt";

  OFstream fio(of1, ios::out);

  Real TSync = 1.;
  Real mSync = 1;
  Real charge = 1;
  addSyncPart(mSync,charge,TSync);

  nMaxMacroParticles = 1000;
  Real BeamLength = 1.00;
  nReals_Macro = 1.00e14/(nMaxMacroParticles*BeamLength);
  mainHerd = addMacroHerd(nMaxMacroParticles);
  readParts(mainHerd, "Bm_Parts_ini_0", nMaxMacroParticles);

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done reading mainherd:\n";
  }

/////////////////
// Make a Lattice
/////////////////
 
  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Create lattice :\n"; 
  }

  String LatType = "Ring";
  Real LengthTunes = 12.;
  Real XTune = 6.40;
  Real YTune = 6.30;
  Real RhoInv = 1.0*2.*pi/LengthTunes;
  Integer nElements = 12;
  Uniform_LAT(LatType, LengthTunes, XTune, YTune, RhoInv, nElements);

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done Creating Lattice:\n"; 
  }

//-------------------------------------------------------
// boundaries and 3DSC integrator
//-------------------------------------------------------
  Integer nMacrosMin = 1;
  Integer nxBins = 64, nyBins = 64, nphiBins=24;
  Real eps = 0.001;
  String BPShape = "Circle";
  Real BP1 = 110., BP2 = 110.;
  Integer BPPoints = 64, BPModes = 20;
  Real Gridfact = 2.0; 

  addSpCh3D_FFTSet_MPI(nxBins, nyBins, nphiBins, eps,
                       BPShape, BP1, BP2,
                       BPPoints, BPModes, Gridfact, nMacrosMin);


  //MPI stuff
  if( nRank == 0 ) {
    cerr << "Done Creating SC node:\n";
  }

///////////////////////////////////////////
// Add Moment and StatLat Nodes
///////////////////////////////////////////

  addMomentNode("MomentNode(0)", 4, 4, "Moments");       
  addStatLatNode("StatLatNode(0)", 5, "StatLats");       
  activateMomentNode(1);
  activateStatLatNode(1);

// Set up to histogram the distributions:

    Integer i, nBins = 60;
    RealVector xHist(nBins), yHist(nBins);

    Real xGMin, dx, yGMin, dy, phiGMin, dphi;
    xGMin = -39.; dx = 78./Real(nBins);
    yGMin = -39.; dy = 78./Real(nBins);
    phiGMin = -3.1415926539; dphi = -2.*phiGMin/Real(nBins);

// Emittance Output:
    nEmitBins = 100;   deltaEmitBin = 2.;  // set up emittance bins


//////////////////////////////
// Start Output:
//////////////////////////////

  showStart(fio);

//////////////////////////////
// do some turns, and dump particles for later plots:
//////////////////////////////


  //MPI stuff
  if( nRank == 0 ) {
   cerr << "Start Tracking\n";
  }


  Real et;
  timerOn();

  //MPI stuff
  if( nRank == 0 ) {
    cerr << "======== Start My Calculation  =========\n";
  }

//Integer index_Nodes = 2;
//turnToNode(mainHerd, index_Nodes);
doTurn(10);  

  if( nRank == 0 ) {
    cerr << "======== STOP Calculation  =========\n";
  }

//-------------------------------------------------------

  String nameOfHerd = "Bm_Parts_000_MPI_";

  MPI_String_Add_Integer(nameOfHerd,nRank);
  nameOfHerd = nameOfHerd + ".dat";
  OFstream fio1(nameOfHerd, ios::out);

  dumpParts(mainHerd, fio1);
  fio1.close();

  quit

