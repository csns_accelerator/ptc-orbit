////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   SpaceCharge3D.cc
//
// AUTHOR
//   Slava Danilov, Jeff Holmes, John Galambos
//   ORNL, vux@ornl.gov
//
// CREATED
//   4/10/2001
//
// DESCRIPTION
//   Source code for the 3D spacecharge kick.
//   This module contains source for the SpaceCharge3D.cc  related info.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "SpCh3D.h"
#include "SpaceCharge3D.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "Node.h"
#include "Particles.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "ComplexMat.h"
#include "SCLTypes.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>
#include <fftw.h>

#if defined(USE_PVM)
#include "pvm3.h"
#endif

using namespace std;

Array(ObjectPtr) SpCh3DPointers;
extern Array(ObjectPtr) mParts, nodes;
extern Array(ObjectPtr) syncP;
extern Array(ObjectPtr) tMaps;

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS SpCh3D
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(SpCh3D, Node);

Vector(Real) SpCh3D::_xGrid, SpCh3D::_yGrid, SpCh3D::_phiGrid;
Array3(Real) SpCh3D::_potentialSC, SpCh3D::_rho3D;
Matrix(Real) SpCh3D::_phisc, SpCh3D::_fscx, SpCh3D::_fscy;
Real SpCh3D::_xGridMin, SpCh3D::_xGridMax;
Real SpCh3D::_yGridMin,SpCh3D::_yGridMax;
Real SpCh3D::_phiGridMin, SpCh3D::_phiGridMax;

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS SpCh3D_BF
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(SpCh3D_BF, SpCh3D);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   SpCh3D_BF::_fullBin
//
// DESCRIPTION
//   Bins the macro particles in all 3 dimensions
//
// PARAMETERS
//   mp - the macro herd to operate on.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void SpCh3D_BF::_fullBin(MacroPart &mp)
{
   Integer i, j, iX, iY, iPhi, iPhim, iPhip;

 // Find particle extent:

   mp._findXYPExtrema();

   Real dExtra;
   dExtra = (mp._xMax - mp._xMin) / Real(_nXBins - 2);
   _xGridMin = mp._xMin - dExtra;
   _xGridMax = mp._xMax + dExtra;

   dExtra = (mp._yMax - mp._yMin) / Real(_nYBins - 2);
   _yGridMin = mp._yMin - dExtra;
   _yGridMax = mp._yMax + dExtra;

   dExtra = 0.;
   if(_nPhiBins > 2) 
     dExtra = (mp._phiMax - mp._phiMin) / Real(_nPhiBins - 2);
   _phiGridMin = mp._phiMin - dExtra;
   _phiGridMax = mp._phiMax + dExtra;
   if (_phiGridMin < -Consts::pi) _phiGridMin = -Consts::pi;
   if (_phiGridMax > Consts::pi) _phiGridMax = Consts::pi;

 // Define the grid:

   _dx = (_xGridMax - _xGridMin) / Real(_nXBins);
   _dy = (_yGridMax - _yGridMin) / Real(_nYBins);
   _dphi = (_phiGridMax - _phiGridMin) / Real(_nPhiBins);

   for (iX = 1; iX <= _nXBins+1; iX++)
     _xGrid(iX) = _xGridMin + Real(iX-1) * _dx;

   for (iY = 1; iY <= _nYBins+1; iY++)
     _yGrid(iY) = _yGridMin + Real(iY-1) * _dy;
 
   for (iPhi = 1; iPhi <= _nPhiBins+1; iPhi++)
     _phiGrid(iPhi) = _phiGridMin + Real(iPhi-1) * _dphi;

 // Bin the particles:

   _rho3D = 0.;

   for(i=1; i <= mp._nMacros; i++)
   {
     iX = 1 + Integer((mp._x(i) - _xGridMin)/_dx + 0.5);
     iY = 1 + Integer((mp._y(i) - _yGridMin)/_dy + 0.5);
     iPhi = 1 + Integer((mp._phi(i) - _phiGridMin)/_dphi + 0.5);

     if(iX < 2) iX = 2;
     if(iX > _nXBins) iX = _nXBins;
     if(iY < 2) iY = 2;
     if(iY > _nYBins) iY = _nYBins;
     if(iPhi < 2) iPhi = 1;
     if(iPhi > _nPhiBins) iPhi = _nPhiBins + 1;
     iPhim = iPhi - 1;
     iPhip = iPhi + 1;
     if(iPhi == _nPhiBins + 1) iPhip = 2;
     if(iPhi == 1) iPhim = _nPhiBins;

      mp._xBin(i) = iX;
      mp._yBin(i) = iY;
      mp._LPositionIndex(i) = iPhi;

      mp._xFractBin(i) = (mp._x(i) - _xGrid(iX)) / _dx;
      mp._yFractBin(i) = (mp._y(i) - _yGrid(iY)) / _dy;
      mp._fractLPosition(i) = (mp._phi(i) - _phiGrid(iPhi)) / _dphi;

      // TSC binning, see Hockney and Eastwood:

      Real Wxm = 0.5 * (0.5 - mp._xFractBin(i))
                     * (0.5 - mp._xFractBin(i));
      Real Wx0 = 0.75 - mp._xFractBin(i) * mp._xFractBin(i);
      Real Wxp = 0.5 * (0.5 + mp._xFractBin(i))
	             * (0.5 + mp._xFractBin(i));
      Real Wym = 0.5 * (0.5 - mp._yFractBin(i))
	             * (0.5 - mp._yFractBin(i));
      Real Wy0 = 0.75 - mp._yFractBin(i) * mp._yFractBin(i);
      Real Wyp = 0.5 * (0.5 + mp._yFractBin(i))
	             * (0.5 + mp._yFractBin(i));
      Real Wphim = 0.5 * (0.5 - mp._fractLPosition(i))
	               * (0.5 - mp._fractLPosition(i));
      Real Wphi0 = 0.75 - mp._fractLPosition(i) * mp._fractLPosition(i);
      Real Wphip = 0.5 * (0.5 + mp._fractLPosition(i))
	               * (0.5 + mp._fractLPosition(i));

     _rho3D(iX-1,iY-1,iPhim) += Wxm * Wym * Wphim;
     _rho3D(iX  ,iY-1,iPhim) += Wx0 * Wym * Wphim;
     _rho3D(iX+1,iY-1,iPhim) += Wxp * Wym * Wphim;
     _rho3D(iX-1,iY  ,iPhim) += Wxm * Wy0 * Wphim;
     _rho3D(iX  ,iY  ,iPhim) += Wx0 * Wy0 * Wphim;
     _rho3D(iX+1,iY  ,iPhim) += Wxp * Wy0 * Wphim;
     _rho3D(iX-1,iY+1,iPhim) += Wxm * Wyp * Wphim;
     _rho3D(iX  ,iY+1,iPhim) += Wx0 * Wyp * Wphim;
     _rho3D(iX+1,iY+1,iPhim) += Wxp * Wyp * Wphim;
     _rho3D(iX-1,iY-1, iPhi) += Wxm * Wym * Wphi0;
     _rho3D(iX  ,iY-1, iPhi) += Wx0 * Wym * Wphi0;
     _rho3D(iX+1,iY-1, iPhi) += Wxp * Wym * Wphi0;
     _rho3D(iX-1,iY  , iPhi) += Wxm * Wy0 * Wphi0;
     _rho3D(iX  ,iY  , iPhi) += Wx0 * Wy0 * Wphi0;
     _rho3D(iX+1,iY  , iPhi) += Wxp * Wy0 * Wphi0;
     _rho3D(iX-1,iY+1, iPhi) += Wxm * Wyp * Wphi0;
     _rho3D(iX  ,iY+1, iPhi) += Wx0 * Wyp * Wphi0;
     _rho3D(iX+1,iY+1, iPhi) += Wxp * Wyp * Wphi0;
     _rho3D(iX-1,iY-1,iPhip) += Wxm * Wym * Wphip;
     _rho3D(iX  ,iY-1,iPhip) += Wx0 * Wym * Wphip;
     _rho3D(iX+1,iY-1,iPhip) += Wxp * Wym * Wphip;
     _rho3D(iX-1,iY  ,iPhip) += Wxm * Wy0 * Wphip;
     _rho3D(iX  ,iY  ,iPhip) += Wx0 * Wy0 * Wphip;
     _rho3D(iX+1,iY  ,iPhip) += Wxp * Wy0 * Wphip;
     _rho3D(iX-1,iY+1,iPhip) += Wxm * Wyp * Wphip;
     _rho3D(iX  ,iY+1,iPhip) += Wx0 * Wyp * Wphip;
     _rho3D(iX+1,iY+1,iPhip) += Wxp * Wyp * Wphip;
   }
	
// Periodic boundary conditions
//   - levelling the last and the first node density 

   for (i=1 ; i <= _nXBins+1 ; i++)
   {
     for(j=1 ;j <= _nYBins+1 ; j++)
     {
       _rho3D(i,j,1) = _rho3D(i,j,1)+_rho3D(i,j,_nPhiBins+1);  
       _rho3D(i,j,_nPhiBins+1)= _rho3D(i,j,1);
     }
   }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    SpCh3D_BF::_nodeCalculator
//
// DESCRIPTION
// Calculates the potentials on the grid nodes
//
// PARAMETERS
//   None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void  SpCh3D_BF::_nodeCalculator(MacroPart &mp)
{
  // test to see if this is not the main herd:

   if(mp._feelsHerds == 1)  // just bin the particles, use existing forces:
   {
       _fullBin(mp);
       return;
   }

   if(mp._nMacros < _nMacrosMin && !Parallel::parallelRun) 
   {
     mp._globalNMacros = mp._nMacros;
     return;  
   }
    
  _fullBin(mp);

  // calculate the potentials  with

  _potentialSC=0.;

  // potentials of the charge of the mesh point plus
  // the potential of the image charges of the vacuum chamber 
  // with the radius  SpaceCharge3D::_radVChamber

  // potential argument incremented by 1 in order
  // to implement 9 point kick scheme
  // without complications


  Integer i,j,k,l,m;
  Real xImage, yImage, xTest, yTest, rTestSquare, logArg, logItself;
  Real R, xObs, yObs, rObsSquare, RSquare;

  R=SpaceCharge3D::_radVChamber;
  RSquare = R*R;
  for(i=1;i<=_nPhiBins+1;i++)
  {
    for(j=1;j<=_nXBins;j++)
    {
      for(k=1;k<=_nYBins;k++)
      {
        xObs=_xGridMin+_dx*(j-1);
        yObs=_yGridMin+_dy*(k-1);
        rObsSquare=xObs*xObs + yObs*yObs;

          for(l=1;l<=_nXBins;l++)
          {
            for(m=1;m<=_nYBins;m++)
            {
              xTest=_xGridMin+_dx*(l-1);
              yTest=_yGridMin+_dy*(m-1);
              rTestSquare=xTest*xTest+yTest*yTest;

              if (rTestSquare > 0.0000001) 
              { 
                xImage = xTest* R*R/rTestSquare;
                yImage = yTest* R*R/rTestSquare;
              }
              else
              {
                xImage=R*100000.;
                yImage=R*100000.;
              }

              if(l!=_nXBins/2+1 || m!=_nYBins/2+1)    
              {
                if((j!=l || k!=m) && rTestSquare < R*R && rObsSquare < R*R)
                {
                  logArg = ((xObs-xTest)*(xObs-xTest)
                         + (yObs-yTest)*(yObs-yTest))*R*R/rTestSquare/
                           ((xObs-xImage)*(xObs-xImage)
                         + (yObs-yImage)*(yObs-yImage));
                  logItself = log(logArg);
                  _potentialSC(j+1,k+1,i) += _rho3D(l,m,i)*logItself ;
                }
              }
              else
              { 
                if((j!=_nXBins/2+1 || k!=_nYBins/2+1) && rObsSquare < R*R )
                { 
                  _potentialSC(j+1,k+1,i) += _rho3D(l,m,i)*
                                             log((xObs*xObs +yObs*yObs)/R/R) ;
                }
              }
            }
          }
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//     SpCh3D_BF::_updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with an Brute Force 3D Spacecharge.
// The transverse kick is
//    added to each macro particle here.
//
// PARAMETERS
//   MacroPart herd
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void  SpCh3D_BF::_updatePartAtNode(MacroPart &mp)
{
  // Average kick interpolated from bin location:
  // 9 point transverse weight scheme for the potential
  // 3 point longitudinal scheme
  // then the differentiation of the potentials

  Real kickX, kickY, kickE, CoefT;
  Real CoefL;
  Real _lambda;
  Real rClassical = 1.534698e-18; // Classical p radius

  SyncPart *sp = SyncPart::safeCast
                 (syncP((Particles::syncPart & Particles::All_Mask)-1));

  //debug ===shishlo === start ==
  //cout << "SpCh3D_FFT::_updatePartAtNode START!!! \n";
  //cout << "             nMacros in the herd = " << mp._nMacros << "\n";

  if(mp._nMacros < _nMacrosMin && !Parallel::parallelRun) 
  {
    return;  
  }
  //debug ===shishlo === stop ==

  // The coefficients for the kicks


  _lambda = Injection::nReals_Macro *
            Ring::harmonicNumber * Consts::twoPi
            / (Ring::lRing * _dphi);

  CoefL = Sqr(sp->_charge) * _lambda * rClassical * _lkick/
          (sp->_mass * Sqr(sp->_betaSync) * Cube(sp->_gammaSync));

  CoefT = -1000. * 1000. * CoefL;

  Integer iPhim, iPhi, iPhip;

  Integer j;
  for (j = 1; j <= mp._nMacros; j++)
  {
    iPhi = mp._LPositionIndex(j);
    if((iPhi > _nPhiBins) || (iPhi < 1)) iPhi = 1;
    iPhip = iPhi + 1;
    iPhim = iPhi - 1;
    if(iPhi == _nPhiBins) iPhip = 1;
    if(iPhi == 1) iPhim = _nPhiBins;
    if(_nPhiBins < 2)
    {
      iPhi = 1;
      iPhim = 1;
      iPhip = 1;
    }

    Integer iX, iY;
    iX = mp._xBin(j);
    iY = mp._yBin(j);
    Real xFract = mp._xFractBin(j);
    Real yFract = mp._yFractBin(j);
    Real phiFract = mp._fractLPosition(j);

    // TSC binning, see Hockney and Eastwood:

    Real Wxm = 0.5 * (0.5 - xFract)
                   * (0.5 - xFract);
    Real Wx0 = 0.75 - xFract * xFract;
    Real Wxp = 0.5 * (0.5 + xFract)
	           * (0.5 + xFract);
    Real Wym = 0.5 * (0.5 - yFract)
	           * (0.5 - yFract);
    Real Wy0 = 0.75 - yFract * yFract;
    Real Wyp = 0.5 * (0.5 + yFract)
                   * (0.5 + yFract);
    Real Wphim = 0.5 * (0.5 - phiFract)
                     * (0.5 - phiFract);
    Real Wphi0 = 0.75 - phiFract * phiFract;
    Real Wphip = 0.5 * (0.5 + phiFract)
	             * (0.5 + phiFract);

    Real dWxm = (0.5 - xFract) / _dx;
    Real dWx0 = 2. * xFract / _dx;
    Real dWxp = -(0.5 + xFract) / _dx;
    Real dWym = (0.5 - yFract) / _dy;
    Real dWy0 = 2. * yFract / _dy;
    Real dWyp = -(0.5 + yFract) / _dy;
    Real dWphim = (0.5 - phiFract) * Ring::harmonicNumber * Consts::twoPi
                  / (_dphi * Ring::lRing);
    Real dWphi0 = 2. * phiFract * Ring::harmonicNumber * Consts::twoPi
                  / (_dphi * Ring::lRing);
    Real dWphip = -(0.5 + phiFract) * Ring::harmonicNumber * Consts::twoPi
                  / (_dphi * Ring::lRing);

    kickX = Wphim *
            (
            dWxm * Wym * _potentialSC(iX  ,iY  ,iPhim) +
            dWx0 * Wym * _potentialSC(iX+1,iY  ,iPhim) +
            dWxp * Wym * _potentialSC(iX+2,iY  ,iPhim) +
            dWxm * Wy0 * _potentialSC(iX  ,iY+1,iPhim) +
            dWx0 * Wy0 * _potentialSC(iX+1,iY+1,iPhim) +
            dWxp * Wy0 * _potentialSC(iX+2,iY+1,iPhim) +
            dWxm * Wyp * _potentialSC(iX  ,iY+2,iPhim) +
            dWx0 * Wyp * _potentialSC(iX+1,iY+2,iPhim) +
            dWxp * Wyp * _potentialSC(iX+2,iY+2,iPhim)
            )
            +
            Wphi0 *
            (
            dWxm * Wym * _potentialSC(iX  ,iY  , iPhi) +
            dWx0 * Wym * _potentialSC(iX+1,iY  , iPhi) +
            dWxp * Wym * _potentialSC(iX+2,iY  , iPhi) +
            dWxm * Wy0 * _potentialSC(iX  ,iY+1, iPhi) +
            dWx0 * Wy0 * _potentialSC(iX+1,iY+1, iPhi) +
            dWxp * Wy0 * _potentialSC(iX+2,iY+1, iPhi) +
            dWxm * Wyp * _potentialSC(iX  ,iY+2, iPhi) +
            dWx0 * Wyp * _potentialSC(iX+1,iY+2, iPhi) +
            dWxp * Wyp * _potentialSC(iX+2,iY+2, iPhi)
            )
            +
            Wphip *
            (
            dWxm * Wym * _potentialSC(iX  ,iY  ,iPhip) +
            dWx0 * Wym * _potentialSC(iX+1,iY  ,iPhip) +
            dWxp * Wym * _potentialSC(iX+2,iY  ,iPhip) +
            dWxm * Wy0 * _potentialSC(iX  ,iY+1,iPhip) +
            dWx0 * Wy0 * _potentialSC(iX+1,iY+1,iPhip)  +
            dWxp * Wy0 * _potentialSC(iX+2,iY+1,iPhip) +
            dWxm * Wyp * _potentialSC(iX  ,iY+2,iPhip) +
            dWx0 * Wyp * _potentialSC(iX+1,iY+2,iPhip) +
            dWxp * Wyp * _potentialSC(iX+2,iY+2,iPhip)
            );

    kickY = Wphim *
            (
            Wxm * dWym * _potentialSC(iX  ,iY  ,iPhim) +
            Wx0 * dWym * _potentialSC(iX+1,iY  ,iPhim) +
            Wxp * dWym * _potentialSC(iX+2,iY  ,iPhim) +
            Wxm * dWy0 * _potentialSC(iX  ,iY+1,iPhim) +
            Wx0 * dWy0 * _potentialSC(iX+1,iY+1,iPhim) +
            Wxp * dWy0 * _potentialSC(iX+2,iY+1,iPhim) +
            Wxm * dWyp * _potentialSC(iX  ,iY+2,iPhim) +
            Wx0 * dWyp * _potentialSC(iX+1,iY+2,iPhim) +
            Wxp * dWyp * _potentialSC(iX+2,iY+2,iPhim)
            )
            +
            Wphi0 *
            (
            Wxm * dWym * _potentialSC(iX  ,iY  , iPhi) +
            Wx0 * dWym * _potentialSC(iX+1,iY  , iPhi) +
            Wxp * dWym * _potentialSC(iX+2,iY  , iPhi) +
            Wxm * dWy0 * _potentialSC(iX  ,iY+1, iPhi) +
            Wx0 * dWy0 * _potentialSC(iX+1,iY+1, iPhi) +
            Wxp * dWy0 * _potentialSC(iX+2,iY+1, iPhi) +
            Wxm * dWyp * _potentialSC(iX  ,iY+2, iPhi) +
            Wx0 * dWyp * _potentialSC(iX+1,iY+2, iPhi) +
            Wxp * dWyp * _potentialSC(iX+2,iY+2, iPhi)
            )
            +
            Wphip *
            (
            Wxm * dWym * _potentialSC(iX  ,iY  ,iPhip) +
            Wx0 * dWym * _potentialSC(iX+1,iY  ,iPhip) +
            Wxp * dWym * _potentialSC(iX+2,iY  ,iPhip) +
            Wxm * dWy0 * _potentialSC(iX  ,iY+1,iPhip) +
            Wx0 * dWy0 * _potentialSC(iX+1,iY+1,iPhip) +
            Wxp * dWy0 * _potentialSC(iX+2,iY+1,iPhip) +
            Wxm * dWyp * _potentialSC(iX  ,iY+2,iPhip) +
            Wx0 * dWyp * _potentialSC(iX+1,iY+2,iPhip) +
            Wxp * dWyp * _potentialSC(iX+2,iY+2,iPhip)
            );

    kickE = dWphim *
            (
            Wxm * Wym * _potentialSC(iX  ,iY  ,iPhim) +
            Wx0 * Wym * _potentialSC(iX+1,iY  ,iPhim) +
            Wxp * Wym * _potentialSC(iX+2,iY  ,iPhim) +
            Wxm * Wy0 * _potentialSC(iX  ,iY+1,iPhim) +
            Wx0 * Wy0 * _potentialSC(iX+1,iY+1,iPhim) +
            Wxp * Wy0 * _potentialSC(iX+2,iY+1,iPhim) +
            Wxm * Wyp * _potentialSC(iX  ,iY+2,iPhim) +
            Wx0 * Wyp * _potentialSC(iX+1,iY+2,iPhim) +
            Wxp * Wyp * _potentialSC(iX+2,iY+2,iPhim)
            )
            +
            dWphi0 *
            (
            Wxm * Wym * _potentialSC(iX  ,iY  , iPhi) +
            Wx0 * Wym * _potentialSC(iX+1,iY  , iPhi) +
            Wxp * Wym * _potentialSC(iX+2,iY  , iPhi) +
            Wxm * Wy0 * _potentialSC(iX  ,iY+1, iPhi) +
            Wx0 * Wy0 * _potentialSC(iX+1,iY+1, iPhi) +
            Wxp * Wy0 * _potentialSC(iX+2,iY+1, iPhi) +
            Wxm * Wyp * _potentialSC(iX  ,iY+2, iPhi) +
            Wx0 * Wyp * _potentialSC(iX+1,iY+2, iPhi) +
            Wxp * Wyp * _potentialSC(iX+2,iY+2, iPhi)
            )
            +
            dWphip *
            (
            Wxm * Wym * _potentialSC(iX  ,iY  ,iPhip) +
            Wx0 * Wym * _potentialSC(iX+1,iY  ,iPhip) +
            Wxp * Wym * _potentialSC(iX+2,iY  ,iPhip) +
            Wxm * Wy0 * _potentialSC(iX  ,iY+1,iPhip) +
            Wx0 * Wy0 * _potentialSC(iX+1,iY+1,iPhip) +
            Wxp * Wy0 * _potentialSC(iX+2,iY+1,iPhip) +
            Wxm * Wyp * _potentialSC(iX  ,iY+2,iPhip) +
            Wx0 * Wyp * _potentialSC(iX+1,iY+2,iPhip) +
            Wxp * Wyp * _potentialSC(iX+2,iY+2,iPhip)
            );

     mp._xp(j)   += CoefT * kickX;
     mp._yp(j)   += CoefT * kickY;
     mp._dp_p(j) += CoefL * kickE;
     mp._deltaE(j) = mp._dp_p(j) / mp._syncPart._dppFac;
  }
}


///////////////////////////////////////////////////////////////////////////
//
// Static MEMBER FUNCTIONS FOR CLASS SpCh3D_FFT
//
///////////////////////////////////////////////////////////////////////////

     rfftwnd_plan SpCh3D_FFT::_planForward,
                  SpCh3D_FFT::_planBackward;
     FFTW_REAL *SpCh3D_FFT::_in;
     FFTW_COMPLEX *SpCh3D_FFT::_out1;
     FFTW_COMPLEX *SpCh3D_FFT::_out2;
     FFTW_COMPLEX *SpCh3D_FFT::_out;
     Matrix(Real) SpCh3D_FFT::_GreensF; 
     Matrix(Real) SpCh3D_FFT::_rho;
     Integer SpCh3D_FFT::_initialized=0;

     //inserted by shishlo =====START=====
     Real SpCh3D_FFT::_dx_static = 0.0;
     Real SpCh3D_FFT::_dy_static = 0.0;
     //inserted by shishlo =====STOP======

     //debug ===shishlo === start ==
     Integer SpCh3D_FFT::_constructor_counter = 0;
     //debug ===shishlo === stop ==

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS SpCh3D_FFT
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(SpCh3D_FFT, SpCh3D);

   SpCh3D_FFT::SpCh3D_FFT(const String &n, const Integer &order,
            Integer &nXBins, Integer &nYBins, Integer &nPhiBins,
            Real &eps, Real &length,
            const String &BPShape, Real &BP1,
            Real &BP2, Real &BP3, Real &BP4,
            Integer &BPPoints, Integer &BPModes,
            Real &Gridfact,
            const Integer &nMacrosMin) :
            SpCh3D(n, order, nXBins, nYBins, nPhiBins, length, nMacrosMin, eps)
   {

     //debug ===shishlo === start ==
       _constructor_counter++;
     //      cout << "SpCh3D_FFT::SpCh3D_FFT ==CONSTRUCTOR== NmacrosMin ="
     //           << nMacrosMin << " constructor counter = "
     //           << _constructor_counter << "\n";
     /*
             if(_constructor_counter == 1)
             {
               cout << "SpCh3D_FFT::SpCh3D_FFT ==CONSTRUCTOR== \n";
	       cout << "parameters: \n"
                    << "nXBins nYBins nPhiBins   = " << nXBins  << " "
                    << nYBins << " " << nPhiBins << "\n"
                    << "BPModes BPPoints BPShape = " << BPModes << " "
                    << BPPoints << " " << BPShape << "\n"
                    << "Gridfact = " << Gridfact <<"\n";
             }
     */
     //debug ===shishlo === stop ==

             _xGrid.resize(_nXBins+1);
             _yGrid.resize(_nYBins+1);
             _phiGrid.resize(_nPhiBins+1);
             _potentialSC.resize(_nXBins+1, _nYBins+1, _nPhiBins+1);
             _rho3D.resize(_nXBins+1, _nYBins+1, _nPhiBins+1);
             _phisc.resize(_nXBins+1, _nYBins+1);
             _fscx.resize(_nXBins+1, _nYBins+1);
             _fscy.resize(_nXBins+1, _nYBins+1);

	     _BPShape = BPShape;
             _BPPoints = BPPoints;
             if (_BPShape == "Rectangle") _BPPoints = 4*(BPPoints/4);
             _BPModes  = BPModes;
             _Gridfact = Gridfact;
             _BPx.resize(_BPPoints);
             _BPy.resize(_BPPoints);
             _BPPhi.resize(_BPPoints);
             _BPPhiH.resize(_BPPoints,2*_BPModes+1);
             _BPH.resize(2*_BPModes+1);

             _iXmin = Integer(Real(_nXBins) * (1. - 1./_Gridfact) / 2.) - 1;
             if (_iXmin < 1) _iXmin = 1;
             _iXmax = _nXBins + 2 - _iXmin;
             if (_iXmax > _nXBins) _iXmax = _nXBins;
             _iYmin = Integer(Real(_nYBins) * (1. - 1./_Gridfact) / 2.) - 1;
             if (_iYmin < 1) _iYmin = 1;
             _iYmax = _nYBins + 2 - _iYmin;
             if (_iYmax > _nYBins) _iYmax = _nYBins;

	     //inserted by shishlo =====START===== 08.16.01
               _phiGridMin = -Consts::pi;
               _phiGridMax =  Consts::pi;
	     //inserted by shishlo =====STOP====== 08.16.01


     //debug ===shishlo === start ==
     /*
             if(_constructor_counter == 1)
             {
	       cout << "_iXmin  _iXmax  = " << _iXmin << " " << _iXmax
                    << "\n" << "_iYmin  _iYmax  = " << _iYmin
                    << " " << _iYmax << "\n";
             }
     */
     //debug ===shishlo === stop ==



 
             Integer i, j, BPPO4, i1, i2, i3, i4, jc, js;
             Real r, theta, dtheta, rj, rsq, dx, dy, rfac;
             Vector(Real) th;
             th.resize(_BPPoints);

             if (_BPShape == "Circle")
	     {
	       _BPr = BP1;
               _BPrnorm = _BPr;
               dtheta = 2. * Consts::pi / _BPPoints;
               for (i = 1; i <= _BPPoints; i++)
	       {
		 th(i) = (i-1) * dtheta;
                 _BPx(i) = _BPr * cos(th(i));
                 _BPy(i) = _BPr * sin(th(i));
	       }
	     }

             if (_BPShape == "Ellipse")
	     {
	       _BPa = BP1;
	       _BPb = BP2;
               rsq = _BPa*_BPb;
               _BPrnorm = pow(rsq, 0.5);
               BPPO4 = _BPPoints/4;

               Real ds = 2. * Consts::pi * _BPrnorm / _BPPoints;
               Real denom;
               Real resid = 1.0;

               while (resid > 1.0e-08 || resid < -1.0e-08)
	       {
                  denom = _BPb;
                  dtheta = ds / denom;
                  theta = dtheta / 2.;
                  th(1) = 0.;
                  for (i = 1; i <= BPPO4; i++)
	          {
                     denom = _BPb*_BPb
                          + (_BPa*_BPa-_BPb*_BPb)*sin(theta)*sin(theta);
                     denom = pow(denom, 0.5);
                     dtheta = ds / denom;
                     th(i+1) = th(i) + dtheta;
                     theta += dtheta;
	          }
                  resid = th(1+BPPO4) - Consts::pi / 2.;
                  ds *= Consts::pi / (2.*th(1+BPPO4));
	       }
               /*
               cout << "Resid = " << resid << "   "
                    << th(1+BPPO4) << "\n\n";
	       */

               for (i = 1; i <= BPPO4; i++)
	       {
                 i1 = i;
                 i2 = 2*BPPO4+2 - i1;
		 th(i2) = Consts::pi - th(i1);
	       }

               for (i = 1; i <= 2*BPPO4; i++)
	       {
                 i1 = i;
                 i3 = 2*BPPO4 + i1;
		 th(i3) = Consts::pi + th(i1);
	       }
               
               for (i = 1; i <= _BPPoints; i++)
	       {
                 _BPx(i) = _BPa * cos(th(i));
                 _BPy(i) = _BPb * sin(th(i));
                 th(i) = atan2(_BPy(i),_BPx(i));
                 /*
                 cout << i << "  " << th(i) << "  "
                      << _BPx(i) << "  " << _BPy(i) << "\n";
		 */
	       }
               //cout << "\n";
	     }

             if (_BPShape == "Rectangle")
	     {
               _BPxMin = BP1;
               _BPxMax = BP2;
               _BPyMin = BP3;
               _BPyMax = BP4;
               rsq = ((_BPxMax - _BPxMin) * (_BPxMax - _BPxMin) +
                      (_BPyMax - _BPyMin) * (_BPyMax - _BPyMin)) / 4.;
               _BPrnorm = pow(rsq, 0.5);
               BPPO4 = _BPPoints/4;
               dx = (_BPxMax - _BPxMin) / BPPO4;
               dy = (_BPyMax - _BPyMin) / BPPO4;
               for (i = 1; i <= BPPO4; i++)
	       {
                 i1 = i;
	         _BPx(i1) = _BPxMax;
                 _BPy(i1) = _BPyMin + (i-1) * dy;
                 th(i1) = atan2(_BPy(i1),_BPx(i1));

                 i2 = BPPO4 + i1;
	         _BPx(i2) = _BPxMax - (i-1) * dx;
                 _BPy(i2) = _BPyMax;
                 th(i2) = atan2(_BPy(i2),_BPx(i2));

                 i3 = BPPO4 + i2;
	         _BPx(i3) = _BPxMin;
                 _BPy(i3) = _BPyMax - (i-1) * dy;
                 th(i3) = atan2(_BPy(i3),_BPx(i3));

                 i4 = BPPO4 + i3;
	         _BPx(i4) = _BPxMin + (i-1) * dx;
                 _BPy(i4) = _BPyMin;
                 th(i4) = atan2(_BPy(i4),_BPx(i4));
	       }
	     }

             for (i = 1; i <= _BPPoints; i++)
	     {
                rsq = _BPx(i)*_BPx(i) + _BPy(i)*_BPy(i);
                r = pow(rsq,0.5);
                rfac = r / _BPrnorm;
                rj = 1;
                _BPPhiH(i,1) = 1.0;
                for (j = 1; j <= _BPModes; j++)
	        {
                   jc = 2 * j;
                   js = 2 * j + 1;
                   rj *= rfac;
                  _BPPhiH(i,jc) = rj * cos(j*th(i));
                  _BPPhiH(i,js) = rj * sin(j*th(i));
	        }
	     }

	     //inserted by shishlo =====START=====
             // 3D SC can be used only with proper boundary specification
             if( _BPShape != "Circle" && _BPShape != "Ellipse" &&
                 _BPShape != "Rectangle")
             {
	       cout << "SpCh3D_FFT::SpCh3D_FFT ==CONSTRUCTOR=    STOP \n";
               cout << "You should define BPShape \n";
               cout << "STOP. \n";
               exit(1);
	     }
	     //inserted by shishlo =====STOP======

          if(_initialized) return;

	  // Use this memory allocation scheme as per FFTW manual. Also
          // g++ chokes on new with arguments here (not everywhere!)

                _in = (FFTW_REAL *) 
                   new char[_nXBins * _nYBins
                                    * sizeof(FFTW_REAL)];
                _out1 =(FFTW_COMPLEX *) 
                   new char[_nXBins * (_nYBins/2+1)
                                    * sizeof(FFTW_COMPLEX)];
                _out2 =(FFTW_COMPLEX *) 
                   new char[_nXBins * (_nYBins/2+1)
                                    * sizeof(FFTW_COMPLEX)];
                _out =(FFTW_COMPLEX *) 
                   new char[_nXBins * (_nYBins/2+1)
                                    * sizeof(FFTW_COMPLEX)];

               _planForward = rfftw2d_create_plan(_nXBins, _nYBins, 
                         FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE);
               _planBackward = rfftw2d_create_plan(_nXBins, _nYBins, 
                         FFTW_COMPLEX_TO_REAL, FFTW_ESTIMATE);
               _GreensF.resize(_nXBins, _nYBins);
               _rho.resize(_nXBins, _nYBins);
               _initialized = 1;
   }

   SpCh3D_FFT::~SpCh3D_FFT()
   { 
       if(_in)
	 {
          rfftwnd_destroy_plan(_planForward);
          rfftwnd_destroy_plan(_planBackward);
          delete [] _in;
          delete [] _out1;
          delete [] _out2;
          delete [] _out;
	 }
   }   

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    SpCh3D_FFT::NodeCalculator
//
// DESCRIPTION
//    Bins the macro particles, calculates the 3-D density profile,
//    the 3-D potential with conducting wall boundary conditions on
//    the beam pipe, and the 3-D forces.
//    Presently assumes a uniform rectangular grid.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void SpCh3D_FFT::_nodeCalculator(MacroPart &mp)
{

    Integer i, j, index, iX, iY, iPhi;
    Real epsSq, rTransX, rTransY, rTot2;

    //debug ===shishlo === start ==
    /*
    cout << "SpCh3D_FFT::_nodeCalculator NmacrosMin and nMacro ="
         << _nMacrosMin << " " << mp._nMacros << "\n";
    */
    //debug ===shishlo === stop ==

  // test to see if this is not the main herd:

    if(mp._feelsHerds == 1)  // just bin the particles, use existing forces:
    {
        _fullBin(mp);
        return;
    }

    if(mp._nMacros < _nMacrosMin && !Parallel::parallelRun) 
    {
      mp._globalNMacros = mp._nMacros;
      return;  
    }
    
  // Find particle grid extent:

    _GridExtent(mp);

  // Define the grid:

    //inserted by shishlo =====START=====
    Integer grid_initialized = 0;
    //inserted by shishlo =====STOP======

    _dx = (_xGridMax - _xGridMin) / Real(_nXBins);
    _dy = (_yGridMax - _yGridMin) / Real(_nYBins);

    //inserted by shishlo =====START=====
    if(_dx == _dx_static && _dy == _dy_static) grid_initialized = 1;
    //inserted by shishlo =====STOP======

    _dphi = (_phiGridMax - _phiGridMin) / Real(_nPhiBins);

    //inserted by shishlo =====START=====
    if(grid_initialized != 1)
    {
    //inserted by shishlo =====STOP======
      for (iX = 1; iX <= _nXBins+1; iX++)
        _xGrid(iX) = _xGridMin + Real(iX-1) * _dx;
      for (iY = 1; iY <= _nYBins+1; iY++)
        _yGrid(iY) = _yGridMin + Real(iY-1) * _dy;
    //inserted by shishlo =====START=====
    }
    //inserted by shishlo =====STOP======

    for (iPhi = 1; iPhi <= _nPhiBins+1; iPhi++)
      _phiGrid(iPhi) = _phiGridMin + Real(iPhi-1) * _dphi;

    // Bin the particles:

    _fullBin(mp);

   // Calculate the Greens funtion grid FFT

   Real denom = 1. / (Real(_nXBins) * Real(_nYBins));

   //inserted by shishlo =====START=====
   if(grid_initialized != 1)
   {
   //inserted by shishlo =====STOP======

     if(_eps <  0.)
       epsSq = Sqr(_eps);
     else
       epsSq = Sqr(_eps) * _dx * _dy;

     for (iY = 1; iY <= _nYBins/2+1; iY++)
     {
       rTransY = (iY-1) * _dy;
       for (iX = 1; iX <= _nXBins/2+1; iX++)
       {
         rTransX = (iX-1) * _dx;
//       rTot2 = rTransX*rTransX + rTransY*rTransY + epsSq;
         rTot2 = rTransX*rTransX + rTransY*rTransY;
         _GreensF(iX, iY) = 0.0;
         if(rTot2 > 0.0)
         {
           _GreensF(iX, iY) = log(rTot2);
         }
       }
       for (iX = _nXBins/2+2; iX <= _nXBins; iX++)
       {
         _GreensF(iX, iY) = _GreensF(_nXBins+2-iX, iY);
       }
     }
     for (iY = _nYBins/2+2; iY <=_nYBins; iY++)
     {
       for (iX = 1; iX <= _nXBins; iX++)
       {
         _GreensF(iX, iY) = _GreensF(iX, _nYBins+2-iY);
       }
     }

     //   Calculate the FFT of the Greens Function:

     for (i = 0; i < _nXBins; i++)
     for (j = 0; j < _nYBins; j++)
     {
       _in[j + _nYBins*i] = _GreensF(i+1, j+1);
     }

     rfftwnd_one_real_to_complex(_planForward, _in, _out2);

     //inserted by shishlo =====START=====
     //cout << "GREEN function calculated!!!! \n";
   }
   //inserted by shishlo =====STOP======

   //   Calculate the FFT of the binned charge distribution:

   _potentialSC = 0.;

   for (iPhi = 1; iPhi <= _nPhiBins+1; iPhi++)
   {

     for (i = 0; i < _nXBins; i++)
     for (j = 0; j < _nYBins; j++)
     {
        _in[j + _nYBins*i] = _rho3D(i+1, j+1, iPhi);
     }

     rfftwnd_one_real_to_complex(_planForward, _in, _out1);

     // Do Convolution:

     for (i = 0; i < _nXBins; i++)
     for (j = 0; j < _nYBins/2+1; j++)
     {
        index = j + (_nYBins/2+1)*i;
        c_re(_out[index]) = c_re(_out1[index])*c_re(_out2[index]) -
                            c_im(_out1[index])*c_im(_out2[index]);
        c_im(_out[index]) = c_re(_out1[index])*c_im(_out2[index]) +
                            c_im(_out1[index])*c_re(_out2[index]);
     }

     // Do Inverse FFT to get the Potential:

     rfftwnd_one_complex_to_real(_planBackward, _out, _in);

     _phisc = 0.;

     for (iX = _iXmin; iX <= _iXmax; iX++)
     for (iY = _iYmin; iY <= _iYmax; iY++)
     {
        index = iY-1 + _nYBins * (iX-1);
        _phisc(iX, iY) = _in[index] * denom;
     }

     // Now satisfy conducting wall boundary conditions on the beampipe.

     _LSQBP();

     for (iX = 1; iX <= _nXBins; iX++)
     for (iY = 1; iY <= _nYBins; iY++)
     {
       _potentialSC(iX, iY, iPhi) = _phisc(iX, iY);
     }
   }

   //inserted by shishlo =====START=====
   _dx_static = _dx;
   _dy_static = _dy;
   //inserted by shishlo =====STOP======
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//     SpCh3D_FFT::_updatePartAtNode
//
// DESCRIPTION
//     Calls the specified local calculator for an operation on
//     a MacroParticle with an  FFT 3D Spacecharge.
//     The transverse kick is
//     added to each macro particle here.
//
// PARAMETERS
//   MacroPart herd
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void  SpCh3D_FFT::_updatePartAtNode(MacroPart &mp)
{
  // Average kick interpolated from bin location:
  // 9 point transverse weight scheme for the potential
  // 3 point longitudinal scheme
  // then the differentiation of the potentials

  Real kickX, kickY, kickE, CoefT;
  Real CoefL;
  Real _lambda;
  Real rClassical = 1.534698e-18; // Classical p radius

  SyncPart *sp = SyncPart::safeCast
                 (syncP((Particles::syncPart & Particles::All_Mask)-1));

  //debug ===shishlo === start ==
  //cout << "SpCh3D_FFT::_updatePartAtNode START!!! \n";
  //cout << "             nMacros in the herd = " << mp._nMacros << "\n";

  if(mp._nMacros < _nMacrosMin && !Parallel::parallelRun) 
  {
    return;  
  }
  //debug ===shishlo === stop ==

  // The coefficients for the kicks

  _lambda = Injection::nReals_Macro *
            Ring::harmonicNumber * Consts::twoPi
            / (Ring::lRing * _dphi);

  CoefL = Sqr(sp->_charge) * _lambda * rClassical * _lkick/
          (sp->_mass * Sqr(sp->_betaSync) * Cube(sp->_gammaSync));

  CoefT = -1000. * 1000. * CoefL;

  Integer iPhim, iPhi, iPhip;

  Integer j;
  for (j = 1; j <= mp._nMacros; j++)
  {
    //inserted by shishlo =====START=====
    if( mp._xBin(j) == -1)
    {
      continue;
    }
    //inserted by shishlo =====STOP======

    iPhi = mp._LPositionIndex(j);
    if((iPhi > _nPhiBins) || (iPhi < 1)) iPhi = 1;
    iPhip = iPhi + 1;
    iPhim = iPhi - 1;
    if(iPhi == _nPhiBins) iPhip = 1;
    if(iPhi == 1) iPhim = _nPhiBins;
    if(_nPhiBins < 2)
    {
      iPhi = 1;
      iPhim = 1;
      iPhip = 1;
    }

    Integer iX, iY;
    iX = mp._xBin(j);
    iY = mp._yBin(j);
    Real xFract = mp._xFractBin(j);
    Real yFract = mp._yFractBin(j);
    Real phiFract = mp._fractLPosition(j);

    // TSC binning, see Hockney and Eastwood:

    Real Wxm = 0.5 * (0.5 - xFract)
                   * (0.5 - xFract);
    Real Wx0 = 0.75 - xFract * xFract;
    Real Wxp = 0.5 * (0.5 + xFract)
	           * (0.5 + xFract);
    Real Wym = 0.5 * (0.5 - yFract)
	           * (0.5 - yFract);
    Real Wy0 = 0.75 - yFract * yFract;
    Real Wyp = 0.5 * (0.5 + yFract)
                   * (0.5 + yFract);
    Real Wphim = 0.5 * (0.5 - phiFract)
                     * (0.5 - phiFract);
    Real Wphi0 = 0.75 - phiFract * phiFract;
    Real Wphip = 0.5 * (0.5 + phiFract)
	             * (0.5 + phiFract);

    Real dWxm = (0.5 - xFract) / _dx;
    Real dWx0 = 2. * xFract / _dx;
    Real dWxp = -(0.5 + xFract) / _dx;
    Real dWym = (0.5 - yFract) / _dy;
    Real dWy0 = 2. * yFract / _dy;
    Real dWyp = -(0.5 + yFract) / _dy;
    Real dWphim = (0.5 - phiFract) * Ring::harmonicNumber * Consts::twoPi
                  / (_dphi * Ring::lRing);
    Real dWphi0 = 2. * phiFract * Ring::harmonicNumber * Consts::twoPi
                  / (_dphi * Ring::lRing);
    Real dWphip = -(0.5 + phiFract) * Ring::harmonicNumber * Consts::twoPi
                  / (_dphi * Ring::lRing);

    kickX = Wphim *
            (
            dWxm * Wym * _potentialSC(iX-1,iY-1,iPhim) +
            dWx0 * Wym * _potentialSC(iX  ,iY-1,iPhim) +
            dWxp * Wym * _potentialSC(iX+1,iY-1,iPhim) +
            dWxm * Wy0 * _potentialSC(iX-1,iY  ,iPhim) +
            dWx0 * Wy0 * _potentialSC(iX  ,iY  ,iPhim) +
            dWxp * Wy0 * _potentialSC(iX+1,iY  ,iPhim) +
            dWxm * Wyp * _potentialSC(iX-1,iY+1,iPhim) +
            dWx0 * Wyp * _potentialSC(iX  ,iY+1,iPhim) +
            dWxp * Wyp * _potentialSC(iX+1,iY+1,iPhim)
            )
            +
            Wphi0 *
            (
            dWxm * Wym * _potentialSC(iX-1,iY-1, iPhi) +
            dWx0 * Wym * _potentialSC(iX  ,iY-1, iPhi) +
            dWxp * Wym * _potentialSC(iX+1,iY-1, iPhi) +
            dWxm * Wy0 * _potentialSC(iX-1,iY  , iPhi) +
            dWx0 * Wy0 * _potentialSC(iX  ,iY  , iPhi) +
            dWxp * Wy0 * _potentialSC(iX+1,iY  , iPhi) +
            dWxm * Wyp * _potentialSC(iX-1,iY+1, iPhi) +
            dWx0 * Wyp * _potentialSC(iX  ,iY+1, iPhi) +
            dWxp * Wyp * _potentialSC(iX+1,iY+1, iPhi)
            )
            +
            Wphip *
            (
            dWxm * Wym * _potentialSC(iX-1,iY-1,iPhip) +
            dWx0 * Wym * _potentialSC(iX  ,iY-1,iPhip) +
            dWxp * Wym * _potentialSC(iX+1,iY-1,iPhip) +
            dWxm * Wy0 * _potentialSC(iX-1,iY  ,iPhip) +
            dWx0 * Wy0 * _potentialSC(iX  ,iY  ,iPhip) +
            dWxp * Wy0 * _potentialSC(iX+1,iY  ,iPhip) +
            dWxm * Wyp * _potentialSC(iX-1,iY+1,iPhip) +
            dWx0 * Wyp * _potentialSC(iX  ,iY+1,iPhip) +
            dWxp * Wyp * _potentialSC(iX+1,iY+1,iPhip)
            );

    kickY = Wphim *
            (
            Wxm * dWym * _potentialSC(iX-1,iY-1,iPhim) +
            Wx0 * dWym * _potentialSC(iX  ,iY-1,iPhim) +
            Wxp * dWym * _potentialSC(iX+1,iY-1,iPhim) +
            Wxm * dWy0 * _potentialSC(iX-1,iY  ,iPhim) +
            Wx0 * dWy0 * _potentialSC(iX  ,iY  ,iPhim) +
            Wxp * dWy0 * _potentialSC(iX+1,iY  ,iPhim) +
            Wxm * dWyp * _potentialSC(iX-1,iY+1,iPhim) +
            Wx0 * dWyp * _potentialSC(iX  ,iY+1,iPhim) +
            Wxp * dWyp * _potentialSC(iX+1,iY+1,iPhim)
            )
            +
            Wphi0 *
            (
            Wxm * dWym * _potentialSC(iX-1,iY-1, iPhi) +
            Wx0 * dWym * _potentialSC(iX  ,iY-1, iPhi) +
            Wxp * dWym * _potentialSC(iX+1,iY-1, iPhi) +
            Wxm * dWy0 * _potentialSC(iX-1,iY  , iPhi) +
            Wx0 * dWy0 * _potentialSC(iX  ,iY  , iPhi) +
            Wxp * dWy0 * _potentialSC(iX+1,iY  , iPhi) +
            Wxm * dWyp * _potentialSC(iX-1,iY+1, iPhi) +
            Wx0 * dWyp * _potentialSC(iX  ,iY+1, iPhi) +
            Wxp * dWyp * _potentialSC(iX+1,iY+1, iPhi)
            )
            +
            Wphip *
            (
            Wxm * dWym * _potentialSC(iX-1,iY-1,iPhip) +
            Wx0 * dWym * _potentialSC(iX  ,iY-1,iPhip) +
            Wxp * dWym * _potentialSC(iX+1,iY-1,iPhip) +
            Wxm * dWy0 * _potentialSC(iX-1,iY  ,iPhip) +
            Wx0 * dWy0 * _potentialSC(iX  ,iY  ,iPhip) +
            Wxp * dWy0 * _potentialSC(iX+1,iY  ,iPhip) +
            Wxm * dWyp * _potentialSC(iX-1,iY+1,iPhip) +
            Wx0 * dWyp * _potentialSC(iX  ,iY+1,iPhip) +
            Wxp * dWyp * _potentialSC(iX+1,iY+1,iPhip)
            );

    kickE = dWphim *
            (
            Wxm * Wym * _potentialSC(iX-1,iY-1,iPhim) +
            Wx0 * Wym * _potentialSC(iX  ,iY-1,iPhim) +
            Wxp * Wym * _potentialSC(iX+1,iY-1,iPhim) +
            Wxm * Wy0 * _potentialSC(iX-1,iY  ,iPhim) +
            Wx0 * Wy0 * _potentialSC(iX  ,iY  ,iPhim) +
            Wxp * Wy0 * _potentialSC(iX+1,iY  ,iPhim) +
            Wxm * Wyp * _potentialSC(iX-1,iY+1,iPhim) +
            Wx0 * Wyp * _potentialSC(iX  ,iY+1,iPhim) +
            Wxp * Wyp * _potentialSC(iX+1,iY+1,iPhim)
            )
            +
            dWphi0 *
            (
            Wxm * Wym * _potentialSC(iX-1,iY-1, iPhi) +
            Wx0 * Wym * _potentialSC(iX  ,iY-1, iPhi) +
            Wxp * Wym * _potentialSC(iX+1,iY-1, iPhi) +
            Wxm * Wy0 * _potentialSC(iX-1,iY  , iPhi) +
            Wx0 * Wy0 * _potentialSC(iX  ,iY  , iPhi) +
            Wxp * Wy0 * _potentialSC(iX+1,iY  , iPhi) +
            Wxm * Wyp * _potentialSC(iX-1,iY+1, iPhi) +
            Wx0 * Wyp * _potentialSC(iX  ,iY+1, iPhi) +
            Wxp * Wyp * _potentialSC(iX+1,iY+1, iPhi)
            )
            +
            dWphip *
            (
            Wxm * Wym * _potentialSC(iX-1,iY-1,iPhip) +
            Wx0 * Wym * _potentialSC(iX  ,iY-1,iPhip) +
            Wxp * Wym * _potentialSC(iX+1,iY-1,iPhip) +
            Wxm * Wy0 * _potentialSC(iX-1,iY  ,iPhip) +
            Wx0 * Wy0 * _potentialSC(iX  ,iY  ,iPhip) +
            Wxp * Wy0 * _potentialSC(iX+1,iY  ,iPhip) +
            Wxm * Wyp * _potentialSC(iX-1,iY+1,iPhip) +
            Wx0 * Wyp * _potentialSC(iX  ,iY+1,iPhip) +
            Wxp * Wyp * _potentialSC(iX+1,iY+1,iPhip)
            );

     mp._xp(j)   += CoefT * kickX;
     mp._yp(j)   += CoefT * kickY;
     mp._dp_p(j) += CoefL * kickE;
     mp._deltaE(j) = mp._dp_p(j) / mp._syncPart._dppFac;

     /*
     cout << "debug delta px py dp_p  = " << CoefT * kickX
          << " " << CoefT * kickY << " " << CoefL*kickE << "\n ";
     */

  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    SpCh3D_FFT::_GridExtent
//
// DESCRIPTION
//    Finds the extent of the grid using the particle distribution and
//    the boundary parameters.
//
///////////////////////////////////////////////////////////////////////////

Void SpCh3D_FFT::_GridExtent(MacroPart &mp)
{
   mp._findXYPExtrema();

   mp._globalNMacros = mp._nMacros;
   if( mp._globalNMacros < _nMacrosMin) return;

 // Define grid extrema (for transverse grid consider beam pipe
 //                      and double the size
 //                      for longitudinal grid make sure closest grid point to
 //                      particles is interior point:

  //inserted by shishlo =====START===== 08.16.01
  //if the new longitudinal grid limits is changed 
  //more than by 5% they should be recalculated

   //cout.precision(15);
   //cout.width(15); 
   //cout << "debug mp._phiMin mp._phiMax = " << mp._phiMin
   //     << " " <<  mp._phiMax << "\n";
   

   Real phiGridMin, phiGridMax, phiGridWidth;
   Real dExtra;
   dExtra = 0.;
   if(_nPhiBins > 2) 
     dExtra = (mp._phiMax - mp._phiMin) / Real(_nPhiBins - 2);
   phiGridMin = mp._phiMin - 2.0*dExtra;
   phiGridMax = mp._phiMax + 2.0*dExtra;

   phiGridWidth = _phiGridMax - _phiGridMin;

   Real percent_range = 0.05;

   if(phiGridMin < _phiGridMin || 
      phiGridMax > _phiGridMax ||  
      phiGridMax < (_phiGridMax - percent_range*phiGridWidth) ||
      phiGridMin > (_phiGridMin + percent_range*phiGridWidth))
   {
      phiGridWidth = phiGridMax - phiGridMin;
      _phiGridMin  = phiGridMin - 0.5*percent_range*phiGridWidth; 
      _phiGridMax  = phiGridMax + 0.5*percent_range*phiGridWidth;
   }

  //inserted by shishlo =====START===== 08.16.01

   if (_phiGridMin < -Consts::pi) _phiGridMin = -Consts::pi;
   if (_phiGridMax > Consts::pi) _phiGridMax = Consts::pi;
  
   //cout << "debug _phiGridMin _phiGridMax = " << _phiGridMin
   //     << " " << _phiGridMax << "\n";

   if (_BPShape == "Circle")
   {
       _xGridMin = -_BPr;
       _xGridMax = _BPr;
       _yGridMin = -_BPr;
       _yGridMax = _BPr;
   }

   if (_BPShape == "Ellipse")
   {
       _xGridMin = -_BPa;
       _xGridMax = _BPa;
       _yGridMin = -_BPb;
       _yGridMax = _BPb;
   }

   if (_BPShape == "Rectangle")
   {
       _xGridMin = _BPxMin;
       _xGridMax = _BPxMax;
       _yGridMin = _BPyMin;
       _yGridMax = _BPyMax;
   }

   _xGridMin = _xGridMin * _Gridfact;
   _xGridMax = _xGridMax * _Gridfact;
   _yGridMin = _yGridMin * _Gridfact;
   _yGridMax = _yGridMax * _Gridfact;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   SpCh3D_FFT::_fullBin
//
// DESCRIPTION
//   Bins the macroparticles in all 3 dimensions
//
// PARAMETERS
//   mp - the macro herd to operate on.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void SpCh3D_FFT::_fullBin(MacroPart &mp)
{
 
  Integer i, j, iX, iY, iPhi, iPhim, iPhip;

  //inserted by shishlo =====START=====
  Real x,y;
  Integer i_bad;
  //inserted by shishlo =====STOP======


  // Bin the particles:

    _rho3D = 0.;

    for(i=1; i <= mp._nMacros; i++)
    {

      //inserted by shishlo =====START=====
      x = mp._x(i);
      y = mp._y(i);
      i_bad = 0;
      if( _BPShape == "Circle")
      {
	if((x*x + y*y) >= _BPr*_BPr) i_bad = 1;
      }
      if( _BPShape == "Rectangle")
      {
	if((x <= _BPxMin) || (x >= _BPxMax) ||
           (y <= _BPyMin) || (y >= _BPyMax)) i_bad = 1;
      }      
      if( _BPShape == "Ellipse")
      {
	if((x/_BPa*x/_BPa + y/_BPb*y/_BPb) >= 1.0) i_bad = 1;
      }
      if(i_bad != 0)
      {
        mp._xBin(i) = -1;
        continue;
      }
      //inserted by shishlo =====STOP======

      iX = 1 + Integer((mp._x(i) - _xGridMin)/_dx + 0.5);
      iY = 1 + Integer((mp._y(i) - _yGridMin)/_dy + 0.5);
      iPhi = 1 + Integer((mp._phi(i) - _phiGridMin)/_dphi + 0.5);

      if(iX < _iXmin+1) iX = _iXmin+1;
      if(iX > _iXmax-1) iX = _iXmax-1;
      if(iY < _iYmin+1) iY = _iYmin+1;
      if(iY > _iYmax-1) iY = _iYmax-1;
      if(iPhi < 2) iPhi = 1;
      if(iPhi > _nPhiBins) iPhi = _nPhiBins + 1;
      iPhim = iPhi - 1;
      iPhip = iPhi + 1;
      if(iPhi == _nPhiBins + 1) iPhip = 2;
      if(iPhi == 1) iPhim = _nPhiBins;

      mp._xBin(i) = iX;
      mp._yBin(i) = iY;
      mp._LPositionIndex(i) = iPhi;

      mp._xFractBin(i) = (mp._x(i) - _xGrid(iX)) / _dx;
      mp._yFractBin(i) = (mp._y(i) - _yGrid(iY)) / _dy;
      mp._fractLPosition(i) = (mp._phi(i) - _phiGrid(iPhi)) / _dphi;

      // TSC binning, see Hockney and Eastwood:

      Real Wxm = 0.5 * (0.5 - mp._xFractBin(i))
                     * (0.5 - mp._xFractBin(i));
      Real Wx0 = 0.75 - mp._xFractBin(i) * mp._xFractBin(i);
      Real Wxp = 0.5 * (0.5 + mp._xFractBin(i))
	             * (0.5 + mp._xFractBin(i));
      Real Wym = 0.5 * (0.5 - mp._yFractBin(i))
	             * (0.5 - mp._yFractBin(i));
      Real Wy0 = 0.75 - mp._yFractBin(i) * mp._yFractBin(i);
      Real Wyp = 0.5 * (0.5 + mp._yFractBin(i))
	             * (0.5 + mp._yFractBin(i));
      Real Wphim = 0.5 * (0.5 - mp._fractLPosition(i))
	               * (0.5 - mp._fractLPosition(i));
      Real Wphi0 = 0.75 - mp._fractLPosition(i) * mp._fractLPosition(i);
      Real Wphip = 0.5 * (0.5 + mp._fractLPosition(i))
	               * (0.5 + mp._fractLPosition(i));

     _rho3D(iX-1,iY-1,iPhim) += Wxm * Wym * Wphim;
     _rho3D(iX  ,iY-1,iPhim) += Wx0 * Wym * Wphim;
     _rho3D(iX+1,iY-1,iPhim) += Wxp * Wym * Wphim;
     _rho3D(iX-1,iY  ,iPhim) += Wxm * Wy0 * Wphim;
     _rho3D(iX  ,iY  ,iPhim) += Wx0 * Wy0 * Wphim;
     _rho3D(iX+1,iY  ,iPhim) += Wxp * Wy0 * Wphim;
     _rho3D(iX-1,iY+1,iPhim) += Wxm * Wyp * Wphim;
     _rho3D(iX  ,iY+1,iPhim) += Wx0 * Wyp * Wphim;
     _rho3D(iX+1,iY+1,iPhim) += Wxp * Wyp * Wphim;
     _rho3D(iX-1,iY-1, iPhi) += Wxm * Wym * Wphi0;
     _rho3D(iX  ,iY-1, iPhi) += Wx0 * Wym * Wphi0;
     _rho3D(iX+1,iY-1, iPhi) += Wxp * Wym * Wphi0;
     _rho3D(iX-1,iY  , iPhi) += Wxm * Wy0 * Wphi0;
     _rho3D(iX  ,iY  , iPhi) += Wx0 * Wy0 * Wphi0;
     _rho3D(iX+1,iY  , iPhi) += Wxp * Wy0 * Wphi0;
     _rho3D(iX-1,iY+1, iPhi) += Wxm * Wyp * Wphi0;
     _rho3D(iX  ,iY+1, iPhi) += Wx0 * Wyp * Wphi0;
     _rho3D(iX+1,iY+1, iPhi) += Wxp * Wyp * Wphi0;
     _rho3D(iX-1,iY-1,iPhip) += Wxm * Wym * Wphip;
     _rho3D(iX  ,iY-1,iPhip) += Wx0 * Wym * Wphip;
     _rho3D(iX+1,iY-1,iPhip) += Wxp * Wym * Wphip;
     _rho3D(iX-1,iY  ,iPhip) += Wxm * Wy0 * Wphip;
     _rho3D(iX  ,iY  ,iPhip) += Wx0 * Wy0 * Wphip;
     _rho3D(iX+1,iY  ,iPhip) += Wxp * Wy0 * Wphip;
     _rho3D(iX-1,iY+1,iPhip) += Wxm * Wyp * Wphip;
     _rho3D(iX  ,iY+1,iPhip) += Wx0 * Wyp * Wphip;
     _rho3D(iX+1,iY+1,iPhip) += Wxp * Wyp * Wphip;
   }
	
// Periodic boundary conditions
//   - levelling the last and the first node density 

   for (i=1 ; i <= _nXBins+1 ; i++)
   {
     for(j=1 ;j <= _nYBins+1 ; j++)
     {
       _rho3D(i,j,1) = _rho3D(i,j,1)+_rho3D(i,j,_nPhiBins+1);  
       _rho3D(i,j,_nPhiBins+1)= _rho3D(i,j,1);
     }
   }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    SpCh3D_FFT::_LSQBP
//
// DESCRIPTION
//    Evaluates residual on beam pipe.
//
///////////////////////////////////////////////////////////////////////////

Void SpCh3D_FFT::_LSQBP()
{
  Integer iB, j, k, index, iX, iY;
  Real xFract, yFract, Wxm, Wx0, Wxp, Wym, Wy0, Wyp;
  Matrix(Real) M;
  Vector(Real) V, W;
  Integer nparams = 2*_BPModes+1;

  for (iB = 1; iB <= _BPPoints; iB++)
  {
     iX = 1 + Integer((_BPx(iB)-_xGridMin)/_dx + 0.5);
     iY = 1 + Integer((_BPy(iB)-_yGridMin)/_dy + 0.5);

     if(iX < _iXmin+1) iX = _iXmin+1; // keep particle within mainherd grid
     if(iX > _iXmax-1) iX = _iXmax-1;
     if(iY < _iYmin+1) iY = _iYmin+1;
     if(iY > _iYmax-1) iY = _iYmax-1;

     xFract = (_BPx(iB) - _xGrid(iX))/_dx;
     yFract = (_BPy(iB) - _yGrid(iY))/_dy;

     // TSC interpolation, see Hockney and Eastwood:
      
     Real Wxm = 0.5 * (0.5 - xFract)
	            * (0.5 - xFract);
     Real Wx0 = 0.75 - xFract * xFract;
     Real Wxp = 0.5 * (0.5 + xFract)
	            * (0.5 + xFract);
     Real Wym = 0.5 * (0.5 - yFract)
	            * (0.5 - yFract);
     Real Wy0 = 0.75 - yFract * yFract;
     Real Wyp = 0.5 * (0.5 + yFract)
	            * (0.5 + yFract);
     _BPPhi(iB) = Wxm * Wym * _phisc(iX-1, iY-1) +
                  Wxm * Wy0 * _phisc(iX-1, iY) +
                  Wxm * Wyp * _phisc(iX-1, iY+1) +
                  Wx0 * Wym * _phisc(iX, iY-1) +
                  Wx0 * Wy0 * _phisc(iX, iY) +
                  Wx0 * Wyp * _phisc(iX, iY+1) +
                  Wxp * Wym * _phisc(iX+1, iY-1) +
                  Wxp * Wy0 * _phisc(iX+1, iY) +
                  Wxp * Wyp * _phisc(iX+1, iY+1);
  }

  M.resize(nparams,nparams);
  W.resize(nparams);
  V.resize(nparams);

  M = 0.;
  V = 0.;
  W = 0.;

  for (iB = 1; iB <= _BPPoints; iB++)
  {
     for (k = 1; k <= nparams; k++)
     {
        W(k) += _BPPhi(iB) * _BPPhiH(iB,k);
        for (j=1; j <= nparams; j++)
        {
	   M(j,k) += _BPPhiH(iB,j) * _BPPhiH(iB,k);
	}
     }
  }

  MathLib::solveLinear(M, W, V);

  for (k=1; k <= nparams; k++)
  {
     _BPH(k) = V(k);
  }

  _BPResid = 0.;
  for (iB = 1; iB <= _BPPoints; iB++)
  {
     for (k = 1; k <= nparams; k++)
     {
        _BPPhi(iB) -= _BPH(k) * _BPPhiH(iB,k);
     }
     _BPResid += _BPPhi(iB) * _BPPhi(iB);
  }
  _BPResid = pow(_BPResid,0.5) / _BPPoints;

  Real MaxPhi = 0.;
  for (iX = _iXmin; iX <= _iXmax; iX++)
  {
     for (iY = _iYmin; iY <= _iYmax; iY++)
     {
        Real rsq = _xGrid(iX) * _xGrid(iX) + _yGrid(iY) * _yGrid(iY);
        Real r = pow(rsq,0.5);
        Real theta = atan2(_yGrid(iY),_xGrid(iX));
        Real rk = 1.0;
        index = 1;
        Real Mode = 1.0;
        _phisc(iX,iY) -= _BPH(index) * Mode;
        for (k = 1; k<= _BPModes; k++)
        {
	   rk *= r / _BPrnorm;
           index++;
           Mode = rk * cos(k*theta);
           _phisc(iX,iY) -= _BPH(index) * Mode;
           index++;
           Mode = rk * sin(k*theta);
           _phisc(iX,iY) -= _BPH(index) * Mode;
        }
	if (MaxPhi < _phisc(iX,iY)) MaxPhi = _phisc(iX,iY);
	if (MaxPhi < -_phisc(iX,iY)) MaxPhi = -_phisc(iX,iY);
     }
  }
  if(MaxPhi != 0.0) _BPResid /= MaxPhi;
  _resid = _BPResid;

}

///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE SpaceCharge3D
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    SpaceCharge3D:ctor
//
// DESCRIPTION
//    Initializes the various Bump related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D::ctor()
{

// set some initial values

   nSpaceCharge3D = 0;
   nSpChpot3D=0;
   _nXBins = 64;
   _nYBins = 64;
   _nPhiBins = 64;
   _radVChamber = 100.;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  SpaceCharge3D::addSpCh3D_BF
//
// DESCRIPTION
//    Adds a Longitudinal Space Charge Node
//
//
// PARAMETERS
//    name:    Name for the TM 
//    order:   Order index
//    nBins    Number of bins to use
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D::addSpCh3D_BF(const String &name, const Integer &order,  
             Integer &nXBins, Integer  &nYBins, Integer  &nPhiBins,
                       Real &eps, Real &length, const Integer &nMacrosMin)

{

  if (nNodes == nodes.size())
     nodes.resize(nNodes + 100);
  if(nSpaceCharge3D == SpCh3DPointers.size())
      SpCh3DPointers.resize(nSpaceCharge3D + 100);



   nNodes++;


   _nXBins =nXBins; 
   _nYBins=nYBins;  
   _nPhiBins= nPhiBins; 
  
   nodes(nNodes-1) = new SpCh3D_BF(name, order, nXBins, nYBins, nPhiBins, length,
                            nMacrosMin, eps);
   nSpaceCharge3D ++;
   nSpChpot3D ++;
   
   
   SpCh3DPointers(nSpaceCharge3D -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}
    
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  SpaceCharge3D::addSpCh3D_BFSet
//
// DESCRIPTION
//    Adds a set of 3D Space Charge Nodes, 
//    one for every Matrix element present. 
//    Adds the set to be second order symplectic in the
//    integration. I.e., use a "central" kick over the 1/2 length
//    of the previous matrix element + 1/2 length of the present matrix
//    element.
//
// PARAMETERS
//
//    nxb       Number of X bins to use
//    nyb       Number of Y bins to use
//    eps:      The smoothing parameter to use (fraction of a bin length)
//    nMacrosMin The minimum number of macros to have before using 
//                 the kick
//
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D::addSpCh3D_BFSet(Integer &nxb, Integer &nyb, Integer &nphi, Real &eps,
                      const Integer &nMacrosMin)
{
    Node *n1;
    MapBase *tm, *tm2;
    Integer n;
    String wname;
    Real avLength, len;

    if( nTransMaps < 2) except(tooFewTransMaps);

    char num2[10];
    String cons("3DSC");
    
    // Add kick for 1st half of TM number 1:

    wname = cons+"0";   
    tm = MapBase::safeCast(tMaps(0));
    avLength = tm->_length/2.;
    addSpCh3D_BF(wname, (tm->_oindex-5), nxb, nyb, nphi, eps, avLength,
              nMacrosMin);
	      
    // Add central kicks :

    for (n=1; n<=nTransMaps-1; n++)
    {
        
        tm = MapBase::safeCast(tMaps(n-1));
        tm2 = MapBase::safeCast(tMaps(n));
        sprintf(num2, "(%d)", n); 
        wname = cons+num2;        
        avLength = ( tm->_length + tm2->_length)/2.;       
        addSpCh3D_BF(wname, (tm->_oindex+5), nxb, nyb, nphi, eps, 
          avLength, nMacrosMin);
     }

    // Add kick for last half of last TM:

    sprintf(num2, "(%d)", nTransMaps); 
     wname = cons+num2;  
    tm = MapBase::safeCast(tMaps(nTransMaps-1));
    len = tm->_length/2.;
    addSpCh3D_BF(wname, (tm->_oindex+5), nxb, nyb, nphi, eps, len,
              nMacrosMin);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  SpaceCharge3D::addSpCh3D_FFT
//
// DESCRIPTION
//    Adds a Longitudinal Space Charge Node
//
// PARAMETERS
//    name           :  Name for the TM 
//    order          :  Order index
//    n(X,Y,Phi)Bins :  Number of bins to use
//    BP...          :  Conducting wall beam pipe parameters
//    Gridfact       :  Transverse grid size compared to problem extent
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D::addSpCh3D_FFT(const String &name, const Integer &order,
                    Integer &nXBins, Integer  &nYBins, Integer &nPhiBins,
                    Real &eps, Real &length,
                    const String &BPShape, Real &BP1,
                    Real &BP2, Real &BP3, Real &BP4,
                    Integer &BPPoints, Integer &BPModes,
                    Real &Gridfact,
                    const Integer &nMacrosMin)
{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + 100);
  if(nSpaceCharge3D == SpCh3DPointers.size())
      SpCh3DPointers.resize(nSpaceCharge3D + 100);

   nNodes++;

   _nXBins =nXBins; 
   _nYBins=nYBins;  
   _nPhiBins= nPhiBins; 
  
   nodes(nNodes-1) = new SpCh3D_FFT(name, order,
                                    nXBins, nYBins, nPhiBins,
                                    eps, length,
                                    BPShape, BP1, BP2, BP3, BP4,
                                    BPPoints, BPModes, Gridfact,
                                    nMacrosMin);
   nSpaceCharge3D ++;
   nSpChpot3D ++;
 
   SpCh3DPointers(nSpaceCharge3D-1) = nodes(nNodes-1);

   nodesInitialized = 0;

}
    
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  SpaceCharge3D::addSpCh3D_FFTSet
//
// DESCRIPTION
//    Adds a set of 3D Space Charge Nodes, 
//    one for every Matrix element present. 
//    Adds the set to be second order in the
//    integration. I.e., use a "central" kick over the 1/2 length
//    of the previous matrix element + 1/2 length of the present matrix
//    element.
//
// PARAMETERS
//
//    nxb       Number of X   bins to use
//    nyb       Number of Y   bins to use
//    nphi      Number of Phi bins to use
//    eps:      The smoothing parameter to use (fraction of a bin length)
//    BP...     Conducting wall beam pipe parameters
//    Gridfact  Transverse grid size compared to problem extent
//    nMacrosMin The minimum number of macros to have before doing
//              the kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D::addSpCh3D_FFTSet(
                        Integer &nxb, Integer &nyb, Integer &nphi,
                        Real &eps,
                        const String &BPShape, Real &BP1,
                        Real &BP2, Real &BP3, Real &BP4,
                        Integer &BPPoints, Integer &BPModes,
                        Real &Gridfact,
                        const Integer &nMacrosMin)
{
    Node *n1;
    MapBase *tm, *tm2;
    Integer n;
    String wname;
    Real avLength;

    if( nTransMaps < 2) except(tooFewTransMaps);

    char num2[10];
    String cons("3DSC");
    
    // Add kick for 1st half of TM number 1:

    wname = cons+"0";   
    tm = MapBase::safeCast(tMaps(0));
    avLength = tm->_length/2.;
    addSpCh3D_FFT(wname, (tm->_oindex-5), nxb, nyb, nphi, eps, avLength,
              BPShape, BP1, BP2, BP3, BP4, BPPoints, BPModes, Gridfact,
              nMacrosMin);
	      
    // Add central kicks :

    for (n=1; n<=nTransMaps-1; n++)
    {
        
        tm = MapBase::safeCast(tMaps(n-1));
        tm2 = MapBase::safeCast(tMaps(n));
        sprintf(num2, "(%d)", n); 
        wname = cons+num2;        
        avLength = ( tm->_length + tm2->_length)/2.;       
        addSpCh3D_FFT(wname, (tm->_oindex+5), nxb, nyb, nphi,
              eps, avLength,
              BPShape, BP1, BP2, BP3, BP4, BPPoints, BPModes, Gridfact,
              nMacrosMin);
     }

    // Add kick for last half of last TM:

    sprintf(num2, "(%d)", nTransMaps); 
    wname = cons+num2;  
    tm = MapBase::safeCast(tMaps(nTransMaps-1));
    avLength = tm->_length/2.;
    addSpCh3D_FFT(wname, (tm->_oindex+5), nxb, nyb, nphi, eps, avLength,
              BPShape, BP1, BP2, BP3, BP4, BPPoints, BPModes, Gridfact,
              nMacrosMin);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  
//  SpaceCharge3D::showSpCh3D
//
// DESCRIPTION
//    Show a 3D space charge info
//
//
// PARAMETERS
//    
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D::showSpCh3D( const Integer &n, Ostream &os)

{
    Integer i;
  
    // if (n > nTImpedance) except (badTImpedance);
    SpCh3D *ti = SpCh3D::safeCast(SpCh3DPointers(n-1)); 

    os << "\t" << "Bin\t" << "Phi\t" << "Count\n";
    
    for (i=1; i<=ti->_nXBins; i++)
        os << i << "\t\t"
           << (-pi + (i-1)*twoPi/(ti->_nXBins)) ;
           
}
    
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  SpaceCharge3D::dumpPotential
//
// DESCRIPTION
//    These routines dump Potential info to vectors accesible from
//    the Shell
//
//
// PARAMETERS
//    n - the n'th L-space charge node. 
//    herd = the herd to calculate kick with.
//    ang - the longitudinal phase angle to provide the kick at (rad).
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D::dumpPotential(const Integer &n,
 const Integer &nphi, Ostream &osharmonics)
// Dump the space charge potential
{
  if (n > nSpaceCharge3D) except (badSpaceCharge3D);
  SpCh3D *fti = SpCh3D::safeCast(SpCh3DPointers(n-1));

  for (Integer i=1; i<= fti->_nXBins+3; i++)
  {
    osharmonics << setw(15)  << i ;
    for (Integer j=1; j<=fti->_nYBins+2; j++)
    {
      osharmonics << setw(15)  << SpCh3D::_potentialSC(i,j,nphi);
    }
    osharmonics << "\n";
  }  
}

Void SpaceCharge3D::dumpPhiCount( const Integer &n, Vector(Real) &p)
// Dump the binned distribution
{
}

