/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   SpaceCharge3D_MPI
//
// AUTHOR
//   Slava Danilov, Jeff Holmes, John Galambos,  ORNL, vux@ornl.gov
//   
//
// CREATED
//   07/13/2001
//
// DESCRIPTION
//   Module descriptor file for the 3D SpaceCharge module. This module contains
//   information about 3D Space Charge.The MPI parallel implementation.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module SpaceCharge3D_MPI - "SpaceCharge 3D Module."
                         - "Written by Slava Danilov and enhanced by Jeff Holmes"
{

Inherits Consts, Particles, Ring, TransMap, Injection;

Errors
  badHerdNo             - "Sorry, you asked for an undefined macro Herd",
  tooFewTransMaps       - "Sorry, you need more Transfer Matricies before you",
  badPipeShape          - "Sorry, BPShape parameters should be Circle, Ellipse, or Rectangle";


public:

  Void ctor() 
                        - "Constructor for the SpaceCharge3D_MPI module."
                        - "Initializes build values.";

  Void dtor() 
                        - "Destructor for the SpaceCharge3D_MPI module."
                        - "Finalize the SpaceCharge3D_MPI module";

  Void addSpCh3D_FFTSet_MPI(
                        Integer &nxb, Integer &nyb, Integer &nphi,
                        Real &eps,
                        String &BPShape, Real &BP1, Real &BP2,
                        Integer &BPPoints, Integer &BPModes,
                        Real &Gridfact,
                        Integer &nMacrosMin)

                        - "Adds a set of 3D Space Charge Nodes.";
 
  Void addSpCh3D_FFT_MPI(String &name, Integer &order,
                         Integer &nXBins, Integer  &nYBins, Integer &nPhiBins,
                         Real &length, Integer &nMacrosMin)

                        - "Adds a 3DSC_FFT Space Charge Node";

  Void switchLoadManagerOn()
                        - "Switches Load Manager On";

  Void switchLoadManagerOff()
                        - "Switches Load Manager Off";


//-------------debugging methods-----------start-------------------------

  Void createPartDistr(Integer &nCountLM,Integer &nSlices_total)
                        - "Debugging method."
                        - "Creates the MacroPartDistributor instance.";

  Void setMaximalNumberOfDistrParts(Integer &nMax)
                        - "Debugging method."
                        - "Defines the chunk size to avoid frequent memory allocation.";

  Void distributeParticle()
                        - "Debugging method."
                        - "Calls the MacroPartDistributor instance to distribute particles.";

  Void distributeParticleEvenly()
                        - "Debugging method."
                        - "Calls the MacroPartDistributor instance to distribute particles evenly.";


  Real rand_MPI()
                        - "Debugging method."
                        - "Generates random values in the 0 1 range.";


  Void createBoundary(Integer &xBins, Integer &yBins, 
                      Real &xSize, Real &ySize,
                      Integer &BPPoints, Integer &BPShape,
                      Integer &BPModes, Real &eps)
                        - "Debugging method."
                        - "Creates the  ORBIT_Boundary instance.";

  Void createFFT3DSpaceCharge_MPI(Integer &nXBins, Integer &nYBins, 
                                  Integer &nZBins, Integer &minBunchSize)
                        - "Debugging method."
                        - "Creates the FFT3DSpaceCharge_MPI instance.";

  Void propagateFFT3DSpaceCharge_MPI(Real &length)
                        - "Debugging method."
                        - "Propagates herd through the lattice.";

  Integer getTotalNumbOfMacros_MPI()
                        - "Debugging method."
                        - "Returns the total number of macro-particles.";

//-------------debugging methods-----------stop-------------------------


private:

  Integer
    iMPIini_       - "inf. about MPI 0-non initialized 1-initialized";
  Integer
    nMPIrank_      - "rank of this CPU";
  Integer
    nMPIsize_      - "number of CPUs for Parallel run";

}

