/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    TSpaceCharge.cc
//
// AUTHOR
//    John Galambos
//    ORNL, jdg@ornl.gov
//
// CREATED
//    5/20/98
//
//  DESCRIPTION
//    Module descriptor file for the Transverse Space Charge module.
//   This module contains source for the TSpaceCharge related info.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "TSpaceCharge.h"
#include "Node.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "ComplexMat.h"
#include "SCLTypes.h"
#include <fstream>
#include <iomanip>
#include <cmath>
#include <fftw.h>
#include <rfftw.h>
#include "mpi.h"
#include "Boundary.h"
#include "FastMults.h"
#include "SC_Ellipt.h"
#include "SC_CylCoords.h"

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////
#define FORCETAG1 33
#define FORCETAG2 34
#define GRIDTAG1 31
#define GRIDTAG2 32

Array(ObjectPtr) TSpaceChargePointers;
extern Array(ObjectPtr) mParts, nodes, tMaps;
extern Array(ObjectPtr) syncP;
Real rClassical = 1.534698e-18; // Classical p radius

// Prototypes, globals for parallel routines:

Integer syncTSCBins(Real &xGridMin, Real &xGridMax,
                 Real &yGridMin, Real &yGridMax, Real &phiMin,
                 Real &phiMax, Integer &nMacros);
Void syncTSCRho1(Integer &nPoints);
Vector(Real) globalRho, localRho;

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    TransSC
//
// INHERITANCE RELATIONSHIPS
//    TransSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a base class for transverse  space charge implementations.
//
// PUBLIC MEMBERS
//   _nMacrosMin  Minimum number of macro particles before using this node.
//   _eps         Smoothing parameter for force calculation, fraction of
//                the bin size, i.e. _eps = 1 => smooth over entire bin
//                If _eps<0, use |_eps| as absoulte smoothing length [mm]
// PIC stuff:
//   _nXBins      Reference to the number of horizontal bins.
//   _nYBins      Reference to the number of vertical bins.
//   _lkick       Longitudinal length over which kick is applied (m)
//   _xGrid       The horizontal grid values relative to the closed orbit (mm)
//   _yGrid       The vertical grid values relative to the closed orbit (mm)
//   _phisc       The potential function over the grid
//   _fscx        The horizontal space charge force over the grid
//   _fscy        The vertical space charge force over the grid
//   _lambda      The average line charge / # of real particles (1/m)
//   _perveance   The perveance of the beam (using average line charge)
//   _resid       Residual of least squares fit for beam pipe potential.
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class TransSC : public Node {
  Declare_Standard_Members(TransSC, Node);
public:
    TransSC(const String &n, const Integer &order,
                 Integer &nXBins, Integer &nYBins, Real &length,
                 const Integer &nMacrosMin, Real &eps) :
            Node(n, 0., order), _nXBins(nXBins), _nYBins(nYBins),
            _lkick(length), _nMacrosMin(nMacrosMin), _eps(eps)

        {
                _xGrid.resize(_nXBins+1);
                _yGrid.resize(_nYBins+1);
                _phisc.resize(_nXBins+1, _nYBins+1);
                _fscx.resize(_nXBins+1, _nYBins+1);
                _fscy.resize(_nXBins+1, _nYBins+1);
        }

  ~TransSC()
  {
  }
    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;

    Integer _nXBins;
    Integer _nYBins;
    Real _lkick;
    Real _eps;
    const Integer _nMacrosMin;
    Real _perveance, _lambda;
    static Matrix(Real) _phisc, _fscx, _fscy;
    static Vector(Real) _xGrid, _yGrid;
    Real _resid;
 protected:
    Real _dx, _dy;
    Real _xGridMin, _xGridMax, _yGridMin, _yGridMax;
    Real _LFactor;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TransSC
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(TransSC, Node);
Matrix(Real) TransSC::_phisc, TransSC::_fscx, TransSC::_fscy;
Vector(Real) TransSC::_xGrid, TransSC::_yGrid;
///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BFPICTransSC
//
// INHERITANCE RELATIONSHIPS
//    BFPICTransSC -> TransSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for a brute force PIC implementation of a TransSC.
//    It bins the particles in cells, but uses a brute force pair-wise
//    sum over bins to calculate the force.
//
// PUBLIC MEMBERS
//   _gridfact    Factor used to define grid extent relative to particles.
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class BFPICTransSC : public TransSC {
  Declare_Standard_Members(BFPICTransSC,TransSC);
public:
    BFPICTransSC(const String &n, const Integer &order,
                 Integer &nXBins, Integer &nYBins, Real &eps, Real &length,
                 const Integer &nMacrosMin, Real &gf) :
            TransSC(n, order, nXBins, nYBins, length, nMacrosMin, eps),
                _gridFact(gf)
        {
        }

  ~BFPICTransSC()
  {
  }
    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);

    Real _gridFact;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BFPICTransSC
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BFPICTransSC, TransSC);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BFPICTransSC::NodeCalculator
//
// DESCRIPTION
//    Bins the macro particles, and calculates the 2-D density profile.
//    Presently assumes a uniform recilinear grid.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void BFPICTransSC::_nodeCalculator(MacroPart &mp)
{

    Integer i,iX, iY, iXS, iYS;
    Real x1, x2, y1, y2;
    Matrix(Real) rho(_nXBins+1, _nYBins+1);

    if(mp._nMacros < _nMacrosMin) return;

  // Find particle extent:

    mp._findXYPExtrema();

    // Define grid extrema:

   Real dxExtra = _gridFact/2.*(mp._xMax - mp._xMin);
   Real dyExtra = _gridFact/2.*(mp._yMax - mp._yMin);
   _xGridMin = mp._xMin - dxExtra;
   _xGridMax = mp._xMax + dxExtra;
   _yGridMin = mp._yMin - dyExtra;
   _yGridMax = mp._yMax + dyExtra;

   _dx = (_xGridMax - _xGridMin)/Real(_nXBins);
   _dy = (_yGridMax - _yGridMin)/Real(_nYBins);

   // Define the grid:

   for (iX=1; iX<= _nXBins+1; iX++)
         _xGrid(iX) = _xGridMin + Real(iX-1) * _dx;

   for (iY = 1; iY<= _nYBins+1; iY++)
         _yGrid(iY) = _yGridMin + Real(iY-1) * _dy;


   // Bin the particles:


    for(i=1; i <= mp._nMacros; i++)
    {
       iX = 1 + Integer( (mp._x(i) - _xGridMin)/_dx);
       iY = 1 + Integer( (mp._y(i) - _yGridMin)/_dy);

       mp._xBin(i) = iX;
       mp._yBin(i) = iY;

       mp._xFractBin(i) = (mp._x(i) - _xGrid(iX))/_dx;
       mp._yFractBin(i) = (mp._y(i) - _yGrid(iY))/_dy;

       // Bilinear binning:

       rho(iX,iY) += (1.-mp._xFractBin(i)) * (1.-mp._yFractBin(i));
       rho(iX,iY+1) += (1.-mp._xFractBin(i)) * mp._yFractBin(i)  ;
       rho(iX+1,iY) += mp._xFractBin(i) * (1.-mp._yFractBin(i));
       rho(iX+1,iY+1) += mp._xFractBin(i) * mp._yFractBin(i);
    }


    SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart & Particles::All_Mask) - 1));


    // This peveance uses the average line density "lambda".

    _lambda = Real(mp._nMacros) * Injection::nReals_Macro * 2. * Consts::pi *
             Ring::harmonicNumber
          / (Ring::lRing * (mp._phiMax - mp._phiMin) );

    _perveance =  Sqr(sp->_charge)  *  _lambda * rClassical /
      (2. * Sqr(sp->_betaSync) * Cube(sp->_gammaSync)* sp->_mass);

    //   Calculate the force field:

    Real rTransX, rTransY, rTot2, rr;

    _fscx = 0.;
    _fscy = 0.;

    // Claculate the "force fields"
   // Streamline these loops as much as possible, this is expensive!

    Real a,b,epsSq;

    if(_eps < 0.)
       epsSq = Sqr(_eps);
     else
       epsSq = Sqr(_eps) * _dx * _dy;


    for (iX = 1; iX <=_nXBins; iX++)
      {
          a = _xGrid(iX);
      for (iY = 1; iY <= _nYBins; iY++)
        {
            b = _yGrid(iY);
         for(iXS = 1; iXS<= _nXBins; iXS++)
	   {
             rTransX = a - _xGrid(iXS);

              for(iYS = 1; iYS <= _nYBins; iYS++)
	       {
                rTransY = b - _yGrid(iYS);
                rTot2 = rTransX*rTransX + rTransY*rTransY + epsSq;
//                rTot2 = rTransX*rTransX + rTransY*rTransY;
                rr = 0.0;
                if(rTot2 > 0.0)
                {
                  rr = rho(iXS, iYS) / rTot2;
                }
                _fscx(iX,iY) += rr * rTransX;
                _fscy(iX,iY) += rr * rTransY;
               }
	   }
	}
      }

    // Include the perveance: (factor of 1.e3 to get F in MKS)

    Real factor = 4.0e3 * _perveance/mp._nMacros;

    for (iX = 1; iX <=_nXBins; iX++)
      {
      for (iY = 1; iY <= _nYBins; iY++)
        {
           _fscx(iX, iY) *= factor;
           _fscy(iX, iY) *= factor;
	}
      }

  // Set some values needed for longitudinal coefficient later:

  _LFactor = 1.;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BFPICTransSC::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with an TransSC. The Transverse space charge kick is
//    added to each macro particle here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void BFPICTransSC::_updatePartAtNode(MacroPart &mp)
{
  Real f1, f2, f3, f4, fx, fy;
  Integer j;

  //  ofstream fio("tune.out", ios::out);

    if(mp._nMacros < _nMacrosMin) return;

    for (j=1; j<= mp._nMacros; j++)
    {

    // Find horizontal force:

      f1 = _fscx(mp._xBin(j), mp._yBin(j));
      f2 = _fscx(mp._xBin(j)+1, mp._yBin(j));
      f3 = _fscx(mp._xBin(j)+1, mp._yBin(j)+1);
      f4 = _fscx(mp._xBin(j), mp._yBin(j)+1);

      fx = (1. - mp._xFractBin(j)) * (1. - mp._yFractBin(j))*f1 +
         mp._xFractBin(j) * (1. - mp._yFractBin(j))*f2 +
         (1. - mp._xFractBin(j)) * mp._yFractBin(j)*f4 +
         mp._xFractBin(j) * mp._yFractBin(j) * f3;

  // Find Vertical Force:

      f1 = _fscy(mp._xBin(j), mp._yBin(j));
      f2 = _fscy(mp._xBin(j)+1, mp._yBin(j));
      f3 = _fscy(mp._xBin(j)+1, mp._yBin(j)+1);
      f4 = _fscy(mp._xBin(j), mp._yBin(j)+1);

      fy = (1. - mp._xFractBin(j)) * (1. - mp._yFractBin(j))*f1 +
         mp._xFractBin(j) * (1. - mp._yFractBin(j))*f2 +
         (1. - mp._xFractBin(j)) * mp._yFractBin(j)*f4 +
         mp._xFractBin(j) * mp._yFractBin(j) * f3;

  // Include local line density effect, if it is available.

      if(mp._longBinningDone)  _LFactor = mp._LPosFactor(j);

      mp._xp(j) += fx * 1000. * _lkick * _LFactor;
      mp._yp(j) += fy * 1000. * _lkick * _LFactor;

      //      fio << mp._x(j) << "  "  << mp._y(j) << "  "
      //    << 1000. * fx *_LFactor*_lkick   << "  "
      //    << 1000. * fy *_LFactor*_lkick << "\n";
    }

}


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    FFTTransSC
//
// INHERITANCE RELATIONSHIPS
//    FFTTransSC -> TransSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for a brute force PIC implementation of a TransSC.
//    It bins the particles in cells, but uses an FFT implementaion
//    to calculate the force.
//
// PUBLIC MEMBERS
//
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class FFTTransSC : public TransSC {
  Declare_Standard_Members(FFTTransSC,TransSC);
public:
    FFTTransSC(const String &n, const Integer &order,
                 Integer &nXBins, Integer &nYBins, Real &eps, Real &length,
                 const Integer &nMacrosMin);
    ~FFTTransSC();
    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
 private:
    static fftwnd_plan _planForward, _planBackward;
    static FFTW_COMPLEX * _in;
    static FFTW_COMPLEX * _out1;
    static FFTW_COMPLEX * _out2;
    static Integer _initialized;
    static Matrix(Complex) _GreensF;
    static Matrix(Real) _rho;
  };

///////////////////////////////////////////////////////////////////////////
//
// Static MEMBER FUNCTIONS FOR CLASS FFTTransSC
//
///////////////////////////////////////////////////////////////////////////

     fftwnd_plan FFTTransSC::_planForward, FFTTransSC::_planBackward;
     FFTW_COMPLEX *FFTTransSC::_in;
     FFTW_COMPLEX *FFTTransSC::_out1;
     FFTW_COMPLEX *FFTTransSC::_out2;
     Matrix(Complex) FFTTransSC::_GreensF;
     Matrix(Real) FFTTransSC::_rho;
     Integer FFTTransSC::_initialized=0;

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FFTTransSC
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(FFTTransSC, TransSC);

   FFTTransSC::FFTTransSC(const String &n, const Integer &order,
            Integer &nXBins, Integer &nYBins, Real &eps, Real &length,
            const Integer &nMacrosMin) :
            TransSC(n, order, nXBins, nYBins, length, nMacrosMin, eps)
        {
          if(_initialized) return;

	  // Use this memory allocation scheme as per FFTW manual. Also
          // g++ chokes on new with arguments here (not everywhere!)

                _in = (FFTW_COMPLEX *)
                   new char[_nXBins * _nYBins * sizeof(FFTW_COMPLEX)];
                _out1 =(FFTW_COMPLEX *)
                   new char[_nXBins * _nYBins * sizeof(FFTW_COMPLEX)];
                _out2 =(FFTW_COMPLEX *)
                   new char[_nXBins * _nYBins * sizeof(FFTW_COMPLEX)];

               _planForward = fftw2d_create_plan(_nXBins, _nYBins,
                         FFTW_FORWARD, FFTW_ESTIMATE);
               _planBackward = fftw2d_create_plan(_nXBins, _nYBins,
                         FFTW_BACKWARD, FFTW_ESTIMATE);
               _GreensF.resize(_nXBins, _nYBins);
               _rho.resize(_nXBins+2, _nYBins+2);
               _initialized = 1;
        }

    FFTTransSC::~FFTTransSC()
     {
       if(_in)
	 {
          fftwnd_destroy_plan(_planForward);
          fftwnd_destroy_plan(_planBackward);
          delete [] _in;
          delete [] _out1;
          delete [] _out2;
	 }
     }

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FFTTransSC::NodeCalculator
//
// DESCRIPTION
//    Bins the macro particles, and calculates the 2-D density profile.
//    Calculates the 2-D forces.
//    Presently assumes a uniform recilinear grid.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FFTTransSC::_nodeCalculator(MacroPart &mp)
{

    Integer i, index, iX, iY, iXS, iYS, j, nPoints;
    Real x1, x2, y1, y2;
    Real epsSq, rTransX, rTransY, rTot2, rr;
    Integer nMacros;
    static Integer first = 1;

    // test to see if this is not the main herd:

    if(mp._feelsHerds ==1)  // just bin the particles, use existing forces:
      {
        for(i=1; i <= mp._nMacros; i++)
        {
           iX = 1 + Integer( (mp._x(i) - _xGridMin)/_dx);
           iY = 1 + Integer( (mp._y(i) - _yGridMin)/_dy);

           if(iX <1) iX =1; // keep particle within mainherd grid
           if (iX > _nXBins) iX=_nXBins;
           if(iY <1) iY =1;
           if (iY > _nYBins) iY=_nYBins;

           mp._xBin(i) = iX;
           mp._yBin(i) = iY;

           mp._xFractBin(i) = (mp._x(i) - _xGrid(iX))/_dx;
           mp._yFractBin(i) = (mp._y(i) - _yGrid(iY))/_dy;

         }
        _LFactor = 1.;
        return;
      }

   // Find particle extent:

    mp._findXYPExtrema();

   // Synch the extrema and number of particles: ==MPI==
   nMacros = mp._nMacros;
   syncTSCBins(mp._xMin, mp._xMax, mp._yMin, mp._yMax,
               mp._phiMin, mp._phiMax, nMacros);
   mp._globalNMacros = nMacros;

   if( mp._globalNMacros < _nMacrosMin) return;

 // Define grid extrema (take 2 times the actual particle extent):

 // rectangular grid:

   Real dxExtra = 0.5*(mp._xMax - mp._xMin);
   Real dyExtra = 0.5*(mp._yMax - mp._yMin);

   _xGridMin = mp._xMin - dxExtra;
   _xGridMax = mp._xMax + dxExtra;
   _yGridMin = mp._yMin - dyExtra;
   _yGridMax = mp._yMax + dyExtra;

   /*
  // Square grid, used to compare to accsim only:
   Real dxExtra = (mp._xMax - mp._xMin);
   Real dyExtra = (mp._yMax - mp._yMin);
   Real dExtra = Max(dxExtra,dyExtra);
   _xGridMin = (mp._xMin +mp._xMax)/2. - dExtra;
   _xGridMax = _xGridMin + 2.*dExtra;
   _yGridMin = (mp._yMin +mp._yMax)/2. - dExtra;
   _yGridMax = _yGridMin + 2.*dExtra;
   */

    // This peveance uses the average line density "lambda".

    _lambda = Real(mp._globalNMacros) * Injection::nReals_Macro
              * 2. * Consts::pi * Ring::harmonicNumber /
                (Ring::lRing * (mp._phiMax - mp._phiMin) );

    _perveance =  Sqr(mp._syncPart._charge)  *  _lambda * rClassical /
      (2. * Sqr(mp._syncPart._betaSync) * Cube(mp._syncPart._gammaSync)*
       mp._syncPart._mass);

   // Define the grid:
       _dx = (_xGridMax - _xGridMin)/Real(_nXBins);
       _dy = (_yGridMax - _yGridMin)/Real(_nYBins);

       for (iX=1; iX<= _nXBins+1; iX++)
             _xGrid(iX) = _xGridMin + Real(iX-1) * _dx;

       for (iY = 1; iY<= _nYBins+1; iY++)
             _yGrid(iY) = _yGridMin + Real(iY-1) * _dy;

       // Bin the particles:
       _rho = 0.;

        for(i=1; i <= mp._nMacros; i++)
        {
           iX = 1 + Integer( (mp._x(i) - _xGridMin)/_dx);
           iY = 1 + Integer( (mp._y(i) - _yGridMin)/_dy);

           mp._xBin(i) = iX;
           mp._yBin(i) = iY;

          mp._xFractBin(i) = (mp._x(i) - _xGrid(iX))/_dx;
          mp._yFractBin(i) = (mp._y(i) - _yGrid(iY))/_dy;

         // Bilinear binning:

         _rho(iX,iY) += (1.-mp._xFractBin(i)) * (1.-mp._yFractBin(i));
         _rho(iX,iY+1) += (1.-mp._xFractBin(i)) * mp._yFractBin(i);
         _rho(iX+1,iY) += mp._xFractBin(i) * (1.-mp._yFractBin(i));
         _rho(iX+1,iY+1) += mp._xFractBin(i) * mp._yFractBin(i);
	}

//   Sync the charge for parallel runs
//   Since the forces are not used on the outer +- 1/4 of the grid points
//   (for FFT method) don't waste time doing these points.
//    We start the charge cvalc. here and then take a break to do the
//    Greens funct. stuff to avoid  latency for the parallel calcs.

    nPoints = (_nXBins/2+2) * (_nYBins/2 +2);
    if(first)
     {
        localRho.resize(nPoints);
        globalRho.resize(nPoints);
        first = 0;
     }

       i = 0;
       for(iX = _nXBins/4; iX<= (3*_nXBins/4+1); iX++)
       for(iY = _nYBins/4; iY<= (3*_nYBins/4+1); iY++)
	 {
           i++;
           localRho(i) = _rho(iX, iY);
	 }

    syncTSCRho1(nPoints);  // send the charge densities


      // Calculate the Greens funtion grid FFT

        if(_eps <  0.)
          epsSq = Sqr(_eps);
         else
          epsSq = Sqr(_eps) * _dx * _dy;

        for (iY = 1; iY <=_nYBins/2; iY++)   // assign middle to top-1 rows
         {
           rTransY = _dy * (iY-1);
           for (iX = 1; iX <= _nXBins/2; iX++)
            {
              rTransX = (iX-1) * _dx;
              rTot2 = rTransX*rTransX + rTransY*rTransY + epsSq;
//              rTot2 = rTransX*rTransX + rTransY*rTransY;
              _GreensF(iX, iY).re = 0.0;
              _GreensF(iX, iY).im = 0.0;
              if(rTot2 > 0.0)
              {
                _GreensF(iX, iY).re = rTransX / rTot2;
                _GreensF(iX, iY).im = rTransY / rTot2;
              }
	    }


            _GreensF(_nXBins/2+1, iY).re = 0.; // end point
            _GreensF(_nXBins/2+1, iY).im = 0.;

            for (iX = _nXBins/2+2; iX <= _nXBins; iX++)
            {
              rTransX = (iX - 1 - _nXBins) * _dx;
              rTot2 = rTransX*rTransX + rTransY*rTransY + epsSq;
//              rTot2 = rTransX*rTransX + rTransY*rTransY;
              _GreensF(iX, iY).re = 0.0;
              _GreensF(iX, iY).im = 0.0;
              if(rTot2 > 0.0)
              {
                _GreensF(iX, iY).re = rTransX / rTot2;
                _GreensF(iX, iY).im = rTransY / rTot2;
              }
	    }
          }

        for(iX=1; iX<= _nXBins; iX++)   // Null the top row:
          {
            _GreensF(iX,_nYBins/2+1).re = 0.;
            _GreensF(iX,_nYBins/2+1).im = 0.;
          }

        for (iY = _nYBins/2+2; iY <=_nYBins; iY++)  // Bottom rows:
          {
            rTransY = _dy * (iY-1 - _nYBins);
            for (iX = 1; iX <= _nXBins/2; iX++)
            {
              rTransX = (iX-1) * _dx;
              rTot2 = rTransX*rTransX + rTransY*rTransY + epsSq;
//              rTot2 = rTransX*rTransX + rTransY*rTransY;
              _GreensF(iX, iY).re = 0.0;
              _GreensF(iX, iY).im = 0.0;
              if(rTot2 > 0.0)
              {
                _GreensF(iX, iY).re = rTransX / rTot2;
                _GreensF(iX, iY).im = rTransY / rTot2;
              }
	    }

            _GreensF(_nXBins/2+1, iY).re = 0.; // end point
            _GreensF(_nXBins/2+1, iY).im = 0.;

            for (iX = _nXBins/2+2; iX <= _nXBins; iX++)
            {
              rTransX = (iX - 1 - _nXBins) * _dx;
              rTot2 = rTransX*rTransX + rTransY*rTransY + epsSq;
//              rTot2 = rTransX*rTransX + rTransY*rTransY;
              _GreensF(iX, iY).re = 0.0;
              _GreensF(iX, iY).im = 0.0;
              if(rTot2 > 0.0)
              {
                _GreensF(iX, iY).re = rTransX / rTot2;
                _GreensF(iX, iY).im = rTransY / rTot2;
              }
	    }
          }

        //   Calculate the FFT of the Greens Function:

         for(j=0; j<= _nYBins-1; j++)
           for(i=0; i<= _nXBins-1; i++)

           {
             c_re(_in[j + _nYBins*i]) = _GreensF(i+1, j+1).re;
             c_im(_in[j + _nYBins*i]) = _GreensF(i+1, j+1).im;
           }

         fftwnd(_planForward, 1, _in, 1, 0, _out2, 1, 0);


        // gather the charge densities:

        i = 0;
        for(iX = _nXBins/4; iX<= (3*_nXBins/4+1); iX++)
        for(iY = _nYBins/4; iY<= (3*_nYBins/4+1); iY++)
	  {
            i++;
            _rho(iX, iY) = globalRho(i);
          }

    //   Calculate the FFT of the binned charge distribution:

         for(j=0; j<= _nYBins-1; j++)
           for(i=0; i<= _nXBins-1; i++)
           {
             c_re(_in[j + _nYBins*i]) = _rho(i+1, j+1);
             c_im(_in[j + _nYBins*i]) = 0.;
           }
/*
         for(i=0; i< _nXBins; i++)
           for(j=0; j< _nYBins; j++)
           { c_re(_out1[j+_nYBins*i]) = 0.;
           c_im(_out1[j+_nYBins*i]) = 0.;
           }

*/
         fftwnd(_planForward, 1, _in, 1, 0, _out1, 1, 0);

         // Do Convolution:

         for(j=0; j<= _nYBins-1; j++)
           for(i=0; i<= _nXBins-1; i++)

           {
             index = j + _nYBins*i;
             c_re(_in[index]) = c_re(_out1[index])*c_re(_out2[index]) -
                                c_im(_out1[index])*c_im(_out2[index]);
             c_im(_in[index]) = c_re(_out1[index])*c_im(_out2[index]) +
                                c_im(_out1[index])*c_re(_out2[index]);
           }

         // Do Inverse FFT to get the Force:

         fftwnd(_planBackward, 1, _in, 1, 0, _out1, 1, 0);

        _fscx = 0.;
        _fscy = 0.;

        // Include the perveance: (factor of 1.e3 to get F in MKS.
        //                           since our distances are in mm)

        Real factor = 4.0e3 * _perveance/
             (Real(mp._globalNMacros) * Real(_nXBins) * Real(_nYBins));

        for (iX = 1; iX <=_nXBins; iX++)
          {
          for (iY = 1; iY <= _nYBins; iY++)
            {
               index = iY-1 + _nYBins * (iX-1);
               _fscx(iX, iY) = c_re(_out1[index]) * factor;
               _fscy(iX, iY) = c_im(_out1[index]) * factor;
	    }
          }


  // Set some values needed for longitudinal coefficient later:

  _LFactor = 1.;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//  syncTSCRho1
//
// DESCRIPTION
//    Synchronizes the charge density across all parallel jobs
//    Does the scatter part here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void syncTSCRho1(Integer &nPoints)
{
    int i_flag;
    MPI_Initialized(&i_flag);
    if(i_flag){
       MPI_Allreduce ( (double *) &localRho.data(),
                       (double *) &globalRho.data(),
                       nPoints,
		       MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    }
    else{
      for(int i=1; i<= nPoints;i++){
	globalRho(i)=localRho(i);
      }
    }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//  syncTSCBins
//
// DESCRIPTION
//    Synchronizes the bin size across all parallel jobs
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Integer syncTSCBins(Real &xGMin, Real &xGMax,
                 Real &yGMin, Real &yGMax, Real &phiMin,
                 Real &phiMax, Integer &nMacros)
{

  int i_flag;
  MPI_Initialized(&i_flag);

  if(i_flag){

   double d_local[6];
   double d_global[6];
   d_local[0]=-xGMin;
   d_local[1]= xGMax;
   d_local[2]=-yGMin;
   d_local[3]= yGMax;
   d_local[4]=-phiMin;
   d_local[5]= phiMax;

   MPI_Allreduce ( d_local,d_global,6,MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

   xGMin = - d_global[0];
   xGMax =   d_global[1];
   yGMin = - d_global[2];
   yGMax =   d_global[3];
   phiMin= - d_global[4];
   phiMax=   d_global[5];
  }

  int nMacros_local = nMacros;

  if(i_flag){
    MPI_Allreduce ( &nMacros_local,&nMacros,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FFTTransSC::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with an TransSC. The Transverse space charge kick is
//    added to each macro particle here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FFTTransSC::_updatePartAtNode(MacroPart &mp)
{
  Real f1, f2, f3, f4, fx, fy;
  Integer j;

    if(mp._globalNMacros < _nMacrosMin && mp._feelsHerds == 0) return;

    for (j=1; j<= mp._nMacros; j++)
    {

    // Find horizontal force:

      f1 = _fscx(mp._xBin(j), mp._yBin(j));
      f2 = _fscx(mp._xBin(j)+1, mp._yBin(j));
      f3 = _fscx(mp._xBin(j)+1, mp._yBin(j)+1);
      f4 = _fscx(mp._xBin(j), mp._yBin(j)+1);

      fx = (1. - mp._xFractBin(j)) * (1. - mp._yFractBin(j))*f1 +
         mp._xFractBin(j) * (1. - mp._yFractBin(j))*f2 +
         (1. - mp._xFractBin(j)) * mp._yFractBin(j)*f4 +
         mp._xFractBin(j) * mp._yFractBin(j) * f3;

  // Find Vertical Force:

      f1 = _fscy(mp._xBin(j), mp._yBin(j));
      f2 = _fscy(mp._xBin(j)+1, mp._yBin(j));
      f3 = _fscy(mp._xBin(j)+1, mp._yBin(j)+1);
      f4 = _fscy(mp._xBin(j), mp._yBin(j)+1);

//     cerr << "c ";
//     cerr << f1 << "  " << f2 << "  "
//      << f3  << "  " << f4 << "  "
//      << mp._x(j) << "  " << mp._y(j) <<"\n";

      fy = (1. - mp._xFractBin(j)) * (1. - mp._yFractBin(j))*f1 +
         mp._xFractBin(j) * (1. - mp._yFractBin(j))*f2 +
         (1. - mp._xFractBin(j)) * mp._yFractBin(j)*f4 +
         mp._xFractBin(j) * mp._yFractBin(j) * f3;

  // Include local line density effect, if it is available.


      if(mp._longBinningDone)  _LFactor = mp._LPosFactor(j);

      mp._xp(j) += fx * 1000. * _lkick * _LFactor;
      mp._yp(j) += fy * 1000. * _lkick * _LFactor;

      //  fio << mp._x(j) << "  "  << mp._y(j) << "  "
      //    << 1000. * fx*_LFactor*_lkick   << "  "
      //    << 1000. * fy*_LFactor*_lkick << "\n";
    }
}



///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    PWSTransSC
//
// INHERITANCE RELATIONSHIPS
//    PWSTransSC -> TransSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for a pairwise-sum (non-PIC) TransSC implementation.
//    This method is simple, but very slow. It is usefule for checking
//    other methods though.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class PWSTransSC : public TransSC {
  Declare_Standard_Members(PWSTransSC,TransSC);
public:
    PWSTransSC(const String &n, const Integer &order, Integer &nx,
                 Integer &ny,
                 Real &eps, Real &length, const Integer &nMacrosMin) :
            TransSC(n, order, nx, ny, length, nMacrosMin, eps)
      {
      }

   ~PWSTransSC()
      {
      }
    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
    Vector(Real) _fx, _fy;

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS PWSTransSC
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(PWSTransSC, TransSC);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    PWSTransSC::NodeCalculator
//
// DESCRIPTION
//    Sets up the pairwise sum TransSC calculation
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void PWSTransSC::_nodeCalculator(MacroPart &mp)
{
    Integer i,j;
    Real dx,dy, dfx, dfy, denom;

    _fx.resize(mp._nMacros);
    _fy.resize(mp._nMacros);

    if(mp._nMacros < _nMacrosMin) return;

    SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart & Particles::All_Mask) - 1));

    // Find the longitudinal extent of particles:

    mp._phiMin = mp._phiMax = 0;

    for (i=1; i <= mp._nMacros; i++)
    {
      if(mp._phi(i) > mp._phiMax)  mp._phiMax = mp._phi(i);
      if(mp._phi(i) < mp._phiMin)  mp._phiMin = mp._phi(i);
    }


    // This peveance uses the average line density "lambda".

    _lambda = Real(mp._nMacros) * Injection::nReals_Macro * 2. * Consts::pi *
             Ring::harmonicNumber
          / (Ring::lRing * (mp._phiMax - mp._phiMin) );

    _perveance =  Sqr(sp->_charge)  *  _lambda * rClassical /
      (2. * Sqr(sp->_betaSync) * Cube(sp->_gammaSync)* sp->_mass);


  // Set some values needed for longitudinal coefficient later:

  _LFactor = 1.;

  // do pair-wise sum of distance part of the force:

  for(i=1; i<= mp._nMacros; i++)
    for(j=i+1; j<= mp._nMacros ; j++)
      {
        dx = mp._x(i) - mp._x(j);
        dy = mp._y(i) - mp._y(j);
        denom = Sqr(dx) + Sqr(dy) + Sqr(_eps);
        dfx = dx/denom;
        dfy = dy/denom;
        _fx(i) += dfx;
        _fx(j) -= dfx;
        _fy(i) += dfy;
        _fy(j) -= dfy;
      }

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    PWSTransSC::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with a pairwise sum TransSC.
//    The Transverse space charge kick is
//    added to each macro particle here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void PWSTransSC::_updatePartAtNode(MacroPart &mp)
{
  Real factor, fx, fy;
  Integer j;


    if(mp._nMacros < _nMacrosMin) return;

    for(j=1; j<= mp._nMacros; j++)
    {

      // Include local line density effect, if it is available.
        if(mp._longBinningDone)  _LFactor = mp._LPosFactor(j);


      // Add the kick. Factor of 1.e6 includes 1.e3 to get force in MKS
      // (we used mm for lengths) and another 1.e3 to get kick in mrad

      factor = 4.e6 * _perveance * _lkick * _LFactor / Real(mp._nMacros);
      mp._xp(j) += _fx(j) * factor;
      mp._yp(j) += _fy(j) * factor;

    }

}


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    PotentialTransSC
//
// INHERITANCE RELATIONSHIPS
//    PotentialTransSC -> TransSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for space charge calculation with beam pipe and
//    conducting wall boundary condition.
//    It bins the particles in cells using a second order nine point
//    scheme, uses an FFT implementaion to calculate space charge
//    potential, and a least squares solution to satisfy the beam pipe
//    conducting wall boundary condition.
//
//
// The parameters BP1,BP2,BP3,BP4 have different meanings for different
// shapes:
// shape = "Ellipse" BP1 is 1/2 horizontal size, BP2 - 1/2 vertical size
// shape = "Circle" BP1 - radius
// shape = "Rectangle" BP1 - minX, BP2 - maxX, BP3 - minY, BP4 - maxY
// Shape could be "Ellipse","Rectangle","Circle"
// Sizes in [mm]
//
///////////////////////////////////////////////////////////////////////////

class PotentialTransSC : public TransSC {
  Declare_Standard_Members(PotentialTransSC,TransSC);
public:
    PotentialTransSC(const String &n, const Integer &order,
                 Integer &nXBins, Integer &nYBins,
                 Real &eps, Real &length,
                 const String &BPShape, Real &BP1,
                 Real &BP2, Real &BP3, Real &BP4,
                 Integer &BPPoints, Integer &BPModes,
                 Real &Gridfact,
                 const Integer &nMacrosMin);
    ~PotentialTransSC();
    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
    Void _GridExtent(MacroPart &mp);
    Void _LSQBP(Integer &iXmin, Integer &iXmax,
                Integer &iYmin, Integer &iYmax);
    static String _BPShape;
    static Real _BPr, _BPa, _BPb, _BPxMin, _BPxMax, _BPyMin, _BPyMax;
    static Integer _BPPoints, _BPModes;
    static Real _Gridfact;
    static Vector(Real) _BPx, _BPy, _BPPhi;
    static Matrix(Real) _BPPhiH;
    static Vector(Real) _BPH;
    static Real _BPrnorm, _BPResid;

 private:
    static rfftwnd_plan _planForward, _planBackward;
    static FFTW_REAL * _in;
    static FFTW_COMPLEX * _out1;
    static FFTW_COMPLEX * _out2;
    static FFTW_COMPLEX * _out;
    static Matrix(Real) _GreensF;
    static Matrix(Real) _rho;
    static Integer _initialized;
    static  double* _phisc_tmp_local;
    static  double* _phisc_tmp_global;
    static ORBIT_Boundary* _boundary;
  };

///////////////////////////////////////////////////////////////////////////
//
// Static MEMBER FUNCTIONS FOR CLASS PotentialTransSC
//
///////////////////////////////////////////////////////////////////////////

    String PotentialTransSC::_BPShape;
    Real PotentialTransSC::_BPr;
    Real PotentialTransSC::_BPa;
    Real PotentialTransSC::_BPb;
    Real PotentialTransSC::_BPxMin;
    Real PotentialTransSC::_BPxMax;
    Real PotentialTransSC::_BPyMin;
    Real PotentialTransSC::_BPyMax;

    Integer PotentialTransSC::_BPPoints;
    Integer PotentialTransSC::_BPModes;
    Real PotentialTransSC::_Gridfact;
    Vector(Real) PotentialTransSC::_BPx;
    Vector(Real) PotentialTransSC::_BPy;
    Vector(Real) PotentialTransSC::_BPPhi;
    Matrix(Real) PotentialTransSC::_BPPhiH;
    Vector(Real) PotentialTransSC::_BPH;
    Real PotentialTransSC::_BPrnorm;
    Real PotentialTransSC::_BPResid;


     rfftwnd_plan PotentialTransSC::_planForward,
                  PotentialTransSC::_planBackward;
     FFTW_REAL *PotentialTransSC::_in;
     FFTW_COMPLEX *PotentialTransSC::_out1;
     FFTW_COMPLEX *PotentialTransSC::_out2;
     FFTW_COMPLEX *PotentialTransSC::_out;
     Matrix(Real) PotentialTransSC::_GreensF;
     Matrix(Real) PotentialTransSC::_rho;
     Integer PotentialTransSC::_initialized=0;
     double* PotentialTransSC::_phisc_tmp_local;
     double* PotentialTransSC::_phisc_tmp_global;
     ORBIT_Boundary* PotentialTransSC::_boundary;

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS PotentialTransSC
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(PotentialTransSC, TransSC);

   PotentialTransSC::PotentialTransSC(const String &n, const Integer &order,
            Integer &nXBins, Integer &nYBins, Real &eps, Real &length,
            const String &BPShape, Real &BP1,
            Real &BP2, Real &BP3, Real &BP4,
            Integer &BPPoints, Integer &BPModes,
            Real &Gridfact,
            const Integer &nMacrosMin) :
            TransSC(n, order, nXBins, nYBins, length, nMacrosMin, eps)
        {
              if(_initialized) return;

	      //--------new bounday instance START=====
	      int boundary_shape = -1;
	      double boundary_xsize = 0.;
	      double boundary_ysize = 0.;

	      if (BPShape != "None"){

		if(BPShape == "Circle") {
		  boundary_shape = 1;
		  boundary_xsize = 2.0*BP1;
		  boundary_ysize = boundary_xsize;
		}

		if(BPShape == "Ellipse") {
		  boundary_shape = 2;
		  boundary_xsize = 2.0*BP1;
		  boundary_ysize = 2.0*BP2;
		}

		if(BPShape == "Rectangle") {
		  boundary_shape = 3;
		  boundary_xsize = BP2 - BP1;
		  boundary_ysize = BP4 - BP3;
		}

		_boundary = new ORBIT_Boundary(nXBins,nYBins,
					       boundary_xsize,boundary_ysize,
					       BPPoints,boundary_shape,
					       BPModes,eps);
	      }
	      else{
		_boundary = NULL;
	      }


	      //debug output for checking the boundary points
	      // _boundary->checkBoundary();

	      //--------new bounday instance START=====

	     _BPShape = BPShape;
             _BPPoints = BPPoints;
             if (_BPShape == "Rectangle") _BPPoints = 4*(BPPoints/4);
             _BPModes  = BPModes;
             _Gridfact = Gridfact;
             _BPx.resize(_BPPoints);
             _BPy.resize(_BPPoints);
             _BPPhi.resize(_BPPoints);
             _BPPhiH.resize(_BPPoints,2*_BPModes+1);
             _BPH.resize(2*_BPModes+1);

             Integer i, j, BPPO4, i1, i2, i3, i4, jc, js;
             Real r, theta, dtheta, rj, rsq, dx, dy, rfac;
             Vector(Real) th;
             th.resize(_BPPoints);

             if (_BPShape == "Circle")
	     {
	       _BPr = BP1;
               _BPrnorm = _BPr;
               dtheta = 2. * Consts::pi / _BPPoints;
               for (i = 1; i <= _BPPoints; i++)
	       {
		 th(i) = (i-1) * dtheta;
                 _BPx(i) = _BPr * cos(th(i));
                 _BPy(i) = _BPr * sin(th(i));

		 /**
                 cout << i << "  "
                      << _BPx(i) << "  " << _BPy(i) << "\n";
		 */

	       }
	     }

             if (_BPShape == "Ellipse")
	     {
	       _BPa = BP1;
	       _BPb = BP2;
               rsq = _BPa*_BPb;
               _BPrnorm = pow(rsq, 0.5);
               BPPO4 = _BPPoints/4;

               Real ds = 2. * Consts::pi * _BPrnorm / _BPPoints;
               Real denom;
               Real resid = 1.0;

               while (resid > 1.0e-08 || resid < -1.0e-08)
	       {
                  denom = _BPb;
                  dtheta = ds / denom;
                  theta = dtheta / 2.;
                  th(1) = 0.;
                  for (i = 1; i <= BPPO4; i++)
	          {
                     denom = _BPb*_BPb
                          + (_BPa*_BPa-_BPb*_BPb)*sin(theta)*sin(theta);
                     denom = pow(denom, 0.5);
                     dtheta = ds / denom;
                     th(i+1) = th(i) + dtheta;
                     theta += dtheta;
	          }
                  resid = th(1+BPPO4) - Consts::pi / 2.;
                  ds *= Consts::pi / (2.*th(1+BPPO4));
	       }

               /*
               cout << "Resid = " << resid << "   "
                    << th(1+BPPO4) << "\n\n";
	       */

               for (i = 1; i <= BPPO4; i++)
	       {
                 i1 = i;
                 i2 = 2*BPPO4+2 - i1;
		 th(i2) = Consts::pi - th(i1);
	       }

               for (i = 1; i <= 2*BPPO4; i++)
	       {
                 i1 = i;
                 i3 = 2*BPPO4 + i1;
		 th(i3) = Consts::pi + th(i1);
	       }

               for (i = 1; i <= _BPPoints; i++)
	       {
                 _BPx(i) = _BPa * cos(th(i));
                 _BPy(i) = _BPb * sin(th(i));
                 th(i) = atan2(_BPy(i),_BPx(i));

                 /**
                 cout << i << "  "
                      << _BPx(i) << "  " << _BPy(i) << "\n";
		 */

	       }
               //cout << "\n";
	     }

             if (_BPShape == "Rectangle")
	     {
               _BPxMin = BP1;
               _BPxMax = BP2;
               _BPyMin = BP3;
               _BPyMax = BP4;
               rsq = ((_BPxMax - _BPxMin) * (_BPxMax - _BPxMin) +
                      (_BPyMax - _BPyMin) * (_BPyMax - _BPyMin)) / 4.;
               _BPrnorm = pow(rsq, 0.5);
               BPPO4 = _BPPoints/4;
               dx = (_BPxMax - _BPxMin) / BPPO4;
               dy = (_BPyMax - _BPyMin) / BPPO4;
               for (i = 1; i <= BPPO4; i++)
	       {
                 i1 = i;
	         _BPx(i1) = _BPxMax;
                 _BPy(i1) = _BPyMin + (i-1) * dy;
                 th(i1) = atan2(_BPy(i1),_BPx(i1));

                 i2 = BPPO4 + i1;
	         _BPx(i2) = _BPxMax - (i-1) * dx;
                 _BPy(i2) = _BPyMax;
                 th(i2) = atan2(_BPy(i2),_BPx(i2));

                 i3 = BPPO4 + i2;
	         _BPx(i3) = _BPxMin;
                 _BPy(i3) = _BPyMax - (i-1) * dy;
                 th(i3) = atan2(_BPy(i3),_BPx(i3));

                 i4 = BPPO4 + i3;
	         _BPx(i4) = _BPxMin + (i-1) * dx;
                 _BPy(i4) = _BPyMin;
                 th(i4) = atan2(_BPy(i4),_BPx(i4));

		 /**
                 cout << i << "  "
                      << _BPx(i) << "  " << _BPy(i) << "\n";
		 */

	       }
	     }

             if (_BPShape != "None")
	     {
               for (i = 1; i <= _BPPoints; i++)
	       {
                  rsq = _BPx(i)*_BPx(i) + _BPy(i)*_BPy(i);
                  r = pow(rsq,0.5);
                  rfac = r / _BPrnorm;
                  rj = 1;
                  _BPPhiH(i,1) = 1.0;
                  for (j = 1; j <= _BPModes; j++)
	          {
                     jc = 2 * j;
                     js = 2 * j + 1;
                     rj *= rfac;
                    _BPPhiH(i,jc) = rj * cos(j*th(i));
                    _BPPhiH(i,js) = rj * sin(j*th(i));
	          }
	       }
	     }

	  // Use this memory allocation scheme as per FFTW manual. Also
          // g++ chokes on new with arguments here (not everywhere!)

                _in = (FFTW_REAL *)
                   new char[_nXBins * _nYBins
                                    * sizeof(FFTW_REAL)];
                _out1 =(FFTW_COMPLEX *)
                   new char[_nXBins * (_nYBins/2+1)
                                    * sizeof(FFTW_COMPLEX)];
                _out2 =(FFTW_COMPLEX *)
                   new char[_nXBins * (_nYBins/2+1)
                                    * sizeof(FFTW_COMPLEX)];
                _out =(FFTW_COMPLEX *)
                   new char[_nXBins * (_nYBins/2+1)
                                    * sizeof(FFTW_COMPLEX)];

               _planForward = rfftw2d_create_plan(_nXBins, _nYBins,
                         FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE);
               _planBackward = rfftw2d_create_plan(_nXBins, _nYBins,
                         FFTW_COMPLEX_TO_REAL, FFTW_ESTIMATE);
               _GreensF.resize(_nXBins, _nYBins);
               _rho.resize(_nXBins, _nYBins);
               _initialized = 1;

	       //start ===MPI===
               //parallel stuff for ===MPI===
               _phisc_tmp_local  = (double *) malloc ( sizeof(double)*(_nXBins+1)*(_nYBins+1));
               _phisc_tmp_global = (double *) malloc ( sizeof(double)*(_nXBins+1)*(_nYBins+1));


        }

    PotentialTransSC::~PotentialTransSC()
     {
       if(_in)
	 {
          rfftwnd_destroy_plan(_planForward);
          rfftwnd_destroy_plan(_planBackward);
          delete [] _in;
          delete [] _out1;
          delete [] _out2;
          delete [] _out;
	  //this addition for the fast boundary effect calculation
	  if(_boundary != NULL) delete _boundary;
          free(_phisc_tmp_local);
          free(_phisc_tmp_global);
	 }
     }

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    PotentialTransSC::NodeCalculator
//
// DESCRIPTION
//    Bins the macro particles, calculates the 2-D density profile,
//    the 2-D potential with conducting wall boundary conditions on
//    the beam pipe, and the 2-D forces.
//    Presently assumes a uniform rectangular grid.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void PotentialTransSC::_nodeCalculator(MacroPart &mp)
{

    Integer i, index, iX, iY, j, nPoints;
    Real x1, x2, y1, y2;
    Real epsSq, rTransX, rTransY, rTot2, rr;
    Integer nMacros;
    static Integer first = 1;
    Integer iXmin, iXmax, iYmin, iYmax;

  // test to see if this is not the main herd:

    if(mp._feelsHerds == 1)  // just bin the particles, use existing forces:
      {
        for(i=1; i <= mp._nMacros; i++)
        {
           iX = 1 + Integer( (mp._x(i) - _xGridMin)/_dx + 0.5);
           iY = 1 + Integer( (mp._y(i) - _yGridMin)/_dy + 0.5);

           if(iX < 1) iX = 1;
           if (iX > _nXBins) iX = _nXBins+1;
           if(iY < 1) iY = 1;
           if (iY > _nYBins) iY = _nYBins+1;

           mp._xBin(i) = iX;
           mp._yBin(i) = iY;

           mp._xFractBin(i) = (mp._x(i) - _xGrid(iX))/_dx;
           mp._yFractBin(i) = (mp._y(i) - _yGrid(iY))/_dy;

         }
        _LFactor = 1.;
        return;
      }

    int i_flag;
    MPI_Initialized(&i_flag);

    if(i_flag){
      MPI_Allreduce ( &mp._nMacros,&mp._globalNMacros,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    }
    else{
      mp._globalNMacros = mp._nMacros;
    }

    if(mp._globalNMacros < _nMacrosMin){ return;}

    // Find particle grid extent:

    _GridExtent(mp);

    double _phiMax_tmp_local = mp._phiMax;
    double _phiMin_tmp_local = mp._phiMin;
    double _phiMax_tmp_global;
    double _phiMin_tmp_global;
    if(i_flag){
      MPI_Allreduce ( &_phiMax_tmp_local,&_phiMax_tmp_global,1,MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce ( &_phiMin_tmp_local,&_phiMin_tmp_global,1,MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    }
    else{
      mp._globalNMacros = mp._nMacros;
      _phiMax_tmp_global = _phiMax_tmp_local;
      _phiMin_tmp_global = _phiMin_tmp_local;
    }

    // This perveance uses the average line density "lambda".

    _lambda = Real(mp._globalNMacros) * Injection::nReals_Macro
              * 2. * Consts::pi * Ring::harmonicNumber /
                (Ring::lRing * (_phiMax_tmp_global - _phiMin_tmp_global) );

    _perveance =  Sqr(mp._syncPart._charge)  *  _lambda * rClassical /
      (2. * Sqr(mp._syncPart._betaSync) * Cube(mp._syncPart._gammaSync)*
       mp._syncPart._mass);

   // Define the grid:
       _dx = (_xGridMax - _xGridMin)/Real(_nXBins);
       _dy = (_yGridMax - _yGridMin)/Real(_nYBins);

       for (iX = 1; iX <= _nXBins+1; iX++)
             _xGrid(iX) = _xGridMin + Real(iX-1) * _dx;

       for (iY = 1; iY <= _nYBins+1; iY++)
             _yGrid(iY) = _yGridMin + Real(iY-1) * _dy;

       iXmin = Integer(Real(_nXBins) * (1. - 1./_Gridfact) / 2.) - 1;
       iXmax = _nXBins + 2 - iXmin;
       if(iXmax > _nXBins) iXmax = _nXBins;
       iYmin = Integer(Real(_nYBins) * (1. - 1./_Gridfact) / 2.) - 1;
       iYmax = _nYBins + 2 - iYmin;
       if(iYmax > _nYBins) iYmax = _nYBins;

       /**
       std::cout<<"debug _dx = "<< _dx <<"  _dy=" << _dy << std::endl;
       std::cout<<"debug _xGridMin = "<<_xGridMin <<"  _xGridMax = "<<_xGridMax<<std::endl;
       std::cout<<"debug _yGridMin = "<<_yGridMin <<"  _yGridMax = "<<_yGridMax<<std::endl;
       std::cout<<"debug _Gridfact = "<< _Gridfact << std::endl;
       */

       //============== debug output for boundary START
       /**
       _boundary->defineExtXYgrid(&_xGridMin,&_xGridMax,&_yGridMin,&_yGridMax,&_dx,&_dy);
       std::cout<<"debug =============================================== "<<std::endl;
       std::cout<<"debug _dx = "<< _dx <<"  _dy=" << _dy << std::endl;
       std::cout<<"debug _xGridMin = "<<_xGridMin <<"  _xGridMax = "<<_xGridMax<<std::endl;
       std::cout<<"debug _yGridMin = "<<_yGridMin <<"  _yGridMax = "<<_yGridMax<<std::endl;
       */
       //============== debug output for boundary STOP


       // Bin the particles:
       _rho = 0.;

        for(i=1; i <= mp._nMacros; i++)
        {
           iX = 1 + Integer( (mp._x(i) - _xGridMin)/_dx + 0.5);
           iY = 1 + Integer( (mp._y(i) - _yGridMin)/_dy + 0.5);

           if(iX < iXmin+1) iX = iXmin+1;
           if (iX > iXmax-1) iX = iXmax-1;
           if(iY < iYmin+1) iY = iYmin+1;
           if (iY > iYmax-1) iY = iYmax-1;

           mp._xBin(i) = iX;
           mp._yBin(i) = iY;

          mp._xFractBin(i) = (mp._x(i) - _xGrid(iX))/_dx;
          mp._yFractBin(i) = (mp._y(i) - _yGrid(iY))/_dy;

         // TSC binning, see Hockney and Eastwood:

	  Real Wxm = 0.5 * (0.5 - mp._xFractBin(i))
	                 * (0.5 - mp._xFractBin(i));
          Real Wx0 = 0.75 - mp._xFractBin(i) * mp._xFractBin(i);
	  Real Wxp = 0.5 * (0.5 + mp._xFractBin(i))
	                 * (0.5 + mp._xFractBin(i));
	  Real Wym = 0.5 * (0.5 - mp._yFractBin(i))
	                 * (0.5 - mp._yFractBin(i));
          Real Wy0 = 0.75 - mp._yFractBin(i) * mp._yFractBin(i);
	  Real Wyp = 0.5 * (0.5 + mp._yFractBin(i))
	                 * (0.5 + mp._yFractBin(i));
         _rho(iX-1,iY-1) += Wxm * Wym;
         _rho(iX-1,iY)   += Wxm * Wy0;
         _rho(iX-1,iY+1) += Wxm * Wyp;
         _rho(iX,iY-1)   += Wx0 * Wym;
         _rho(iX,iY)     += Wx0 * Wy0;
         _rho(iX,iY+1)   += Wx0 * Wyp;
         _rho(iX+1,iY-1) += Wxp * Wym;
         _rho(iX+1,iY)   += Wxp * Wy0;
         _rho(iX+1,iY+1) += Wxp * Wyp;
	}


   // Calculate the Greens funtion grid FFT

   if(_eps <  0.)
      epsSq = Sqr(_eps);
   else
      epsSq = Sqr(_eps) * _dx * _dy;

   for (iY = 1; iY <= _nYBins/2+1; iY++)
   {
      rTransY = (iY-1) * _dy;

      for (iX = 1; iX <= _nXBins/2+1; iX++)
      {
         rTransX = (iX-1) * _dx;
         rTot2 = rTransX*rTransX + rTransY*rTransY + epsSq;
//         rTot2 = rTransX*rTransX + rTransY*rTransY;
         _GreensF(iX, iY) = 0.0;
         if(rTot2 > 0.0)
         {
           _GreensF(iX, iY) = log(rTot2) / 2.;
         }
      }

      for (iX = _nXBins/2+2; iX <= _nXBins; iX++)
      {
         _GreensF(iX, iY) = _GreensF(_nXBins+2-iX, iY);
      }
   }

   for (iY = _nYBins/2+2; iY <=_nYBins; iY++)
   {
      for (iX = 1; iX <= _nXBins; iX++)
      {
         _GreensF(iX, iY) = _GreensF(iX, _nYBins+2-iY);
      }
   }

   //   Calculate the FFT of the Greens Function:

   for (i = 0; i < _nXBins; i++)
   for (j = 0; j < _nYBins; j++)
   {
      _in[j + _nYBins*i] = _GreensF(i+1, j+1);
   }

   rfftwnd_one_real_to_complex(_planForward, _in, _out2);


   //   Calculate the FFT of the binned charge distribution:

   for (i = 0; i < _nXBins; i++)
   for (j = 0; j < _nYBins; j++)
   {
      _in[j + _nYBins*i] = _rho(i+1, j+1);
   }

   rfftwnd_one_real_to_complex(_planForward, _in, _out1);


   // Do Convolution:

   for (i = 0; i < _nXBins; i++)
   for (j = 0; j < _nYBins/2+1; j++)
   {
      index = j + (_nYBins/2+1)*i;
      c_re(_out[index]) = c_re(_out1[index])*c_re(_out2[index]) -
                         c_im(_out1[index])*c_im(_out2[index]);
      c_im(_out[index]) = c_re(_out1[index])*c_im(_out2[index]) +
                         c_im(_out1[index])*c_re(_out2[index]);
   }

   // Do Inverse FFT to get the Potential:

   rfftwnd_one_complex_to_real(_planBackward, _out, _in);

   _phisc = 0.;

   // Include the perveance: (factor of 1.e3 to get F in MKS.
   //                           since our distances are in mm)

   Real factor = 4.0e3 * _perveance/
             (Real(mp._globalNMacros) * Real(_nXBins) * Real(_nYBins));


   // start ===MPI===
   int i_tmp=0;

   for (iX = iXmin; iX <= iXmax; iX++)
   for (iY = iYmin; iY <= iYmax; iY++)
   {
      index = iY-1 + _nYBins * (iX-1);
      _phisc_tmp_local[i_tmp]= _in[index] * factor;
      i_tmp++;
   }

    if(i_flag){
      MPI_Allreduce ( _phisc_tmp_local,_phisc_tmp_global,i_tmp,MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    }
    else{
      for(i=0;i<i_tmp;i++){ _phisc_tmp_global[i]= _phisc_tmp_local[i]; }
    }

   i_tmp=0;

   for (iX = iXmin; iX <= iXmax; iX++)
   for (iY = iYmin; iY <= iYmax; iY++)
   {
      _phisc(iX, iY) = _phisc_tmp_global[i_tmp];
      i_tmp++;
   }
  // stop ===MPI===

   // Now satisfy conducting wall boundary conditions on the beampipe.

   if (_BPShape != "None"){

     //this is an old slow way to update potential with boundary condition
     //_LSQBP(iXmin, iXmax, iYmin, iYmax);

     //this is a new fast way to update potential with boundary condition
     _boundary->addBoundaryPotential(_phisc,iXmin, iXmax, iYmin, iYmax);

   }
   // Set some values needed for longitudinal coefficient later:

   _LFactor = 1.;

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    PotentialTransSC::_updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with a PotentialTransSC. The Transverse space
//    charge kick is added to each macro particle here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void PotentialTransSC::_updatePartAtNode(MacroPart &mp)
{
  Real f1, f2, f3, f4, fx, fy;
  Integer j, iX, iY;

    if(mp._globalNMacros < _nMacrosMin && mp._feelsHerds == 0) return;

    for (j = 1; j <= mp._nMacros; j++)
    {

     iX = mp._xBin(j);
     iY = mp._yBin(j);

     Real xFract = mp._xFractBin(j);
     Real yFract = mp._yFractBin(j);

     // TSC interpolation, see Hockney and Eastwood:

     Real Wxm = 0.5 * (0.5 - xFract)
	            * (0.5 - xFract);
     Real Wx0 = 0.75 - xFract * xFract;
     Real Wxp = 0.5 * (0.5 + xFract)
	            * (0.5 + xFract);
     Real Wym = 0.5 * (0.5 - yFract)
	            * (0.5 - yFract);
     Real Wy0 = 0.75 - yFract * yFract;
     Real Wyp = 0.5 * (0.5 + yFract)
	            * (0.5 + yFract);
     Real dWxm = -(0.5 - xFract) / _dx;
     Real dWx0 = - 2. * xFract / _dx;
     Real dWxp =  (0.5 + xFract) / _dx;
     Real dWym = -(0.5 - yFract) / _dy;
     Real dWy0 = - 2. * yFract / _dy;
     Real dWyp =  (0.5 + yFract) / _dy;

     fx = dWxm * Wym * _phisc(iX-1, iY-1) +
          dWxm * Wy0 * _phisc(iX-1, iY) +
          dWxm * Wyp * _phisc(iX-1, iY+1) +
          dWx0 * Wym * _phisc(iX, iY-1) +
          dWx0 * Wy0 * _phisc(iX, iY) +
          dWx0 * Wyp * _phisc(iX, iY+1) +
          dWxp * Wym * _phisc(iX+1, iY-1) +
          dWxp * Wy0 * _phisc(iX+1, iY) +
          dWxp * Wyp * _phisc(iX+1, iY+1);

     fy = Wxm * dWym * _phisc(iX-1, iY-1) +
          Wxm * dWy0 * _phisc(iX-1, iY) +
          Wxm * dWyp * _phisc(iX-1, iY+1) +
          Wx0 * dWym * _phisc(iX, iY-1) +
          Wx0 * dWy0 * _phisc(iX, iY) +
          Wx0 * dWyp * _phisc(iX, iY+1) +
          Wxp * dWym * _phisc(iX+1, iY-1) +
          Wxp * dWy0 * _phisc(iX+1, iY) +
          Wxp * dWyp * _phisc(iX+1, iY+1);

  // Include local line density effect, if it is available

      if(mp._longBinningDone)  _LFactor = mp._LPosFactor(j);

      mp._xp(j) += fx * 1000. * _lkick * _LFactor;
      mp._yp(j) += fy * 1000. * _lkick * _LFactor;

    }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    PotentialTransSC::_GridExtent
//
// DESCRIPTION
//    Finds the extent of the grid using the particle distribution and
//    the boundary parameters.
//
///////////////////////////////////////////////////////////////////////////

Void PotentialTransSC::_GridExtent(MacroPart &mp)
{
   mp._findXYPExtrema();

 // Define grid extrema (start with the actual particle extent,
 //                      then consider beam pipe, and double the size):

 // rectangular grid:

   _xGridMin = mp._xMin;
   _xGridMax = mp._xMax;
   _yGridMin = mp._yMin;
   _yGridMax = mp._yMax;

   if (_BPShape == "Circle")
   {
     if (_xGridMin > -_BPr)
       _xGridMin = -_BPr;
     if (_xGridMax < _BPr)
       _xGridMax = _BPr;
     if (_yGridMin > -_BPr)
       _yGridMin = -_BPr;
     if (_yGridMax < _BPr)
       _yGridMax = _BPr;
   }

   if (_BPShape == "Ellipse")
   {
     if (_xGridMin > -_BPa)
       _xGridMin = -_BPa;
     if (_xGridMax < _BPa)
       _xGridMax = _BPa;
     if (_yGridMin > -_BPb)
       _yGridMin = -_BPb;
     if (_yGridMax < _BPb)
       _yGridMax = _BPb;
   }

   if (_BPShape == "Rectangle")
   {
     if (_xGridMin > _BPxMin)
       _xGridMin = _BPxMin;
     if (_xGridMax < _BPxMax)
       _xGridMax = _BPxMax;
     if (_yGridMin >  _BPyMin)
       _yGridMin = _BPyMin;
     if (_yGridMax < _BPyMax)
       _yGridMax = _BPyMax;
   }

   _xGridMin = _xGridMin * _Gridfact;
   _xGridMax = _xGridMax * _Gridfact;
   _yGridMin = _yGridMin * _Gridfact;
   _yGridMax = _yGridMax * _Gridfact;


   //this is parallel ====MPI====
  int i_flag;
  MPI_Initialized(&i_flag);

  if(i_flag){

   double d_local[4];
   double d_global[4];
   d_local[0]=-_xGridMin ;
   d_local[1]= _xGridMax;
   d_local[2]=-_yGridMin;
   d_local[3]= _yGridMax;

   MPI_Allreduce ( d_local,d_global,4,MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

   _xGridMin = - d_global[0];
   _xGridMax =   d_global[1];
   _yGridMin = - d_global[2];
   _yGridMax =   d_global[3];

  }



}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    PotentialTransSC::_LSQBP
//
// DESCRIPTION
//    Evaluates residual on beam pipe.
//
///////////////////////////////////////////////////////////////////////////

Void PotentialTransSC::_LSQBP(Integer &iXmin, Integer &iXmax,
                           Integer &iYmin, Integer &iYmax)
{
  Integer iB, j, k, index, iX, iY;
  Real xFract, yFract, Wxm, Wx0, Wxp, Wym, Wy0, Wyp;
  Matrix(Real) M;
  Vector(Real) V, W;
  Integer nparams = 2*_BPModes+1;

  for (iB = 1; iB <= _BPPoints; iB++)
  {
     iX = 1 + Integer((_BPx(iB)-_xGridMin)/_dx + 0.5);
     iY = 1 + Integer((_BPy(iB)-_yGridMin)/_dy + 0.5);

     if(iX < iXmin+1) iX = iXmin+1; // keep particle within mainherd grid
     if (iX > iXmax-1) iX = iXmax-1;
     if(iY < iYmin+1) iY = iYmin+1;
     if (iY > iYmax-1) iY = iYmax-1;

     xFract = (_BPx(iB) - _xGrid(iX))/_dx;
     yFract = (_BPy(iB) - _yGrid(iY))/_dy;

     // TSC interpolation, see Hockney and Eastwood:

     Real Wxm = 0.5 * (0.5 - xFract)
	            * (0.5 - xFract);
     Real Wx0 = 0.75 - xFract * xFract;
     Real Wxp = 0.5 * (0.5 + xFract)
	            * (0.5 + xFract);
     Real Wym = 0.5 * (0.5 - yFract)
	            * (0.5 - yFract);
     Real Wy0 = 0.75 - yFract * yFract;
     Real Wyp = 0.5 * (0.5 + yFract)
	            * (0.5 + yFract);
     _BPPhi(iB) = Wxm * Wym * _phisc(iX-1, iY-1) +
                  Wxm * Wy0 * _phisc(iX-1, iY) +
                  Wxm * Wyp * _phisc(iX-1, iY+1) +
                  Wx0 * Wym * _phisc(iX, iY-1) +
                  Wx0 * Wy0 * _phisc(iX, iY) +
                  Wx0 * Wyp * _phisc(iX, iY+1) +
                  Wxp * Wym * _phisc(iX+1, iY-1) +
                  Wxp * Wy0 * _phisc(iX+1, iY) +
                  Wxp * Wyp * _phisc(iX+1, iY+1);
  }

  M.resize(nparams,nparams);
  W.resize(nparams);
  V.resize(nparams);

  M = 0.;
  V = 0.;
  W = 0.;

  for (iB = 1; iB <= _BPPoints; iB++)
  {
     for (k = 1; k <= nparams; k++)
     {
        W(k) += _BPPhi(iB) * _BPPhiH(iB,k);
        for (j=1; j <= nparams; j++)
        {
	   M(j,k) += _BPPhiH(iB,j) * _BPPhiH(iB,k);
	}
     }
  }

  MathLib::solveLinear(M, W, V);

  for (k=1; k <= nparams; k++)
  {
     _BPH(k) = V(k);
  }

  _BPResid = 0.;
  for (iB = 1; iB <= _BPPoints; iB++)
  {
     for (k = 1; k <= nparams; k++)
     {
        _BPPhi(iB) -= _BPH(k) * _BPPhiH(iB,k);
     }
     _BPResid += _BPPhi(iB) * _BPPhi(iB);
  }
  _BPResid = pow(_BPResid,0.5) / _BPPoints;

  Real MaxPhi = 0.;
  for (iX = iXmin; iX <= iXmax; iX++)
  {
     for (iY = iYmin; iY <= iYmax; iY++)
     {
        Real rsq = _xGrid(iX) * _xGrid(iX) + _yGrid(iY) * _yGrid(iY);
        Real r = pow(rsq,0.5);
        Real theta = atan2(_yGrid(iY),_xGrid(iX));
        Real rk = 1.0;
        index = 1;
        Real Mode = 1.0;
        _phisc(iX,iY) -= _BPH(index) * Mode;
        for (k = 1; k<= _BPModes; k++)
        {
	   rk *= r / _BPrnorm;
           index++;
           Mode = rk * cos(k*theta);
           _phisc(iX,iY) -= _BPH(index) * Mode;
           index++;
           Mode = rk * sin(k*theta);
           _phisc(iX,iY) -= _BPH(index) * Mode;
        }
	if (MaxPhi < _phisc(iX,iY)) MaxPhi = _phisc(iX,iY);
	if (MaxPhi < -_phisc(iX,iY)) MaxPhi = -_phisc(iX,iY);
     }
  }
  _BPResid /= MaxPhi;
  _resid = _BPResid;

}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//
//   FMMTransSC
//
// INHERITANCE RELATIONSHIPS
//   FMMTransSC -> TransSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for space charge calculation with beam pipe and
//   conducting wall boundary condition.
//   It uses a fast multipole method (FFM) to calculate the space charge
//   potential and force, and a least squares solution to satisfy the beam
//   pipe conducting wall boundary condition.
//
//   The parameters BP1, BP2, BP3, BP4 have different meanings for different
//   shapes:
//   shape = "Ellipse" BP1 is 1/2 horizontal size, BP2 - 1/2 vertical size
//   shape = "Circle" BP1 - radius
//   shape = "Rectangle" BP1 - minX, BP2 - maxX, BP3 - minY, BP4 - maxY
//   Shape should be "Ellipse","Rectangle", "Circle", "None"
//   Sizes in [mm]
//
///////////////////////////////////////////////////////////////////////////

class FMMTransSC : public TransSC
{
  Declare_Standard_Members(FMMTransSC, TransSC);

  public:
  FMMTransSC(const String &n, const Integer &order,
             Integer &nTerms, Integer &nLevels,
             Integer &nXYBins, Integer &nZBins,
             Real &eps, Real &length,
             const String &BPShape,
             Real &BP1, Real &BP2, Real &BP3, Real &BP4,
             Integer &BPPoints, Integer &BPModes,
             Real &BPResid, Real &Gridfact,
             const Integer &nMacrosMin);
  ~FMMTransSC();
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _GridExtent(MacroPart &mp);
  Void _Invert(double** M, double** MINV, int n);

  static int _nTerms, _nLevels, _nXYBins, _nZBins;
  static String _BPShape;
  static int _BPPoints, _BPModes;
  static double   _BPResid;
  static double   _Gridfact;
  static double   _BPrnorm;
  static double*  _BPx;
  static double*  _BPy;
  static double** _BPPhiH1;
  static double** _BPPhiH2;

  static double _BPr, _BPa, _BPb, _BPxMin, _BPxMax, _BPyMin, _BPyMax;

  private:
  static FMMCalculator* _FMM;
  static int _initialized;
};

///////////////////////////////////////////////////////////////////////////
//
// Static MEMBERS FOR CLASS FMMTransSC
//
///////////////////////////////////////////////////////////////////////////

  int FMMTransSC::_nTerms;
  int FMMTransSC::_nLevels;
  int FMMTransSC::_nXYBins;
  int FMMTransSC::_nZBins;
  String FMMTransSC::_BPShape;
  int FMMTransSC::_BPPoints;
  int FMMTransSC::_BPModes;
  double   FMMTransSC::_BPResid;
  double   FMMTransSC::_Gridfact;
  double   FMMTransSC::_BPrnorm;

  double*  FMMTransSC::_BPx;
  double*  FMMTransSC::_BPy;
  double** FMMTransSC::_BPPhiH1;
  double** FMMTransSC::_BPPhiH2;

  double FMMTransSC::_BPr;
  double FMMTransSC::_BPa;
  double FMMTransSC::_BPb;
  double FMMTransSC::_BPxMin;
  double FMMTransSC::_BPxMax;
  double FMMTransSC::_BPyMin;
  double FMMTransSC::_BPyMax;

  FMMCalculator* FMMTransSC::_FMM;

  int FMMTransSC::_initialized = 0;

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FMMTransSC
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(FMMTransSC, TransSC);

FMMTransSC::FMMTransSC(const String &n, const Integer &order,
                       Integer &nTerms, Integer &nLevels,
                       Integer &nXYBins, Integer &nZBins,
                       Real &eps, Real &length,
                       const String &BPShape,
                       Real &BP1, Real &BP2, Real &BP3, Real &BP4,
                       Integer &BPPoints, Integer &BPModes,
                       Real &BPResid, Real &Gridfact,
                       const Integer &nMacrosMin) :
                       TransSC(n, order, nXYBins, nZBins, length,
                       nMacrosMin, eps)
{
  if(_initialized) return;

  _nTerms  = int(nTerms);
  _nLevels = int(nLevels);
  _nXYBins  = int(nXYBins);
  _nZBins  = int(nZBins);
  _BPShape  = BPShape;
  _BPPoints = 4 * (int(BPPoints) / 4);
  _BPModes  = int(BPModes);
  if(_BPShape == "None")
  {
    _BPPoints = 0;
    _BPModes  = 0;
  }
  _BPResid  = double(BPResid);
  _Gridfact = double(Gridfact);
  _BPx      = new double[_BPPoints];
  _BPy      = new double[_BPPoints];
  _BPPhiH1  = new double*[_BPPoints];
  _BPPhiH2  = new double*[2 * _BPModes + 1];

  int i;

  for(i = 0; i < _BPPoints; i++)
  {
    _BPPhiH1[i] = new double[2 * _BPModes + 1];
  }

  for(i = 0; i < (2 * _BPModes + 1); i++)
  {
    _BPPhiH2[i] = new double[2 * _BPModes + 1];
  }

  double* rad;
  double* th;
  rad = new double[_BPPoints];
  th  = new double[_BPPoints];

  if(_BPShape == "Circle")
  {
    _BPr = double(BP1);
    _BPrnorm = _BPr;
    int i;
    double dtheta = 2. * Consts::pi / _BPPoints;
    for (i = 0; i < _BPPoints; i++)
    {
      rad[i] = _BPr;
      th[i]   = i * dtheta;
      _BPx[i] = _BPr * cos(th[i]);
      _BPy[i] = _BPr * sin(th[i]);
    }
  }

  if(_BPShape == "Ellipse")
  {
    _BPa = double(BP1);
    _BPb = double(BP2);
    double BPrsq = _BPa * _BPb;
    double BPasq = _BPa * _BPa;
    double BPbsq = _BPb * _BPb;
    _BPrnorm = pow(BPrsq, 0.5);
    int BPPO4 = _BPPoints / 4;

    double ds = 2. * double(Consts::pi) * _BPrnorm / _BPPoints;
    double rsq, term1, term2, dtheta, theta;
    double resid = 1.0;

    while(resid > 1.0e-08 || resid < -1.0e-08)
    {
      term1    = BPbsq;
      term2    = 0.0;
      rsq      = BPasq;
      dtheta   = ds / pow(rsq * (1.0 + term2 / (term1 * term1)), 0.5);
      theta    = dtheta / 2.;
      th[0] = 0.;
      for(i = 0; i < BPPO4; i++)
      {
        double sn2 = sin(theta) * sin(theta);
        term1 = BPbsq + (BPasq - BPbsq) * sn2;
        term2 = (BPbsq - BPasq) * (BPbsq - BPasq) * sn2 * (1.0 - sn2);
        rsq   = BPrsq * BPrsq / term1;
        dtheta = ds / pow(rsq * (1.0 + term2 / (term1 * term1)), 0.5);
        th[i + 1] = th[i] + dtheta;
        theta += dtheta;
      }
      resid = th[BPPO4] - double(Consts::pi) / 2.;
      ds *= double(Consts::pi) / (2. * th[BPPO4]);
    }

    int i1, i2, i3;

    for(i = 0; i < BPPO4; i++)
    {
      i1 = i;
      i2 = 2 * BPPO4 - i1;
      th[i2] = double(Consts::pi) - th[i1];
    }

    for (i = 1; i < 2 * BPPO4; i++)
    {
      i1 = i;
      i3 = 2 * BPPO4 + i1;
      th[i3] = double(Consts::pi) + th[i1];
    }

    for(i = 0; i < _BPPoints; i++)
    {
      term1   = BPbsq + (BPasq - BPbsq) * sin(th[i]) * sin(th[i]);
      rad[i]  = pow(BPrsq * BPrsq / term1, 0.5);
      _BPx[i] = rad[i] * cos(th[i]);
      _BPy[i] = rad[i] * sin(th[i]);
    }
  }

  if(_BPShape == "Rectangle")
  {
    _BPxMin = double(BP1);
    _BPxMax = double(BP2);
    _BPyMin = double(BP3);
    _BPyMax = double(BP4);
    double rsq = ((_BPxMax - _BPxMin) * (_BPxMax - _BPxMin) +
                 (_BPyMax - _BPyMin) * (_BPyMax - _BPyMin)) / 4.;
    _BPrnorm   = pow(rsq, 0.5);
    int BPPO4  = _BPPoints / 4;
    double dx  = (_BPxMax - _BPxMin) / BPPO4;
    double dy  = (_BPyMax - _BPyMin) / BPPO4;
    int i1, i2, i3, i4;
    for (i = 0; i < BPPO4; i++)
    {
      i1 = i;
      _BPx[i1]   = _BPxMax;
      _BPy[i1]   = _BPyMin + i * dy;
      rad[i1]    = pow(_BPx[i1] * _BPx[i1] + _BPy[i1] * _BPy[i1], 0.5);
      th[i1]     = atan2(_BPy[i1], _BPx[i1]);

      i2 = BPPO4 + i1;
      _BPx[i2]   = _BPxMax - i * dx;
      _BPy[i2]   = _BPyMax;
      rad[i2]    = pow(_BPx[i2] * _BPx[i2] + _BPy[i2] * _BPy[i2], 0.5);
      th[i2]     = atan2(_BPy[i2], _BPx[i2]);

      i3 = BPPO4 + i2;
      _BPx[i3]   = _BPxMin;
      _BPy[i3]   = _BPyMax - i * dy;
      rad[i3]    = pow(_BPx[i3] * _BPx[i3] + _BPy[i3] * _BPy[i3], 0.5);
      th[i3]     = atan2(_BPy[i3], _BPx[i3]);

      i4 = BPPO4 + i3;
      _BPx[i4]   = _BPxMin + i * dx;
      _BPy[i4]   = _BPyMin;
      rad[i4]    = pow(_BPx[i4] * _BPx[i4] + _BPy[i4] * _BPy[i4], 0.5);
      th[i4]     = atan2(_BPy[i4], _BPx[i4]);
    }
  }

  if(_BPShape != "None")
  {
    double rfac, rj;
    int j, jc, js;

    for(i = 0; i < _BPPoints; i++)
    {
      rfac = rad[i] / _BPrnorm;
      rj = 1;
      _BPPhiH1[i][0] = 1.0;
      for (j = 1; j <= _BPModes; j++)
      {
        jc = 2 * j - 1;
        js = 2 * j;
        rj *= rfac;
        _BPPhiH1[i][jc] = rj * cos(j * th[i]);
        _BPPhiH1[i][js] = rj * sin(j * th[i]);
      }
    }

    double** TEMP;
    TEMP = new double*[2 * _BPModes + 1];
    for(i = 0; i < (2 * _BPModes + 1); i++)
    {
      TEMP[i] = new double[2 * _BPModes + 1];
    }

    for(i = 0; i < (2 * _BPModes + 1); i++)
    {
      for(j = 0; j < (2 * _BPModes + 1); j++)
      {
        int k;
        TEMP[i][j] = 0.0;
        for(k = 0; k < _BPPoints; k++)
        {
          TEMP[i][j] += _BPPhiH1[k][i] * _BPPhiH1[k][j];
        }
      }
    }

    _Invert(TEMP, _BPPhiH2, 2 * _BPModes + 1);

    for(i = 0; i < (2 * _BPModes + 1); i++)
    {
      delete TEMP[i];
    }
    delete TEMP;
  }

  delete rad;
  delete th;

  double ZLength = double(2. * Consts::pi);

  /*
  for(i = 0; i < _BPPoints; i++)
  {
    cout << i << "  " << _BPx[i] << "  " << _BPy[i] << "\n";
  }
  */

  _FMM = new FMMCalculator(_nTerms  , _nXYBins, _eps,
                           _nZBins  ,  ZLength,
                           _BPPoints, _BPModes, _BPrnorm, _BPResid,
                           _BPx     , _BPy    , _BPPhiH1, _BPPhiH2);

  _initialized = 1;
}

FMMTransSC::~FMMTransSC()
{
  delete _BPx;
  delete _BPy;
  int i;
  for(i = 0; i < _BPPoints; i++)
  {
    delete _BPPhiH1[i];
  }
  for(i = 0; i < 2 * _BPModes + 1; i++)
  {
    delete _BPPhiH2[i];
  }
  delete _BPPhiH1;
  delete _BPPhiH2;
  delete _FMM;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FMMTransSC::NodeCalculator
//
// DESCRIPTION
//    Finds grid boundaries and sets up force multipliers
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FMMTransSC::_nodeCalculator(MacroPart &mp)
{
  int i_flag;
  MPI_Initialized(&i_flag);

  if(i_flag)
  {
    MPI_Allreduce(&mp._nMacros, &mp._globalNMacros, 1,
                  MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }
  else
  {
    mp._globalNMacros = mp._nMacros;
  }

  if(mp._globalNMacros < _nMacrosMin) return;

  // Find particle grid extent:

  _GridExtent(mp);

  double _phiMax_tmp_local = mp._phiMax;
  double _phiMin_tmp_local = mp._phiMin;
  double _phiMax_tmp_global;
  double _phiMin_tmp_global;

  if(i_flag)
  {
    MPI_Allreduce(&_phiMax_tmp_local, &_phiMax_tmp_global, 1,
                  MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(&_phiMin_tmp_local, &_phiMin_tmp_global, 1,
                  MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  }
  else
  {
    mp._globalNMacros  = mp._nMacros;
    _phiMax_tmp_global = _phiMax_tmp_local;
    _phiMin_tmp_global = _phiMin_tmp_local;
  }

  _FMM->_SetCoords(_xGridMin, _xGridMax, _yGridMin, _yGridMax);
  _FMM->_DistributeParts(mp);
  _FMM->_SeedMultipoles(mp);
  _FMM->_UpSweep();
  _FMM->_DownSweep();
  _FMM->_ForceFromBeam(mp);
  if(_BPShape != "None") _FMM->_ForceFromBoundary(mp);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FMMTransSC::_updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with a FMMTransSC. The Transverse space
//    charge kick is added to each macro particle here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FMMTransSC::_updatePartAtNode(MacroPart &mp)
{
  // Use the average line density "lambda" for a single macroparticle.
  // The FMM calculator will sum over all macroparticles,
  // weighted by longitudinal density.

  _lambda = Injection::nReals_Macro * Ring::harmonicNumber / Ring::lRing;

  double SCKick_Coeff = 1000000. * 2.0 * _lambda * _lkick *
                        mp._syncPart._charge * mp._syncPart._charge *
                        rClassical /
                       (mp._syncPart._betaSync *
                        mp._syncPart._betaSync *
                        mp._syncPart._gammaSync *
                        mp._syncPart._gammaSync *
                        mp._syncPart._gammaSync * mp._syncPart._mass);
  _FMM->_ApplyForce(mp, SCKick_Coeff);
  _FMM->_ClearFMM();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FMMTransSC::_GridExtent
//
// DESCRIPTION
//   Finds the extent of the grid using the particle distribution and
//   the boundary parameters.
//
///////////////////////////////////////////////////////////////////////////

Void FMMTransSC::_GridExtent(MacroPart &mp)
{
  Real GridCenter, HalfGrid;

  mp._findXYPExtrema();

  // Define grid extrema (start with the actual particle extent,
  //                      then consider beam pipe, and select the size):

  // rectangular grid:

  _xGridMin = mp._xMin;
  _xGridMax = mp._xMax;
  _yGridMin = mp._yMin;
  _yGridMax = mp._yMax;

  if(_BPShape == "Circle")
  {
    if(_xGridMin > -_BPr) _xGridMin = -_BPr;
    if(_xGridMax <  _BPr) _xGridMax =  _BPr;
    if(_yGridMin > -_BPr) _yGridMin = -_BPr;
    if(_yGridMax <  _BPr) _yGridMax =  _BPr;
  }

  if (_BPShape == "Ellipse")
  {
    if(_xGridMin > -_BPa) _xGridMin = -_BPa;
    if(_xGridMax <  _BPa) _xGridMax =  _BPa;
    if(_yGridMin > -_BPb) _yGridMin = -_BPb;
    if(_yGridMax <  _BPb) _yGridMax =  _BPb;
  }

  if (_BPShape == "Rectangle")
  {
    if(_xGridMin > _BPxMin) _xGridMin = _BPxMin;
    if(_xGridMax < _BPxMax) _xGridMax = _BPxMax;
    if(_yGridMin > _BPyMin) _yGridMin = _BPyMin;
    if(_yGridMax < _BPyMax) _yGridMax = _BPyMax;
  }

  GridCenter = (_xGridMax + _xGridMin) / 2.0;
  HalfGrid   = _Gridfact * (_xGridMax - _xGridMin) / 2.0;
  _xGridMin = GridCenter - HalfGrid;
  _xGridMax = GridCenter + HalfGrid;
  GridCenter = (_yGridMax + _yGridMin) / 2.0;
  HalfGrid   = _Gridfact * (_yGridMax - _yGridMin) / 2.0;
  _yGridMin = GridCenter - HalfGrid;
  _yGridMax = GridCenter + HalfGrid;

  // Make it square for fast multipoles:

  if((_xGridMax - _xGridMin) < (_yGridMax - _yGridMin))
  {
    GridCenter = (_xGridMax + _xGridMin) / 2.0;
    HalfGrid   = (_yGridMax - _yGridMin) / 2.0;
    _xGridMin  = GridCenter - HalfGrid;
    _xGridMax  = GridCenter + HalfGrid;
  }

  if((_yGridMax - _yGridMin) < (_xGridMax - _xGridMin))
  {
    GridCenter = (_yGridMax + _yGridMin) / 2.0;
    HalfGrid   = (_xGridMax - _xGridMin) / 2.0;
    _yGridMin  = GridCenter - HalfGrid;
    _yGridMax  = GridCenter + HalfGrid;
  }

  //this is parallel ====MPI====
  int i_flag;
  MPI_Initialized(&i_flag);

  if(i_flag)
  {
    double d_local[4];
    double d_global[4];
    d_local[0]=-_xGridMin ;
    d_local[1]= _xGridMax;
    d_local[2]=-_yGridMin;
    d_local[3]= _yGridMax;

    MPI_Allreduce(d_local, d_global, 4, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    _xGridMin = - d_global[0];
    _xGridMax =   d_global[1];
    _yGridMin = - d_global[2];
    _yGridMax =   d_global[3];
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FMMTransSC::_Invert
//
// DESCRIPTION
//   Inverts nxn matrix M returning result as MINV
//
///////////////////////////////////////////////////////////////////////////

Void FMMTransSC::_Invert(double** M, double** MINV, int n)
{
  int i, j, k, ipvt;
  double Max, Coeff;

  double* Temp;
  Temp = new double[n];

  for(i = 0; i < n; i++)
  {
    for(j = 0; j < n; j++)
    {
      MINV[i][j] = 0.0;
    }
    MINV[i][i] = 1.0;
  }

  for(j = 0; j < n; j++)
  {
    Max = 0.0;
    ipvt = j;

    for(i = j; i < n; i++)
    {
      if(M[i][j] > Max)
      {
        Max = M[i][j];
        ipvt = i;
      }
      if(-M[i][j] > Max)
      {
        Max = -M[i][j];
        ipvt = i;
      }
    }

    if(ipvt != j)
    {
      for(k = 0; k < n; k++)
      {
        Temp[k]       = M[ipvt][k];
        M[ipvt][k]    = M[j][k];
        M[j][k]       = Temp[k];
        Temp[k]       = MINV[ipvt][k];
        MINV[ipvt][k] = MINV[j][k];
        MINV[j][k]    = Temp[k];
      }
    }

    for(i = 0; i < n; i++)
    {
      if(i != j)
      {
        Coeff = M[i][j] / M[j][j];
        for(k = j; k < n; k++)
        {
          M[i][k] -= Coeff * M[j][k];
        }
        M[i][j] = 0.0;
        for(k = 0; k < n; k++)
        {
          MINV[i][k] -= Coeff * MINV[j][k];
        }
      }
    }

    Coeff = M[j][j];
    for(k = j; k < n; k++)
    {
      M[j][k] /= Coeff;
    }
    for(k = 0; k < n; k++)
    {
      MINV[j][k] /= Coeff;
    }
  }

  delete Temp;
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//
//   ElliptTransSC
//
// INHERITANCE RELATIONSHIPS
//   ElliptTransSC -> TransSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for space charge calculation with beam pipe and
//   uniform elliptical space charge distribution
//   It uses rms widths to set up charge distribution,
//   and a least squares solution to satisfy the beam
//   pipe conducting wall boundary condition.
//
//   The parameters BP1, BP2, BP3, BP4 have different meanings for different
//   shapes:
//   shape = "Ellipse" BP1 is 1/2 horizontal size, BP2 - 1/2 vertical size
//   shape = "Circle" BP1 - radius
//   shape = "Rectangle" BP1 - minX, BP2 - maxX, BP3 - minY, BP4 - maxY
//   Shape should be "Ellipse","Rectangle", "Circle", "None"
//   Sizes in [mm]
//
///////////////////////////////////////////////////////////////////////////

class ElliptTransSC : public TransSC
{
  Declare_Standard_Members(ElliptTransSC, TransSC);

  public:
  ElliptTransSC(const String &n, const Integer &order,
                Integer &nZBins, Real &length,
                const String &BPShape,
                Real &BP1, Real &BP2, Real &BP3, Real &BP4,
                Integer &BPPoints, Integer &BPModes,
                Real &BPResid,
                const Integer &nMacrosMin);
  ~ElliptTransSC();
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _Invert(double** M, double** MINV, int n);

  static int _nZBins;
  static String _BPShape;
  static int _BPPoints, _BPModes;
  static double   _BPResid;
  static double   _BPrnorm;
  static double*  _BPx;
  static double*  _BPy;
  static double** _BPPhiH1;
  static double** _BPPhiH2;

  static double _BPr, _BPa, _BPb, _BPxMin, _BPxMax, _BPyMin, _BPyMax;

  private:
  static ElliptCalculator* _Ellipt;
  static int _initialized;
};

///////////////////////////////////////////////////////////////////////////
//
// Static MEMBERS FOR CLASS ElliptTransSC
//
///////////////////////////////////////////////////////////////////////////

  int ElliptTransSC::_nZBins;
  String ElliptTransSC::_BPShape;
  int ElliptTransSC::_BPPoints;
  int ElliptTransSC::_BPModes;
  double   ElliptTransSC::_BPResid;
  double   ElliptTransSC::_BPrnorm;

  double*  ElliptTransSC::_BPx;
  double*  ElliptTransSC::_BPy;
  double** ElliptTransSC::_BPPhiH1;
  double** ElliptTransSC::_BPPhiH2;

  double ElliptTransSC::_BPr;
  double ElliptTransSC::_BPa;
  double ElliptTransSC::_BPb;
  double ElliptTransSC::_BPxMin;
  double ElliptTransSC::_BPxMax;
  double ElliptTransSC::_BPyMin;
  double ElliptTransSC::_BPyMax;

  ElliptCalculator* ElliptTransSC::_Ellipt;

  int ElliptTransSC::_initialized = 0;

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS ElliptTransSC
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(ElliptTransSC, TransSC);

ElliptTransSC::ElliptTransSC(const String &n, const Integer &order,
                             Integer &nZBins, Real &length,
                             const String &BPShape,
                             Real &BP1, Real &BP2, Real &BP3, Real &BP4,
                             Integer &BPPoints, Integer &BPModes,
                             Real &BPResid,
                             const Integer &nMacrosMin) :
                             TransSC(n, order, nZBins, nZBins, length,
                             nMacrosMin, length)
{
  if(_initialized) return;

  _nZBins  = int(nZBins);
  _BPShape  = BPShape;
  _BPPoints = 4 * (int(BPPoints) / 4);
  _BPModes  = int(BPModes);
  if(_BPShape == "None")
  {
    _BPPoints = 0;
    _BPModes  = 0;
  }
  _BPResid  = double(BPResid);
  _BPx      = new double[_BPPoints];
  _BPy      = new double[_BPPoints];
  _BPPhiH1  = new double*[_BPPoints];
  _BPPhiH2  = new double*[2 * _BPModes + 1];

  int i;

  for(i = 0; i < _BPPoints; i++)
  {
    _BPPhiH1[i] = new double[2 * _BPModes + 1];
  }

  for(i = 0; i < (2 * _BPModes + 1); i++)
  {
    _BPPhiH2[i] = new double[2 * _BPModes + 1];
  }

  double* rad;
  double* th;
  rad = new double[_BPPoints];
  th  = new double[_BPPoints];

  if(_BPShape == "Circle")
  {
    _BPr = double(BP1);
    _BPrnorm = _BPr;
    int i;
    double dtheta = 2. * Consts::pi / _BPPoints;
    for (i = 0; i < _BPPoints; i++)
    {
      rad[i] = _BPr;
      th[i]   = i * dtheta;
      _BPx[i] = _BPr * cos(th[i]);
      _BPy[i] = _BPr * sin(th[i]);
    }
  }

  if(_BPShape == "Ellipse")
  {
    _BPa = double(BP1);
    _BPb = double(BP2);
    double BPrsq = _BPa * _BPb;
    double BPasq = _BPa * _BPa;
    double BPbsq = _BPb * _BPb;
    _BPrnorm = pow(BPrsq, 0.5);
    int BPPO4 = _BPPoints / 4;

    double ds = 2. * double(Consts::pi) * _BPrnorm / _BPPoints;
    double rsq, term1, term2, dtheta, theta;
    double resid = 1.0;

    while(resid > 1.0e-08 || resid < -1.0e-08)
    {
      term1    = BPbsq;
      term2    = 0.0;
      rsq      = BPasq;
      dtheta   = ds / pow(rsq * (1.0 + term2 / (term1 * term1)), 0.5);
      theta    = dtheta / 2.;
      th[0] = 0.;
      for(i = 0; i < BPPO4; i++)
      {
        double sn2 = sin(theta) * sin(theta);
        term1 = BPbsq + (BPasq - BPbsq) * sn2;
        term2 = (BPbsq - BPasq) * (BPbsq - BPasq) * sn2 * (1.0 - sn2);
        rsq   = BPrsq * BPrsq / term1;
        dtheta = ds / pow(rsq * (1.0 + term2 / (term1 * term1)), 0.5);
        th[i + 1] = th[i] + dtheta;
        theta += dtheta;
      }
      resid = th[BPPO4] - double(Consts::pi) / 2.;
      ds *= double(Consts::pi) / (2. * th[BPPO4]);
    }

    int i1, i2, i3;

    for(i = 0; i < BPPO4; i++)
    {
      i1 = i;
      i2 = 2 * BPPO4 - i1;
      th[i2] = double(Consts::pi) - th[i1];
    }

    for (i = 1; i < 2 * BPPO4; i++)
    {
      i1 = i;
      i3 = 2 * BPPO4 + i1;
      th[i3] = double(Consts::pi) + th[i1];
    }

    for(i = 0; i < _BPPoints; i++)
    {
      term1   = BPbsq + (BPasq - BPbsq) * sin(th[i]) * sin(th[i]);
      rad[i]  = pow(BPrsq * BPrsq / term1, 0.5);
      _BPx[i] = rad[i] * cos(th[i]);
      _BPy[i] = rad[i] * sin(th[i]);
    }
  }

  if(_BPShape == "Rectangle")
  {
    _BPxMin = double(BP1);
    _BPxMax = double(BP2);
    _BPyMin = double(BP3);
    _BPyMax = double(BP4);
    double rsq = ((_BPxMax - _BPxMin) * (_BPxMax - _BPxMin) +
                 (_BPyMax - _BPyMin) * (_BPyMax - _BPyMin)) / 4.;
    _BPrnorm   = pow(rsq, 0.5);
    int BPPO4  = _BPPoints / 4;
    double dx  = (_BPxMax - _BPxMin) / BPPO4;
    double dy  = (_BPyMax - _BPyMin) / BPPO4;
    int i1, i2, i3, i4;
    for (i = 0; i < BPPO4; i++)
    {
      i1 = i;
      _BPx[i1]   = _BPxMax;
      _BPy[i1]   = _BPyMin + i * dy;
      rad[i1]    = pow(_BPx[i1] * _BPx[i1] + _BPy[i1] * _BPy[i1], 0.5);
      th[i1]     = atan2(_BPy[i1], _BPx[i1]);

      i2 = BPPO4 + i1;
      _BPx[i2]   = _BPxMax - i * dx;
      _BPy[i2]   = _BPyMax;
      rad[i2]    = pow(_BPx[i2] * _BPx[i2] + _BPy[i2] * _BPy[i2], 0.5);
      th[i2]     = atan2(_BPy[i2], _BPx[i2]);

      i3 = BPPO4 + i2;
      _BPx[i3]   = _BPxMin;
      _BPy[i3]   = _BPyMax - i * dy;
      rad[i3]    = pow(_BPx[i3] * _BPx[i3] + _BPy[i3] * _BPy[i3], 0.5);
      th[i3]     = atan2(_BPy[i3], _BPx[i3]);

      i4 = BPPO4 + i3;
      _BPx[i4]   = _BPxMin + i * dx;
      _BPy[i4]   = _BPyMin;
      rad[i4]    = pow(_BPx[i4] * _BPx[i4] + _BPy[i4] * _BPy[i4], 0.5);
      th[i4]     = atan2(_BPy[i4], _BPx[i4]);
    }
  }

  if(_BPShape != "None")
  {
    double rfac, rj;
    int j, jc, js;

    for(i = 0; i < _BPPoints; i++)
    {
      rfac = rad[i] / _BPrnorm;
      rj = 1;
      _BPPhiH1[i][0] = 1.0;
      for (j = 1; j <= _BPModes; j++)
      {
        jc = 2 * j - 1;
        js = 2 * j;
        rj *= rfac;
        _BPPhiH1[i][jc] = rj * cos(j * th[i]);
        _BPPhiH1[i][js] = rj * sin(j * th[i]);
      }
    }

    double** TEMP;
    TEMP = new double*[2 * _BPModes + 1];
    for(i = 0; i < (2 * _BPModes + 1); i++)
    {
      TEMP[i] = new double[2 * _BPModes + 1];
    }

    for(i = 0; i < (2 * _BPModes + 1); i++)
    {
      for(j = 0; j < (2 * _BPModes + 1); j++)
      {
        int k;
        TEMP[i][j] = 0.0;
        for(k = 0; k < _BPPoints; k++)
        {
          TEMP[i][j] += _BPPhiH1[k][i] * _BPPhiH1[k][j];
        }
      }
    }

    _Invert(TEMP, _BPPhiH2, 2 * _BPModes + 1);

    for(i = 0; i < (2 * _BPModes + 1); i++)
    {
      delete TEMP[i];
    }
    delete TEMP;
  }

  delete rad;
  delete th;

  double ZLength = double(2. * Consts::pi);

  /*
  for(i = 0; i < _BPPoints; i++)
  {
    cout << i << "  " << _BPx[i] << "  " << _BPy[i] << "\n";
  }
  */

  _Ellipt = new ElliptCalculator(_nZBins  ,  ZLength,
                                 _BPPoints, _BPModes, _BPrnorm, _BPResid,
                                 _BPx     , _BPy    , _BPPhiH1, _BPPhiH2);

  _initialized = 1;
}

ElliptTransSC::~ElliptTransSC()
{
  delete _BPx;
  delete _BPy;
  int i;
  for(i = 0; i < _BPPoints; i++)
  {
    delete _BPPhiH1[i];
  }
  for(i = 0; i < 2 * _BPModes + 1; i++)
  {
    delete _BPPhiH2[i];
  }
  delete _BPPhiH1;
  delete _BPPhiH2;
  delete _Ellipt;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    ElliptTransSC::NodeCalculator
//
// DESCRIPTION
//    Sets up uniform elliptical density parameters and force multipliers
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void ElliptTransSC::_nodeCalculator(MacroPart &mp)
{
  int i_flag;
  MPI_Initialized(&i_flag);

  if(i_flag)
  {
    MPI_Allreduce(&mp._nMacros, &mp._globalNMacros, 1,
                  MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }
  else
  {
    mp._globalNMacros = mp._nMacros;
  }

  if(mp._globalNMacros < _nMacrosMin) return;

  double _phiMax_tmp_local = mp._phiMax;
  double _phiMin_tmp_local = mp._phiMin;
  double _phiMax_tmp_global;
  double _phiMin_tmp_global;

  if(i_flag)
  {
    MPI_Allreduce(&_phiMax_tmp_local, &_phiMax_tmp_global, 1,
                  MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(&_phiMin_tmp_local, &_phiMin_tmp_global, 1,
                  MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  }
  else
  {
    mp._globalNMacros  = mp._nMacros;
    _phiMax_tmp_global = _phiMax_tmp_local;
    _phiMin_tmp_global = _phiMin_tmp_local;
  }

  _Ellipt->_SetDistribution(mp);
  if(_BPShape != "None")
  {
    _Ellipt->_BoundaryCoeffs();
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    ElliptTransSC::_updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with a ElliptTransSC. The Transverse space
//    charge kick is added to each macro particle here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void ElliptTransSC::_updatePartAtNode(MacroPart &mp)
{
  // Use the average line density "lambda" for a single macroparticle.
  // The Ellipt calculator will sum over all macroparticles,
  // weighted by longitudinal density.

  _lambda = Injection::nReals_Macro * _nZBins * Ring::harmonicNumber /
            Ring::lRing;

  double SCKick_Coeff = 1000000. * 2.0 * _lambda * _lkick *
                        mp._syncPart._charge * mp._syncPart._charge *
                        rClassical /
                       (mp._syncPart._betaSync *
                        mp._syncPart._betaSync *
                        mp._syncPart._gammaSync *
                        mp._syncPart._gammaSync *
                        mp._syncPart._gammaSync * mp._syncPart._mass);
  _Ellipt->_ApplyForce(mp, SCKick_Coeff);
  _Ellipt->_ClearEllipt();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   ElliptTransSC::_Invert
//
// DESCRIPTION
//   Inverts nxn matrix M returning result as MINV
//
///////////////////////////////////////////////////////////////////////////

Void ElliptTransSC::_Invert(double** M, double** MINV, int n)
{
  int i, j, k, ipvt;
  double Max, Coeff;

  double* Temp;
  Temp = new double[n];

  for(i = 0; i < n; i++)
  {
    for(j = 0; j < n; j++)
    {
      MINV[i][j] = 0.0;
    }
    MINV[i][i] = 1.0;
  }

  for(j = 0; j < n; j++)
  {
    Max = 0.0;
    ipvt = j;

    for(i = j; i < n; i++)
    {
      if(M[i][j] > Max)
      {
        Max = M[i][j];
        ipvt = i;
      }
      if(-M[i][j] > Max)
      {
        Max = -M[i][j];
        ipvt = i;
      }
    }

    if(ipvt != j)
    {
      for(k = 0; k < n; k++)
      {
        Temp[k]       = M[ipvt][k];
        M[ipvt][k]    = M[j][k];
        M[j][k]       = Temp[k];
        Temp[k]       = MINV[ipvt][k];
        MINV[ipvt][k] = MINV[j][k];
        MINV[j][k]    = Temp[k];
      }
    }

    for(i = 0; i < n; i++)
    {
      if(i != j)
      {
        Coeff = M[i][j] / M[j][j];
        for(k = j; k < n; k++)
        {
          M[i][k] -= Coeff * M[j][k];
        }
        M[i][j] = 0.0;
        for(k = 0; k < n; k++)
        {
          MINV[i][k] -= Coeff * MINV[j][k];
        }
      }
    }

    Coeff = M[j][j];
    for(k = j; k < n; k++)
    {
      M[j][k] /= Coeff;
    }
    for(k = 0; k < n; k++)
    {
      MINV[j][k] /= Coeff;
    }
  }

  delete Temp;
}


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//
//   CylCoordsTransSC
//
// INHERITANCE RELATIONSHIPS
//   CylCoordsTransSC -> TransSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for space charge calculation with
//   cylindrical coordinates and circular conducting wall.
//
///////////////////////////////////////////////////////////////////////////

class CylCoordsTransSC : public TransSC
{
  Declare_Standard_Members(CylCoordsTransSC, TransSC);

  public:
  CylCoordsTransSC(const String &n, const Integer &order,
                   Integer &nZBins, Integer &nRBins,
                   Integer &nThetas, Integer &nModes,
                   Real &RBnd, Integer &fInterp,
                   Real &length,
                   const Integer &nMacrosMin);
  ~CylCoordsTransSC();
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);

  static int _nZBins;
  static int _nRBins;
  static int _nThetas;
  static int _nModes;
  static double _RBnd;
  static int _fInterp;

  private:
  static CylCoordsCalculator* _CylCoords;
  static int _initialized;
};

///////////////////////////////////////////////////////////////////////////
//
// Static MEMBERS FOR CLASS CylCoordsTransSC
//
///////////////////////////////////////////////////////////////////////////

  int CylCoordsTransSC::_nZBins;
  int CylCoordsTransSC::_nRBins;
  int CylCoordsTransSC::_nThetas;
  int CylCoordsTransSC::_nModes;
  int CylCoordsTransSC::_fInterp;

  double CylCoordsTransSC::_RBnd;

  CylCoordsCalculator* CylCoordsTransSC::_CylCoords;

  int CylCoordsTransSC::_initialized = 0;

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS CylCoordsTransSC
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(CylCoordsTransSC, TransSC);

CylCoordsTransSC::CylCoordsTransSC(const String &n, const Integer &order,
                                   Integer &nZBins, Integer &nRBins,
                                   Integer &nThetas, Integer &nModes,
                                   Real &RBnd, Integer &fInterp,
                                   Real &length,
                                   const Integer &nMacrosMin) :
                                   TransSC(n, order, nZBins, nRBins,
                                           length, nMacrosMin, length)
{
  if(_initialized) return;

  _nZBins  = int(nZBins);
  _nRBins  = int(nRBins);
  _nThetas = int(nThetas);
  _nModes  = int(nModes);
  _fInterp = int(fInterp);

  _RBnd    = double(RBnd);
  double ZLength = 2. * Consts::pi;

  _CylCoords = new CylCoordsCalculator(_nZBins, _nRBins, _nThetas, _nModes,
                                       ZLength, _RBnd);

  _initialized = 1;
}

CylCoordsTransSC::~CylCoordsTransSC()
{
  delete _CylCoords;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    CylCoordsTransSC::NodeCalculator
//
// DESCRIPTION
//    Bins particles in CylCoords and calculates potential
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void CylCoordsTransSC::_nodeCalculator(MacroPart &mp)
{
  int i_flag;
  MPI_Initialized(&i_flag);

  if(i_flag)
  {
    MPI_Allreduce(&mp._nMacros, &mp._globalNMacros, 1,
                  MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }
  else
  {
    mp._globalNMacros = mp._nMacros;
  }

  if(mp._globalNMacros < _nMacrosMin) return;

  double _phiMax_tmp_local = mp._phiMax;
  double _phiMin_tmp_local = mp._phiMin;
  double _phiMax_tmp_global;
  double _phiMin_tmp_global;

  if(i_flag)
  {
    MPI_Allreduce(&_phiMax_tmp_local, &_phiMax_tmp_global, 1,
                  MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(&_phiMin_tmp_local, &_phiMin_tmp_global, 1,
                  MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  }
  else
  {
    mp._globalNMacros  = mp._nMacros;
    _phiMax_tmp_global = _phiMax_tmp_local;
    _phiMin_tmp_global = _phiMin_tmp_local;
  }

  _CylCoords->_DistributeParts(mp);
  _CylCoords->_GetPotential();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    CylCoordsTransSC::_updatePartAtNode
//
// DESCRIPTION
//    Kicks the macroparticles using the calculated potential.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void CylCoordsTransSC::_updatePartAtNode(MacroPart &mp)
{
  // Use the average line density "lambda" for a single macroparticle.
  // The CylCoords calculator will sum over all macroparticles,
  // weighted by longitudinal density.

  _lambda = Injection::nReals_Macro * _nZBins * Ring::harmonicNumber /
            Ring::lRing;

  double SCKick_Coeff = 1000000. * 4.0 * Consts::pi * _lambda * _lkick *
                        mp._syncPart._charge * mp._syncPart._charge *
                        rClassical /
                       (mp._syncPart._betaSync *
                        mp._syncPart._betaSync *
                        mp._syncPart._gammaSync *
                        mp._syncPart._gammaSync *
                        mp._syncPart._gammaSync * mp._syncPart._mass);


  if(_fInterp == 1) _CylCoords->_ApplyForce1(mp, SCKick_Coeff);
  if(_fInterp == 2) _CylCoords->_ApplyForce2(mp, SCKick_Coeff);
}



///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE TSpaceCharge
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    TSpaceCharge::ctor
//
// DESCRIPTION
//    Initializes the various TSpaceCharge related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::ctor()
{

// set some initial values

    nTransSCs = 0;

 }

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::addBFTransSCSet
//
// DESCRIPTION
//    Adds a set of brute force PIC Transverse Space Charge Nodes,
//    one for every Matrix element present.
//    Adds the set to be second order symplectic in the
//    integration. I.e., use a "central" kick over the 1/2 length
//    of the previous matrix element + 1/2 length of the present matrix
//    element.
//
// PARAMETERS
//
//    nxb       Number of X bins to use
//    nyb       Number of Y bins to use
//    eps:      The smoothing parameter to use (fraction of a bin length)
//    nMacrosMin The minimum number of macros to have before using
//                 the kick
//    gf        Grid factor = extra length of grid / max. extent of
//              particles. Same ratio used in X and Y.
//
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addBFTransSCSet(Integer &nxb, Integer &nyb, Real &eps,
                      const Integer &nMacrosMin, Real &gf)
{
    Node *n1;
    MapBase *tm, *tm2;
    Integer n;
    String wname;
    Real avLength, len;

    if( nTransMaps < 2) except(tooFewTransMaps);

    char num2[10];
    String cons("TransSC");

    // Add kick for 1st half of TM number 1:

    wname = cons+"0";
    tm = MapBase::safeCast(tMaps(0));
    avLength = tm->_length/2.;
    addBFTransSC(wname, (tm->_oindex-5), nxb, nyb, eps, avLength,
              nMacrosMin, gf);

    // Add central kicks :

    for (n=1; n<=nTransMaps-1; n++)
    {

        tm = MapBase::safeCast(tMaps(n-1));
        tm2 = MapBase::safeCast(tMaps(n));
        sprintf(num2, "(%d)", n);
        wname = cons+num2;
        avLength = ( tm->_length + tm2->_length)/2.;
        addBFTransSC(wname, (tm->_oindex+5), nxb, nyb, eps,
          avLength, nMacrosMin, gf);
     }

    // Add kick for last half of last TM:

    sprintf(num2, "(%d)", nTransMaps);
     wname = cons+num2;
    tm = MapBase::safeCast(tMaps(nTransMaps-1));
    len = tm->_length/2.;
    addBFTransSC(wname, (tm->_oindex+5), nxb, nyb, eps, len,
              nMacrosMin, gf);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::addBFTransSC
//
// DESCRIPTION
//    Adds a Brute Force Transverse Space Charge Node
//
//
// PARAMETERS
//    name:    Name for the TM
//    order:   Order index
//    nXBins   Number of X bins to use
//    nYBins   Number of Y bins to use
//    eps:     The smoothing parameter to use (fraction of a bin length)
//    length:  Longitudinal length to apply the kick (m)
//    nMacrosMin The minimum number of macros to have before using
//               the kick
//    gf       Grid factor = extra length of grid / max. extent of
//             particles. Same ration used in X and Y.
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge:: addBFTransSC(const String &n, const Integer &o,
                  Integer &nXBins, Integer  &nYBins, Real &eps, Real &length,
                       const Integer &nMacrosMin, Real &gf)


{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + 100);
  if(nTransSCs == TSpaceChargePointers.size())
      TSpaceChargePointers.resize(nTransSCs + 100);

   nNodes++;

   nodes(nNodes-1) = new BFPICTransSC(n, o, nXBins, nYBins, eps, length,
                                  nMacrosMin, gf);
   nTransSCs ++;

   TSpaceChargePointers(nTransSCs -1) = nodes(nNodes-1);

   nodesInitialized = 0;

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::addFFTTransSCSet
//
// DESCRIPTION
//    Adds a set of FFT force PIC Transverse Space Charge Nodes,
//    one for every Matrix element present.
//    Adds the set to be second order symplectic in the
//    integration. I.e., use a "central" kick over the 1/2 length
//    of the previous matrix element + 1/2 length of the present matrix
//    element.
//
// PARAMETERS
//
//    nxb       Number of X bins to use
//    nyb       Number of Y bins to use
//    eps:      The smoothing parameter to use (fraction of a bin length)
//    nMacrosMin The minimum number of macros to have before using
//                 the kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addFFTTransSCSet(Integer &nxb, Integer &nyb, Real &eps,
                      const Integer &nMacrosMin)
{
    Node *n1;
    MapBase *tm, *tm2;
    Integer n;
    String wname;
    Real avLength, len;

    if( nTransMaps < 2) except(tooFewTransMaps);

    char num2[10];
    String cons("TransSC");

    // Add kick for 1st half of TM number 1:

    wname = cons+"0";
    tm = MapBase::safeCast(tMaps(0));
    avLength = tm->_length/2.;
    addFFTTransSC(wname, (tm->_oindex-5), nxb, nyb, eps, avLength,
              nMacrosMin);

    // Add central kicks :

    for (n=1; n<=nTransMaps-1; n++)
    {

        tm = MapBase::safeCast(tMaps(n-1));
        tm2 = MapBase::safeCast(tMaps(n));
        sprintf(num2, "(%d)", n);
        wname = cons+num2;
        avLength = ( tm->_length + tm2->_length)/2.;
        addFFTTransSC(wname, (tm->_oindex+5), nxb, nyb, eps,
          avLength, nMacrosMin);
     }

    // Add kick for last half of last TM:

    sprintf(num2, "(%d)", nTransMaps);
     wname = cons+num2;
    tm = MapBase::safeCast(tMaps(nTransMaps-1));
    len = tm->_length/2.;
    addFFTTransSC(wname, (tm->_oindex+5), nxb, nyb, eps, len,
              nMacrosMin);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::addFFTTransSC
//
// DESCRIPTION
//    Adds an FFT Transverse Space Charge Node
//
//
// PARAMETERS
//    name:    Name for the kick
//    order:   Order index
//    nXBins   Number of X bins to use
//    nYBins   Number of Y bins to use
//    eps:     The smoothing parameter to use (fraction of a bin length)
//    length:  Longitudinal length to apply the kick (m)
//    nMacrosMin The minimum number of macros to have before using
//               the kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge:: addFFTTransSC(const String &n, const Integer &o,
                   Integer &nXBins, Integer  &nYBins, Real &eps, Real &length,
                       const Integer &nMacrosMin)


{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + 100);
  if(nTransSCs == TSpaceChargePointers.size())
      TSpaceChargePointers.resize(nTransSCs + 100);

   nNodes++;

   nodes(nNodes-1) = new FFTTransSC(n, o, nXBins, nYBins, eps, length,
                                  nMacrosMin);
   nTransSCs ++;

   TSpaceChargePointers(nTransSCs -1) = nodes(nNodes-1);

   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::addPWSTransSC
//
// DESCRIPTION
//    Adds a Pair-wise sum Transverse Space Charge Node
//
//
// PARAMETERS
//    name:    Name for the TM
//    order:   Order index
//    eps:     The smoothing parameter to use (mm)
//    length:  Longitudinal length to apply the kick (m)
//    nMacrosMin The minimum number of macros to have before using
//               the kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addPWSTransSC(const String &n, const Integer &o,
                       Real &eps, Real &length, const Integer &nMacrosMin)

{
   Integer nx,ny;
   nx = ny = 1;

  if (nNodes == nodes.size())
     nodes.resize(nNodes + 100);
  if(nTransSCs == TSpaceChargePointers.size())
      TSpaceChargePointers.resize(nTransSCs + 100);

   nNodes++;

   nodes(nNodes-1) = new PWSTransSC(n, o, nx,ny, eps, length, nMacrosMin);
   nTransSCs ++;

   TSpaceChargePointers(nTransSCs -1) = nodes(nNodes-1);

   nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::addPWSTransSCSet
//
// DESCRIPTION
//    Adds a set of pair-wise sum Transverse Space Charge Nodes,
//    one for every Matrix element present.
//    Adds the set to be second order symplectic in the
//    integration. I.e., use a "central" kick over the 1/2 length
//    of the previous matrix element + 1/2 length of the present matrix
//    element.
//
// PARAMETERS
//
//    eps:      The smoothing parameter to use (mm)
//    nMacrosMin The minimum number of macros to have before using
//                 the kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addPWSTransSCSet(Real &eps, const Integer &nMacrosMin)
{
    Node *n1;
    MapBase *tm, *tm2;
    Integer n;
    String wname;
    Real avLength, len;

    if( nTransMaps < 2) except(tooFewTransMaps);

    char num2[10];
    String cons("TransSC");

    // Add kick for 1st half of TM number 1:

    wname = cons+"0";
    tm = MapBase::safeCast(tMaps(0));
    avLength = tm->_length/2.;
    addPWSTransSC(wname, (tm->_oindex-5), eps, avLength, nMacrosMin);

    // Add central kicks :

    for (n=1; n<=nTransMaps-1; n++)
    {

        tm = MapBase::safeCast(tMaps(n-1));
        tm2 = MapBase::safeCast(tMaps(n));
        sprintf(num2, "(%d)", n);
        wname = cons+num2;
        avLength = ( tm->_length + tm2->_length)/2.;
        addPWSTransSC(wname, (tm->_oindex+5), eps, avLength, nMacrosMin);
     }

    // Add kick for last half of last TM:

    sprintf(num2, "(%d)", nTransMaps);
     wname = cons+num2;
     len = tm->_length/2.;

    tm = MapBase::safeCast(tMaps(nTransMaps-1));
    addPWSTransSC(wname, (tm->_oindex+5), eps, len, nMacrosMin);

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::addPotentialTransSCSet
//
// DESCRIPTION
//    Adds a set of FFT force PIC Transverse Space Charge Nodes,
//    one for every Matrix element present.
//    Beam Pipe is included with conducting wall boundary condition.
//
// PARAMETERS
//
//    nxb:       Number of X bins to use
//    nyb:       Number of Y bins to use
//    eps:       The smoothing parameter to use (fraction of a bin length)
//    BPShape:   String for boundary shape (Circle, Ellipse, Rectangle)
//    BP1:       Parameter #1, meaning depends on BPShape
//    BP2:       Parameter #2, meaning depends on BPShape
//    BP3:       Parameter #3, meaning depends on BPShape
//    BP4:       Parameter #4, meaning depends on BPShape
//    BPPoints:  Number of boundary points for conducting wall
//    BPModes:   Number of modes for homogeneous solution
//    Gridfact:   Expansion factor for grid extent
//    nMacrosMin: The minimum number of macros to have before using
//               the kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addPotentialTransSCSet(Integer &nxb, Integer &nyb,
                                       Real &eps,
                                       const String &BPShape, Real &BP1,
                                       Real &BP2, Real &BP3, Real &BP4,
                                       Integer &BPPoints, Integer &BPModes,
                                       Real &Gridfact,
                                       const Integer &nMacrosMin)
{
    Node *n1;
    MapBase *tm, *tm2;
    Integer n;
    String wname;
    Real avLength, len;

    if( nTransMaps < 2) except(tooFewTransMaps);

    char num2[10];
    String cons("TransSC");

    // Add kick for 1st half of TM number 1:

    wname = cons+"0";
    tm = MapBase::safeCast(tMaps(0));
    avLength = tm->_length/2.;
    addPotentialTransSC(wname, (tm->_oindex-5), nxb, nyb,
                     eps, avLength,
                     BPShape, BP1, BP2, BP3, BP4,
                     BPPoints, BPModes, Gridfact,
		     nMacrosMin);

    // Add central kicks :

    for (n=1; n<=nTransMaps-1; n++)
    {
        tm = MapBase::safeCast(tMaps(n-1));
        tm2 = MapBase::safeCast(tMaps(n));
        sprintf(num2, "(%d)", n);
        wname = cons+num2;
        avLength = ( tm->_length + tm2->_length)/2.;
        addPotentialTransSC(wname, (tm->_oindex+5), nxb, nyb,
                         eps, avLength,
                         BPShape, BP1, BP2, BP3, BP4,
                         BPPoints, BPModes, Gridfact,
                         nMacrosMin);
     }

    // Add kick for last half of last TM:

    sprintf(num2, "(%d)", nTransMaps);
    wname = cons+num2;
    tm = MapBase::safeCast(tMaps(nTransMaps-1));
    avLength = tm->_length/2.;
    addPotentialTransSC(wname, (tm->_oindex+5), nxb, nyb,
                     eps, avLength,
                     BPShape, BP1, BP2, BP3, BP4,
                     BPPoints, BPModes, Gridfact,
                     nMacrosMin);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::addPotentialTransSC
//
// DESCRIPTION
//    Adds an FFT Transverse Space Charge Node with beam pipe
//    and conducting wall boundary condition.
//
//
// PARAMETERS
//    name:       Name for the kick
//    order:      Order index
//    nXBins      Number of X bins to use
//    nYBins      Number of Y bins to use
//    eps:        The smoothing parameter to use (fraction of a bin length)
//    length:     Longitudinal length to apply the kick (m)
//    BPShape:    String for boundary shape (Circle, Ellipse, Rectangle)
//    BP1:        Parameter #1, meaning depends on BPShape
//    BP2:        Parameter #2, meaning depends on BPShape
//    BP3:        Parameter #3, meaning depends on BPShape
//    BP4:        Parameter #4, meaning depends on BPShape
//    BPPoints:   Number of boundary points for conducting wall
//    BPModes:    Number of modes for homogeneous solution
//    Gridfact:    Expansion factor for grid extent
//    nMacrosMin: The minimum number of macros to have before using
//                the kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addPotentialTransSC(const String &n, const Integer &o,
                                    Integer &nXBins, Integer  &nYBins,
                                    Real &eps, Real &length,
                                    const String &BPShape, Real &BP1,
                                    Real &BP2, Real &BP3, Real &BP4,
		                    Integer &BPPoints, Integer &BPModes,
                                    Real &Gridfact,
                                    const Integer &nMacrosMin)
{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + 100);
  if(nTransSCs == TSpaceChargePointers.size())
      TSpaceChargePointers.resize(nTransSCs + 100);

   nNodes++;

   nodes(nNodes-1) = new PotentialTransSC(n, o, nXBins, nYBins,
                                       eps, length,
                                       BPShape, BP1, BP2, BP3, BP4,
                                       BPPoints, BPModes, Gridfact,
                                       nMacrosMin);



   nTransSCs ++;

   TSpaceChargePointers(nTransSCs -1) = nodes(nNodes-1);

   nodesInitialized = 0;

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TSpaceCharge::addFMMTransSCSet
//
// DESCRIPTION
//   Adds a set of FMM Transverse Space Charge Nodes,
//   one for every lattice element present.
//   Beam Pipe is included with conducting wall boundary condition.
//
// PARAMETERS
//
//   nTerms:    Number of FMM terms in multipole expansion
//   nLevels:   Number of FMM levels
//   nZBins:    Number of longitudinal bins
//   eps:       The smoothing parameter (fraction of a bin length)
//   BPShape:   String for boundary shape (Circle, Ellipse, Rectangle)
//   BP1:       Parameter #1, meaning depends on BPShape
//   BP2:       Parameter #2, meaning depends on BPShape
//   BP3:       Parameter #3, meaning depends on BPShape
//   BP4:       Parameter #4, meaning depends on BPShape
//   BPPoints:  Number of boundary points for conducting wall
//   BPModes:   Number of modes for homogeneous solution
//   BPResid:   Residual for boundary potential convergence
//   Gridfact:  Grid Sizing Coefficient
//   nMacrosMin: The minimum number of macros to induce a kick
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addFMMTransSCSet(Integer &nTerms, Integer &nLevels,
                                    Integer &nZBins, Real &eps,
                                    const String &BPShape,
                                    Real &BP1, Real &BP2,
                                    Real &BP3, Real &BP4,
                                    Integer &BPPoints, Integer &BPModes,
                                    Real &BPResid, Real &Gridfact,
                                    const Integer &nMacrosMin)
{
  MapBase *tmi, *tmf;
  Integer n;
  String name;
  Real kickLength;

  if(nTransMaps < 2) except(tooFewTransMaps);

  char num[10];
  String cons("TransSC");

  // Add kick for 1st half of TM number 1:

  name = cons + "0";
  tmf = MapBase::safeCast(tMaps(0));
  kickLength = tmf->_length / 2.;
  addFMMTransSC(name, (tmf->_oindex - 5), nTerms,
                nLevels, nZBins, eps, kickLength,
                BPShape, BP1, BP2, BP3, BP4,
                BPPoints, BPModes, BPResid, Gridfact, nMacrosMin);

  // Add central kicks :

  for (n = 1; n <= nTransMaps - 1; n++)
  {
    tmi = MapBase::safeCast(tMaps(n - 1));
    tmf = MapBase::safeCast(tMaps(n));
    sprintf(num, "(%d)", n);
    name = cons + num;
    kickLength = (tmi->_length + tmf->_length) / 2.;
    addFMMTransSC(name, (tmi->_oindex + 5), nTerms,
                  nLevels, nZBins, eps, kickLength,
                  BPShape, BP1, BP2, BP3, BP4,
                  BPPoints, BPModes, BPResid, Gridfact, nMacrosMin);
  }

  // Add kick for last half of last TM:

  sprintf(num, "(%d)", nTransMaps);
  name = cons + num;
  tmi = MapBase::safeCast(tMaps(nTransMaps - 1));
  kickLength = tmi->_length / 2.;
  addFMMTransSC(name, (tmi->_oindex + 5), nTerms,
                nLevels, nZBins, eps, kickLength,
                BPShape, BP1, BP2, BP3, BP4,
                BPPoints, BPModes, BPResid, Gridfact, nMacrosMin);
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TSpaceCharge::addFMMTransSC
//
// DESCRIPTION
//   Adds an FMM Transverse Space Charge Node with beam pipe
//   and conducting wall boundary condition.
//
// PARAMETERS
//   name:       Name for the kick
//   order:      Order index
//   nTerms:     Number of FMM terms in multipole expansion
//   nLevels:    Number of FMM levels
//   nZBins:     Number of longitudinal bins
//   eps:        The smoothing parameter (fraction of a bin length)
//   length:     Longitudinal length for kick (m)
//   BPShape:    String for boundary shape (Circle, Ellipse, Rectangle)
//   BP1:        Parameter #1, meaning depends on BPShape
//   BP2:        Parameter #2, meaning depends on BPShape
//   BP3:        Parameter #3, meaning depends on BPShape
//   BP4:        Parameter #4, meaning depends on BPShape
//   BPPoints:   Number of boundary points for conducting wall
//   BPModes:    Number of modes for homogeneous solution
//   BPResid:    Residual for boundary potential convergence
//   Gridfact:   Grid Sizing Coefficient
//   nMacrosMin: The minimum number of macros to have before using
//               the kick
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addFMMTransSC(const String &name, const Integer &order,
                    Integer &nTerms, Integer &nLevels, Integer &nZBins,
                    Real &eps, Real &length,
                    const String &BPShape,
                    Real &BP1, Real &BP2, Real &BP3, Real &BP4,
                    Integer &BPPoints, Integer &BPModes,
                    Real &BPResid, Real &Gridfact,
                    const Integer &nMacrosMin)
{
  if (nNodes == nodes.size())
    nodes.resize(nNodes + 100);
  if(nTransSCs == TSpaceChargePointers.size())
    TSpaceChargePointers.resize(nTransSCs + 100);

  nNodes++;

  Integer nXYBins = 1;

  for(Integer i = 0; i < nLevels; i++)
  {
    nXYBins *= 2;
  }

  nodes(nNodes - 1) = new FMMTransSC(name, order,
                                     nTerms, nLevels,
                                     nXYBins, nZBins,
                                     eps, length,
                                     BPShape, BP1, BP2, BP3, BP4,
                                     BPPoints, BPModes,
                                     BPResid, Gridfact, nMacrosMin);

  nTransSCs ++;

  TSpaceChargePointers(nTransSCs - 1) = nodes(nNodes - 1);

  nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TSpaceCharge::addElliptTransSCSet
//
// DESCRIPTION
//   Adds a set of Ellipt Transverse Space Charge Nodes,
//   one for every lattice element present.
//   Beam Pipe is included with conducting wall boundary condition.
//
// PARAMETERS
//
//   nZBins:    Number of longitudinal bins
//   BPShape:   String for boundary shape (Circle, Ellipse, Rectangle)
//   BP1:       Parameter #1, meaning depends on BPShape
//   BP2:       Parameter #2, meaning depends on BPShape
//   BP3:       Parameter #3, meaning depends on BPShape
//   BP4:       Parameter #4, meaning depends on BPShape
//   BPPoints:  Number of boundary points for conducting wall
//   BPModes:   Number of modes for homogeneous solution
//   BPResid:   Residual for boundary potential convergence
//   nMacrosMin: The minimum number of macros to induce a kick
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addElliptTransSCSet(Integer &nZBins,
                                       const String &BPShape,
                                       Real &BP1, Real &BP2,
                                       Real &BP3, Real &BP4,
                                       Integer &BPPoints, Integer &BPModes,
                                       Real &BPResid,
                                       const Integer &nMacrosMin)
{
  MapBase *tmi, *tmf;
  Integer n;
  String name;
  Real kickLength;

  if(nTransMaps < 2) except(tooFewTransMaps);

  char num[10];
  String cons("TransSC");

  // Add kick for 1st half of TM number 1:

  name = cons + "0";
  tmf = MapBase::safeCast(tMaps(0));
  kickLength = tmf->_length / 2.;
  addElliptTransSC(name, (tmf->_oindex - 5), nZBins, kickLength,
                   BPShape, BP1, BP2, BP3, BP4,
                   BPPoints, BPModes, BPResid, nMacrosMin);

  // Add central kicks :

  for (n = 1; n <= nTransMaps - 1; n++)
  {
    tmi = MapBase::safeCast(tMaps(n - 1));
    tmf = MapBase::safeCast(tMaps(n));
    sprintf(num, "(%d)", n);
    name = cons + num;
    kickLength = (tmi->_length + tmf->_length) / 2.;
    addElliptTransSC(name, (tmi->_oindex + 5), nZBins, kickLength,
                     BPShape, BP1, BP2, BP3, BP4,
                     BPPoints, BPModes, BPResid, nMacrosMin);
  }

  // Add kick for last half of last TM:

  sprintf(num, "(%d)", nTransMaps);
  name = cons + num;
  tmi = MapBase::safeCast(tMaps(nTransMaps - 1));
  kickLength = tmi->_length / 2.;
  addElliptTransSC(name, (tmi->_oindex + 5), nZBins, kickLength,
                BPShape, BP1, BP2, BP3, BP4,
                BPPoints, BPModes, BPResid, nMacrosMin);
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TSpaceCharge::addElliptTransSC
//
// DESCRIPTION
//   Adds an Ellipt Transverse Space Charge Node with beam pipe
//   and conducting wall boundary condition.
//
// PARAMETERS
//   name:       Name for the kick
//   order:      Order index
//   nZBins:     Number of longitudinal bins
//   length:     Longitudinal length for kick (m)
//   BPShape:    String for boundary shape (Circle, Ellipse, Rectangle)
//   BP1:        Parameter #1, meaning depends on BPShape
//   BP2:        Parameter #2, meaning depends on BPShape
//   BP3:        Parameter #3, meaning depends on BPShape
//   BP4:        Parameter #4, meaning depends on BPShape
//   BPPoints:   Number of boundary points for conducting wall
//   BPModes:    Number of modes for homogeneous solution
//   BPResid:    Residual for boundary potential convergence
//   nMacrosMin: The minimum number of macros to have before using
//               the kick
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addElliptTransSC(const String &name, const Integer &order,
                                    Integer &nZBins, Real &length,
                                    const String &BPShape,
                                    Real &BP1, Real &BP2,
                                    Real &BP3, Real &BP4,
                                    Integer &BPPoints, Integer &BPModes,
                                    Real &BPResid,
                                    const Integer &nMacrosMin)
{
  if (nNodes == nodes.size())
    nodes.resize(nNodes + 100);
  if(nTransSCs == TSpaceChargePointers.size())
    TSpaceChargePointers.resize(nTransSCs + 100);

  nNodes++;

  nodes(nNodes - 1) = new ElliptTransSC(name, order,
                                        nZBins, length,
                                        BPShape, BP1, BP2, BP3, BP4,
                                        BPPoints, BPModes,
                                        BPResid, nMacrosMin);

  nTransSCs ++;

  TSpaceChargePointers(nTransSCs - 1) = nodes(nNodes - 1);

  nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TSpaceCharge::addCylCoordsTransSCSet
//
// DESCRIPTION
//   Adds a set of Cylindrical Coordinate Space Charge Nodes,
//   one for every lattice element present.
//   Circular beam Pipe is included
//   with conducting wall boundary condition.
//
// PARAMETERS
//
//   nZBins:    Number of longitudinal bins
//   nRBins:    Number of radial bins
//   nThetas:   Number of angle bins
//   nModes:    Number of angle modes
//   RBnd:      Radial boundary, conducting wall
//   fInterp:   1 - exact theta, 2 - interpolate in theta
//   nMacrosMin: The minimum number of macros to induce a kick
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addCylCoordsTransSCSet(Integer &nZBins,
                                          Integer &nRBins,
                                          Integer &nThetas,
                                          Integer &nModes,
                                          Real &RBnd,
                                          Integer &fInterp,
                                          const Integer &nMacrosMin)
{
  MapBase *tmi, *tmf;
  Integer n;
  String name;
  Real kickLength;

  if(nTransMaps < 2) except(tooFewTransMaps);

  char num[10];
  String cons("TransSC");

  // Add kick for 1st half of TM number 1:

  name = cons + "0";
  tmf = MapBase::safeCast(tMaps(0));
  kickLength = tmf->_length / 2.;
  addCylCoordsTransSC(name, (tmf->_oindex - 5),
                      nZBins, nRBins,
                      nThetas, nModes,
                      RBnd, fInterp,
                      kickLength, nMacrosMin);

  // Add central kicks :

  for (n = 1; n <= nTransMaps - 1; n++)
  {
    tmi = MapBase::safeCast(tMaps(n - 1));
    tmf = MapBase::safeCast(tMaps(n));
    sprintf(num, "(%d)", n);
    name = cons + num;
    kickLength = (tmi->_length + tmf->_length) / 2.;
    addCylCoordsTransSC(name, (tmi->_oindex + 5),
                        nZBins, nRBins,
                        nThetas, nModes,
                        RBnd, fInterp,
                        kickLength, nMacrosMin);
  }

  // Add kick for last half of last TM:

  sprintf(num, "(%d)", nTransMaps);
  name = cons + num;
  tmi = MapBase::safeCast(tMaps(nTransMaps - 1));
  kickLength = tmi->_length / 2.;
  addCylCoordsTransSC(name, (tmi->_oindex + 5),
                      nZBins, nRBins,
                      nThetas, nModes,
                      RBnd, fInterp,
                      kickLength, nMacrosMin);
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TSpaceCharge::addCylCoordsTransSC
//
// DESCRIPTION
//   Adds a Cylindrical Coordinate Space Charge Node with
//   circular beam pipe and conducting wall boundary condition.
//
// PARAMETERS
//   name:       Name for the kick
//   order:      Order index
//   nZBins:     Number of longitudinal bins
//   nRBins:     Number of radial bins
//   nThetas:    Number of angle bins
//   nModes:     Number of angle modes
//   RBnd:       Radial boundary, conducting wall
//   fInterp:    1 - exact theta, 2 - interpolate in theta
//   length:     Longitudinal length for kick (m)
//   nMacrosMin: The minimum number of macros to have before using
//               the kick
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::addCylCoordsTransSC(const String &name,
                                       const Integer &order,
                                       Integer &nZBins,
                                       Integer &nRBins,
                                       Integer &nThetas,
                                       Integer &nModes,
                                       Real &RBnd,
                                       Integer &fInterp,
                                       Real &length,
                                       const Integer &nMacrosMin)
{
  if(nNodes == nodes.size())
    nodes.resize(nNodes + 100);
  if(nTransSCs == TSpaceChargePointers.size())
    TSpaceChargePointers.resize(nTransSCs + 100);

  nNodes++;

  nodes(nNodes - 1) = new CylCoordsTransSC(name, order,
                                           nZBins, nRBins,
                                           nThetas, nModes,
                                           RBnd, fInterp,
                                           length, nMacrosMin
);

  nTransSCs ++;

  TSpaceChargePointers(nTransSCs - 1) = nodes(nNodes - 1);

  nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::showTransSC
//
// DESCRIPTION
//    Show a Transverse space charge info:
//
//
// PARAMETERS
//
//  n = the transverse space charge number.
//  os = the stream to dump the info to.
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::showTransSC( const Integer &n, Ostream &os)

{
    Integer i,j;

    if (n > nTransSCs) except (badTransSC);

    TransSC *tsc = TransSC::safeCast(TSpaceChargePointers(n-1));

    os << "\n\n";
    os << "Perveance = " << tsc->_perveance << "\n"
       << "  lambda = " << tsc->_lambda  << "\n"
       << "eps       = " << tsc->_eps << "\n"
       << "kick length (m) " << tsc->_lkick << "\n\n";


    os << "Force_x = \n\n";

    for (j=1; j<=tsc->_nYBins; j++)
      {
       for (i=1; i<= tsc->_nXBins; i++)
//       os  << setw(7) << tsc->_fscx(i,j) << "\t";
       os  << setw(7) << TransSC::_fscx(i,j) << "\t";
       os << "\n\n";
      }

    os << "Force_y = \n\n";

    for (j=1; j<=tsc->_nYBins; j++)
      {
       for (i=1; i<= tsc->_nYBins; i++)
//       os << setw(7) << tsc->_fscy(i,j) << "\t";
       os << setw(7) << TransSC::_fscy(i,j) << "\t";
       os << "\n\n";
      }


    os << "_xGrid = \n\n";
    os << tsc->_xGrid;
    os << "\n\n";
    os << "_yGrid = \n\n";
    os << tsc->_yGrid;
    os << "\n\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::showPotSC
//
// DESCRIPTION
//    Show a Transverse space charge info, including potential:
//
//
// PARAMETERS
//
//  n = the transverse space charge number.
//  os = the stream to dump the info to.
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::showPotSC( const Integer &n, Ostream &os)

{
    Integer i,j;

    if (n > nTransSCs) except (badTransSC);

    TransSC *tsc = TransSC::safeCast(TSpaceChargePointers(n-1));

    os << "\n\n";
    os << "Perveance       = " << tsc->_perveance << "\n"
       << "lambda          = " << tsc->_lambda  << "\n"
       << "eps             = " << tsc->_eps << "\n"
       << "kick length (m) = " << tsc->_lkick << "\n"
       << "LSQ Residual    = " << tsc->_resid << "\n\n";

    os << "x, y, Potential = \n\n";

    for (j=1; j<=tsc->_nYBins; j++)
      {
       for (i=1; i<= tsc->_nXBins; i++)
	 os <<  TransSC::_xGrid(i) << "   "
            <<  TransSC::_yGrid(j) << "   "
            << TransSC::_phisc(i,j) << "\n";
      }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::dumpFSC
//
// DESCRIPTION
//    Dump the transverse space charge x and y force fields
//
//
// PARAMETERS
//
// n     The number (order) of the Transverse space charge
// fx    Reference to the real matrix to dump horizontal force to.
// fy    Reference to the real matrix to dump the vertical force to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::dumpFSC( const Integer &n, Matrix(Real) &mx,
                             Matrix(Real) &my)

{
    Integer i,j;

    if (n > nTransSCs) except (badTransSC);

    TransSC *tsc = TransSC::safeCast(TSpaceChargePointers(n-1));

    mx.resize(tsc->_nXBins, tsc->_nYBins);
    my.resize(tsc->_nXBins, tsc->_nYBins);

    mx = tsc->_fscx;
    my = tsc->_fscy;

}


Void TSpaceCharge::dumpXYGrid( const Integer &n, Vector(Real) &xs,
                               Vector(Real) &ys)
{
    Integer i,j;

    if (n > nTransSCs) except (badTransSC);

    TransSC *tsc = TransSC::safeCast(TSpaceChargePointers(n-1));

    xs.resize(tsc->_nXBins);
    ys.resize(tsc->_nYBins);

    xs = tsc->_xGrid;
    ys = tsc->_yGrid;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TSpaceCharge::dumpPotSC
//
// DESCRIPTION
//    Dump the transverse space charge potential
//
//
// PARAMETERS
//
// n     The number (order) of the Transverse space charge
// m     Reference to the real matrix to dump potential.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TSpaceCharge::dumpPotSC( const Integer &n, Matrix(Real) &m)
{
    Integer i,j;

    if (n > nTransSCs) except (badTransSC);

    TransSC *tsc = TransSC::safeCast(TSpaceChargePointers(n-1));

    m.resize(tsc->_nXBins, tsc->_nYBins);

    m = tsc->_phisc;
}

