/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   SpCh3D_MPI.h
//
// AUTHORS
//   Slava Danilov, Jeff Holmes, John Galambos
//   ORNL, vux@ornl.gov
//
// CREATED
//   7/13/2001
//
// DESCRIPTION
//   Header file for the 3D space charge class (MPI parallel implementation). 
//   Separated from SpaceCharge3D.cc to use elsewhere.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(__SpCh3D_MPI__)
#define __SpCh3D_MPI__

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Node.h"

#include "mpi.h"

#include "Boundary.h"
#include "FFT3DSpaceCharge_MPI.h"


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME : SpCh3D_MPI
//   
// INHERITANCE RELATIONSHIPS
//    SpCh3D_MPI -> Node* -> Object
//
// DESCRIPTION
//   This is a wrapper class for the ORBIT_FFT3DSpaceCharge class to provide
//   this class with methods _updatePartAtNode and _nodeCalculator, and
//   data about the length of the element  _lkick.
//
// PIC stuff:
//   _nXBins      Reference to the number of horizontal bins.
//   _nYBins      Reference to the number of vertical bins.
//   _nZBins      Reference to the number of longitudinal bins.
//   _boundary    Reference to the ORBIT_Boundary class instance
//   _lkick       Longitudinal length over which kick is applied (m)
//
/////////////////////////////////////////////////////////////////////////////

class SpCh3D_MPI : public Node {
  Declare_Standard_Members(SpCh3D_MPI, Node);

  public:

  SpCh3D_MPI(const String &n, const Integer &order, 
             Integer &nXBins, Integer &nYBins, Integer &nZBins, Real &length,
             Integer &nMacrosMin) : Node(n, 0., order), _lkick(length)
  {
    _FFT3DSpaceCharge_MPI = ORBIT_FFT3DSpaceCharge::getORBIT_FFT3DSpaceCharge
                           (nXBins,nYBins,nZBins,nMacrosMin);
  }

  ~SpCh3D_MPI()
  {    
    delete _boundary;
    delete _FFT3DSpaceCharge_MPI;
  };

    Void nameOut(String &wname) { wname = _name; };
    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
    Void _setBoundary(ORBIT_Boundary* boundary){ _boundary = boundary; };

    protected:

    ORBIT_Boundary* _boundary;
    ORBIT_FFT3DSpaceCharge* _FFT3DSpaceCharge_MPI;

    Real _lkick;
     
};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif   // __SpCh3D_MPI__


