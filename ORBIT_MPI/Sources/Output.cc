/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Output.cc
//
// AUTHOR
//    John GalambosORNL, jdg@ornl.gov
//
// CREATED
//    10/2/97
//
//  DESCRIPTION
//    Source file for the Output module. This module organizes the Output nodes.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Output.h"
#include "MacroPart.h"
#include "Node.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "StrClass.h"
#include "StreamType.h"
#include "Foil.h"
#include <cmath>
#if defined(_WIN32)
  #include <strstrea.h>
#else
  #include <sstream>
#endif

#include <iomanip>

using namespace std;

//MPI header file for parallel stuff ==MPI==
#include "mpi.h"


///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

// Lists of macro Particles, etc.

extern Array(ObjectPtr) nodes, ONodes, tMaps;

extern Array(ObjectPtr) mParts, syncP, foilPointers, thinLensPointers;;


// Header strings

static const char *nodeHeader =
"Node #  Name                    Index   Length        Position      Type\n";
static const char *nodeLine =
"------  ----------------------  ------  ------------  ------------  -----------\n";

static const char *turnHeader =
" Turn  Time  n-Macros  n-Reals hits/p e-X-RMS  e-Y-RMS  eps-X   eps-Y  BF\n";

static const char *turnLine =
"----  ------ -------  -------  -----  --------  -------- -------- ------- --------\n";


///////////////////////////////////////////////////////////////////////////
//
// UTILITY  ROUTINES
//
///////////////////////////////////////////////////////////////////////////


const Integer lineLength = 78;
const String cr("\n");

static Void title(Ostream &os, const String &t)
{
  Integer ind = (lineLength - t.length()) / 2;
  String ut(t);

  os << "\n\n" << setw(ind) << " " << setw(0) << ut.upper() << cr;
}

static Void subtitle(Ostream &os, const String &st)
{
  Integer ind = (lineLength - st.length()) / 2;

  os << cr << setw(ind) << " " << setw(0) << st << cr;
}

Void section(Ostream &os, const String &s)
{
  String us(s);

  os << cr << us.upper() << "\n";
}

Void subsection(Ostream &os, const String &ss)
{
  os << cr << ss << ":\n";
}

static Void subsubsection(Ostream &os, const String &sss)
{
  os << sss << ":\n";
}

static Void line(Ostream &os, Integer i)
{
  os << "  " 
    << "--------------------------------------------------------";
  if (i > 1)
    os << "----------------";

  os << cr;
}

Void line(Ostream &os, const String &l, Real v)
{
  long dummy;

  os << "  ";
  dummy = os.setf(ios::left, ios::adjustfield);
  os << setw(40) << l;
  dummy = os.setf(ios::right, ios::adjustfield);
  os << setw(16) << v << cr;
}

static Void line(Ostream &os, const String &l, Real v1, Real v2)
{
  long dummy;

  os << "  ";
  dummy = os.setf(ios::left, ios::adjustfield);
  os << setw(40) << l;
  dummy = os.setf(ios::right, ios::adjustfield);
  os << setw(16) << v1 << setw(16) << v2 << cr;
}


static Void line(Ostream &os, const String &l, const String &v)
{
  long dummy;

  os << "  ";
  dummy = os.setf(ios::left, ios::adjustfield);
  os << setw(40) << l;
  dummy = os.setf(ios::right, ios::adjustfield);
  os << setw(16) << v << cr;
}

static Void line(Ostream &os, const String &l, const String &v1,
  const String &v2)
{
  long dummy;

  os << "  ";
  dummy = os.setf(ios::left, ios::adjustfield);
  os << setw(40) << l;
  dummy = os.setf(ios::right, ios::adjustfield);
  os << setw(16) << v1 << setw(16) << v2 << cr;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Output::ctor
//
// DESCRIPTION
//    Initializes the various constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Output::ctor()
{

  // set some initial values

  // parallel stuff  ==MPI==  ==start===
    iMPIini=0;
    MPI_Initialized(&iMPIini);
    nMPIsize = 1;
    nMPIrank = 0;
    if(iMPIini > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank);
    }
  // parallel stuff  ==MPI==  ==stop===
    
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showNodes(Ostream &os)
//
// DESCRIPTION
//    Shows basic Node information.
//
// PARAMETERS
//    sout - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Output::showNodes(Ostream &sout)
{
    if(!nodesInitialized) initNodes();
    if(nMPIrank != 0) return;

    Node *n1;
    MapBase *tm;
    Integer n;
    String wname;

    sout << "\n Nodes:\n";
    sout << nodeHeader;
    sout << nodeLine;


  sout.setf(ios::left);

  for (n=1; n<=nNodes; n++)
  {
    n1 = Node::safeCast(ONodes(n-1));
    n1->nameOut(wname);
    if(n1 -> isKindOf(MapBase::desc()))
    {
      tm = MapBase::safeCast(ONodes(n-1));
      if(wname.length() > 22)
        wname = wname.before(21) + "*";

      sout.setf(ios::left, ios::adjustfield);

      sout << setw(6) << n << "  " << setw(22)
           << wname << "  " << setw(0);
      sout << setw(6) << n1->_oindex << "  " << setw(0);
      sout << setw(12) << n1->_length << "  " << setw(0);
      sout << setw(12) << n1->_position << "  " << setw(0);
      sout << setw(11) << tm->_et << setw(0);
      sout << "\n";
    }
    else
    {
      sout << setw(6) << n << "  " << setw(22)
           << wname << "  " << setw(0);
      sout << setw(6) << n1->_oindex << "  " << setw(0);
      sout << "\n";
    }
  }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showTransMaps(Ostream &os)
//
// DESCRIPTION
//    Shows basic Transfer Matrix information.
//
// PARAMETERS
//    sout - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Output::showTransMaps(Ostream &sout)
{

    if (!nodesInitialized) initRing();
    if(nMPIrank != 0 ) return;
    
    Node *n1;
    MapBase *tm;
    Integer n;
    String wname;

    sout << "\n Transfer Matrices:\n";
    
    sout << " #        Name          beta-X      beta-Y     alpha-X     alpha-Y    Type\n";
    sout << "--- ---------------- ----------- ----------- ----------- ---------- --------\n";


      for (n=1; n<=nNodes; n++)
    {
        n1 = Node::safeCast(ONodes(n-1));
        if(n1->isKindOf(MapBase::desc()) )
        {
        tm = MapBase::safeCast(ONodes(n-1));        
            
        tm->nameOut(wname);
        if (wname.length() > 16)
            wname = wname.before(15) + "*";
        sout.setf(ios::left, ios::adjustfield);

        sout << setw(4) << n << setw(17) << wname << setw(0);
        sout << " " << setw(11) << tm->_betaX << setw(0);
        sout << " " << setw(11) << tm->_betaY << setw(0);
        sout << " " << setw(11) << tm->_alphaX << setw(0);
        sout << " " << setw(11) << tm->_alphaY << setw(0);
        sout << " " << setw(8) << tm->_et << setw(0);
        sout << "\n";
        }
    }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showRing(Ostream &os)
//
// DESCRIPTION
//    Shows the Ring Node info
//
// PARAMETERS
//    os - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Output::showRing(Ostream &sout)
{
    if(nMPIrank != 0 ) return;

        // First display global info:
    section(sout, "Ring Output");
    subsection(sout, "Global Information");

    line(sout, "Ring length [m]", lRing);
    line(sout, "Transition gamma", gammaTrans);
    line(sout, "Compaction Factor", compactionFact);
    sout << "\n";
    subsection(sout, "Starting Information");
    line(sout, "   ",  "X Value", "Y Value");
    line(sout, "   ",  "-------", "-------");
    line(sout, "chromaticity", chromaticityX, chromaticityY);
    line(sout, "tune", nuX, nuY);
    line(sout, "eta [m]", etaX, etaY);
    line(sout, "eta-prime", etaPX, etaPY);
    line(sout, "average beta [m]", betaXAvg, betaYAvg);
    line(sout, "Starting beta [m]", betaX, betaY);
    line(sout, "Starting alpha", alphaX, alphaY);

    subsection(sout, "Node Information");
    showNodes(sout);

    subsection(sout, "Thin Lenses");
    ThinLens::showThinLens(sout);
    
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showInject(Ostream &sout)
//
// DESCRIPTION
//    Shows the particle injection parameters.
//    Right now it assumes "Joho" types are used fort the transverse directions.
//
// PARAMETERS
//    sout - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Output::showInject(Ostream &sout)
{
    if(nMPIrank != 0 ) return;

    Real epsRMS;
    SyncPart *sp = 
    SyncPart::safeCast(syncP((Particles::syncPart & Particles::All_Mask) - 1));

    section(sout, "Injector Output");

    subsection(sout, "Synchronous particle:");
    line(sout, "Kinetic energy (GeV)", sp->_eKinetic);
    line(sout, "Mass (amu)", sp->_mass);
    line(sout, "Charge", sp->_charge);
    line(sout, "Beta", sp->_betaSync);
    line(sout, "Gamma", sp->_gammaSync);


    subsection(sout, "Horizontal Distribution");
    if (nXDistF ==0)
        sout << "\n\nNo X Initilizer specified\n\n";
      else
      {
          line(sout, "Maximum Emittance [pi-mm-mrad]", epsXLimInj);
          line(sout, "Beta [m]", betaXInj);
          line(sout, "Alpha", alphaXInj);
          line(sout, "Maximum Emittance [pi-mm-mrad]", epsXLimInj);
          line(sout, "RMS Emittance [pi-mm-mrad]", epsYRMSInj);
      }
    
    subsection(sout, "Vertical Distribution");
    if (nYDistF ==0)
         sout <<  "\n\nNo Y Initilizer specified\n\n";
      else
      {
          line(sout, "Maximum Emittance [pi-mm-mrad]", epsYLimInj);
          line(sout, "Beta [m]", betaYInj);
          line(sout, "Alpha", alphaYInj);
          line(sout, "Maximum Emittance [pi-mm-mrad]", epsYLimInj);
          line(sout, "RMS Emittance [pi-mm-mrad]", epsYRMSInj);
      }

    subsection(sout, "Longitudinal Distribution");
    if (nLDistF ==0)
         sout <<  "\n\nNo Longitudinal Initilizer specified\n\n";
      else
      {

          if(useUniformLI)
          {
              line(sout, "Energy Offset [GeV]", EOffset);
              line(sout, "Fractional uniform energy spread", deltaEFracInj);
              line(sout, "Minimum injection angle [deg]", phiMinInj);
              line(sout, "Maximum injection angle [deg]", phiMaxInj);
          }
          if(useGULI)
          {
              line(sout, "Mean energy [GeV]", EInjMean );
              line(sout, "Energy sigma [GeV]", EInjSigma);
              if (EInjMin >=0.) line(sout, "Min Energy [GeV]", EInjMin);
              if (EInjMax >=0.) line(sout, "Max Energy [GeV]", EInjMax);
              line(sout, "Minimum injection angle [deg]", phiMinInj);
              line(sout, "Maximum injection angle [deg]", phiMaxInj);
          }
          if(useSNSES)
          {
              line(sout, "Mean energy [GeV]", EInjMean );
              line(sout, "Energy sigma [GeV]", EInjSigma);
              if (EInjMin >=0.) line(sout, "Min Energy [GeV]", EInjMin);
              if (EInjMin >=0.) line(sout, "Max Energy [GeV]", EInjMax);
              line(sout, "Minimum injection angle [deg]", phiMinInj);
              line(sout, "Maximum injection angle [deg]", phiMaxInj);
              line(sout, "Mean jitter [GeV]", ECMean);
              line(sout, "Jitter sigma [GeV]", ECSigma);
              if (ECMin >=0.) line(sout, "Min Jitter [GeV]", ECMin);
              if (ECMin >=0.) line(sout, "Max Jitter [GeV]", ECMax);
              if (ECDriftI !=0.) line(sout, "Initial Centroid Drift [GeV]",
                                      ECDriftI);
              if (ECDriftF !=0.) line(sout, "Final Centroid Drift [GeV]",
                                      ECDriftF);
              if (DriftTime !=0.) line(sout, "Final Centroid Drift [GeV]",
                                       DriftTime);
              line(sout, "Spreader frequency", ESNu);
              line(sout, "Spreader phase", ESPhase);
              line(sout, "Spreader amplitude", ESMax);
          }
          if(useJohoLI)
	    {
              line(sout, "Joho exponent", MLJoho);
              line(sout, "delta E-max [GeV]", dELimInj);
              line(sout, "delta phi-max [deg]", phiLimInj);
	    }
      }

    subsection(sout, "Injection Intensity");

    line(sout, "Macros/turn injected", nMacrosPerTurn);
    line(sout, "Real particles/macro", nReals_Macro);
    line(sout, "Turn interval", injectTurnInterval);
    line(sout, "Max number of injected macros", nMaxMacroParticles);

    if(nFoils ==0 )
       section(sout, "No Foil used");            
      else
      {
          
        Foil *foil = Foil::safeCast(foilPointers(0));

        section(sout, "Foil Input");

        subsection(sout, "Foil Size:");
          line(sout, "Max Foil x [mm]", foil->_xMax);
          line(sout, "Min Foil x [mm]", foil->_xMin);
          line(sout, "Max Foil y [mm]", foil->_yMax);
          line(sout, "Min Foil y [mm]", foil->_yMin);
          line(sout, "Foil thickness [micro-g/cm2]", foil->_thick);
          line(sout, "Foil Z", Injection::foilZ);
          line(sout, "Foil density (g/cm3)", Injection::rhoFoil);
      }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showFoil(Ostream &sout)
//
// DESCRIPTION
//    Shows the some foil  output
//
// PARAMETERS
//    sout - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Output::showFoil(Ostream &sout)
{
    if(nFoils ==0 )
    {
            if(nMPIrank == 0 ) section(sout, "No Foil used");
            return;
     }

    if(nMPIrank == 0 ) section(sout, "Foil Output");

    Foil *foil = Foil::safeCast(foilPointers(0));

    miscFoilCalcs();
    if(nMPIrank != 0 ) return;

    subsection(sout, "Foil Output:");
          line(sout, "Max Foil temp [K]", peakFoilTemp); 
          line(sout, "Average hits/particle", avgFoilHits);         
          line(sout, "Fract. Injected parts miss foil", missFoilFrac);
          line(sout, "H0 4 & 5 states fraction", fH0States45);
          line(sout, "NES loss fraction", fLossNES);

    if(Injection::useFoilScattering)
      {
      subsection(sout, "Foil scattering enabled");
      line(sout, "Minimum Scattering angle [rad]", thetaScatMin);
      line(sout, "Scattering m.f.p. [cm]", lScatter);
      line(sout, "Average number of scatters", nScatters);       
      }
    else
      subsection(sout, "Foil scattering not used");
          
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showStart(Ostream &sout)
//
// DESCRIPTION
//    Shows some basic run input to a stream.
//
// PARAMETERS
//    sout - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Output::showStart(Ostream &sout)
{
  if(nMPIrank != 0 ) return;

  String t, v, st;

  Sys::dateTime(t);
  Sys::applVersion(v);

  st = "Case " + runName + " at " + t + " using ORBIT v" + v;
  sout << st << "\n";
  
  showRing(sout);
  showInject(sout);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showTurnInfo
//
// DESCRIPTION
//    Show general turn information 
//
// PARAMETERS
//    os - The Ostream to send info to
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Output::showTurnInfo(Ostream &os)
{

    static Integer first = 1;
    
  // Make sure stuff is calculated:

    miscFoilCalcs();

    calcEmitBase(mainHerd);

    MacroPart *mp = MacroPart::safeCast(mParts((mainHerd & All_Mask)-1) );

    int nMacroParticles_global = nMacroParticles;
    syncNMacs(nMacroParticles_global);
    
    if(nMPIrank != 0 ) return; 
  
    if(first)
    {
        first=0;
        os << turnHeader;
        os << turnLine;
    }
    
    os << setw(5) << nTurnsDone << " " << setw(0);
    os << setw(5) << time << " " << setw(0);    
    os << setw(8) << nMacroParticles_global  << " " << setw(0);
    os << setw(8) << (nMacroParticles_global *nReals_Macro) << " " << setw(0);
    os << setw(6) << avgFoilHits << " " << setw(0);    
    os << setw(9) << xEmitRMS << " " << setw(0);
    os << setw(9) << yEmitRMS << " " << setw(0);
    os << setw(9) << xEmitMax << " " << setw(0);
    os << setw(9) << yEmitMax << " " << setw(0);
    os << setw(7) << mp->_bunchFactor << " " << setw(0);        
    os << "\n";
}

Void Output::showTiming(Ostream &os, const Real &et)
{
  if(nMPIrank != 0 ) return; 

  section(os, "Timing Output");

  line(os, "Elapsed time", et);

  os << flush;
}

Void Output::clear()
{
  if(nMPIrank != 0 ) return; 

  system("clear");
}

Void Output::showTurn()
{
   MacroPart *mp = MacroPart::safeCast(mParts((mainHerd & All_Mask)-1) );

   int nLostMacros = mp->_lostOnes._nLostMacros;
   int nLostMacrosGlobal = nLostMacros;
   if( nMPIsize > 1 )
   {
     MPI_Allreduce(&nLostMacros, &nLostMacrosGlobal, 1, 
                   MPI_INT,MPI_SUM, MPI_COMM_WORLD);
   }

  // Make sure stuff is calculated:

    miscFoilCalcs();

    calcEmitBase(mainHerd);

    int nMacroParticles_global = nMacroParticles;
    syncNMacs(nMacroParticles_global);
    
    if(nMPIrank != 0 ) return; 

    cout << "Turn#  n-Macros  n-Macros Lost\n";
    cout << "-----  --------  -------------\n";

    cout << setw(5) << nTurnsDone << "  " << setw(0);
    cout << setw(8) << nMacroParticles_global  << "  " << setw(0);
    cout << setw(13) << nLostMacrosGlobal << setw(0);
    cout << "\n\n";

    Integer j, k, maxTurn = 1060;
    char bracket1, bracket2;
    String progress, space;
    Real percentage;

    bracket1 = 91;
    bracket2 = 93;
    progress = "#";
    space = " ";

    percentage = ((Real) nTurnsDone / (Real) maxTurn) * 100;
    cout << bracket1;
    for (j = 1; j <= (nTurnsDone / 53); j++) cout << progress;
    for (k = (nTurnsDone / 53); k < 20; k++) cout << space;
    cout << bracket2 << space;
    cout << setiosflags (ios::fixed | ios::showpoint) << setprecision(1);
    cout << percentage;
    cout << "%" << endl;

}
