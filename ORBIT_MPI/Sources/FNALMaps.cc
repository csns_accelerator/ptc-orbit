/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   FNALMaps.cc
//
// AUTHOR
//   Jeff Holmes
//   ORNL, jzh@ornl.gov
//
// CREATED
//   04/10/2002
//
// DESCRIPTION
//   Stub file for interfacing with FNAL Beamline Maps
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <iomanip>

#include "FNALMaps.h"
#include "HerdCoords.h"
#include "NodeDat.h"

#if defined(USE_FNALMaps)

#include "PhysicsConstants.h"
#include "beamline.h"
#include "bmlfactory.h"
#include "LattFuncSage.h"
#include "TransitionVisitor.h"

#endif   //USE_FNALMaps 

using namespace std;

///////////////////////////////////////////////////////////////////////////
// Temporary Array Dimensions
///////////////////////////////////////////////////////////////////////////
#define nEltDim 1000

#if defined(USE_FNALMaps)

madparser* mp = 0; // Nonsense needed for bmlfactory.
bmlfactory* bfPtr;
beamline* bmlPtr;
sector* sectPtr[nEltDim];
MatrixD* mtrxPtr[nEltDim];
// Mapping* mapPtr[nEltDim];
int nElts;

#endif   //USE_FNALMaps 

FNALTMs::FNALTMs(char* MADFile, char* MADLine, const int &maporder,
                 const double &E0, const double &ETotal,
                 const double &Charge)
{
  _MADFile = MADFile;
  _MADLine = MADLine;
  _maporder = maporder;
  _E0 = E0;
  _ETotal = ETotal;
  _Charge = Charge;
  _Gamma = _ETotal / _E0;
  _Beta = pow((1.0 - 1.0 / (_Gamma * _Gamma)), 0.5);
};

void FNALTMs::getLattice(int &nElements, double &gammaTrans,
                         NodeDat FNode[])
{
#if defined(USE_FNALMaps)

  double momentum = sqrt(_ETotal * _ETotal - _E0 * _E0);
  double brho = fabs(momentum) / (_Charge * PH_CNV_brho_to_p);

  bfPtr = new bmlfactory(_MADFile, brho);
  bmlPtr = bfPtr->create_beamline(_MADLine);

  Proton p(bmlPtr->Energy());
  int maporder = _maporder;
  if (_maporder == 0) maporder = 1;

  Jet::BeginEnvironment(maporder);
  coord x(0.0),  y(0.0),  z(0.0),
       px(0.0), py(0.0), pz(0.0);
  JetC::lastEnv = JetC::CreateEnvFrom(Jet::EndEnvironment());

  LattFuncSage lfs(bmlPtr, false);
  JetProton jpr(p);
  bmlPtr->propagate(jpr);

  int lfs_result = lfs.TuneCalc(&jpr, false);
  if( lfs_result != 0 )
  {
    cerr << "\n*** ERROR *** " << __FILE__ << ", line " << __LINE__ << ": "
         << "\n*** ERROR *** Something went wrong while "
         << "calculating tune: error no. "
         << lfs_result
         << "\n*** ERROR *** \n"
         << endl;
    return;
  }

  lfs.Slow_CS_Calc(&jpr);
  lfs.Disp_Calc(&jpr);

  const LattFuncSage::lattFunc* lfPtr = 0;
  DeepBeamlineIterator dbi(bmlPtr);
  bmlnElmnt* q = 0;
  nElements = 0;

  while(q = dbi++)
  {
    lfPtr = dynamic_cast<LattFuncSage::lattFunc*>(q->dataHook.find("Twiss"));
    if(lfPtr)
    {
      nElements++;

      strcpy(FNode[nElements]._eltType, q->Type());
      strcpy(FNode[nElements]._eltName, q->Name());
      FNode[nElements]._beta_X = lfPtr->beta.hor;
      FNode[nElements]._beta_Y = lfPtr->beta.ver;
      FNode[nElements]._alpha_X = lfPtr->alpha.hor;
      FNode[nElements]._alpha_Y = lfPtr->alpha.ver;
      FNode[nElements]._eta_x = lfPtr->dispersion.hor;
      FNode[nElements]._etap_x = lfPtr->dPrime.hor;
      FNode[nElements]._length = q->Length();
    }
  }
  dbi.reset();
  strcpy(FNode[0]._eltType, FNode[nElements]._eltType);
  strcpy(FNode[0]._eltName, FNode[nElements]._eltName);
  FNode[0]._beta_X = FNode[nElements]._beta_X;
  FNode[0]._beta_Y = FNode[nElements]._beta_Y;
  FNode[0]._alpha_X = FNode[nElements]._alpha_X;
  FNode[0]._alpha_Y = FNode[nElements]._alpha_Y;
  FNode[0]._eta_x = FNode[nElements]._eta_x;
  FNode[0]._etap_x = FNode[nElements]._etap_x;
  FNode[0]._length = 0.0;

  nElts = nElements;

  TransitionVisitor trv;
  bmlPtr->accept(trv);
  if(TransitionVisitor::OKAY == trv.getErrorCode())
  {
    double MomentumCompact = trv.getMomentumCompaction();
    gammaTrans = trv.getTransitionGamma();
  }
  else
  {
    cerr << "Error occurred: "
         << trv.getErrorMessage()
         << endl;
  }

  if(_maporder != 0)
  {
    int i;
    int j = 0;
    while(q = dbi++)
    {
      JetProton jp(bmlPtr->Energy());
      q->propagate(jp);
      Mapping map(jp.State());
      for(i = 0; i < map.Dim(); i++)
      {
        if(fabs(map(i).standardPart()) > 1.0e-8)
        {
          cerr << "\n*** WARNING *** Mapping for "
               << q->Type() << "  " << q->Name()
               << " does not map zero to zero. Be very careful!!"
               << endl;
        }
      }

      sectPtr[j]  = new sector(map, q->Length());
      mtrxPtr[j] = new MatrixD(map.Jacobian());
//      mapPtr[j]  = new Mapping(map);
      j++;
    }
  }
#else
  printf("There are no FNALMaps.\n Stop. \n");
  exit(1);
#endif   //USE_FNALMaps
}

void FNALTMs::stepNodeProtons(int &eltIndex, HerdCoords *Coords)
{
#if defined(USE_FNALMaps)

  int i, k;
  double p_x, p_npx, p_y, p_npy, p_cdt, p_ndp;
  Proton myProton;
//  Vector state(Particle::PSD);
  DeepBeamlineIterator dbi(bmlPtr);
  bmlnElmnt* q;

  q = 0;
  for (k = 1; k <= eltIndex; k++) q = dbi++;

  for(i = 0; i < Coords -> _nMacros; i++ )
  {
    myProton.SetReferenceEnergy(_ETotal);

    p_x = Coords -> _x_map[i];
    p_npx = Coords -> _xp_map[i];
    p_y = Coords -> _y_map[i];
    p_npy = Coords -> _yp_map[i];
    p_cdt = Coords -> _phi_map[i];
    p_ndp = Coords -> _deltaE_map[i];

    p_x /= 1000.;
    p_npx /= 1000.;
    p_y /= 1000.;
    p_npy /= 1000.;
    p_cdt *= (bmlPtr -> OrbitLength(myProton) / M_TWOPI);
    p_ndp /= (_ETotal * _Beta * _Beta);

    myProton.set_x(p_x);
    myProton.set_npx(p_npx);
    myProton.set_y(p_y);
    myProton.set_npy(p_npy);
    myProton.set_cdt(p_cdt);
    myProton.set_ndp(p_ndp);

    if (_maporder == 0)
    {
      q->propagate(myProton);
    }
    else
    {
      int j = eltIndex - 1;
      sectPtr[j]->propagate(myProton);  // Use sector reps of elements
//      state = myProton.State();
//      state = (*(mapPtr[j]))(state);   // Use maps directly
    }

    p_x = myProton.get_x();
    p_npx = myProton.get_npx();
    p_y = myProton.get_y();
    p_npy = myProton.get_npy();
    p_cdt = myProton.get_cdt();
    p_ndp = myProton.get_ndp();

    p_x *= 1000.;
    p_npx *= 1000.;
    p_y *= 1000.;
    p_npy *= 1000.;
    p_cdt /= (bmlPtr -> OrbitLength(myProton) / M_TWOPI);
    p_ndp *= (_ETotal * _Beta * _Beta);

    Coords -> _x_map[i] = p_x;
    Coords -> _xp_map[i] = p_npx;
    Coords -> _y_map[i] = p_y;
    Coords -> _yp_map[i] = p_npy;
    Coords -> _phi_map[i] = p_cdt;
    Coords -> _deltaE_map[i] = p_ndp;
  }

  for (i = eltIndex + 1; i <= nElts; i++) q = dbi++;
  q = dbi++;
  if(0 != q)
  {
    cerr << "ERROR: Miscount!!" << endl;
    return;
  }
  dbi.reset();
#else
  printf("There are no FNALMaps.\n Stop. \n");
  exit(1);
#endif   //USE_FNALMaps
}
