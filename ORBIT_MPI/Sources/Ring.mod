/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   Ring.mod
//
// AUTHOR
//   John Galambos,  ORNL, jdg@ornl.gov
//
// CREATED
//   10/02/97
//
// DESCRIPTION
//   Module descriptor file for the Ring module. This module contains
//   general information/manipulations common to all Node types.
//
// REVISION HISTORY
//   Modification to keep running time in ring.
//   04/30/2007
//
/////////////////////////////////////////////////////////////////////////////

module Ring - "Ring Module."
            - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, Particles;

Errors
  badNTurns    - "You've entered a bad number of turns",
  badNodeNum1  - "You are trying to turn to a node that has already passed"
               - "Specify a larger node number",
  badNodeNum2  - "You tried to turn to a node beyond the end of the Ring!",
  noParts2Turn - "Can't do a turn without a defining a MacroParticle",
  noRingNodes  - "Please construct a ring with nodes before trying "
               - "to do a turn",
  noHerd       - "Please specify a herd before doing a turn",
  badHerdNo    - "You are trying to advance a nonexistent herd",
  noSyncPart   - "No SyncPart available to update",
  nodeError    - "Oops, there was a Node error",
  badHerdPos   - "A herd is not in position to do the requested action";
Warnings
  badBucketGuess - "Oops I can't get started with the bucket separatrix calc."
                 - "Try another starting point guess";
public:

  Void ctor()
              - "Constructor for the Ring module."
              - "Initializes build values.";

  Integer
    nodesInitialized - "Flag to indicate node initialization status",
    ringInitialized  - "Flag to indicate ring initialization status",
    nTurnsDone       - "Number of turns completed by the mainHerd",
    nNodes           - "Number of Nodes in the Ring";

  Void initNodes() - "Routine to initialize nodes prior to tracking";
  Void initRing()  - "Routine to initialize Ring stuff prior to tracking";

// Routines to push herds:

  Void doTurn(const Integer &n)
                   - "Pushes the mainHerd n turns around the ring.";
  Void doTurnBase(Integer &herdNo, const Integer &n)
                   - "Pushes herd 'herdNo'  n turns around the ring.";
  Void turnToNode(Integer &herdNo, const Integer &n)
                   - "Push a herd to a specified node";
  Void finishTurn(Integer &herdNo)
                   - "Push a herd from present location to"
                   - "the end of the Ring";
  Void turnHerds( const Integer &n)
                   - "Pushes all the herds n turns around the ring.";
  Void clearNodes() - "Clear all the Nodes from the Ring";

// Overall Ring Stuff:

  Real
    alphaX         - "alpha X at start of Ring",
    alphaY         - "alpha Y at start of Ring",
    betaX          - "Beta X at start of Ring [m]",
    betaY          - "Beta Y at start of Ring [m]",
    betaXAvg       - "Average beta x [m]",
    betaYAvg       - "Average beta y [m]",
    chromaticityX  - "X chromaticity",
    chromaticityY  - "Y chromaticity",
    etaX           - "Dispersion in x [m]",
    etaPX          - "d X-Dispersion / dx ",
    etaY           - "Dispersion in y [m]",
    etaPY          - "d Y-Dispersion / dy",
    gammaX         - "Gamma X value at the start of Ring",
    gammaY         - "Gamma Y value at the start of Ring",
    lRing          - "Total ring length [m]",
    compactionFact - "Compaction factor",
    etaCompaction  - "The eta-compaction factor",
    gammaTrans     - "Transition gamma",
    nuX            - "X tune",
    nuY            - "Y tune",
    globalAperture - "A global circ. machine aperture. Executes "
		   - "if set to a value > 0. Default is -1.0",
// Longitudinal Stuff

    harmonicNumber - "Harmonic number";
  Void calcBucket(const Real &dE, const Real &phi)
                   - "Routine to calculate longitudinal bucket contour";

  Integer
    nBucketTurns  - "Number of turns required to calculate bucket contour",
    nLongUpdates  - "Number of longitudinal updates/turn, "
                  - "for long-only tracking",
    longTrackOnly - "Flag for long-only tracking";
   

   RealVector
    bucketphi     - "The bucket contour angle coordinates [deg]",
    bucketdE      - "The bucket contour energy coordinates [GeV]";
  Real
    bucketArea    - "The bucket area [ev-sec]",
    bucketHeight  - "The bucket height [ev]",
    bucketdE0     - "Starting delta E for bucket tracking (GeV)",
    bucketphi0    - "Parameter for starting delta phi for bucket tracking";

// Time stuff:

  Real
    time          - "Time since start of tracking (msec)";

private:

  Integer

   nPartTurnsDone - "Number of particles*turns completed by the mainHerd";

  IntegerVector
    numNodes      - "Ordered number for nodes";
}
