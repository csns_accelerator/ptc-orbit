/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TeaPot.mod
//
// AUTHOR
//   Joshua Abrams, Knox College, jabrams@knox.edu
//   Steven Bunch, University of Tennessee, sbunch2@utk.edu
//
// CREATED
//   09/09/02
//
// MODIFIED
//   03/24/03
//
// DESCRIPTION
//   Stacks thick-thin sequence to emulate long elements
//
// REVISION HISTORY
//   Added Multipole functions for Multipoles, Quadrupoles, and Bends
//
/////////////////////////////////////////////////////////////////////////////

module TeaPot - "TeaPot Module."
              - "Written by Joshua Abrams, jabrams@knox.edu"
{

  Inherits Consts, Particles, Ring, TransMap;

  Errors
  badHerdNo     - "Sorry, you asked for an undefined macro Herd",
  badMADFile    - "Sorry, I can't find the MAD file you specified",
  EOFTwiss      - "Oops - I'm at the end of the Twiss file. More TMs"
                - "than !0 length twiss sets. Check MAD deck for 0"
                - "length elements",
  badTeaPotNode - "Sorry, you asked for an undefined TeaPot Node",
  noTeaPotNode  - "Sorry, you asked for a nonexistent TeaPot Node";

  public:

  Integer
  nTPNodes    - "Number of TeaPot Nodes in the Ring",
  kickTurn    - "Turn number to apply single turn kick";

  Real
  hwaveT      - "Horizontal time kicker waveform",
  vwaveT      - "Vertical time kicker waveform",
  hwf         - "Varying horizontal kicker waveform factor",
  vwf         - "Varying vertical kicker waveform factor",
  dhwf        - "Constant horizontal kicker waveform factor",
  dvwf        - "Constant vertical kicker waveform factor",
  tstart      - "Start time of kicker waveform",
  tstop       - "End time of kicker waveform",
  tinc        - "Waveform incrementation interval",
  strinitial  - "Initial magnet waveform strength",
  strfinal    - "Final magnet waveform strength",
  tstrinitial - "Initial magnet waveform time",
  tstrfinal   - "Final magnet waveform time",
  strength    - "Varying magnet waveform factor",
  dPoP        - "delta P / P for chromatic matrix diagnostics",
  rhoInv      - "1 / Radius of curvature for each element";

  RealMatrix
  transportMatrix, xMatrix, yMatrix;

  Void ctor() - "Constructor for the TeaPot module.";

  Void nullOut(const String &n, const Integer &order,
               Integer &transmapNumber,
               Real &bx, Real &by,
               Real &ax, Real &ay,
               Real &ex, Real &epx,
               Real &length, const String &et)
               - "Routine to remove a transfer matrix";

  Void buildTPlattice(const String &MADTwissFile,
                      const String &MADLATFile,
                      const Integer &nstepTPD,
                      const Integer &nstepTPM,
                      const Integer &fringeM,
                      const Integer &nstepTPQ,
                      const Integer &fringeQ,
                      const Integer &nstepTPB,
                      const Integer &fringeB,
                      const Integer &nstepTPS,
                      const Integer &nstepTPK);

  Void buildTPlatticeNew(const String &LATFile,
                         const String &ring_line,
                         const Integer &nstepTPD,
                         const Integer &nstepTPM,
                         const Integer &fringeM,
                         const Integer &nstepTPQ,
                         const Integer &fringeQ,
                         const Integer &nstepTPB,
                         const Integer &fringeB,
                         const Integer &nstepTPS,
                         const Integer &nstepTPK);

  Void showTP(Ostream &os)
              - "Routine to print TP Lattice to a stream";

  Void dumpTPLAT(Ostream &os)
              - "Routine to dump detailed TP Lattice to a stream";

  Void replaceTPD(const String &n, const Integer &order,
                  const String &et,
		  const Integer &nsteps)
                  - "Routine to replace a drift";

  Void replaceTPM(const String &n, const Integer &order,
                  const String &et,
                  const Real &tilt,
                  const Integer &pole, const Real &kl,
                  const Integer &skew, 
		  const Integer &TPsubindex,
		  const Integer &nsteps,
                  const Integer &fringeIN, const Integer &fringeOUT)
		  - "Routine to replace a multipole";

  Void replaceTPQ(const String &n, const Integer &order,
                  const String &et,
                  const Real &tilt,
                  const Real &kq,
		  const Integer &TPsubindex,
		  const Integer &nsteps,
                  const Integer &fringeIN, const Integer &fringeOUT)
		  - "Routine to replace a quadrupole";

  Void replaceTPB(const String &n, const Integer &order,
                  const Real &l, const String &et,
                  const Real &tilt,
                  const Real &theta,
                  const Real &ea1, const Real &ea2,
                  const Integer &nsteps,
                  const Integer &fringeIN, const Integer &fringeOUT)
		  - "Routine to replace a bending magnet";

  Void replaceTPS(const String &n, const Integer &order,
                  const String &et,
                  const Real &B,
		  const Integer &TPsubindex,
		  const Integer &nsteps)
		  - "Routine to replace a solenoid";

  Void replaceTPMCF(const String &n, const Integer &order,
                    const String &et,
                    const Real &tilt,
                    const Integer &vecnum,
                    const IntegerVector &pole,
                    const RealVector &kl,
                    const IntegerVector &skew,
		    const Integer &TPsubindex,
                    const Integer &nsteps,
                    const Integer &fringeIN, const Integer &fringeOUT)
		    - "Routine to replace a combined function multipole";

  Void replaceTPQCF(const String &n, const Integer &order,
                    const String &et,
                    const Real &tilt, const Real &kq,
                    const Integer &vecnum,
                    const IntegerVector &pole,
                    const RealVector &kl,
                    const IntegerVector &skew,
		    const Integer &TPsubindex,
                    const Integer &nsteps,
                    const Integer &fringeIN, const Integer &fringeOUT)
		    - "Routine to replace a combined function quadrupole";

  Void replaceTPBCF(const String &n, const Integer &order,
                    const Real &l, const String &et,
                    const Real &tilt,
                    const Real &theta,
                    const Real &ea1, const Real &ea2,
                    const Integer &vecnum,
                    const IntegerVector &pole,
                    const RealVector &kl,
                    const IntegerVector &skew,
                    const Integer &nsteps,
                    const Integer &fringeIN, const Integer &fringeOUT)
		    - "Routine to replace a combined function bending magnet";

  Void replaceTPK(const String &n, const Integer &order,
                  const String &et,
                  const Real &hkick, const Real &vkick,
                  const Integer &TPsubindex,
                  const Integer &nsteps)
		  - "Routine to replace a time dependent kicker";

  Void addTPD(const String &n, const Integer &order,
              const Real &bx, const Real &by,
              const Real &ax, const Real &ay,
              const Real &ex, const Real &epx,
              const Real &l, const String &et,
              const Integer &nsteps)
              - "Routuine to add a drift";

  Void addTPM(const String &n, const Integer &order,
              const Real &bx, const Real &by,
              const Real &ax, const Real &ay,
              const Real &ex, const Real &epx,
              const Real &l, const String &et,
              const Real &tilt,
              const Integer &pole,  const Real &kl,
              const Integer &skew,
	      const Integer &TPsubindex,
	      const Integer &nsteps,
              const Integer &fringeIN, const Integer &fringeOUT)
              - "Routine to add a multipole";

  Void addTPQ(const String &n, const Integer &order,
              const Real &bx, const Real &by,
              const Real &ax, const Real &ay,
              const Real &ex, const Real &epx,
              const Real &l, const String &et,
              const Real &tilt,
              const Real &kq,
	      const Integer &TPsubindex,
	      const Integer &nsteps,
              const Integer &fringeIN, const Integer &fringeOUT)
              - "Routine to add a quadrupole";

  Void addTPB(const String &n, const Integer &order,
              const Real &bx, const Real &by,
              const Real &ax, const Real &ay,
              const Real &ex, const Real &epx,
              const Real &l, const String &et,
              const Real &tilt,
              const Real &theta,
              const Real &ea1, const Real &ea2,
              const Integer &nsteps,
              const Integer &fringeIN, const Integer &fringeOUT)
              - "Routine to add a bending magnet";

  Void addTPS(const String &n, const Integer &order,
              const Real &bx, const Real &by,
              const Real &ax, const Real &ay,
              const Real &ex, const Real &epx,
              const Real &l, const String &et,
              const Real &B,
	      const Integer &TPsubindex,
	      const Integer &nsteps)
              - "Routine to add a solenoid";

  Void addTPMCF(const String &n, const Integer &order,
                const Real &bx, const Real &by,
                const Real &ax, const Real &ay,
                const Real &ex, const Real &epx,
                const Real &l, const String &et,
                const Real &tilt,
                const Integer &vecnum,
                const IntegerVector &pole,
                const RealVector &kl,
                const IntegerVector &skew,
		const Integer &TPsubindex,
                const Integer &nsteps,
                const Integer &fringeIN, const Integer &fringeOUT)
                 - "Routine to add a combined function multipole";

  Void addTPQCF(const String &n, const Integer &order,
                const Real &bx, const Real &by,
                const Real &ax, const Real &ay,
                const Real &ex, const Real &epx,
                const Real &l, const String &et,
                const Real &tilt,
                const Real &kq, 
                const Integer &vecnum,
                const IntegerVector &pole,
                const RealVector &kl,
                const IntegerVector &skew,
		const Integer &TPsubindex,
                const Integer &nsteps,
                const Integer &fringeIN, const Integer &fringeOUT)
                - "Routine to add a combined function quadrupole";

  Void addTPBCF(const String &n, const Integer &order,
                const Real &bx, const Real &by,
                const Real &ax, const Real &ay,
                const Real &ex, const Real &epx,
                const Real &l, const String &et,
                const Real &tilt,
                const Real &theta,
                const Real &ea1, const Real &ea2,
                const Integer &vecnum,
                const IntegerVector &pole,
                const RealVector &kl,
                const IntegerVector &skew,
                const Integer &nsteps,
                const Integer &fringeIN, const Integer &fringeOUT)
                - "Routine to add a combined function bending magnet";

  Void addTPK(const String &n, const Integer &order,
              const Real &bx, const Real &by,
              const Real &ax, const Real &ay,
              const Real &ex, const Real &epx,
              const Real &l, const String &et,
              const Real &hkick, const Real &vkick,
              const Integer &TPsubindex,
              const Integer &nsteps)
              - "Routine to add a time dependent kicker";

  Void calcLatFuncs(const Integer &herdNo, const String &ring_line,
                    Ostream &sout)
                    - "Routine to calculate lattice functions";

}
