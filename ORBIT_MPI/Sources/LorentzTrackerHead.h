/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   LorentzTrackerHead.h
//
// AUTHOR
//   Jeff Holmes
//
// CREATED
//   03/03/2009
//
// MODIFIED
//
// DESCRIPTION
//   Define classes for tracking through time-dependent electromagnetic fields
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(__ThreeDLorentzMap__)
#define __ThreeDLorentzMap__

#include "Object.h"


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   LZBase
//
// INHERITANCE RELATIONSHIPS
//   LZBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a base class for storing LorentzTracker Node information.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class LZBase : public MapBase
{
  Declare_Standard_Members(LZBase, MapBase);

  public:
  LZBase(const String &n, const Integer &order,
         const Real &bx, const Real &by,
         const Real &ax, const Real &ay,
         const Real &ex, const Real &epx,
         const Real &l, const String &et);

  virtual Void nameOut(String &wname) { wname = _name; }
  virtual Void _updatePartAtNode(MacroPart &mp)=0;
  virtual Void _nodeCalculator(MacroPart &mp)=0;
  virtual Void _showLZ(Ostream &sout)=0;
};


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   LZ3DT
//
// INHERITANCE RELATIONSHIPS
//   LZ3DT -> LZBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class to track in time-dependent electromagnetic fields.
//
// PUBLIC MEMBERS
//   n       (str) --> _name    : Name for this node
//   order   (int) --> _oindex  : node order index
//   bx     (real) --> _betaX   : The horizontal beta value at the
//                                beginning of the Node [m]
//   by     (real) --> _betaY   : The vertical beta value at the
//                                beginning of the Node [m]
//   ax     (real) --> _alphaX  : The horizontal alpha value at the
//                                beginning of the Node
//   ay     (real) --> _alphaY  : The horizontal alpha  value at the
//                                beginning of the Node
//   ex     (real) --> _etaX    : The horizontal dispersion [m]
//   epx    (real) --> _etaPX   : The horizontal dispersion prime
//   l      (real) --> _length  : The length of the node
//   et   (string) --> _et      : Element Type
//   sub     (sub) --> _sub     : Subroutine to acquire fields
//   zi     (real) --> _zi      : Initial tracking position [m]
//   zf     (real) --> _zf      : Final tracking position [m]
//   ds     (real) --> _ds      : Integration step size
//   niters  (int) --> _niters  : Number predictor-corrector iterations
//   resid  (real) --> _resid   : Predictor-corrector residual
//   xrefi  (real) --> _xrefi   : Initial reference particle x [mm]
//   yrefi  (real) --> _yrefi   : Initial reference particle y [mm]
//   eulerai(real) --> _eulerai : Initial reference Euler angle alpha [mr]
//   eulerbi(real) --> _eulerbi : Initial reference Euler angle beta [mr]
//   eulergi(real) --> _eulergi : Initial reference Euler angle gamma [mr]
//   xreff  (real) --> _xreff   : Final reference particle x [mm]
//   yreff  (real) --> _yreff   : Final reference particle y [mm]
//   euleraf(real) --> _euleraf : Final reference Euler angle alpha [mr]
//   eulerbf(real) --> _eulerbf : Final reference Euler angle beta [mr]
//   eulergf(real) --> _eulergf : Final reference Euler angle gamma [mr]
//   sref   (real) --> _sref    : Path length of reference particle [m]
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class LZ3DT : public LZBase
{
  Declare_Standard_Members(LZ3DT, LZBase);
  public:

  LZ3DT(const String &n, const Integer &order,
        const Real &bx, const Real &by,
        const Real &ax, const Real &ay,
        const Real &ex, const Real &epx,
        const Real &l, const String &et,
        const Subroutine sub,
        const Real &zi, const Real &zf,
        const Real &ds, const Integer &niters,
        const Real &resid,
        const Real &xrefi, const Real &yrefi,
        const Real &eulerai, const Real &eulerbi,
        const Real &eulergi);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showLZ(Ostream &sout);

  Subroutine _sub;
  Integer _niters;
  Real  _zi, _zf, _ds, _resid;
  Real _xrefi, _yrefi, _eulerai, _eulerbi, _eulergi;
  Real _xreff, _yreff, _euleraf, _eulerbf, _eulergf, _sref;
};


#endif   // __ThreeDLorentzMap__
