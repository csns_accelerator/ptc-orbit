/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Plots.mod
//
// AUTHOR
//    John GalambosORNL, jdg@ornl.gov
//
// CREATED
//    9/7/98
//
//  DESCRIPTION
//    Module descriptor file for the Plots module
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Plots - "Plots Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, Particles, Ring, LSpaceCharge, Accelerate, Output;
Errors
   noPLPLOT    - "Sorry, I don't have the PLPLOT package installed",
   badHerd     - "You requested a plot of an unknown herd",
   badBinSize  - "The vector size you used for binning is bad";

public:

  Void ctor() 
                  - "Constructor for the Plots module.";

// Misc stuff:

  Void plotXWin(const Integer &herdNo) 
           - "Routine to plot the phase and real space distributions "
           - "of a Herd to an X window";
  Void plotPS(const Integer &herdNo, const String &fName) 
           - "Routine to plot the phase and real space distributions "
           - "of a Herd to a B&W postscipt file";
  Void plotHorizontal(const Integer &herdNo)
           - "Plots the X-X' phase";
  Void plotHorizontalPS(const Integer &herdNo, const String &fn)
           - "Plots the X-X' phase to a postscript file";
  Void plotVertical(const Integer &herdNo)
           - "Plots the Y-Y' phase";
  Void plotVerticalPS(const Integer &herdNo, const String &fn)
           - "Plots the Y-Y' phase to a postscript file";
  Void plotXY(const Integer &herdNo)
           - "Plots the X-Y space";
  Void plotXYPS(const Integer &herdNo, const String &fn)
           - "Plots the X-Y space to a postscript file";
  Void plotLong(const Integer &herdNo)
           - "Plots the longitudinal phase space";
  Void plotLongPS(const Integer &herdNo, const String &fn)
           - "Plots the longitudinal phase space to a postscript file";
  Void plotLongSet(const Integer &herdNo)
           - "Plots the longitudinal phase space distribution + space charge"
           - "to an X window";
  Void plotLongSetPS(const Integer &herdNo, const String &fn)
           - "Plots the longitudinal phase space distribution + space charge"
           - "to a postscript file";
Real

    xMinPlot   - "Minimum x for plots [mm]",
    xMaxPlot   - "Maximum x for plots [mm]",
    xPMinPlot  -"Minimum x-prime for plots [mrad]",
    xPMaxPlot  -"Maximum x-prime for plots [mrad]",

    yMinPlot   - "Minimum y for plots [mm]",
    yMaxPlot   - "Maximum y for plots [mm]",
    yPMinPlot  -"Minimum y-prime for plots [mrad]",
    yPMaxPlot  -"Maximum y-prime for plots [mrad]",

    dEMinPlot  - "Minimum value for dE plots [GeV]",
    dEMaxPlot  - "Maximum value for dE plots [GeV]",
    phiMinPlot - "Minimum value for the phase [rad]",
    phiMaxPlot - "Maximum value for the phase [rad]",
    LSCMinPlot - "Minimum value for Long. space charge plots [kV]",
    LSCMaxPlot - "Maximum value for Long. space charge plots [kV]";

Integer 
    nTransBins  - "Number of bins for transverse histograms",
    nDeltaEBins - "Number of bins for delta E histograms",
    plotFreq    - "Frequency of macros to plot in scatter plots",
    verbosePlot   - "Flag to put run info on plots (defaults = 1, true)";

Void  binHorizontal(const Integer &herdNo, RealVector &xBin, 
                    RealVector &yBin) - "Get a horizontal distribution";
Void  binVertical(const Integer &herdNo, RealVector &xBin, 
                  RealVector &yBin) - "Get a vertical distribution";
Void  binLongitudinal(const Integer &herdNo, RealVector &xBin, 
                      RealVector &yBin) - "Get a longitudinal distribution";
Void  binEnergy(const Integer &herdNo, RealVector &xBin, 
                RealVector &yBin) - "Get an energy distribution";
      
private:
  Void plot4pp(const Integer &herdNo) 
           - "Routine to plot the phase and real space distributions ";

  Void plotLongSetBase(const Integer &herdNo)
           - "Plots the longitudinal phase space distribution + space charge";
  Void plotHorizontalBase(const Integer &herdNo);
  Void plotVerticalBase(const Integer &herdNo);
  Void plotXYBase(const Integer &herdNo);
  Void plotLongBase(const Integer &herdNo);

   RealVector
     xPlot - "dummy vector",
     yPlot - "dummy vector";

Integer
    plotsInited   - "Flag for plot initilization";

}
