/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Correction.mod
//
// AUTHOR
//    Steven Bunch,  University of Tennessee, bunchsc@sns.gov
//
// CREATED
//    7/17/02
//
//  DESCRIPTION
//    Module descriptor file for the Correction module.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Correction - "Correction Module."
                  - "Written by Steven Bunch, Univ of TN, bunchsc@sns.gov"
{
  Inherits Consts, Particles, Ring, Error;

public:

  Void ctor()
                  - "Constructor for the Correction module."
                  - "Initializes build values.";

  Void corrector(const Integer &amherd, const Integer &atherd,
                 const Integer &axdcns, const Integer &axbpms,
                 IntegerVector &adcnx, IntegerVector &abpmx,
                 RealVector &axpj,
                 const Integer &aydcns, const Integer &aybpms,
                 IntegerVector &adcny, IntegerVector &abpmy,
                 RealVector &aypj,
                 const String &dist, RealVector &param, const Integer &aotp)
                  - "Corrector to correct errors in the ring";

  Void ORMcorrector(const String &fn, const Integer &bpms, const Integer &dcns)
                    - "Orbit Response Matrix Corrector";

}
