/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Error.cc
//
// AUTHOR
//    Steven Bunch
//    ORNL, bunchsc@sns.gov
//
// CREATED
//    6/17/02
//
//  DESCRIPTION
//    Module descriptor file for the Error module. This module
//    contains source for magnet alignment errors.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "MacroPart.h"
#include "MapBase.h"
#include "RealMat.h"
#include "StreamType.h"
#include "Error.h"
#include "ErrorBase.h"
#include "ErrorHead.h"
#include "StrClass.h"
#include "TransMapHead.h"
#include "TeaPotHead.h"
#include "TeaPot.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdio>

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

Array(ObjectPtr) ErrorPointers;
extern Array(ObjectPtr) nodes, tMaps, syncP;
Integer etfl, rtfl, sifl, sffl, yifl, yffl, ldfl, fifl, fffl;
Integer dxifl, dxffl, dyifl, dyffl, dlifl, dlffl, bifl, bffl, rifl, rffl;
Integer fi2fl, ff2fl;
Vector(Real) DBLNs;

///////////////////////////////////////////////////////////////////////////
//
// Local routines:
//
///////////////////////////////////////////////////////////////////////////

Void drift_S(MacroPart &mp, Integer &i, Real &length);


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS ErrorBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(ErrorBase, Node);

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS CoordDisplacement
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(CoordDisplacement, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    CoordDisplacement
//
// INHERITANCE RELATIONSHIPS
//    CoordDisplacement -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing generalized coordinate displacements
//
// PUBLIC MEMBERS
//    _dx
//    _dxp
//    _dy
//    _dyp
//    _dphi
//    _ddeltaE
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

CoordDisplacement::CoordDisplacement(const String &n,
                                     const Integer &order,
                                     const Real &dx, const Real &dxp,
                                     const Real &dy, const Real &dyp,
                                     const Real &dphi,
                                     const Real &ddeltaE):
                                     ErrorBase(n, order),
                                     _dx(dx), _dxp(dxp),
                                     _dy(dy), _dyp(dyp),
                                     _dphi(dphi), _ddeltaE(ddeltaE)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    CoordDisplacement::NodeCalculator
//
// DESCRIPTION
//    empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void CoordDisplacement::_nodeCalculator(MacroPart &mp)
{
}

Void CoordDisplacement::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (etfl == 0)
  {
    sout << "\n Displacement Errors:\n";
    sout << " Node      dx [mm]         dy [mm]\n";
    sout << "------ --------------- ---------------\n";
    etfl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _dx;
  sout << setw(15) << _dy;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    CoordDisplacement::updatePartAtNode
//
// DESCRIPTION
//    Adds displacement errors
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void CoordDisplacement::_updatePartAtNode(MacroPart& mp)
{
  Integer i;

  for(i = 1; i <= mp._nMacros; i++)
  {
    mp._x(i) += _dx;
    mp._xp(i) += _dxp;
    mp._y(i) += _dy;
    mp._yp(i) += _dyp;
    mp._phi(i) += _dphi;
    mp._deltaE(i) += _ddeltaE;
    mp._dp_p(i) = mp._deltaE(i) * mp._syncPart._dppFac;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS QuadKicker
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(QuadKicker, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    QuadKicker
//
// INHERITANCE RELATIONSHIPS
//    QuadKicker -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing quadrupole kicks
//
// PUBLIC MEMBERS
//    _k
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

QuadKicker::QuadKicker(const String &n,
                       const Integer &order,
                       const Real &k):
                       ErrorBase(n, order),
                       _k(k)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    QuadKicker::NodeCalculator
//
// DESCRIPTION
//    empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void QuadKicker::_nodeCalculator(MacroPart &mp)
{
}

Void QuadKicker::_showError(Ostream &sout)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    QuadKicker::updatePartAtNode
//
// DESCRIPTION
//    Quadrupole Kicker Action
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void QuadKicker::_updatePartAtNode(MacroPart& mp)
{
  Integer i;

  for(i = 1; i <= mp._nMacros; i++)
  {
    mp._xp(i) += _k * mp._x(i);
    mp._yp(i) -= _k * mp._y(i);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS QuadKickerOsc
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(QuadKickerOsc, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    QuadKickerOsc
//
// INHERITANCE RELATIONSHIPS
//    QuadKickerOsc -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing oscillating quadrupole kicks
//
// PUBLIC MEMBERS
//    _k
//    _freq
//    _phase
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

QuadKickerOsc::QuadKickerOsc(const String &n,
                               const Integer &order,
                               const Real &k, const Real &freq,
                               const Real &phase):
                               ErrorBase(n, order),
                               _k(k), _freq(freq), _phase(phase)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    QuadKickerOsc::NodeCalculator
//
// DESCRIPTION
//    empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void QuadKickerOsc::_nodeCalculator(MacroPart &mp)
{
}

Void QuadKickerOsc::_showError(Ostream &sout)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    QuadKickerOsc::updatePartAtNode
//
// DESCRIPTION
//    Oscillating Quadrupole Kicker Action
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void QuadKickerOsc::_updatePartAtNode(MacroPart& mp)
{
  Integer i;
  Real kick, tring, tconst;
  SyncPart *sp = SyncPart::safeCast(syncP(0));

  tring = Ring::lRing / ((sp->_betaSync) * Consts::vLight);
  tconst = (2.0 * Consts::pi * Ring::time)/(1000. * tring);

  for(i = 1; i <= mp._nMacros; i++)
  {
    kick = _k * sin(_freq * (tconst + mp._phi(i)) + _phase);

    mp._xp(i) += kick * mp._x(i);
    mp._yp(i) -= kick * mp._y(i);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS DipoleKickerOsc
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(DipoleKickerOsc, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    DipoleKickerOsc
//
// INHERITANCE RELATIONSHIPS
//    DipoleKickerOsc -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing oscillating dipole kicks
//
// PUBLIC MEMBERS
//    _k
//    _freq
//    _phase
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

DipoleKickerOsc::DipoleKickerOsc(const String &n,
                               const Integer &order,
                               const Real &k, const Real &freq,
                               const Real &phase):
                               ErrorBase(n, order),
                               _k(k), _freq(freq), _phase(phase)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    DipoleKickerOsc::NodeCalculator
//
// DESCRIPTION
//    empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void DipoleKickerOsc::_nodeCalculator(MacroPart &mp)
{
}

Void DipoleKickerOsc::_showError(Ostream &sout)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    DipoleKickerOsc::updatePartAtNode
//
// DESCRIPTION
//    Oscillating Dipole Kicker Action
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void DipoleKickerOsc::_updatePartAtNode(MacroPart& mp)
{
  Integer i;
  Real kick, tring, tconst;
  SyncPart *sp = SyncPart::safeCast(syncP(0));

  tring = Ring::lRing / ((sp->_betaSync) * Consts::vLight);
  tconst = (2.0 * Consts::pi * Ring::time)/(1000. * tring);

  for(i = 1; i <= mp._nMacros; i++)
  {
    kick = _k * sin(_freq * (tconst + mp._phi(i)) + _phase);

    mp._xp(i) += kick;
    mp._yp(i) -= kick;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LongDisplacement
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(LongDisplacement, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    LongDisplacement
//
// INHERITANCE RELATIONSHIPS
//    LongDisplacement -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Longitudinal Displacement
//
// PUBLIC MEMBERS
//    _ds
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

LongDisplacement::LongDisplacement(const String &n, const Integer &order,
                                   const Real &ds):
                                   ErrorBase(n, order), _ds(ds)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    LongDisplacement::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LongDisplacement::_nodeCalculator(MacroPart &mp)
{
}

Void LongDisplacement::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (ldfl == 0)
  {
    sout << "\n Longitudinal Displacement Errors:\n";
    sout << " Node   Displacement [mm]\n";
    sout << "------ -------------------\n";
    ldfl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _ds;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    LongDisplacement::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LongDisplacement::_updatePartAtNode(MacroPart& mp)
{
  Integer i;
  Real length = _ds / 1000.;
  for(i = 1; i <= mp._nMacros; i++)
  {
    drift_S(mp, i, length);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS StraightRotationXY
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(StraightRotationXY, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationXY
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationXY -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X-Y coordinate rotations
//
// PUBLIC MEMBERS
//    _anglexy
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

StraightRotationXY::StraightRotationXY(const String &n,
                                       const Integer &order,
                                       const Real &anglexy):
                                       ErrorBase(n, order),
                                       _anglexy(anglexy)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationXY::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationXY::_nodeCalculator(MacroPart &mp)
{
}

Void StraightRotationXY::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (rtfl == 0)
  {
    sout << "\n Rotation XY Errors:\n";
    sout << " Node   Angle XY [rad]\n";
    sout << "------ ---------------\n";
    rtfl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _anglexy;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationXY::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationXY::_updatePartAtNode(MacroPart& mp)
{
  Real xtemp, xptemp, ytemp, yptemp;
  Integer i;
  Real cs = cos(_anglexy);
  Real sn = sin(_anglexy);

  for(i = 1; i <= mp._nMacros; i++)
  {
    xtemp  = mp._x(i);
    xptemp = mp._xp(i);
    ytemp  = mp._y(i);
    yptemp = mp._yp(i);

    mp._x(i)  =  cs * xtemp  + sn * ytemp;
    mp._xp(i) =  cs * xptemp + sn * yptemp;
    mp._y(i)  = -sn * xtemp  + cs * ytemp;
    mp._yp(i) = -sn * xptemp + cs * yptemp;
   }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS StraightRotationXSI
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(StraightRotationXSI, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationXSI
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationXSI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X-S coordinate initial rotations
//
// PUBLIC MEMBERS
//    _anglexsi
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

StraightRotationXSI::StraightRotationXSI(const String &n,
                                         const Integer &order,
                                         const Real &anglexsi,
                                         const Real &length):
                                         ErrorBase(n, order),
                                         _anglexsi(anglexsi),
                                         _length(length)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationXSI::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationXSI::_nodeCalculator(MacroPart &mp)
{
}

Void StraightRotationXSI::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (sifl == 0)
  {
    sout << "\n Rotation XS Initial Errors:\n";
    sout << " Node   Angle XS [rad]\n";
    sout << "------ ---------------\n";
    sifl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _anglexsi;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationXSI::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationXSI::_updatePartAtNode(MacroPart& mp)
{
   Real xtemp, xptemp, ytemp, yptemp, ztemp, zptemp, z, zp, s;
   Integer i;

   Real cs = cos(_anglexsi);
   Real sn = sin(_anglexsi);
   Real length = 1000. * _length;
   Real lengtho2 = length / 2;

   Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;

   for(i = 1; i <= mp._nMacros; i++)
   {
     xtemp  =  mp._x(i);
     xptemp =  mp._xp(i);
     ytemp  =  mp._y(i);
     yptemp =  mp._yp(i);
     ztemp  = -1000. * mp._phi(i) / Factor;
     zptemp =  1000. * (1. + mp._dp_p(i));

     mp._x(i)    =  cs * xtemp  + sn * lengtho2;
     mp._xp(i)   =  cs * xptemp - sn * zptemp;
     z           =  sn * xtemp  + cs * ztemp;
     zp          =  sn * xptemp + cs * zptemp;
     mp._phi(i)  = -Factor * z / 1000.;
     mp._dp_p(i) =  zp / 1000. - 1.0;

     s = (-(1.0 - cs) * lengtho2 - sn * xtemp) / 1000.;

     //     Self consistent transverse -> longitudinal coupling:
     //     mp._deltaE(i) = mp._dp_p(i) / mp._syncPart._dppFac;

     drift_S(mp, i, s);
   }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS StraightRotationXSF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(StraightRotationXSF, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationXSF
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationXSF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X-S coordinate final rotations
//
// PUBLIC MEMBERS
//    _anglexsf
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

StraightRotationXSF::StraightRotationXSF(const String &n,
                                         const Integer &order,
                                         const Real &anglexsf,
                                         const Real &length):
                                         ErrorBase(n, order),
                                         _anglexsf(anglexsf),
                                         _length(length)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationXSF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationXSF::_nodeCalculator(MacroPart &mp)
{
}

Void StraightRotationXSF::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (sffl == 0)
  {
    sout << "\n Rotation XS Final Errors:\n";
    sout << " Node   Angle XS [rad]\n";
    sout << "------ ---------------\n";
    sffl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _anglexsf;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationXSF::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationXSF::_updatePartAtNode(MacroPart& mp)
{
   Real xtemp, xptemp, ytemp, yptemp, ztemp, zptemp, z, zp, s;
   Integer i;

   Real cs = cos(_anglexsf);
   Real sn = sin(_anglexsf);
   Real length = 1000. * _length;
   Real lengtho2 = length / 2;

   Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;

   for(i = 1; i <= mp._nMacros; i++)
   {
     xtemp  =  mp._x(i);
     xptemp =  mp._xp(i);
     ytemp  =  mp._y(i);
     yptemp =  mp._yp(i);
     ztemp  = -1000. * mp._phi(i) / Factor;
     zptemp =  1000. * (1. + mp._dp_p(i));

     mp._x(i)    =  cs * xtemp  + sn * lengtho2;
     mp._xp(i)   =  cs * xptemp + sn * zptemp;
     z           = -sn * xtemp  + cs * ztemp;
     zp          = -sn * xptemp + cs * zptemp;
     mp._phi(i)  = -Factor * z / 1000.;
     mp._dp_p(i) =  zp / 1000. - 1.0;

     s = ((1.0 - cs) * lengtho2 + sn * xtemp) / 1000.;

     //     Self consistent transverse -> longitudinal coupling:
     //     mp._dp_p(i) = zp / 1000. - 1.0;
     //     mp._deltaE(i) = mp._dp_p(i) / mp._syncPart._dppFac;
     //     Fix longitudinal momentum to pre rotation value:

     mp._dp_p(i) = mp._deltaE(i) * mp._syncPart._dppFac;
     drift_S(mp, i, s);
   }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS StraightRotationYSI
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(StraightRotationYSI, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationYSI
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationYSI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Y-S coordinate initial rotations
//
// PUBLIC MEMBERS
//    _angleysi
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

StraightRotationYSI::StraightRotationYSI(const String &n,
                                         const Integer &order,
                                         const Real &angleysi,
                                         const Real &length):
                                         ErrorBase(n, order),
                                         _angleysi(angleysi),
                                         _length(length)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationYSI::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationYSI::_nodeCalculator(MacroPart &mp)
{
}

Void StraightRotationYSI::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (yifl == 0)
  {
    sout << "\n Rotation YS Initial Errors:\n";
    sout << " Node   Angle YS [rad]\n";
    sout << "------ ---------------\n";
    yifl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _angleysi;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationYSI::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationYSI::_updatePartAtNode(MacroPart& mp)
{
   Real xtemp, xptemp, ytemp, yptemp, ztemp, zptemp, z, zp, s;
   Integer i;

   Real cs = cos(_angleysi);
   Real sn = sin(_angleysi);
   Real length = 1000. * _length;
   Real lengtho2 = length / 2;

   Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;

   for(i = 1; i <= mp._nMacros; i++)
   {
     xtemp  =  mp._x(i);
     xptemp =  mp._xp(i);
     ytemp  =  mp._y(i);
     yptemp =  mp._yp(i);
     ztemp  = -1000. * mp._phi(i) / Factor;
     zptemp =  1000. * (1. + mp._dp_p(i));

     mp._y(i)    =  cs * ytemp  - sn * lengtho2;
     mp._yp(i)   =  cs * yptemp + sn * zptemp;
     z           = -sn * ytemp  + cs * ztemp;
     zp          = -sn * yptemp + cs * zptemp;
     mp._phi(i)  = -Factor * z / 1000.;
     mp._dp_p(i) =  zp / 1000. - 1.0;

     s = (-lengtho2 * (1.0 - cs) + sn * ytemp) / 1000.;

     //     Self consistent transverse -> longitudinal coupling:
     //     mp._deltaE(i) = mp._dp_p(i) / mp._syncPart._dppFac;

     drift_S(mp, i, s);
   }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS StraightRotationYSF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(StraightRotationYSF, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationYSF
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationYSF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Y-S coordinate final rotations
//
// PUBLIC MEMBERS
//    _angleysf
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

StraightRotationYSF::StraightRotationYSF(const String &n,
                                         const Integer &order,
                                         const Real &angleysf,
                                         const Real &length):
                                         ErrorBase(n, order),
                                         _angleysf(angleysf),
                                         _length(length)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationYSF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationYSF::_nodeCalculator(MacroPart &mp)
{
}

Void StraightRotationYSF::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (yffl == 0)
  {
    sout << "\n Rotation YS Final Errors:\n";
    sout << " Node   Angle YS [rad]\n";
    sout << "------ ---------------\n";
    yffl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _angleysf;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StraightRotationYSF::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void StraightRotationYSF::_updatePartAtNode(MacroPart& mp)
{
   Real xtemp, xptemp, ytemp, yptemp, ztemp, zptemp, z, zp, s;
   Integer i;

   Real cs = cos(_angleysf);
   Real sn = sin(_angleysf);
   Real length = 1000. * _length;
   Real lengtho2 = length / 2;

   Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;

   for(i = 1; i <= mp._nMacros; i++)
   {
     xtemp  =  mp._x(i);
     xptemp =  mp._xp(i);
     ytemp  =  mp._y(i);
     yptemp =  mp._yp(i);
     ztemp  = -1000. * mp._phi(i) / Factor;
     zptemp =  1000. * (1. + mp._dp_p(i));

     mp._y(i)    =  cs * ytemp  - sn * lengtho2;
     mp._yp(i)   =  cs * yptemp - sn * zptemp;
     z           =  sn * ytemp  + cs * ztemp;
     zp          =  sn * yptemp + cs * zptemp;
     mp._phi(i)  = -Factor * z / 1000.;
     mp._dp_p(i) =  zp / 1000. - 1.0;

     s = (lengtho2 * (1.0 - cs) - sn * ytemp) / 1000.;

     //     Self consistent transverse -> longitudinal coupling:
     //     mp._dp_p(i) = zp / 1000. - 1.0;
     //     mp._deltaE(i) = mp._dp_p(i) / mp._syncPart._dppFac;
     //     Fix longitudinal momentum to pre rotation value:

     mp._dp_p(i) = mp._deltaE(i) * mp._syncPart._dppFac;
     drift_S(mp, i, s);
   }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BendFieldI
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BendFieldI, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendFieldI
//
// INHERITANCE RELATIONSHIPS
//    BendFieldI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Bend BendField coordinate initial errors
//
// PUBLIC MEMBERS
//    _drho
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

BendFieldI::BendFieldI(const String &n, const Integer &order,
                       const Real &drho):
                       ErrorBase(n, order), _drho(drho)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendFieldI::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendFieldI::_nodeCalculator(MacroPart &mp)
{
}

Void BendFieldI::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (fi2fl == 0)
  {
    sout << "\n Bend BendField Initial Errors:\n";
    sout << " Node     delta rho\n";
    sout << "------ ---------------\n";
    fi2fl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _drho;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendFieldI::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendFieldI::_updatePartAtNode(MacroPart& mp)
{
  Real xtemp;
  Integer i;

  for(i = 1; i <= mp._nMacros; i++)
  {
    xtemp = mp._x(i);
    mp._x(i) = xtemp - _drho;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BendFieldF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BendFieldF, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendFieldF
//
// INHERITANCE RELATIONSHIPS
//    BendFieldF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Bend BendField final errors
//
// PUBLIC MEMBERS
//    _drho
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

BendFieldF::BendFieldF(const String &n, const Integer &order,
                       const Real &drho):
                       ErrorBase(n, order), _drho(drho)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendFieldF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendFieldF::_nodeCalculator(MacroPart &mp)
{
}

Void BendFieldF::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (ff2fl == 0)
  {
    sout << "\n BendField Final Errors:\n";
    sout << " Node     delta rho\n";
    sout << "------ ---------------\n";
    ff2fl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _drho;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendFieldF::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendFieldF::_updatePartAtNode(MacroPart& mp)
{
  Real xtemp;
  Integer i;

  for(i = 1; i <= mp._nMacros; i++)
  {
    xtemp = mp._x(i);
    mp._x(i) = xtemp + _drho;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BendDisplacementXI
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BendDisplacementXI, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementXI
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementXI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X initial Dipole displacements
//
// PUBLIC MEMBERS
//    _anglexi
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

BendDisplacementXI::BendDisplacementXI(const String &n,
                                       const Integer &order,
                                       const Real &anglexi,
                                       const Real &disp):
                                       ErrorBase(n, order),
                                       _anglexi(anglexi),
                                       _disp(disp)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementXI::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementXI::_nodeCalculator(MacroPart &mp)
{
}

Void BendDisplacementXI::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (dxifl == 0)
  {
    sout << "\n Dipole Disp XI Errors:\n";
    sout << " Node   Disp XI [mm]\n";
    sout << "------ ---------------\n";
    dxifl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _disp;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementXI::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementXI::_updatePartAtNode(MacroPart& mp)
{
  Integer i;

  Real dx = -_disp * cos(_anglexi/2);
  Real ds = _disp * sin(_anglexi/2) / 1000.;

  for(i = 1; i <= mp._nMacros; i++)
  {
    mp._x(i) += dx;
  }
  drift_S(mp, i, ds);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BendDisplacementXF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BendDisplacementXF, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementXF
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementXF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X final Dipole displacements
//
// PUBLIC MEMBERS
//    _anglexf
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

BendDisplacementXF::BendDisplacementXF(const String &n,
                                       const Integer &order,
                                       const Real &anglexf,
                                       const Real &disp):
                                       ErrorBase(n, order),
                                       _anglexf(anglexf),
                                       _disp(disp)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementXF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementXF::_nodeCalculator(MacroPart &mp)
{
}

Void BendDisplacementXF::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (dxffl == 0)
  {
    sout << "\n Dipole Disp XF Errors:\n";
    sout << " Node   Disp XF [mm]\n";
    sout << "------ ---------------\n";
    dxffl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _disp;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementXF::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementXF::_updatePartAtNode(MacroPart& mp)
{
  Integer i;

  Real dx = _disp * cos(_anglexf/2);
  Real ds = _disp * sin(_anglexf/2) / 1000.;

  for(i = 1; i <= mp._nMacros; i++)
  {
    mp._x(i) += dx;
  }
  drift_S(mp, i, ds);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BendDisplacementYI
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BendDisplacementYI, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementYI
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementYI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Y initial Dipole displacements
//
// PUBLIC MEMBERS
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

BendDisplacementYI::BendDisplacementYI(const String &n,
                                       const Integer &order,
                                       const Real &disp):
                                       ErrorBase(n, order), _disp(disp)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementYI::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementYI::_nodeCalculator(MacroPart &mp)
{
}

Void BendDisplacementYI::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (dyifl == 0)
  {
    sout << "\n Dipole Disp YI Errors:\n";
    sout << " Node   Disp YI [mm]\n";
    sout << "------ ---------------\n";
    dyifl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _disp;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementYI::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementYI::_updatePartAtNode(MacroPart& mp)
{
  Integer i;

  for(i = 1; i <= mp._nMacros; i++)
  {
    mp._y(i) -= _disp;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BendDisplacementYF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BendDisplacementYF, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementYF
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementYF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Y final Dipole displacements
//
// PUBLIC MEMBERS
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

BendDisplacementYF::BendDisplacementYF(const String &n,
                                       const Integer &order,
                                       const Real &disp):
                                       ErrorBase(n, order), _disp(disp)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementYF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementYF::_nodeCalculator(MacroPart &mp)
{
}

Void BendDisplacementYF::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (dyffl == 0)
  {
    sout << "\n Dipole Disp YF Errors:\n";
    sout << " Node   Disp YF [mm]\n";
    sout << "------ ---------------\n";
    dyffl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _disp;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementYF::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementYF::_updatePartAtNode(MacroPart& mp)
{
   Integer i;

   for(i = 1; i <= mp._nMacros; i++)
   {
       mp._y(i) += _disp;
   }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BendDisplacementLI
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BendDisplacementLI, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementLI
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementLI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Longitudinal initial Dipole displacements
//
// PUBLIC MEMBERS
//    _angleli
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

BendDisplacementLI::BendDisplacementLI(const String &n,
                                       const Integer &order,
                                       const Real &angleli,
                                       const Real &disp):
                                       ErrorBase(n, order),
                                       _angleli(angleli), _disp(disp)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementLI::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementLI::_nodeCalculator(MacroPart &mp)
{
}

Void BendDisplacementLI::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (dlifl == 0)
  {
    sout << "\n Dipole Disp Long Initial Errors:\n";
    sout << " Node   Disp LI [mm]\n";
    sout << "------ ---------------\n";
    dlifl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _disp;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementLI::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementLI::_updatePartAtNode(MacroPart& mp)
{
  Integer i;

  Real dx = _disp * sin(_angleli/2);
  Real ds = _disp * cos(_angleli/2) / 1000.;

  for(i = 1; i <= mp._nMacros; i++)
  {
    mp._x(i) += dx;
  }
  drift_S(mp, i, ds);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BendDisplacementLF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BendDisplacementLF, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementLF
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementLF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Longitudinal final Dipole displacements
//
// PUBLIC MEMBERS
//    _anglelf
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

BendDisplacementLF::BendDisplacementLF(const String &n,
                                       const Integer &order,
                                       const Real &anglelf,
                                       const Real &disp):
                                       ErrorBase(n, order),
                                       _anglelf(anglelf), _disp(disp)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementLF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementLF::_nodeCalculator(MacroPart &mp)
{
}

Void BendDisplacementLF::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (dlffl == 0)
  {
    sout << "\n Dipole Disp Long Final Errors:\n";
    sout << " Node   Disp Long [mm]\n";
    sout << "------ ---------------\n";
    dlffl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _disp;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BendDisplacementLF::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BendDisplacementLF::_updatePartAtNode(MacroPart& mp)
{
  Integer i;

  Real dx = _disp * sin(_anglelf/2);
  Real ds = -_disp * cos(_anglelf/2) / 1000.;

  for(i = 1; i <= mp._nMacros; i++)
  {
    mp._x(i) += dx;
  }
  drift_S(mp, i, ds);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS RotationI
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(RotationI, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    RotationI
//
// INHERITANCE RELATIONSHIPS
//    RotationI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing initial rotations
//
// PUBLIC MEMBERS
//    _anglei
//    _rhoi
//    _theta
//    _length
//    _et
//    _type
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

RotationI::RotationI(const String &n, const Integer &order,
                     const Real &anglei, const Real &rhoi,
                     const Real &theta, const Real &length,
                     const String &et, const String &type):
                     ErrorBase(n, order), _anglei(anglei),
                     _rhoi(rhoi), _theta(theta), _length(length),
                     _et(et), _type(type)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    RotationI::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void RotationI::_nodeCalculator(MacroPart &mp)
{
}

Void RotationI::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (rifl == 0)
  {
    sout << "\n Rotation Initial Errors:\n";
    sout << " Node   Angle [rad]\n";
    sout << "------ ---------------\n";
    rifl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _anglei;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    RotationI::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void RotationI::_updatePartAtNode(MacroPart& mp)
{
  Real lengtho2, cb, sb, ce, se, s;
  Real ROT11, ROT12, ROT13,
       ROT21, ROT22, ROT23,
       ROT31, ROT32, ROT33;
  Real v1, v2, v3, vr1, vr2, vr3;
  Integer i;

  if((_et == "SBEN") || (_et == "sbend") || (_et == "rbend"))
  {
    lengtho2 = 1000. * tan(_theta/2) / _rhoi;
    cb = cos(_theta / 2);
    sb = sin(_theta / 2);
  }
  else
  {
    lengtho2 = 1000. * _length / 2;
    cb = 1.0;
    sb = 0.0;
  }

  ce = cos(_anglei);
  se = sin(_anglei);

  if(_type == "XS")
  {
    ROT11 = ce;
    ROT12 = 0;
    ROT13 = -se;
    ROT21 = 0;
    ROT22 = 1;
    ROT23 = 0;
    ROT31 = se;
    ROT32 = 0;
    ROT33 = ce;
  }

  if(_type == "YS")
  {
    ROT11 = (cb * cb) + (ce * sb * sb);
    ROT12 = sb * se;
    ROT13 = (cb * sb) - (cb * ce * sb);
    ROT21 = -sb * se;
    ROT22 = ce;
    ROT23 = cb * se;
    ROT31 = (cb * sb) - (cb * ce * sb);
    ROT32 = -cb * se;
    ROT33 = (cb * cb * ce) + (sb * sb);
  }

  if(_type == "XY")
  {
    ROT11 = (cb * cb * ce) + (sb * sb);
    ROT12 = cb * se;
    ROT13 = (-cb * sb) + (cb * ce * sb);
    ROT21 = -cb * se;
    ROT22 = ce;
    ROT23 = -sb * se;
    ROT31 = (-cb * sb) + (cb * ce * sb);
    ROT32 = sb * se;
    ROT33 = (cb * cb) + (ce * sb * sb);
  }

  for(i = 1; i <= mp._nMacros; i++)
  {
    v1 = mp._xp(i);
    v2 = mp._yp(i);
    v3 = 1000. * (1.0 + mp._dp_p(i));

    vr1 = ROT11 * v1 + ROT12 * v2 + ROT13 * v3;
    vr2 = ROT21 * v1 + ROT22 * v2 + ROT23 * v3;
    vr3 = ROT31 * v1 + ROT32 * v2 + ROT33 * v3;

    mp._xp(i) = vr1;
    mp._yp(i) = vr2;
    mp._dp_p(i) = vr3 / 1000. - 1.0;
    //     Self consistent transverse -> longitudinal coupling:
    //     mp._deltaE(i) = mp._dp_p(i) / mp._syncPart._dppFac;

    v1 = mp._x(i);
    v2 = mp._y(i);
    v3 = -lengtho2;

    vr1 = ROT11 * v1 + ROT12 * v2 + ROT13 * v3;
    vr2 = ROT21 * v1 + ROT22 * v2 + ROT23 * v3;
    vr3 = ROT31 * v1 + ROT32 * v2 + ROT33 * v3;

    mp._x(i) = vr1;
    mp._y(i) = vr2;
    s = (-lengtho2 - vr3) / 1000.;
    drift_S(mp, i, s);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS RotationF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(RotationF, ErrorBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    RotationF
//
// INHERITANCE RELATIONSHIPS
//    RotationF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing final rotations
//
// PUBLIC MEMBERS
//    _anglef
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

RotationF::RotationF(const String &n, const Integer &order,
                     const Real &anglef, const Real &rhoi,
                     const Real &theta, const Real &length,
                     const String &et, const String &type):
                     ErrorBase(n, order), _anglef(anglef),
                     _rhoi(rhoi), _theta(theta), _length(length),
                    _et(et), _type(type)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    RotationF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void RotationF::_nodeCalculator(MacroPart &mp)
{
}

Void RotationF::_showError(Ostream &sout)
{
  ErrorBase *nid;
  nid = this;
  if (rffl == 0)
  {
    sout << "\n Rotation Final Errors:\n";
    sout << " Node   Angle [rad]\n";
    sout << "------ ---------------\n";
    rffl = 1;
  }
  sout << setw(6) << nid->_oindex;
  sout << setw(15) << _anglef;
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    RotationF::updatePartAtNode
//
// DESCRIPTION
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void RotationF::_updatePartAtNode(MacroPart& mp)
{
  Real lengtho2, cb, sb, ce, se, s;
  Real ROT11, ROT12, ROT13,
       ROT21, ROT22, ROT23,
       ROT31, ROT32, ROT33;
  Real v1, v2, v3, vr1, vr2, vr3;
  Integer i;

  if((_et == "SBEN") || (_et == "sbend") || (_et == "rbend"))
  {
    cb = cos(_theta / 2);
    sb = sin(_theta / 2);
    lengtho2 = 1000. * tan(_theta/2) / _rhoi;
  }
  else
  {
    lengtho2 = 1000. * _length / 2;
    cb = 1.0;
    sb = 0.0;
  }

  ce = cos(_anglef);
  se = sin(_anglef);

  if(_type == "XS")
  {
    ROT11 = ce;
    ROT12 = 0;
    ROT13 = se;
    ROT21 = 0;
    ROT22 = 1;
    ROT23 = 0;
    ROT31 = -se;
    ROT32 = 0;
    ROT33 = ce;
  }

  if(_type == "YS")
  {
    ROT11 = (cb * cb) + (ce * sb * sb);
    ROT12 = sb * se;
    ROT13 = -(cb * sb) + (cb * ce * sb);
    ROT21 = -sb * se;
    ROT22 = ce;
    ROT23 = -cb * se;
    ROT31 = -(cb * sb) + (cb * ce * sb);
    ROT32 = cb * se;
    ROT33 = (cb * cb * ce) + (sb * sb);
  }

  if(_type == "XY")
  {
    ROT11 = (cb * cb * ce) + (sb * sb);
    ROT12 = -cb * se;
    ROT13 = (cb * sb) - (cb * ce * sb);
    ROT21 = cb * se;
    ROT22 = ce;
    ROT23 = -sb * se;
    ROT31 = (cb * sb) - (cb * ce * sb);
    ROT32 = sb * se;
    ROT33 = (cb * cb) + (ce * sb * sb);
  }

  for(i = 1; i <= mp._nMacros; i++)
  {
    v1 = mp._xp(i);
    v2 = mp._yp(i);
    v3 = 1000. * (1.0 + mp._dp_p(i));

    vr1 = ROT11 * v1 + ROT12 * v2 + ROT13 * v3;
    vr2 = ROT21 * v1 + ROT22 * v2 + ROT23 * v3;
    vr3 = ROT31 * v1 + ROT32 * v2 + ROT33 * v3;

    mp._xp(i) = vr1;
    mp._yp(i) = vr2;
    //     Self consistent transverse -> longitudinal coupling:
    //    mp._dp_p(i) = vr3 / 1000. - 1.0;
    //    mp._deltaE(i) = mp._dp_p(i) / mp._syncPart._dppFac;
    //     Fix longitudinal momentum to pre rotation value:
    mp._dp_p(i) = mp._deltaE(i) * mp._syncPart._dppFac;

    v1 = mp._x(i);
    v2 = mp._y(i);
    v3 = lengtho2;

    vr1 = ROT11 * v1 + ROT12 * v2 + ROT13 * v3;
    vr2 = ROT21 * v1 + ROT22 * v2 + ROT23 * v3;
    vr3 = ROT31 * v1 + ROT32 * v2 + ROT33 * v3;

    mp._x(i) = vr1;
    mp._y(i) = vr2;
    s = (lengtho2 - vr3) / 1000.;
    drift_S(mp, i, s);
  }
}


///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE Error
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Error::ctor
//
// DESCRIPTION
//    Initializes the various Error related constants.
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::ctor()
{
// set initial values

  nErrNodes = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addCoordDisplacement
//
// DESCRIPTION
//    Adds CoordDisplacement Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    dx
//    dxp
//    dy
//    dyp
//    dphi
//    ddeltaE
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addCoordDisplacement(const String &name, const Integer &order,
                                 const Real &dx, const Real &dxp,
                                 const Real &dy, const Real &dyp,
                                 const Real &dphi, const Real &ddeltaE)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new CoordDisplacement(name, order, dx, dxp,
                                          dy, dyp, dphi, ddeltaE);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addQuadKicker
//
// DESCRIPTION
//    Adds Quadrupole Kicker
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    k
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addQuadKicker(const String &name, const Integer &order,
                          const Real &k)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new QuadKicker(name, order, k);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addQuadKickerOsc
//
// DESCRIPTION
//    Adds Quadrupole Kicker
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    k
//    freq
//    phase
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addQuadKickerOsc(const String &name, const Integer &order,
                              const Real &k, const Real &freq,
                              const Real &phase)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new QuadKickerOsc(name, order, k, freq, phase);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addDipoleKickerOsc
//
// DESCRIPTION
//    Adds Dipole Kicker
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    k
//    freq
//    phase
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addDipoleKickerOsc(const String &name, const Integer &order,
                              const Real &k, const Real &freq,
                              const Real &phase)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new DipoleKickerOsc(name, order, k, freq, phase);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addDipoleCorrectorNode
//
// DESCRIPTION
//    Adds Dipole Corrector Node
//
// PARAMETERS
//    name
//    order
//    dxp
//    dyp
//
// RETURNS
//    Pointer to Dipole Corrector Node
//
///////////////////////////////////////////////////////////////////////////

Integer Error::addDipoleCorrectorNode(const String &name,
                                      const Integer &order,
                                      const Real &dxp, const Real &dyp)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new CoordDisplacement(name, order, 0, dxp,
                                          0, dyp, 0, 0);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;

  return nErrNodes;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Error::setDipoleCorrectorNodeVal
//
// DESCRIPTION
//    Sets Dipole Corrector Node
//
// PARAMETERS
//    nodeindex
//    dp
//    XorY
//
// RETURNS
//    nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::setDipoleCorrectorNodeVal(const Integer &nodeindex,
	                              const Real &dp, const String &XorY)
{
   CoordDisplacement *dc = CoordDisplacement::safeCast
                           (ErrorPointers(nodeindex-1));
   if(XorY == "X")dc->_dxp = dp;
   if(XorY == "Y")dc->_dyp = dp;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addStraightRotationXY
//
// DESCRIPTION
//    Adds a Rotation XY Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    anglexy
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addStraightRotationXY(const String &name,
                                  const Integer &order,
                                  const Real &anglexy)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new StraightRotationXY(name, order, anglexy);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addStraightRotationXSI
//
// DESCRIPTION
//    Adds a Rotation XS initial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    anglexsi
//    length
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addStraightRotationXSI(const String &name,
                                   const Integer &order,
                                   const Real &anglexsi,
                                   const Real &length)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new StraightRotationXSI(name, order, anglexsi, length);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addStraightRotationXSF
//
// DESCRIPTION
//    Adds a Rotation XS finial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    anglexsf
//    length
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addStraightRotationXSF(const String &name,
                                   const Integer &order,
                                   const Real &anglexsf,
                                   const Real &length)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new StraightRotationXSF(name, order, anglexsf, length);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addStraightRotationYSI
//
// DESCRIPTION
//    Adds a Rotation YS initial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    angleysi
//    length
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addStraightRotationYSI(const String &name,
                                   const Integer &order,
                                   const Real &angleysi,
                                   const Real &length)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new StraightRotationYSI(name, order, angleysi, length);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addStraightRotationYSF
//
// DESCRIPTION
//    Adds a Rotation YS finial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    angleysf
//    length
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addStraightRotationYSF(const String &name,
                                   const Integer &order,
                                   const Real &angleysf,
                                   const Real &length)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new StraightRotationYSF(name, order, angleysf, length);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addLongDisplacement
//
// DESCRIPTION
//    Adds a Longitudinal Displacement Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    ds
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addLongDisplacement(const String &name,
                                const Integer &order,
                                const Real &ds)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new LongDisplacement(name, order, ds);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendFieldI
//
// DESCRIPTION
//    Adds a BendField initial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    drho
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendFieldI(const String &name, const Integer &order,
                          const Real &drho)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new BendFieldI(name, order, drho);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendFieldF
//
// DESCRIPTION
//    Adds a field finial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    drho
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendFieldF(const String &name, const Integer &order,
                           const Real &drho)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new BendFieldF(name, order, drho);
   nErrNodes++;
   ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
   nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendDisplacementXI
//
// DESCRIPTION
//    Adds a Dipole Disp X initial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    anglexi
//    disp
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendDisplacementXI(const String &name,
                                  const Integer &order,
                                  const Real &anglexi,
                                  const Real &disp)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new BendDisplacementXI(name, order, anglexi, disp);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendDisplacementXF
//
// DESCRIPTION
//    Adds a Dipole Disp X final Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    anglexf
//    disp
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendDisplacementXF(const String &name,
                                  const Integer &order,
                                  const Real &anglexf,
                                  const Real &disp)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new BendDisplacementXF(name, order, anglexf, disp);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendDisplacementYI
//
// DESCRIPTION
//    Adds a Dipole Disp Y initial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    disp
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendDisplacementYI(const String &name,
                                  const Integer &order,
                                  const Real &disp)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new BendDisplacementYI(name, order, disp);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendDisplacementYF
//
// DESCRIPTION
//    Adds a Dipole Disp Y final Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    disp
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendDisplacementYF(const String &name,
                                  const Integer &order,
                                  const Real &disp)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new BendDisplacementYF(name, order, disp);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendDisplacementLI
//
// DESCRIPTION
//    Adds a Dipole Disp Long initial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    angleli
//    disp
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendDisplacementLI(const String &name,
                                  const Integer &order,
                                  const Real &angleli,
                                  const Real &disp)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new BendDisplacementLI(name, order, angleli, disp);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendDisplacementLF
//
// DESCRIPTION
//    Adds a Dipole Disp Long final Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    angleylf
//    disp
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendDisplacementLF(const String &name,
                                  const Integer &order,
                                  const Real &anglelf,
                                  const Real &disp)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

   nNodes++;

  nodes(nNodes-1) = new BendDisplacementLF(name, order, anglelf, disp);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addRotationI
//
// DESCRIPTION
//    Adds a Bend initial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    anglei
//    rhoi
//    theta
//    length
//    et
//    type
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addRotationI(const String &name, const Integer &order,
                         const Real &anglei, const Real &rhoi,
                         const Real &theta, const Real &length,
                         const String &et, const String &type)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new RotationI(name, order, anglei, rhoi,
                                  theta, length, et, type);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addRotationF
//
// DESCRIPTION
//    Adds a Rotation finial Error
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    anglef
//    rhoi
//    theta
//    length
//    et
//    type
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addRotationF(const String &name, const Integer &order,
                         const Real &anglef, const Real &rhoi,
                         const Real &theta, const Real &length,
                         const String &et, const String &type)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nErrNodes == ErrorPointers.size())
    ErrorPointers.resize(nErrNodes + 1);

  nNodes++;

  nodes(nNodes-1) = new RotationF(name, order, anglef, rhoi,
                                  theta, length, et, type);
  nErrNodes++;
  ErrorPointers(nErrNodes-1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addTransDispError
//
// DESCRIPTION
//    Adds a Transverse Displacement Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First node order index
//    order2:  Second node order index
//    dx
//    dy
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addTransDispError(const String &name,
                              const Integer &order1,
                              const Integer &order2,
                              const Real &dx, const Real &dy)
{
  Real ndx, ndy;

  ndx = -dx;
  ndy = -dy;

  addCoordDisplacement(name, order1, ndx, 0., ndy, 0., 0., 0.);
  addCoordDisplacement(name, order2, dx, 0., dy, 0., 0., 0.);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addTransDispErrorSet
//
// DESCRIPTION
//    Adds a Shift Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    et:           Element type
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addTransDispErrorSet(const String &name, const String &dist,
                                 Vector(Real) &param, const String &et,
                                 const Integer &nTransMapMin,
                                 const Integer &nTransMapMax,
                                 const Integer &order)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  Integer j, js, norder, fl, jMin, jMax;
  String eletype;
  Real dx, dy, ndx, ndy;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));

  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          ndx = param(2) + drand(0)*(param(3) - param(2));
          ndy = param(5) + drand(0)*(param(6) - param(5));
        }
        if(dist == "GAUSS")
        {
          ndx = getGauss(param(2), param(3), param(4));
          ndy = getGauss(param(5), param(6), param(7));
        }
        norder -=order;
        addCoordDisplacement(name, norder, ndx, 0., ndy, 0., 0., 0.);
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        dx = -ndx;
        dy = -ndy;
        norder -=(10-order);
        addCoordDisplacement(name, norder, dx, 0., dy, 0., 0., 0.);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          ndx = param(2) + drand(0)*(param(3) - param(2));
          ndy = param(5) + drand(0)*(param(6) - param(5));
        }
        if(dist == "GAUSS")
        {
          ndx = getGauss(param(2), param(3), param(4));
          ndy = getGauss(param(5), param(6), param(7));
        }
        norder -=order;
        addCoordDisplacement(name, norder, ndx, 0., ndy, 0., 0., 0.);
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        dx = -ndx;
        dy = -ndy;
        norder -=(10-order);
        addCoordDisplacement(name, norder, dx, 0., dy, 0., 0., 0.);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addStraightRotationError
//
// DESCRIPTION
//    Adds a Straight Rotation Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First node order index
//    order2:  Second node order index
//    angle
//    type
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addStraightRotationError(const String &name,
                                     const Integer &order1,
                                     const Integer &order2,
                                     const Real &angle,
                                     const String &type)
{
  Integer o1, o2, i;
  Real l = 0.0;
  MapBase *tml;

  o1 = (order1/10) + 1;
  o2 = order2/10;

  for(i = o1; i <= o2; i++)
  {
    tml = MapBase::safeCast(tMaps(i-1));
    l += tml->_length;
  }

  if(type == "XY")
  {
    addStraightRotationXY(name, order1, angle);
    addStraightRotationXY(name, order2, -angle);
  }

  if(type == "XS")
  {
    addStraightRotationXSI(name, order1, angle, l);
    addStraightRotationXSF(name, order2, angle, l);
  }

  if(type == "YS")
  {
    addStraightRotationYSI(name, order1, angle, l);
    addStraightRotationYSF(name, order2, angle, l);
  }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addStraightRotationErrorSet
//
// DESCRIPTION
//    Adds a Rotation Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    et:           Element type
//    type
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addStraightRotationErrorSet(const String &name,
                                        const String &dist,
                                        Vector(Real) &param,
                                        const String &et,
                                        const String &type,
                                        const Integer &nTransMapMin,
                                        const Integer &nTransMapMax,
                                        const Integer &order)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  MapBase *tml;
  Integer j, js, norder, norder1, norder2, fl, i, jMin, jMax;
  String eletype;
  Real ang, l;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));
 
  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          ang = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          ang = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder/10;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = (norder-10)/10;
        l = 0.0;
        for(i = norder1; i <= norder2; i++)
        {
          tml = MapBase::safeCast(tMaps(i-1));
          l += tml->_length;
        }
        norder1 =(norder1*10)-order;
        norder2 =(norder2*10)+order;
        addStraightRotationError(name,norder1,norder2,ang,type);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          ang = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          ang = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder/10;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = (norder-10)/10;
        l = 0.0;
        for(i = norder1; i <= norder2; i++)
        {
          tml = MapBase::safeCast(tMaps(i-1));
          l += tml->_length;
        }
        norder1 =(norder1*10)-order;
        norder2 =(norder2*10)+order;
        addStraightRotationError(name,norder1,norder2,ang,type);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addLongDispError
//
// DESCRIPTION
//    Adds a Longitudinal Displacement Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First  node order index
//    order2:  Second  node order index
//    ds
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addLongDispError(const String &name,
                             const Integer &order1,
                             const Integer &order2,
                             const Real &ds)
{
  addLongDisplacement(name, order1, ds);
  addLongDisplacement(name, order2, -ds);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addLongDispErrorSet
//
// DESCRIPTION
//    Adds a Longitudinal Shift Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    et:           Element type
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addLongDispErrorSet(const String &name,
                                const String &dist,
                                Vector(Real) &param,
                                const String &et,
                                const Integer &nTransMapMin,
                                const Integer &nTransMapMax,
                                const Integer &order)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  Integer j, js, norder, fl, jMin, jMax;
  String eletype;
  Real ds;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));

  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          ds = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          ds = getGauss(param(2), param(3), param(4));
        }
        norder -=order;
        addLongDisplacement(name, norder, -ds);
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder -=(10-order);
        addLongDisplacement(name, norder, ds);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          ds = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          ds = getGauss(param(2), param(3), param(4));
        }
        norder -=order;
        addLongDisplacement(name, norder, -ds);
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder -=(10-order);
        addLongDisplacement(name, norder, ds);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addMultFieldError
//
// DESCRIPTION
//    Adds a Mult Field Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First node order index
//    order2:  Second node order index
//    et
//    tilt
//    pole
//    kl
//    skew
//    err
//    TPsubindex
//    nsteps
//    fringeIN
//    fringeOUT
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addMultFieldError(const String &name,
                              const Integer &order1,
                              const Integer &order2,
                              const String &et,
                              const Real &tilt,
                              const Integer &pole,
                              const Real &kl,
                              const Integer &skew,
                              const Real &err,
                              const Integer &TPsubindex,
                              const Integer &nsteps,
                              const Integer &fringeIN,
                              const Integer &fringeOUT)
{
  Integer pol, skw, frI, frO, o1, o2, nelts, i, order, nstps;
  Real tlt, k;
  TPM *TP;

  o1 = order1/10;
  o2 = order2/10;
  nelts = (o2 - o1) + 1;

  tlt = tilt;
  pol = pole;
  k = kl * (1.0 + err) / nelts;
  skw = skew;

  for(i = o1; i <= o2; i++)
  {
    if(nsteps > 0)
    {
      nstps = nsteps;
      order = i * 10;
      frI = fringeIN;
      frO = fringeOUT;
      if(i != o1)
      {
        frI = 0;
      }
      if(i != o2)
      {
        frO = 0;
      }
      TeaPot::replaceTPM("TPM", order, et, tlt,
                         pol, k, skw,
                         TPsubindex, nstps, frI, frO);
    }
    else
    {
      TP = TPM::safeCast(tMaps(i-1));
      TP->_kl = TP->_kl * (1.0 + err);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addMultFieldErrorSet
//
// DESCRIPTION
//    Adds a Mult Field Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    et:           Element type
//    tilt:         Element role
//    pole
//    kl
//    skew
//    TPsubindex
//    nsteps
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addMultFieldErrorSet(const String &name,
                                 const String &dist,
                                 Vector(Real) &param,
                                 const String &et,
                                 const Real &tilt,
                                 const Integer &pole,
                                 const Real &kl,
                                 const Integer &skew,
                                 const Integer &TPsubindex,
                                 const Integer &nsteps,
                                 const Integer &fringeIN,
                                 const Integer &fringeOUT,
                                 const Integer &nTransMapMin,
                                 const Integer &nTransMapMax)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  Integer j, js, norder, norder1, norder2, fl, i, jMin, jMax;
  String eletype;
  Real err;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));

  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          err = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          err = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = norder-10;
        addMultFieldError(name, norder1, norder2,
                          et, tilt, pole, kl, skew, err,
                          TPsubindex, nsteps, fringeIN, fringeOUT);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          err = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          err = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = norder-10;
        addMultFieldError(name, norder1, norder2,
                          et, tilt, pole, kl, skew, err,
                          TPsubindex, nsteps, fringeIN, fringeOUT);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addQuadFieldError
//
// DESCRIPTION
//    Adds a Quad Field Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First node order index
//    order2:  Second node order index
//    et
//    tilt
//    kq
//    err
//    TPsubindex
//    nsteps
//    fringeIN
//    fringeOUT
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addQuadFieldError(const String &name,
                              const Integer &order1,
                              const Integer &order2,
                              const String &et,
                              const Real &tilt,
                              const Real &kq,
                              const Real &err,
                              const Integer &TPsubindex,
                              const Integer &nsteps,
                              const Integer &fringeIN,
                              const Integer &fringeOUT)
{
  Integer frI, frO, o1, o2, nelts, i, order, nstps;
  Real tlt, k;
  TPQ *TP;

  tlt = tilt;
  o1 = order1/10;
  o2 = order2/10;
  nelts = (o2 - o1) + 1;

  k = kq * (1.0 + err);

  for(i = o1; i <= o2; i++)
  {
    if(nsteps > 0)
    {
      nstps = nsteps;
      order = i * 10;
      frI = fringeIN;
      frO = fringeOUT;
      if(i != o1)
      {
        frI = 0;
      }
      if(i != o2)
      {
        frO = 0;
      }
      TeaPot::replaceTPQ("TPQ", order, et, tlt, k,
                         TPsubindex, nstps, frI, frO);
    }
    else
    {

      TP = TPQ::safeCast(tMaps(i-1));
      TP->_kq = TP->_kq * (1.0 + err);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addQuadFieldErrorSet
//
// DESCRIPTION
//    Adds a Quad Field Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    et:           Element type
//    tilt:         Element role
//    kq
//    TPsubindex
//    nsteps
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addQuadFieldErrorSet(const String &name,
                                 const String &dist,
                                 Vector(Real) &param,
                                 const String &et,
                                 const Real &tilt,
                                 const Real &kq,
                                 const Integer &TPsubindex,
                                 const Integer &nsteps,
                                 const Integer &fringeIN,
                                 const Integer &fringeOUT,
                                 const Integer &nTransMapMin,
                                 const Integer &nTransMapMax)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  Integer j, js, norder, norder1, norder2, fl, i, jMin, jMax;
  String eletype;
  Real err;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));

  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          err = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          err = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = norder-10;
        addQuadFieldError(name, norder1, norder2,
                          et, tilt, kq, err,
                          TPsubindex, nsteps, fringeIN, fringeOUT);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          err = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          err = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = norder-10;
        addQuadFieldError(name, norder1, norder2,
                          et, tilt, kq, err,
                          TPsubindex, nsteps, fringeIN, fringeOUT);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendFieldError
//
// DESCRIPTION
//    Adds a Bend BendField Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First node order index
//    order2:  Second node order index
//    et
//    tilt
//    theta
//    ea1
//    ea2
//    err
//    nsteps
//    fringeIN
//    fringeOUT
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendFieldError(const String &name,
                              const Integer &order1,
                              const Integer &order2,
                              const String &et,
                              const Real &tilt,
                              const Real &theta,
                              const Real &ea1,
                              const Real &ea2,
                              const Real &err,
                              const Integer &nsteps,
                              const Integer &fringeIN,
                              const Integer &fringeOUT)
{
  Integer frI, frO, o1, o2, nelts, i, order, nstps;
  Real tlt, e1, e2, l = 0.0, th, rho, rhon, drho, ln, thetan;
  TPB *TP;
  MapBase *tml;

  tlt = tilt;
  o1 = (order1/10) + 1;
  o2 = order2/10;
  nelts = (o2 - o1) + 1;

  if(nsteps > 0)
  {
    nstps = nsteps;
    th = theta;
  }
  else
  {
    nstps = -nsteps;
    th = 0.0;
  }

  for(i = o1; i <= o2; i++)
  {
    if(nsteps > 0)
    {
      tml = MapBase::safeCast(tMaps(i-1));
      l += tml->_length;
    }
    else
    {
      TP = TPB::safeCast(tMaps(i-1));
      l += TP->_length;
      th += TP->_theta;
    }
  }

  rho = l / th;
  rhon = rho / (1.0 + err);
  drho = 1000. * (rhon - rho);

  l = rhon * th;
  ln = l/nelts;
  thetan = th/nelts;

  for(i = o1; i <= o2; i++)
  {
    if(nsteps > 0)
    {
      order = i * 10;
      e1 = ea1;
      e2 = ea2;
      frI = fringeIN;
      frO = fringeOUT;
      if(i != o1)
      {
        e1 = 0.0;
        frI = 0;
      }
      if(i != o2)
      {
        e2 = 0.0;
        frO = 0;
      }
      TeaPot::replaceTPB("TPB", order, ln, et, tlt,
                         thetan, e1, e2,
                         nstps, frI, frO);
    }
    else
    {
      TP = TPB::safeCast(tMaps(i-1));
      TP->_length = ln;
      TP->_theta = thetan;
    }
  }

  addBendFieldI(name, order1, drho);
  addBendFieldF(name, order2, drho);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendFieldErrorSet
//
// DESCRIPTION
//    Adds a Bend Field Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    et:           Element type
//    tilt:         Element role
//    theta
//    ea1
//    ea2
//    nsteps
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendFieldErrorSet(const String &name,
                                 const String &dist,
                                 Vector(Real) &param,
                                 const String &et,
                                 const Real &tilt,
                                 const Real &theta,
                                 const Real &ea1,
                                 const Real &ea2,
                                 const Integer &nsteps,
                                 const Integer &fringeIN,
                                 const Integer &fringeOUT,
                                 const Integer &nTransMapMin,
                                 const Integer &nTransMapMax,
                                 const Integer &order)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  Integer j, js, norder, norder1, norder2, fl, i, jMin, jMax;
  String eletype;
  Real err;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));

  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          err = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          err = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = norder-10;
        norder1 = norder1-order;
        norder2 = norder2+order;
        addBendFieldError(name, norder1, norder2,
                          et, tilt, theta, ea1, ea2, err,
                          nsteps, fringeIN, fringeOUT);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          err = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          err = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = norder-10;
        norder1 = norder1-order;
        norder2 = norder2+order;
        addBendFieldError(name, norder1, norder2,
                          et, tilt, theta, ea1, ea2, err,
                          nsteps, fringeIN, fringeOUT);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addSolnFieldError
//
// DESCRIPTION
//    Adds a Soln Field Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First node order index
//    order2:  Second node order index
//    et
//    B
//    err
//    TPsubindex
//    nsteps
//    fringeIN
//    fringeOUT
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addSolnFieldError(const String &name,
                              const Integer &order1,
                              const Integer &order2,
                              const String &et,
                              const Real &B,
                              const Real &err,
                              const Integer &TPsubindex,
                              const Integer &nsteps)
{
  Integer o1, o2, nelts, i, order, nstps;
  Real Bs;
  TPS *TP;

  o1 = order1/10;
  o2 = order2/10;
  nelts = (o2 - o1) + 1;

  Bs = B * (1.0 + err);

  for(i = o1; i <= o2; i++)
  {
    if(nsteps > 0)
    {
      nstps = nsteps;
      order = i * 10;
      TeaPot::replaceTPS("TPQ", order, et, Bs,
                         TPsubindex, nstps);
    }
    else
    {
      TP = TPS::safeCast(tMaps(i-1));
      TP->_B = TP->_B * (1.0 + err);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addSolnFieldErrorSet
//
// DESCRIPTION
//    Adds a Soln Field Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    et:           Element type
//    B
//    TPsubindex
//    nsteps
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addSolnFieldErrorSet(const String &name,
                                 const String &dist,
                                 Vector(Real) &param,
                                 const String &et,
                                 const Real &B,
                                 const Integer &TPsubindex,
                                 const Integer &nsteps,
                                 const Integer &nTransMapMin,
                                 const Integer &nTransMapMax)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  Integer j, js, norder, norder1, norder2, fl, i, jMin, jMax;
  String eletype;
  Real err;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));

  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          err = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          err = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = norder-10;
        addSolnFieldError(name, norder1, norder2,
                          et, B, err,
                          TPsubindex, nsteps);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          err = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          err = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = norder-10;
        addSolnFieldError(name, norder1, norder2,
                          et, B, err,
                          TPsubindex, nsteps);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendDisplacementError
//
// DESCRIPTION
//    Adds a Dipole Displacement Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First node order index
//    order2:  Second node order index
//    angle
//    disp
//    type
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendDisplacementError(const String &name,
                                     const Integer &order1,
                                     const Integer &order2,
                                     const Real &angle,
                                     const Real &disp,
                                     const String &type)
{
  if(type == "X")
  {
    Error::addBendDisplacementXI(name, order1, angle, disp);
    Error::addBendDisplacementXF(name, order2, angle, disp);
  }

  if(type == "Y")
  {
    Error::addBendDisplacementYI(name, order1, disp);
    Error::addBendDisplacementYF(name, order2, disp);
  }

  if(type == "L")
  {
    Error::addBendDisplacementLI(name, order1, angle, disp);
    Error::addBendDisplacementLF(name, order2, angle, disp);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addBendDisplacementErrorSet
//
// DESCRIPTION
//    Adds a Dipole Displacement Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    et:           Element type
//    angle
//    type
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addBendDisplacementErrorSet(const String &name,
                                        const String &dist,
                                        Vector(Real) &param,
                                        const String &et,
                                        const Real &angle,
                                        const String &type,
                                        const Integer &nTransMapMin,
                                        const Integer &nTransMapMax,
                                        const Integer &order)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  Integer j, js, norder, norder1, norder2, fl, i, jMin, jMax;
  String eletype;
  Real disp;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));

  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          disp = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          disp = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder/10;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = (norder-10)/10;
        norder1 =(norder1*10)-order;
        norder2 =(norder2*10)+order;
        addBendDisplacementError(name,norder1,norder2,angle,disp,type);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          disp = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          disp = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder/10;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = (norder-10)/10;
        norder1 =(norder1*10)-order;
        norder2 =(norder2*10)+order;
        addBendDisplacementError(name,norder1,norder2,angle,disp,type);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addRotationError
//
// DESCRIPTION
//    Adds a Rotation Error
//
// PARAMETERS
//    name:    Name for this node
//    order1:  First node order index
//    order2:  Second node order index
//    angle
//    rhoi
//    theta
//    et
//    type
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addRotationError(const String &name,
                             const Integer &order1,
                             const Integer &order2,
                             const Real &angle,
                             const Real &rhoi,
                             const Real &theta,
                             const String &et,
                             const String &type)
{
  Integer o1, o2, i;
  Real l = 0.0;
  MapBase *tml;

  o1 = (order1/10) + 1;
  o2 = order2/10;

  for(i = o1; i <= o2; i++)
  {
    tml = MapBase::safeCast(tMaps(i-1));
    l += tml->_length;
  }

  addRotationI(name, order1, angle, rhoi, theta, l, et, type);
  addRotationF(name, order2, angle, rhoi, theta, l, et, type);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::addRotationErrorSet
//
// DESCRIPTION
//    Adds a Rotation Error to a set
//
// PARAMETERS
//    name:         Name for this node
//    dist
//    param:        Parameter vector
//    rhoi
//    theta
//    et:           Element type
//    type
//    nTransMapMin: Minimum of range for setting errors
//    nTransMapMax: Maximum of range for setting errors
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Error::addRotationErrorSet(const String &name,
                                const String &dist,
                                Vector(Real) &param,
                                const Real &rhoi,
                                const Real &theta,
                                const String &et,
                                const String &type,
                                const Integer &nTransMapMin,
                                const Integer &nTransMapMax,
                                const Integer &order)
{
  if(!nodesInitialized) initNodes();

  MapBase *tm;
  MapBase *tml;
  Integer j, js, norder, norder1, norder2, fl, i, jMin, jMax;
  String eletype;
  Real ang, l;

  jMin = 1;
  jMax = nTransMaps;
  if(jMin < nTransMapMin) jMin = nTransMapMin;
  js = jMin;
  if(nTransMapMax > 0)
  {
    if(jMax > nTransMapMax) jMax = nTransMapMax;
  }
  else
  {
    jMin = 1; 
  }
  
  fl = 1;
  drand(param(1));

  while(fl == 1)
  {
    tm = MapBase::safeCast(tMaps(js-1));
    eletype = tm->_et;

    if(et != eletype)
    {
      fl = 0;
    }
    js++;
  }

  for (j = js; j <= jMax; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          ang = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          ang = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder/10;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = (norder-10)/10;
        l = 0.0;
        for(i = norder1; i <= norder2; i++)
        {
          tml = MapBase::safeCast(tMaps(i-1));
          l += tml->_length;
        }
        norder1 =(norder1*10)-order;
        norder2 =(norder2*10)+order;
        addRotationError(name,norder1,norder2,ang,rhoi,theta,et,type);
        fl = 0;
      }
    }
  }

  for (j = jMin; j < js; j++)
  {
    tm = MapBase::safeCast(tMaps(j-1));
    eletype = tm->_et;
    norder = tm->_oindex;

    if(et == eletype)
    {
      if(fl == 0)
      {
        if(dist == "UNIFORM")
        {
          ang = param(2) + drand(0)*(param(3) - param(2));
        }
        if(dist == "GAUSS")
        {
          ang = getGauss(param(2), param(3), param(4));
        }
        norder1 = norder/10;
        fl = 1;
      }
    }
    else
    {
      if(fl == 1)
      {
        norder2 = (norder-10)/10;
        l = 0.0;
        for(i = norder1; i <= norder2; i++)
        {
          tml = MapBase::safeCast(tMaps(i-1));
          l += tml->_length;
        }
        norder1 =(norder1*10)-order;
        norder2 =(norder2*10)+order;
        addRotationError(name,norder1,norder2,ang,rhoi,theta,et,type);
        fl = 0;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Error::showErrors
//
// DESCRIPTION
//    Prints out the settings of the active errors
//
// PARAMETERS
//    sout - The stream to send output to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Error::showErrors(Ostream &sout)
{
  Integer i;
  ErrorBase *tlp;
  if (nErrNodes == 0)
  {
    sout << "\n No Errors are included \n\n";
    return;
  }

  sout << "\n\n Errors present in ring:\n\n";
  etfl = 0;
  rtfl = 0;
  sifl = 0;
  sffl = 0;
  yifl = 0;
  yffl = 0;
  ldfl = 0;
  fifl = 0;
  fffl = 0;
  dxifl = 0;
  dxffl = 0;
  dyifl = 0;
  dyffl = 0;
  dlifl = 0;
  dlffl = 0;
  bifl = 0;
  bffl = 0;
  rifl = 0;
  rffl = 0;
  fi2fl = 0;
  ff2fl = 0;

  for (i=1; i <= nErrNodes; i++)
  {
    tlp = ErrorBase::safeCast(ErrorPointers(i-1));
    tlp->_showError(sout);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Error::drand
//
// DESCRIPTION
//    Creates Random Number
//    
// PARAMETERS
//    r - seed value
//
// RETURNS
//    random
//
///////////////////////////////////////////////////////////////////////////

Real Error::drand (const Real &r)
{
   Integer a1 = 1536, a0 = 1029, a1ma0 = 507, c = 1731;
   static Integer x1 = 0, x0 = 0;
   Integer y1, y0, r_int, rand_int;
   Real x1_real, r_rem, random;

   if (r == 0.)
   {
      y0 = a0*x0;
      y1 = a1*x1 + a1ma0*(x0-x1) + y0;
      y0 = y0 + c;
      x0 = y0 - 2048*(y0/2048);
      y1 = y1 + (y0-x0)/2048;
      x1 = y1 - 2048*(y1/2048);
   }

   if (r > 0.)
   {
      r_int = Integer(r);
      r_rem = r - Real(r_int);
      x1_real = r_rem*4194304. + 0.5;
      x1 = Integer(x1_real);
      x0 = x1 - 2048*(x1/2048);
      x1 = (x1-x0)/2048;
   }

   rand_int = x1*2048 + x0;
   random = Real(rand_int)/4194304.;
   return random;
}

/////////////////////////////////////////////////////////////////////////
//
// NAME
//    Error::derf
//
// DESCRIPTION
//    This function returns the error function ERF(x) with fractional
//    error everywhere less than 1.2*10-7.
//    Adapted from the book "Numerical Recipes"
//
// PARAMETERS
//    x:  argument
//
// RETURNS
//    The value of the error function at x
//
//////////////////////////////////////////////////////////////////////////

Real Error::derf(const Real &x)
{
   Real zx, t, erfcc;
   if (x < 9.0)
   {
      zx = x;
      if (x < 0.) zx = -x;
      t = 1.0 / (1.0 + 0.5 * zx);

      erfcc = t * exp(-(zx*zx) - 1.26551223 +
         t * (1.00002368 + t * (0.37409196 + t * (0.09678418 +
         t * (-0.18628806 + t * (0.27886807 + t * (-1.13520398 +
         t * (1.48851587 + t * (-0.82215223 + t * 0.17087277)))))))));

      if (x < 0.0)
	 erfcc = 2.0 - erfcc;
   }
   else
   {
      erfcc = 0.0;
   }

   return (1.0 - erfcc);
}

/////////////////////////////////////////////////////////////////////////
//
// NAME
//    Error::root_normal
//
// DESCRIPTION
//    
// PARAMETERS
//    errtest
//    ymin
//    ymax
//    tol
//
// RETURNS
//    rtbis
//
//////////////////////////////////////////////////////////////////////////

Real Error::root_normal(const Real &errtest, const Real &ymin,
                        const Real &ymax, const Real &tol)
{
   Integer i,imax = 50;
   Real rtbis=0., dx, xmid, fmid;

   if ((derf(ymin)-errtest) < 0.)
   {
      rtbis = ymin;
      dx = ymax - ymin;
   }
   else
   {
      rtbis = ymax;
      dx = ymin - ymax;
   }
   if (dx < 0.) dx = -dx;
   for (i = 0; i < imax; i++)
   {
      dx = dx * 0.5;
      xmid = rtbis + dx;
      fmid = derf(xmid) - errtest;
      if (fmid <= 0.) rtbis = xmid;
      if (dx < tol || fmid == 0.)
      return rtbis;
   }
      return rtbis;
}

/////////////////////////////////////////////////////////////////////////
//
// NAME
//    Error::getGauss
//
// DESCRIPTION
//    Gaussian Distribution
//
// PARAMETERS
//    mean
//    sigma
//    cutoff
//
// RETURNS
//    gerr
//
//////////////////////////////////////////////////////////////////////////

Real Error::getGauss(const Real &mean, const Real &sigma,
                     const Real &cutoff)
{
    Real pmin, pmax, area, errtest;
    Real gmin, gmax, tol, gerr, root;
    tol = 1.0e-14;
    gmin = 0;
    gmax = 10;
    pmin = 0;
    pmax = 1;

    if (cutoff > 0.)
    {
       pmin = 0.5 - 0.5*derf((cutoff)/pow(2.,0.5));
       pmax = 0.5 + 0.5*derf((cutoff)/pow(2.,0.5));
    }

    area = pmin + (pmax-pmin)*drand(0);
    errtest = 2.*area - 1.0;
    if (errtest < 0.) errtest = -errtest;
    gmax = 10.0;
    while ((derf(gmax) - errtest) < 0.) gmax *=10.; 
    root=root_normal(errtest, gmin, gmax, tol);
    if (area >= 0.5)
    {
       gerr = mean + pow(2.,0.5)*sigma*root;
    }
    else
    {
       gerr = mean - pow(2.,0.5)*sigma*root;
    }
    return gerr;
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  drift_S
//
// DESCRIPTION
//  drifts a particle
//
// PARAMETERS
//  mp   = reference to the macro-particle herd
//  length = length of the drift
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void drift_S(MacroPart &mp, Integer &i, Real &length)
{
  Real x_init, y_init, phi_init;
  Real KNL, px, py, phifac;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);

  x_init = mp._x(i);
  y_init = mp._y(i);

  phi_init = mp._phi(i);
  KNL = 1.0 / (1.0 + mp._dp_p(i));
  px = mp._xp(i) / 1000.0;
  py = mp._yp(i) / 1000.0;

  mp._x(i) = x_init + KNL * length * mp._xp(i);
  mp._y(i) = y_init + KNL * length * mp._yp(i);

  phifac = (px * px +
            py * py +
            mp._dp_p(i) * mp._dp_p(i) * gamma2i
           ) / 2.0;
  phifac = (phifac * KNL - mp._dp_p(i) * gamma2i) * KNL;
  mp._phi(i) = phi_init + Factor * length * phifac;
}
