/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Injection.mod
//
// AUTHOR
//    John GalambosORNL, jdg@ornl.gov
//    Modified by Jeff Holmes and Steve Bunch: 2002, 02/2003
//
// CREATED
//    12/12/97
//
//  DESCRIPTION
//    Module descriptor file for the Injection module. This module
//    injects macropartciles using prescribed initializers. It also has
//    the facilities to model foil injection, scattering, and losses.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Injection - "Injection Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Collimator, Consts, MathLib, Particles, Ring;

  Errors
    badGTrapVals    - "Bas input for the trapezoidal phi dist",
    tooManyInitializers  - "Sorry, You tried to add too many initializers"
                         - " for X, Y or Longitudinal initializations. "
                         - "I can only handle 1 per coordinate now",
    tooManyFoils - "Opps - I can only handle 1 foil now",
    noFoils      - "Can't do this, there are no foils defined",
    noSyncPart   - "You must add a sync part first",
    noInjWithSimpleTune - "I can't presently inject parts with the simple"
                      -  "tune calculation on";

  public:

    Void ctor()
                  - "Constructor for the Particle module."
                  - "Initializes build values.";

  // Macro Particle Stuff:

  Integer
    injectParts         - "= 1 -> inject, = 0 -> don't inject",
    nMaxMacroParticles  - "Maximum number of macro particles to inject",
    injectTurnInterval  - "Injection turn interval to inject macros on",
    nMacrosPerTurn      - "Number of macros to inject on an injection turn",
    nInjectedMacros     - "Total number of injected macro particles including"
                        - "those that missed foil",
    nMacrosMissedFoil   - "Number of injected macro particles that miss foil",
    nFoils              - "Number of foil nodes in the Ring",
    saveFoilHits        - "= 1: write, = 0: don't write foil hit coords";

  Real
    nReals_Macro        - "Number of real particles per macro particle";

  Integer
    nFoilHits()         - "Total number of all macroparticle foil hits";

  // Foil Stuff:

  Real
    // Foil Input:
    dEdXFoil            - "Energy loss rate from beam -> foil [Mev/(g/cm2)]",
    sigmaNES            - "NES cross section in foil [barns]",
    foilZ               - "Charge number  of the foil",
    foilAMU             - "Foil atomic weight [AMU]",
    rhoFoil             - "Foil density [g/cm3]",
    muScatter           - "Foil scattering coefficient",
    radlength           - "Slowing down length",
    R                   - "Nuclear radius parameter",

//    CFoil               - "Coefficient for density effect",
//    aFoil               - "Coefficient for density effect",
//    bFoil               - "Coefficient for density effect",
//    X1Foil              - "Coefficient for density effect",
//    X0Foil              - "Coefficient for density effect",


    // Calculated Foil quantities:
    avgFoilHits         - "Avg. foil hits/particle",
    peakFoilTemp        - "Peak foil temperature [K]",
    fLossNES            - "Fraction of beam lost to NES",
    fH0States45         - "Fraction of the beam in H0 4&5 states",
    missFoilFrac        - "Fraction of injected particles missing foil",
    lScatter            - "Scattering length (mfp) in foil [cm]",
    nScatters           - "Average number of scatters/foil traversal",
    thetaScatMin        - "Minimum scattering angle [rad]",
    thickCm             - "foil thickness in cm";

  Integer
    useFoilScattering    - "Flag to activet the foil Coulomb scattering";

  Void miscFoilCalcs() - "Routine to calculatye misc. foil parameters";

  Void InjectParts(const Integer &np)
                       - "Routine to pick macro particles for injection";

  // General Linac Beam Stuff:

  Real
    x0Inj        - "Center of beam in x direction (mm)",
    xP0Inj       - "Center of beam in x-prime direction (mrad)",
    y0Inj        - "Center of beam in y direction (mm)",
    yP0Inj       - "Center of beam in y-prime direction (mrad)",

    dXInj        - "Delta X from x0Inj for an injected particle [mm]",
    dXPInj       - "Delta XP from xP0Inj for an injected particle [mrad]",

    dYInj        - "Delta Y from y0Inj for an injected particle [mm]",
    dYPInj       - "Delta YP from yP0Inj for an injected particle [mrad]",

    deltaE       - "Delta E of the injected macroparticle [MeV)",
    phi          - "Phi of the injected macroparticle [deg] ";

  Void addXInitializer(const String &n, Subroutine sub) 
         - "Adds a routine to do X coordinate initializations";

  Void addYInitializer(const String &n, Subroutine sub) 
         - "Adds a routine to do Y coordinate initializations";

  Void addLongInitializer(const String &n, Subroutine sub)
         - "Adds a routine to do longitudinal coordinate initializations";

  // Joho / Binomial injection distributions:

  Real
    MXJoho       - "Shape parameter to use in Joho X distribution",
    MYJoho       - "Shape parameter to use in Joho Y distribution",
    MLJoho       - "Shape parameter to use in Joho longitudinal distribution",
    xTailFraction- "Fraction of horizontal beam to treat as a tail",
    xTailFactor  - "Ratio of x tail emittance to core emittance",
    yTailFraction- "Fraction of vertical beam to treat as a tail",
    yTailFactor  - "Ratio of y tail emittance to core emittance",
    lTailFraction- "Fraction of longitudinal beam to treat as a tail",
    lTailFactor  - "Ratio of longitudinal tail emittance to core emittance",

    betaXInj     - "Injected beam beta-X value [m]",
    alphaXInj    - "Injected beam beta-X value [m]",
    epsXLimInj   - "Injected beam limiting ellipse X emittance [pi mm-mrad]",
    epsXRMSInj   - "Injected beam ellipse X RMS emittance [pi mm-mrad]",
    betaYInj     - "Injected beam beta-Y value [m]",
    alphaYInj    - "Injected beam beta-Y value [m]",
    epsYLimInj   - "Injected beam limiting ellipse Y emittance [pi mm-mrad]",
    epsYRMSInj   - "Injected beam ellipse Y RMS emittance [pi mm-mrad]",
    phiLimInj    - "Max. longitudinal phi extent of bunch (degrees)",
    dELimInj     - "Max. longitudinal delta E extent [GeV]",
    deltaPhiBunch- "Bunch separation (degrees) when nLongInjBunch > 0",
    deltaPhiNotch- "For notched beam, the full width of the notch (degrees)";



  Integer
    nLongInjBunch - "Number of discrete bunches to use for injection";

  Void JohoXDist() - "Routine to set injected X coordinates to a "
                   - "Joho type distribution";

  Void JohoYDist() - "Routine to set injected Y coordinates to a "
                   - "Joho type distribution";

  Void JohoLDist() - "Routine to set injected longitudinal "
                   - "coordinates to a Joho type distribution";



  //  Uniform Longitudinal Injection distributions

  Real
    deltaETruncate - "Fractional truncation energy of energy distribution",
    deltaEWidth    - "Fractional width parameter of energy distribution",
    deltaEFracInj  - "Fractional energy injected particle ",
    EOffset        - "Offset energy for uniform dist [GeV]",
    phiMinInj      - "Minimum injected phi (deg)",
    phiMaxInj      - "Maximum Injected phi (deg)";

  Void UniformLongDist()
           - "Routine to set up uniform longitudinal injected coordinates";

  // Uniform longitudinal injection with Gaussian Energy Dist.

  Void GULongDist() - "Routine to generate initial longitudinal coordinates"
                    - "With gaussian energy distributions and uniform phi";

  Void GUEJohoLDist() - "Routine which uses JohoLDist in the phi direction,"
		      - "and bi-gaussian distribution in energy";

  // SNS Energy Spreader Cavity Input Distribution

  Void SNSESpreadDist() - "Routine to generate initial "
                        - "longitudinal coordinates with gaussian "
                        - "energy distributions, modified by sinusoidal "
                        - "spreader, and random centroid jitter, "
                        - "and uniform phi";

  // Uniform longitudinal injection with Lorentz Energy Dist.

  Void LorentzLongDist() - "Generate initial longitudinal coordinates"
                  - "With Lorentz energy distributions and uniform phi";

  // Trapezoidal longitudinal injection with Gaussian Energy Dist.

  Void GTrapLongDist() - "Routine to generate initial longitudinal"
                       - " coordinates with gaussian energy "
                       - "distributions and trapezoidal phi";
  Real
    phiMinInj1    - "Absolute minimum injected phi (deg)",
    phiMinInj2    - "Minimum for flattop injected phi (deg)",
    phiMaxInj1    - "Maximum for flattop injected phi (deg)",
    phiMaxInj2   -  "Absolute maximum Injected phi (deg)";

// Trapezoidal longitudinal injection with uniform Energy Dist.

  Void UTrapLongDist() - "Routine to generate initial longitudinal coordinates"
                  - "With uniform energy distributions and trapezoidal phi";

  Void CosULongDist() - "Routine to generate initial longitudinal coordinates"
                  - "With a cos energy distributions and uniform phi";

  Real
   EInjMean       - "Mean of the Gaussian in GULongDist [GeV]",
   EInjSigma      - "Sigma of the Gaussian in GULongDist [GeV]",
   EInjSigma1     - "Sigma of first Gaussian in bi-Gaussian in GUEJohoLDist [GeV]",
   EInjSigma2     - "Sigma of second Gaussian in bi-Gaussian in GUEJohoLDist [GeV]",
   EFracSigma1    - "Fraction of beam inside first Gaussian",
   EFracSigma2    - "Fraction of beam inside second Gaussian",
   EInjMin        - "Min truncation of the Gaussian in GULongDist [GeV]",
   EInjMax        - "Max truncation of the Gaussian in GULongDist [GeV]";

  Real
   ECMean       - "Mean of Gaussian Centroid Jitter in SNSESpreadDist [GeV]",
   ECSigma      - "Sigma of Gaussian Centroid Jitter in SNSESpreadDist [GeV]",
   ECMin        - "Min of Gaussian Centroid Jitter in SNSESpreadDist [GeV]",
   ECMax        - "Max of Gaussian Centroid Jitter in SNSESpreadDist [GeV]",
   ECDriftI	- "Initial Drift Energy of Centroid [GeV]",
   ECDriftF	- "Final Drift Energy of Centroid [GeV]",
   DriftTime	- "Total Duration of Centroid Drift [ms]",
   ESNu         - "Frequency of Sinusoidal Energy Spreader [ms-1]",
   ESPhase      - "Constant phase of Sinusoidal Energy Spreader [Radians]",
   ESMax        - "Amplitude; of Sinusoidal Energy Spreader [GeV]",
   NullTime	- "Time for decay of spreader voltage [ms]";

  Integer
    randSeed	- "Seed for random numbers";

  Void addFoil(const String &name, const Integer &order,
               const Real &xmin,  const Real &xmax,
               const Real &ymin, const Real &ymax,
               const Real &thick)
                - "Add a Foil Node";

  // Misc stuff:

  Integer
    nXDistF      - "Number of X distribution function initializers",
    nYDistF      - "Number of Y distribution function initializers",
    nLDistF      - "Number of Longitdinal distribution function initializers";

  Integer
    useUniformLI - "Switch indicating use of uniform long. initializer",
    useJohoLI    - "Switch indicating use of Joho long. initializer",
    useGULI      - "Switch indicating use of gauss/uniform long. initializer",
    useSNSES     - "Switch indicating use of SNSES long. initializer";

  // Foil Hits Histogram:

  Integer
    nxfh         - "Number of horizontal foil hit grid spaces",
    nyfh         - "Number of vertical foil hit grid spaces",
    bunchindex   - "Coordinate index of bunch to be injected";

  Real
    xminfh       - "Minimum horizontal foil coordinate",
    xmaxfh       - "Maximum horizontal foil coordinate",
    yminfh       - "Minimum vertical foil coordinate",
    ymaxfh       - "Maximum vertical foil coordinate",
    dxfh         - "Horizontal foil hit grid size",
    dyfh         - "Vertical foil hit grid size";

  RealVector
    xfh          - "Horizontal foil hit grid centers",
    yfh          - "Vertical foil hit grid centers",
    xv           - "Horizontal   coordinates of bunch to be injected",
    xpv          - "Horizontal   momenta     of bunch to be injected",
    yv           - "Vertical     coordinates of bunch to be injected",
    ypv          - "Vertical     momenta     of bunch to be injected",
    phiv         - "Longitudinal coordinates of bunch to be injected",
    dEv          - "Energy       coordinates of bunch to be injected";

  RealMatrix
    fhp          - "Number of proton foil hits in each grid rectangle",
    fhpinj       - "Number of H- injection foil hits in each grid rectangle",
    fhpe         - "Number of proton + electron foil hits in each grid rectangle";

  Void dumpFoilHits(Ostream &os) - "Dumps Histogram of Foil Hits";
  Void dumpFoilHitsGlobal(Ostream &os) - "Dumps Global Histogram of Foil Hits";


  // Foil Hits Histgram and Tempareture rise

  Integer
    modelE       - "Model for the stopping power for electron 1->Classical Bethe-Bloch 2->ESTAR (for carbon graphite only)";

  Real           
    RepRate      - "Repetetion rate of injection [Hz]";


  Void dumpFoilHAndT(Ostream &os) - "Dumps Histogram of Foil Hits Tempareture distribution and evolution of tempareture on the hottest point.";


}
