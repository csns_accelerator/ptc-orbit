/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME//    Injection.cc
//
// AUTHOR
//    John Galambos ORNL, jdg@ornl.gov
//
// CREATED
//    10/2/97
//
//  DESCRIPTION
//    Source file for the Injection module. This module implements
//    the macro particle injection from prescibed distributions.
//    It also includes the Foil modeling.
//
//  REVISION HISTORY
//    Mar-Apr/2008
//    dumpFoilHAndT is added to estimate the temperature distribution and the max. temp.,
//    applicable to parallel processing.
//    by Masa. Aiba,  Masamitsu.Aiba@cern.ch
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Injection.h"
#include "Collimator.h"
#include "Node.h"
#include "MacroPart.h"
#include "Foil.h"
#include "Spline.h"
#include <cmath>
#include <cstdlib>
#include "StreamType.h"
#include <fstream>
#include <iostream>
#include <iomanip>

//parallel stuff
#include "mpi.h"

using namespace std;
///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

// Lists of synchronous particles and macro particles

static Array(ObjectPtr) xdistf, ydistf, ldistf;
extern  Array(ObjectPtr) foilPointers;

Array(ObjectPtr) foilPointers;
extern Array(ObjectPtr) syncP, mParts, nodes;

///////////////////////////////////////////////////////////////////////////
//
// Local Functions:
//
///////////////////////////////////////////////////////////////////////////

Real rootNorm(const Real &ymin, const Real &ymax, const Real &tol);
Real randnum;
Void initFoilHits();

//MPI temporary containers for coordinates
Vector(Real) xVec_temp_MPI;
Vector(Real) xpVec_temp_MPI;
Vector(Real) yVec_temp_MPI;
Vector(Real) ypVec_temp_MPI;
Vector(Real) dEVec_temp_MPI;
Vector(Real) phiVec_temp_MPI;

// some physical constants used here:

static Real  BohrRadius=0.52917706e-8;  // hydrogenic Bohr radius in cm
static Real  hBar = 1.0545887e-27;      // Planck's constant in erg-sec
static Real  eCharge = 4.803242e-10;    // in esu or statcoulombs
static Real  nAvogadro = 6.022045e23;
static Real  deg2Rad = 1.74532925e-2;

Integer yes = 1;
Integer no = 0;

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    DistFunct
//
// INHERITANCE RELATIONSHIPS
//    DistFunct -> Object -> IOSystem
//
// USING/CONTAINING RELATIONSHIPS
//    Object (U)
//
// DESCRIPTION
//    A class for storing pointers to a distribution function
//      which is used to pick initial macroParticle coordinates.
//
// PUBLIC MEMBERS
//    DistFunct:       Constructor for making DistFunct objects
//   ~DistFunct:       Destructor for the DistFunct class.
//    _sub             Pointer to the routine to call to do  initializations
//    _showInt         
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class DistFunct : public Object {
  Declare_Standard_Members(DistFunct, Object);
public:
    DistFunct(const String &n, Subroutine sub)
             : _name(n), _sub(sub)  { }
    
  ~DistFunct();

  String _name;
  virtual Void callInitializer();
  virtual Void nameOut(String &wname) { wname = _name; }

  Subroutine _sub;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS DistFunct
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(DistFunct, Object);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    DistFunct::callInitializer()
//
// DESCRIPTION
//    Calls the specified  initializer
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
//////////////////////////////////////////////////////////////////////////

Void DistFunct::callInitializer()
{
  _sub();
}
DistFunct::~DistFunct()
{}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS Foil
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(Foil, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Foil::NodeCalculator
//
// DESCRIPTION
//    Calls the injection routine to see if particles should be added.
//    Sets up foil scattering information (useFoilScattering != 0).
//    Repeated single scattering model from ACCSIM (ASCAT)
//      (useFoilScattering = 1).
//    Uses physics from collimation module
//      (useFoilScattering = 2):
//      ionization energy loss
//      small angle coulomb scattering
//      Rutherford scattering
//      nuclear elastic and inelastic scattering
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Foil::_nodeCalculator(MacroPart &mp)
{
  if((mp._herdName == "mainHerd") && Injection::injectParts != 0)
    Injection::InjectParts(Injection::nMacrosPerTurn);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Foil::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//      a MacroParticle by the Foil.
//    Checks to see if the macro intersects the foil.
//    useFoilScattering = 1:
//      Repeated single scattering model from ACCSIM (ASCAT)
//    useFoilScattering = 2:
//      Uses physics from collimation module:
//        ionization energy loss
//        small angle coulomb scattering
//        Rutherford scattering
//        nuclear elastic and inelastic scattering
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Foil::_updatePartAtNode(MacroPart& mp)
{
  Integer j;
  Real zrl;
  Real pInj0, pv, sigmaCoul, term, TFRadius, thetaScatMax;
  Real theta, thetaX, thetaY, random1, phi;
  Integer idum, lastArg, trackit;
  Real eCross, iCross, rCross, e_frac, i_frac, r_frac, b_pN;
  Real totCross, random, choice, dlength, meanFreePath;
  Real rl, stepsize, stepmax, radlengthfac;
  Real p, E, beta, dE, M_nuc, M;
  Real theta_norm, delta_pos, delta_mom;
  Real t, dp_x = 0., dp_y = 0, pfac, xpfac, ypfac, directionfac;
  Real thx, thy;
  Spline ElValue(crossEn, crossEl);
  Spline InelValue(crossEn, crossInel);
  Spline RuthValue(crossEn, crossRuth);

  // Thickness in cm

  Injection::thickCm = _thick / (1.e6 * Injection::rhoFoil);
  dlength = 0.001 * Injection::thickCm;

  // Seed random number generator
  idum = -(unsigned)time(0);

  if(Injection::useFoilScattering == 1)
  {
    // Momentum in g*cm/sec

    pInj0 = 1.6726e-22 * mp._syncPart._mass * mp._syncPart._betaSync *
            mp._syncPart._gammaSync * Consts::vLight;

    // Thomas-Fermi atom radius (cm):

    TFRadius = Injection::muScatter *  BohrRadius *
               Pow(Injection::foilZ, -0.33333);

    // Minimum scattering angle:

    Injection::thetaScatMin =  hBar / (pInj0 * TFRadius);

    // Theta max as per Jackson (13.102)

    thetaScatMax = 274.e5 * Consts::eMass * Consts::vLight /
                   (pInj0 * Pow(Injection::foilAMU, 0.33333));

    pv = 1.e2 * pInj0 * mp._syncPart._betaSync * Consts::vLight;
    term = Injection::foilZ * Sqr(eCharge) / pv;
    sigmaCoul = Consts::fourPi * Sqr(term) / Sqr(Injection::thetaScatMin);

    // Scattering area per area

    Injection::nScatters = nAvogadro * Injection::rhoFoil /
                           Injection::foilAMU *
                           Injection::thickCm * sigmaCoul;

    // Mean free path

    Injection::lScatter = Injection::thickCm/Injection::nScatters;
  }

  if(Injection::useFoilScattering == 2)
  {
    M = mp._syncPart._e0;
    M_nuc = Injection::foilAMU * 0.931494;
    E = mp._syncPart._eTotal;
    radlengthfac = Injection::radlength * 10.;
    b_pN = 14.5 * pow(Injection::foilAMU, 0.6666667);
  }

  j = 1;
  while(j <= mp._nMacros)
  {
    if((mp._x(j) >= _xMin) && (mp._x(j) <= _xMax) &&
       (mp._y(j) >= _yMin) && (mp._y(j) <= _yMax))
    {
      Integer ifh, jfh;
      ifh = Integer((mp._x(j) - Injection::xminfh) /
                    Injection::dxfh) + 1;
      jfh = Integer((mp._y(j) - Injection::yminfh) /
                    Injection::dyfh) + 1;
      if(ifh < 1) ifh = 1;
      if(ifh > Injection::nxfh) ifh = Injection::nxfh;
      if(jfh < 1) jfh = 1;
      if(jfh > Injection::nyfh) jfh = Injection::nyfh;
        Injection::fhp(ifh,jfh) = Injection::fhp(ifh,jfh) + 1.0;
        Injection::fhpe(ifh,jfh) = Injection::fhpe(ifh,jfh) + 1.0;
      if(mp._foilHits(j) < 1)
      {
        Injection::fhpinj(ifh,jfh) = Injection::fhpinj(ifh,jfh) + 1.0;
        Injection::fhpe(ifh,jfh) = Injection::fhpe(ifh,jfh) + 2.0;
      }

      mp._foilHits(j)++;

      if(Injection::saveFoilHits != 0)
      {
        OFstream fio1("FoilHitsInject", ios::app);
        OFstream fio2("FoilHitsAll", ios::app);
        fio2 << mp._nTurnsDone  << "   "
             << j               << "   "
             << mp._foilHits(j) << "   "
             << mp._x(j)        << "   "
             << mp._xp(j)       << "   "
             << mp._y(j)        << "   "
             << mp._yp(j)       << "   "
             << mp._phi(j)      << "   "
             << mp._deltaE(j)       << "\n";
        if(mp._foilHits(j) < 2)
	{
          fio1 << mp._nTurnsDone  << "   "
               << j               << "   "
               << mp._foilHits(j) << "   "
               << mp._x(j)        << "   "
               << mp._xp(j)       << "   "
               << mp._y(j)        << "   "
               << mp._yp(j)       << "   "
               << mp._phi(j)      << "   "
               << mp._deltaE(j)       << "\n";
        }
        fio1.close();
        fio2.close();
    }

      // scattering:

      if(Injection::useFoilScattering == 1)
      {
        zrl = Injection::thickCm;  // distance remaining in foil //
        thetaX = 0.;
        thetaY = 0.;

	// Generate interaction points until particle exits foil

        while (zrl >= 0.0)
        {
          random1 = Collimator::ran1(idum);
          zrl += Injection::lScatter * log(random1);
          if(zrl < 0.0) break; // exit foil

	  // Generate random angles

          random1 = Collimator::ran1(idum);
          phi = Consts::twoPi * random1;
          random1 = Collimator::ran1(idum);
          theta = Injection::thetaScatMin * Sqrt(random1 / (1. - random1));
          thetaX += theta * cos(phi);
          thetaY += theta * sin(phi);
        }
           mp._xp(j) += 1000. * thetaX;
           mp._yp(j) += 1000. * thetaY;
      }

      if(Injection::useFoilScattering == 2)
      {
        zrl = 10.0 * Injection::thickCm;
        E = mp._syncPart._eTotal + mp._deltaE(j);
        p = pow((E * E - M * M), 0.5);
        beta = p / E;
        pfac = 1.0 + mp._dp_p(j);
        xpfac = mp._xp(j) / (1000.* pfac);
        ypfac = mp._yp(j) / (1000.* pfac);
        directionfac = sqrt(1.0 + xpfac * xpfac + ypfac * ypfac);
        rl = zrl * directionfac;

        theta = 0.0136 / (beta * p)
                / sqrt(radlengthfac);
        eCross = ElValue(E - M);
        iCross = InelValue(E - M);
	/*
	//        theta_norm = theta * sqrt(radlengthfac);
	//        lastArg = 0;
	//        lastArg = 2;
	//        rCross = Collimator::getRuth_t(theta_norm, E, p,
	//                                       Injection::R,
	//                                       Injection::foilZ,
	//                                       idum, lastArg);
	rCross = RuthValue(E - M);
	*/
        stepsize = rl;
        trackit = 0;
        Collimator::getRuthJackson(stepsize,
                                   Injection::foilZ,
                                   Injection::foilAMU,
                                   Injection::rhoFoil,
		                   idum, beta, trackit,
                                   rCross, thx, thy);
        totCross = eCross + iCross + rCross;

        while(zrl > 0)
        {
          rl = zrl * directionfac;
          meanFreePath = (Injection::foilAMU / nAvogadro /
                         (Injection::rhoFoil * 1.0e+06) /
                         (totCross * 1.0e-28)) * 1000.;
          stepmax = -meanFreePath * log(Collimator::ran1(idum));
          stepsize = stepmax;
          if(stepmax > rl) stepsize = rl + dlength;

          // MCS
	  /*
          Collimator::MCS(stepsize, theta, idum, mp._xp(j),
                          delta_pos, delta_mom);
          mp._x(j)  += delta_pos;
          mp._xp(j) += delta_mom * pfac;
          Collimator::MCS(stepsize, theta, idum, mp._yp(j),
                          delta_pos, delta_mom);
          mp._y(j)  += delta_pos;
          mp._yp(j) += delta_mom * pfac;
	  */
          Collimator::MCSJackson(stepsize,
                                 Injection::foilZ,
                                 Injection::foilAMU,
                                 Injection::rhoFoil,
                                 idum, beta, pfac,
                                 mp._x(j), mp._y(j),
                                 mp._xp(j), mp._yp(j));

          //Ionization energy loss
          dE = Collimator::IonEnergyLoss(beta,
                                         Injection::foilZ,
                                         Injection::foilAMU);
          dE = -dE * Injection::rhoFoil / 10000. * stepsize;
          mp._deltaE(j) += dE;
          mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

          if(((mp._deltaE(j) + mp._syncPart._eKinetic) < 0.02) &&
             (!TransMap::tuneCalcOn))
          {
            // Remove particle below 20 MeV

            mp._addLostMacro(j, _position);
            j--;
            zrl = -1.;
          }
          else if(stepmax > rl)
          {
            // MCS out, no nuclear interaction

            zrl = -1.;
          }
          else
          {
            //Nuclear interact

            E = mp._syncPart._eTotal + mp._deltaE(j);
            p = pow((E * E - M * M), 0.5);
            beta = p / E;
            theta = 0.0136 / (beta * p)
                    / sqrt(radlengthfac);
            pfac = 1.0 + mp._dp_p(j);

            eCross = ElValue(E - M);
            iCross = InelValue(E - M);
	    /*
	    //            theta_norm = theta * sqrt(radlengthfac);
            //            lastArg = 0;
	    //            lastArg = 2;
	    //            rCross = Collimator::getRuth_t(theta_norm, E, p,
	    //                                           Injection::R,
	    //                                           Injection::foilZ,
	    //                                           idum, lastArg);
	    rCross = RuthValue(E - M);
	    */
            trackit = 0;
            Collimator::getRuthJackson(stepsize,
                                       Injection::foilZ,
                                       Injection::foilAMU,
                                       Injection::rhoFoil,
	                               idum, beta, trackit,
                                       rCross, thx, thy);
            totCross = eCross + iCross + rCross;
            e_frac = eCross/totCross;
            i_frac = iCross/totCross;
            r_frac = rCross/totCross;
	    choice = Collimator::ran1(idum);

            // Nuclear Elastic Scattering

            if((choice >= 0.) && (choice <= e_frac))
            {
              if((E - M) <= 0.4)
              {
                t=Collimator::getElast_t(p, Injection::foilAMU, idum);
              }
              if((E - M) > 0.4)
              {
                t=-log(Collimator::ran1(idum))/b_pN;
              }
              Collimator::getKick(t, p, M_nuc, dp_x, dp_y, idum);
              mp._xp(j) += dp_x * pfac;
              mp._yp(j) += dp_y * pfac;
            }

            // Rutherford Coulomb scattering

            if((choice > e_frac) && (choice <= (1 - i_frac)))
            {
	      /*
              theta_norm = theta * sqrt(radlengthfac);
              lastArg = 1;
	      //              lastArg = 3;
              t = Collimator::getRuth_t(theta_norm, E, p,
                                        Injection::R,
                                        Injection::foilZ,
                                        idum, lastArg);
              Collimator::getKick(t, p, M_nuc, dp_x, dp_y, idum);
              mp._xp(j) += dp_x * pfac;
              mp._yp(j) += dp_y * pfac;
	      */
              trackit = 1;
              Collimator::getRuthJackson(stepsize,
                                         Injection::foilZ,
                                         Injection::foilAMU,
                                         Injection::rhoFoil,
	                                 idum, beta, trackit,
                                         rCross, thx, thy);

              xpfac = mp._xp(j) / (1000.* pfac);
              ypfac = mp._yp(j) / (1000.* pfac);

              Real anglex = atan(xpfac) + thx;
              Real angley = atan(ypfac) + thy;

              mp._xp(j) = tan(anglex) * (1000.* pfac);
              mp._yp(j) = tan(angley) * (1000.* pfac);
            }

            xpfac = mp._xp(j) / (1000. * pfac);
            ypfac = mp._yp(j) / (1000. * pfac);
            directionfac = sqrt(1.0 + xpfac * xpfac + ypfac * ypfac);
            zrl -= stepsize / directionfac;

            // Nuclear Inelastic absorption

            if((choice > (1.-i_frac)) && (choice <= 1.)
               && !TransMap::tuneCalcOn)
            {
              mp._addLostMacro(j, _position);
              j--;
              zrl = -1.;
            }
          }
        }
      }
    }
    j++;
  }
}


///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE Injection
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection::ctor
//
// DESCRIPTION
//    Initializes the various constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::ctor()
{

// set some initial values

  injectParts = 1;
  injectTurnInterval = 1;
  nMacrosPerTurn = 1;
  saveFoilHits = 0;
  x0Inj = 0.;
  xP0Inj = 0.;
  y0Inj = 0.;
  yP0Inj = 0.;

  nXDistF = 0;
  nYDistF = 0;
  nLDistF = 0;

        // Linac injected beam:
    MXJoho = 3.;
    alphaXInj = 0.;     betaXInj = 10.;
    epsXLimInj = 0.5;   epsXRMSInj = 0.;
    xTailFraction = 0.; xTailFactor  = 1.;

    MYJoho = 3.;
    alphaYInj = 0.;      betaYInj = 10.;
    epsYLimInj = 0.5;    epsYRMSInj = 0.;
    yTailFraction = 0.;  yTailFactor  = 1.;

    MLJoho = 1.;
    lTailFraction = 0.;  lTailFactor  = 1.;
    phiLimInj = 120.;    dELimInj = 0.001;
    nLongInjBunch = 0;   deltaPhiBunch = 0.;
    deltaPhiNotch=0.;

    // Unifor Long. Dist:
         
    phiMinInj = -100.; phiMaxInj = 100.;
    deltaEFracInj = 0.01; EOffset = 0.;

    // for trapezoidal phi injection
    phiMinInj1 = -110.; phiMaxInj1 = 100.; 
    phiMinInj2 = -100.; phiMaxInj2 = 110.;

        // Stuff for Gaussian Linac Energy:
    EInjMean = 1000.;
    EInjSigma = 2.;
    EInjMin = -10.; // No Truncation
    EInjMax = -10.; // No Truncation

    useUniformLI = 0;
    useGULI = 0;
    useSNSES = 0;
    useJohoLI = 0;
    

    randSeed = 1;
  // Initialize the random number generator.
   Real test = MathLib::ran1(randSeed);

   nMacrosMissedFoil = 0;
   nInjectedMacros = 0;

       // Foil stuff:
   dEdXFoil    = 2.0;  // MeV/(g/cm2)
   nFoils = 0;
   sigmaNES = 0.37;  // Barns
   foilZ = 6;
   foilAMU = 12.011;
   rhoFoil = 2.265;   // density, g/cm3
   useFoilScattering = 0;
   muScatter = 1.35;
   radlength = 18.8;
   R = 0.94 * Pow(foilAMU, 1./3.);

   // For dumpFoilHAndT
   modelE = 1 ;
   RepRate = 1.0 ;
   //   CFoil = -3.22 ; // stuffs for density effect in electron ionization energy loss
   //   aFoil = 0.531 ; //  Reference, INTERACTIONS OF PHOTONS AND LEPTONS WITH MATTER, Roy and Reed, Academic Press (1968) 
   //   bFoil = 2.63 ;  //             R. M. Sternheimer, Phys. Rev. 103, p511 (1956)
   //   X1Foil = 2.0 ; // default values for Graphite
   //   X0Foil= -0.05 ; // 

   bunchindex = 0;
   xv.resize(1);
   xpv.resize(1);
   yv.resize(1);
   ypv.resize(1);
   phiv.resize(1);
   dEv.resize(1);

   xVec_temp_MPI.resize(1);
   xpVec_temp_MPI.resize(1);
   yVec_temp_MPI.resize(1);
   ypVec_temp_MPI.resize(1);
   dEVec_temp_MPI.resize(1);
   phiVec_temp_MPI.resize(1);

   Integer n;
   Real fn;
   nxfh = 10;
   nyfh = 10;
   xminfh = -100.;
   xmaxfh = 100.;
   yminfh = -100.;
   ymaxfh = 100.;
   dxfh = (xmaxfh - xminfh) / nxfh;
   dyfh = (ymaxfh - yminfh) / nyfh;
   xfh.resize(nxfh);
   for(n = 1; n <= nxfh; n++)
   {
     fn = Real(n) - 0.5;
     xfh(n) = xminfh + fn * dxfh;
   }
   yfh.resize(nyfh);
   for(n = 1; n <= nyfh; n++)
   {
     fn = Real(n) - 0.5;
     yfh(n) = yminfh + fn * dyfh;
   }
   fhp.resize(nxfh,nyfh);
   fhpinj.resize(nxfh,nyfh);
   fhpe.resize(nxfh,nyfh);
   fhp = 0.0;
   fhpinj = 0.0;
   fhpe = 0.0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Injection::addXInitializer
//
// DESCRIPTION
//    Adds a function to use to initialize X coordinates of MacroParticles.
//
// PARAMETERS
//    name:           A name for the initializer
//    sub:            The routine to call to do the X initializations.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Injection::addXInitializer(const String &n, Subroutine sub) 
{
    if (nXDistF > 0) except(tooManyInitializers);
    
    xdistf.resize(++nXDistF);
  
    xdistf(nXDistF-1) = new DistFunct(n, sub);

}

Void Injection::addYInitializer(const String &n, Subroutine sub) 
{
    if (nYDistF > 0) except(tooManyInitializers);
    
    ydistf.resize(++nYDistF);
  
    ydistf(nYDistF-1) = new DistFunct(n, sub);

}
Void Injection::addLongInitializer(const String &n, Subroutine sub)
{
  if (nLDistF > 0) except(tooManyInitializers);

  ldistf.resize(++nLDistF);

  ldistf(nLDistF-1) = new DistFunct(n, sub);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Injection::JohoXDist
//
// DESCRIPTION
//    Samples from a Joho (binomial) type distribution for X injected coodiates
//       epsXLimInj is the limiting emittance to sample from.
//       MXJoho is the "binomial" exponent of the distribution.
//               = 0 for hollow shell in phase space
//               = 0.5 for flat profile in real space
//               = 1   for uniform in phase space
//               = 1.5 for elliptical in phase space
//               = 2 for parabolic in phase space
//               = 3 for truncated Gaussian in phase space
//               = infinite for gaussian in phase space
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Injection::JohoXDist()
{
    static Integer first=true;
    static Real  epsx, cosChiX, gammax, mxi, sinChiX, xm, xpm, xl, xpl;
    Real  s1, s2, a, al, u, v, dx, dxp;

    if(first)
    {
      epsx = epsXLimInj * 2./(1. + MXJoho);  // 2 sigma epsilon
        xm = Sqrt(epsx);
        gammax = (1 + Sqr(alphaXInj))/betaXInj;
        xpm = Sqrt(epsx * gammax);
        cosChiX = Sqrt(1./(1. + Sqr(alphaXInj)) );
        sinChiX = -alphaXInj * cosChiX;
//        chiX = atan(sinChiX/cosChiX);
        xl = sqrt(epsXLimInj * betaXInj);
        xpl = sqrt(epsXLimInj * gammax);
        mxi = 1./MXJoho;
        epsXRMSInj = 0.5 * epsXLimInj/(1. + MXJoho);
        first = false;        
    }

    s1 = ran1(randSeed);
    s2 = ran1(randSeed);
    a = Sqrt(1 - Pow(s1, mxi));
    al = 2. * pi * s2;
    u = a * cos(al);
    v = a * sin(al);
    dXInj = xl*u;
    dXPInj = xpl * (u*sinChiX + v*cosChiX);
    if(xTailFraction > 0.)
      {
        if( ran1(randSeed) < xTailFraction)
	  {  
            dXInj *= xTailFactor;
            dXPInj *= xTailFactor;
	  }
      }

}
    
Void Injection::JohoYDist()
{
    static Integer first=true;
    static Real  epsy, cosChiY, gammay, myi, sinChiY, ym, ypm, yl, ypl;
    Real  s1, s2, a, al, u, v, dy, dyp;

    if(first)
    {
        epsy = epsYLimInj * 2./(1. + MYJoho);
        ym = Sqrt(epsy);
        gammay = (1 + Sqr(alphaYInj))/betaYInj;
        ypm = Sqrt(epsy * gammay);
        cosChiY = Sqrt(1./(1. + Sqr(alphaYInj)) );
        sinChiY = -alphaYInj * cosChiY;
//        chiY = atan(sinChiY/cosChiY);
        yl = sqrt(epsYLimInj * betaYInj);
        ypl = sqrt(epsYLimInj * gammay);
        myi = 1./MYJoho;
        epsYRMSInj = 0.5 * epsYLimInj/(1. + MYJoho);
        first = false;
        
    }

    s1 = ran1(randSeed);
    s2 = ran1(randSeed);
    a = Sqrt(1 - Pow(s1, myi));
    al = 2. * pi * s2;
    u = a * cos(al);
    v = a * sin(al);
    dYInj = yl*u;
    dYPInj = ypl * (u*sinChiY + v*cosChiY);

    if(yTailFraction > 0.)
      {
        if( ran1(randSeed) < yTailFraction)
	  {  
            dYInj *= yTailFactor;
            dYPInj *= yTailFactor;
	  }
      }
}

Void Injection::JohoLDist()
{
  Real  MLInv, s1, s2, a, al, u, v;
  Real  phiInj, dEInj, offset, phiTemp;
  Integer iBunch;

  MLInv = 1./MLJoho;
  useJohoLI = 1;

  s1 = ran1(randSeed);
  s2 = ran1(randSeed);
  a = Sqrt(1 - Pow(s1, MLInv));
  al = 2. * pi * s2;
  u = a * cos(al);
  v = a * sin(al);
  phiInj = phiLimInj*u;
  dEInj = dELimInj * v;

  if(lTailFraction > 0.)
  {
    if( ran1(randSeed) < lTailFraction)
    {
      phiInj *= lTailFactor;
      dEInj *= lTailFactor;
    }
  }

  if(nLongInjBunch > 1)
  {
    iBunch = 1 + nLongInjBunch * ran1(randSeed);
    iBunch = Min(iBunch, nLongInjBunch);
    offset = Real(2 * iBunch - nLongInjBunch - 1)/2.;
    phiTemp = offset * deltaPhiBunch;
    if(deltaPhiNotch != 0.)
    {
      while(phiTemp < deltaPhiNotch/2. &&
            phiTemp > -deltaPhiNotch/2.)
      {
        iBunch = 1  + nLongInjBunch * ran1(randSeed);
        iBunch = Min(iBunch, nLongInjBunch);
        offset = Real(2 * iBunch - nLongInjBunch - 1)/2.;
        phiTemp = offset * deltaPhiBunch;
      }
    }
    phiInj += phiTemp;
  }

  phi = deg2Rad * phiInj;
  deltaE = dEInj;
}




///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Injection::UniformLongDist
//
// DESCRIPTION
//    Samples from a uniform phi distribution and a uniform Energy spread.
//      - phiMinInj (phiMaxInj) is the min (max) injection angle (degrees)
//      - EOffset is the energy offset of injected beam relative to the
//           synchrounous particle.
//      - deltaEFracInj is the fractional energy 1/2 spread of the beam.
//        i.e. deltaE = EOffset + ESync *(+- deltaEFracInj)
//                 
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Injection::UniformLongDist()
{
    useUniformLI = 1;
    
    phi = deg2Rad * (
        phiMinInj + (phiMaxInj - phiMinInj)*ran1(randSeed) );
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));
    
    deltaE = EOffset +
        sp->_eKinetic * -deltaEFracInj * (1. - 2.*ran1(randSeed));


}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Injection::LorentzLongDist()
//
// DESCRIPTION
//    Samples from a uniform phi distribution and a truncated Lorentz
//        energy distribution.
//      - phiMinInj (phiMaxInj) is the min (max) injection angle (degrees)
//      - EOffset is the energy offset of injected beam relative to the
//           synchrounous particle.
//      - deltaEWidth is the fractional width parameter of energy
//           distribution.
//      - deltaETruncate is the fractional truncation energy of energy
//           distribution.
//        i.e. deltaE = EOffset + ESync *(+- deltaEFracInj)
//                 
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Injection::LorentzLongDist()
{
    Real inv_tan, deltaERatio, angle;
    useUniformLI = 1;
    
    phi = deg2Rad * (
        phiMinInj + (phiMaxInj - phiMinInj)*ran1(randSeed) );
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

    inv_tan = pi / 2.;
    deltaERatio = deltaETruncate / deltaEWidth;
    if (deltaERatio < 10000.) inv_tan = atan(deltaERatio);
    angle = (2.*ran1(randSeed) - 1.) * inv_tan;
    deltaE = EOffset + sp->_eKinetic * deltaEWidth * tan(angle);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection:::GTrapLongDist()
//
// DESCRIPTION
//    Function to generate random initial longitudinal coordinates
//    for a trapezoidal distribution in phi and a gaussian distribution in dE.
//
//   Specify phiMinInj1,  phiMinInj2, phiMaxInj1 and phiMaxInj2
//        in degrees for the phi initialization.
//
//   Specify the EInjMean, EInjSigma, EInjMin and EInjMax [Gev] for the
//     energy initializations. The Min and Max energies are used for 
//     truncation (set = 0 for no truncation).
//
// PARAMETERS
//    None
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::GTrapLongDist()
{
    static Integer first=true;
    static Real area1, area2, area3, areaTot, frac1, frac2;
    Real random1, random2;

    if(first)
      {
        useGULI = 0;
        area1 = 0.5 * (phiMinInj2 - phiMinInj1);
        area2 = (phiMaxInj1 - phiMinInj2);
        area3 = 0.5 * (phiMaxInj2 - phiMaxInj1);
        if(area1 < 0. || area2 < 0. || area3 < 0.) except(badGTrapVals);
        areaTot = area1 + area2 + area3;
        frac1 = area1/areaTot; frac2 = (area1 + area2)/areaTot;
        first = 0;
      }

    random1 = ran1(randSeed);

    if(random1 <= frac1)
      {
        random2 = random1 / frac1;
        phi = deg2Rad * (phiMinInj1 + 
                         Sqrt(random2)*(phiMinInj2 - phiMinInj1) );
      }

    if(random1 > frac1 && random1 <= frac2)
	   {
             random2 = (random1-frac1) * areaTot / area2;
             phi = deg2Rad * (phiMinInj2 + random2 *(phiMaxInj1 - phiMinInj2));
	   }
    if(random1 > frac2)
	   {
              random2 = (random1-frac2) * areaTot/ area3;
              phi = deg2Rad * (phiMaxInj2  - 
                         Sqrt(random2) * (phiMaxInj2- phiMaxInj1) );

	   }

//    if (nSyncP < 1) except(noSyncPart);
    
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));
     
  //   Generate a deltaE from a gaussian:

  Real tol = 1.0e-6, ymin = 0.0, ymax = 10.0, pmin = 0.0, pmax = 1.0; 
  Real root, EInj;

  //   If EInjMin < 0, then do regular Gaussian distribution.
  //   If EInjMin >= 0, then this will indicate that a truncated Gaussian
  //   distribution is desired.  Instead of the tails of the normal
  //   curve going to + or - infinity, the tails will be cut off at
  //   EInjMin and EInjMax.
  //   (pmin and pmax are between [0,1])


  if (EInjMin >= 0.)
    {
      if(EInjMin >= EInjMean)
        pmin = 0.5 + 0.5*MathLib::erf((EInjMin-EInjMean)/(sqrt(2.)* EInjSigma));
      else
        pmin = 0.5 - 0.5*MathLib::erf((EInjMean-EInjMin)/(sqrt(2.)* EInjSigma));
    }
  
  if (EInjMax >= 0.)    { 
      if(EInjMax >= EInjMean)
        pmax = 0.5 + 0.5*MathLib::erf((EInjMax-EInjMean)/(sqrt(2.)* EInjSigma));
      else
        pmax = 0.5 - 0.5*MathLib::erf((EInjMean-EInjMax)/(sqrt(2.)* EInjSigma));
    }  
  
  randnum = pmin + (pmax-pmin) * ran1(randSeed);

  
  while ((MathLib::erf(ymax) - Abs(2.* randnum - 1.) ) < 0.)
    {
      ymax *= 10.;
    }

  // Call rootNorm. (like "zeroin", rootNorm finds the roots)

  root=rootNorm(ymin,ymax,tol);

  if (randnum >= 0.5)
    EInj = (sqrt(2.) * EInjSigma*root) + EInjMean;
  else
    EInj = EInjMean - (sqrt(2.) * EInjSigma*root);
  
  deltaE = (EInj - sp->_eKinetic);
  
  
}
///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection:::GTrapLongDist()
//
// DESCRIPTION
//    Function to generate random initial longitudinal coordinates
//    for a trapezoidal distribution in phi and a uniform distribution in dE.
//
//   Specify phiMinInj1,  phiMinInj2, phiMaxInj1 and phiMaxInj2
//        in degrees for the phi initialization.
//
//   Specify 
//        EOffset = offset from sync energy for source center (GeV)
//        deltaEFracInj=  +- fraction of the sync part kinetic energy
//                      
//
// PARAMETERS
//    None
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::UTrapLongDist()
{
    static Integer first=true;
    static Real area1, area2, area3, areaTot, frac1, frac2;
    Real random1, random2;

    if(first)
      {
        useGULI = 0;
        area1 = 0.5 * (phiMinInj2 - phiMinInj1);
        area2 = (phiMaxInj1 - phiMinInj2);
        area3 = 0.5 * (phiMaxInj2 - phiMaxInj1);
        if(area1 < 0. || area2 < 0. || area3 < 0.) except(badGTrapVals);
        areaTot = area1 + area2 + area3;
        frac1 = area1/areaTot; frac2 = (area1 + area2)/areaTot;
        first = 0;
      }

    random1 = ran1(randSeed);

    if(random1 <= frac1)
      {
        random2 = random1 / frac1;
        phi = deg2Rad * (phiMinInj1 + 
                         Sqrt(random2)*(phiMinInj2 - phiMinInj1) );
      }

    if(random1 > frac1 && random1 <= frac2)
	   {
             random2 = (random1-frac1) * areaTot / area2;
             phi = deg2Rad * (phiMinInj2 + random2 *(phiMaxInj1 - phiMinInj2));
	   }
    if(random1 > frac2)
	   {
              random2 = (random1-frac2) * areaTot/ area3;
              phi = deg2Rad * (phiMaxInj2  - 
                         Sqrt(random2) * (phiMaxInj2- phiMaxInj1) );

	   }
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

    deltaE = EOffset +
        sp->_eKinetic * -deltaEFracInj * (1. - 2.*ran1(randSeed));
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection:::GULongDist()
//
// DESCRIPTION
//    Function to generate random initial longitudinal coordinates
//    for a uniform distribution in phi and a gaussian distribution in dE.
//
//   Specify phiMinInj and phiMaxInj in degrees for the phi initialization.
//   Specify the EInjMean, EInjSigma, EInjMin and EInjMax [Gev] for the
//     energy initializations. The Min and Max energies are used for
//     truncation (set = 0 for no truncation).
//
// PARAMETERS
//    None
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::GULongDist()
{
  useGULI = 1;

  phi = deg2Rad * (phiMinInj +
                  (phiMaxInj - phiMinInj) * ran1(randSeed));

  SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

  //   Generate a deltaE from a gaussian:

  Real tol = 1.0e-6, ymin = 0.0, ymax = 10.0, pmin = 0.0, pmax = 1.0;
  Real root, EInj;

  //   If EInjMin < 0, then do regular Gaussian distribution.
  //   If EInjMin >= 0, then this will indicate that a truncated Gaussian
  //   distribution is desired.  Instead of the tails of the normal
  //   curve going to + or - infinity, the tails will be cut off at
  //   EInjMin and EInjMax.
  //   (pmin and pmax are between [0,1])

  if (EInjMin >= 0.)
  {
    if(EInjMin >= EInjMean)
      pmin = 0.5 + 0.5 * MathLib::erf((EInjMin - EInjMean) /
             (sqrt(2.) * EInjSigma));
    else
      pmin = 0.5 - 0.5 * MathLib::erf((EInjMean - EInjMin) /
             (sqrt(2.) * EInjSigma));

    if(EInjMax >= EInjMean)
      pmax = 0.5 + 0.5 * MathLib::erf((EInjMax - EInjMean) /
             (sqrt(2.) * EInjSigma));
    else
      pmax = 0.5 - 0.5 * MathLib::erf((EInjMean - EInjMax) /
             (sqrt(2.) * EInjSigma));
  }

  randnum = pmin + (pmax - pmin) * ran1(randSeed);

  while ((MathLib::erf(ymax) - Abs(2. * randnum - 1.)) < 0.)
  {
    ymax *= 10.;
  }

  // Call rootNorm. (like "zeroin", rootNorm finds the roots)

  root = rootNorm(ymin, ymax, tol);

  if (randnum >= 0.5)
    EInj = EInjMean + (sqrt(2.) * EInjSigma * root);
  else
    EInj = EInjMean - (sqrt(2.) * EInjSigma * root);

  deltaE = (EInj - sp->_eKinetic);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection:::GUEJohoLDist()
//
// DESCRIPTION
//    Function to generate minibunches with prescribed spacing in phi,
//    and a bi-gaussian distribution in dE.
//
//   Specify phiLimInj, nLongInjBunch, and deltaPhiBunch in degrees for 
//   the phi initialization.
//   Specify the EInjMean, EInjSigma, EInjMin and EInjMax [Gev] for the
//     energy initializations. The Min and Max energies are used for
//     truncation (set = 0 for no truncation).
//
// PARAMETERS
//    None
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::GUEJohoLDist()
{  
  //This part copied from JohoLDist()
  Real  MLInv, s1, s2, a, al, u, v;
  Real  phiInj, dEInj, offset, phiTemp;
  Integer iBunch;

  MLInv = 1./MLJoho;
  useJohoLI = 1;

  s1 = ran1(randSeed);
  s2 = ran1(randSeed);
  a = Sqrt(1 - Pow(s1, MLInv));
  al = 2. * pi * s2;
  u = a * cos(al);
  v = a * sin(al);
  phiInj = phiLimInj*u;
  dEInj = dELimInj * v;

  if(lTailFraction > 0.)
  {
    if( ran1(randSeed) < lTailFraction)
    {
      phiInj *= lTailFactor;
      dEInj *= lTailFactor;
    }
  }

  if(nLongInjBunch > 1)
  {
    iBunch = 1 + nLongInjBunch * ran1(randSeed);
    iBunch = Min(iBunch, nLongInjBunch);
    offset = Real(2 * iBunch - nLongInjBunch - 1)/2.;
    phiTemp = offset * deltaPhiBunch;
    if(deltaPhiNotch != 0.)
    {
      while(phiTemp < deltaPhiNotch/2. &&
            phiTemp > -deltaPhiNotch/2.)
      {
        iBunch = 1  + nLongInjBunch * ran1(randSeed);
        iBunch = Min(iBunch, nLongInjBunch);
        offset = Real(2 * iBunch - nLongInjBunch - 1)/2.;
        phiTemp = offset * deltaPhiBunch;
      }
    }
    phiInj += phiTemp;
  }

  phi = deg2Rad * phiInj;

  //This part adapted from GULongDist.

  SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

  //   Generate a deltaE from a gaussian:
  
  Real tol = 1.0e-6, ymin = 0.0, ymax = 10.0, pmin = 0.0, pmax = 1.0;
  Real root, EInj;

  //   If EInjMin < 0, then do regular Gaussian distribution.
  //   If EInjMin >= 0, then this will indicate that a truncated Gaussian
  //   distribution is desired.  Instead of the tails of the normal
  //   curve going to + or - infinity, the tails will be cut off at
  //   EInjMin and EInjMax.
  //   (pmin and pmax are between [0,1])

  
  if(EInjSigma1 > EInjSigma2){
    EInjSigma = EInjSigma1;
  }
  else{ 
    EInjSigma = EInjSigma2;
  }

  if (EInjMin >= 0.)
  {
    if(EInjMin >= EInjMean)
      pmin = 0.5 + 0.5 * MathLib::erf((EInjMin - EInjMean) /
             (sqrt(2.) * EInjSigma));
    else
      pmin = 0.5 - 0.5 * MathLib::erf((EInjMean - EInjMin) /
             (sqrt(2.) * EInjSigma));

    if(EInjMax >= EInjMean)
      pmax = 0.5 + 0.5 * MathLib::erf((EInjMax - EInjMean) /
             (sqrt(2.) * EInjSigma));
    else
      pmax = 0.5 - 0.5 * MathLib::erf((EInjMean - EInjMax) /
             (sqrt(2.) * EInjSigma));
  }
 
  randnum = pmin + (pmax - pmin) * ran1(randSeed);

  while ((MathLib::erf(ymax) - Abs(2. * randnum - 1.)) < 0.)
  {
    ymax *= 10.;
  }
 
  // Call rootNorm. (like "zeroin", rootNorm finds the roots)

  root = rootNorm(ymin, ymax, tol);
 
  //Choose which of the gaussians in the bi-gaussian dist. to put the particle in.

  if( ran1(randSeed) < EFracSigma1){
    EInjSigma = EInjSigma1;
  }
  else{
    EInjSigma = EInjSigma2;
  }

  if (randnum >= 0.5)
    EInj = EInjMean + (sqrt(2.) * EInjSigma * root);
  else
    EInj = EInjMean - (sqrt(2.) * EInjSigma * root);
  
  deltaE = (EInj - sp->_eKinetic);
  
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection:::SNSESpreadDist()
//
// DESCRIPTION
//    Function to generate random initial longitudinal coordinates
//    for a uniform distribution in phi and a gaussian distribution in dE,
//    and then spread the energies sinusoidally in time with an
//    additional random component for centroid jitter.
//
//   Specify phiMinInj and phiMaxInj in degrees for the phi initialization.
//   Specify the EInjMean, EInjSigma, EInjMin and EInjMax [Gev] for the
//     energy initializations. The Min and Max energies are used for
//     truncation (set = 0 for no truncation).
//   For the centroid jitter, specify
//     ECMean, ECSigma, ECMin and ECMax [Gev].
//   For the sinusoidal spread, specify
//     frequency ESNu and amplitude ESMax.
//
// PARAMETERS
//    None
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::SNSESpreadDist()
{
  useSNSES = 1;

  if(ran1(randSeed) > lTailFraction * 360. /
                      (360. - phiMaxInj + phiMinInj))
  {
    phi = deg2Rad * (phiMinInj +
                    (phiMaxInj - phiMinInj) * ran1(randSeed) );
  }
  else
  {
    phi = -Consts::pi + Consts::twoPi * ran1(randSeed);
  }

  SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

  //   Generate a linac energy distribution from a gaussian:

  Real tol = 1.0e-6, ymin = 0.0, ymax = 10.0, pmin = 0.0, pmax = 1.0;
  Real root, EInj, EC, ES;

  //   If EInjMin < 0, then do regular Gaussian distribution.
  //   If EInjMin >= 0, then this will indicate that a truncated Gaussian
  //   distribution is desired.  Instead of the tails of the normal
  //   curve going to + or - infinity, the tails will be cut off at
  //   EInjMin and EInjMax.
  //   (pmin and pmax are between [0,1])

  pmin = 0.0;
  pmax = 1.0;

  if (EInjMin >= 0.)
  {
    if(EInjMin >= EInjMean)
      pmin = 0.5 + 0.5 * MathLib::erf((EInjMin - EInjMean) /
             (sqrt(2.) * EInjSigma));
    else
      pmin = 0.5 - 0.5 * MathLib::erf((EInjMean - EInjMin) /
             (sqrt(2.)* EInjSigma));

    if(EInjMax >= EInjMean)
      pmax = 0.5 + 0.5 * MathLib::erf((EInjMax - EInjMean) /
             (sqrt(2.)* EInjSigma));
    else
      pmax = 0.5 - 0.5 * MathLib::erf((EInjMean - EInjMax) /
             (sqrt(2.)* EInjSigma));
  }

  randnum = pmin + (pmax - pmin) * ran1(randSeed);

  while ((MathLib::erf(ymax) - Abs(2.* randnum - 1.) ) < 0.)
  {
    ymax *= 10.;
  }

  // Call rootNorm. (like "zeroin", rootNorm finds the roots)

  root=rootNorm(ymin, ymax, tol);

  if (randnum >= 0.5)
    EInj = EInjMean + (sqrt(2.) * EInjSigma * root);
  else
    EInj = EInjMean - (sqrt(2.) * EInjSigma * root);

  //   If ECMin < -1000000.0, then do regular Gaussian distribution.
  //   If ECMin >= -1000000.0, then this will indicate that a
  //   truncated Gaussian distribution is desired.
  //   Instead of the tails of the normal curve going to
  //   + or - infinity, the tails will be cut off at
  //   ECMin and ECMax.
  //   (pmin and pmax are between [0,1])

  pmin = 0.0;
  pmax = 1.0;

  Real ECDrift = ECDriftI + (ECDriftF - ECDriftI) * Ring::time / DriftTime;

  if (ECMin >= -1000000.0)
  {
    if(ECMin >= ECMean)
      pmin = 0.5 + 0.5 * MathLib::erf((ECMin - ECMean) /
             (sqrt(2.)* ECSigma));
    else
      pmin = 0.5 - 0.5 * MathLib::erf((ECMean - ECMin) /
             (sqrt(2.)* ECSigma));

    if(ECMax >= ECMean)
      pmax = 0.5 + 0.5 * MathLib::erf((ECMax - ECMean) /
             (sqrt(2.)* ECSigma));
    else
      pmax = 0.5 - 0.5 * MathLib::erf((ECMean - ECMax) /
             (sqrt(2.)* ECSigma));
  }

  randnum = pmin + (pmax - pmin) * ran1(randSeed);

  while ((MathLib::erf(ymax) - Abs(2.* randnum - 1.)) < 0.)
  {
    ymax *= 10.;
  }

  // Call rootNorm. (like "zeroin", rootNorm finds the roots)

  root=rootNorm(ymin, ymax, tol);

  if (randnum >= 0.5)
    EC = ECMean + ECDrift + (sqrt(2.) * ECSigma * root);
  else
    EC = ECMean + ECDrift - (sqrt(2.) * ECSigma * root);

  Integer IPhase = Integer(ESNu * Ring::time);
  Real PhaseC = 2.0 * Consts::pi * (ESNu * Ring::time - IPhase);
  Real TurnTime = 1000. * Ring::lRing/(sp->_betaSync * vLight);
  Real PhasePhiFac = ESNu * TurnTime;
  Real PhasePhi = PhasePhiFac * phi;
  Real Phase = PhaseC + PhasePhi + ESPhase;
  ES = ESMax * sin(Phase);
  Real tfac = 1.0;
  if (NullTime > 0.)
  {
    if (Ring::time >= NullTime)
    {
      tfac = 0.0;
    }
    else
    {
      tfac = pow(1.0 - Ring::time / NullTime, 0.5);
    }
  }
  ES *= tfac;

  deltaE = (EInj + EC + ES - sp->_eKinetic);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection::CosULongDist()
//
// DESCRIPTION
//    Function to generate random initial longitudinal coordinates
//    for a uniform distribution in phi and a cosine distribution in dE.
//
//   Specify phiMinInj and phiMaxInj in degrees for the phi initialization.
//   Specify the deltaEMax [Gev] for the energy initializations.
//   Specify the EOffset energy offset from synchronous particle.
//
// PARAMETERS
//    None
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::CosULongDist()
{
  useGULI = 1;

  phi = deg2Rad * (phiMinInj + (phiMaxInj - phiMinInj) * ran1(randSeed));
  SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

  Real ang = asin(2. * ran1(randSeed) - 1);

  deltaE = EOffset + 2. * sp->_eKinetic * deltaEFracInj * ang / Consts::pi;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    rootNorm
//
// DESCRIPTION
//    Function to return a root
//
// PARAMETERS
//   ymin  : minimum y on interval [ymin,ymax]
//   ymax  : maximum y on interval [ymin,ymax]
//   tol   : tolerance for solution
//
// RETURNS
//   rtbis : the root 
//
///////////////////////////////////////////////////////////////////////////

Real rootNorm(const Real &ymin, const Real &ymax, const Real &tol)
{
  // It is assumed  that f(ymin) and f(ymax) have opposite signs
  // without a  check.  rootNorm  returns a zero  x  in the given interval
  // ax, bx  to within a tolerance  4*macheps*abs(x) + tol, where macheps
  // is the relative machine precision. 
  // This function was modelled from the Fortran version of RTBIS in
  // Numerical Recipes, page 247, where rtbis equals the root.

   Integer i,imax = 40;
   Real rtbis=0.,dx,xmid,fmid;
   
   if ( (MathLib::erf(ymin) - Abs(2.* randnum - 1.)) < 0.)
     {
       rtbis = ymin;
       dx = ymax - ymin;   
     }
   else
     {
       rtbis=ymax;
       dx = ymin - ymax;
     }
   for (i = 1; i <= imax; i++)
     {
       dx = dx * 0.5;
       xmid = rtbis + dx;
       fmid = MathLib::erf(xmid) - Abs(2.*randnum-1.);
       if (fmid <= 0.) rtbis = xmid;
       if (Abs(dx) < tol || fmid == 0.) 
	 return rtbis;
     }
	 return rtbis;   
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Injection::InjectParts
//
// DESCRIPTION
//   Pick macroparticles to inject into the mainHerd
//
// PARAMETERS
//   np = Number of macro particles to create from the active
//        X,Y, and longitudinal initializers.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Injection::InjectParts(const Integer &np)
{
  Integer i;
  Real x = 0., xp = 0., y = 0., yp = 0.;

  int nMacroParticles_global = nMacroParticles;
 
  // Parallel stuff   =====MPI====  start
  // Non-parallel by default

  int nRank = 0;
  int numprocs = 1;
  int i_flag;
  MPI_Initialized(&i_flag);
  if(i_flag)
  {
    MPI_Comm_rank(MPI_COMM_WORLD,&nRank);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  }

  if((injectParts == 2) && (bunchindex == 0))
  {
    IFstream fio1("LinacDist", ios::in);
    xv.resize(nMaxMacroParticles);
    xpv.resize(nMaxMacroParticles);
    yv.resize(nMaxMacroParticles);
    ypv.resize(nMaxMacroParticles);
    phiv.resize(nMaxMacroParticles);
    dEv.resize(nMaxMacroParticles);
    for(i = 1; i <= nMaxMacroParticles; i++)
    {
      fio1 >> xv(i) >> xpv(i) >> yv(i) >> ypv(i) >> phiv(i) >> dEv(i);
    }
    fio1.close();
  }

  if(i_flag)
  {
    MPI_Allreduce(&nMacroParticles, &nMacroParticles_global,
                  1, MPI_INT,MPI_SUM, MPI_COMM_WORLD);           
  }

  // Parallel stuff   =====MPI====  stop

  if(nMacroParticles_global >= nMaxMacroParticles) return;

  if((nTurnsDone % injectTurnInterval) != 0) return;

  // Parallel stuff   =====MPI====

  xVec_temp_MPI.resize(np);
  xpVec_temp_MPI.resize(np);
  yVec_temp_MPI.resize(np);
  ypVec_temp_MPI.resize(np);
  dEVec_temp_MPI.resize(np);
  phiVec_temp_MPI.resize(np);

  DistFunct *xdf;
  DistFunct *ydf;    
  DistFunct *ldf;

  int n_injected = 0;

  if(nRank == 0 )
  {
    if(injectParts != 2)
    {
      for (i = 1; i <= np; i++)
      {
        deltaE = 0.;  // Default Longitudinal stuff to 0.
        phi = 0.;

        // initialize the Long. stuff first, since dispersion effects need this
  
        if(nLDistF) 
        {
          ldf = DistFunct::safeCast(ldistf(nLDistF - 1));
          ldf -> callInitializer();      
        }

        if(nXDistF)
        {
          xdf = DistFunct::safeCast(xdistf(nXDistF - 1));
          xdf -> callInitializer();
          x = x0Inj +  dXInj;
          xp = xP0Inj + dXPInj;
        }

        if(nYDistF)
        {
          ydf = DistFunct::safeCast(ydistf(nYDistF - 1));
          ydf -> callInitializer();
          y = y0Inj +  dYInj;
          yp = yP0Inj + dYPInj;
        }

        if(nFoils > 0)
        {
          // check for foil/particle intersection

          Foil *foil = Foil::safeCast(foilPointers(0));
          if(x >= foil -> _xMin && x <= foil -> _xMax  &&
             y >= foil -> _yMin && y <= foil->_yMax)
          {
            n_injected++;
            xVec_temp_MPI(n_injected) = x;
            xpVec_temp_MPI(n_injected) = xp;
            yVec_temp_MPI(n_injected) = y;
            ypVec_temp_MPI(n_injected) = yp;
            dEVec_temp_MPI(n_injected) = deltaE;
            phiVec_temp_MPI(n_injected) = phi;
          }
          else 
          {
            nMacrosMissedFoil++;
            // Could add these to file for later reference.
          }
        }
        else
        {
          n_injected++;
          xVec_temp_MPI(n_injected) = x;
          xpVec_temp_MPI(n_injected) = xp;
          yVec_temp_MPI(n_injected) = y;
          ypVec_temp_MPI(n_injected) = yp;
          dEVec_temp_MPI(n_injected) = deltaE;
          phiVec_temp_MPI(n_injected) = phi;
        }
      }
    }
    else
    {
      for (i = 1; i <= np; i++)
      {
        bunchindex++;
        if(bunchindex <= nMaxMacroParticles)
	{
          x      = xv(bunchindex)  + x0Inj;
          xp     = xpv(bunchindex) + xP0Inj;
          y      = yv(bunchindex)  + y0Inj;
          yp     = ypv(bunchindex) + yP0Inj;
          phi    = phiv(bunchindex);
          deltaE = dEv(bunchindex);
	}

        if(nFoils > 0)
        {
          // check for foil/particle intersection

          Foil *foil = Foil::safeCast(foilPointers(0));
          if(x >= foil -> _xMin && x <= foil -> _xMax  &&
             y >= foil -> _yMin && y <= foil->_yMax)
          {
            n_injected++;
            xVec_temp_MPI(n_injected) = x;
            xpVec_temp_MPI(n_injected) = xp;
            yVec_temp_MPI(n_injected) = y;
            ypVec_temp_MPI(n_injected) = yp;
            dEVec_temp_MPI(n_injected) = deltaE;
            phiVec_temp_MPI(n_injected) = phi;
          }
          else 
          {
            nMacrosMissedFoil++;
            // Could add these to file for later reference.
          }
        }
        else
        {
          n_injected++;
          xVec_temp_MPI(n_injected) = x;
          xpVec_temp_MPI(n_injected) = xp;
          yVec_temp_MPI(n_injected) = y;
          ypVec_temp_MPI(n_injected) = yp;
          dEVec_temp_MPI(n_injected) = deltaE;
          phiVec_temp_MPI(n_injected) = phi;
        }
      }
    }
  }

  if(i_flag)
  {
    MPI_Bcast((int *)    &n_injected, 1, MPI_INT, 0, MPI_COMM_WORLD); 
    MPI_Bcast((double *) &xVec_temp_MPI.data(),   n_injected,
              MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast((double *) &xpVec_temp_MPI.data(),  n_injected,
              MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast((double *) &yVec_temp_MPI.data(),   n_injected,
              MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast((double *) &ypVec_temp_MPI.data(),  n_injected,
              MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast((double *) &dEVec_temp_MPI.data(),  n_injected,
              MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast((double *) &phiVec_temp_MPI.data(), n_injected,
              MPI_DOUBLE, 0, MPI_COMM_WORLD);
  }

  int n_remainder = n_injected % numprocs;
  int n_inj_local = n_injected/numprocs;

  //inject the equal number of particles on each CPU

  int i_start = nRank*n_inj_local + 1;
  int i_stop =  (nRank+1)*n_inj_local;

  for( i=i_start ; i<= i_stop ; i++)
  {
    addMacroPart2(xVec_temp_MPI(i),
    xpVec_temp_MPI(i),
    yVec_temp_MPI(i),
    ypVec_temp_MPI(i), 
    dEVec_temp_MPI(i), 
    phiVec_temp_MPI(i));
  }

  int n_max_index = numprocs*n_inj_local;

  for(i = 0; i < n_remainder; i++)
  {
    int i_cpu = (int)( numprocs * (rand() / (1.0 + RAND_MAX)));
    //int i_cpu = i;
    MPI_Bcast(&i_cpu, 1 , MPI_INT,0,MPI_COMM_WORLD);
    if(nRank == i_cpu)
    {
      addMacroPart2(xVec_temp_MPI(i + 1 + n_max_index),
      xpVec_temp_MPI(i + 1 + n_max_index),
      yVec_temp_MPI(i + 1 + n_max_index),
      ypVec_temp_MPI(i + 1 + n_max_index), 
      dEVec_temp_MPI(i + 1 + n_max_index), 
      phiVec_temp_MPI(i + 1 + n_max_index));	
      n_inj_local++;
    }
  }

  nInjectedMacros += n_inj_local;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Injection::addFoil
//
// DESCRIPTION
//    Adds a transfer matrix object
//
// PARAMETERS
//    name:    Name for the TM 
//    order:   Order index
//    xmin    Minimum foil x (mm)
//    xmax    Maximum foil x (mm)
//    ymin    Minimum foil y (mm)
//    ymax    Maximum foil y (mm)
//    thick   Foil Thickness (u-g/cm)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Injection::addFoil(const String &name, const Integer &order,
  const Real &xmin,  const Real &xmax,  const Real &ymin,  const Real &ymax,
  const Real &thick)

{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + 1);
  if(nFoils == foilPointers.size())
      foilPointers.resize(nFoils + 1);
  
   nNodes++;
  
   nodes(nNodes-1) = new Foil(name, order, xmin, xmax, ymin, ymax,
                              thick);
   nFoils ++;
   if (nFoils > 1) except(tooManyFoils);

   
   foilPointers(nFoils -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

   xminfh = xmin;
   xmaxfh = xmax;
   yminfh = ymin;
   ymaxfh = ymax;
   initFoilHits();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection::nFoilHits
//
// DESCRIPTION
//    Finds the total number of hits
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Integer  Injection::nFoilHits()
{
    Integer k;
    MacroPart *mp;
    Real sum = 0;

    mp = MacroPart::safeCast(mParts((mainHerd & All_Mask)-1));

    for (k=1; k <= mp->_nMacros; k++)
        sum += mp->_foilHits(k);

    int i_sum = (int) sum;

    return i_sum;
    
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Injection::miscFoilCalcs
//
// DESCRIPTION
//    Find misc. foil info
//
// PARAMETERS
//    None
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Injection::miscFoilCalcs()
{

//    if(nFoils ==0 ) except(noFoils);
    if(nFoils == 0 ) return;

     //non-parallel by default
     int nRank=0;
     int numprocs=1;  
     int i_flag;
     MPI_Initialized(&i_flag);
     if(i_flag){
       MPI_Comm_rank(MPI_COMM_WORLD,&nRank);
       MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
     }
     int nMacroParticles_global = nMacroParticles;
     int nInjectedMacros_global = nInjectedMacros;
     int nMacrosMissedFoil_global = nMacrosMissedFoil;
     int nFoilHits_local = nFoilHits();
     int nFoilHits_global = nFoilHits_local;
     if(i_flag){
       MPI_Allreduce ( &nMacroParticles,&nMacroParticles_global,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
       MPI_Allreduce ( &nInjectedMacros,&nInjectedMacros_global,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
       MPI_Allreduce ( &nMacrosMissedFoil,&nMacrosMissedFoil_global,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD); 
       MPI_Allreduce ( &nFoilHits_local,&nFoilHits_global,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD);
     }
        

    Real beamArea, currentEff;
    Real sigma0 = 8.e5;  // cross section from -1 -> 0 (barns)
    Real sigma0p = 3.5e5;// cross section from 0 -> 1 (barns)
    
    Foil *foil = Foil::safeCast(foilPointers(0));

    avgFoilHits = Real(nFoilHits_global)/Real(nMacroParticles_global);
    missFoilFrac = Real(nMacrosMissedFoil_global)/Real(nInjectedMacros_global);

    // calculate the peak foil temperature:

    beamArea = pi*sqrt(betaXInj*epsXRMSInj)
                 * sqrt(betaYInj*epsYRMSInj);    // RMS area
    currentEff = nReals_Macro*Real(nMacroParticles_global)/(1.e-3 * time); //parts/sec
        // normalized to BNL calcs spring 98:
    peakFoilTemp = Pow(
        (2.24e-6* dEdXFoil  * currentEff * foil->_thick/beamArea), 0.25);

    Real thick2 = 0.6025e-6 * foil->_thick/Injection::foilAMU;
        // Assume 2.59% of H0 in 4 & 5 states:
    fH0States45 = 0.0259 *
        (sigma0 * exp(-sigma0p * thick2) - sigma0p*exp(-sigma0*thick2))/
        (sigma0 - sigma0p);
    fLossNES = thick2 * sigmaNES * avgFoilHits;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection::dumpFoilHits
//
// DESCRIPTION
//    Dumps Histogram of Foil Hits
//
// PARAMETERS
//    Output Stream
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::dumpFoilHits(Ostream &os)
{
  Integer i, j;
  Real gridSquareArea = dxfh * dyfh;
  Real fhptot = 0.0;
  Real fhpinjtot = 0.0;
  Real fhpetot = 0.0;
  Matrix(Real) fhploc, fhpinjloc, fhpeloc;

  fhploc.resize(nxfh,nyfh);
  fhpinjloc.resize(nxfh,nyfh);
  fhpeloc.resize(nxfh,nyfh);

  for(i = 1; i <= nxfh; i++)
  {
    for(j = 1; j <= nyfh; j++)
    {
      fhptot = fhptot + fhp(i,j);
      fhpinjtot = fhpinjtot + fhpinj(i,j);
      fhpetot = fhpetot + fhpe(i,j);
    }
  }
  //  fhptot = fhptot * nReals_Macro / gridSquareArea;  // Aiba do not understand "/ gridSquareArea".
  //  fhpinjtot = fhpinjtot * nReals_Macro / gridSquareArea; // If they are "total", the denominator should be removed.
  //  fhpetot = fhpetot * nReals_Macro / gridSquareArea;
    fhptot = fhptot * nReals_Macro ;
    fhpinjtot = fhpinjtot * nReals_Macro ;
    fhpetot = fhpetot * nReals_Macro ;

  os << "xminfh = " << xminfh << "\n"
     << "xmaxfh = " << xmaxfh << "\n"
     << "yminfh = " << yminfh << "\n"
     << "ymaxfh = " << ymaxfh << "\n"
     << "nxfh = " << nxfh << "\n"
     << "nyfh = " << nyfh << "\n"
     << "dxfh = " << dxfh << "\n"
     << "dyfh = " << dyfh << "\n"
     << "gridSquareArea = " << gridSquareArea << "\n"
     << "nReals_Macro = " << nReals_Macro << "\n"
     << "fhptot = " << fhptot << "\n"
     << "fhpinjtot = " << fhpinjtot << "\n"
     << "fhpetot = " << fhpetot << "\n\n\n";

  for(i = 1; i <= nxfh; i++)
  {
    for(j = 1; j <= nyfh; j++)
    {
      fhploc(i,j) = fhp(i,j) * nReals_Macro / gridSquareArea;
      fhpinjloc(i,j) = fhpinj(i,j) * nReals_Macro / gridSquareArea;
      fhpeloc(i,j) = fhpe(i,j) * nReals_Macro / gridSquareArea;

      os << i       << "   " << j        << "   "
         << xfh(i)  << "   " << yfh(j)   << "   "
         << fhploc(i,j) << "   " << fhpinjloc(i,j) << "   "
         << fhpeloc(i,j) << "\n";
    }
    os << "\n";
  }
}





///////////////////////////////////////////////////////////////////////////
//
// NAME
//    initFoilHits
//
// DESCRIPTION
//    Initializes Histogram of Foil Hits
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void initFoilHits()
{
  Integer n;
  Real fn;
  Injection::dxfh = (Injection::xmaxfh - Injection::xminfh) /
                    Injection::nxfh;
  Injection::dyfh = (Injection::ymaxfh - Injection::yminfh) /
                    Injection::nyfh;
  Injection::xfh.resize(Injection::nxfh);
  for(n = 1; n <= Injection::nxfh; n++)
  {
    fn = Real(n) - 0.5;
    Injection::xfh(n) = Injection::xminfh + fn * Injection::dxfh;
  }
  Injection::yfh.resize(Injection::nyfh);
  for(n = 1; n <= Injection::nyfh; n++)
  {
    fn = Real(n) - 0.5;
    Injection::yfh(n) = Injection::yminfh + fn * Injection::dyfh;
  }
  Injection::fhp.resize(Injection::nxfh,Injection::nyfh);
  Injection::fhpinj.resize(Injection::nxfh,Injection::nyfh);
  Injection::fhpe.resize(Injection::nxfh,Injection::nyfh);
  Injection::fhp = 0.0;
  Injection::fhpinj = 0.0;
  Injection::fhpe = 0.0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection::dumpFoilHitsGlobal
//
// DESCRIPTION
//    Dumps Histogram of Foil Hits
//
// PARAMETERS
//    Output Stream
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::dumpFoilHitsGlobal(Ostream &os)
{
  Integer i, j, nPoints, iX, iY;
  Real gridSquareArea = dxfh * dyfh;
  Real fhptot = 0.0;
  Real fhpinjtot = 0.0;
  Real fhpetot = 0.0;
  Vector(Real) localfhp, globalfhp;
  Vector(Real) localfhpinj, globalfhpinj;
  Vector(Real) localfhpe, globalfhpe;


    nPoints = nxfh * nyfh;
    localfhp.resize(nPoints);
    globalfhp.resize(nPoints); 
    localfhpinj.resize(nPoints);
    globalfhpinj.resize(nPoints); 
    localfhpe.resize(nPoints);
    globalfhpe.resize(nPoints); 
       
       i = 0;
       for(iX = 1; iX <= nxfh; iX++)
       for(iY = 1; iY <= nyfh; iY++)
	 {
           i++;
           localfhp(i) = fhp(iX, iY);
           localfhpinj(i) = fhpinj(iX, iY);
           localfhpe(i) = fhpe(iX, iY);

	 }

    //non-parallel by default
    int nRank=0;
    int numprocs=1;  
    int i_flag;
    MPI_Initialized(&i_flag);
    if(i_flag){
       MPI_Allreduce ( (double *) &localfhp.data(), (double *) &globalfhp.data(), nPoints, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       MPI_Allreduce ( (double *) &localfhpinj.data(), (double *) &globalfhpinj.data(), nPoints, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       MPI_Allreduce ( (double *) &localfhpe.data(), (double *) &globalfhpe.data(), nPoints, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    }
    else{
      for(int i=1; i<= nPoints;i++){
	globalfhp(i)=localfhp(i);
	globalfhpinj(i)=localfhpinj(i);
	globalfhpe(i)=localfhpe(i);
      }
    }


       i = 0;
       for(iX = 1; iX <= nxfh; iX++)
       for(iY = 1; iY <= nyfh; iY++)
	 {
            i++;
            fhp(iX, iY) = globalfhp(i);
            fhpinj(iX, iY) = globalfhpinj(i);
            fhpe(iX, iY) = globalfhpe(i);
          }    

  if(nMPIrank == 0){     
  for(i = 1; i <= nxfh; i++)
  {
    for(j = 1; j <= nyfh; j++)
    {
      fhptot = fhptot + fhp(i,j);
      fhpinjtot = fhpinjtot + fhpinj(i,j);
      fhpetot = fhpetot + fhpe(i,j);
    }
  }
  fhptot = fhptot * nReals_Macro ;
  fhpinjtot = fhpinjtot * nReals_Macro ;
  fhpetot = fhpetot * nReals_Macro ;

  os << "xminfh = " << xminfh << "\n"
     << "xmaxfh = " << xmaxfh << "\n"
     << "yminfh = " << yminfh << "\n"
     << "ymaxfh = " << ymaxfh << "\n"
     << "nxfh = " << nxfh << "\n"
     << "nyfh = " << nyfh << "\n"
     << "dxfh = " << dxfh << "\n"
     << "dyfh = " << dyfh << "\n"
     << "gridSquareArea = " << gridSquareArea << "\n"
     << "nReals_Macro = " << nReals_Macro << "\n"
     << "fhptot = " << fhptot << "\n"
     << "fhpinjtot = " << fhpinjtot << "\n"
     << "fhpetot = " << fhpetot << "\n\n\n";

  for(i = 1; i <= nxfh; i++)
  {
    for(j = 1; j <= nyfh; j++)
    {
      fhp(i,j) = fhp(i,j) * nReals_Macro / gridSquareArea;
      fhpinj(i,j) = fhpinj(i,j) * nReals_Macro / gridSquareArea;
      fhpe(i,j) = fhpe(i,j) * nReals_Macro / gridSquareArea;

      os << i       << "   " << j        << "   "
         << xfh(i)  << "   " << yfh(j)   << "   "
         << fhp(i,j) << "   " << fhpinj(i,j) << "   "
         << fhpe(i,j) << "\n";
    }
    os << "\n";
  }
  }
}




///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Injection::dumpFoilHAndT
//
// DESCRIPTION
//    For proton only for the time beeing
//    Dumps Histogram of Foil Hits, Temperature distribution,
//    and evolution of temperature on the hottest spot
//
// PARAMETERS
//    Output Stream
//    RepRate: the repetetion rate of injection (Hz), default 1 Hz.
//    modelE:  Model to calculate the stopping power of electron,
//             see also comments in the code.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Injection::dumpFoilHAndT(Ostream &os)
{
  Integer i, j, ihot, jhot, iX, iY, nPoints;
  Real gridSquareArea = dxfh * dyfh;
  Real fhptot = 0.0;
  Real fhpinjtot = 0.0;
  Real fhpetot = 0.0;
  Real fhmax = 0.0;


  Real T0 = 298.0; // room temperature
  Real Me = 0.51099906e6 ; // electron mass
  Real Mp = 938.272e6 ; // proton mass
  Real KoA, Tmax, Imee, arg; //stuff for dEdX

  Real re = 2.8179409e-13 ; // electron classical radius (cm !)
  Real ne ; // electron density per unit volume
  Real Av = 6.022137e23 ; // Avogadro number
  Real edEdX ;// dEdX for electron
  Real delDE ; // the density effect term
  Real XDE ; // Log10(gamma*beta) 
  Real lEe ; // Log10(electron kinetic energy)

  Real Ce2p ; // ratio of dE/dX, proon vs. electron

  Real dE; // energy emission per step

  Integer cyc; // number of cycles
  Integer ind;
  Real cool_int; // time step to solve the derivative equation numerically
  Real inj_int; // period for one cycle
  Real Ce = 0.7; // emissivity
  Real Carea = 2.0e-6; // unit area of emission, 2 faces
  Real S= 5.67e-8; // Boltzmann constant
  Real Cp; // Heat capasity (J/K/g) ?

  Real lT;  
  Real ltime;

  Vector(Real) EvsT; //Energy deposition vs. Temperature rise
  Vector(Real) MaxTEvol;// evolution of temp. at hottest spot
  Matrix(Real) EpA; //Energy per unit Area (J/mm^2)
  Matrix(Real) EpW; //Energy per unit Weight (J/g)
  Matrix(Real) MaxT; // Temperture distribution after 20 cycles
  Matrix(Real) fheff; // The effective number of protons


  Foil *foil = Foil::safeCast(foilPointers(0));
  SyncPart *sp = SyncPart::safeCast
                 (syncP((Particles::syncPart & Particles::All_Mask)-1));


  Vector(Real) localfhp, globalfhp;
  Vector(Real) localfhpinj, globalfhpinj;
  Vector(Real) localfhpe, globalfhpe;


    nPoints = nxfh * nyfh;
    localfhp.resize(nPoints);
    globalfhp.resize(nPoints); 
    localfhpinj.resize(nPoints);
    globalfhpinj.resize(nPoints); 
    localfhpe.resize(nPoints);
    globalfhpe.resize(nPoints); 
       
       i = 0;
       for(iX = 1; iX <= nxfh; iX++)
       for(iY = 1; iY <= nyfh; iY++)
	 {
           i++;
           localfhp(i) = fhp(iX, iY);
           localfhpinj(i) = fhpinj(iX, iY);
           localfhpe(i) = fhpe(iX, iY);

	 }

    //non-parallel by default
    int nRank=0;
    int numprocs=1;  
    int i_flag;
    MPI_Initialized(&i_flag);
    if(i_flag){
       MPI_Allreduce ( (double *) &localfhp.data(), (double *) &globalfhp.data(), nPoints, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       MPI_Allreduce ( (double *) &localfhpinj.data(), (double *) &globalfhpinj.data(), nPoints, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       MPI_Allreduce ( (double *) &localfhpe.data(), (double *) &globalfhpe.data(), nPoints, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    }
    else{
      for(int i=1; i<= nPoints;i++){
	globalfhp(i)=localfhp(i);
	globalfhpinj(i)=localfhpinj(i);
	globalfhpe(i)=localfhpe(i);
      }
    }


  i = 0;
   for(iX = 1; iX <= nxfh; iX++)
   for(iY = 1; iY <= nyfh; iY++)
     {
        i++;
        fhp(iX, iY) = globalfhp(i);
        fhpinj(iX, iY) = globalfhpinj(i);
        fhpe(iX, iY) = globalfhpe(i);
     }    


  if(nMPIrank == 0){

  //  dE/dX [eV cm^2/g] for proton
  KoA= 1.0e6 * 0.307075 / foilAMU;
  Imee = 10.0 * foilZ;

  Tmax = 2.0 * Me * Sqr(sp->_betaSync * sp->_gammaSync) / 
       (1.0 + 2.0 * sp->_gammaSync * (Me / Mp ) + Sqr(Me / Mp));
  arg = 2.0  * Me * Sqr(sp->_betaSync * sp->_gammaSync) * Tmax / Sqr(Imee);

  dEdXFoil = KoA * foilZ / Sqr(sp->_betaSync) * (Log(arg)/2.0 - Sqr(sp->_betaSync));


    //  dE/dX [eV cm^2/g] for electron
    // The model 1 would be applicable to foil materials other than carbon.
    // The model 2 is dedicated to carbon graphite, it gives rather exact number.
  if (modelE == 1)
   {
    //  Reference, INTERACTIONS OF PHOTONS AND LEPTONS WITH MATTER, Roy and Reed, Academic Press (1968) 
    ne = foilZ * rhoFoil * Av / foilAMU ;
    arg = (sp->_gammaSync - 1.0) * Sqr(sp->_betaSync) * Sqr(sp->_gammaSync * Me) / 2.0 / Sqr(Imee);

    //  XDE = Log10(sp->_gammaSync * sp->_betaSync) ;
    //  if(XDE > X0Foil) {
    //  delDE = 4.606 * Log10(sp->_gammaSync * sp->_betaSync) + CFoil + aFoil * Pow(X1Foil-Log10(sp->_gammaSync * sp->_betaSync), bFoil)  ;
    //  } else {
    //  delDE = 0.0 ;
    //  }
    // ! Density effect is ignored because it is not significant and requires many parameter which depends on foil material.
    edEdX = Consts::twoPi * ne * Me * Sqr(re) / Sqr(sp->_betaSync) * (Log(arg) + ((Sqr(sp->_gammaSync) - 2.0 * sp->_gammaSync + 9.0) / 8.0  - (2.0 * sp->_gammaSync - 1.0) * Log(2.0)) / Sqr(sp->_gammaSync) ) / rhoFoil;
   }
  if (modelE == 2)
   {
    //  Table of stopping power is obtained with ESTAR, http://physics.nist.gov/PhysRefData/Star/Text/ESTAR.html , for Carbon Graphite (rho=1.7g/cm^2)
    //  and fitted by polynomial.
     lEe = Log10((sp->_gammaSync-1.0) * Me /1.0e6);
     // edEdX = 1.0e6 * (1.62625 - 0.23122 * lEe + 0.87126 * Sqr(lEe) -0.61094 * Pow(lEe, 3.0) + 0.21803 * Pow(lEe, 4.0) - 0.04722 * Pow(lEe, 5.0) + 0.07312 * Pow(lEe, 6.0)); // Pow might give wrong value for negative input!?
     edEdX = 1.0e6 * (1.62625 - 0.23122 * lEe + 0.87126 * Sqr(lEe) -0.61094 * lEe*Sqr(lEe) + 0.21803 * Sqr(lEe)*Sqr(lEe) - 0.04722 * lEe*Sqr(lEe)*Sqr(lEe) + 0.07312 * Sqr(lEe)*Sqr(lEe)*Sqr(lEe));
   }


  // dE/dX ratio
  Ce2p = edEdX / dEdXFoil ;


  fheff.resize(nxfh,nyfh); // effective number of protons/mm^2
  EpW.resize(nxfh,nyfh);
  EpA.resize(nxfh,nyfh);
  for(i = 1; i <= nxfh; i++)
  {
    for(j = 1; j <= nyfh; j++)
    {
      fhptot = fhptot + fhp(i,j);
      fhpinjtot = fhpinjtot + fhpinj(i,j);
      fhpetot = fhpetot + fhpe(i,j);
      fheff(i,j)=(fhp(i,j) + 2.0 * Ce2p * fhpinj(i,j))* nReals_Macro / gridSquareArea; // 2 electrons per H-
      //  Calculate energy deposited per bin 
      //  (H- and p+)/mm2 -> J/g
      //  factor 100 for cm^2 -> mm^2
      EpW(i,j)= 100.0 * fheff(i,j) * dEdXFoil * 1.602e-19;
      if(fhmax < fheff(i,j))
      {
	 fhmax=fheff(i,j);
         ihot = i;
         jhot = j;
      }
      EpA(i,j) = 0.0; // initialize EpA
    }
  }

  fhptot = fhptot * nReals_Macro ;
  fhpinjtot = fhpinjtot * nReals_Macro ;
  fhpetot = fhpetot * nReals_Macro ;


  EvsT.resize(4000);

  lT = T0;
  for(i = 1; i <= 4000; i++)
  {
    Cp = (528.75 - 205.9 * Pow(lT-273.0, 0.33333) +
          154.21 * sqrt(lT-273.0) - 1.53 * (lT - 273.0) +
          9.15e-5 * Sqr(lT - 273.0)) / 1000.0 ;
    lT = lT + 1./Cp ;
    EvsT(i) = lT - 298.0 ; // dT from room temperature
  }

  //  Calculation of tempareture evolution over 20 cycles

  inj_int = 1.0 / RepRate ;
  cool_int = inj_int / 500.0 ; // 500 steps for one cycle

  MaxT.resize(nxfh,nyfh);
  MaxTEvol.resize(10000);



  for(i = 1; i <= nxfh; i++)
  {
     for(j = 1; j <= nyfh; j++)
     {
         for(cyc = 1; cyc <= 20; cyc++)
         {
            // add energy deposition
            EpA(i,j) = EpA(i,j) + EpW(i,j) * foil->_thick * 1.0e-8;

            ltime = 0.0;
            ind=1;
            for(ind = 1; ind <= 500; ind++)
	    {
	      if (EpA(i,j)/foil->_thick/1.0e-8 > 0.01)
	       {
                 if ((i == ihot) && (j == jhot))
	          {
		   MaxTEvol(ind+(cyc-1)*500)=EvsT(int(EpA(i,j)/foil->_thick/1.0e-8))+298.0;
	          }
                 if (int(EpA(i,j)/foil->_thick/1.0e-8) >= 4000)
		  {
                   os << "Warning: Temperature will be too high, exceeding the heat capasity curve given internally." << "\n";
                   return;
                  }
                 lT = EvsT(int(EpA(i,j)/foil->_thick/1.0e-8)) + 298.0;
		 dE = Ce * S * Carea * (Pow(lT, 4.0)-Pow(298.0, 4.0)) * cool_int;
                 EpA(i,j) = EpA(i,j) - dE;
               }
               else 
	       {
                 if ((i == ihot) && (j == jhot))
	         {
		   MaxTEvol(ind+(cyc-1)*500)=EvsT(int(EpA(i,j)/foil->_thick/1.0e-8))+298.0;
                 }
	       }
             ltime=ltime+cool_int;
	   }
	}
     // add energy deposition (cycle=21)
     EpA(i,j) = EpA(i,j) + EpW(i,j) * foil->_thick * 1.0e-8;
     ltime = 0.0;
     ind=1;
     MaxT(i,j) = EvsT(int(EpA(i,j)/foil->_thick/1.0e-8)) + 298.0;
     }
  }

  os << "xminfh = " << xminfh << " (mm)" << "\n"
     << "xmaxfh = " << xmaxfh << " (mm)" << "\n"
     << "yminfh = " << yminfh << " (mm)" << "\n"
     << "ymaxfh = " << ymaxfh << " (mm)" << "\n"
     << "nxfh = " << nxfh << "\n"
     << "nyfh = " << nyfh << "\n"
     << "dxfh = " << dxfh << " (mm)" << "\n"
     << "dyfh = " << dyfh << " (mm)" << "\n"
     << "gridSquareArea = " << gridSquareArea << " (mm^2)" << "\n"
     << "nReals_Macro = " << nReals_Macro << "\n"
     << "Maximum temperature = " << MaxTEvol(9501) << " (K)" << "\n"
     << "for RepRate  = " << RepRate << " (Hz)" << "\n"
     << "fhptot = " << fhptot << "\n"
     << "fhpinjtot = " << fhpinjtot << "\n"
     << "fhpetot = " << fhpetot << "\n\n\n";


    os << "Temparature distribution" << "\n";
    os << "x_grid_id  y_grid_id  x(mm)  y(mm)  N_P(/mm^2)  N_H-(/mm^2)  N_P+e(/mm^2)  Temp.(K)" << "\n";


  for(i = 1; i <= nxfh; i++)
  {
    for(j = 1; j <= nyfh; j++)
    {
      fhp(i,j) = fhp(i,j) * nReals_Macro / gridSquareArea;
      fhpinj(i,j) = fhpinj(i,j) * nReals_Macro / gridSquareArea;
      fhpe(i,j) = fhpe(i,j) * nReals_Macro / gridSquareArea;

      os << i       << "   " << j        << "   "
         << xfh(i)  << "   " << yfh(j)   << "   "
         << fhp(i,j) << "   " << fhpinj(i,j) << "   "
         << fhpe(i,j) << "   " << MaxT(i,j) << "\n";
    }
    os << "\n";
  }


    os << "Temparature evolution of hottest point" << "\n";
    os << "time(sec)   Temparature(K)" << "\n";

    os << "0.0  298" << "\n";

  ltime = 10*cool_int;
  for(ind=10; ind <= 9990; ind=ind+10)
  {
    os << ltime <<"  " << MaxTEvol(ind+1) << "\n";
    ltime=ltime+10*cool_int;
  }

  }

}
