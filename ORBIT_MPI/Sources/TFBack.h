/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TImp.h
//
// AUTHOR
//   Slava Danilov, Andrey Shishlo, Jeff Holmes ORNL, vux@ornl.gov
//
// CREATED
//   6/17/2002
//
// DESCRIPTION
//   Header file for the transverse feedback class.
//   Separated from TFeedBack.cc to use elsewhere.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(__TFBack__)
#define __TFBack__

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "fftw.h"
#include "RealMat.h"
#include "ComplexMat.h"
#include "SCLTypes.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TFeedBack
//
// INHERITANCE RELATIONSHIPS
//   TFeedBack -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a base class for storing transverse impedance information.
//    
// PUBLIC MEMBERS
//   Integer:
//     _nBins        Reference to the number of bins.
//     _nParts_Bin   Integer Vector containing the particles/bin
//
//   Real
//     _deltaPhiBin  The phi bin width (rad).
//     _phiCount     Vector of the number of macros in each phi bin.
//
//   Void:
//     nameOut - dumps node name to a string
//
// PROTECTED MEMBERS
//     _forceCalculated - switch indicating that
//                        the force has been calculated.
//
// PRIVATE MEMBERS
//   None.
//
///////////////////////////////////////////////////////////////////////////

class TFBack : public Node
{
  Declare_Standard_Members(TFBack, Node);
  public:
    TFBack(const String &n, const Integer &order, Integer &nBins);
    ~TFBack();

    virtual Void nameOut(String &wname) {wname = _name;}
    virtual Void _updatePartAtNode(MacroPart &mp) = 0;
    virtual Void _nodeCalculator(MacroPart &mp) = 0;
 
    virtual Void _longBin(MacroPart &mp);

    Vector(Real) _phiCount;
    Real _deltaPhiBin;
    Vector(Real) _meshCharge;
    Vector(Real) _xCentroid, _xpCentroid;
    Vector(Real) _yCentroid, _ypCentroid;

    Integer _nBins;


  protected:
    Real _dipole2TKick;
    Integer _forceCalculated;
 
   //===MPI stuff =====start=====
   int iMPIini_;
   int nMPIsize_;
   int nMPIrank_;

    double* buff_MPI_0_;
    double* buff_MPI_1_;
   //===MPI stuff =====stop======
  
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  FBKick
//
// INHERITANCE RELATIONSHIPS
//   FBKick -> TFeedBack -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for storing transverse kick information,
//   done with the FFT implementation.
//
// PUBLIC MEMBERS
//   _useAverage  Calculate kick at bins, rather than for each particle,
//                if == 1. Interpolate back to particles.
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None.
//
///////////////////////////////////////////////////////////////////////////

class FBKick : public TFBack
{
  Declare_Standard_Members(FBKick,TFBack);
  public:
    FBKick(const String &n, const Integer &order, Integer &nBins,
Integer &nTurnDelay, Integer &useAvg, Vector(Real) &CoordinateFilter, 
                               Vector(Real) &AngleFilter );
    ~FBKick();

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
  

    Integer _useAverage,_nTurnDelay;
    Vector(Real) _AFilter, _CFilter;


 private:
    Vector(Real) _deltaXpGrid;
    Vector(Real) _deltaYpGrid;
    Vector(Real) _centroidXPreviousTurns;
    Vector(Real) _angleXPreviousTurns;
    Vector(Real) _centroidYPreviousTurns;
     Vector(Real) _angleYPreviousTurns;

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif   // __TFBack__
