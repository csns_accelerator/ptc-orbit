/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Plots.cc
//
// AUTHOR
//    John Galambos ORNL, jdg@ornl.gov
//
// CREATED
//    10/2/97
//
//  DESCRIPTION
//    Source file for the Plots module. This module implements
//    some hardwired plots using the PLPLOT package. 
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "RealMat.h"
#include "Plots.h"
#include "MacroPart.h"
#include "LongSC.h"
#include <cmath>

//=====MPI stuff============start====
#include "mpi.h"
//=====MPI stuff============stop ====

#if defined(USE_PLPLOT)
#define PL_DOUBLE
#include "plplot.h"
#include "plevent.h"
#endif // USE_PLPLOT

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

// Lists of synchronous particles and macro particles

extern Array(ObjectPtr) syncP, mParts;
extern Array(ObjectPtr) LSpaceChargePointers;
// Masks for indices

const Integer SyncP_Mask = 0x10000;
static int fakeArgc = 1;
static char *fakeArgv[1] = { "samba" };


///////////////////////////////////////////////////////////////////////////
//
// Utility functions:
//
///////////////////////////////////////////////////////////////////////////
 
Void binX(Vector(Real) &xBin, Vector(Real) &yBin, 
                 MacroPart &mp)
{

   //===Parallel MPI ======start======
   int iMPIini_=0;
   MPI_Initialized(&iMPIini_);
   int nMPIsize_ = 1;
   int nMPIrank_ = 0;
   if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======

    Integer i, bin, nBins =xBin.rows();
    //    if(nBins < 2) except(badBinSize);    
    //    if(yBin.rows() < nBins) except(badBinSize);
    Real dx = xBin(2) - xBin(1);
    yBin = 0.;

    for (i = 1; i <= mp._nMacros; i++) 
      {
	bin = 1+ Integer( (mp._x(i) - xBin(1)) / dx );
	bin = (bin <= nBins ? bin : nBins);
	yBin(bin)++;
      }

   //===Parallel MPI ======start======
    if( nMPIsize_ > 1) {
      double*  buff= (double *) malloc (sizeof(double)*nBins);
      MPI_Allreduce((double *) &yBin.data(),  buff, nBins, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      for (i = 1; i <= nBins; i++){
        yBin(i)=buff[i-1];
      } 
      free(buff);
    }
   //===Parallel MPI ======stop=======

}

Void binY(Vector(Real) &xBin, Vector(Real) &yBin, 
                 MacroPart &mp)
{

   //===Parallel MPI ======start======
   int iMPIini_=0;
   MPI_Initialized(&iMPIini_);
   int nMPIsize_ = 1;
   int nMPIrank_ = 0;
   if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======

    Integer i, bin, nBins =xBin.rows();
    //    if(nBins < 2) except(badBinSize);    
    //    if(yBin.rows() < nBins) except(badBinSize);
    Real dx = xBin(2) - xBin(1);
    yBin = 0.;

    for (i = 1; i <= mp._nMacros; i++) 
      {
	bin = 1+ Integer( (mp._y(i) - xBin(1)) / dx );
	bin = (bin <= nBins ? bin : nBins);
	yBin(bin)++;
      }
 
  //===Parallel MPI ======start======
    if( nMPIsize_ > 1) {
      double*  buff= (double *) malloc (sizeof(double)*nBins);
      MPI_Allreduce((double *) &yBin.data(),  buff, nBins, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       for (i = 1; i <= nBins; i++){
         yBin(i)=buff[i-1];
       } 
      free(buff);
    }
   //===Parallel MPI ======stop=======


}

Void binPhi(Vector(Real) &xBin, Vector(Real) &yBin, 
                 MacroPart &mp)
{

   //===Parallel MPI ======start======
   int iMPIini_=0;
   MPI_Initialized(&iMPIini_);
   int nMPIsize_ = 1;
   int nMPIrank_ = 0;
   if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======

    Integer i, bin, nBins =xBin.rows();
    //    if(nBins < 2) except(badBinSize);    
    //    if(yBin.rows() < nBins) except(badBinSize);
    Real dx = xBin(2) - xBin(1);
    yBin = 0.;

    for (i = 1; i <= mp._nMacros; i++) 
      {
	bin = 1+ Integer( (mp._phi(i) - xBin(1)) / dx );
	bin = (bin <= nBins ? bin : nBins);
	yBin(bin)++;
      }

  //===Parallel MPI ======start======
    if( nMPIsize_ > 1) {
      double*  buff= (double *) malloc (sizeof(double)*nBins);
      MPI_Allreduce((double *) &yBin.data(),  buff, nBins, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      for (i = 1; i <= nBins; i++){
        yBin(i)=buff[i-1];
      } 
      free(buff);
    }
   //===Parallel MPI ======stop=======

}

Void bindE(Vector(Real) &xBin, Vector(Real) &yBin,
           MacroPart &mp)
{

   //===Parallel MPI ======start======
   int iMPIini_=0;
   MPI_Initialized(&iMPIini_);
   int nMPIsize_ = 1;
   int nMPIrank_ = 0;
   if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======

    Integer i, bin, nBins =xBin.rows();
    //    if(nBins < 2) except(badBinSize);    
    //    if(yBin.rows() < nBins) except(badBinSize);
    Real dx = xBin(2) - xBin(1);
    yBin = 0.;

    for (i = 1; i <= mp._nMacros; i++) 
      {
	bin = 1+ Integer( (mp._deltaE(i) - xBin(1)) / dx );
	bin = (bin <= nBins ? bin : nBins);
	yBin(bin)++;
      }

  //===Parallel MPI ======start======
    if( nMPIsize_ > 1) {
      double*  buff= (double *) malloc (sizeof(double)*nBins);
      MPI_Allreduce((double *) &yBin.data(),  buff, nBins, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      for (i = 1; i <= nBins; i++){
        yBin(i)=buff[i-1];
      } 
      free(buff);
    }
   //===Parallel MPI ======stop=======

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Plots::ctor
//
// DESCRIPTION
//    Initializes the various constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Plots::ctor()
{

    xMinPlot = -100.;
    xMaxPlot = 100.;
    xPMinPlot = -20.;
    xPMaxPlot = 20.;


    yMinPlot = -100.;
    yMaxPlot = 100.;
    yPMinPlot = -20.;
    yPMaxPlot = 20.;


    dEMinPlot = -10.e-3;
    dEMaxPlot = 10.e-3;
    phiMinPlot = -pi;
    phiMaxPlot = pi;
    LSCMinPlot = -10.;
    LSCMaxPlot =  10.;

    plotsInited   = 0;
    nTransBins = 32;
    nDeltaEBins = 32;

    plotFreq = 1;
    verbosePlot = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotXWin()
//
// DESCRIPTION
//   plots the transverse phase spaces, real space and long. space
//   to an X window.   
//
// PARAMETERS

//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////
Void Plots::plotXWin(const Integer &herdNo)
{
#if defined(USE_PLPLOT)

    plend();
    plsdev("xwin");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);

    plotsInited = true;

    plot4pp(herdNo);
//    plotsInited = false;

    pleop();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

  
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotPS()
//
// DESCRIPTION
//   plots the transverse phase spaces, real spce and long. space
//   to an postsrcipt file.   
//
// PARAMETERS
//    &herdNo - The herd identifier
//    &fName - String to name file.
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotPS(const Integer &herdNo,const String &fName)
{
#if defined(USE_PLPLOT)
    plend();

    plsori(-1); // do portrait, not landscape.
    plsfnam(fName);
    plsdev("ps");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);

    plotsInited = true;   
    plot4pp(herdNo);
//    plotsInited = false;

    plend();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

  
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plot4pp()
//
// DESCRIPTION
//   Basic plots the transverse phase spaces, real spce and long. space   
//   Ploting device is already defined.
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plot4pp(const Integer &herdNo)
{
#if defined(USE_PLPLOT)

  if ( (herdNo & All_Mask) > Particles::nHerds)
         except (badHerd);

    MacroPart *mp;
    mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));

    //    mp->_findXYPExtrema();

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plssym(0., 0.25);      // try to make the dots smaller
    plschr(0., 0.65);      // scale fonts and tiks for 4pp
    plsmaj(0., 0.65);  plsmin(0., 0.65);

    plvpor(0.1, 0.45, 0.55, 0.9);
    plotHorizontalBase(herdNo);   // x-x'

    plvpor(0.6, 0.95, 0.55, 0.9);
    plotVerticalBase(herdNo);    // y-y':
 
    plvpor(0.1, 0.45, 0.075, 0.425);
    plotXYBase(herdNo);      // x-y:

    plvpor(0.6, 0.95, 0.075, 0.425);
    plotLongBase(herdNo);    // dE - phi
#else
   except(noPLPLOT);
#endif   // USE_PLPLOT
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotHorizontal()
//
// DESCRIPTION
//   Plot the Horizontal phase space
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotHorizontal(const Integer &herdNo)
{

#if defined(USE_PLPLOT)

    if(plotsInited)    plend();
    plsdev("xwin");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);


    if(!plotsInited)
     {
       plotsInited  = true;   
       plssym(0., 0.25);
     }

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plvpor(0.15, 0.95, 0.1, 0.9);
    plotHorizontalBase(herdNo);

    pleop();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotHorizontalPS
//
// DESCRIPTION
//   Plot the Horizontal phase space to a B&W postscript file.
//
// PARAMETERS
//    &herdNo - The herd identifier
//    &fileName - file to print to.
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotHorizontalPS(const Integer &herdNo, const String &fileName)
{

#if defined(USE_PLPLOT)
    plend();

    plsori(-1); // do portrait, not landscape.
    plsfnam(fileName);
    plsdev("ps");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);
    plssym(0., 0.25);

    if(!plotsInited)
     {
       plotsInited  = true;   
       plssym(0., 0.25);
     }

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plvpor(0.15, 0.95, 0.1, 0.9);  
    plotHorizontalBase(herdNo);
    plotsInited = false;

    plend();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotHorizontalBase()
//
// DESCRIPTION
//   Plot the x-x' phase
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotHorizontalBase(const Integer &herdNo)
{
#if defined(USE_PLPLOT)

    Integer i, j, bin;
    Real dx, yMax;
    Vector(Real) xBin(nTransBins), yBin(nTransBins);

  if ( (herdNo & All_Mask) > Particles::nHerds)
         except (badHerd);

    MacroPart *mp;
    mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
    mp->_findXYPExtrema();

    // x-x':

    plcol(5);
    plwind(xMinPlot, xMaxPlot, xPMinPlot, xPMaxPlot); // draw a box
    plbox("bcnst", 0.0, 0, "bcnstv", 0.0, 0);
    pllab("x (mm)", "x' (mrad)", " ");

   // draw the points
    
    xPlot.resize(mp->_nMacros/plotFreq);
    yPlot.resize(mp->_nMacros/plotFreq);
    j = 0;
    for(i=1; i <= mp->_nMacros; i++)
    {
      if(i% plotFreq == 0)
	{
          j++;
          xPlot(j) = mp->_x(i);
          yPlot(j) = mp->_xp(i);
	}
    }

    plcol(2);
    plpoin(j, (Real *) &xPlot.data(), (Real *) &yPlot.data(), 1);

    // superimpose histogram:

    dx = (mp->_xMax - mp->_xMin) / Real(nTransBins);
    for (i = 1; i <= nTransBins; i++) 
	xBin(i) = mp->_xMin + (i-1) * dx;
   
    binX(xBin, yBin, *mp);

    // reScale y's:

     yMax = 0;
     for(i=1; i<= nTransBins; i++)
        if(yBin(i) > yMax) yMax = yBin(i);
     for(i=1; i<= nTransBins; i++)
        yBin(i) = xPMinPlot + 0.25*yBin(i)/yMax * (xPMaxPlot-xPMinPlot);

   plcol(4);      
   for (i = 1; i < nTransBins; i++) 
   {
     pljoin(xBin(i), xPMinPlot, xBin(i), yBin(i));
     pljoin(xBin(i), yBin(i), xBin(i + 1), yBin(i));
     pljoin(xBin(i + 1), yBin(i), xBin(i + 1), xPMinPlot);
    }

     pljoin(xBin(nTransBins), xPMinPlot, 
            xBin(nTransBins), yBin(nTransBins));
     pljoin(xBin(nTransBins), yBin(nTransBins),
            mp->_xMax, yBin(nTransBins));
     pljoin(mp->_xMax, yBin(nTransBins),
             mp->_xMax,xPMinPlot);
            
#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotVertical()
//
// DESCRIPTION
//   Plot the vertical phase space
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotVertical(const Integer &herdNo)
{

#if defined(USE_PLPLOT)

    if(plotsInited)    plend();
    plsdev("xwin");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);


    if(!plotsInited)
     {
       plotsInited  = true;   
       plssym(0., 0.25);
     }

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plvpor(0.15, 0.95, 0.1, 0.9);
    plotVerticalBase(herdNo);

    pleop();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotVerticalPS()
//
// DESCRIPTION
//   Plot the Vertical phase space to a postscriptfile.
//
// PARAMETERS
//    &herdNo - The herd identifier
//    &fileName - file to print to.
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotVerticalPS(const Integer &herdNo, const String &fileName)
{

#if defined(USE_PLPLOT)

    plend();

    plsori(-1); // do portrait, not landscape.
    plsfnam(fileName);
    plsdev("ps");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);
    plssym(0., 0.25);

    if(!plotsInited)
     {
       plotsInited  = true;   
       plssym(0., 0.25);
     }

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plvpor(0.15, 0.95, 0.1, 0.9);  
    plotVerticalBase(herdNo);
    plotsInited = false;

    plend();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotVerticalBase()
//
// DESCRIPTION
//   Plot the y-yp' phase
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotVerticalBase(const Integer &herdNo)
{

#if defined(USE_PLPLOT)
    Integer i, j, bin;
    Real dx, yMax;
    Vector(Real) xBin(nTransBins), yBin(nTransBins);

  if ( (herdNo & All_Mask) > Particles::nHerds)
         except (badHerd);

    MacroPart *mp;
    mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
    mp->_findXYPExtrema();

    plcol(5);
    plwind(yMinPlot, yMaxPlot, yPMinPlot, yPMaxPlot); // draw a box
    plbox("bcnst", 0.0, 0, "bcnstv", 0.0, 0);
    pllab("y (mm)", "y' (mrad)", " ");

   // draw the points
    
    xPlot.resize(mp->_nMacros/plotFreq);
    yPlot.resize(mp->_nMacros/plotFreq);

    j = 0;
    for(i=1; i <= mp->_nMacros; i++)
    {
      if(i% plotFreq == 0)
	{
          j++;
          xPlot(j) = mp->_y(i);
          yPlot(j) = mp->_yp(i);
	}
    }

    plcol(2);
    plpoin(j, (Real *) &xPlot.data(), (Real *) &yPlot.data(), 1);

    // superimpose histogram:

    dx = (mp->_yMax - mp->_yMin) / Real(nTransBins);
    for (i = 1; i <= nTransBins; i++) 
	xBin(i) = mp->_yMin + (i-1) * dx;

    binY(xBin, yBin, *mp);

    // reScale y's:

     yMax = 0;
     for(i=1; i<= nTransBins; i++)
        if(yBin(i) > yMax) yMax = yBin(i);
     for(i=1; i<= nTransBins; i++)
        yBin(i) = yPMinPlot + 0.25*yBin(i)/yMax * (yPMaxPlot-yPMinPlot);

   plcol(4);      
   for (i = 1; i < nTransBins; i++) 
   {
     pljoin(xBin(i), yPMinPlot, xBin(i), yBin(i));
     pljoin(xBin(i), yBin(i), xBin(i + 1), yBin(i));
     pljoin(xBin(i + 1), yBin(i), xBin(i + 1), yPMinPlot);
    }

     pljoin(xBin(nTransBins), yPMinPlot, 
            xBin(nTransBins), yBin(nTransBins));
     pljoin(xBin(nTransBins), yBin(nTransBins),
            mp->_yMax, yBin(nTransBins));
     pljoin(mp->_yMax, yBin(nTransBins),
             mp->_yMax,yPMinPlot);

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotXY()
//
// DESCRIPTION
//   Plot the x-y space
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotXY(const Integer &herdNo)
{

#if defined(USE_PLPLOT)

    if(plotsInited)    plend();
    plsdev("xwin");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);


    if(!plotsInited)
     {
       plotsInited  = true;   
       plssym(0., 0.25);
     }

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plvpor(0.15, 0.95, 0.1, 0.9);
    plotXYBase(herdNo);

    pleop();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotXYPS()
//
// DESCRIPTION
//   Plot the XY space to a postscriptfile.
//
// PARAMETERS
//    &herdNo - The herd identifier
//    &fileName - file to print to.
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotXYPS(const Integer &herdNo, const String &fileName)
{

#if defined(USE_PLPLOT)
    plend();

    plsori(-1); // do portrait, not landscape.
    plsfnam(fileName);
    plsdev("ps");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);
    plssym(0., 0.25);

    if(!plotsInited)
     {
       plotsInited  = true;   
       plssym(0., 0.25);
     }

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plvpor(0.15, 0.95, 0.1, 0.9);  
    plotXYBase(herdNo);
    plotsInited = false;

    plend();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotXYBase()
//
// DESCRIPTION
//   Plot the x-y space
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotXYBase(const Integer &herdNo)
{

#if defined(USE_PLPLOT)
  Integer i,j;

  if ( (herdNo & All_Mask) > Particles::nHerds)
         except (badHerd);

    MacroPart *mp;
    mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
    mp->_findXYPExtrema();

    plcol(5);
    plwind(xMinPlot, xMaxPlot, yMinPlot, yMaxPlot); // draw a box
    plbox("bcnst", 0.0, 0, "bcnstv", 0.0, 0);
    pllab("x (mm)", "y (mm)", " ");

   // draw the points
    
    xPlot.resize(mp->_nMacros/plotFreq);
    yPlot.resize(mp->_nMacros/plotFreq);
    j = 0;
    for(i=1; i <= mp->_nMacros; i++)
    {
      if(i% plotFreq == 0)
	{
          j++;
          xPlot(j) = mp->_x(i);
          yPlot(j) = mp->_y(i);
	}
    }

    plcol(2);
    plpoin(j, (Real *) &xPlot.data(), (Real *) &yPlot.data(), 1);

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotLong()
//
// DESCRIPTION
//   Plot the longitudinal phase space
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotLong(const Integer &herdNo)
{

#if defined(USE_PLPLOT)
//    if(plotsInited)    plend();
    plend();
    plsdev("xwin");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);


    if(!plotsInited)
     {
       plotsInited  = true;   
       plssym(0., 0.25);
     }

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plvpor(0.15, 0.95, 0.1, 0.9);
    plotLongBase(herdNo);

    pleop();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotLongPS()
//
// DESCRIPTION
//   Plot the longitudinal phase space to a postscriptfile.
//
// PARAMETERS
//    &herdNo - The herd identifier
//    &fileName - file to print to.
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotLongPS(const Integer &herdNo, const String &fileName)
{

#if defined(USE_PLPLOT)
    plend();

    plsori(-1); // do portrait, not landscape.
    plsfnam(fileName);
    plsdev("ps");
    plinit();
    pladv(0);

    plfontld(1);
    plspause(0);
    plssym(0., 0.25);

    if(!plotsInited)
     {
       plotsInited  = true;   
       plssym(0., 0.25);
     }

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }

    plvpor(0.15, 0.95, 0.1, 0.9);  
    plotLongBase(herdNo);
    plotsInited = false;

    plend();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotLongBase()
//
// DESCRIPTION
//   Plot the longitudinal phase space
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotLongBase(const Integer &herdNo)
{

#if defined(USE_PLPLOT)

    Integer i, j, bin;
    Real dx, yMax;
    Vector(Real) xBin(nLongBins), yBin(nLongBins);
    Vector(Real) xEBin(nDeltaEBins), yEBin(nDeltaEBins);

    if ( (herdNo & All_Mask) > Particles::nHerds)
         except (badHerd);

    SyncPart *sp;
    sp = SyncPart::safeCast(syncP(0));
    MacroPart *mp;
    mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
    mp->_findXYPExtrema();

    plcol(5);
    plwind(phiMinPlot, phiMaxPlot, dEMinPlot, dEMaxPlot); // draw a box
    plbox("bcnst", 0.0, 0, "bcnstv", 0.0, 0);
    pllab("phase (rad)", "dE (GeV)", " ");


   // draw the points
    
    xPlot.resize(mp->_nMacros/plotFreq);
    yPlot.resize(mp->_nMacros/plotFreq);
    j = 0;
    for(i=1; i <= mp->_nMacros; i++)
    {
      if(i% plotFreq == 0)
	{
          j++;
          xPlot(j) = mp->_phi(i);
          yPlot(j) = mp->_deltaE(i);
	}
    }

    plcol(2);
    plpoin(j, (Real *) &xPlot.data(), 
           (Real *) &yPlot.data(), 1);

    // superimpose phi histogram:
    dx = (mp->_phiMax - mp->_phiMin) / Real(nLongBins);

    if (dx > Consts::tiny)
      {
      for (i = 1; i <= nLongBins; i++) 
       {
	  xBin(i) = mp->_phiMin + (i-1) * dx;
	  yBin(i) = 0.0;
       }

      for (i = 1; i <= mp->_nMacros; i++) 
        {
	  bin = 1+ Integer( (mp->_phi(i) - mp->_phiMin) / dx );
	  bin = (bin <= nLongBins ? bin : nLongBins);
	  yBin(bin)++;
        }

      // reScale y's:

       yMax = 0;
       for(i=1; i<= nLongBins; i++)
          if(yBin(i) > yMax) yMax = yBin(i);
       for(i=1; i<= nLongBins; i++)
          yBin(i) = dEMinPlot + 0.25*yBin(i)/yMax * (dEMaxPlot-dEMinPlot);

       plcol(4);      
       for (i = 1; i < nLongBins; i++) 
       {
        pljoin(xBin(i), dEMinPlot, xBin(i), yBin(i));
        pljoin(xBin(i), yBin(i), xBin(i + 1), yBin(i));
        pljoin(xBin(i + 1), yBin(i), xBin(i + 1), dEMinPlot);
        }

       pljoin(xBin(nLongBins), dEMinPlot, 
            xBin(nLongBins), yBin(nLongBins));
       pljoin(xBin(nLongBins), yBin(nLongBins),
            mp->_phiMax, yBin(nLongBins));
       pljoin(mp->_phiMax, yBin(nLongBins),
             mp->_phiMax,dEMinPlot);
      }

    // superimpose deltaE histogram:
    
     // claculate bins:

    mp->_findDEExtrema();

    dx = (mp->_dEMax - mp->_dEMin) / Real(nDeltaEBins);
    if(dx < Consts::tiny) return;

    for (i = 1; i <= nDeltaEBins; i++) 
     {
	xEBin(i) = mp->_dEMin + (i-1) * dx;
	yEBin(i) = 0.0;
     }
  
    for (i = 1; i <= mp->_nMacros; i++) 
      {
	bin = 1 + Integer( (mp->_deltaE(i) - mp->_dEMin) / dx );
	bin = (bin <= nDeltaEBins ? bin : nDeltaEBins);
	yEBin(bin)++;
      }
  
    // reScale y's:

     yMax = 0;
     for(i=1; i<= nDeltaEBins; i++)
        if(yEBin(i) > yMax) yMax = yEBin(i);
     for(i=1; i<= nDeltaEBins; i++)
        yEBin(i) = phiMaxPlot - 0.125*yEBin(i)/yMax * (phiMaxPlot-phiMinPlot);
  
     // Plot 'em

   plcol(4);      
   for (i = 1; i < nDeltaEBins; i++) 
   {
     pljoin(phiMaxPlot, xEBin(i), yEBin(i), xEBin(i));
     pljoin(yEBin(i), xEBin(i), yEBin(i), xEBin(i+1));
     pljoin(yEBin(i), xEBin(i+1), phiMaxPlot, xEBin(i+1));

   }


     pljoin(phiMaxPlot, xEBin(nDeltaEBins),
            yEBin(nDeltaEBins), xEBin(nDeltaEBins));
     pljoin(yEBin(nDeltaEBins), xEBin(nDeltaEBins),
            yEBin(nDeltaEBins), mp->_dEMax);

     pljoin( yEBin(nDeltaEBins), mp->_dEMax, phiMaxPlot, mp->_dEMax);

     // add the separatrix:


     if(Accelerate::nRFCavities == 0) return;

     plcol(3);  
     Accelerate::getUFixedPoint(); // get seperatrix fixed point
     Real ang = sp->_phase + Ring::bucketphi0 * (uFixedPoint - sp->_phase);
     Ring::calcBucket(bucketdE0, ang);
     Integer np = Ring::bucketdE.rows();

     for (i = 1; i < np-1; i++)  pljoin(bucketphi(i), bucketdE(i), 
             bucketphi(i+1), bucketdE(i+1));

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotLongSet()
//
// DESCRIPTION
//   Plot the longitudinal phase space, RF bucket + L. Space Charge
//    to an X window.
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotLongSet(const Integer &herdNo)
{

#if defined(USE_PLPLOT)

    plend();

    plsdev("xwin");
    plinit();
    pladv(0);
    
    plfontld(1);
    plspause(0);


    plotsInited  = true;   
 
     plotLongSetBase(herdNo);

     plotsInited = false;

    pleop();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotLongSetPS()
//
// DESCRIPTION
//   Plot the longitudinal phase space, RF bucket + L. Space Charge
//   to a file.
//
// PARAMETERS
//    &herdNo - The herd identifier
//    &fileName - file to print to.
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotLongSetPS(const Integer &herdNo, const String &fileName)
{

#if defined(USE_PLPLOT)
    plend();

    plsori(-1); // do portrait, not landscape.
    plsfnam(fileName);
    plsdev("ps");
    plinit();
    pladv(0);
    
    plfontld(1);
    plspause(0);

    plotsInited = true;   
    plotLongSetBase(herdNo);
    plotsInited = false;

    plend();

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// plotLongSetBase()
//
// DESCRIPTION
//   Plot the longitudinal phase space, RF bucket + L. Space Charge
//   to a preset up plotting window or file.
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::plotLongSetBase(const Integer &herdNo)
{
#if defined(USE_PLPLOT)
    Integer i, j, bin;
    Real dx, yMax, yScale, kickMin, kickMax;
    Vector(Real) xBin(nLongBins), yBin(nLongBins);
    LongSC *lsc;
    Vector(Real) ang(nLongBins), kick(nLongBins);

  if ( (herdNo & All_Mask) > Particles::nHerds)
         except (badHerd);

    if(verbosePlot)
    {
      plvpor(0., 1., 0., 1.);
      plcol(6);
      char num[10];
      sprintf(num, "%d", nTurnsDone); 
      String text = "Case = " + runName + ", turn = " + num;
      plmtex("t", -1.,0.025 , 0, text.cptr());
    }
    plssym(0., 0.25);      // try to make the dots smaller
    plschr(0., 0.65);      // scale fonts and tiks for 4pp
    plsmaj(0., 0.65);  plsmin(0., 0.65);


//  Plot the voltage  and dist vs. phi:

    plvpor(0.1, 0.95, 0.55, 0.9);    
    
    MacroPart *mp;
    mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));

    mp->_findXYPExtrema();

    plcol(5);
    plwind(phiMinPlot, phiMaxPlot, LSCMinPlot, LSCMaxPlot);
    plbox("bcnst",0.0, 0, "bcnstv", 0.0, 0); // draw a box
    pllab("phase (rad)", "Voltage/turn (kV)", " ");

     //  Plot the spacecharge:

     if(nLongSCs > 0)
       {
        kickMax = kickMin = 0.;
        for(i=1; i<= nLongBins; i++)
          {
            ang(i) = -Consts::pi + Real(i-1)* Consts::twoPi/(Real(nLongBins));
            for(j=1; j<= nLongSCs; j++)
	      {
                lsc = LongSC::safeCast(LSpaceChargePointers(j - 1));
                kick(i) += 1.e6 * lsc->_kick(ang(i)); // in kV
                if(kick(i) > kickMax) kickMax = kick(i);
                if(kick(i) < kickMin) kickMin = kick(i);
	      }
	  }
        yScale = Max(Abs(kickMax), Abs(kickMin));

        plcol(2);
        for(i=1; i< nLongBins; i++) 
        	pljoin(ang(i), kick(i), ang(i+1), kick(i+1));

       }

    // Plot phi histogram:

    dx = (mp->_phiMax - mp->_phiMin) / Real(nLongBins);


    for (i = 1; i <= nLongBins; i++) 
     {
	xBin(i) = mp->_phiMin + (i-1) * dx;
	yBin(i) = 0.0;
     }

    for (i = 1; i <= mp->_nMacros; i++) 
      {
	bin = 1+ Integer( (mp->_phi(i) - mp->_phiMin) / dx );
	bin = (bin <= nLongBins ? bin : nLongBins);
	yBin(bin)++;
      }

    // reScale y's:

     yMax = 0;
     for(i=1; i<= nLongBins; i++)
        if(yBin(i) > yMax) yMax = yBin(i);
     for(i=1; i<= nLongBins; i++)
        yBin(i) = LSCMinPlot + 0.25*yBin(i)/yMax * (LSCMaxPlot - LSCMinPlot);

   plcol(4);      
   for (i = 1; i < nLongBins; i++) 
   {
     pljoin(xBin(i), LSCMinPlot, xBin(i), yBin(i));
     pljoin(xBin(i), yBin(i), xBin(i + 1), yBin(i));
     pljoin(xBin(i + 1), yBin(i), xBin(i + 1), LSCMinPlot);
    }

     pljoin(xBin(nLongBins), LSCMinPlot, 
            xBin(nLongBins), yBin(nLongBins));
     pljoin(xBin(nLongBins), yBin(nLongBins),
            mp->_phiMax, yBin(nLongBins));
     pljoin(mp->_phiMax, yBin(nLongBins),
             mp->_phiMax,LSCMinPlot);


// Plot the points now:

    plvpor(0.1, 0.95, 0.075, 0.425);
    plcol(5);
    plwind(phiMinPlot, phiMaxPlot, dEMinPlot, dEMaxPlot);
    plbox("bcnst",0.0, 0, "bcnstv", 0.0, 0); // draw a box
    pllab("phase (rad)", "dE (GeV)", " ");

   // draw the points
    
    xPlot.resize(mp->_nMacros/plotFreq);
    yPlot.resize(mp->_nMacros/plotFreq);
    j = 0;
    for(i=1; i <= mp->_nMacros; i++)
    {
      if(i% plotFreq == 0)
	{
          j++;
          xPlot(j) = mp->_phi(i);
          yPlot(j) = mp->_deltaE(i);
	}
    }
    plcol(2);
    plpoin(j, (Real *) &xPlot.data(), 
           (Real *) &yPlot.data(), 1);

    plotsInited = false;

#else
   except(noPLPLOT);
#endif   // USE_PLPLOT

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// binHorizontal()
//
// DESCRIPTION
//   Plot the horizontal distribution of a herd
//
// PARAMETERS
//    &herdNo - The herd identifier
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Plots::binHorizontal(const Integer &herdNo, Vector(Real) &xBin,
                          Vector(Real) &yBin)
{
  if((herdNo & All_Mask) > Particles::nHerds)
    except (badHerd);
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
  if (xBin.rows() < 2) except(badBinSize);
  if (yBin.rows() < 2) except(badBinSize);

  binX(xBin, yBin, *mp);
}

Void Plots::binVertical(const Integer &herdNo, Vector(Real) &xBin,
                        Vector(Real) &yBin)
{
  if((herdNo & All_Mask) > Particles::nHerds)
    except (badHerd);
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
  if (xBin.rows() < 2) except(badBinSize);
  if (yBin.rows() < 2) except(badBinSize);

  binY(xBin, yBin, *mp);
}

Void Plots::binLongitudinal(const Integer &herdNo, Vector(Real) &xBin,
                            Vector(Real) &yBin)
{
  if((herdNo & All_Mask) > Particles::nHerds)
    except (badHerd);
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
  if(xBin.rows() < 2) except(badBinSize);
  if(yBin.rows() < 2) except(badBinSize);

  binPhi(xBin, yBin, *mp);
}

Void Plots::binEnergy(const Integer &herdNo, Vector(Real) &xBin,
                      Vector(Real) &yBin)
{
  if((herdNo & All_Mask) > Particles::nHerds)
    except (badHerd);
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
  if(xBin.rows() < 2) except(badBinSize);
  if(yBin.rows() < 2) except(badBinSize);

  bindE(xBin, yBin, *mp);
}
