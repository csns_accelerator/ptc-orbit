///////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    ErrorBase.h
//
// AUTHOR
//    Steven Bunch, University of Tennessee, bunchsc@sns.gov
//
// CREATED
//    6/17/02
//
//  DESCRIPTION
//    Base header file for Error class
//
//  REVISION HISTORY
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    ErrorBase
//
// INHERITANCE RELATIONSHIPS
//    ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    This is a base class for errors.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

#if !defined(__Err__)
#define __Err__

#include "Object.h"

class ErrorBase : public Node
{
    Declare_Standard_Members(ErrorBase, Node);
  public:
    ErrorBase(const String &n, const Integer &order):
              Node(n, 0., order)
    {
    }
    Void nameOut(String &wname) { wname = _name; }
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Void _showError(Ostream &sout)=0;
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
};

#endif   // __Err__
