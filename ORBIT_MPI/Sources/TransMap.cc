/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TransMap.cc
//
// AUTHOR
//   John Galambos
//   ORNL, jdg@ornl.gov
//
// CREATED
//   12/11/97
//
// DESCRIPTION
//   Module descriptor file for the TransMap module. This module contains
//   Source for the TransMap module. This module generates and implements
//   Transfer matrices.
//
// REVISION HISTORY
//   03/2000 : Added 2nd Order TransMatrix2.
//             Jeff Holmes (jzh@ornl.gov) and John Galambos
//   03/2002 : Moved TransMatrixBase to MapBase and
//             changed TransMatrix1 to accomodate coupling
//             by using same technique as TransMatrix2.
//             Jeff Holmes
//   07/2006 : The interface to PTC code added. To use it you should
//             specify USE_PTC environment variable during the compilation
//             and linking. The PTC code library should be compiled before that.
//             Andrei Shishlo
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "TransMap.h"
#include "MacroPart.h"
#include "Node.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "StrClass.h"
#include "StreamType.h"
#include "RealMat.h"
#include "IntegerMat.h"
#include <cmath>
#if defined(_WIN32)
  #include <strstrea.h>
#else
  #include <sstream>
#endif
#include <iomanip>
// BeamLine Headers
#include "FNALMaps.h"
#include "HerdCoords.h"

//========= PTC interface ============================
#if defined(USE_PTC)

#include "cinterface2ptc.hh"

#endif   //USE_PTC

using namespace std;
///////////////////////////////////////////////////////////////////////////
// Temporary Array Dimensions
///////////////////////////////////////////////////////////////////////////

#define nEltDim  1000

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

// Lists of macro particles, etc.

Array(ObjectPtr) tMaps;

extern Array(ObjectPtr) mParts, nodes, syncP;

// Granularity to allocate entries in work arrays

const Integer Node_Alloc_Chunk = 10;

// Prototype of some local stuff:

Void addFNALMap(const String &name, const Integer &order,
                const Integer &maporder,
                FNALTMs *BeamLine,
                const Integer &BmLIndex,
                const Real &bx, const Real &by,
                const Real &ax, const Real &ay,
                const Real &ex, const Real &epx,
                const Real &l, const String &et);
Void TMfindPhases(MapBase &tm, MacroPart& mp, const Integer &i);
Void TMdoTunes(MapBase &tm, MacroPart &mp);
Real  TMxPhase, TMyPhase;

Void addPTC_Map(const String &name, const Integer &order,
                const Integer &orbit_ptc_node_inde_in,
                const Real &bx, const Real &by,
                const Real &ax, const Real &ay,
                const Real &ex, const Real &epx,
                const Real &l, const String &et);

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS MapBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(MapBase, Node);

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TransMatrix1
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TransMatrix1, MapBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TransMatrix1::TransMatrix1
//
// DESCRIPTION
//
//   Constructor for 1st order transfer matrix class. Reads in the
//   real matrices and culls the null values.
//
// PARAMETERS
//   ..
//
///////////////////////////////////////////////////////////////////////////

TransMatrix1::TransMatrix1(const String &n, const Integer &order,
                           const Matrix(Real) &R,
                           const Real &bx, const Real &by,
                           const Real &ax, const Real &ay,
                           const Real &ex, const Real &epx,
                           const Real &l_r, const String &et,
                           const Integer &matrep) :
                           MapBase(n,order,bx,by,ax,ay,ex,epx,l_r,et)
{
  Integer i,j, k, l;


  MacroPart *mp;
  mp= MacroPart::safeCast
      (mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  Real gamma = mp->_syncPart._gammaSync;
  Real gamma2 = gamma * gamma;
  Real beta = mp->_syncPart._betaSync;
  Real beta2 = beta * beta;
  _matrep = matrep;
  _detX = R(1,1) * R(2,2) - R(1,2) * R(2,1);
  _detY = R(3,3) * R(4,4) - R(3,4) * R(4,3);

  //  cull null values from R matrix.

  // set up culled R matrix transverse elements:

  _nRTVals = 0;

  for (j = 1; j <= 4; j++)
  {
    for(k = 1; k <= 4; k++)
    {
      if(Abs(R(j,k)) > Consts::tiny) _nRTVals++;
    }
    k = 6;
    if(Abs(R(j,k)) > Consts::tiny) _nRTVals++;
  }

  _jRTVals.resize(_nRTVals);
  _kRTVals.resize(_nRTVals);
  _RTVals.resize(_nRTVals);
;
  i=0;
  for (j = 1; j <= 4; j++)
  {
    for(k = 1 ; k <= 4; k++)
    {
      if(Abs(R(j,k)) > Consts::tiny)
      {
        i++;
        _jRTVals(i) = j;
        _kRTVals(i) = k;
        _RTVals(i) = R(j,k);
      }
    }
    k = 6;
    if(Abs(R(j,k)) > Consts::tiny)
    {
      i++;
      _jRTVals(i) = j;
      _kRTVals(i) = k;
      if(_matrep != 0) _RTVals(i) = R(j,k);
      if(_matrep == 0) _RTVals(i) = beta * R(j,k);
    }
  }

  // set up culled R matrix longitudinal elements:

  _nRLVals = 0;

  for(k = 1; k <= 4; k++)
  {
    if(Abs(R(5,k)) > Consts::tiny) _nRLVals++;
  }
  k = 6;
  if(Abs(R(5,k)) > Consts::tiny) _nRLVals++;

  _kRLVals.resize(_nRLVals);
  _RLVals.resize(_nRLVals);

  i = 0;
  for(k = 1; k <= 4; k++)
  {
    if(Abs(R(5,k)) > Consts::tiny)
    {
      i++;
      _kRLVals(i) = k;
      if(_matrep != 0) _RLVals(i) = R(5,k);
      if(_matrep == 0) _RLVals(i) = -beta * R(5,k);
    }
  }
  k = 6;
  if(Abs(R(5,k)) > Consts::tiny)
  {
    i++;
    _kRLVals(i) = k;
    if(_matrep != 0) _RLVals(i) = R(5,k);
    if(_matrep == 0) _RLVals(i) = -beta2 * R(5,k);
  }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    TransMatrix1::NodeCalculator
//
// DESCRIPTION
//    Does not do anything if tune tracking is not activated.
//    Otherwise, checks to see if any new particles have appeared
//    since the last tune tracking initiation, and does appropriate
//    initializations for them.
//
// PARAMETERS
//    mp = reference to the herd being tracked.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TransMatrix1::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TransMatrix1 *tm =
      TransMatrix1::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TMfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TMxPhase;
      Particles::yPhaseOld(i) = TMyPhase;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TransMatrix1::updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle with the the Transfer Matrix.
//   Advance the macroparticle coordinates through a transfer matrix.
//   If the tune claculation is on, calculate the betatron phase advance
//   through this matrix element.
//
// PARAMETERS
//   mp = reference to the herd being traked.
//   i  = macro-particle index in the herd.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TransMatrix1::_updatePartAtNode(MacroPart& mp)
{
  Integer i, iR;
  Real gamma = mp._syncPart._gammaSync;
  Real gamma2 = gamma * gamma;
  Vector(Real) oldVals(6), newVals(4);
  Real R5Sum, term;
  Real factor = 2. * Consts::pi * Ring::harmonicNumber / Ring::lRing;

  if(_length < 0.0) return;

  for(i = 1; i <= mp._nMacros; i++)
  {
    oldVals(1) = mp._x(i) / 1000.;
    oldVals(2) = mp._xp(i) / 1000.;
    oldVals(3) = mp._y(i) / 1000.;
    oldVals(4) = mp._yp(i) / 1000.;
    oldVals(5) = mp._phi(i);
    oldVals(6) = mp._dp_p(i);
    newVals = 0;

    // R(i,5) = 0, so we don't have to worry about
    // using phi instead of length on RHS

    for (iR=1; iR <= _nRTVals; iR++)
      newVals(_jRTVals(iR)) += _RTVals(iR) * oldVals(_kRTVals(iR));

    mp._x(i)  =  newVals(1) * 1000.;
    mp._xp(i) =  newVals(2) * 1000.;
    mp._y(i)  =  newVals(3) * 1000.;
    mp._yp(i) =  newVals(4) * 1000.;

    R5Sum = 0.;
    for(iR = 1; iR <= _nRLVals; iR++)
      R5Sum += _RLVals(iR) * oldVals(_kRLVals(iR));

    if(_matrep != 0) term = -_length * oldVals(6) / gamma2;
    if(_matrep == 0) term = 0.0;

    mp._phi(i) += factor * (R5Sum + term);

    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TMdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TransMatrix2
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TransMatrix2, MapBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TransMatrix2::TransMatrix2
//
// DESCRIPTION
//
//   Constructor for 2nd order transfer matrix class. Reads in the
//   real matrices and culls the null values.
//
// PARAMETERS
//   ..
//
///////////////////////////////////////////////////////////////////////////

TransMatrix2::TransMatrix2(const String &n, const Integer &order,
                           const Matrix(Real) &R, const Array3(Real) &T,
                           const Real &bx, const Real &by,
                           const Real &ax, const Real &ay,
                           const Real &ex, const Real &epx,
                           const Real &l_r, const String &et,
                           const Integer &matrep) :
                           MapBase(n,order,bx,by,ax,ay,ex,epx,l_r,et)
{
  Integer i,j, k, l;

  MacroPart *mp;
  mp= MacroPart::safeCast
      (mParts((Particles::mainHerd & Particles::All_Mask) - 1));
  Real gamma = mp->_syncPart._gammaSync;
  Real gamma2 = gamma * gamma;
  Real beta = mp->_syncPart._betaSync;
  Real beta2 = beta * beta;
  Real beta3 = beta2 * beta;

  _matrep = matrep;
  _detX = R(1,1) * R(2,2) - R(1,2) * R(2,1);
  _detY = R(3,3) * R(4,4) - R(3,4) * R(4,3);

  //  cull null values from R matrix and T matrices.

  // set up culled R matrix transverse elements:

  _nRTVals = 0;

  for (j = 1; j <= 4; j++)
  {
    for(k = 1; k <= 4; k++)
    {
      if(Abs(R(j,k)) > Consts::tiny) _nRTVals++;
    }
    k = 6;
    if(Abs(R(j,k)) > Consts::tiny) _nRTVals++;
  }

  _jRTVals.resize(_nRTVals);
  _kRTVals.resize(_nRTVals);
  _RTVals.resize(_nRTVals);

  i=0;
  for (j = 1; j <= 4; j++)
  {
    for(k = 1 ; k <= 4; k++)
    {
      if(Abs(R(j,k)) > Consts::tiny)
      {
        i++;
        _jRTVals(i) = j;
        _kRTVals(i) = k;
        _RTVals(i) = R(j,k);
      }
    }
    k = 6;
    if(Abs(R(j,k)) > Consts::tiny)
    {
      i++;
      _jRTVals(i) = j;
      _kRTVals(i) = k;
      if(_matrep != 0) _RTVals(i) = R(j,k);
      if(_matrep == 0) _RTVals(i) = beta * R(j,k);
    }
  }

  // set up culled R matrix longitudinal elements:

  _nRLVals = 0;
  for(k = 1; k <= 4; k++)
  {
    if(Abs(R(5,k)) > Consts::tiny) _nRLVals++;
  }
  k = 6;
  if(Abs(R(5,k)) > Consts::tiny) _nRLVals++;

  _kRLVals.resize(_nRLVals);
  _RLVals.resize(_nRLVals);

  i = 0;
  for(k = 1; k <= 4; k++)
  {
    if(Abs(R(5,k)) > Consts::tiny)
    {
      i++;
      _kRLVals(i) = k;
      if(_matrep != 0) _RLVals(i) = R(5,k);
      if(_matrep == 0) _RLVals(i) = -beta * R(5,k);
    }
  }
  k = 6;
  if(Abs(R(5,k)) > Consts::tiny)
  {
    i++;
    _kRLVals(i) = k;
    if(_matrep != 0) _RLVals(i) = R(5,k);
    if(_matrep == 0) _RLVals(i) = -beta2 * R(5,k);
  }

  // set up culled T matrix transverse elements:

  _nTTVals = 0;

  for(j = 1; j <= 4; j++)
  {
    for(k = 1; k <= 4; k++)
    {
      for(l = 1; l <= k; l++)
      {
  	if(Abs(T(j,k,l)) > Consts::tiny) _nTTVals++;
      }
    }
    k = 6;
    for(l = 1; l <= 4; l++)
    {
      if(Abs(T(j,k,l)) > Consts::tiny) _nTTVals++;
    }
    l = 6;
    if(Abs(T(j,k,l)) > Consts::tiny) _nTTVals++;
  }

  _jTTVals.resize(_nTTVals);
  _kTTVals.resize(_nTTVals);
  _lTTVals.resize(_nTTVals);
  _TTVals.resize(_nTTVals);

  i=0;
  for(j = 1; j <= 4; j++)
  {
    for(k = 1; k <= 4; k++)
    {
      for(l = 1; l <= k; l++)
      {
        if(Abs(T(j,k,l)) > Consts::tiny)
        {
          i++;
          if(l==k)
          {
            _TTVals(i) = T(j,k,l);
	  }
          else
          {
            _TTVals(i) = 2. * T(j,k,l);
	  }
          _jTTVals(i) = j;
          _kTTVals(i) = k;
          _lTTVals(i) = l;
	}
      }
    }
    k = 6;
    for(l = 1; l <= 4; l++)
    {
      if(Abs(T(j,k,l)) > Consts::tiny)
      {
        i++;
        if(_matrep != 0) _TTVals(i) = 2. * T(j,k,l);
        if(_matrep == 0) _TTVals(i) = beta * 2. * T(j,k,l);
        _jTTVals(i) = j;
        _kTTVals(i) = k;
        _lTTVals(i) = l;
      }
    }
    l = 6;
    if(Abs(T(j,k,l)) > Consts::tiny)
    {
      i++;
      if(_matrep != 0) _TTVals(i) = T(j,k,l);
      if(_matrep == 0) _TTVals(i) = beta2 * T(j,k,l);
      _jTTVals(i) = j;
      _kTTVals(i) = k;
      _lTTVals(i) = l;
    }
  }

  // set up culled T matrix longitudinal elements:

  _nTLVals = 0;

  for(k = 1; k <= 4; k++)
  {
    for(l = 1; l <= k; l++)
    {
      if(Abs(T(5,k,l)) > Consts::tiny) _nTLVals++;
    }
  }
  k = 6;
  for(l = 1; l <= 4; l++)
  {
    if(Abs(T(5,k,l)) > Consts::tiny) _nTLVals++;
  }
  l=6;
  if(Abs(T(5,k,l)) > Consts::tiny) _nTLVals++;

  _kTLVals.resize(_nTLVals);
  _lTLVals.resize(_nTLVals);
  _TLVals.resize(_nTLVals);

  i=0;
  for(k = 1; k <= 4; k++)
  {
    for(l = 1; l <= k; l++)
    {
      if(Abs(T(5,k,l)) > Consts::tiny)
      {
        i++;
        if(l==k)
        {
          if(_matrep != 0) _TLVals(i) = T(5,k,l);
          if(_matrep == 0) _TLVals(i) = -beta * T(5,k,l);
	}
        else
        {
          if(_matrep != 0) _TLVals(i) = 2. * T(5,k,l);
          if(_matrep == 0) _TLVals(i) = -beta * 2. * T(5,k,l);
	}
        _kTLVals(i) = k;
        _lTLVals(i) = l;
      }
    }
  }
  k = 6;
  for(l = 1; l <= 4; l++)
  {
    if(Abs(T(5,k,l)) > Consts::tiny)
    {
      i++;
      if(_matrep != 0) _TLVals(i) = 2. * T(5,k,l);
      if(_matrep == 0) _TLVals(i) = -beta2 * 2. * T(5,k,l);
      _kTLVals(i) = k;
      _lTLVals(i) = l;
    }
  }
  l=6;
  if(Abs(T(5,k,l)) > Consts::tiny)
  {
    i++;
    if(_matrep != 0) _TLVals(i) = T(5,k,l);
    if(_matrep == 0) _TLVals(i) = -beta3 * T(5,k,l);
    _kTLVals(i) = k;
    _lTLVals(i) = l;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TransMatrix2::NodeCalculator
//
// DESCRIPTION
//   Does not do anything if tune tracking is not activated.
//   Otherwise, checks to see if any new particles have appeared
//   since the last tune tracking initiation, and does appropriate
//   initializations for them.
//
// PARAMETERS
//   mp = reference to the herd being tracked.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TransMatrix2::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TransMatrix2 *tm =
      TransMatrix2::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TMfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TMxPhase;
      Particles::yPhaseOld(i) = TMyPhase;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TransMatrix2::updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle with the the Transfer Matrix.
//   Advance the macroparticle coordinates through a transfer matrix.
//   If the tune claculation is on, calculate the betatron phase advance
//   through this matrix element.
//
// PARAMETERS
//   mp = reference to the herd being traked.
//   i  = macro-particle index in the herd.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TransMatrix2::_updatePartAtNode(MacroPart& mp)
{
  Integer i, iR, iT;
  Real dpp2;
  Real gamma = mp._syncPart._gammaSync;
  Real gamma2 = gamma * gamma;
  Real beta = mp._syncPart._betaSync;
  Real beta2 = beta * beta;
  Real factor1 = 1./(2. * gamma2 * beta2 * beta2);
  Real factor2 = (1. + 1.5 * gamma2 * beta2);
  Real factor3 = 2. * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real R5Sum, T5Sum, term3, term4;
  Vector(Real) oldVals(6), newVals(4);

  if(_length < 0.0) return;

  for(i = 1; i <= mp._nMacros; i++)
  {
    oldVals(1) = mp._x(i) / 1000.;
    oldVals(2) = mp._xp(i) / 1000.;
    oldVals(3) = mp._y(i) / 1000.;
    oldVals(4) = mp._yp(i) / 1000.;
    oldVals(5) = mp._phi(i);
    if(_matrep != 0)
    {
      dpp2 = mp._dp_p(i)
             - factor1 * Sqr(mp._deltaE(i) / mp._syncPart._eTotal);
    }
    if(_matrep == 0) dpp2 = mp._dp_p(i);
    oldVals(6) = dpp2;
    newVals = 0;

    // R(i,5) = 0 and
    // T(i,5,k) = T(i,j,5) = 0, so we don't have to worry about
    // using phi instead of length on RHS

    for(iR = 1; iR <= _nRTVals; iR++)
    {
      newVals(_jRTVals(iR)) += _RTVals(iR) * oldVals(_kRTVals(iR));
    }

    for(iT = 1; iT <= _nTTVals; iT++)
    {
      newVals(_jTTVals(iT)) += _TTVals(iT) * oldVals(_kTTVals(iT)) *
                               oldVals(_lTTVals(iT));
    }
    mp._x(i)  =  newVals(1) * 1000.;
    mp._xp(i) =  newVals(2) * 1000.;
    mp._y(i)  =  newVals(3) * 1000.;
    mp._yp(i) =  newVals(4) * 1000.;

    R5Sum = 0.;
    for(iR = 1; iR <= _nRLVals; iR++)
    {
      R5Sum += _RLVals(iR) * oldVals(_kRLVals(iR));
    }
    if(_matrep != 0)
    {
      R5Sum *= (1. - oldVals(6) / gamma2);
    }

    T5Sum = 0.;
    for(iT = 1; iT <= _nTLVals; iT++)
    {
      T5Sum += _TLVals(iT)
               * oldVals(_kTLVals(iT)) * oldVals(_lTLVals(iT));
    }

    term3 = 0.0;
    term4 = 0.0;
    if(_matrep != 0)
    {
      term3 = -_length * oldVals(6)/gamma2;
      term4 = _length * Sqr(oldVals(6)/gamma2 ) * factor2;
    }

    mp._phi(i) += factor3 *
                  (R5Sum + T5Sum + term3  + term4);

    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TMdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FNALMap
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(FNALMap, MapBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FNALMap::FNALMap
//
// DESCRIPTION
//
//   Constructor for transport using maps of order maporder
//   from FNAL Beamline libraries.  Order = 0 uses "exact" maps.
//
// PARAMETERS
//   ..
//
///////////////////////////////////////////////////////////////////////////

FNALMap::FNALMap(const String &n, const Integer &order,
                 const Integer &maporder,
                 FNALTMs *BeamLine,
                 const Integer &BmLIndex,
                 const Real &bx, const Real &by,
                 const Real &ax, const Real &ay,
                 const Real &ex, const Real &epx,
                 const Real &l, const String &et) :
                 MapBase(n,order,bx,by,ax,ay,ex,epx,l,et)
{
    _maporder = maporder;
    _BeamLine = BeamLine;
    _BmLIndex = BmLIndex;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FNALMap::NodeCalculator
//
// DESCRIPTION
//   Does not do anything if tune tracking is not activated.
//   Otherwise, checks to see if any new particles have appeared
//   since the last tune tracking initiation, and does appropriate
//   initializations for them.
//
// PARAMETERS
//   mp = reference to the herd being tracked.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FNALMap::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    FNALMap *tm =
      FNALMap::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TMfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TMxPhase;
      Particles::yPhaseOld(i) = TMyPhase;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FNALMap::updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle with map from the FNAL Beamline transport.
//   If the tune claculation is on, calculate the betatron phase advance
//   through this matrix element.
//
// PARAMETERS
//   mp = reference to the herd being traked.
//   i  = macro-particle index in the herd.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FNALMap::_updatePartAtNode(MacroPart& mp)
{
  Integer I;
  int i, nMacros;
  double x_map;
  double xp_map;
  double y_map;
  double yp_map;
  double phi_map;
  double deltaE_map;

  if(_length < 0.0) return;

  HerdCoords *Coords;
  Coords = new HerdCoords;

  nMacros = mp._nMacros;
  for(i = 0; i < nMacros; i++)
  {
    I = Integer(i+1);
    x_map = double(mp._x(I));
    xp_map = double(mp._xp(I));
    y_map = double(mp._y(I));
    yp_map = double(mp._yp(I));
    phi_map = double(mp._phi(I));
    deltaE_map = double(mp._deltaE(I));
    Coords -> setCoord(x_map, xp_map,
                       y_map, yp_map,
                       phi_map, deltaE_map);
  }

  int eltIndex = int(_BmLIndex);
  _BeamLine->stepNodeProtons(eltIndex, Coords);

  for(i = 0; i < nMacros; i++)
  {
    I = Integer(i+1);
    mp._x(I) = Real(Coords -> _x_map[i]);
    mp._xp(I) = Real(Coords -> _xp_map[i]);
    mp._y(I) = Real(Coords -> _y_map[i]);
    mp._yp(I) = Real(Coords -> _yp_map[i]);
    mp._phi(I) = Real(Coords -> _phi_map[i]);
    mp._deltaE(I) = Real(Coords -> _deltaE_map[i]);
    mp._dp_p(I) = mp._deltaE(I) * mp._syncPart._dppFac;
    if(Abs(mp._phi(I)) > Consts::pi)
    {
      Real sign = -mp._phi(I) / Abs(mp._phi(I));
      mp._phi(I) = Consts::pi * sign + fmod(mp._phi(I),Consts::pi);
    }
  }

  delete Coords;

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TMdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE TransMap
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    TransMap::ctor
//
// DESCRIPTION
//    Initializes the various TransMap related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::ctor()
{

// set some initial values

    nTransMaps = 0;
    nTransMats1 = 0;
    nTransMats2 = 0;

    tuneCalcOn = 0;
    nTuneTurns = 0;
    useSimpleTuneCalc = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TransMap::addTransferMatrix
//
// DESCRIPTION
//   Adds a transfer matrix object
//
// PARAMETERS
//   name:    Name for the TM
//   order:   Order index
//   R        The (6x6) transfer matrix
//   bx       Horizontal beta at beginning of element [m]
//   by       Vertical beta at beginning [m]
//   ax       horizontal alpha at beginning
//   ay       vertical alpha at beginning
//   etax     horizontal dispersion at beginning [m]
//   etaPX    d etax/dx at beginning
//   l        length of the element [m]
//   et       element type
//   matrep   =0 for MAD, !=0 otherwise (DIMAD, Uniform lattice)
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::addTransferMatrix(const String &name, const Integer &order,
  const Matrix(Real) &R,
  const Real &bx, const Real &by, const Real &ax, const Real &ay,
  const Real &ex, const Real &epx,
  const Real &l, const String &et, const Integer &matrep)

{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
      tMaps.resize(nTransMaps + Node_Alloc_Chunk);

   nNodes++;
   nodes(nNodes-1) = new TransMatrix1(name, order, R,
                                      bx, by, ax, ay,
                                      ex, epx,
                                      l, et, matrep);
   nTransMaps ++;
   nTransMats1 ++;
   tMaps(nTransMaps -1) = nodes(nNodes-1);

   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TransMap::addTransferMatrix2
//
// DESCRIPTION
//   Adds a transfer matrix object
//
// PARAMETERS
//   name:    Name for the TM
//   order:   Order index
//   R        The (6x6) 1st order transfer matrix
//   T        The 6x6x6 2nd order transfer matrix
//   bx       Horizontal beta at beginning of element [m]
//   by       Vertical beta at beginning [m]
//   ax       horizontal alpha at beginning
//   ay       vertical alpha at beginning
//   etax     horizontal dispersion at beginning [m]
//   etaPX    d etax/dx at beginning
//   l        length of the element [m]
//   et       element type
//   matrep   =0 for MAD, !=0 otherwise (DIMAD, Uniform lattice)
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::addTransferMatrix2(const String &name, const Integer &order,
   const Matrix(Real) &R, const Array3(Real) &T,
   const Real &bx, const Real &by, const Real &ax, const Real &ay,
   const Real &ex, const Real &epx,
   const Real &l, const String &et, const Integer &matrep)

{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
      tMaps.resize(nTransMaps + Node_Alloc_Chunk);

   nNodes++;

   nodes(nNodes-1) = new TransMatrix2(name, order, R, T,
                                      bx, by, ax, ay,
                                      ex, epx,
                                      l, et, matrep);
   nTransMaps ++;
   nTransMats2 ++;
   tMaps(nTransMaps -1) = nodes(nNodes-1);

   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   addFNALMap
//   Note:  This is a local routine.
//
// DESCRIPTION
//   Adds an FNALMap object
//
// PARAMETERS
//   name:      Name for the TM
//   order:     Order index
//   maporder:  Order of the map.  0 = "exact" transport.
//   BeamLine:  Pointer to FNAL BeamLine Stub.
//   bx         Horizontal beta at beginning of element [m]
//   by         Vertical beta at beginning [m]
//   ax         horizontal alpha at beginning
//   ay         vertical alpha at beginning
//   etax       horizontal dispersion at beginning [m]
//   etaPX      d etax/dx at beginning
//   l          length of the element [m]
//   et         element type
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void addFNALMap(const String &name, const Integer &order,
                const Integer &maporder,
                FNALTMs *BeamLine,
                const Integer &BmLIndex,
                const Real &bx, const Real &by,
                const Real &ax, const Real &ay,
                const Real &ex, const Real &epx,
                const Real &l, const String &et)
{
   Integer RingnNodes = Ring::nNodes;
   Integer TransMaps = TransMap::nTransMaps;
   Integer FNALMaps = TransMap::nFNALMaps;
   if (RingnNodes == nodes.size())
      nodes.resize(RingnNodes + Node_Alloc_Chunk);
   if (TransMaps == tMaps.size())
      tMaps.resize(TransMaps + Node_Alloc_Chunk);

   RingnNodes++;

   nodes(RingnNodes-1) = new FNALMap(name, order,
                                     maporder, BeamLine, BmLIndex,
                                     bx, by, ax, ay, ex, epx, l, et);
   TransMaps ++;
   FNALMaps ++;
   tMaps(TransMaps-1) = nodes(RingnNodes-1);

   Ring::nNodes = RingnNodes;
   TransMap::nTransMaps = TransMaps;
   TransMap::nFNALMaps = FNALMaps;

   Ring::nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TransMap::readDIMADFile
//
// DESCRIPTION
//    Reads in 1st order TM info from a DIMAD output file.
//
// PARAMETERS
//    name  - The name of the DIMAD output file to read
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::readDIMADFile(const String &DIMADFile)
{
    Real bxNew, axNew, byNew, ayNew, eta_x, etap_x, nux, nuy, elen, alen;
    Real bxSum = 0., bySum = 0.;
    Real axSum = 0., aySum = 0.;
    String test, elementType;
    Integer p1, p2, line, found = 0;
    char c='\n';
    Integer nMat=0, num;
    Vector(Real) r11, r12, r16, r21, r22, r26, r33, r34, r43, r44,
        r51, r52, r56, bet_x, alph_x, bet_y, alph_y, et_x, etp_x,
        leng, dist;
    Vector(Integer) idel;
    Matrix(Real) R(6,6);
    Integer matrep = 1;

    //IFstream fio("dimad.out", ios::in);
    IFstream fio(DIMADFile, ios::in);
     if(fio.fail())
        {
           fio.close();
           except(badDIMADFile);
        }


// Find Ring Length:
    while (!found)
    {
        line = getline(fio, test, c, 0);
        if(test.contains("TOTAL LENGTH OF MACHINE IS"))
            found=1;
    }
    p1 = test.index(":")+1; p2 = test.index("METERS")-1;
    lRing = atof(test.between(p1,p2));


// Horizontal Matched parameters:
   found = 0;
    while (!found)
    {
        line = getline(fio, test, c, 0);
        if(test.contains("COMPACTION"))
            found=1;
    }

    p1 = test.index("=")+1; p2 = test.index("GAMMA")-1;
    compactionFact = atof(test.between(p1,p2));
    p1 = test.index("TR =")+9; p2 = test.length()-1;
    gammaTrans = atof(test.between(p1,p2));

    line = getline(fio, test, c, 0);
    line = getline(fio, test, c, 0);
    p1 = test.index("NU =")+4; p2 = test.length()-1;
    nuX = atof(test.between(p1,p2));

    line = getline(fio, test, c, 0);
    p1 = test.index("ETA =")+5; p2 = test.index("ETAP")-9;
    etaX = atof(test.between(p1,p2));
    p1 = test.index("ETAP =")+6; p2 = test.length()-1;
    etaPX = atof(test.between(p1,p2));

    line = getline(fio, test, c, 0);
    p1 = test.index("ALPHA =")+7; p2 = test.index("BETA")-9;
    alphaX = atof(test.between(p1,p2));
    p1 = test.index("BETA =")+6; p2 = test.length()-1;
    betaX  = atof(test.between(p1,p2));
    gammaX = (1. + Sqr(alphaX))/betaX;

    line = getline(fio, test, c, 0);
    line = getline(fio, test, c, 0);
    p1 = test.index("CHROMATICITY   =")+16; p2 = test.length()-1;
    chromaticityX = atof(test.between(p1,p2));

   found = 0;
    while (!found)
    {
        line = getline(fio, test, c, 0);
        if(test.contains("VERTICAL"))
            found=1;
    }


    line = getline(fio, test, c, 0);
    line = getline(fio, test, c, 0);
    p1 = test.index("NU =")+4; p2 = test.length()-1;
    nuY = atof(test.between(p1,p2));

    line = getline(fio, test, c, 0);
    p1 = test.index("ETA =")+5; p2 = test.index("ETAP")-9;
    etaY = atof(test.between(p1,p2));
    p1 = test.index("ETAP =")+6; p2 = test.length()-1;
    etaPY = atof(test.between(p1,p2));

    line = getline(fio, test, c, 0);
    p1 = test.index("ALPHA =")+7; p2 = test.index("BETA")-9;
    alphaY = atof(test.between(p1,p2));
    p1 = test.index("BETA =")+6; p2 = test.length()-1;
    betaY = atof(test.between(p1,p2));
    gammaY = (1. + Sqr(alphaY))/betaY;


    line = getline(fio, test, c, 0);
    line = getline(fio, test, c, 0);
    p1 = test.index("CHROMATICITY   =")+16; p2 = test.length()-1;
    chromaticityY = atof(test.between(p1,p2));

// Find start of Matrices, and loop through them:

    while (!fio.eof())
    {
        line = getline(fio, test, c, 0);
        if(test.contains("ELEMENT #"))
            {
             nMat ++;
            if(nMat > r11.rows())
	      {
                r11.resize(nMat+100); r12.resize(nMat+100);
                r16.resize(nMat+100);
                r21.resize(nMat+100); r22.resize(nMat+100);
                r26.resize(nMat+100);
                r33.resize(nMat+100); r34.resize(nMat+100);
                r43.resize(nMat+100);
                r44.resize(nMat+100); r51.resize(nMat+100);
                r52.resize(nMat+100);
                r56.resize(nMat+100); idel.resize(nMat+100),
                alph_x.resize(nMat+100);
                bet_x.resize(nMat+100); alph_y.resize(nMat+100);
                bet_y.resize(nMat+100);
                et_x.resize(nMat+100); etp_x.resize(nMat+100);
                leng.resize(nMat+100);
                dist.resize(nMat+100);
	      }
            idel(nMat) = atoi(test.between(59,63));
            while(!test.contains("FIRST")) line = getline(fio, test, c, 0);
            line = getline(fio, test, c, 0);
            line = getline(fio, test, c, 0);
            r11(nMat) = atof(test.between(2,15));
            r12(nMat) = atof(test.between(17,30));
            r16(nMat) = atof(test.between(77,90));
            line = getline(fio, test, c, 0);
            r21(nMat) = atof(test.between(2,15));
            r22(nMat) = atof(test.between(17,30));
            r26(nMat) = atof(test.between(77,90));
            line = getline(fio, test, c, 0);
            r33(nMat) = atof(test.between(32,45));
            r34(nMat) = atof(test.between(47,60));
            line = getline(fio, test, c, 0);
            r43(nMat) = atof(test.between(32,45));
            r44(nMat) = atof(test.between(47,60));
            line = getline(fio, test, c, 0);
            r51(nMat) = atof(test.between(2,15));
            r52(nMat) = atof(test.between(17,30));
            r56(nMat) = atof(test.between(77,90));

            }
    }

    if (nMat < 1) except(badDIMADFile2);

//  Loop back through file to pick up average info on matrices

    fio.close();
    //fio.open("dimad.out", ios::in);
    fio.open(DIMADFile, ios::in);

    found=0;
    while (!found)
    {
        line = getline(fio, test, c, 0);
        if(test.contains("ELEMENT   #")) found = 1;
    }
    line = getline(fio, test, c, 0);
    line = getline(fio, test, c, 0);
    line = getline(fio, test, c, 0);
    Real bx = atof(test.between(15,21));
    Real ax = atof(test.between(24,30));
    Real by = atof(test.between(33,39));
    Real ay = atof(test.between(42,48));

    Integer id;

    Integer i = 1;
    Real nxFinal, nyFinal;

    found=0;
    while (!found)
    {
        line = getline(fio, test, c, 0);
        if(test.contains("MAXIMUM LATTICE"))
            {
                found = 1;
                break;
            }
        id     = atoi(test.between(9,13));
        bxNew  = atof(test.between(15,22));
        axNew  = atof(test.between(25,31));
        byNew  = atof(test.between(34,40));
        ayNew  = atof(test.between(43,49));
        eta_x  = atof(test.between(51,56));
        etap_x = atof(test.between(58,64));
        nux    = atof(test.between(79,85));
        nuy    = atof(test.between(87,93));
        elen   = atof(test.between(97,101));
        alen   = atof(test.between(104,112));
        bxSum += elen * 0.5*(bxNew+bx);
        bySum += elen * 0.5*(byNew+bx);
        axSum += elen * 0.5*(axNew+ax);
        aySum += elen * 0.5*(ayNew+ax);
        bx = bxNew;
        by = byNew;
        ax = axNew;
        ay = ayNew;
        if(id == idel(i))  // Do matrix element assignments if at one

        {
            bet_x(i) = bxNew;
            alph_x(i) = axNew;
            bet_y(i) = byNew;
            alph_y(i) = ayNew;
            et_x(i) = eta_x;
            etp_x(i) = etap_x;
            dist(i) = alen;
            i++;
            nxFinal = nux;
            nyFinal =  nuy;
       }
    }

    nuX += Integer(nxFinal);  // Add in Integer part
    nuY += Integer(nyFinal);

    betaXAvg = bxSum/dist(nMat);
    betaYAvg = bySum/dist(nMat);

    leng(1) = dist(1);   // Find section lengths
    for (i=2; i<=nMat; i++) leng(i) = dist(i) - dist(i-1);

// Now we're ready to make objects out of the 1st order transfer matrices:
  char num2[10];
  String cons("DIMADTM1");

  for (i=1; i<= nMat; i++)
  {
    num = 10*i;
    sprintf(num2, "(%d)", i);
    R(1,1) = r11(i);  R(1,2) = r12(i); R(1,6) = r16(i);
    R(2,1) = r21(i);  R(2,2) = r22(i); R(2,6) = r26(i);
    R(3,3) = r33(i);  R(3,4) = r34(i);
    R(4,3) = r43(i);  R(4,4) = r44(i);
    R(5,1) = r51(i);  R(5,2) = r52(i); R(5,6) = r56(i);
    elementType = "DMAD";
    addTransferMatrix(cons+num2, num, R,
                      bet_x(i), bet_y(i), alph_x(i), alph_y(i),
                      et_x(i), etp_x(i),
                      leng(i), elementType, matrep);
  }

  fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TransMap::readMADFile
//
// DESCRIPTION
//    Reads in Twiss and matrix info from a MAD run.
//    This routine expects to read MAD "TWISS" and output files.
//
// PARAMETERS
//    MADTwissFile  - The name of the TWISS file to read
//    MADMatrixFile  - The name of the Transfer Matrix file to read
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::readMADFile(const String &MADTwissFile,
                           const String &MADMatrixFile)
{
  String elementName, elementType, name1, test,
         test1, test2, test3, test4, test5, test6;
  Integer i, j, line, num, nRead;
  char c = '\n';
  Integer nElements;
  Real alpha_X,  alpha_Y, beta_X, beta_Y, eta_x, etap_x,
       length, phiX, phiY, sPos;
  char num2[10];
  String cons("MADTM1");
  Matrix(Real) R(6,6);
  Integer matrep = 0;

  MacroPart *mp;
  mp = MacroPart::safeCast
       (mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  // Open the files:

  IFstream fio(MADTwissFile, ios::in);
  if(fio.good() != 1) except(badMADFile);

  IFstream fio2(MADMatrixFile, ios::in);
  if(fio2.good() != 1) except(badMADFile);

  // Find number of elements

  line = getline(fio, test, c, 0);
  nElements = atoi(test.between(59,63));
  // cerr << "nElements = " << nElements << "\n";
  line = getline(fio, test, c, 0);  // This line is empty
  line = getline(fio, test1, c, 0);
  line = getline(fio, test2, c, 0);
  line = getline(fio, test3, c, 0);
  line = getline(fio, test4, c, 0);
  line = getline(fio, test5, c, 0);
  elementName = test1.between(0,10);
  elementType = elementName.excpt(4,7);
  length = atof(test1.between(20,31));

  // set Ring parameters at beginning:
  Ring::alphaX = atof(test3.between(0,15));
  Ring::betaX = atof(test3.between(16,31));
  Ring::alphaY = atof(test4.between(0,15));
  Ring::betaY = atof(test4.between(16,31));
  Ring::etaX = atof(test3.between(48,63));
  Ring::etaPX = atof(test3.between(64,79));
  Ring::etaX = Ring::etaX * mp->_syncPart._betaSync;
  Ring::etaPX = Ring::etaPX * mp->_syncPart._betaSync;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  nRead = 0; // Index for number of Twiss sets read
  num = 10;  // Label index for each transfer matrix we add

  // Loop through files and construct a matrix and
  // twiss parameters for each element

  while(!fio2.eof())
  {
    // find next transfer matrix:
    name1 = "xxx";   // find start of next TM
    while((name1 != "Element transfer matrix") && !fio2.eof())
    {
      line = getline(fio2, test1, c, 0);
      name1 = test1.between(10,32);
    }
    if (fio2.eof()) break;

    for(i = 1; i <= 6; i++)
      for(j = 1; j <= 6; j++) fio2 >> R(i,j);

    // find corresponding twiss parameters

    test6 = "MONI";
    while(test6 == "MONI")
    {
      line = getline(fio, test1, c, 0);
      line = getline(fio, test2, c, 0);
      line = getline(fio, test3, c, 0);
      line = getline(fio, test4, c, 0);
      line = getline(fio, test5, c, 0);
      test6 = test1.between(0,3);
    }

    elementName = test1.between(0,10);
    elementType = elementName.excpt(4,7);
    length = atof(test1.between(20,31));
    if(fio.eof()) except(EOFTwiss);

    alpha_X = atof(test3.between(0,15));
    alpha_Y = atof(test4.between(0,15));
    beta_X = atof(test3.between(16,31));
    beta_Y = atof(test4.between(16,31));
    phiX = atof(test3.between(32,47))/twoPi;
    phiY = atof(test4.between(32,47))/twoPi;
    eta_x = atof(test3.between(48,63));
    etap_x = atof(test3.between(64,79));
    eta_x = eta_x * mp->_syncPart._betaSync;
    etap_x = etap_x * mp->_syncPart._betaSync;
    sPos = atof(test1.between(64,79));  // Don't use this here

    if(test6 != "MARK")
    {
      sprintf(num2, "(%d)", (nRead+1));

      addTransferMatrix(cons + num2, num, R,
                        beta_X, beta_Y, alpha_X, alpha_Y,
                        eta_x, etap_x,
                        length, elementType, matrep);

      num += 10;
      nRead++;
    }
  }
  fio.close();
  fio2.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TransMap::readMADFile2
//
// DESCRIPTION
//    Reads in Twiss and matrix info from a MAD run.
//    This routine expects to read MAD "TWISS" and output files.
//    The transfer matrix file must contain the 1st and 2nd order
//    matrices.
//
// PARAMETERS
//    MADTwissFile  - The name of the TWISS file to read
//    MADMatrixFile  - The name of the Transfer Matrix file to read
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::readMADFile2(const String &MADTwissFile,
                            const String &MADMatrixFile)
{
  String elementName, elementType, name1, test,
         test1, test2, test3, test4, test5, test6;
  Integer i, j, k, line, num, nRead;
  char c = '\n';
  Integer nElements;
  Real alpha_X,  alpha_Y, beta_X, beta_Y, eta_x, etap_x,
       length, phiX, phiY, sPos;
  char num2[10];
  String cons("MADTM2");
  Matrix(Real) R(6,6);
  Array3(Real) T(6,6,6);
  Integer matrep = 0;

  MacroPart *mp;
  mp = MacroPart::safeCast
       (mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  // Open the files:

  IFstream fio(MADTwissFile, ios::in);
  if(fio.good() != 1) except(badMADFile);

  IFstream fio2(MADMatrixFile, ios::in);
  if(fio2.good() != 1) except(badMADFile);

  // Find number of elements

  line = getline(fio, test, c, 0);
  nElements = atoi(test.between(59,63));
  // cerr << "nElements = " << nElements << "\n";
  line = getline(fio, test, c, 0);  // This line is empty
  line = getline(fio, test1, c, 0);
  line = getline(fio, test2, c, 0);
  line = getline(fio, test3, c, 0);
  line = getline(fio, test4, c, 0);
  line = getline(fio, test5, c, 0);
  elementName = test1.between(0,10);
  elementType = elementName.excpt(4,7);
  length = atof(test1.between(20,31));

  // set Ring parameters at beginning:
  Ring::alphaX = atof(test3.between(0,15));
  Ring::betaX = atof(test3.between(16,31));
  Ring::alphaY = atof(test4.between(0,15));
  Ring::betaY = atof(test4.between(16,31));
  Ring::etaX = atof(test3.between(48,63));
  Ring::etaPX = atof(test3.between(64,79));
  Ring::etaX = Ring::etaX * mp->_syncPart._betaSync;
  Ring::etaPX = Ring::etaPX * mp->_syncPart._betaSync;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  nRead = 0; // Index for number of Twiss sets read
  num = 10;  // Label index for each transfer matrix we add

  // Loop through files and construct a matrix and
  // twiss parameters for each element

  while(!fio2.eof())
  {
    // find next transfer matrix:
    name1 = "xxx";   // find start of next TM
    while((name1 != "Element transfer matrix") && !fio2.eof())
    {
      line = getline(fio2, test1, c, 0);
      name1 = test1.between(10,32);
    }
    if (fio2.eof()) break;

    for(i = 1; i <= 6; i++)
      for(j = 1; j <= 6; j++) fio2 >> R(i,j);

    for(k = 1; k <= 6; k++)
    {
      name1 = "xxx";   // find start of next TM
      while (name1 != "Second order terms" && !fio2.eof())
      {
        line = getline(fio2, test1, c, 0);
        name1 = test1.between(10, 27);
      }
      for(i = 1; i <= 6; i++)
	for(j = 1; j <= 6; j++) fio2 >> T(k,i,j);
    }

    // find corresponding twiss parameters

    test6 = "MONI";
    while(test6 == "MONI")
    {
      line = getline(fio, test1, c, 0);
      line = getline(fio, test2, c, 0);
      line = getline(fio, test3, c, 0);
      line = getline(fio, test4, c, 0);
      line = getline(fio, test5, c, 0);
      test6 = test1.between(0,3);
    }

    elementName = test1.between(0,10);
    elementType = elementName.excpt(4,7);
    length = atof(test1.between(20,31));
    if(fio.eof()) except(EOFTwiss);

    alpha_X = atof(test3.between(0,15));
    alpha_Y = atof(test4.between(0,15));
    beta_X = atof(test3.between(16,31));
    beta_Y = atof(test4.between(16,31));
    phiX = atof(test3.between(32,47))/twoPi;
    phiY = atof(test4.between(32,47))/twoPi;
    eta_x = atof(test3.between(48,63));
    etap_x = atof(test3.between(64,79));
    eta_x = eta_x * mp->_syncPart._betaSync;
    etap_x = etap_x * mp->_syncPart._betaSync;
    sPos = atof(test1.between(64,79));  // Don't use this here

    if(test6 != "MARK")
    {
      sprintf(num2, "(%d)", (nRead+1));

      addTransferMatrix2(cons + num2, num, R, T,
                         beta_X, beta_Y, alpha_X, alpha_Y,
                         eta_x, etap_x,
                         length, elementType, matrep);

      num += 10;
      nRead++;
    }
  }
  fio.close();
  fio2.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// TransMap::Uniform_LAT
//
// DESCRIPTION
//    Defines a Uniform Focusing Lattice.
//    The only input required is the argument list in the call.
//
// PARAMETERS
//    LatType - Linac, Ring, or Arc
//    LengthTunes - The length to which the tunes are referenced
//    XTune - Horizontal Tune
//    YTune - Vertical Tune
//    RhoInv - 0 for Linac, 2*PI/LengthTunes for Ring, specify for Arc
//    nElements  - The number of lattice elements
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::Uniform_LAT(String &LatType, Real &LengthTunes,
                           Real &XTune, Real &YTune,
                           Real &RhoInv, Integer &nElements)
{
//  Local variables:


    Integer i, j, k, num, nRead;
    Real alpha_X,  alpha_Y, beta_X, beta_Y, eta_x, etap_x;
    Real length, angleX, angleY, cosX, sinX, cosY, sinY;
    char num2[10];
    String elementType, cons("UniformTM1");
    Matrix(Real) R(6,6);
    Integer matrep = 1;

    lRing = LengthTunes;
    if(LatType == "Linac") RhoInv = 0.;
    if(LatType == "Ring") RhoInv = Consts::twoPi / LengthTunes;
    alpha_X = 0.;
    beta_X = LengthTunes / (Consts::twoPi * XTune);
    alpha_Y = 0.;
    beta_Y = LengthTunes / (Consts::twoPi * YTune);
    eta_x = beta_X * beta_X * RhoInv;
    etap_x = 0.;
    gammaTrans = 1.e+10;
    if(eta_x != 0.)
       gammaTrans = pow(LengthTunes / (Consts::twoPi * eta_x),0.5);

    length = LengthTunes / nElements;
    angleX = length / beta_X;
    angleY = length / beta_Y;
    cosX = cos(angleX);
    sinX = sin(angleX);
    cosY = cos(angleY);
    sinY = sin(angleY);

//  Set Ring parameters at beginning:

    Ring::betaX  = beta_X;
    Ring::betaY  = beta_Y;
    Ring::alphaX = alpha_X;
    Ring::alphaY = alpha_Y;
    Ring::etaX = eta_x;
    Ring::etaPX = etap_x;
    Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
    Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

//  Construct the transfer matrix

    for (i=1; i<=6; i++)
       for (j=1; j<=6; j++) R(i,j) = 0;

    R(1,1) = cosX;
    R(1,2) = beta_X * sinX;
    R(1,6) = eta_x * (1. - cosX);
    R(2,1) = -sinX / beta_X;
    R(2,2) = cosX;
    R(2,6) = eta_x * sinX / beta_X;
    R(3,3) = cosY;
    R(3,4) = beta_Y * sinY;
    R(4,3) = -sinY / beta_Y;
    R(4,4) = cosY;
    R(5,1) = RhoInv * beta_X * sinX;
    R(5,2) = RhoInv * beta_X * beta_X * (1. - cosX);
    R(5,5) = 1;
    R(5,6) = RhoInv * eta_x * (length - beta_X * sinX);
    R(6,6) = 1;


    for (k = 1; k <= nElements; k++)
    {
       nRead = k - 1;
       num = 10 * k;  // Label index for each transfer matrix we add
       sprintf(num2, "(%d)", (nRead+1));
 	elementType = "ULAT";
	addTransferMatrix(cons+num2, num, R,
                          beta_X, beta_Y, alpha_X, alpha_Y,
                          eta_x, etap_x,
                          length, elementType, matrep);
    }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// TransMap::FNALMapLine
//
// DESCRIPTION
//    Sets up an FNAL Beamline library exact transport line.
//    The only input required is the argument list in the call.
//
// PARAMETERS
//    MADFile - File containing line information in MAD input format.
//    MADLine - Name of line being USEd in MADFile
//    maporder - order of maps. Order 0 uses "exact" maps.
//    herd - place to get mass and total energy.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::FNALMapLine(const String &MADFile, const String &MADLine,
                           const Integer &maporder, const Integer &herd)
{
    MacroPart *mp;
    if ( (herd & All_Mask) > nHerds) except (badHerd);
    mp= MacroPart::safeCast(mParts((herd & All_Mask) - 1));
    double E0 = double(mp->_syncPart._e0);
    double ETotal = double(mp->_syncPart._eTotal);
    double Charge = double(mp->_syncPart._charge);

    int nElements, ik;
    double gammaT;
    NodeDat FNode[nEltDim];

    Integer Elements, k, num;
    String elementType, cons("FNAL");
    Real beta_X, beta_Y, alpha_X, alpha_Y, eta_x, etap_x, length;

    char numStr[10];
    FNALTMs *BeamLine;

    char* MADfile = MADFile.cptr();
    char* MADline = MADLine.cptr();

    int map_order = int(maporder);
    BeamLine = new FNALTMs(MADfile, MADline, map_order, E0, ETotal, Charge);
    BeamLine -> getLattice(nElements, gammaT, FNode);

//  Set Ring parameters at beginning:

    Ring::betaX  = Real(FNode[0]._beta_X);
    Ring::betaY  = Real(FNode[0]._beta_Y);
    Ring::alphaX = Real(FNode[0]._alpha_X);
    Ring::alphaY = Real(FNode[0]._alpha_Y);
    Ring::etaX = Real(FNode[0]._eta_x);
    Ring::etaPX = Real(FNode[0]._etap_x);
    Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
    Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

    Real sum = 0.0;
    Elements = Integer(nElements);
    for (k = 1; k <= nElements; k++)
    {
       ik = int(k);
       beta_X = Real(FNode[ik]._beta_X);
       beta_Y = Real(FNode[ik]._beta_Y);
       alpha_X = Real(FNode[ik]._alpha_X);
       alpha_Y = Real(FNode[ik]._alpha_Y);
       eta_x = Real(FNode[ik]._eta_x);
       etap_x = Real(FNode[ik]._etap_x);
       length = Real(FNode[ik]._length);
       elementType = String(FNode[ik]._eltType);
       sum += length;
       sprintf(numStr, "(%d)", k);
       num = 10 * k;  // Label index for each transfer matrix we add
       addFNALMap(cons+numStr, num, maporder, BeamLine, k,
                  beta_X, beta_Y, alpha_X, alpha_Y,
                  eta_x, etap_x, length, elementType);
    }

    gammaTrans = Real(gammaT);
    lRing = sum;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TransMap::startTuneCalc
//
// DESCRIPTION
//    Initializes stuff needed to calculate the particle tunes
//
// PARAMETERS
//    none
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::startTuneCalc()
{

    startTuneCalcBase(mainHerd);
}


Void TransMap::startTuneCalcBase(Integer &herd)
{

    if ( (herd & All_Mask) > Particles::nHerds)
         except (badHerd);
    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1) );
    if(mp->_currentNode != 1) except(notAtRingStart);

    Integer i;
    Integer extraChunk=100;

    if(useSimpleTuneCalc)
     {
      nMacroTunes = mp->_nMacros;
     }
     else tuneCalcOn = 1;

  // Size the storage matrices

    xPhaseOld.resize(mp->_nMacros+extraChunk);
    yPhaseOld.resize(mp->_nMacros+extraChunk);
    xPhaseTot.resize(mp->_nMacros+extraChunk);
    yPhaseTot.resize(mp->_nMacros+extraChunk);
    xTune.resize(mp->_nMacros+extraChunk);
    yTune.resize(mp->_nMacros+extraChunk);
    if(!useSimpleTuneCalc) lTuneTrack.resize(mp->_nMacros+extraChunk);

    lTuneTrack = 0.;
    xPhaseTot = 0.;
    yPhaseTot = 0.;

    // Set up starting phases for existing macros.
    // Assume we're at the start of Ring.

    MapBase *tm = MapBase::safeCast(tMaps(nTransMaps-1));

    for (i=1; i<= mp->_nMacros; i++)
      {
       TMfindPhases(*tm, *mp,i);
       xPhaseOld(i) = TMxPhase;
       yPhaseOld(i) = TMyPhase;
      }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TransMap::stopTuneCalc
//
// DESCRIPTION
//    Stops the particle tune calculation, and wraps up some stuff.
//
// PARAMETERS
//    none
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::stopTuneCalc()
{

    stopTuneCalcBase(mainHerd);

}

Void TransMap::stopTuneCalcBase(Integer &herd)
{
    if ( (herd & All_Mask) > Particles::nHerds)
         except (badHerd);
   MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1) );

   Integer i;
   Real xTune1, yTune1;

   MapBase *tm = MapBase::safeCast(tMaps(nTransMaps-1));

   if(useSimpleTuneCalc)
     {
       if(nMacroTunes != mp->_nMacros) except(tunePartsBad);

       if(mp->_currentNode != 1) except(notAtRingStart);
       for (i=1; i<= mp->_nMacros; i++)
        {
	  TMfindPhases(*tm, *mp,i);
           xTune1 = (xPhaseOld(i) - TMxPhase) / Consts::twoPi;
           if(xTune1 < 0.) xTune1 += 1.;
           xTune(i) = (nTuneTurns*xTune(i) + xTune1)/
                   ( Real(nTuneTurns) + 1.);

           yTune1 = (yPhaseOld(i) - TMyPhase) / Consts::twoPi;
           if(yTune1 < 0.) yTune1 += 1.;
           yTune(i) = (nTuneTurns*yTune(i) + yTune1)/
                   ( Real(nTuneTurns) + 1.);
	}
      nTuneTurns++;
     }

   else
     {
       for(i=1; i<=mp->_nMacros; i++)
         {

           xTune(i) = xPhaseTot(i) * Ring::lRing /
                   (Consts::twoPi * lTuneTrack(i) );
           yTune(i) = yPhaseTot(i) * Ring::lRing /
                   (Consts::twoPi * lTuneTrack(i) );
         }
       tuneCalcOn = 0;
     }

   xTune.resize(mp->_nMacros);
   yTune.resize(mp->_nMacros);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TMfindPhases
//
// DESCRIPTION
//    Finds the betatron X and Y phases
//
// PARAMETERS
//    tm = reference to the TransMatrix1 member
//    mp = reference to the macro-particle main herd
//    i  = index to  the herd member
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TMfindPhases(MapBase &tm, MacroPart& mp, const Integer &i)
{
    Real angle, xval, xpval, yval, ypval;

    // Find normalized coordinates on ellipse transformed to circle:

    xval = (mp._x(i) - 1000. * tm._etaX * mp._dp_p(i) )/sqrt(tm._betaX);
    xpval = (mp._xp(i) - 1000. * tm._etaPX * mp._dp_p(i)) * sqrt(tm._betaX)
             + xval * tm._alphaX;

    yval = mp._y(i)/sqrt(tm._betaY);
    ypval = (mp._yp(i) + mp._y(i) * tm._alphaY/tm._betaY ) * sqrt(tm._betaY);

    angle = atan2(xpval, xval);
    if(angle < 0.) angle += Consts::twoPi;
    TMxPhase = angle;


    angle = atan2(ypval, yval);
    if(angle < 0.) angle += Consts::twoPi;
    TMyPhase = angle;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TMdoTunes
//
// DESCRIPTION
//    Incerments the tunes of a macroParticle herd. It is used as
//    the herd is stepped  through transfer matrices.
//
// PARAMETERS
//    tm = reference to the TransMatrix1 member
//    mp = reference to the macro-particle herd
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TMdoTunes(MapBase &tm, MacroPart &mp)
{
   Integer i;
    Real dPhase;
    Real phiCrit = Consts::pi;

    for(i=1; i<=mp._nMacros; i++)
    {

        TMfindPhases(tm, mp, i);   // find new betatron phase

      // Horizontal Phase:

        dPhase = Particles::xPhaseOld(i) - TMxPhase;
        if(dPhase < (-Consts::pi) ) dPhase += Consts::twoPi;
        //  Subtract out false 0 angle crossings from s.c. "kick-backs":
          if(dPhase > Consts::pi && Particles::xPhaseOld(i) > phiCrit)
          dPhase -= Consts::twoPi;
        Particles::xPhaseTot(i) += dPhase;
        Particles::xPhaseOld(i) = TMxPhase;

      // Vertical phase:

        dPhase = Particles::yPhaseOld(i) - TMyPhase;
        if(dPhase < (-Consts::pi) )
          dPhase += Consts::twoPi;
        //  Subtract out fake 0 angle crossings from s.c. "kick-backs"
        if(dPhase > Consts::pi && Particles::yPhaseOld(i) > phiCrit)
          dPhase -= Consts::twoPi;
        Particles::yPhaseTot(i) += dPhase;
        Particles::yPhaseOld(i) = TMyPhase;

        //Tune tracking length:

        Particles::lTuneTrack(i) += tm._length;
    }
}

//==================================================================
// PTC code interface ==============================================
//==================================================================
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// TransMap::InitPTC
//
// DESCRIPTION
//    Reads the input PTC file and initializes all structures.
//
// PARAMETERS
//   PTC_File  - the flat PTC file
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TransMap::InitPTC(const String &PTC_File)
{

    char* PTC_file = PTC_File.cptr();
    int length_of_name = 128;
    char file_name[length_of_name];
    strncpy(file_name,PTC_file,length_of_name-1);

    double m = 1.;
    int c = +1;
    double enrg = 1.0;

    int nElements = 0;
    double gammaT = 0.;
    double sum;
    int nHarm = 0;

    String elementType("ptc"), cons("PTC");
    Real beta_X, beta_Y, alpha_X, alpha_Y, eta_x, etap_x, length;
    beta_X = 1.0;
    beta_Y = 1.0;
    alpha_X = 1.0;
    alpha_Y = 1.0;
    eta_x = 1.0;
    etap_x = 1.0;

    char numStr[10];


//========= PTC interface ============================
#if defined(USE_PTC)
    ptc_init_(file_name,length_of_name-1);

    //get get alpha,beta, eta at the beginning of the ring
    ptc_get_twiss_init_(&beta_X,&beta_Y,&alpha_X,&alpha_Y,&eta_x,&etap_x);

    //get energy, mass and charge of the sync part.
    ptc_get_syncpart_(&m,&c,&enrg);

    //get number of PTC ORBIT nodes, harmonic number, length of the ring, and gamma transition
    ptc_get_ini_params_(&nElements,&nHarm,&sum,&gammaT);


    Ring::betaX  = beta_X;
    Ring::betaY  = beta_Y;
    Ring::alphaX = alpha_X;
    Ring::alphaY = alpha_Y;
    Ring::etaX = eta_x;
    Ring::etaPX = etap_x;
    Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
    Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;
    Ring::gammaTrans = Real(gammaT);
    Ring::harmonicNumber = nHarm;
    Ring::lRing = sum;


    sum = 0.;
    for (int i = 0; i < nElements; i++)
    {
       //call interface function to get alpha,beta, eta, length
       ptc_get_twiss_for_node_(&i,&length,&beta_X,&beta_Y,&alpha_X,&alpha_Y,&eta_x,&etap_x);
       sum += length;
       sprintf(numStr, "(%d)", i);
       int num = 10 * i;  // Label index for each transfer matrix we add
       addPTC_Map(cons+numStr, num, i,
                  beta_X, beta_Y, alpha_X, alpha_Y,
                  eta_x, etap_x, length, elementType);
    }

    //check if sum != Ring::lRing

#else
    std::cout<< "================================================\n";
    std::cout<< "You have to compile ORBIT with USE_PTC option!  \n";
    std::cout<< "          ============STOP===========           \n";
    std::cout<< "================================================\n";
    std::exit(1);
#endif   //USE_PTC

    Particles::addSyncPart(m,c,enrg);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// TransMap::readAccelerationTableForPTC
//
// DESCRIPTION
//    Reads the accelerarion table for PTC
///////////////////////////////////////////////////////////////////////////

Void TransMap::readAccelerationTableForPTC(const String &PTC_acc_File)
{

    char* PTC_acc_file = PTC_acc_File.cptr();
    int length_of_name = 128;
    char file_name[length_of_name];
    strncpy(file_name,PTC_acc_file,length_of_name-1);

//========= PTC interface ============================
#if defined(USE_PTC)
    ptc_read_accel_table_(file_name,length_of_name-1);
#else
    std::cout<< "================================================\n";
    std::cout<< "You have to compile ORBIT with USE_PTC option!  \n";
    std::cout<< "          ============STOP===========           \n";
    std::cout<< "================================================\n";
    std::exit(1);
#endif   //USE_PTC

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// TransMap::ReadPTCscript
//
// DESCRIPTION
//    Reads the PTC commands from external file and executes them
///////////////////////////////////////////////////////////////////////////

Void TransMap::ReadPTCscript(const String &PTC_File)
{
    char* PTC_file = PTC_File.cptr();
    int length_of_name = 128;
    char file_name[length_of_name];
    strncpy(file_name,PTC_file,length_of_name-1);

//========= PTC interface ============================
#if defined(USE_PTC)
    ptc_script_(file_name,length_of_name-1);

    TransMap::UpdateTwissFromPTC();
#else
    std::cout<< "================================================\n";
    std::cout<< "You have to compile ORBIT with USE_PTC option!  \n";
    std::cout<< "          ============STOP===========           \n";
    std::cout<< "================================================\n";
    std::exit(1);
#endif   //USE_PTC

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// TransMap::UpdateTwissFromPTC
//
// DESCRIPTION
//    Updates ORBIT Twiss parameters from PTC.
//    It could be needed after acceleration or reading the PTC script.
///////////////////////////////////////////////////////////////////////////

Void TransMap::UpdateTwissFromPTC()
{
//========= PTC interface ============================
#if defined(USE_PTC)

    double m = 1.;
    int c = +1;
    double enrg = 1.0;

    Real beta_X, beta_Y, alpha_X, alpha_Y, eta_x, etap_x, length;

    //get get alpha,beta, eta at the beginning of the ring
    ptc_get_twiss_init_(&beta_X,&beta_Y,&alpha_X,&alpha_Y,&eta_x,&etap_x);

    //get energy, mass and charge of the sync part.
    ptc_get_syncpart_(&m,&c,&enrg);

    Particles::updateSyncPart(m,c,enrg);

    int nElements = 0;
    double gammaT = 0.;
    double sum;
    int nHarm = 0;

    //get number of PTC ORBIT nodes, harmonic number, length of the ring, and gamma transition
    ptc_get_ini_params_(&nElements,&nHarm,&sum,&gammaT);

    Ring::betaX  = beta_X;
    Ring::betaY  = beta_Y;
    Ring::alphaX = alpha_X;
    Ring::alphaY = alpha_Y;
    Ring::etaX = eta_x;
    Ring::etaPX = etap_x;
    Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
    Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;
    Ring::gammaTrans = Real(gammaT);
    Ring::harmonicNumber = nHarm;
    Ring::lRing = sum;

    if(TransMap::nTransMaps != nElements){
          std::cout<< "================================================         \n";
	  std::cout<< "PTC and ORBIT have different number of ORBIT_PTC nodes!  \n";
	  std::cout<< "This could be result of PTC script execution.            \n";
	  std::cout<< "          ============STOP===========                    \n";
	  std::cout<< "================================================         \n";
	  std::exit(1);
    }

    for (int i = 0; i < nElements; i++)
    {
       //call interface function to get alpha,beta, eta, length
       ptc_get_twiss_for_node_(&i,&length,&beta_X,&beta_Y,&alpha_X,&alpha_Y,&eta_x,&etap_x);

       MapBase *tm = MapBase::safeCast(tMaps(i));
       tm->_betaX = beta_X;
       tm->_betaY = beta_Y;
       tm->_alphaX = alpha_X;
       tm->_alphaY = alpha_Y;
       tm->_etaX = eta_x;
       tm->_etaPX = etap_x;
    }

    Ring::nodesInitialized = 0;
#else
    std::cout<< "================================================\n";
    std::cout<< "You have to compile ORBIT with USE_PTC option!  \n";
    std::cout<< "          ============STOP===========           \n";
    std::cout<< "================================================\n";
    std::exit(1);
#endif   //USE_PTC
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// TransMap::SynchronousSetPTC
//
// DESCRIPTION
//    Calls the ptc_synchronous_set method of the PTC code.
//    The index should be negative.
///////////////////////////////////////////////////////////////////////////

Void TransMap::SynchronousSetPTC(const Integer &i_val)
{
//========= PTC interface ============================
#if defined(USE_PTC)
  int i = i_val;
  if(i >= 0){
    std::cout<< "================================================         \n";
    std::cout<< "TransMap::SynchronousSetPTC                              \n";
    std::cout<< "You can not call this method with positive parameter!    \n";
    std::cout<< "          ============STOP===========                    \n";
    std::cout<< "================================================         \n";
    std::exit(1);
  }
  ptc_synchronous_set_(&i);
#else
    std::cout<< "================================================\n";
    std::cout<< "You have to compile ORBIT with USE_PTC option!  \n";
    std::cout<< "          ============STOP===========           \n";
    std::cout<< "================================================\n";
    std::exit(1);
#endif   //USE_PTC
}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// TransMap::SynchronousAfterPTC
//
// DESCRIPTION
//    Calls the ptc_synchronous_after method of the PTC code.
//    The index should be negative.
///////////////////////////////////////////////////////////////////////////

Void TransMap::SynchronousAfterPTC(const Integer &i_val)
{
//========= PTC interface ============================
#if defined(USE_PTC)
  int i = i_val;
  if(i >= 0){
    std::cout<< "================================================         \n";
    std::cout<< "TransMap::SynchronousAfterPTC                            \n";
    std::cout<< "You can not call this method with positive parameter!    \n";
    std::cout<< "          ============STOP===========                    \n";
    std::cout<< "================================================         \n";
    std::exit(1);
  }
  ptc_synchronous_after_(&i);
#else
    std::cout<< "================================================\n";
    std::cout<< "You have to compile ORBIT with USE_PTC option!  \n";
    std::cout<< "          ============STOP===========           \n";
    std::cout<< "================================================\n";
    std::exit(1);
#endif   //USE_PTC
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS PTC_Map
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(PTC_Map, MapBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   PTC_Map::PTC_Map
//
// DESCRIPTION
//
//   Constructor for transport map that delegates tracking to the PTC.
//
// PARAMETERS
//   ..
//
///////////////////////////////////////////////////////////////////////////

PTC_Map::PTC_Map(const String &n, const Integer &order,
            const Integer &orbit_ptc_node_inde_in,
            const Real &bx, const Real &by,
            const Real &ax, const Real &ay,
            const Real &ex, const Real &epx,
            const Real &l, const String &et):
            MapBase(n,order,bx,by,ax,ay,ex,epx,l,et)
{
  orbit_ptc_node_index = orbit_ptc_node_inde_in;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   PTC_Map::NodeCalculator
//
// DESCRIPTION
//   Does not do anything if tune tracking is not activated.
//   Otherwise, checks to see if any new particles have appeared
//   since the last tune tracking initiation, and does appropriate
//   initializations for them.
//
// PARAMETERS
//   mp = reference to the herd being tracked.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void PTC_Map::_nodeCalculator(MacroPart &mp)
{

  //call to the ptc code to figure out what to do
  //  i_task = 0 - do not do anything
  //  i_task = 1 - energy of the sync. particle changed

//========= PTC interface ============================
#if defined(USE_PTC)

  int action_type = -1;
  ptc_get_task_type_(&orbit_ptc_node_index,&action_type);
  if(action_type != 0){
    if(action_type == 1){
      //The energy of the sync particle changed!!! Do something!

      double m,kin_e;
      int c;

      //get energy, mass and charge of the sync part.
      ptc_get_syncpart_(&m,&c,&kin_e);

      //frequency
      double omega;
      ptc_get_omega_(&omega);


      //Actions should be defined. ?????
      std::cout<<"Stop ORBIT execution. \n"
	       <<"The actions on the energy change had not been defined yet! \n"
	       <<"STOP\n";
      std::exit(1);

    }
  }

#else
    std::cout<< "================================================\n";
    std::cout<< "You have to compile ORBIT with USE_PTC option!  \n";
    std::cout<< "          ============STOP===========           \n";
    std::cout<< "================================================\n";
    std::exit(1);
#endif   //USE_PTC

  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    MapBase *tm = MapBase::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TMfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TMxPhase;
      Particles::yPhaseOld(i) = TMyPhase;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   PTC_Map::updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle with map from the PTC tracking.
//   If the tune claculation is on, calculate the betatron phase advance
//   through this matrix element.
//
// PARAMETERS
//   mp = reference to the herd being traked.
//   i  = macro-particle index in the herd.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void PTC_Map::_updatePartAtNode(MacroPart& mp)
{
  Integer I;
  int nMacros;
  double x_map;
  double xp_map;
  double y_map;
  double yp_map;
  double phi_map;
  double deltaE_map;

  if(_length < 0.0) return;

#if defined(USE_PTC)
  ptc_synchronous_set_(&orbit_ptc_node_index);
#else
#endif   //USE_PTC

  nMacros = mp._nMacros;
  for(int i = 0; i < nMacros; i++)
  {
    I = Integer(i+1);
    x_map = double(mp._x(I));
    xp_map = double(mp._xp(I));
    y_map = double(mp._y(I));
    yp_map = double(mp._yp(I));
    phi_map = double(mp._phi(I));
    deltaE_map = double(mp._deltaE(I));

//========= PTC interface ============================
#if defined(USE_PTC)
    //track one particle
    ptc_track_particle_(&orbit_ptc_node_index, &x_map, &xp_map,&y_map, &yp_map,&phi_map, &deltaE_map);
#else
    std::cout<< "================================================\n";
    std::cout<< "You have to compile ORBIT with USE_PTC option!  \n";
    std::cout<< "          ============STOP===========           \n";
    std::cout<< "================================================\n";
    std::exit(1);
#endif   //USE_PTC

    mp._x(I) = x_map;
    mp._xp(I) = xp_map;
    mp._y(I) = y_map;
    mp._yp(I) = yp_map;
    mp._phi(I) = phi_map;
    mp._deltaE(I) = deltaE_map;
    mp._dp_p(I) = mp._deltaE(I) * mp._syncPart._dppFac;
  }


#if defined(USE_PTC)
  ptc_synchronous_after_(&orbit_ptc_node_index);
#else
#endif   //USE_PTC

  for(int i = 0; i < nMacros; i++)
  {
    I = Integer(i+1);
    if(Abs(mp._phi(I)) > Consts::pi)
    {
      Real sign = -mp._phi(I) / Abs(mp._phi(I));
      mp._phi(I) = Consts::pi * sign + fmod(mp._phi(I),Consts::pi);
    }
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TMdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   addPTC_Map
//   Note:  This is a local routine.
//
// DESCRIPTION
//   Adds an PTC_Map object
//
// PARAMETERS
//   name:      Name for the TM
//   order:     Order index
//   orbit_ptc_node_inde_in - ptc node index
//   bx         Horizontal beta at beginning of element [m]
//   by         Vertical beta at beginning [m]
//   ax         horizontal alpha at beginning
//   ay         vertical alpha at beginning
//   etax       horizontal dispersion at beginning [m]
//   etaPX      d etax/dx at beginning
//   l          length of the element [m]
//   et         element type
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void addPTC_Map(const String &name, const Integer &order,
                const Integer &orbit_ptc_node_inde_in,
                const Real &bx, const Real &by,
                const Real &ax, const Real &ay,
                const Real &ex, const Real &epx,
                const Real &l, const String &et)
{
   Integer RingnNodes = Ring::nNodes;
   Integer TransMaps = TransMap::nTransMaps;
   if (RingnNodes == nodes.size())
      nodes.resize(RingnNodes + Node_Alloc_Chunk);
   if (TransMaps == tMaps.size())
      tMaps.resize(TransMaps + Node_Alloc_Chunk);

   RingnNodes++;

   nodes(RingnNodes-1) = new PTC_Map(name, order,
                                     orbit_ptc_node_inde_in,
                                     bx, by, ax, ay, ex, epx, l, et);
   TransMaps ++;
   tMaps(TransMaps-1) = nodes(RingnNodes-1);

   Ring::nNodes = RingnNodes;
   TransMap::nTransMaps = TransMaps;

   Ring::nodesInitialized = 0;
}
