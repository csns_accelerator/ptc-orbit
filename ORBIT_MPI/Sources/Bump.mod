/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Bump.mod
//
// AUTHOR
//    John Galambos,  ORNL, jdg@ornl.gov
//
// CREATED
//    12/15/97
//
//  DESCRIPTION
//    Module descriptor file for the Bump module. This module contains
//    information about ideal bumps.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Bump - "Bump Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, Particles, Ring, Injection;

Errors
   badInterpSizes - "Oops - there's a bad vector size sent to "
                  - "interpolateBumps(). Check x,x',y,y',t sizes",
   badBumpTimes   - "Something is fishy with the values in bumpTimes";

public:

  Void ctor() 
                  - "Constructor for the Bump  module."
                  - "Initializes build values.";

  Integer
   nIdealBumps    - "Number of Ideal Bumps in the Ring";

  Real 
  xIdealBump      - "The x value of the ideal bump at a point in time (mm)",
  xPIdealBump     - "The x prime of the ideal bump at a point in time (mrad)",
  yIdealBump      - "The y value of the ideal bump at a point in time (mm)",
  yPIdealBump     - "The y prime of the ideal bump at a point in time (mrad)";

  Void addIdealBump(const String &name, const Integer &order, 
    const Integer &upDown,  const Subroutine sub)
			- "Add a Bump Node";

 Integer 
  bumpOn         - "Switch indicating whether ideal bump is on (==1)";

// Stuff used in e-fold bump scaling:

 Real 
  eFoldTimeX      - "Normailze e-fold time for x bump",
  eFoldTimeY      - "Normailze e-fold time for y bump",
  tBump0          - "Time for start of bump [m-sec]",
  tBumpF          - "Time at end of bump [m-sec]",
  xBump0          - "X Bump at start of bump [mm]",
  xBumpF          - "Final X Bump at end of bump [mm]",
  xPBump0         - "X-prime Bump at start of bump [mrad]",
  yBump0          - "Y Bump at start of bump [mm]",
  yBumpF          - "Final Y Bump at end of bump [mm]",
  yPBump0         - "Y-prime Bump at start of bump [mrad]",
  yBump1          - "Y Bump ramped magnitude at start of bump [mm]",
  xBump1          - "X Bump ramped magnitude at start of bump [mm]",
  yPBump1         - "Y' Bump ramped magnitude at start of bump [mrad]",
  xPBump1         - "X' Bump ramped magnitude at start of bump [mrad]";

  Void eFoldBump()  - "Routine used to apply an e-fold bump scheme"
                    - "with specified start and end points";
  Void eFoldBump2()  - "Routine used to apply an e-fold bump scheme"
                    - "with specified fixed and ramped magnitudes";

// Stuff used in the linear interpolation Bump scheme:


   RealVector
    bumpTimes      - "Vector containing the bump times [msec]",
    xBumpPoints    - "Vector containing the x bump points [mm]",
    xPBumpPoints   - "Vector containing the x' bump points [mrad]",
    yBumpPoints    - "Vector containing the y bump points [mm]",
    yPBumpPoints   - "Vector containing the y' bump points [mrad]";

  Void interpolateBumps()  - "Routine to do linear interpolation of"
                           - " the bumps from input vectors";
   
  Void sizeBumpPoints(const Integer &np)
                           - "Sizes vectors used in iterpolating bump "
                           - "points, see routine interpolateBumps";

}
