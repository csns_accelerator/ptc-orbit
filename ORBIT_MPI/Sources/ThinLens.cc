/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    ThinLens.cc
//
// AUTHOR
//    John Galambos, Sarah Cousineau, & Jeff Holmes
//    ORNL, jdg@ornl.gov
//
// CREATED
//    11/10/98
//
//  DESCRIPTION
//    Module descriptor file for the ThinLens module. This module 
//    contains source for the ThinLens related info.
//
//  REVISION HISTORY
//    BIG kicker added 07/31/00
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "ThinLens.h"
#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "StreamType.h"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

Array(ObjectPtr) thinLensPointers;
extern Array(ObjectPtr) mParts, nodes;

#define PI 3.1415926539

///////////////////////////////////////////////////////////////////////////
//
// Local routines:
//
///////////////////////////////////////////////////////////////////////////

Integer TLfactorial(Integer n)
{
   return (n>1) ? n*TLfactorial(n-1) :1;
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   ThinLensBase 
//
// INHERITANCE RELATIONSHIPS
//    ThinLensBase  -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a base class for storing ThinLens information. 
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class ThinLensBase : public Node {
  Declare_Standard_Members(ThinLensBase, Node);
public:
    ThinLensBase(const String &n, const Integer &order):
            Node(n, 0., order)
            
        {           
        }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Void _showThinLens(Ostream &sout)=0;

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS ThinLensBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(ThinLensBase, Node);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   IntegLens2D 
//
// INHERITANCE RELATIONSHIPS
//    IntegLens2D  -> ThinLensBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing 2-D IntegrableLens information. 
//
// PUBLIC MEMBERS
//    Real &_b     - linear coefficient (mm-1)
//    Real &_a     - non-linear coefficient (mm-2)
//
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class IntegLens2D : public ThinLensBase {
  Declare_Standard_Members(IntegLens2D, ThinLensBase);
public:
    IntegLens2D(const String &n, const Integer &order, 
          const Real &a, const Real &b):
            ThinLensBase(n, order), _a(a), _b(b)
            
        {           
        }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showThinLens(Ostream &sout);

    const Real _a;
    const Real _b;

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS IntegLens2D
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(IntegLens2D, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    IntegLens2D::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void IntegLens2D::_nodeCalculator(MacroPart &mp)
{    
 
}

Void IntegLens2D::_showThinLens(Ostream &sout)
{    
  sout << "2-D Integrable Thin Lens:"  << "\n";
  sout << " Linear coefficient b [mm-1] " << _b << "\n";
  sout << " Non-linear coefficient a [mm-2] " << _a << "\n";
  sout << " Position in ring [m] " << _position << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    IntegLens2D::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//     a MacroParticle with a 2-D IntegrableLens
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void IntegLens2D::_updatePartAtNode(MacroPart& mp)
{
    Real rTrans2;

    Integer j;
     
    for(j=1; j<=mp._nMacros; j++)
    {
 
      rTrans2 = (Sqr(mp._x(j)) +  Sqr(mp._y(j)) );
      mp._xp(j) -= _b * mp._x(j)/(1 + _a * rTrans2 );
      mp._yp(j) -= _b * mp._y(j)/(1 + _a * rTrans2 );
    }
  
}






///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
// BIGKickLens
//
// INHERITANCE RELATIONSHIPS
//   BIGKickLens -> ThinLensBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing BIG kicker information.                  
//
// PUBLIC MEMBERS
//                                                                           
// Real _kl      The strength of the kick in mrad, at reference energy. 
// Integer _size The size of the kick array
// Real _dPhi    The rise and fall time of the kicker (in radians)
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

class BIGKickLens : public ThinLensBase {
  Declare_Standard_Members(BIGKickLens, ThinLensBase);
public:
    BIGKickLens(const String &name, const Integer &order,
		const Integer &direction, Vector(Real) &kl, 
		const Integer &size, const Real &dPhi):
      ThinLensBase(name, order), _direction(direction), _kl(kl), 
      _size(size), _dPhi(dPhi)
        {           
        }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showThinLens(Ostream &sout);

    const Integer _direction, _size;
    Vector(Real) _kl;
    const Real _dPhi;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BIGKickLens
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BIGKickLens, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BIGKickLens::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void BIGKickLens::_nodeCalculator(MacroPart &mp)
{    
 
}

Void BIGKickLens::_showThinLens(Ostream &sout)
{    
  sout << "Big Kick Lens:"  << _name << "\n";
  sout << " Position in ring [m] " << _position << "\n";
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    BIGKickLens::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//     a MacroParticle with a BIG kicker. 
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void BIGKickLens::_updatePartAtNode(MacroPart& mp)
{
    Integer i,j;
    static Integer index;
    static Integer BIG_counter=1;

    if(index==0) index=1;
    if(index==_size+1) index=1;

    if(_direction == 1){
      for(j=1; j<=mp._nMacros; j++)
	{
	  if(mp._phi(j) < (-PI + _dPhi)){
	    mp._xp(j) += _kl(index)/_dPhi * (PI + mp._phi(j));
	  }
	  if(mp._phi(j) > (PI - _dPhi)){
	    mp._xp(j) += _kl(index)/_dPhi * (PI - mp._phi(j));
	  }
	  if( (mp._phi(j) > (-PI + _dPhi)) && (mp._phi(j) < (PI - _dPhi)) ){
	    mp._xp(j) += _kl(index);
	  }
	}
    }
    if(_direction == 2){
      for(j=1; j<=mp._nMacros; j++)
	{
	  if(mp._phi(j) < (-PI + _dPhi)){
	    mp._yp(j) += _kl(index)/_dPhi * (PI + mp._phi(j));
	  }
	  if(mp._phi(j) > (PI - _dPhi)){
	    mp._yp(j) += _kl(index)/_dPhi * (PI - mp._phi(j));
	  }
	  if( (mp._phi(j) > (-PI + _dPhi)) && (mp._phi(j) < (PI - _dPhi)) ){
	    mp._yp(j) += _kl(index);
	  }
	}
    }
    if(_direction != 1 && _direction != 2){
      cerr<<"Error: Invalid kicker direction entered.\n";
    }
    
    if(BIG_counter == ThinLens::nBIGKickLens){
      index++;
      BIG_counter=1;
    }
    else
      BIG_counter++;
   
}


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
// InflateLens
//
// INHERITANCE RELATIONSHIPS
//   InflateLens -> ThinLensBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing InflateLens information.                  
//
// PUBLIC MEMBERS
//                                                                           
// Real _factor   The factor to multiply x, x', y, y' by on every turn. 
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

class InflateLens : public ThinLensBase {
  Declare_Standard_Members(InflateLens, ThinLensBase);
public:
    InflateLens(const String &name, const Integer &order, 
           const Real &factor):
      ThinLensBase(name, order), _factor(factor)
        {           
        }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showThinLens(Ostream &sout);

    const Real _factor;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS InflateLens
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(InflateLens, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    InflateLens::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void InflateLens::_nodeCalculator(MacroPart &mp)
{    
 
}

Void InflateLens::_showThinLens(Ostream &sout)
{    
  sout << "Inflate Lens:"  << _name << "\n";
  sout << " Position in ring [m] " << _position << "\n";
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    InflateLens::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//     a MacroParticle with an phase space multiplication factor. 
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void InflateLens::_updatePartAtNode(MacroPart& mp)
{
    Integer j;
    
    for(j=1; j<=mp._nMacros; j++)
    { 
      mp._x(j)  *= _factor;
      mp._xp(j) *= _factor;
      mp._y(j)  *= _factor;
      mp._yp(j) *= _factor;
    }
    
}


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
// ThinMPole
//
// INHERITANCE RELATIONSHIPS
//    ThinMPole  -> ThinLensBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing general thin multipole lens information. 
//
// PUBLIC MEMBERS
//    Integer _n   The order of the multipole (0=dipole, 1=quad, 2=sext, ..)
//    Real _kl     The integrated strength of the field expansion (1/m^_n)
//                 where the field expension is 1/(1/B*rho)*dB_y/(dx)^n at
//                 x=0.
//    Integer _skew Switch, if ==1, pole is rotated by pi/(2*n+2)
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class ThinMPole : public ThinLensBase {
  Declare_Standard_Members(ThinMPole, ThinLensBase);
public:
    ThinMPole(const String &name, const Integer &order, 
          const Integer &n, const Real &kl, const Integer &sk):
            ThinLensBase(name, order), _n(n), _kl(kl), _skew(sk)
        {           
        }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showThinLens(Ostream &sout);

    const Real _kl;
    const Integer _n, _skew;

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS ThinMPole
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(ThinMPole, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    ThinMPole::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void ThinMPole::_nodeCalculator(MacroPart &mp)
{    
 
}

Void ThinMPole::_showThinLens(Ostream &sout)
{    
  sout << "Thin Multipole Lens:"  << _name << "\n";
  sout << " Multipole order n =  " << _n << "\n";
  sout << " K*L factor [m^-n] " << _kl << "\n";
  if(_skew) sout << " Lens is skewed \n";
  sout << " Position in ring [m] " << _position << "\n";
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    ThinMPole::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//     a MacroParticle with a 2-D IntegrableLens. This kick follows 
//     the formalism described in thye ACCSIM manual.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void ThinMPole::_updatePartAtNode(MacroPart& mp)
{
    Complex z, zn;
    Integer i, j;
     
    for(j = 1; j <= mp._nMacros; j++)
    {
      z = Complex(1.e-3 * mp._x(j), 1.e-3 *mp._y(j));
          // in meters since pole strength is in meters

      // take power of z to the _n:

      zn = Complex(1., 0.);
      for (i = 0; i < _n; i++)
      {
        zn = zn *  z;
      }

      // MAD Conventions on signs of multipole terms

      if(_skew)
      {
        mp._xp(j) += 1.e3 * _kl/TLfactorial(_n) * zn.im;
        mp._yp(j) += 1.e3 * _kl/TLfactorial(_n) * zn.re;
      }
      else
      {
        mp._xp(j) -= 1.e3 * _kl/TLfactorial(_n) * zn.re;
        mp._yp(j) += 1.e3 * _kl/TLfactorial(_n) * zn.im;
      }
    }
}


///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE ThinLens
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    ThinLens::ctor
//
// DESCRIPTION
//    Initializes the various ThinLens related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void ThinLens::ctor()
{

// set some initial values

    nThinLens = 0;
    nBIGKickLens = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  ThinLens::addIntegrableLens2D
//
// DESCRIPTION
//    Adds a 2D Integrable Lens kick. The kick goes as:
//    ~ a * r/(1 + b * r^2)
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    a - non linear coefficient (mm-2)
//    b - linear coefficient (mm-1)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void ThinLens::addIntegrableLens2D(const String &name, 
          const Integer &order,const  Real &a, const Real &b)

{

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nThinLens == thinLensPointers.size())
      thinLensPointers.resize(nThinLens + 1);

   nNodes++;
  
   nodes(nNodes-1) = new IntegLens2D(name, order, a, b);
   nThinLens++;
   
   thinLensPointers(nThinLens -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  ThinLens::addBIGKickLens
//
// DESCRIPTION
//    Adds a BIG kicker
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    direction: direction to kick in (1 for horizontal, 2 for vertical)
//    kl   : array of kicking strengths [mrad]
//    size : size of kicking array
//    dPhi : rise and fall time of kicker (in radians)
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void ThinLens::addBIGKickLens(const String &name, const Integer &order, 
     const Integer &direction, Vector(Real) &kl, const Integer &size, 
     const Real &dPhi)

{

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nThinLens == thinLensPointers.size())
      thinLensPointers.resize(nThinLens + 1);

   nNodes++;

   nodes(nNodes-1) = new BIGKickLens(name, order, direction, 
				     kl, size, dPhi);
   nThinLens++;
   nBIGKickLens++;
   
   thinLensPointers(nThinLens -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  ThinLens::addInflateLens
//
// DESCRIPTION
//    Adds an routine to inflate the particle emittance.
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    factor:  factor to multiply the phase space coords. by on 
//             every turn.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void ThinLens::addInflateLens(const String &name, 
     const Integer &order, const Real &factor)

{

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nThinLens == thinLensPointers.size())
      thinLensPointers.resize(nThinLens + 1);

   nNodes++;

   nodes(nNodes-1) = new InflateLens(name, order, factor);
   nThinLens++;
   
   thinLensPointers(nThinLens -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  ThinLens::addThinLens
//
// DESCRIPTION
//    Adds a thin lens multipole element
//
// PARAMETERS
//    name:    Name for this node
//    order:   node order index
//    n    :   mulitpole order. 0 = dipole, 1=quad, 2=sextupole, 3=octupole,...
//    kl   : integrated strength (1/m^n). This is the integral of "k" =
//           1/(B*rho) * dB_y/(dx)^n at x = 0 over the element length "l"
//    skew - switch to use a skew element. If  == 1, the thin lens is rotated
//           by pi/(2*n + 2)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void ThinLens::addThinLens(const String &name, 
     const Integer &order,const  Integer &n, const Real &kl, const Integer &sk)

{

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nThinLens == thinLensPointers.size())
      thinLensPointers.resize(nThinLens + 1);

   nNodes++;

   nodes(nNodes-1) = new ThinMPole(name, order, n, kl, sk);
   nThinLens++;
   
   thinLensPointers(nThinLens -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}




///////////////////////////////////////////////////////////////////////////
//
// NAME
//    ThinLens::showThinLens
//
// DESCRIPTION
//    Prints out the settings of the active thin lenses
//
// PARAMETERS
//    sout - The stream to send output to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void ThinLens::showThinLens(Ostream &sout)
{

  ThinLensBase *tlp;

  if (nThinLens == 0)
    {
      sout << "\n No Thin Lenses are added \n\n";
      return;
    }

   sout << "/n/n Thin Lens present in ring:/n/n";

   for (Integer i=1; i<= nThinLens; i++)
     {

        tlp = ThinLensBase::safeCast(thinLensPointers(i-1));
        tlp->_showThinLens(sout);
     }
}











