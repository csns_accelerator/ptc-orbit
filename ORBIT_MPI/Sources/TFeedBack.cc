////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    TFeedBack.cc
//
// AUTHOR
//    Slava Danilov, Jeff Holmes, John Galambos
//    ORNL, vux@ornl.gov
//
// CREATED
//    6/11/2002
//
//  DESCRIPTION
//    Module descriptor file for the Transverse Impedance module.
//   This module contains source for the TImpedance related info.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "TFBack.h"
#include "TFeedBack.h"
#include "Node.h"
#include "Particles.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "ComplexMat.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>

//===MPI stuff =
#include "mpi.h"

using namespace std;

/// STATIC DEFINITIONS

Array(ObjectPtr) TFeedBackPointers;
extern Array(ObjectPtr) mParts, nodes;
extern Array(ObjectPtr) syncP;


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TImpedance
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(TFBack, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    TFBack::TFBack, ~TFBack
//
// DESCRIPTION
//    Constructor and destructor for TFBack class
//
// PARAMETERS
//    None.
//
// RETURNS
//
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

TFBack::TFBack(const String &n, const Integer &order, 
                 Integer &nBins):
            Node(n, 0., order), _nBins(nBins)
{
   _meshCharge.resize(_nBins+1);
   _xCentroid.resize(_nBins+1);
   _xpCentroid.resize(_nBins+1);
   _yCentroid.resize(_nBins+1);
   _ypCentroid.resize(_nBins+1); 
   _phiCount.resize(_nBins+1);
   _forceCalculated = 0;  

   //===MPI stuff =====start=====
   iMPIini_=0;
   MPI_Initialized(&iMPIini_);
   nMPIsize_ = 1;
   nMPIrank_ = 0;
    if(iMPIini_ > 0){
       MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
       MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
     }
    if( nMPIsize_ > 1){
      //buffers to conform x,xp,y,and yp centroids
      buff_MPI_0_ = (double *) malloc ( sizeof(double)*4*(_nBins+1));
      buff_MPI_1_ = (double *) malloc ( sizeof(double)*4*(_nBins+1));
    }
   //===MPI stuff =====stop======   
            
}

TFBack::~TFBack()
{     
 if( nMPIsize_ > 1){
   free(buff_MPI_0_);
   free(buff_MPI_1_);
 }
}
 

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TFBack::_longBin
//
// DESCRIPTION
//    Bins the macro particles
//    and their transverse
//    displacements longitudinally, and calculates the dipole moments
//    of every bin.
//
//    _phiCount(i) is the number of macros in bin i.
//
// PARAMETERS
//    mp - the macro herd to operate on.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TFBack::_longBin(MacroPart &mp)
{
    Integer nBinsP1 = _nBins+1;
    Real w, phiFactor;
    Integer i, iLocal, nMacros;
    Real position;

    _deltaPhiBin = Consts::twoPi/(_nBins);
    
       _meshCharge=0;
       _xCentroid=0;
       _xpCentroid=0;
       _yCentroid=0;
       _ypCentroid=0; 
       _phiCount=0;

// Bin the particles longitudinally. Also store info as to which bin each
// partcile is in
     
    mp._phiMin = mp._phiMax = 0;

    for (i=1; i <= mp._nMacros; i++)
    {
      if(mp._phi(i) > mp._phiMax)  mp._phiMax = mp._phi(i);
      if(mp._phi(i) < mp._phiMin)  mp._phiMin = mp._phi(i);
      //        mp = MacroPart::safeCast(mParts(i-1));

        w = (mp._phi(i) + Consts::pi)/_deltaPhiBin;
        iLocal = Integer(w);
        position = w - Real(iLocal);       
        iLocal += 1;       
        if(iLocal < 1) iLocal =1;
        if(iLocal > (_nBins)) iLocal = _nBins;

        _xCentroid(iLocal) += (1. - position) * mp._x(i); 
         _xCentroid(iLocal+1) +=  position * mp._x(i);
        _xpCentroid(iLocal) += (1.- position) * mp._xp(i);
        _xpCentroid(iLocal + 1) += position * mp._xp(i);
        _yCentroid(iLocal) += (1. - position) * mp._y(i);
        _yCentroid(iLocal+1) += position * mp._y(i);
        _ypCentroid(iLocal) += (1. - position) * mp._yp(i);
        _ypCentroid(iLocal + 1) += position * mp._yp(i);

        mp._fractLPosition(i) = position;
        mp._LPositionIndex(i) = iLocal;        
    }

    // Wrap 1st and last bins:
    
    _phiCount(1)        += _phiCount(nBinsP1);
    _phiCount(nBinsP1)   = _phiCount(1);
    _xCentroid(1)       += _xCentroid(nBinsP1); 
    _xCentroid(nBinsP1)  = _xCentroid(1) ;
    _xpCentroid(1)      += _xpCentroid(nBinsP1);
    _xpCentroid(nBinsP1) = _xpCentroid(1);
    _yCentroid(1)       += _yCentroid(nBinsP1);
    _yCentroid(nBinsP1)  = _yCentroid(1);
    _ypCentroid(1)      += _ypCentroid(nBinsP1);
    _ypCentroid(nBinsP1) = _ypCentroid(1);

    //===MPI==== parallel stuff  =====start=====
    if( nMPIsize_ <= 1) return;
    int ind,ind1;
    for( int i=0; i < nBinsP1; i++){
      ind  = 4*i;
      ind1 = i +1;
      buff_MPI_0_[ind+0]=_xCentroid(ind1);
      buff_MPI_0_[ind+1]=_xpCentroid(ind1);
      buff_MPI_0_[ind+2]=_yCentroid(ind1);
      buff_MPI_0_[ind+3]=_ypCentroid(ind1);
    }
    MPI_Allreduce(buff_MPI_0_, buff_MPI_1_, 4*nBinsP1 , MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    for( int i=0; i < nBinsP1; i++){
      ind  = 4*i;
      ind1 = i +1;
      _xCentroid(ind1) = buff_MPI_1_[ind+0];
      _xpCentroid(ind1)= buff_MPI_1_[ind+1];
      _yCentroid(ind1) = buff_MPI_1_[ind+2];
      _ypCentroid(ind1)= buff_MPI_1_[ind+3];
    }
    //===MPI==== parallel stuff  =====stop======   


}



///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FFTTImpedance
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(FBKick, TFBack);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FBKick::FBKick, ~FBKick 
//
// DESCRIPTION
//    Constructor and destructor for FBKick class
//
// PARAMETERS
//    None.
//
// RETURNS
//
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

FBKick::FBKick(const String &n, const Integer &order, 
            Integer &nBins,Integer &nTurnDelay, Integer &useAvg, 
              Vector(Real) &CoordinateFilter, Vector(Real) &AngleFilter):
            TFBack(n, order, nBins), 
	    _useAverage(useAvg),_AFilter(AngleFilter),
                _CFilter(CoordinateFilter),_nTurnDelay(nTurnDelay)
{
               
     Real dummy;
     Integer delayArraydim;
     delayArraydim=(_nBins+1)*_nTurnDelay;		

     _deltaXpGrid.resize(_nBins+1);
     _deltaYpGrid.resize(_nBins+1);
     _centroidXPreviousTurns.resize(delayArraydim);
     _angleXPreviousTurns.resize(delayArraydim);
     _centroidYPreviousTurns.resize(delayArraydim);
     _angleYPreviousTurns.resize(delayArraydim);


     _centroidXPreviousTurns=0.;
     _angleXPreviousTurns=0.;
     _centroidYPreviousTurns=0.;
     _angleYPreviousTurns=0.;

}

FBKick::~FBKick()
{     
 
} 


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    TFeedBack::_nodeCalculator
//
// DESCRIPTION
//    Bins the macro particles, and calculates the line density.
//    Does an FFT on the binned distribution, and sets up some
//    stuff for the particle kick calculations.
//
// PARAMETERS
//    mp - the macro herd to operate on.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void  FBKick::_nodeCalculator(MacroPart &mp)
{


    _forceCalculated = 0;

    Real dummy;
    Integer i, iLocal, iCycle, nDelay;
    SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart & 
                                      Particles::All_Mask) - 1));
 
 

  _dipole2TKick =
     Injection::nReals_Macro  * Real(_nBins)  * 1.e-9 *
        sp->_charge * Consts::eCharge * Consts::vLight/ sp->_betaSync/ 
        (Ring::lRing * Real(sp->_eTotal))/(Real(_nBins));   


 



 

    _longBin(mp);  // Bin the particles.


  
    
     if(TFeedBack::useXdimTFB){
 

// shifting the previous coordinates and angles in the array

for (nDelay=1; nDelay<= _nTurnDelay-1; nDelay++)
  {
for (iCycle=1; iCycle<= _nBins+1; iCycle++)
        {                  
_centroidXPreviousTurns(iCycle+(_nTurnDelay-nDelay)*(_nBins+1))=
                  _centroidXPreviousTurns(iCycle+(_nTurnDelay-nDelay-1)*(_nBins+1));
_angleXPreviousTurns(iCycle+(_nTurnDelay-nDelay)*(_nBins+1))=
                         _angleXPreviousTurns(iCycle+(_nTurnDelay-nDelay-1)*(_nBins+1));                 
        } 

  }


      // storing the last set of coordinates and angles

 for (iCycle=1; iCycle<= _nBins; iCycle++)
        {                  
_centroidXPreviousTurns(iCycle)=_xCentroid(iCycle);
_angleXPreviousTurns(iCycle)=_xpCentroid(iCycle);                 
        }   

 _centroidXPreviousTurns(_nBins+1)=_centroidXPreviousTurns(1);
_angleXPreviousTurns(_nBins+1)=_angleXPreviousTurns(1);



                                 }

     //cerr  <<  "Bug hunt 8" << "\n";


 if(TFeedBack::useYdimTFB){
 

// shifting the previous coordinates and angles in the array

for (nDelay=1; nDelay<= _nTurnDelay-1; nDelay++)
  {
for (iCycle=1; iCycle<= _nBins+1; iCycle++)
        {                  

   
_centroidYPreviousTurns(iCycle+(_nTurnDelay-nDelay)*(_nBins+1))=
                  _centroidYPreviousTurns(iCycle+(_nTurnDelay-nDelay-1)*(_nBins+1));


_angleYPreviousTurns(iCycle+(_nTurnDelay-nDelay)*(_nBins+1))=
                         _angleYPreviousTurns(iCycle+(_nTurnDelay-nDelay-1)*(_nBins+1));                 
   

 
       } 

  }

 
      // storing the last set of coordinates and angles

 for (iCycle=1; iCycle<= _nBins; iCycle++)
        {                  
_centroidYPreviousTurns(iCycle)=_yCentroid(iCycle);
_angleYPreviousTurns(iCycle)=_ypCentroid(iCycle);                 
        }   

	
 _centroidYPreviousTurns(_nBins+1)=_centroidYPreviousTurns(1);
_angleYPreviousTurns(_nBins+1)=_angleYPreviousTurns(1);

//cerr  <<  "Bug hunt 9" << "\n";

                                }






 







    
 if(TFeedBack::useXdimTFB)
  {
 for (Integer j=1; j<= _nBins; j++)
      { 
	dummy = _xpCentroid(j);

 _deltaXpGrid(j) =  dummy;     

      }
  }
       _deltaXpGrid(_nBins+1) = _deltaXpGrid(1);

if(TFeedBack::useYdimTFB)
  {
 for (Integer j=1; j<= _nBins; j++)
      { 
dummy = _ypCentroid(j);
 _deltaYpGrid(j) =  dummy;     

      }
  }
       _deltaYpGrid(_nBins+1) = _deltaYpGrid(1); 



  // Do a FFT of the beam
    // presently uses the FFTW package

        //calculate FFT harmonics for X



   _forceCalculated = 1;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//     FBKick::_updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with an  FFTTImpedance.
// The transverse kick is
//    added to each macro particle here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void  FBKick::_updatePartAtNode(MacroPart &mp)
{
   

 Real Kick, angle;

    Integer j,l, iDelay, index;
    Kick=0.;
    

  Real rescoef;
  // rescoef=-TFeedBack::_resistivegain*_nBins/mp._nMacros;

  //===MPI=== parallel stuff ====start===shishlo  
  mp._globalNMacros = mp._nMacros;
  if( nMPIsize_ > 1){
    int nMacros, nMacrosGlobal;
    nMacros = mp._nMacros;
    MPI_Allreduce(&nMacros, &nMacrosGlobal, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    mp._globalNMacros = nMacrosGlobal;
  }
  if( mp._globalNMacros == 0 ) return;
  //===MPI=== parallel stuff ====stop====shishlo

     rescoef=-1.0000*_nBins/mp._globalNMacros;

    if(!_forceCalculated) return;	

 // Average kick interpolated from bin location:
   
 if(_useAverage)
      {
	// updating X dimension
   if(TFeedBack::useXdimTFB)
                       {

  for (j=1; j<= mp._nMacros; j++)
     
	{
	  Kick=0.;
       
	  for (iDelay=1; iDelay<=_nTurnDelay; iDelay++)
	    // _nTurnDelay      
        {   
	  index=mp._LPositionIndex(j)+(iDelay-1)*(_nBins+1);               
          Kick += _AFilter(iDelay)*(_angleXPreviousTurns(index)   * (1 - mp._fractLPosition(j)) +
	       _angleXPreviousTurns(index+1) *  mp._fractLPosition(j));	 
   

      }

  mp._xp(j) += Kick*rescoef; 
	}	



       } // end of Xdimension

   //cerr  <<  "Bug hunt 1" << "\n";

if(TFeedBack::useYdimTFB)
                       {

  for (j=1; j<= mp._nMacros; j++)
     
	{
	  Kick=0.;
       
	  for (iDelay=1; iDelay<=_nTurnDelay; iDelay++)
	    // _nTurnDelay      
        {   
	  index=mp._LPositionIndex(j)+(iDelay-1)*(_nBins+1);               
         Kick += _AFilter(iDelay)*(_angleYPreviousTurns(index)   * (1 - mp._fractLPosition(j)) +
	       _angleYPreviousTurns(index+1) *  mp._fractLPosition(j));	 
   

      }

  mp._yp(j) += Kick*rescoef; 
	}	

  //cerr  <<  "Bug hunt 2" << "\n";



       } // end of Ydimension

      } 

  else


        
      {   
	if(TFeedBack::useXdimTFB) {     
        for (j=1; j<= mp._nMacros; j++)
         { 
            angle= mp._phi(j);        
            mp._xp(j) += 0.;
	 }                   }
	if(TFeedBack::useYdimTFB) {     
        for (j=1; j<= mp._nMacros; j++)
         {
            angle= mp._phi(j);         
            mp._yp(j) += 0.;
	 }                   }

      } 


 // cerr  <<  "Bug hunt 3" << "\n";  
 
}




///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE TImpedance
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    TFeedBack::ctor
//
// DESCRIPTION
//    Initializes the various Bump related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void  TFeedBack::ctor()
{

// set some initial values


    nLBinsTFBack = 32;
   nTFeedBack  = 0;
      
   useXdimTFB = 0;
   useYdimTFB = 0;
   _resistivegain=0.;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TFeedBack::
//
// DESCRIPTION
//    Adds a Transverse Feedback node
//
//
// PARAMETERS
//    name:    Name for the TFB 
//    order:   Order index
//    nBins    Number of bins to use
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TFeedBack::addTFeedBack(const String &name, const Integer &order, 
         Integer &nBins,Integer &nTurnDelay, Integer &useAvg,
            Vector(Real) &CoordinateFilter, Vector(Real) &AngleFilter )


{

 if (nNodes == nodes.size())
     nodes.resize(nNodes + 1);
  if(nTFeedBack == TFeedBackPointers.size())
      TFeedBackPointers.resize(nTFeedBack + 1);



   nNodes = nNodes+1;
   
  
nodes(nNodes-1) = new FBKick(name, order, nBins,nTurnDelay, useAvg,  
                  CoordinateFilter, AngleFilter);
    nTFeedBack = nTFeedBack + 1;
     
  
   
   
   TFeedBackPointers(nTFeedBack -1) = nodes(nNodes-1);
 
   nodesInitialized = 0;

}
    

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  
//  TFeedBack::showTFeedBack
//
// DESCRIPTION
//    Show a transverse feedback info:
//
//
// PARAMETERS
//    
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TFeedBack::showTFeedBack( const Integer &n, Ostream &os)

{
    Integer i;
  
 if (n >  nTFeedBack) except ( badTFeedBack );
 

}
    
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TFeedBack::dumpFeedBackKick
//
// DESCRIPTION
//    These routines dump TFeedBack info to vectors accesible from
//    the Shell
//
//
// PARAMETERS
//    n - the n'th L-space charge node. 
//    herd = the herd to calculate kick with.
//    ang - the longitudinal phase angle to provide the kick at (rad).
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Real TFeedBack::dumpFeedBackKick( const Integer &n, Real &ang)
// Dump the space charge kick per particle
{
    MacroPart *mp;
  if (n >  nTFeedBack) except ( badTFeedBack );
 
}

