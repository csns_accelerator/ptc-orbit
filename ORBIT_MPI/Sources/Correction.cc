/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Correction.cc
//
// AUTHOR
//    Steven Bunch
//    University of Tennessee, bunchsc@sns.gov
//
// CREATED
//    7/17/02
//
//  DESCRIPTION
//    Module descriptor file for the Correction module. This module
//    contains source for the Correction related info.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Correction.h"
#include "Error.h"
#include "Diagnostic.h"
#include "Solver.h"
#include "Object.h"
#include "MacroPart.h"
#include "Particles.h"
#include "StreamType.h"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

extern Array(ObjectPtr) mParts;

Real fx, fy, f;
Vector(Integer) jxp, jyp, temp1;
Integer numParts, nMaxMacroParticles, therd, mherd, adcns, abpms;
Integer xbpms, ybpms, xdcns, ydcns, jcl1, cal1, fom, fom2, fl = 1;
Integer nx, mx, ny, my, i, j, pa, otp;
Real xc, xpc, yc, ypc, dEc, phic;
Vector(Integer) dcnx, bpmx, dcny, bpmy;
Vector(Real) xpj, ypj, aparam, xrand, yrand, var1;
Vector(Real) bpmvals;
Matrix(Real) ORM;
Real dx, dy;
String adist;
Real pmin, pmax, area, errtest;
Real gmin, gmax, tol2, gerr, root;
Void calc1();
Void calc2();

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Correction::ctor
//
// DESCRIPTION
//    Initializes the various Correction related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Correction::ctor()
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Correction::corrector
//
// DESCRIPTION
//    Corrects errors
//
// PARAMETERS
//
//    amherd - Main Herd
//    atherd - Test Herd
//    axdcns - Number of X Dipole Corrector Nodes
//    aydcns - Number of Y Dipole Corrector Nodes
//    adcnx  - Array of X Dipole Corrector Node Indexes
//    adcny  - Array of Y Dipole Corrector Node Indexes
//    axbpms - Number of X BPM Nodes
//    aybpms - Number of Y BPM Nodes
//    abpmx  - Array of X BPM Node Indexes
//    abpmy  - Array of Y BPM Node Indexes
//    axpj   - Array of X Initial guesses
//    aypj   - Array of Y Initial guesses
//    dist   - BPM uncertainty error distribution type
//    param  - BPM uncertainty parameterization
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Correction::corrector(const Integer &amherd, const Integer &atherd,
                 const Integer &axdcns, const Integer &axbpms,
                 Vector(Integer) &adcnx, Vector(Integer) &abpmx,
                 Vector(Real) &axpj,
                 const Integer &aydcns, const Integer &aybpms,
                 Vector(Integer) &adcny, Vector(Integer) &abpmy,
                 Vector(Real) &aypj,
                 const String &dist, Vector(Real) &param, const Integer &aotp)
{
  Error::drand(param(1));
  adist = dist;
  xdcns = axdcns;
  ydcns = aydcns;
  xbpms = axbpms;
  ybpms = aybpms;
  otp = aotp;

  if (xdcns != 0)
  {
    dcnx.resize(xdcns);
    for (nx = 1; nx <= xdcns; nx++)
    {
       dcnx(nx) = adcnx(nx);
    }
  }

  if (ydcns != 0)
  {
    dcny.resize(ydcns);
    for (ny = 1; ny <= ydcns; ny++)
    {
       dcny(ny) = adcny(ny);
    }
  }

  if (xbpms != 0)
  {
    bpmx.resize(xbpms);
    xrand.resize(xbpms);
    for (mx = 1; mx <= xbpms; mx++)
    {
       bpmx(mx) = abpmx(mx);
       xrand(mx) = Error::drand(0);
    }
  }

  if (ybpms != 0)
  {
    bpmy.resize(ybpms);
    yrand.resize(ybpms);
    for (my = 1; my <= ybpms; my++)
    {
       bpmy(my) = abpmy(my);
       yrand(my) = Error::drand(0);
    }
  }

  if (xdcns != 0)
  {
    xpj.resize(xdcns);
    for (i = 1; i <= xdcns; i++)
    {
       xpj(i) = axpj(i);
    }
  }

  if (ydcns != 0)
  {
    ypj.resize(ydcns);
    for (j = 1; j <= ydcns; j++)
    {
       ypj(j) = aypj(j);
    }
  }

  aparam.resize(7);
  for (pa = 1; pa <= 7; pa++)
  {
     aparam(pa) = param(pa);
  }

  therd = atherd;
  mherd = amherd;

  MacroPart *mpi = MacroPart::safeCast(mParts((therd & All_Mask)-1));
  nMaxMacroParticles = mpi->_nMacros;

  if ((xbpms != 0) || (ybpms != 0)) fom = Solver::addFOM("f", f, 1, 1);

  jxp.resize(xdcns);
  for (i = 1; i <= xdcns; i++)
  {
    jxp(i) = Solver::addVariable("xpj", xpj(i), 1.0, 1, 1, -1.0, 1.0, 1);
  }

  jyp.resize(ydcns);
  for (j = 1; j <= ydcns; j++)
  {
    jyp(j) = Solver::addVariable("ypj", ypj(j), 1.0, 1, 1, -1.0, 1.0, 1);
  }

  jcl1 = Solver::addCalculator("Corrector", calc1, 1, 1);

  Solver::solve(1);

  Solver::showEqnSetBase(cout,0);
}

Void calc1()
{

  for (numParts = 1; numParts <= nMaxMacroParticles; numParts++)
  {
     xc = Particles::xValBase(numParts, therd);
     xpc = Particles::xpValBase(numParts, therd);
     yc = Particles::yValBase(numParts, therd);
     ypc = Particles::ypValBase(numParts, therd);
     dEc = Particles::deltaEValBase(numParts, therd);
     phic = Particles::phiValBase(numParts, therd);
     Particles::setPartValsBase(numParts, mherd, xc,
                                xpc, yc, ypc, dEc, phic); 
  }

  for (i = 1; i <= xdcns; i++)
  {
    Error::setDipoleCorrectorNodeVal(dcnx(i), xpj(i), "X");
  }

  for (j = 1; j <= ydcns; j++)
  {
    Error::setDipoleCorrectorNodeVal(dcny(j), ypj(j), "Y");
  }

  Ring::doTurn(1);

  fx = 0.0;
  for (i = 1; i <= xbpms; i++)
  {
    if(adist == "UNIFORM")
    {
      dx = aparam(2) + xrand(i) * (aparam(3) - aparam(2));
    }
    if(adist == "GAUSS")
    {
      tol2 = 1.0e-14;
      gmin = 0.0;
      gmax = 10.0;
      pmin = 0.0;
      pmax = 1.0;

      if (aparam(4) > 0.)
      {
         pmin = 0.5 - 0.5 * Error::derf((aparam(4)) / pow(2.,0.5));
         pmax = 0.5 + 0.5 * Error::derf((aparam(4)) / pow(2.,0.5));
      }

      area = pmin + (pmax - pmin) * xrand(i);
      errtest = 2. * area - 1.0;
      if (errtest < 0.) errtest = -errtest;
      gmax = 10.0;
      while ((Error::derf(gmax) - errtest) < 0.) gmax *= 10.0;
      root = Error::root_normal(errtest, gmin, gmax, tol2);
      if (area >= 0.5)
      {
         dx = aparam(2) + pow(2.,0.5) * aparam(3) * root;
      }
      else
      {
         dx = aparam(2) - pow(2.,0.5) * aparam(3) * root;
      }
    }

    fx += (Diagnostic::BPMSignal(bpmx(i),"X") + dx) *
          (Diagnostic::BPMSignal(bpmx(i),"X") + dx);
  }

  fy = 0.0;
  for (j = 1; j <= ybpms; j++)
  {
    if(adist == "UNIFORM")
    {
      dy = aparam(5) + yrand(j) * (aparam(6) - aparam(5));
    }
    if(adist == "GAUSS")
    {
      tol2 = 1.0e-14;
      gmin = 0.0;
      gmax = 10.0;
      pmin = 0.0;
      pmax = 1.0;

      if (aparam(7) > 0.)
      {
         pmin = 0.5 - 0.5 * Error::derf((aparam(7)) / pow(2.,0.5));
         pmax = 0.5 + 0.5 * Error::derf((aparam(7)) / pow(2.,0.5));
      }

      area = pmin + (pmax - pmin) * yrand(j);
      errtest = 2. * area - 1.0;
      if (errtest < 0.) errtest = -errtest;
      gmax = 10.0;
      while ((Error::derf(gmax) - errtest) < 0.) gmax *= 10.0;
      root = Error::root_normal(errtest, gmin, gmax, tol2);
      if (area >= 0.5)
      {
         dy = aparam(5) + pow(2.,0.5) * aparam(6) * root;
      }
      else
      {
         dy = aparam(5) - pow(2.,0.5) * aparam(6) * root;
      }
    }

    fy += (Diagnostic::BPMSignal(bpmy(j),"Y") + dy) *
          (Diagnostic::BPMSignal(bpmy(j),"Y") + dy);
  }

  f = fx + fy;
  if(otp == 1) cerr << "f = " << f << endl;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Correction::ORMcorrector
//
// DESCRIPTION
//    
//
// PARAMETERS
//
//
// RETURNS
//    
//
///////////////////////////////////////////////////////////////////////////

Void Correction::ORMcorrector(const String &fn, const Integer &bpms,
                              const Integer &dcns)
{
  Real dummy;
  abpms = bpms;
  adcns = dcns;

  fstream fio1(fn, ios::in);
  bpmvals.resize(abpms);
  for(i = 1; i <= abpms; i++)
  {
    fio1 >> dummy;
    bpmvals(i) = dummy;
  }
  fio1.close();

  ORM.resize(abpms, adcns);

  fstream fio2("OrbitResponseMatrix", ios::in);
  for(i = 1; i <= abpms; i++)
  {
    for(j = 1; j <= adcns; j++)
    {
      fio2 >> dummy;
      ORM(i,j) = dummy;
    }
  }

  fio2.close();

  fom2 = Solver::addFOM("f", f, 1, 1);

  temp1.resize(adcns);
  var1.resize(adcns);

  for (i = 1; i <= adcns; i++)
  {
    temp1(i) = Solver::addVariable("dcn", var1(i), 1.0, 1, 1, -1.0, 1.0, 1);
  }

  cal1 = Solver::addCalculator("ORMCorrector", calc2, 1, 1);

  Solver::solve(1);

  Solver::showEqnSetBase(cout,0);
}

Void calc2()
{
  Vector(Real) Mx(abpms);
  
  f = 0.0;
  for( i = 1; i <= abpms; i++ )
  {
    Mx(i) = 0.0;
  }

  for( i = 1; i <= abpms; i++ )
  {
    for( j = 1; j <= adcns; j++ )
    {
      Mx(i) += (ORM(i,j) * var1(j));
    }
  }

  for( i = 1; i <= abpms; i++)
  {
    f += (bpmvals(i) + Mx(i)) * (bpmvals(i) + Mx(i));
  }
}
