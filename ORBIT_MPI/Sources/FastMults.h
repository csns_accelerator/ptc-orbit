#if !defined(__FastMults__)
#define __FastMults__

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <mpi.h>
#include <vector>
#include <complex>

#include "Object.h"
#include "MacroPart.h"

using namespace std;


///////////////////////////////////////////////////////////////////////////
// The Cell class:
///////////////////////////////////////////////////////////////////////////

class Cell
{
  public:

  // Constructor

  Cell(int nTerms, int level,
       int yIndex, int xIndex);

  // Destructor

  ~Cell();

  // Variables and Methods

  int _nTerms;
  int _level;
  int _yIndex;
  int _xIndex;

  int _EmptyM;
  int _EmptyT;

  std::complex<double>* _MultCoeffs;
  std::complex<double>* _TaylorCoeffs;

  int _nCellMacros;
  vector<int> _CellMacroIndex;
  vector<double> _CellMacroForcex;
  vector<double> _CellMacroForcey;

  int _nCellBPs;
  vector<int> _CellBPIndex;
  vector<double> _CellBPPot;

  double _xMin;
  double _xCenter;
  double _xMax;
  double _yMin;
  double _yCenter;
  double _yMax;

  void _MultCoeffsfromMacros(MacroPart& mp);
  void _addToMultCoeffs(std::complex<double>* MultCoeffs);
  void _addToTaylorCoeffs(std::complex<double>* TaylorCoeffs);
};


///////////////////////////////////////////////////////////////////////////
// The Row class:
///////////////////////////////////////////////////////////////////////////

class Row
{
  public:

  // Constructor

  Row(int nTerms, int level,
      int yIndex, int nXYBins);

  // Destructor

  ~Row();

  // Variables and Methods

  int _nTerms;
  int _level;
  int _yIndex;
  int _nXYBins;
  Cell** _CellPtr;
};


///////////////////////////////////////////////////////////////////////////
// The Level class:
///////////////////////////////////////////////////////////////////////////

class Level
{
  public:

  // Constructor

  Level(int nTerms, int level, int nXYBins);

  // Destructor

  ~Level();

  // Variables and Methods

  int _nTerms;
  int _level;
  int _nXYBins;
  Row** _RowPtr;
};


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   FMMCalculator
//
// INHERITANCE RELATIONSHIPS
//   None
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   Class for calculating space charge kicks
//   by the fast multipole method including
//   a conducting wall boundary
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class FMMCalculator : public Object
{
  Declare_Standard_Members(FMMCalculator, Object);

  public:

  // Constructor

  FMMCalculator(int nTerms, int nXYBins, double eps,
                int nZBins, double ZLength,
                int BPPoints, int BPModes,
                double BPrnorm, double BPResid,
                double* BPx , double* BPy,
                double** BPPhiH1, double** BPPhiH2) :
                _nTerms(nTerms), _nXYBins(nXYBins), _eps(eps),
                _nZBins(nZBins), _ZLength(ZLength),
                _BPPoints(BPPoints), _BPModes(BPModes),
		_BPrnorm(BPrnorm), _BPResid(BPResid),
                _BPx(BPx), _BPy(BPy),
		_BPPhiH1(BPPhiH1), _BPPhiH2(BPPhiH2)
  {
    _nTerms++;

    int nXYB = _nXYBins;
    _nLevels = 0;
    while(nXYB > 1)
    {
      nXYB /= 2;
      _nLevels++;
    }
    _LevelPtr = new Level*[_nLevels];

    nXYB = _nXYBins;
    int level = 0;
    while(nXYB > 1)
    {
      _LevelPtr[level] = new Level(_nTerms, level, nXYB);
      nXYB /= 2;
      level++;
    }

    _ZDist = new double[_nZBins];

    int k, l;
    _Factorial = new double[2 * _nTerms];
    _Binom     = new double*[2 * _nTerms];

    _Factorial[0] = 1.0;
    _Binom[0] = new double[1];
    _Binom[0][0] = 1.0;

    for(k = 1; k < 2 * _nTerms; k++)
    {
      _Factorial[k] = double(k) * _Factorial[k - 1];
      _Binom[k] = new double[k + 1];
      for(l = 0; l <= k; l++)
      {
        _Binom[k][l] = _Factorial[k] / (_Factorial[l] * _Factorial[k - l]);
      }
    }
  }

  // Destructor

  ~FMMCalculator()
  {
    int level, k, l;

    for(level = 0; level < _nLevels; level++)
    {
      delete _LevelPtr[level];
    }

    for(k = 0; k < 2 * _nTerms; k++)
    {
      delete[] _Binom[k];
    }

    delete[] _LevelPtr;
    delete[] _Factorial;
    delete[] _Binom;

    delete _ZDist;
  }

  // Variables and Methods

  int _nTerms;
  int _nXYBins;
  double _eps;
  int _nZBins;
  double _ZLength;
  double _ZCenter;
  double* _ZDist;
  int _BPPoints;
  int _BPModes;
  double _BPrnorm;
  double _BPResid;
  double* _BPx;
  double* _BPy;
  double** _BPPhiH1;
  double** _BPPhiH2;

  int _nLevels;
  Level**  _LevelPtr;
  double*  _Factorial;
  double** _Binom;

  double _xMin;
  double _xMax;
  double _yMin;
  double _yMax;

  double _SCKick_Coeff;

  void _SetCoords(double xMin, double xMax,
                  double yMin, double yMax);

  void _DistributeParts(MacroPart& mp);

  void _SeedMultipoles(MacroPart& mp);

  void _UpSweep();

  void _DownSweep();

  void _ForceFromBeam(MacroPart& mp);

  void _shiftMultipoles(int leveli, int yIndexi, int xIndexi,
                        int levelf, int yIndexf, int xIndexf);

  void _TaylorFromMultipoles(int leveli, int yIndexi, int xIndexi,
                             int levelf, int yIndexf, int xIndexf);

  void _shiftTaylor(int leveli, int yIndexi, int xIndexi,
                    int levelf, int yIndexf, int xIndexf);

  void _evaluateMultipoles(MacroPart& mp,
                           int yIndex, int xIndex);

  void _PairWiseOneCell(MacroPart& mp,
                        int yIndex, int xIndex);

  void _PairWiseTwoCells(MacroPart& mp,
                         int yIndexi, int xIndexi,
                         int yIndexf, int xIndexf);

  void _ForceFromBoundary(MacroPart& mp);

  void _ApplyForce(MacroPart& mp, double SCKick_Coeff);

  void _ClearFMM();
};


#endif   // __FastMults__
