/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Particles.cc
//
// AUTHOR
//    John GalambosORNL, jdg@ornl.gov
//
// CREATED
//    10/2/97
//
//  DESCRIPTION
//    Source file for the Particles module. This module implements
//    the macro particle managment.
//
//  REVISION HISTORY
//    7/3/02 - Added setPartValsBase function
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Particles.h"
#include "MacroPart.h"
#include <cmath>

#if defined(_WIN32)
  #include <strstrea.h>
#else
  #include <sstream>
#endif

#include <iostream>
#include <fstream>
#include <iomanip>

//MPI header file for parallel stuff ==MPI==
#include "mpi.h"


using namespace std;
///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

// Lists of synchronous particles and macro particles

Array(ObjectPtr) syncP;
Array(ObjectPtr) mParts;

// Masks for indices

const Integer SyncP_Mask = 0x10000;
const Integer mPart_Mask = 0x20000;

// Granularity to allocate entries in work arrays

const Integer Particle_Alloc_Chunk = 1000;

static const char *mpHeader =
" #      x(mm)      xp (mrad)   y(mm)        yp (mrad)     dE (GeV)     phi (rad) \n";
static const char *mpLine =
"---- ------------ ------------ ------------ ------------ ------------  ----------\n";

static Integer nMains = 0;

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Particles::ctor
//
// DESCRIPTION
//    Initializes the various constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Particles::ctor()
{

// set some initial values

    All_Mask = 0x0FFFF;

    nMacroParticles = 0;
    nBucketParticles = 0;

    partChunkSize = 10000;

    nHerds = 0;
    mainHerd = 0;

  // parallel stuff  ==MPI==  ==start===
    iMPIini=0;
    MPI_Initialized(&iMPIini);
    nMPIsize = 1;
    nMPIrank = 0;
    if(iMPIini > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank);
    }
  // parallel stuff  ==MPI==  ==stop===

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::addSyncPart
//
// DESCRIPTION
//    Adds a synchronous particle to the system.
//
// PARAMETERS
//    m:              Mass (AMU)
//    c:              Charge number
 //    tSync           The kinetic energy (MeV)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::addSyncPart(const Real &m, const Integer &c, Real &E)
{
    if(nSyncP ==1) except(tooManySyncPart);

  if (nSyncP == syncP.size())
     syncP.resize(nSyncP + 1);
  nSyncP++;

   syncP(nSyncP-1) = new SyncPart(m, c, E);

  syncPart = nSyncP | SyncP_Mask;
}


Void Particles::updateSyncPart(const Real &m, const Integer &c, Real &E)
{
    if (!(syncPart &  SyncP_Mask)) except(badSyncPIndex);
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));
    sp->update(m,c,E);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::getMainHerdSize
//
// DESCRIPTION
//    Returns the number of macro-particles in the herd
//
// PARAMETERS
//   const Integer &herd - herd index
//
// RETURNS
//    Returns the number of macro-particles in the h
//
///////////////////////////////////////////////////////////////////////////

Integer Particles::getHerdSize(const Integer &herd)
{
    MacroPart *mp;
    if ( (herd & All_Mask) > nHerds) except (badHerdNo);
    mp = MacroPart::safeCast(mParts((herd & All_Mask) - 1));
    return  mp->_nMacros;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::getMainHerdSizeGlobal
//
// DESCRIPTION
//    Returns the number of macro-particles in the herd across all CPUs
//
// PARAMETERS
//   const Integer &herd - herd index
//
// RETURNS
//    Returns the number of macro-particles in all herds
//
///////////////////////////////////////////////////////////////////////////

Integer Particles::getHerdSizeGlobal(const Integer &herd)
{
    MacroPart *mp;
    if ( (herd & All_Mask) > nHerds) except (badHerdNo);
    mp = MacroPart::safeCast(mParts((herd & All_Mask) - 1));

    int nMacrosGlobal;
    int nMacros = mp->_nMacros;
    nMacrosGlobal = nMacros;
    if(nMPIsize > 1){
      MPI_Allreduce(&nMacros,&nMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    }
    mp->_globalNMacros = nMacrosGlobal;

    return  mp->_globalNMacros;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::setHerdCenterXY(const Integer &herd,  const Real &x_c, const Real &y_c)
//
// DESCRIPTION
//    Sets the geometric center of the herd in the new position in the (x,y) plane
//
// PARAMETERS
//   const Integer &herd - herd index , x_c and y_c new herd's center position
//
// RETURNS
//    Returns the number of macro-particles in all herds
//
///////////////////////////////////////////////////////////////////////////

Void Particles::setHerdCenterXY(const Integer &herd,  const Real &x_c, const Real &y_c)
{
  MacroPart *mp;
  if ( (herd & All_Mask) > nHerds) except (badHerdNo);
  mp = MacroPart::safeCast(mParts((herd & All_Mask) - 1));

  int nMacrosGlobal;
  int nMacros = mp->_nMacros;
  nMacrosGlobal = nMacros;
  if(nMPIsize > 1){
    MPI_Allreduce(&nMacros,&nMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  }
  mp->_globalNMacros = nMacrosGlobal ;

  if(  nMacrosGlobal == 0) return;

  double x_cent_local = 0.;
  double y_cent_local = 0.;
  for(int i = 1; i <=  nMacros; i++){
    x_cent_local += mp->_x(i);
    y_cent_local += mp->_y(i);
  }

  double x_cent_global = x_cent_local;
  double y_cent_global = y_cent_local;

  if(nMPIsize > 1){
    MPI_Allreduce(&x_cent_local,&x_cent_global,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(&y_cent_local,&y_cent_global,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  }

  x_cent_global = x_cent_global/nMacrosGlobal;
  y_cent_global = y_cent_global/nMacrosGlobal;

  for(int i = 1; i <=  nMacros; i++){
    mp->_x(i) = mp->_x(i) - x_cent_global + x_c;
    mp->_y(i) = mp->_y(i) - y_cent_global + y_c;
  }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::addMacroHerd
//
// DESCRIPTION
//    Adds a herd of macroParticles
//
// PARAMETERS
//    s:              The syncP identifier (Integer)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Integer Particles::addMacroHerd(const Integer &chunkSize)
{
    static Integer nMains = 0;

    nMains ++;
    if(nMains > 1) except(tooManyMains);
    if (!(syncPart &  SyncP_Mask)) except(badSyncPIndex);
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

    nHerds++;
    mParts.resize(nHerds);
    mParts(nHerds - 1) = new MacroPart(*sp,chunkSize, "mainHerd");
    return nHerds | mPart_Mask;
}

Integer Particles::addMainHerd(const Integer &chunkSize)
{
    nMains ++;
    if(nMains > 1) except(tooManyMains);
    if (!(syncPart &  SyncP_Mask)) except(badSyncPIndex);
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

    nHerds++;
    mParts.resize(nHerds);
    mParts(nHerds - 1) = new MacroPart(*sp,chunkSize, "mainHerd");
    return nHerds | mPart_Mask;
}

Integer Particles::addMacroHerdBase(const Integer &chunkSize, const String &name)
{
    if (!(syncPart &  SyncP_Mask)) except(badSyncPIndex);
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

    nHerds++;
    mParts.resize(nHerds);
    mParts(nHerds - 1) = new MacroPart(*sp,chunkSize, name);
    return nHerds | mPart_Mask;
}




///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::setHerdFeelLevel
//
// DESCRIPTION
//    Sets the _feelsHerd member
//
// PARAMETERS
//    herd :           herd index
//    feelHerd:        The feelHerd level for this herd
//        0 = feels only itself (default)
//        1 = feels only others (for test herds)
//        2 = feels itself and others (for parallel cases) NOT IMPLEMENTED!
//
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::setHerdFeelLevel(Integer &herd, const Integer &fl)
{
    if((herd & All_Mask) > nHerds) except(badHerdNumber);
     MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
     mp->_feelsHerds = fl;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::addMacroPartBase
//
// DESCRIPTION
//    Adds a macro particle to an arbitrary herd
//
// PARAMETERS
//    herd:           The herd Idenifier
//    x:              The initial x value (mm)
//    xP:             The initial x prime value (mrad)
//    y:              The initial y value (mm)
//    yP:             The initial yp prime value (mrad)
//    dE              The delta energy from the sync. particle (GeV)
//    phi             The longitudinal angle relative to the sync. part. (rad)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::addMacroPartBase2(const Integer &herd, const Real &x,
                    const Real &xP, const Real &y,
                    const Real &yP, const Real &dE, const Real &phi)
{

  if(herd & mPart_Mask)
    {
     if((herd & All_Mask) > nHerds) except(badHerdNumber);

     MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
      if ( mp->_nMacros == mp->_x.rows()) mp->_resize(partChunkSize);

      mp->_insertMacroPart(x,xP,y,yP,dE,phi);
      if(herd == mainHerd) nMacroParticles =   mp->_nMacros;
    }
  else
     except(badHerdNumber);
}


Void Particles::addMacroPartBase(const Integer &herd, const Real &x,
                             const Real &xP, const Real &y, const Real &yP)
{
  if(herd & mPart_Mask)
    {
     if((herd & All_Mask) > nHerds) except(badHerdNumber);
     MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
      if ( mp->_nMacros == mp->_x.rows()) mp->_resize(partChunkSize);
      mp->_insertMacroPart(x,xP,y,yP);
      if(herd == mainHerd) nMacroParticles =   mp->_nMacros;
    }
  else
     except(badHerdNumber);

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::addMacroPart
//
// DESCRIPTION
//    Adds a macro particle to the main herd.
//
// PARAMETERS
//    x:       The initial x value (mm)
//    xP:      The initial x prime value (mrad)
//    y:       The initial y value (mm)
//    yP:      The initial yp prime value (mrad)
//    dE       The initial delta E (GeV)
//    phi      The initial longidinal position relative to the Sync part (rad)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::addMacroPart2(const Real &x,
                    const Real &xP, const Real &y,
                    const Real &yP, const Real &dE, const Real &phi)
{
   addMacroPartBase2(mainHerd, x, xP, y, yP, dE, phi);
}

Void Particles::addMacroPart(const Real &x,
                             const Real &xP, const Real &y, const Real &yP)
{

  addMacroPartBase(mainHerd,x,xP,y,yP);

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::addBucketPart
//
// DESCRIPTION
//    Adds a special "bucket" macro particle to do Longitudinal bucket
//    calculations.
//
// PARAMETERS
//    dE:             The delta E (GeV)
//    phi             The initial angle (rad)
//    s:              The syncP identifier (Integer)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::addBucketPart(const Real &dE, const Real &phi)
{
    Integer bucketHerdSize = 1;

    if(nBucketParticles ==1) except (tooManyBuckParts);

    if (!(syncPart &  SyncP_Mask)) except(badSyncPIndex);
    SyncPart *sp = SyncPart::safeCast(syncP((syncPart & All_Mask) - 1));

    nHerds++;
    mParts.resize(nHerds);
    mParts(nHerds - 1) = new MacroPart(*sp,bucketHerdSize, "bucketHerd");
    bucketHerd = nHerds | mPart_Mask;
    nBucketParticles = 1;


    MacroPart *mp = MacroPart::safeCast(mParts((bucketHerd & All_Mask)-1));
    mp->_resize(2);
    mp->_feelsHerds = 1;
    mp->_insertMacroPart(0,0,0,0,dE,phi);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Ring::xVal(const Integer &i)
//      Also:  xpVal, yVal, yPval, phiVal, deltaEVal
//
// DESCRIPTION
//    Dump the x value of particle i
//
// PARAMETERS
//    i  - index of pacroparticle i
//
// RETURNS
//     X value (mm)
//
///////////////////////////////////////////////////////////////////////////

Real Particles::xValBase(const Integer &i, const Integer &herd)
{
    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
    if (i > mp->_nMacros) except(badMacroPartNum);
    return mp->_x(i);
}

Real Particles::xVal(const Integer &i)
{

    if (i > nMacroParticles) except(badMacroPartNum);
    return xValBase(i, mainHerd);
}

Real Particles::xpValBase(const Integer &i, const Integer &herd)
{
    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
    if (i > mp->_nMacros) except(badMacroPartNum);
    return mp->_xp(i);
}

Real Particles::xpVal(const Integer &i)
{

    if (i > nMacroParticles) except(badMacroPartNum);
    return xpValBase(i, mainHerd);
}


Real Particles::yValBase(const Integer &i, const Integer &herd)
{
    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
    if (i > mp->_nMacros) except(badMacroPartNum);
    return mp->_y(i);
}

Real Particles::yVal(const Integer &i)
{

    if (i > nMacroParticles) except(badMacroPartNum);
    return yValBase(i, mainHerd);
}

Real Particles::ypValBase(const Integer &i, const Integer &herd)
{
    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
    if (i > mp->_nMacros) except(badMacroPartNum);
    return mp->_yp(i);
}

Real Particles::ypVal(const Integer &i)
{

    if (i > nMacroParticles) except(badMacroPartNum);
    return ypValBase(i, mainHerd);
}



Real Particles::deltaEValBase(const Integer &i, const Integer &herd)
{

    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
    if (i > mp->_nMacros) except(badMacroPartNum);
    return mp->_deltaE(i);
}

Real Particles::dp_pValBase(const Integer &i, const Integer &herd)
{

    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
    if (i > mp->_nMacros) except(badMacroPartNum);
    return mp->_dp_p(i);
}


Real Particles::deltaEVal(const Integer &i)
{

    if (i > nMacroParticles) except(badMacroPartNum);
    return deltaEValBase(i, mainHerd);
}

Real Particles::phiValBase(const Integer &i, const Integer &herd)
{
    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
    if (i > mp->_nMacros) except(badMacroPartNum);
    return mp->_phi(i);
}

Real Particles::phiVal(const Integer &i)
{
    if (i > nMacroParticles) except(badMacroPartNum);
    return phiValBase(i, mainHerd);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Ring::setPartValsBase
//
// DESCRIPTION
//    Updates the parameters of particle i
//
// PARAMETERS
//    i       - index of pacroparticle i
//    nx      - new x value
//    nxp     - new xp value
//    ny      - new y value
//    nyp     - new yp value
//    ndeltaE - new deltaE value
//    nphi    - new phi value
//
// RETURNS
//    nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::setPartValsBase(const Integer &i, const Integer &herd,
                                const Real &nx, const Real &nxp,
                                const Real &ny, const Real &nyp,
                                const Real &ndeltaE, const Real &nphi)
{
    MacroPart *mp = MacroPart::safeCast(mParts((herd & All_Mask)-1));
    if (i > mp->_nMacros) except(badMacroPartNum);
    mp->_x(i) = nx;
    mp->_xp(i) = nxp;
    mp->_y(i) = ny;
    mp->_yp(i) = nyp;
    mp->_deltaE(i) = ndeltaE;
    mp->_phi(i) = nphi;
    mp->_dp_p(i) = ndeltaE * mp->_syncPart._dppFac;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::xVals(RealVector &x)
//
// DESCRIPTION
//    Dump the x values of all particles to a Vector x
//      Also:  xpVals, yVals, yPvals, phiVals, deltaEVals
//
// PARAMETERS
//    x - Reference to a RealVector where the x values are dumped.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::xVals(Vector(Real) &x)
{
    MacroPart *mp;
    x.resize(nMacroParticles);
        mp= MacroPart::safeCast(mParts((mainHerd & All_Mask)-1));
        for (Integer i=1; i<= mp->_nMacros; i++)

    {

        x(i) = mp->_x(i);
    }

}

Void Particles::yVals(Vector(Real) &y)
{
    MacroPart *mp;
    y.resize(nMacroParticles);
    mp= MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));
    for (Integer i=1; i<= mp->_nMacros; i++)
    {

        y(i) = mp->_y(i);
    }

}

Void Particles::xpVals(Vector(Real) &xp)
{
    MacroPart *mp;
    xp.resize(nMacroParticles);
    mp= MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));
    for (Integer i=1; i<= mp->_nMacros; i++)
    {
        xp(i) = mp->_xp(i);
    }

}

Void Particles::ypVals(Vector(Real) &yp)
{
    MacroPart *mp;
    yp.resize(nMacroParticles);
        mp= MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));
    for (Integer i=1; i<= mp->_nMacros; i++)
    {
        yp(i) = mp->_yp(i);
    }

}
Void Particles::deltaEVals(Vector(Real) &de)
{
    MacroPart *mp;
    de.resize(nMacroParticles);
    mp= MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));
    for (Integer i=1; i<= mp->_nMacros; i++)
    {

        de(i) = mp->_deltaE(i);
    }

}

Void Particles::phiVals(Vector(Real) &phi)
{
    MacroPart *mp;
    phi.resize(nMacroParticles);
    mp= MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));
    for (Integer i=1; i<= mp->_nMacros; i++)
    {
        phi(i) = mp->_phi(i);
    }

}

Void Particles::absPhiVals(Vector(Real) &phi)
{
    Real dPhi;
    Real phiAvg = 0.;
    MacroPart *mp;
    phi.resize(nMacroParticles);
    mp= MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));
    for (Integer i=1; i<= mp->_nMacros; i++) phiAvg += mp->_phi(i);
    phiAvg/= Real(nMacroParticles);

    for (Integer i=1; i<= mp->_nMacros; i++)
    {
        dPhi = mp->_phi(i) - phiAvg;
        if(dPhi > 0.) phi(i) = dPhi;
        else  phi(i) = -dPhi;
    }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::dumpPart(const Integer &n, Ostream &os, const Integer &index)
//
// DESCRIPTION
//    Dump the x,xp,y,yp,phi,dE values of particle number index to a stream
//
// PARAMETERS
//    n     - Reference to a macro-particle herd
//    os    - Reference to a stream to dump the info.
//    index - Reference to a macro-particle index
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::dumpPart(const Integer &n, Ostream &os, const Integer &index)
{
   MacroPart *mp;

   if ( (n & All_Mask) > nHerds) except (badHerdNo);

   mp = MacroPart::safeCast(mParts((n & All_Mask) - 1));

   Integer i = index;
   os << mp->_x(i) << "  "
      << mp->_xp(i) << "  "
      << mp->_y(i) << "  "
      << mp->_yp(i) << "  "
      << mp->_phi(i) << "  "
      << mp->_deltaE(i) << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::dumpParts(const Integer &n, Ostream &os)
//
// DESCRIPTION
//    Dump the x,xp,y,yp,phi,dE values of all particles to a stream
//
// PARAMETERS
//    n - Reference to a macro-particle herd
//    os  Reference to a stream to dump the info.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::dumpParts(const Integer &n, Ostream &os)
{
    MacroPart *mp;

   if ( (n & All_Mask) > nHerds) except (badHerdNo);

     mp= MacroPart::safeCast(mParts((n & All_Mask) - 1));
		 os<<std::setprecision(8);
    for (Integer i=1; i<= mp->_nMacros; i++)
     {
        os << mp->_x(i) << "  "
           << mp->_xp(i) << "  "
           << mp->_y(i) << "  "
           << mp->_yp(i) << "  "
           << mp->_phi(i) << "  "
           << mp->_deltaE(i) << "\n";
     }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::dumpPartsGlobal(const Integer &n, Ostream &os)
//
// DESCRIPTION
//    Dump the x,xp,y,yp,phi,dE values of all particles to a stream from all CPUs
//
// PARAMETERS
//    n - Reference to a macro-particle herd
//    os  Reference to a stream to dump the info.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::dumpPartsGlobal(const Integer &n, Ostream &os)
{
    MacroPart *mp;

   if ( (n & All_Mask) > nHerds) except (badHerdNo);

     mp= MacroPart::safeCast(mParts((n & All_Mask) - 1));
     if(nMPIrank == 0){
        os<<std::setprecision(8);
        for (Integer i=1; i<= mp->_nMacros; i++){
          os << mp->_x(i) << "  "
             << mp->_xp(i) << "  "
             << mp->_y(i) << "  "
             << mp->_yp(i) << "  "
             << mp->_phi(i) << "  "
             << mp->_deltaE(i) << "\n";
        }
      if( nMPIsize == 1) return;
     }

     //parallel case ======MPI================start=======
       MPI_Status statusMPI ;
       int* nMacrosArr     = (int *) malloc (sizeof(int)*nMPIsize);
       int* nMacrosArr_MPI = (int *) malloc (sizeof(int)*nMPIsize);
       for(int i=0; i < nMPIsize; i++){
         nMacrosArr[i]=0;
         if( i == nMPIrank ){ nMacrosArr[i]=mp->_nMacros;}
       }
       MPI_Allreduce(nMacrosArr ,nMacrosArr_MPI,nMPIsize,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

       int nMacroChank = 1000;
       static double buff_sr[6000];

       //sending and receiving macro-particles coordinates
       for( int i = 1; i < nMPIsize; i++){

				 int nStart = 0;
				 int nStop = nMacroChank;
				 while(nStart <  nMacrosArr_MPI[i]){
					 if(nStop > nMacrosArr_MPI[i]) nStop = nMacrosArr_MPI[i];
					 if( (nStop - nStart) <= 0) break;

					 if( i == nMPIrank ){
						 for( int j = nStart; j < nStop; j++){
							 buff_sr[6*(j-nStart)+ 0] = mp->_x(j+1);
							 buff_sr[6*(j-nStart) + 1] = mp->_xp(j+1);
							 buff_sr[6*(j-nStart) + 2] = mp->_y(j+1);
							 buff_sr[6*(j-nStart) + 3] = mp->_yp(j+1);
							 buff_sr[6*(j-nStart) + 4] = mp->_deltaE(j+1);
							 buff_sr[6*(j-nStart) + 5] = mp->_phi(j+1);
						 }
					 }

					 MPI_Bcast(buff_sr, 6*nMacroChank, MPI_DOUBLE, i , MPI_COMM_WORLD);

					 if(nMPIrank == 0){
						 for( int j = 0; j < (nStop - nStart); j++){
							 os   <<buff_sr[6*j + 0]<< "  "
							 <<buff_sr[6*j + 1]<< "  "
							 <<buff_sr[6*j + 2]<< "  "
							 <<buff_sr[6*j + 3]<< "  "
							 <<buff_sr[6*j + 5]<< "  "
							 <<buff_sr[6*j + 4]<< "\n";
						 }
					 }
					 nStart = nStart + nMacroChank;
					 nStop = nStop + nMacroChank;
				 }
       }

       //free all memories
       free(nMacrosArr);
       free(nMacrosArr_MPI);
     //parallel case ======MPI================stop========
		 os << flush;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::readParts(const Integer &n, String &fName,
//                       const Integer nParts)
//
// DESCRIPTION
//    Read the x,xp,y,yp,phi,dE values of all particles from a stream
//
// PARAMETERS
//    n     - Reference to a macro-particle herd
//    fName - is  the name of the file to rread from
//    nParts - is the number of parts to read.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::readParts(const Integer &n, const String &fName,
                          const Integer &nParts)
{
  readPartsParallel(n,fName,nParts,0);
}


Void Particles::readParts2(const Integer &n, const String &fName,
                           const Integer &nParts)
{
  readPartsParallel(n,fName,nParts,1);
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::showExtrema(const Integer &n, Ostream &os)
//
// DESCRIPTION
//    Dump the max and min x,xp,y,yp,phi,dE values of
//    all particles to a stream
//
// PARAMETERS
//    n - Reference to a macro-particle herd
//    os  Reference to a stream to dump the info.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::showExtrema(const Integer &n, Ostream &os)
{
    MacroPart *mp;

   if ( (n & All_Mask) > nHerds) except (badHerdNo);

     mp= MacroPart::safeCast(mParts((n & All_Mask) - 1));
     mp->_findXYPExtrema();
     mp->_findDEExtrema();

     os  << "Min x (mm) = " << mp->_xMin << "\n"
         << "Max x (mm) = " << mp->_xMax << "\n"
         << "Min y (mm) = " << mp->_yMin << "\n"
         << "Max y (mm) = " << mp-> _yMax << "\n"
         << "Min phi (rad) = " << mp-> _phiMin << "\n"
         << "Max phi (rad) = " << mp-> _phiMax << "\n"
         << "Min dE (GeV) = " << mp-> _dEMin << "\n"
         << "Max dE (GeV) = " << mp-> _dEMax << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::dumpLostParts(const Integer &n,Ostream &os)
//
// DESCRIPTION
//    Dump the lost particle information of a herd
//
// PARAMETERS
//    n - Reference to a macro-particle herd
//    os  Reference to a stream to dump the info.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::dumpLostParts(const Integer &n, Ostream &os)
{
    MacroPart *mp;

   if ( (n & All_Mask) > nHerds) except (badHerdNo);

     mp= MacroPart::safeCast(mParts((n & All_Mask) - 1));


     os << "Number of lost particles = " << mp->_lostOnes._nLostMacros << "\n";
     if(!mp->_lostOnes._nLostMacros) return;

     os  << "x (mm)   xp(mrad)  y(mm)   yp(mrad)  phi(rad)   deltaE GeV)"
         << "Long Pos(m)   node    turn  \n";
     for (Integer i=1; i<= mp->_lostOnes._nLostMacros; i++)
       {
         os  << mp->_lostOnes._xLost(i) << "\t"
         << mp->_lostOnes._xpLost(i) << "\t"
         << mp->_lostOnes._yLost(i) << "\t"
         << mp->_lostOnes._ypLost(i) << "\t"
         << mp->_lostOnes._phiLost(i) << "\t"
         << mp->_lostOnes._deltaELost(i) << "\t"
         << mp->_lostOnes._LPosLost(i) << "\t"
         << mp->_lostOnes._nodeLost(i) << "\t"
         << mp->_lostOnes._turnLost(i) << "\n" ;
       }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::dumpLostPartsGlobal(const Integer &n,Ostream &os)
//
// DESCRIPTION
//    Dump the lost particle information of a herd for all CPUs
//    This routine should be used for a parallel run.
//
// PARAMETERS
//    n - Reference to a macro-particle herd
//    os  Reference to a stream to dump the info.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::dumpLostPartsGlobal(const Integer &n, Ostream &os)
{
    MacroPart *mp;

   if ( (n & All_Mask) > nHerds) except (badHerdNo);

   mp= MacroPart::safeCast(mParts((n & All_Mask) - 1));

   int nLostMacros = mp->_lostOnes._nLostMacros;
   int nLostMacrosGlobal = nLostMacros;
   if( nMPIsize > 1 ){
     MPI_Allreduce(&nLostMacros ,&nLostMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
   }

   if( nMPIrank == 0 ){
    os << "Number of lost particles = " << nLostMacrosGlobal << "\n";
   }

   if(!nLostMacrosGlobal) return;

   if( nMPIrank == 0 ){
    os  << "x (mm)   xp(mrad)  y(mm)   yp(mrad)  phi(rad)   deltaE GeV)"
        << "Long Pos(m)   node    turn  \n";
     for (Integer i=1; i<= mp->_lostOnes._nLostMacros; i++)
       {
         os  << mp->_lostOnes._xLost(i) << "\t"
         << mp->_lostOnes._xpLost(i) << "\t"
         << mp->_lostOnes._yLost(i) << "\t"
         << mp->_lostOnes._ypLost(i) << "\t"
         << mp->_lostOnes._phiLost(i) << "\t"
         << mp->_lostOnes._deltaELost(i) << "\t"
         << mp->_lostOnes._LPosLost(i) << "\t"
         << mp->_lostOnes._nodeLost(i) << "\t"
         << mp->_lostOnes._turnLost(i) << "\n" ;
       }
   }

   if( nMPIsize == 1) return;
   //parallel case ======MPI================start=======
       MPI_Status statusMPI ;
       int* nMacrosArr     = (int *) malloc (sizeof(int)*nMPIsize);
       int* nMacrosArr_MPI = (int *) malloc (sizeof(int)*nMPIsize);
       for(int i=0; i < nMPIsize; i++){
         nMacrosArr[i]=0;
         if( i == nMPIrank ){ nMacrosArr[i]=nLostMacros;}
       }
       MPI_Allreduce(nMacrosArr ,nMacrosArr_MPI,nMPIsize,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

       int nMacroChank = nMacrosArr_MPI[0];
       for(int i=0;i<nMPIsize;i++){
          if(nMacroChank < nMacrosArr_MPI[i]) nMacroChank = nMacrosArr_MPI[i];
       }
       double* buff_sr = (double *) malloc (sizeof(double)*nMacroChank*9);

       //sending and receiving macro-particles coordinates
       for( int i = 1; i < nMPIsize; i++){
	  if( i == nMPIrank ){
            for( int j = 0; j < nMacrosArr_MPI[i]; j++){
	        buff_sr[9*j + 0] = mp->_lostOnes._xLost(j+1);
	        buff_sr[9*j + 1] = mp->_lostOnes._xpLost(j+1);
	        buff_sr[9*j + 2] = mp->_lostOnes._yLost(j+1);
	        buff_sr[9*j + 3] = mp->_lostOnes._ypLost(j+1);
	        buff_sr[9*j + 4] = mp->_lostOnes._phiLost(j+1);
	        buff_sr[9*j + 5] = mp->_lostOnes._deltaELost(j+1);
	        buff_sr[9*j + 6] = mp->_lostOnes._LPosLost(j+1);
	        buff_sr[9*j + 7] = mp->_lostOnes._nodeLost(j+1);
	        buff_sr[9*j + 8] = mp->_lostOnes._turnLost(j+1);
	    }
            MPI_Send(buff_sr, 9*nMacrosArr_MPI[i], MPI_DOUBLE, 0, 1111, MPI_COMM_WORLD);
	  }
          if(nMPIrank == 0){
	    MPI_Recv(buff_sr, 9*nMacrosArr_MPI[i], MPI_DOUBLE, i, 1111, MPI_COMM_WORLD, &statusMPI);
            for( int j = 0; j < nMacrosArr_MPI[i]; j++){
	       os   <<buff_sr[9*j + 0]<< "\t"
	            <<buff_sr[9*j + 1]<< "\t"
	            <<buff_sr[9*j + 2]<< "\t"
	            <<buff_sr[9*j + 3]<< "\t"
	            <<buff_sr[9*j + 4]<< "\t"
    	            <<buff_sr[9*j + 5]<< "\t"
    	            <<buff_sr[9*j + 6]<< "\t"
    	            <<( (int) buff_sr[9*j + 7]) << "\t"
    	            <<( (int) buff_sr[9*j + 8]) << "\n";
            }
	  }
       }


     //free all memories
     free(nMacrosArr);
     free(nMacrosArr_MPI);
     free(buff_sr);
   //parallel case ======MPI================stop========

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showHerd(const Integer &n, Ostream &os)
//
// DESCRIPTION
//    Shows basic MacroPart information.
//
// PARAMETERS
//    n - Reference to a macro-particle herd
//    sout - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::showHerd(const Integer &n, Ostream &sout)
{

    MacroPart *mp;
   if ( (n & All_Mask) > nHerds) except (badHerdNo);

     mp= MacroPart::safeCast(mParts((n & All_Mask) - 1));


    sout << "\n MacroParticles:\n";

    sout << mpHeader;
    sout << mpLine;


    sout.setf(ios::left);

    for (Integer j=1; j <= mp->_nMacros; j++)
    {

        sout.setf(ios::left);
        sout << setw(4) << j << " " << setw(0);
        sout << " " << setw(12) << mp->_x(j) << setw(0);
        sout << " " << setw(12) << mp->_xp(j) << setw(0);
        sout << " " << setw(12) << mp->_y(j) << setw(0);
        sout << " " << setw(12) << mp->_yp(j) << setw(0);
        sout << " " << setw(12) << mp->_deltaE(j) << setw(0);
        sout << " " << setw(12) << mp->_phi(j) << setw(0);
        sout << "\n";

    }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Output::showHerdGlobal(const Integer &n, Ostream &os)
//
// DESCRIPTION
//    Shows basic MacroPart information.
//
// PARAMETERS
//    n - Reference to a macro-particle herd
//    sout - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::showHerdGlobal(const Integer &n, Ostream &sout)
{

    MacroPart *mp;
   if ( (n & All_Mask) > nHerds) except (badHerdNo);

     mp= MacroPart::safeCast(mParts((n & All_Mask) - 1));

     if(nMPIrank == 0){
       sout << "\n MacroParticles:\n";
       sout << mpHeader;
       sout << mpLine;
       sout.setf(ios::left);
     }

     int count_global = 0;

     if(nMPIrank == 0){
       for (Integer j=1; j <= mp->_nMacros; j++){
        sout.setf(ios::left);
        sout << setw(4) << j << " " << setw(0);
        sout << " " << setw(12) << mp->_x(j) << setw(0);
        sout << " " << setw(12) << mp->_xp(j) << setw(0);
        sout << " " << setw(12) << mp->_y(j) << setw(0);
        sout << " " << setw(12) << mp->_yp(j) << setw(0);
        sout << " " << setw(12) << mp->_deltaE(j) << setw(0);
        sout << " " << setw(12) << mp->_phi(j) << setw(0);
        sout << "\n";
        count_global++;
       }
       if( nMPIsize == 1) return;
     }

     //parallel case ======MPI================start=======
       MPI_Status statusMPI ;
       int* nMacrosArr     = (int *) malloc (sizeof(int)*nMPIsize);
       int* nMacrosArr_MPI = (int *) malloc (sizeof(int)*nMPIsize);
       for(int i=0; i < nMPIsize; i++){
         nMacrosArr[i]=0;
         if( i == nMPIrank ){ nMacrosArr[i]=mp->_nMacros;}
       }
       MPI_Allreduce(nMacrosArr ,nMacrosArr_MPI,nMPIsize,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

       int nMacroChank = nMacrosArr_MPI[0];
       for(int i=0;i<nMPIsize;i++){
          if(nMacroChank < nMacrosArr_MPI[i]) nMacroChank = nMacrosArr_MPI[i];
       }
       double* buff_sr = (double *) malloc (sizeof(double)*nMacroChank*6);

       //sending and receiving macro-particles coordinates
       for( int i = 1; i < nMPIsize; i++){
	  if( i == nMPIrank ){
            for( int j = 0; j < nMacrosArr_MPI[i]; j++){
	        buff_sr[6*j + 0] = mp->_x(j+1);
	        buff_sr[6*j + 1] = mp->_xp(j+1);
	        buff_sr[6*j + 2] = mp->_y(j+1);
	        buff_sr[6*j + 3] = mp->_yp(j+1);
	        buff_sr[6*j + 4] = mp->_deltaE(j+1);
	        buff_sr[6*j + 5] = mp->_phi(j+1);
	    }
            MPI_Send(buff_sr, 6*nMacrosArr_MPI[i], MPI_DOUBLE, 0, 1111, MPI_COMM_WORLD);
	  }
          if(nMPIrank == 0){
	    MPI_Recv(buff_sr, 6*nMacrosArr_MPI[i], MPI_DOUBLE, i, 1111, MPI_COMM_WORLD, &statusMPI);
            for( int j = 0; j < nMacrosArr_MPI[i]; j++){
                    sout.setf(ios::left);
                    sout << setw(4) << (count_global+1) << " " << setw(0);
		    sout << " " << setw(12) <<buff_sr[6*j + 0]<< setw(0);
		    sout << " " << setw(12) <<buff_sr[6*j + 1]<< setw(0);
		    sout << " " << setw(12) <<buff_sr[6*j + 2]<< setw(0);
		    sout << " " << setw(12) <<buff_sr[6*j + 3]<< setw(0);
		    sout << " " << setw(12) <<buff_sr[6*j + 4]<< setw(0);
		    sout << " " << setw(12) <<buff_sr[6*j + 5]<< setw(0);
                    sout << "\n";
                    count_global++;
            }
	  }
       }

       //free all memories
       free(nMacrosArr);
       free(nMacrosArr_MPI);
       free(buff_sr);
     //parallel case ======MPI================stop========

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::showSyncPart(Ostream &os)
//
// DESCRIPTION
//    Shows basic Sync Part information.
//
// PARAMETERS
//    sout - Stream to send output to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::showSyncPart(Ostream &sout)
{

   SyncPart *sp;

     sp= SyncPart::safeCast(syncP(0));


    sout << "\n Synchronous Particle Info:\n";

    sout << "Kinetic Energy [GeV] = " << sp->_eKinetic << "\n";
    sout << "Phase [rad] = " << sp->_phase << "\n";
    sout << "Charge number = " << sp->_charge << "\n";
    sout << "Mass [AMU] = " << sp->_mass << "\n";
    sout << "beta = " << sp->_betaSync << "\n";
    sout << "gamma = " << sp->_gammaSync << "\n";

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::syncNMacs(Integer &)
//
// DESCRIPTION
//    collects the total number of macroparticles for all
//    nodes for a parallel run
//
// PARAMETERS
//    np = the number of macros
//         (input and output for all CPUs)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::syncNMacs(Integer &np)
{
    if(nMPIsize < 2) { return;};

    int np_local  = np;
    int np_global = 0;

    MPI_Allreduce ( &np_local, &np_global, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    np = np_global;

}




///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Particles::readPartsParallel(const Integer &n, String &fName,
//                       const Integer nParts const Integer &type_accsim,)
//
// DESCRIPTION
//    Read the x,xp,y,yp,phi,dE values of all particles from a stream
//    and distributes them between CPUs evenly. The reading is performed
//    by one CPU that sends information to the others.
//
// PARAMETERS
//    n     - Reference to a macro-particle herd
//    fName - is  the name of the file to rread from
//    nParts - is the number of parts to read.
//    type_accsim - is not equal to 1 if it is not accsim input file
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Particles::readPartsParallel(const Integer &n, const String &fName,
				  const Integer &nParts, const Integer &type_accsim)
{

  MacroPart *mp;
  ifstream is;

  if(nMPIrank == 0){
    is.open(fName, ios::in);
    if (is.bad()) except(badReadMacroFile);
  }

  //define chunk size for reading particles' coordinates
  //n_c number of particles in each cpu
  int chunk_size = 5000;
  int n_c = chunk_size/nMPIsize;
  if(n_c < 1) n_c = 1;
  chunk_size = n_c * nMPIsize;

  double* arr_0 = (double *) malloc (sizeof(double) * 6*chunk_size);


  if ( (n & All_Mask) > nHerds) except (badHerdNo);

  mp= MacroPart::safeCast(mParts((n & All_Mask) - 1));


  int info_stop = 0;
  int nP = 0;

  while(info_stop != 1){

    int nn = 0;

    if(nMPIrank == 0){

      while(!is.eof() && (nn < chunk_size))
	{
	  if(is >> arr_0[0+nn*6] >> arr_0[1+nn*6] >>
	     arr_0[2+nn*6] >> arr_0[3+nn*6] >>
	     arr_0[4+nn*6]  >> arr_0[5+nn*6])
	    {
	      if(type_accsim == 1){
                  arr_0[4+nn*6] *= Consts::pi/180.;// temp for  accsim compatibility
                  arr_0[5+nn*6] *= 1.e-3;// temp for  accsim compatibility
	      }
	      nn++;
	    }
	  else
	    if(!is.eof()) except(badReadMacroFile);
	}

      if((nP+nn) > nParts) {
	info_stop = 1;
	nn = nParts - nP;
      }
      nP += nn;
      if(is.eof()) info_stop = 1;
    }

    if(nMPIsize > 1){
      MPI_Bcast ( &nn,              1, MPI_INT,    0, MPI_COMM_WORLD );
      MPI_Bcast ( arr_0, 6*chunk_size, MPI_DOUBLE, 0, MPI_COMM_WORLD );
    }

    int i_start = nMPIrank;
    int i_stop  = nn;

    for(int i = i_start; i < i_stop; i += nMPIsize){
      addMacroPartBase2(n, arr_0[0+i*6] , arr_0[1+i*6] , arr_0[2+i*6] ,
			arr_0[3+i*6],  arr_0[5+i*6] ,  arr_0[4+i*6] );
    }

    if(nMPIsize > 1){
      MPI_Bcast ( &info_stop, 1, MPI_INT, 0, MPI_COMM_WORLD  );
    }

  }

  //free resources
  free(arr_0);

  if(nMPIrank == 0){
    is.close();
  }
}

Real Particles::getSyncPartEnergy()
{
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));
	return mp->_syncPart._eKinetic;
}

////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========START===============
////////////////////////////////////////////////////////////////////////////////

Void Particles::bumpXYcoords(Real &x_bump, Real &y_bump)
{
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));

   for(int ip = 1; ip <= mp->_nMacros; ip++){
     mp->_xp(ip) += x_bump;
     mp->_yp(ip) += y_bump;
   }
}


////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========STOP================
////////////////////////////////////////////////////////////////////////////////
