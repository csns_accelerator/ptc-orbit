/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Error.mod
//
// AUTHOR
//    Steven Bunch, University of Tennessee, bunchsc@sns.gov
//
// CREATED
//    6/17/02
//
//  DESCRIPTION
//    Module descriptor file for the Error module
//    This module contains information about errors
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Error - "Error Module"
                 - "Written by Steven Bunch, ORNL, bunchsc@sns.gov"
{
  Inherits Consts, Particles, Ring, TransMap, TeaPot;

public:

  Void ctor()
                  - "Constructor for the Error module"
                  - "Initializes build values";

  Integer
    nErrNodes - "Number of error nodes";

  Void addCoordDisplacement(const String &name,
                   const Integer &order,
	           const Real &dx, const Real &dxp,
                   const Real &dy, const Real &dyp,
                   const Real &dphi, const Real &ddeltaE)
                  - "Adds a generalized coordinate displacement";

  Void addQuadKicker(const String &name,
                     const Integer &order,
                     const Real &k)
                  - "Adds a quadrupole kicker";

  Void addQuadKickerOsc(const String &name,
                         const Integer &order,
                         const Real &k, const Real &freq,
                         const Real &phase)
                  - "Adds a time dependent quadrupole kicker";

  Void addDipoleKickerOsc(const String &name,
                         const Integer &order,
                         const Real &k, const Real &freq,
                         const Real &phase)
                  - "Adds a time dependent dipole kicker";

  Void addLongDisplacement(const String &name, const Integer &order,
                   const Real &dS)
                  - "Adds a longitudinal displacement";

  Void addStraightRotationXY(const String &name, const Integer &order,
	           const Real &anglexy)
                  - "Adds an xy rotation in straight section";

  Void addStraightRotationXSI(const String &name, const Integer &order,
	           const Real &anglexsi, const Real &length)
                  - "Adds initial xs rotation in straight section";

  Void addStraightRotationXSF(const String &name, const Integer &order,
	           const Real &anglexsf, const Real &length)
                  - "Adds final xs rotation in straight section";

  Void addStraightRotationYSI(const String &name, const Integer &order,
	           const Real &angleysi, const Real &length)
                  - "Adds initial ys rotation in straight section";

  Void addStraightRotationYSF(const String &name, const Integer &order,
	           const Real &angleysf, const Real &length)
                  - "Adds final ys rotation in straight section";

  Void addBendFieldI(const String &name, const Integer &order,
	           const Real &drho)
                  - "Adds a field error";

  Void addBendFieldF(const String &name, const Integer &order,
	           const Real &drho)
                  - "Adds a field error";

  Void addBendDisplacementXI(const String &name, const Integer &order,
	           const Real &anglexi, const Real &disp)
                  - "Adds a x initial dipole displacement";

  Void addBendDisplacementXF(const String &name, const Integer &order,
	           const Real &anglexf, const Real &disp)
                  - "Adds a x final dipole displacement";

  Void addBendDisplacementYI(const String &name, const Integer &order,
	           const Real &disp)
                  - "Adds a y initial dipole displacement";

  Void addBendDisplacementYF(const String &name, const Integer &order,
	           const Real &disp)
                  - "Adds a y final dipole displacement";

  Void addBendDisplacementLI(const String &name, const Integer &order,
	           const Real &angleli, const Real &disp)
                  - "Adds a longitudinal initial dipole displacement";

  Void addBendDisplacementLF(const String &name, const Integer &order,
	           const Real &anglelf, const Real &disp)
                  - "Adds a longitudinal final dipole displacement";

  Void addRotationI(const String &name, const Integer &order,
	           const Real &anglei, const Real &rhoi,
                   const Real &theta, const Real &length,
                   const String &et, const String &type)
                  - "Adds an initial rotation";

  Void addRotationF(const String &name, const Integer &order,
	           const Real &anglef, const Real &rhoi,
                   const Real &theta, const Real &length,
                   const String &et, const String &type)
                  - "Adds a final rotation";

  Void addTransDispError(const String &name,
                   const Integer &order1, const Integer &order2,
                   const Real &dx, const Real &dy)
                  - "Adds a displacement error node";

  Void addTransDispErrorSet(const String &name, const String &dist,
                   RealVector &param, const String &et,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax,
                   const Integer &order)
                  - "Adds displacement errors to a set of nodes";

  Void addLongDispError(const String &name,
                   const Integer &order1, const Integer &order2,
                   const Real &dS)
                  - "Adds a longitudinal displacement error node";

  Void addLongDispErrorSet(const String &name, const String &dist,
                   RealVector &param, const String &et,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax,
                   const Integer &order)
                  - "Adds longitudinal displacement errors to a set of nodes";

  Void addStraightRotationError(const String &name,
                   const Integer &order1, const Integer &order2,
	           const Real &angle, const String &type)
                  - "Adds a rotation error node";

  Void addStraightRotationErrorSet(const String &name, const String &dist,
                   RealVector &param, const String &et,
                   const String &type,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax,
                   const Integer &order)
                  - "Adds rotation errors to a set of nodes";

  Void addMultFieldError(const String &name,
                   const Integer &order1, const Integer &order2,
                   const String &et,
                   const Real &tilt,
                   const Integer &pole,
                   const Real &kl,
                   const Integer &skew,
                   const Real &err,
		   const Integer &TPsubindex,
		   const Integer &nsteps,
                   const Integer &fringeIN, const Integer &fringeOUT)
                  - "Adds a multipole field error node";

  Void addMultFieldErrorSet(const String &name,
                   const String &dist, RealVector &param,
                   const String &et,
                   const Real &tilt,
                   const Integer &pole,
                   const Real &kl,
                   const Integer &skew,
		   const Integer &TPsubindex,
                   const Integer &nsteps,
                   const Integer &fringeIN, const Integer &fringeOUT,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax)
                  - "Adds a multipole field error node set";

  Void addQuadFieldError(const String &name,
                   const Integer &order1, const Integer &order2,
                   const String &et,
                   const Real &tilt,
                   const Real &kq,
                   const Real &err,
		   const Integer &TPsubindex,
		   const Integer &nsteps,
                   const Integer &fringeIN, const Integer &fringeOUT)
                  - "Adds a quadrupole field error node";

  Void addQuadFieldErrorSet(const String &name,
                   const String &dist, RealVector &param,
                   const String &et,
                   const Real &tilt,
                   const Real &kq,
		   const Integer &TPsubindex,
                   const Integer &nsteps,
                   const Integer &fringeIN, const Integer &fringeOUT,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax)
                  - "Adds a quadrupole field error node set";

  Void addBendFieldError(const String &name,
                   const Integer &order1, const Integer &order2,
                   const String &et,
                   const Real &tilt,
                   const Real &theta,
                   const Real &ea1, const Real &ea2,
                   const Real &err,
		   const Integer &nsteps,
                   const Integer &fringeIN, const Integer &fringeOUT)
                  - "Adds a bend field error node";

  Void addBendFieldErrorSet(const String &name,
                   const String &dist, RealVector &param,
                   const String &et,
                   const Real &tilt,
                   const Real &theta,
                   const Real &ea1, const Real &ea2,
                   const Integer &nsteps,
                   const Integer &fringeIN, const Integer &fringeOUT,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax,
                   const Integer &order)
                  - "Adds a bend field error node set";

  Void addSolnFieldError(const String &name,
                   const Integer &order1, const Integer &order2,
                   const String &et,
                   const Real &B,
                   const Real &err,
		   const Integer &TPsubindex,
		   const Integer &nsteps)
                  - "Adds a solenoid field error node";

  Void addSolnFieldErrorSet(const String &name,
                   const String &dist, RealVector &param,
                   const String &et,
                   const Real &B,
		   const Integer &TPsubindex,
                   const Integer &nsteps,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax)
                  - "Adds a solenoid field error node set";

  Void addBendDisplacementError(const String &name,
                   const Integer &order1, const Integer &order2,
	           const Real &angle, const Real &disp,
                   const String &type)
                  - "Adds a dipole disp error node";

  Void addBendDisplacementErrorSet(const String &name, const String &dist,
                   RealVector &param, const String &et,
                   const Real &angle, const String &type,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax,
                   const Integer &order)
                  - "Adds a dipole disp error node set";

  Void addRotationError(const String &name,
                   const Integer &order1, const Integer &order2,
	           const Real &angle, const Real &rhoi,
                   const Real &theta, const String &et,
                   const String &type)
                  - "Adds a rotation error node";

  Void addRotationErrorSet(const String &name, const String &dist,
                   RealVector &param, const Real &rhoi,
                   const Real &theta, const String &et,
                   const String &type,
                   const Integer &nTransMapMin,
                   const Integer &nTransMapMax,
                   const Integer &order)
                  - "Adds rotation errors to a set of nodes";

  Integer addDipoleCorrectorNode(const String &name,
                   const Integer &order,
	           const Real &dxp, const Real &dyp)
                  - "Adds a Dipole Corrector Node";

  Void setDipoleCorrectorNodeVal(const Integer &nodeindex,
	           const Real &dp, const String &XorY)
                  - "Sets Value";

  Void showErrors(Ostream &os)
                  - "Prints errors to a stream";

  Real drand(const Real &r)
                  - "Random number generator";

  Real derf(const Real &x)
                  - "Evaluates error function at x";

  Real root_normal(const Real &errtest, const Real &ymin,
                   const Real &ymax, const Real &tol)
                  - "Finds root";

  Real getGauss(const Real &mean, const Real &sigma,
                   const Real &cutoff)
                  - "Gaussian Distribution";
}
