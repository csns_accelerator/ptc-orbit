#if !defined(__HerdCoords__)
#define __HerdCoords__

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    HerdCoords
//
// INHERITANCE RELATIONSHIPS
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for containing Herd particle coordinates as doubles.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

#define nPartDim 200000

class HerdCoords
{
  public:
  HerdCoords();
  ~HerdCoords(){};

  void setCoord(double &x_map, double &xp_map,
                double &y_map, double &yp_map,
                double &phi_map, double &deltaE_map);

  int _nMacros;
  double _x_map[nPartDim];
  double _xp_map[nPartDim];
  double _y_map[nPartDim];
  double _yp_map[nPartDim];
  double _phi_map[nPartDim];
  double _deltaE_map[nPartDim];

};

#endif   // __HerdCoords__
