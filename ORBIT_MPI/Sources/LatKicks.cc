/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//  LatKicks.cc
//
// AUTHOR
//  Jeff Holmes
//  ORNL, jzh@ornl.gov
//
// CREATED
//  5/03/99
//
// DESCRIPTION
//  Module descriptor file for the Transverse Symplectic Kick module.
//  This module contains source for the LatKicks related info.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "LatKicks.h"
#include "RealMat.h"
#include "SCLTypes.h"
#include "StreamType.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include <fstream>
#include <iomanip>
#include <cmath>

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

#define KickTermsDim 56
#define xyunits 1.e+03
#define xpypunits 1.e+03
Array(ObjectPtr) LatKicksPointers;
extern Array(ObjectPtr) mParts, nodes, tMaps;
extern Array(ObjectPtr) syncP;

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  LatKickNodes
//
// INHERITANCE RELATIONSHIPS
//  LatKickNodes -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//  None.
//
// DESCRIPTION
//  This is a base class for transverse symplectic kick implementations.
//
// Kick stuff:
//
//   nLatKickNodes  Number of nodes in kick file.
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class LatKickNodes : public Node {
   Declare_Standard_Members(LatKickNodes, Node);
   public:
      LatKickNodes(const String &n, const Integer &order) :
                   Node(n, 0., order)
      {
      }

      ~LatKickNodes()
      {     
      }
      virtual Void nameOut(String &wname) { wname = _name; }
      virtual Void _updatePartAtNode(MacroPart &mp)=0;
      virtual Void _nodeCalculator(MacroPart &mp)=0;

   protected:

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LatKickNodes
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(LatKickNodes, Node);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  TKickNodes
//
// INHERITANCE RELATIONSHIPS
//  TKickNodes -> LatKickNodes -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//  None
//
// DESCRIPTION
//  This is a class for handling lattice nonlinearities and errors.
//
// PUBLIC MEMBERS
//
//  xkicks        Number of terms to kick x
//  x_xpow[]      x power of x kick
//  xp_xpow[]     xp power of x kick
//  y_xpow[]      y power of x kick
//  yp_xpow[]     yp power of x kick
//  del_xpow[]    delta power of x kick
//  xkcoeff[]     coefficients of x kick before and after matrix
//
//  xpkicks       Number of terms to kick xp
//  x_xppow[]     x power of xp kick
//  xp_xppow[]    xp power of xp kick
//  y_xppow[]     y power of xp kick
//  yp_xppow[]    yp power of xp kick
//  del_xppow[]   delta power of xp kick
//  xpkcoeff[]    coefficients of xp kick before and after matrix
//
//  ykicks        Number of terms to kick y
//  x_ypow[]      x power of y kick
//  xp_ypow[]     xp power of y kick
//  y_ypow[]      y power of y kick
//  yp_ypow[]     yp power of y kick
//  del_ypow[]    delta power of y kick
//  ykcoeff[]     coefficients of y kick before and after matrix
//
//  ypkicks       Number of terms to kick yp
//  x_yppow[]     x power of yp kick
//  xp_yppow[]    xp power of yp kick
//  y_yppow[]     y power of yp kick
//  yp_yppow[]    yp power of yp kick
//  del_yppow[]   delta power of yp kick
//  ypkcoeff[]    coefficients of yp kick before and after matrix
//
// PROTECTED MEMBERS
//  None
//
// PRIVATE MEMBERS
//  None
//
///////////////////////////////////////////////////////////////////////////

class TKickNodes : public LatKickNodes
{
   Declare_Standard_Members(TKickNodes, LatKickNodes);
   public:
      TKickNodes(const String &n, const Integer &order,
                 const Integer &xkicks,
                 const Vector(Integer) &x_xpow,
                 const Vector(Integer) &xp_xpow,
                 const Vector(Integer) &y_xpow,
                 const Vector(Integer) &yp_xpow,
                 const Vector(Integer) &del_xpow,
                 const Vector(Real) &xcoeff,
                 const Integer &xpkicks,
                 const Vector(Integer) &x_xppow,
                 const Vector(Integer) &xp_xppow,
                 const Vector(Integer) &y_xppow,
                 const Vector(Integer) &yp_xppow,
                 const Vector(Integer) &del_xppow,
                 const Vector(Real) &xpcoeff,
                 const Integer &ykicks,
                 const Vector(Integer) &x_ypow,
                 const Vector(Integer) &xp_ypow,
                 const Vector(Integer) &y_ypow,
                 const Vector(Integer) &yp_ypow,
                 const Vector(Integer) &del_ypow,
                 const Vector(Real) &ycoeff,
                 const Integer &ypkicks,
                 const Vector(Integer) &x_yppow,
                 const Vector(Integer) &xp_yppow,
                 const Vector(Integer) &y_yppow,
                 const Vector(Integer) &yp_yppow,
                 const Vector(Integer) &del_yppow,
                 const Vector(Real) &ypcoeff
                ) : LatKickNodes(n, order)
      {
         Integer k;
         Real units;

         _xkicks = xkicks;

         if (_xkicks != 0)
         {
            _x_xpow.resize(_xkicks);
            _xp_xpow.resize(_xkicks);
            _y_xpow.resize(_xkicks);
            _yp_xpow.resize(_xkicks);
            _del_xpow.resize(_xkicks);

            for (k = 1; k <= xkicks; k++)
            {
               _x_xpow(k) = x_xpow(k);
               _xp_xpow(k) = xp_xpow(k);
               _y_xpow(k) = y_xpow(k);
               _yp_xpow(k) = yp_xpow(k);
               _del_xpow(k) = del_xpow(k);

               units = xyunits / ( pow(xyunits,_x_xpow(k)) * 
                                   pow(xpypunits,_xp_xpow(k)) *
                                   pow(xyunits,_y_xpow(k)) * 
                                   pow(xpypunits,_yp_xpow(k)) );
               _xcoeff(k) = xcoeff(k) * units;
            }
         }

         _xpkicks = xpkicks;

         if (_xpkicks != 0)
         {
            _x_xppow.resize(_xpkicks);
            _xp_xppow.resize(_xpkicks);
            _y_xppow.resize(_xpkicks);
            _yp_xppow.resize(_xpkicks);
            _del_xppow.resize(_xpkicks);

            for (k = 1; k <= xpkicks; k++)
            {

               _x_xppow(k) = x_xppow(k);
               _xp_xppow(k) = xp_xppow(k);
               _y_xppow(k) = y_xppow(k);
               _yp_xppow(k) = yp_xppow(k);
               _del_xppow(k) = del_xppow(k);

               units = xpypunits / ( pow(xyunits,_x_xppow(k)) * 
                                     pow(xpypunits,_xp_xppow(k)) *
                                     pow(xyunits,_y_xppow(k)) * 
                                     pow(xpypunits,_yp_xppow(k)) );
               _xpcoeff(k) = xpcoeff(k) * units;
            }
         }

         _ykicks = ykicks;

         if (_ykicks != 0)
         {
            _x_ypow.resize(_ykicks);
            _xp_ypow.resize(_ykicks);
            _y_ypow.resize(_ykicks);
            _yp_ypow.resize(_ykicks);
            _del_ypow.resize(_ykicks);

            for (k = 1; k <= ykicks; k++)
            {
               _x_ypow(k) = x_ypow(k);
               _xp_ypow(k) = xp_ypow(k);
               _y_ypow(k) = y_ypow(k);
               _yp_ypow(k) = yp_ypow(k);
               _del_ypow(k) = del_ypow(k);

               units = xyunits / ( pow(xyunits,_x_ypow(k)) * 
                                   pow(xpypunits,_xp_ypow(k)) *
                                   pow(xyunits,_y_ypow(k)) * 
                                   pow(xpypunits,_yp_ypow(k)) );
               _ycoeff(k) = ycoeff(k) * units;
            }
         }

         _ypkicks = ypkicks;

         if (_ypkicks != 0)
         {
            _x_yppow.resize(_ypkicks);
            _xp_yppow.resize(_ypkicks);
            _y_yppow.resize(_ypkicks);
            _yp_yppow.resize(_ypkicks);
            _del_yppow.resize(_ypkicks);

            for (k = 1; k <= ypkicks; k++)
            {

               _x_yppow(k) = x_yppow(k);
               _xp_yppow(k) = xp_yppow(k);
               _y_yppow(k) = y_yppow(k);
               _yp_yppow(k) = yp_yppow(k);
               _del_yppow(k) = del_yppow(k);

               units = xpypunits / ( pow(xyunits,_x_yppow(k)) * 
                                     pow(xpypunits,_xp_yppow(k)) *
                                     pow(xyunits,_y_yppow(k)) * 
                                     pow(xpypunits,_yp_yppow(k)) );
               _ypcoeff(k) = ypcoeff(k) * units;
            }
         }
      }

      ~TKickNodes()
      {     
      }
      Void _updatePartAtNode(MacroPart &mp);
      Void _nodeCalculator(MacroPart &mp);

      Integer _xkicks;
      Vector(Integer) _x_xpow, _xp_xpow, _y_xpow, _yp_xpow, _del_xpow;
      Vector(Real) _xcoeff;
      Integer _xpkicks;
      Vector(Integer) _x_xppow, _xp_xppow, _y_xppow, _yp_xppow, _del_xppow;
      Vector(Real) _xpcoeff;
      Integer _ykicks;
      Vector(Integer) _x_ypow, _xp_ypow, _y_ypow, _yp_ypow, _del_ypow;
      Vector(Real) _ycoeff;
      Integer _ypkicks;
      Vector(Integer) _x_yppow, _xp_yppow, _y_yppow, _yp_yppow, _del_yppow;
      Vector(Real) _ypcoeff;

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TKickNodes
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TKickNodes, LatKickNodes);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    TKickNodes::NodeCalculator
//
// DESCRIPTION
//    Sets up the TKickNodes node calculation
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TKickNodes::_nodeCalculator(MacroPart &mp)
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    TKickNodes::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//    a MacroParticle with a symplectic transverse TKick. 
//    The Transverse space charge kick is
//    added to each macro particle here.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TKickNodes::_updatePartAtNode(MacroPart &mp)
{
   Integer j, k;
   Real xfac, xpfac, yfac, ypfac, delfac;
  
// Assumes the factors are scaled for x,y in mm and x',y' in mm-mrad.
// This means that if coefficients are calculated in m and m-rad, they
// must be multiplied by 1.e3 for x,y equations and 1.e6 for x',y'.
// Also, the coefficients must be divided by 1.e3 for each power of x,y
// and by 1.e6 for each power of x',y'.  This routine assumes this has
// been done.

   for(j=1; j<= mp._nMacros; j++)
   {
     for (k = 1; k <= _xkicks; k++)
     {
       if (_xcoeff(k) != 0.)
       {
         xfac = xpfac = yfac = ypfac = delfac = 1.;
         if (_x_xpow(k) != 0) xfac = pow(mp._x(j) , _x_xpow(k));
         if (_xp_xpow(k) != 0) xpfac = pow(mp._xp(j) , _xp_xpow(k));
         if (_y_xpow(k) != 0) yfac = pow(mp._y(j) , _y_xpow(k));
         if (_yp_xpow(k) != 0) ypfac = pow(mp._yp(j) , _yp_xpow(k));
         if (_del_xpow(k) != 0) delfac = pow(mp._dp_p(j) , _del_xpow(k));
         mp._x(j) += _xcoeff(k) * xfac * xpfac * yfac * ypfac * delfac;
       }
     }

     for (k = 1; k <= _xpkicks; k++)
     {
       if (_xpcoeff(k) != 0.)
       {
         xfac = xpfac = yfac = ypfac = delfac = 1.;
         if (_x_xppow(k) != 0) xfac = pow(mp._x(j) , _x_xppow(k));
         if (_xp_xppow(k) != 0) xpfac = pow(mp._xp(j) , _xp_xppow(k));
         if (_y_xppow(k) != 0) yfac = pow(mp._y(j) , _y_xppow(k));
         if (_yp_xppow(k) != 0) ypfac = pow(mp._yp(j) , _yp_xppow(k));
         if (_del_xppow(k) != 0) delfac = pow(mp._dp_p(j) , _del_xppow(k));
         mp._xp(j) += _xpcoeff(k) * xfac * xpfac * yfac * ypfac * delfac;
       }
     }

     for (k = 1; k <= _ykicks; k++)
     {
       if (_ycoeff(k) != 0.)
       {
         xfac = xpfac = yfac = ypfac = delfac = 1.;
         if (_x_ypow(k) != 0) xfac = pow(mp._x(j) , _x_ypow(k));
         if (_xp_ypow(k) != 0) xpfac = pow(mp._xp(j) , _xp_ypow(k));
         if (_y_ypow(k) != 0) yfac = pow(mp._y(j) , _y_ypow(k));
         if (_yp_ypow(k) != 0) ypfac = pow(mp._yp(j) , _yp_ypow(k));
         if (_del_ypow(k) != 0) delfac = pow(mp._dp_p(j) , _del_ypow(k));
         mp._y(j) += _ycoeff(k) * xfac * xpfac * yfac * ypfac * delfac;
       }
     }

     for (k = 1; k <= _ypkicks; k++)
     {
       if (_ypcoeff(k) != 0.)
       {
         xfac = xpfac = yfac = ypfac = delfac = 1.;
         if (_x_yppow(k) != 0) xfac = pow(mp._x(j) , _x_yppow(k));
         if (_xp_yppow(k) != 0) xpfac = pow(mp._xp(j) , _xp_yppow(k));
         if (_y_yppow(k) != 0) yfac = pow(mp._y(j) , _y_yppow(k));
         if (_yp_yppow(k) != 0) ypfac = pow(mp._yp(j) , _yp_yppow(k));
         if (_del_yppow(k) != 0) delfac = pow(mp._dp_p(j) , _del_yppow(k));
         mp._yp(j) += _ypcoeff(k) * xfac * xpfac * yfac * ypfac * delfac;
       }
     }
   }
}

///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE LatKicks
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//  LatKicks::ctor
//
// DESCRIPTION
//  Initializes the various LatKicks related constants.
//
// PARAMETERS
//  None
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LatKicks::ctor()
{
//  Set some initial values

    nLatKickNodes = 0;
 }
    
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  LatKicks::addTKickPair
//
// DESCRIPTION
//  Adds a Transverse Symplectic Kick Node.
//  Adds the set to be second order symplectic in the integration.
//
// PARAMETERS
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LatKicks::addTKickPair(const String &n, const Integer &o,
                            const Integer &xkicks,
                            const Vector(Integer) &x_xpow,
                            const Vector(Integer) &xp_xpow,
                            const Vector(Integer) &y_xpow,
                            const Vector(Integer) &yp_xpow,
                            const Vector(Integer) &del_xpow,
                            const Vector(Real) &xcflow,
                            const Vector(Real) &xcfhi,
                            const Integer &xpkicks,
                            const Vector(Integer) &x_xppow,
                            const Vector(Integer) &xp_xppow,
                            const Vector(Integer) &y_xppow,
                            const Vector(Integer) &yp_xppow,
                            const Vector(Integer) &del_xppow,
                            const Vector(Real) &xpcflow,
                            const Vector(Real) &xpcfhi,
                            const Integer &ykicks,
                            const Vector(Integer) &x_ypow,
                            const Vector(Integer) &xp_ypow,
                            const Vector(Integer) &y_ypow,
                            const Vector(Integer) &yp_ypow,
                            const Vector(Integer) &del_ypow,
                            const Vector(Real) &ycflow,
                            const Vector(Real) &ycfhi,
                            const Integer &ypkicks,
                            const Vector(Integer) &x_yppow,
                            const Vector(Integer) &xp_yppow,
                            const Vector(Integer) &y_yppow,
                            const Vector(Integer) &yp_yppow,
                            const Vector(Integer) &del_yppow,
                            const Vector(Real) &ypcflow,
                            const Vector(Real) &ypcfhi
                           )
{
   if (nNodes == nodes.size())
      nodes.resize(nNodes + 100);
   if (nLatKickNodes == LatKicksPointers.size())
      LatKicksPointers.resize(nLatKickNodes + 100);
  
   //NOTE: Add coding for scaling of coefficients here!!!

   nNodes += 2;
   nLatKickNodes += 2;
   nodes(nNodes - 2) = new TKickNodes(n, o - 2,
                                     xkicks,
                                     x_xpow,
                                     xp_xpow,
                                     y_xpow,
                                     yp_xpow,
                                     del_xpow,
                                     xcflow,
                                     xpkicks,
                                     x_xppow,
                                     xp_xppow,
                                     y_xppow,
                                     yp_xppow,
                                     del_xppow,
                                     xpcflow,
                                     ykicks,
                                     x_ypow,
                                     xp_ypow,
                                     y_ypow,
                                     yp_ypow,
                                     del_ypow,
                                     ycflow,
                                     ypkicks,
                                     x_yppow,
                                     xp_yppow,
                                     y_yppow,
                                     yp_yppow,
                                     del_yppow,
                                     ypcflow
                                    );
   nodes(nNodes - 1) = new TKickNodes(n, o + 2,
                                     xkicks,
                                     x_xpow,
                                     xp_xpow,
                                     y_xpow,
                                     yp_xpow,
                                     del_xpow,
                                     xcfhi,
                                     xpkicks,
                                     x_xppow,
                                     xp_xppow,
                                     y_xppow,
                                     yp_xppow,
                                     del_xppow,
                                     xpcfhi,
                                     ykicks,
                                     x_ypow,
                                     xp_ypow,
                                     y_ypow,
                                     yp_ypow,
                                     del_ypow,
                                     ycfhi,
                                     ypkicks,
                                     x_yppow,
                                     xp_yppow,
                                     y_yppow,
                                     yp_yppow,
                                     del_yppow,
                                     ypcfhi
                                    );
   LatKicksPointers(nLatKickNodes - 2) = nodes(nNodes - 2);
   LatKicksPointers(nLatKickNodes - 1) = nodes(nNodes - 1);
   
   nodesInitialized = 0;
}
    
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  LatKicks::addTKickSet
//
// DESCRIPTION
//  Reads TKick file and
//  adds a set of Transverse Symplectic Kick Nodes,
//  two for every matrix element present.
//  Adds the set to be second order symplectic in the integration.
//
// PARAMETERS
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LatKicks::addTKickSet(const String &fileName)
{
   Node *n1;
   MapBase *tm;
   Integer n;
   String wname;

   if (nTransMaps < 1) except(tooFewTransMaps);

   char num[10];
   String cons("TKicks");

   int j, k, elt_num;
   String s_elt_num;

   Integer xkicks;
   Vector(Integer) x_xpow(KickTermsDim), xp_xpow(KickTermsDim),
                   y_xpow(KickTermsDim), yp_xpow(KickTermsDim),
                   del_xpow(KickTermsDim);
   Vector(Real) xcflow(KickTermsDim),xcfhi(KickTermsDim);
   Integer xpkicks;
   Vector(Integer) x_xppow(KickTermsDim), xp_xppow(KickTermsDim),
                   y_xppow(KickTermsDim), yp_xppow(KickTermsDim),
                   del_xppow(KickTermsDim);
   Vector(Real) xpcflow(KickTermsDim),xpcfhi(KickTermsDim);
   Integer ykicks;
   Vector(Integer) x_ypow(KickTermsDim), xp_ypow(KickTermsDim),
                   y_ypow(KickTermsDim), yp_ypow(KickTermsDim),
                   del_ypow(KickTermsDim);
   Vector(Real) ycflow(KickTermsDim),ycfhi(KickTermsDim);
   Integer ypkicks;
   Vector(Integer) x_yppow(KickTermsDim), xp_yppow(KickTermsDim),
                   y_yppow(KickTermsDim), yp_yppow(KickTermsDim),
                   del_yppow(KickTermsDim);
   Vector(Real) ypcflow(KickTermsDim),ypcfhi(KickTermsDim);

   IFstream fio(fileName, ios::in);
   if (fio.good() != 1) except(badKickFile);

   fio >> s_elt_num >> elt_num;
   if (elt_num != nTransMaps) except(badKickFile);
   
// Add kicks :

   for (n = 0; n < nTransMaps; n++)
   {
      fio >> j;
      fio >> xkicks >> xpkicks;
      fio >> ykicks >> ypkicks;
      for (k = 1; k <= xkicks; k++)
      {
         fio >> x_xpow(k) >> xp_xpow(k);
         fio >> y_xpow(k) >> yp_xpow(k);
         fio >> del_xpow(k) >> xcflow(k);
         fio >> xcfhi(k);
      }
      for (k = 1; k <= xpkicks; k++)
      {
         fio >> x_xppow(k) >> xp_xppow(k);
         fio >> y_xppow(k) >> yp_xppow(k);
         fio >> del_xppow(k) >> xpcflow(k);
         fio >> xpcfhi(k);
      }
      for (k = 1; k <= ykicks; k++)
      {
         fio >> x_ypow(k) >> xp_ypow(k);
         fio >> y_ypow(k) >> yp_ypow(k);
         fio >> del_ypow(k) >> ycflow(k);
         fio >> ycfhi(k);
      }
      for (k = 1; k <= ypkicks; k++)
      {
         fio >> x_yppow(k) >> xp_yppow(k);
         fio >> y_yppow(k) >> yp_yppow(k);
         fio >> del_yppow(k) >> ypcflow(k);
         fio >> ypcfhi(k);
      }

      tm = MapBase::safeCast(tMaps(n));
      sprintf(num, "(%d)", n);
      wname = cons + num;        
      addTKickPair(wname, (tm->_oindex),
                   xkicks,
                   x_xpow,
                   xp_xpow,
                   y_xpow,
                   yp_xpow,
                   del_xpow,
                   xcflow,
                   xcfhi,
                   xpkicks,
                   x_xppow,
                   xp_xppow,
                   y_xppow,
                   yp_xppow,
                   del_xppow,
                   xpcflow,
                   xpcfhi,
                   ykicks,
                   x_ypow,
                   xp_ypow,
                   y_ypow,
                   yp_ypow,
                   del_ypow,
                   ycflow,
                   ycfhi,
                   ypkicks,
                   x_yppow,
                   xp_yppow,
                   y_yppow,
                   yp_yppow,
                   del_yppow,
                   ypcflow,
                   ypcfhi
                  );
   }
   fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  LatKicks::showTKickNode
//
// DESCRIPTION
//  Show the parameters for a symplectic transverse LatKickNode
//
// PARAMETERS
// 
//  n = the LatKickNode number.
//  os = the stream to dump the info to.   
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LatKicks::showTKickNode(const Integer &n, Ostream &os)
{
    Integer k;
    
    if (n > nLatKickNodes) except (badNodeNum);

    TKickNodes *tlk = TKickNodes::safeCast(LatKicksPointers(n-1));

      os << tlk->_xkicks << tlk->_xpkicks;
      os << tlk->_ykicks << tlk->_ypkicks;
      os << "\n\n";
      for (k = 1; k <= tlk->_xkicks; k++)
      {
         os << tlk->_x_xpow(k)   << " ";
         os << tlk->_xp_xpow(k)  << " ";
         os << tlk->_y_xpow(k)   << " ";
         os << tlk->_yp_xpow(k)  << " ";
         os << tlk->_del_xpow(k) << " ";
	 os << tlk->_xcoeff(k)   << "\n";
      }
      os << "\n";
      for (k = 1; k <= tlk->_xpkicks; k++)
      {
	 os << tlk->_x_xppow(k)   << " ";
         os << tlk->_xp_xppow(k)  << " ";
         os << tlk->_y_xppow(k)   << " ";
         os << tlk->_yp_xppow(k)  << " ";
         os << tlk->_del_xppow(k) << " ";
	 os << tlk->_xpcoeff(k)   << "\n";
      }
      os << "\n";
      for (k = 1; k <= tlk->_ykicks; k++)
      {
	 os << tlk->_x_ypow(k)   << " ";
         os << tlk->_xp_ypow(k)  << " ";
         os << tlk->_y_ypow(k)   << " ";
         os << tlk->_yp_ypow(k)  << " ";
         os << tlk->_del_ypow(k) << " ";
	 os << tlk->_ycoeff(k)   << "\n";
      }
      os << "\n";
      for (k = 1; k <= tlk->_ypkicks; k++)
      {
	 os << tlk->_x_yppow(k)   << " ";
         os << tlk->_xp_yppow(k)  << " ";
         os << tlk->_y_yppow(k)   << " ";
         os << tlk->_yp_yppow(k)  << " ";
         os << tlk->_del_yppow(k) << " ";
	 os << tlk->_ypcoeff(k)   << "\n";
      }
      os << "\n\n";
      return;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  LatKicks::dumpTKickNodes
//
// DESCRIPTION
//  Dumps the parameters for all the symplectic transverse LatKickNodes
//
// PARAMETERS
// 
//  os = the stream to dump the info to.   
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LatKicks::dumpTKickNodes(Ostream &os)
{
   Integer n;

   for (n = 1; n <= nLatKickNodes; n++) showTKickNode(n, os); 
   return;
}

