////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Diagnostic.cc
//
// AUTHOR
//    John Galambos, Jeff Holmes
//    ORNL, jdg@ornl.gov
//
// CREATED
//    12/21/98
//
//  DESCRIPTION
//    Source file for the Diagnostic module. This file
//    contains source for the Diagnostic related info including
//    member functions for Diagnostic classes, the Diagnostic nodes in
//    the Ring, and the hooks for the Shell.
//
//
//  REVISION HISTORY
//    Packages Emit*co added by Sarah Cousineau and Jeff Holmes, 08/00.
//    Packages MomentSlice* added by S. Cousineau and J. Holmes., 03/02
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "DiagClass.h"
#include "Diagnostic.h"
#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "StreamType.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "Error.h"
#include "Injection.h"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////



Array(ObjectPtr) DiagnosticPointers;
extern Array(ObjectPtr) syncP, mParts, nodes, tMaps;

///////////////////////////////////////////////////////////////////////////
//
// Local routines:
//
///////////////////////////////////////////////////////////////////////////

// Prototypes for parallel routines:

Integer syncEmit1(Real &xAvg, Real &pxAvg,  Real &yAvg, Real &pyAvg,
                  Integer &nMacros);
Integer syncEmit2(Real &xSig, Real &pxSig,Real &xpxCor , Real &ySig,
                  Real &pySig, Real &ypyCor, Real &xEmitMax, Real &yEmitMax);
Integer syncEmitBins(Vector(Real) &xEmitFrac, Vector(Real) &yEmitFrac,
                     Vector(Real) &xoyEmitFrac, Vector(Real) &xpyEmitFrac,
		     Integer &np);
Void bins(Vector(Real) &phiBin, Vector(Real) &nBin,
          Vector(Real) &xBin, Vector(Real) &yBin,
          MacroPart &mp);
///////////////////////////////////////////////////////////////////////////
//
// External routines:
//
///////////////////////////////////////////////////////////////////////////

extern Void subtitle(Ostream &os, const String &st);
extern Void section(Ostream &os, const String &s);
extern Void subsection(Ostream &os, const String &ss);
extern Void line(Ostream &os, const String &l, Real v);
extern Void line(Ostream &os, const String &l, Real v1, Real v2);
extern Void line(Ostream &os, const String &l, const String &v);
extern Void line(Ostream &os, const String &l, const String &v1,
  const String &v2);


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS DiagnosticBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(DiagnosticBase, Object);

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS CanonicalCoord
//
///////////////////////////////////////////////////////////////////////////

// Static declarations:

Vector(Real) CanonicalCoord::_pxCanonical;
Vector(Real) CanonicalCoord::_pyCanonical;
Vector(Real) CanonicalCoord::_xCanonical;
Vector(Real) CanonicalCoord::_yCanonical;

Define_Standard_Members(CanonicalCoord, DiagnosticBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    CanonicalCoord::_diagCalculator
//
// DESCRIPTION
//  Calculate the canonical momenta for a prescribed herd.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void CanonicalCoord::_diagCalculator()
{
   _pxCanonical.resize(_mp._nMacros);
   _pyCanonical.resize(_mp._nMacros);
   _xCanonical.resize(_mp._nMacros);
   _yCanonical.resize(_mp._nMacros);

    Integer i;
    Real xpfac, ypfac;

    for(i=1; i<= _mp._nMacros; i++)
      {
         _xCanonical(i) = _mp._x(i) - 1000. * Ring::etaX * _mp._dp_p(i);
         _yCanonical(i) = _mp._y(i);
         xpfac =  _mp._xp(i) - 1000. * Ring::etaPX * _mp._dp_p(i);
         ypfac =  _mp._yp(i);
         _pxCanonical(i) = xpfac + _xCanonical(i) * (Ring::alphaX/Ring::betaX);
         _pyCanonical(i) = ypfac + _yCanonical(i) * (Ring::alphaY/Ring::betaY);

      }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    CanonicalCoord::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps the canonical momenta for a prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void CanonicalCoord::_dumpDiagnostic(Ostream &sout)
{
    for(Integer i=1; i<= _mp._nMacros; i++)
      sout << _xCanonical(i) << "   "
           << _yCanonical(i) << "   "
           << _pxCanonical(i) << "   "
           << _pyCanonical(i) << "\n" ;
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS SigmaMatrix
//
///////////////////////////////////////////////////////////////////////////

// Static declarations:

Vector(Real) SigmaMatrix::_XAvg(6);
Matrix(Real) SigmaMatrix::_SigMat(6,6);
Matrix(Real) SigmaMatrix::_CoupMat(2,2);
Real         SigmaMatrix::_Couple;

Define_Standard_Members(SigmaMatrix, DiagnosticBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//  SigmaMatrix::_diagCalculator
//
// DESCRIPTION
//  Calculates the coordinate averages and sigma matrix
//  of a prescribed herd
//
// PARAMETERS
//  None
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SigmaMatrix::_diagCalculator()
{
  Integer i, j, k;
  Vector(Real) X(6);

  Real zophi = Ring::lRing / (Ring::harmonicNumber * Consts::twoPi);

  for (j = 1; j <= 6; j++)
  {
    _XAvg(j) = 0.0;
    for (k = 1; k <= 6; k++)
    {
      _SigMat(j,k) = 0.0;
    }
  }

  for(i = 1; i <= _mp._nMacros; i++)
  {
    X(1) = _mp._x(i)  / 1000.0;
    X(2) = _mp._xp(i) / 1000.0;
    X(3) = _mp._y(i)  / 1000.0;
    X(4) = _mp._yp(i) / 1000.0;
    X(5) = _mp._phi(i) * zophi;
    X(6) = 1.0 + _mp._dp_p(i);
    for (j = 1; j <= 6; j++)
    {
      _XAvg(j) += X(j);
    }
  }

  for (j = 1; j <= 6; j++)
  {
    _XAvg(j) /= _mp._nMacros;
  }

  for(i = 1; i <= _mp._nMacros; i++)
  {
    X(1) = _mp._x(i)  / 1000.0 - _XAvg(1);
    X(2) = _mp._xp(i) / 1000.0 - _XAvg(2);
    X(3) = _mp._y(i)  / 1000.0 - _XAvg(3);
    X(4) = _mp._yp(i) / 1000.0 - _XAvg(4);
    X(5) = _mp._phi(i) * zophi - _XAvg(5);
    X(6) = 1.0 + _mp._dp_p(i) - _XAvg(6);
    for (j = 1; j <= 6; j++)
    {
      for (k = 1; k <= 6; k++)
      {
        _SigMat(j,k) += X(j) * X(k);
      }
    }
  }

  for (j = 1; j <= 6; j++)
  {
    for (k = 1; k <= 6; k++)
    {
      _SigMat(j,k) /= _mp._nMacros;
    }
  }
  _CoupMat(1,1) = _SigMat(1,3) / Sqrt(_SigMat(1,1) * _SigMat(3,3));
  _CoupMat(1,2) = _SigMat(1,4) / Sqrt(_SigMat(1,1) * _SigMat(4,4));
  _CoupMat(2,1) = _SigMat(2,3) / Sqrt(_SigMat(2,2) * _SigMat(3,3));
  _CoupMat(2,2) = _SigMat(2,4) / Sqrt(_SigMat(2,2) * _SigMat(4,4));
  _Couple = Sqrt((_CoupMat(1,1) * _CoupMat(1,1) +
                 _CoupMat(1,2) * _CoupMat(1,2) +
                 _CoupMat(2,1) * _CoupMat(2,1) +
                 _CoupMat(2,2) * _CoupMat(2,2)) / 4.0);
                
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//  SigmaMatrix::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps the coordiante averages and sigma matrix
//  for a prescribed herd to a stream
//
// PARAMETERS
//  sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void SigmaMatrix::_dumpDiagnostic(Ostream &sout)
{
  Integer j, k;
  for(j = 1; j <= 6; j++)
  {
    sout << _XAvg(j) << "\t";
  }
  sout << "\n\n";
  
  for(j = 1; j <= 6; j++)
  {
    for(k = 1; k <= 6; k++)
    {
      sout << _SigMat(j,k) << "\t";
    } 
    sout << "\n";
  }
  sout << "\n";
  for(j = 1; j <= 2; j++)
  {
    for(k = 1; k <= 2; k++)
    {
      sout << _CoupMat(j,k) << "\t";
    } 
    sout << "\n";
  }
  sout << "\n";
  sout << _Couple;
  sout << "\n\n\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS Emittance
//
///////////////////////////////////////////////////////////////////////////

// Define_Standard_Members(Emittance, CanonicalCoord);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Emittance::_diagCalculator
//
// DESCRIPTION
//  Calculate the emittance for a prescribed herd.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Emittance::_diagCalculator()
{
  //
  // Emittances relative to beam centroid.
  //
    Integer i, nMacros;

    _xAvg = _yAvg = _pxAvg = _pyAvg = _xSig = _ySig = _pxSig = _pySig
    = _xpxCor = _ypyCor = 0.;

    Real x2, y2, px2, py2;
    Diagnostic::xEmitMax = Diagnostic::yEmitMax = 0.;

    CanonicalCoord::_diagCalculator();  // find the canonical coordinates
    Diagnostic::xEmit.resize(_mp._nMacros);
    Diagnostic::yEmit.resize(_mp._nMacros);

    for(i=1; i<= _mp._nMacros; i++)  // find 1st moments
    {
        _xAvg += _xCanonical(i);
        _pxAvg += _pxCanonical(i);
        _yAvg += _yCanonical(i);
        _pyAvg += _pyCanonical(i);
    }

    nMacros = _mp._nMacros;
    syncEmit1(_xAvg, _pxAvg, _yAvg,_pyAvg, nMacros);
    _mp._globalNMacros = nMacros;

    _xAvg /= Real(_mp._globalNMacros);
    _pxAvg /= Real(_mp._globalNMacros);
    _yAvg /= Real(_mp._globalNMacros);
    _pyAvg /= Real(_mp._globalNMacros);


     for(i=1; i<= _mp._nMacros; i++)   // find 2nd moments:
     {
        x2 = Sqr(_xCanonical(i) - _xAvg);
        px2 = Sqr(_pxCanonical(i) - _pxAvg);
        _xSig += x2;
        _pxSig += px2;
        _xpxCor += (_xCanonical(i) - _xAvg) *
                   (_pxCanonical(i) - _pxAvg);
	Diagnostic::xEmit(i) = x2/Ring::betaX + px2 * Ring::betaX;
	Diagnostic::xEmitMax = Max(Diagnostic::xEmitMax,Diagnostic::xEmit(i));

        y2 = Sqr( _yCanonical(i) - _yAvg);
        _ySig += y2;
        py2 = Sqr(_pyCanonical(i) - _pyAvg);
        _pySig += py2;
        _ypyCor += (_yCanonical(i) - _yAvg) *
                   (_pyCanonical(i) - _pyAvg);

	Diagnostic::yEmit(i) = y2/Ring::betaY + py2*Ring::betaY;
	Diagnostic::yEmitMax = Max(Diagnostic::yEmitMax,Diagnostic::yEmit(i));
    }

    syncEmit2(_xSig, _pxSig, _xpxCor,_ySig, _pySig, _ypyCor,
                  Diagnostic::xEmitMax, Diagnostic::yEmitMax);

    _xSig   = Sqrt(_xSig/_mp._globalNMacros);
    _pxSig  = Sqrt(_pxSig/_mp._globalNMacros);
    _xpxCor = _xpxCor/_mp._globalNMacros;
    _ySig   = Sqrt(_ySig/_mp._globalNMacros);
    _pySig  = Sqrt(_pySig/_mp._globalNMacros);
    _ypyCor = _ypyCor/_mp._globalNMacros;

 //   Factor of 1 follows the "Wangler/LANL" convention,
 //    NOT the LaPostolle convention:

    Diagnostic::xEmitRMS = Sqrt( Sqr(_xSig *_pxSig) - Sqr(_xpxCor) );
    Diagnostic::yEmitRMS = Sqrt( Sqr(_ySig *_pySig) - Sqr(_ypyCor) );

}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Emittanceco::_diagCalculator
//
// DESCRIPTION
//  Calculate the emittance for a prescribed herd, relative to the closed
//  orbit.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Emittanceco::_diagCalculator()
{

    Integer i, nMacros, nMacrosGlobal;

    Real x2, y2, px2, py2;
    Diagnostic::xEmitMaxco = Diagnostic::yEmitMaxco = 0.;
    Diagnostic::xEmitAveco = Diagnostic::yEmitAveco = 0.;

    CanonicalCoord::_diagCalculator();  // find the canonical coordinates
    Diagnostic::xEmitco.resize(_mp._nMacros);
    Diagnostic::yEmitco.resize(_mp._nMacros);

    nMacros = _mp._nMacros;
    nMacrosGlobal = nMacros;

    //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }

    if( nMPIsize_ > 1){
       MPI_Allreduce(&nMacros, &nMacrosGlobal, 1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    }
    //===Parallel MPI ======stop=======

     _mp._globalNMacros = nMacrosGlobal;

     for(i=1; i<= _mp._nMacros; i++)   // find 2nd moments, rel. to c.o.:
      {
        x2 = Sqr(_xCanonical(i));
        px2 = Sqr(_pxCanonical(i));

	Diagnostic::xEmitco(i) = x2/Ring::betaX + px2 * Ring::betaX;
	Diagnostic::xEmitMaxco = Max(Diagnostic::xEmitMaxco,
				     Diagnostic::xEmitco(i));
	Diagnostic::xEmitAveco += Diagnostic::xEmitco(i);

        y2 = Sqr( _yCanonical(i));
        py2 = Sqr(_pyCanonical(i));

	Diagnostic::yEmitco(i) = y2/Ring::betaY + py2*Ring::betaY;
	Diagnostic::yEmitMaxco = Max(Diagnostic::yEmitMaxco,
				     Diagnostic::yEmitco(i));

	Diagnostic::yEmitAveco += Diagnostic::yEmitco(i);
      }

    //===Parallel MPI ======start======
     if( nMPIsize_ > 1){
       double xxx1,xxx2;
       xxx1 = Diagnostic::xEmitAveco;
       MPI_Allreduce(&xxx1, &xxx2, 1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
       Diagnostic::xEmitAveco = xxx2;
       xxx1 = Diagnostic::yEmitAveco;
       MPI_Allreduce(&xxx1, &xxx2, 1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
       Diagnostic::yEmitAveco = xxx2;

       xxx1 = Diagnostic::xEmitMaxco;
       MPI_Allreduce(&xxx1, &xxx2, 1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
       Diagnostic::xEmitMaxco = xxx2;
       xxx1 = Diagnostic::yEmitMaxco;
       MPI_Allreduce(&xxx1, &xxx2, 1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
       Diagnostic::yEmitMaxco = xxx2;
     }
    //===Parallel MPI ======stop=======

     Diagnostic::xEmitAveco /= Real(_mp._globalNMacros);
     Diagnostic::yEmitAveco /= Real(_mp._globalNMacros);


}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS EmittanceSlice
//
///////////////////////////////////////////////////////////////////////////

//Define_Standard_Members(EmittanceSlice, CanonicalCoord);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//  EmittanceSlice::_diagCalculator
//
// DESCRIPTION
//  Calculate the rms emittance of a particular longitudinal slice
//  for a prescribed herd.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void EmittanceSlice::_diagCalculator()
{

  Integer i, index=0, nMacros=0, nMacrosGlobal=0;

  _xSliceAvg = _ySliceAvg = _pxSliceAvg = _pySliceAvg = _xSliceSig =
    _ySliceSig = _pxSliceSig = _pySliceSig = _xpxSliceCor
    = _ypySliceCor = 0.;

  Real x2, y2, px2, py2, phi;
  Diagnostic::xEmitSliceMax = Diagnostic::yEmitSliceMax = 0.;


  CanonicalCoord::_diagCalculator();  // find the canonical coordinates


    //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======


  for(i=1; i<= _mp._nMacros; i++)  // find 1st moments
    {
      phi=_mp._phi(i);
      if((phi > _phiMin) && (phi < _phiMax)){
        _xSliceAvg  += _xCanonical(i);
        _pxSliceAvg += _pxCanonical(i);
        _ySliceAvg  += _yCanonical(i);
        _pySliceAvg += _pyCanonical(i);
	nMacros++;
      }
    }

    nMacrosGlobal = nMacros;

    //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
       MPI_Allreduce(&nMacros, &nMacrosGlobal, 1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);


      double buff_0[4];
      double buff_1[4];

      buff_0[0]= _xSliceAvg;
      buff_0[1]= _pxSliceAvg;
      buff_0[2]= _ySliceAvg;
      buff_0[3]= _pySliceAvg;
      MPI_Allreduce(buff_0, buff_1, 4,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      _xSliceAvg   = buff_1[0];
      _pxSliceAvg  = buff_1[1];
      _ySliceAvg   = buff_1[2];
      _pySliceAvg  = buff_1[3];
    }
    //===Parallel MPI ======stop=======

  Diagnostic::xEmitSlice.resize(nMacros);
  Diagnostic::yEmitSlice.resize(nMacros);
  _xSliceAvg  /= Real(nMacrosGlobal);
  _pxSliceAvg /= Real(nMacrosGlobal);
  _ySliceAvg  /= Real(nMacrosGlobal);
  _pySliceAvg /= Real(nMacrosGlobal);


  for(i=1; i<= _mp._nMacros; i++)   // find 2nd moments:
    {
      phi=_mp._phi(i);
      if((phi > _phiMin) && (phi < _phiMax)){

	index++;

	x2  = Sqr(_xCanonical(i) - _xSliceAvg);
	px2 = Sqr(_pxCanonical(i) - _pxSliceAvg);
	_xSliceSig   += x2;
	_pxSliceSig  += px2;
	_xpxSliceCor += (_xCanonical(i) - _xSliceAvg) *
	                (_pxCanonical(i) - _pxSliceAvg);

	Diagnostic::xEmitSlice(index) = x2/Ring::betaX + px2 * Ring::betaX;
	Diagnostic::xEmitSliceMax = Max(Diagnostic::xEmitSliceMax,
					Diagnostic::xEmitSlice(index));

	y2  = Sqr( _yCanonical(i) - _ySliceAvg);
	py2 = Sqr(_pyCanonical(i) - _pySliceAvg);
	_ySliceSig   += y2;
	_pySliceSig  += py2;
	_ypySliceCor += (_yCanonical(i) - _ySliceAvg) *
	  (_pyCanonical(i) - _pySliceAvg);

	Diagnostic::yEmitSlice(index) = y2/Ring::betaY + py2*Ring::betaY;
	Diagnostic::yEmitSliceMax = Max(Diagnostic::yEmitSliceMax,
					Diagnostic::yEmitSlice(index));

      }
    }

    //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
      double buff_0[6];
      double buff_1[6];

      buff_0[0]= _xSliceSig;
      buff_0[1]= _pxSliceSig ;
      buff_0[2]= _xpxSliceCor ;
      buff_0[3]= _ySliceSig;
      buff_0[4]= _pySliceSig;
      buff_0[5]= _ypySliceCor;
      MPI_Allreduce(buff_0, buff_1, 6,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      _xSliceSig   = buff_1[0];
      _pxSliceSig  = buff_1[1];
      _xpxSliceCor = buff_1[2];
      _ySliceSig   = buff_1[3];
      _pySliceSig  = buff_1[4];
      _ypySliceCor = buff_1[5];


      buff_0[0]= Diagnostic::xEmitSliceMax;
      buff_0[1]= Diagnostic::yEmitSliceMax;
      MPI_Allreduce(buff_0, buff_1, 2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      Diagnostic::xEmitSliceMax  = buff_1[0];
      Diagnostic::yEmitSliceMax  = buff_1[1];

    }
    //===Parallel MPI ======stop=======

  _xSliceSig = Sqrt(_xSliceSig/nMacrosGlobal);
  _pxSliceSig = Sqrt(_pxSliceSig/nMacrosGlobal);
  _xpxSliceCor = _xpxSliceCor/nMacrosGlobal;
  _ySliceSig = Sqrt(_ySliceSig/nMacrosGlobal);
  _pySliceSig = Sqrt(_pySliceSig/nMacrosGlobal);
  _ypySliceCor = _ypySliceCor/nMacrosGlobal;

  //   Factor of 1 follows the "Wangler/LANL" convention,
  //   NOT the LaPostolle convention:

  Diagnostic::xEmitSliceRMS = Sqrt( Sqr(_xSliceSig *_pxSliceSig)
				    - Sqr(_xpxSliceCor) );
  Diagnostic::yEmitSliceRMS = Sqrt( Sqr(_ySliceSig *_pySliceSig)
				    - Sqr(_ypySliceCor) );


}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//  syncEmit1, syncEmit2
//
// DESCRIPTION
//  syncs info for parallel emittance calculation.
//
// PARAMETERS
//   ......
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Integer syncEmit1(Real &xAvg, Real &pxAvg, Real &yAvg, Real &pyAvg,
                  Integer &nMacros)
{

 int iMPIini_=0;
 MPI_Initialized(&iMPIini_);
 int nMPIsize_ = 1;
 int nMPIrank_ = 0;
 if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
 }
 if( nMPIsize_ == 1) return -1;

 int nMacrosGlobal;
 int nMacrosLocal = nMacros;
 MPI_Allreduce( &nMacrosLocal, &nMacrosGlobal, 1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
 nMacros = nMacrosGlobal;

 double buff_loc [4];
 double buff_glob[4];
 buff_loc[0]= xAvg;
 buff_loc[1]= pxAvg;
 buff_loc[2]= yAvg;
 buff_loc[3]= pyAvg;
 MPI_Allreduce(buff_loc,buff_glob, 4,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
 xAvg  = buff_glob[0];
 pxAvg = buff_glob[1];
 yAvg  = buff_glob[2];
 pyAvg = buff_glob[3];

 return 1;

}

Integer syncEmit2(Real &xSig, Real &pxSig,Real &xpxCor , Real &ySig,
                  Real &pySig, Real &ypyCor, Real &xEmitMax, Real &yEmitMax)
{


 int iMPIini_=0;
 MPI_Initialized(&iMPIini_);
 int nMPIsize_ = 1;
 int nMPIrank_ = 0;
 if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
 }

 if( nMPIsize_ == 1) return -1;

 double buff_loc [6];
 double buff_glob[6];
 buff_loc[0]= xSig;
 buff_loc[1]= pxSig;
 buff_loc[2]= xpxCor;
 buff_loc[3]= ySig;
 buff_loc[4]= pySig;
 buff_loc[5]= ypyCor;
 MPI_Allreduce(buff_loc,buff_glob, 6,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
 xSig   = buff_glob[0];
 pxSig  = buff_glob[1];
 xpxCor = buff_glob[2];
 ySig   = buff_glob[3];
 pySig  = buff_glob[4];
 ypyCor = buff_glob[5];

 buff_loc[0] = xEmitMax;
 buff_loc[1] = yEmitMax;
 MPI_Allreduce(buff_loc,buff_glob, 2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
 xEmitMax = buff_glob[0];
 yEmitMax = buff_glob[1];

 return 1;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Emittance::_showDiagnostic
//
// DESCRIPTION
//  Sends emittance info for a prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Emittance::_showDiagnostic(Ostream &os)
{

//jdg - need to name the herd
    section(os, "Emittance Output");

    line(os,"X RMS Emittance (pi-mm-mrad)", Diagnostic::xEmitRMS);
    line(os,"X max Emittance (pi-mm-mrad)", Diagnostic::xEmitMax);
    line(os,"Y RMS Emittance (pi-mm-mrad)", Diagnostic::yEmitRMS);
    line(os,"Y max Emittance (pi-mm-mrad)", Diagnostic::yEmitMax);

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Emittance::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps emittance info for a prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Emittance::_dumpDiagnostic(Ostream &sout)
{

//jdg - need to name the herd

  sout << Diagnostic::xEmitRMS << "\t" << Diagnostic::yEmitRMS << "\t" ;
  sout << _xAvg << "\t" << _yAvg << "\t" ;
  sout << _pxAvg << "\t" << _pyAvg << "\n";

}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//   EmittanceSlice::_showDiagnostic
//
// DESCRIPTION
//  Sends emittance info for particular longitudinal slice of a
//  prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void EmittanceSlice::_showDiagnostic(Ostream &os)
{

    section(os, "Emittance Slice Output");

    line(os,"X RMS Emittance (pi-mm-mrad)", Diagnostic::xEmitSliceRMS);
    line(os,"X max Emittance (pi-mm-mrad)", Diagnostic::xEmitSliceMax);
    line(os,"Y RMS Emittance (pi-mm-mrad)", Diagnostic::yEmitSliceRMS);
    line(os,"Y max Emittance (pi-mm-mrad)", Diagnostic::yEmitSliceMax);

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    EmittanceSlice::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps emittance info for a particular longitudinal slice of a
//  prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void EmittanceSlice::_dumpDiagnostic(Ostream &sout)
{


  sout << Diagnostic::xEmitSliceRMS << "\t"
       << Diagnostic::yEmitSliceRMS << "\t" ;
  sout << _xSliceAvg << "\t" << _ySliceAvg << "\t" ;
  sout << _pxSliceAvg << "\t" << _pySliceAvg << "\n";

}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LongEmittance
//
///////////////////////////////////////////////////////////////////////////

// Define_Standard_Members(LongEmittance, DiagnosticBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    LongEmittance::_diagCalculator
//
// DESCRIPTION
//  Calculate the longitudinal emittance for a prescribed herd.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void LongEmittance::_diagCalculator()
{

    Integer i, nMacros, nMacrosGlobal;

    _phiAvg = _dEAvg = _phiSig = _dESig = _phiDE = 0.;

    Real phi2,dE2, phiDE;
    phi2 = dE2 = phiDE = 0.;

    Diagnostic::longEmit.resize(_mp._nMacros);

    nMacros = _mp._nMacros;
    nMacrosGlobal = nMacros;

    //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }

    if( nMPIsize_ > 1){
       MPI_Allreduce(&nMacros, &nMacrosGlobal, 1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    }
    //===Parallel MPI ======stop=======


    for(i=1; i<= _mp._nMacros; i++)  // find 1st moments
      {
        _phiAvg += _mp._phi(i);
        _dEAvg += _mp._deltaE(i);
      }

    //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
     double buff_0[2];
     double buff_1[2];

    buff_0[0]= _phiAvg;
     buff_0[1]= _dEAvg;
     MPI_Allreduce(buff_0, buff_1, 2,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
     _phiAvg = buff_1[0];
     _dEAvg  = buff_1[1];
    }
    //===Parallel MPI ======stop=======

     _phiAvg /= Real(nMacrosGlobal);
     _dEAvg /= Real(nMacrosGlobal);

     Diagnostic::longEmitMax = 0.;

    Real factor = 1.e9 * Ring::lRing /
    (2. * Consts::vLight * _mp._syncPart._betaSync * Ring::harmonicNumber);

     for(i=1; i<= _mp._nMacros; i++)   // find 2nd moments:
      {
        phi2 = Sqr(_mp._phi(i) - _phiAvg);
        dE2 = Sqr(_mp._deltaE(i) - _dEAvg);
        phiDE = (_mp._phi(i) - _phiAvg) * (_mp._deltaE(i) - _dEAvg);
        _phiSig += phi2;
        _dESig  += dE2;
        _phiDE  += phiDE;
	Diagnostic::longEmit(i) = factor * Sqrt(dE2 * phi2);
	Diagnostic::longEmitMax = Max(Diagnostic::longEmitMax,
                               Diagnostic::longEmit(i));
      }

    //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
     double buff_0[3];
     double buff_1[3];

     buff_0[0]= _phiSig;
     buff_0[1]= _dESig;
     buff_0[2]= _phiDE;
     MPI_Allreduce(buff_0, buff_1, 3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
     _phiSig = buff_1[0];
     _dESig  = buff_1[1];
     _phiDE  = buff_1[2];

     buff_0[0]=Diagnostic::longEmitMax;
     MPI_Allreduce(buff_0, buff_1, 1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
     Diagnostic::longEmitMax = buff_1[0];

    }
    //===Parallel MPI ======stop=======

       _phiSig = Sqrt(_phiSig/nMacrosGlobal);
       _dESig = Sqrt(_dESig/nMacrosGlobal);
       _phiDE = _phiDE/nMacrosGlobal;

 //   Factor of 1 follows the "Wangler/LANL" convenmtion,
 //    NOT the LaPostolle convention:

    Diagnostic::longEmitRMS = factor * Sqrt( Sqr(_phiSig *_dESig) - Sqr(_phiDE) );

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LongEmittance::_showDiagnostic
//
// DESCRIPTION
//  Sends emittance info for a prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void LongEmittance::_showDiagnostic(Ostream &os)
{

//jdg - need to name the herd
    section(os, "Long Emittance Output");

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    LongEmittance::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps emittance info for a prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void LongEmittance::_dumpDiagnostic(Ostream &sout)
{

//jdg - need to name the herd

}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS Actions
//
///////////////////////////////////////////////////////////////////////////

 Define_Standard_Members(Actions, Emittance);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Actions::_diagCalculator
//
// DESCRIPTION
//  Calculate the actions for a prescribed herd.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Actions::_diagCalculator()
{
  Diagnostic::xAction.resize(_mp._nMacros);
  Diagnostic::yAction.resize(_mp._nMacros);

  CanonicalCoord::_diagCalculator();  // find the canonical coordinates

  for(Integer i=1; i<= _mp._nMacros; i++)
  {
    Diagnostic::xAction(i) =
      _xCanonical(i)  *  _xCanonical(i) / Ring::betaX +
      _pxCanonical(i) * _pxCanonical(i) * Ring::betaX;
    Diagnostic::yAction(i) =
      _yCanonical(i)  *  _yCanonical(i) / Ring::betaY +
      _pyCanonical(i) * _pyCanonical(i) * Ring::betaY;
      }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Actions::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps actions of each macroparticle  in a prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Actions::_dumpDiagnostic(Ostream &sout)
{
    for(Integer i=1; i<= _mp._nMacros; i++)
      sout << Diagnostic::xAction(i) << "   "
           << Diagnostic::yAction(i) << "\n";

}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS StatLat
//
///////////////////////////////////////////////////////////////////////////

 Define_Standard_Members(StatLat, Emittance);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StatLat::_diagCalculator
//
// DESCRIPTION
//  Calculate the canonical momenta for a prescribed herd.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void StatLat::_diagCalculator()
{
   Emittance::_diagCalculator();

   _betaX = Emittance::_xSig * Emittance::_xSig / Diagnostic::xEmitRMS;
   _betaY = Emittance::_ySig * Emittance::_ySig / Diagnostic::yEmitRMS;
   _alphaX = -Emittance::_xpxCor / Diagnostic::xEmitRMS +
     _betaX * Ring::alphaX / Ring::betaX;
   _alphaY = -Emittance::_ypyCor / Diagnostic::yEmitRMS +
     _betaY * Ring::alphaY / Ring::betaY;

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//   StatLat::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps the statistical lattice parameters of a prescribed herd to a stream
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void StatLat::_dumpDiagnostic(Ostream &sout)
{

   sout  << Diagnostic::xEmitRMS << "\t" << Diagnostic::yEmitRMS << "\t"
         << _betaX               << "\t" << _betaY           << "\t"
         << _alphaX              << "\t" << _alphaY          << "\t"
         << Ring::betaX          << "\t" << Ring::betaY      << "\t"
         << Ring::alphaX         << "\t" << Ring::alphaY     << "\n" ;
}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LongDist
//
///////////////////////////////////////////////////////////////////////////

// Static declarations:

Vector(Real) LongDist::_PhiVal;
Vector(Real) LongDist::_lambda;
Vector(Real) LongDist::_xDipole;
Vector(Real) LongDist::_xpDipole;
Vector(Real) LongDist::_yDipole;
Vector(Real) LongDist::_ypDipole;

Define_Standard_Members(LongDist, DiagnosticBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LongDist::_diagCalculator
//
// DESCRIPTION
//   Calculate the longitudinal distributions for a prescribed herd
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LongDist::_diagCalculator()
{
  int nMacros = 0, nMacrosGlobal = 0;

  _PhiVal.resize(_nBins);
  _lambda.resize(_nBins);
  _xDipole.resize(_nBins);
  _xpDipole.resize(_nBins);
  _yDipole.resize(_nBins);
  _ypDipole.resize(_nBins);

  nMacros =_mp._nMacros;
  nMacrosGlobal = nMacros;

  //===Parallel MPI ======start======
  int iMPIini_= 0;
  MPI_Initialized(&iMPIini_);
  int nMPIsize_ = 1;
  int nMPIrank_ = 0;

  if(iMPIini_ > 0)
  {
    MPI_Comm_size(MPI_COMM_WORLD, &nMPIsize_);
    MPI_Comm_rank(MPI_COMM_WORLD, &nMPIrank_);
  }

  if(nMPIsize_ > 1)
  {
    MPI_Allreduce(&nMacros, &nMacrosGlobal, 1,
                  MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }
  //===Parallel MPI ======stop=======

  Integer i;

  //  find averages:

  Real xAvg, xpAvg, yAvg, ypAvg;
  xAvg  = 0.0;
  xpAvg = 0.0;
  yAvg  = 0.0;
  ypAvg = 0.0;

  for(i = 1; i <= _mp._nMacros; i++)
  {
    xAvg  += _mp._x(i);
    xpAvg += _mp._xp(i);
    yAvg  += _mp._y(i);
    ypAvg += _mp._yp(i);
  }

  //===Parallel MPI ======start======
  if(nMPIsize_ > 1)
  {
    double buff_0[4];
    double buff_1[4];
    buff_0[0] = xAvg;
    buff_0[1] = xpAvg;
    buff_0[2] = yAvg;
    buff_0[3] = ypAvg;

    MPI_Allreduce(buff_0, buff_1, 4,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    xAvg  = buff_1[0];
    xpAvg = buff_1[1];
    yAvg  = buff_1[2];
    ypAvg = buff_1[3];
  }
  //===Parallel MPI ======stop=======

  xAvg  /= Real(nMacrosGlobal);
  xpAvg /= Real(nMacrosGlobal);
  yAvg  /= Real(nMacrosGlobal);
  ypAvg /= Real(nMacrosGlobal);

  Real dPhi = 2.0 * Consts::pi / _nBins;
  Integer k, kp;

  for(k = 1; k <= _nBins; k++)
  {
    _PhiVal(k)  = (k - 1) * dPhi - Consts::pi;
    _lambda(k)  = 0.0;
    _xDipole(k) = 0.0;
    _xpDipole(k) = 0.0;
    _yDipole(k) = 0.0;
    _ypDipole(k) = 0.0;
  }

  Real frac;

  for(i = 1; i <= _mp._nMacros; i++)
  {
    frac = (_mp._phi(i) + Consts::pi) / dPhi;
    k = Integer(frac);
    frac -= Real(k);
    if(k >= _nBins) k = 0;
    if(k < 0) k = _nBins - 1;
    k += 1;
    kp = k + 1;
    if(k == _nBins) kp = 1;
    _lambda(k)    +=  1.0 - frac;
    _xDipole(k)   += (1.0 - frac) * (_mp._x(i) - xAvg);
    _xpDipole(k)  += (1.0 - frac) * (_mp._xp(i) - xpAvg);
    _yDipole(k)   += (1.0 - frac) * (_mp._y(i) - yAvg);
    _ypDipole(k)  += (1.0 - frac) * (_mp._yp(i) - ypAvg);
    _lambda(kp)   += frac;
    _xDipole(kp)  += frac * (_mp._x(i) - xAvg);
    _xpDipole(kp) += frac * (_mp._xp(i) - xpAvg);
    _yDipole(kp)  += frac * (_mp._y(i) - yAvg);
    _ypDipole(kp) += frac * (_mp._yp(i) - ypAvg);
  }

  //===Parallel MPI ======start======
  if(nMPIsize_ > 1)
  {
    int nB1 = _nBins;
    int nB2 = _nBins * 2;
    int nB3 = _nBins * 3;
    int nB4 = _nBins * 4;
    int nB5 = _nBins * 5;

    double* buff_0 = (double *) malloc(sizeof(double) * _nBins * 5);
    double* buff_1 = (double *) malloc(sizeof(double) * _nBins * 5);
 
    for(k = 0; k < _nBins; k++)
    {
      kp = k + 1;
      buff_0[k]       = _lambda(kp);
      buff_0[k + nB1] = _xDipole(kp);
      buff_0[k + nB2] = _xpDipole(kp);
      buff_0[k + nB3] = _yDipole(kp);
      buff_0[k + nB4] = _ypDipole(kp);
    }

    MPI_Allreduce(buff_0, buff_1, nB5,
                  MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
 
    for(k = 0; k < _nBins; k++)
    {
      kp = k + 1;
      _lambda(kp)   = buff_0[k];
      _xDipole(kp)  = buff_0[k + nB1];
      _xpDipole(kp) = buff_0[k + nB2];
      _yDipole(kp)  = buff_0[k + nB3];
      _ypDipole(kp) = buff_0[k + nB4];
    }

    free(buff_0);
    free(buff_1);
  }
  //===Parallel MPI ======stop=======

  Real Lambfac = Injection::nReals_Macro * Real(_nBins) *
                 Ring::harmonicNumber / Ring::lRing;
  Real Momfac  = Lambfac / 1000.0;

  for(k = 1; k <= _nBins; k++)
  {
    _lambda(k)   *= Lambfac;
    _xDipole(k)  *= Momfac;
    _xpDipole(k) *= Momfac;
    _yDipole(k)  *= Momfac;
    _ypDipole(k) *= Momfac;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LongDist::_showDiagnostic
//   LongDist::_dumpDiagnostic
//
// DESCRIPTION
//   Dumps longitudinal distribution to a stream
//
// PARAMETERS
//   sout - the stream to dump to
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LongDist::_showDiagnostic(Ostream &sout)
{
}

Void LongDist::_dumpDiagnostic(Ostream &sout)
{
}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS Moment
//
///////////////////////////////////////////////////////////////////////////

 // Static declarations:

 Matrix(Real) Moment::_momentXY;
 Matrix(Real) Moment::_momentXYNorm;

 Define_Standard_Members(Moment, DiagnosticBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Moment::_diagCalculator
//
// DESCRIPTION
//  Calculate the transverse moments for a prescribed herd.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Moment::_diagCalculator()
{
    int nMacros=0, nMacrosGlobal=0;

   _momentXY.resize(_order+1, _order+1);
   _momentXYNorm.resize(_order+1, _order+1);

   Vector(Real) momX(_order+1), momY(_order+1);
   Vector(Real) betaXPow(_order+1), betaYPow(_order+1);

   nMacros =_mp._nMacros;
   nMacrosGlobal = nMacros;

    //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }

    if( nMPIsize_ > 1){
       MPI_Allreduce(&nMacros, &nMacrosGlobal, 1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    }
    //===Parallel MPI ======stop=======

    Integer i,j,n;
    Real xpfac, ypfac;

    //  find averages:
    Real xAvg, yAvg;
    xAvg = yAvg = 0.;

    for(i=1; i<= _mp._nMacros; i++)
      {
         xAvg += _mp._x(i);
         yAvg += _mp._y(i);
      }

   //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
      double buff_0[2];
      double buff_1[2];
      buff_0[0]= xAvg;
      buff_0[1]= yAvg;
      MPI_Allreduce(buff_0, buff_1, 2,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      xAvg = buff_1[0];
      yAvg = buff_1[1];
    }
    //===Parallel MPI ======stop=======

     xAvg /= Real(nMacrosGlobal);
     yAvg /= Real(nMacrosGlobal);

     _momentXY = 0.;
     _momentXYNorm = 0.;

      momX(1) = 1.;
      momY(1) = 1.;
      for(n = 1; n<= _mp._nMacros; n++)
	{

           for( i = 1; i <= _order; i++)
                momX(i+1) = momX(i)*(_mp._x(n) - xAvg);

           for( i = 1; i<= _order; i++)
                momY(i+1) = momY(i)*(_mp._y(n) - yAvg);

           for(j=1; j<=_order+1; j++)
             for(i=1 ; i<= _order+2-j; i++)
                 _momentXY(i,j) += momX(i) * momY(j);

	}

    //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
     double* buff_0 = (double *) malloc (sizeof(double)*(_order+1)*(_order+1));
     double* buff_1 = (double *) malloc (sizeof(double)*(_order+1)*(_order+1));

     int count = 0;
     for(j=1; j<=_order+1; j++){
       for(i=1 ; i<= _order+2-j; i++){
                 buff_0[count]= _momentXY(i,j);
                 count++;
       }
     }

     MPI_Allreduce(buff_0, buff_1, count ,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

     count = 0;
     for(j=1; j<=_order+1; j++){
       for(i=1 ; i<= _order+2-j; i++){
                 _momentXY(i,j) = buff_1[count];
                 count++;
       }
     }
     free(buff_0);
     free(buff_1);
    }
   //===Parallel MPI ======stop=======

       for(i=1; i<= _order+1; i++)
        for(j=1; j<= _order+2-i ; j++)
             _momentXY(i,j) /= Real(nMacrosGlobal);

      _momentXY(1,1) = 1.;  // 0th moment
      _momentXY(2,1) = xAvg;
      _momentXY(1,2) = yAvg;

      betaXPow(1) = 1.;
      betaYPow(1) = 1.;

      for(i=2; i<= _order+1; i++)
	{
	  betaXPow(i) = betaXPow(i-1) * Sqrt(Ring::betaX);
	  betaYPow(i) = betaYPow(i-1) * Sqrt(Ring::betaY);
	}

      for(i=1; i<= _order+1; i++)  // loop over x
        for(j=1; j<= _order+2-i ; j++)  // loop over y
             _momentXYNorm(i,j) = _momentXY(i,j)/(betaXPow(i)*betaYPow(j));
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//  Moments::_showDiagnostic
//  Moments::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps moments of a prescribed herd to a stream. These matrices are
//   printed in Mathematica format.
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Moment::_showDiagnostic(Ostream &sout)
{
  sout << _order << "\n";
  sout << _momentXY << "\n";
  sout << _momentXYNorm << "\n";
}

Void Moment::_dumpDiagnostic(Ostream &sout)
{
  sout << _order << "\t";
  sout << _momentXY(2,1) << "\t" << _momentXY(1,2) << "\t";
  for(Integer i=1; i<= _order+1; i++)
    for(Integer j=1; j<= i; j++)
      sout << _momentXYNorm(i+1-j,j) << "\t";

}



///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS MomentSlice
//
///////////////////////////////////////////////////////////////////////////

 // Static declarations:

 Matrix(Real) MomentSlice::_momentSliceXY;
 Matrix(Real) MomentSlice::_momentSliceXYNorm;

 Define_Standard_Members(MomentSlice, DiagnosticBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MomentSlice::_diagCalculator
//
// DESCRIPTION
//  Calculate the transverse moments for a prescribed herd for a
//  particular longitudinal slice of beam.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MomentSlice::_diagCalculator()
{
   _momentSliceXY.resize(_order+1, _order+1);
   _momentSliceXYNorm.resize(_order+1, _order+1);

   Vector(Real) momX(_order+1), momY(_order+1);
   Vector(Real) betaXPow(_order+1), betaYPow(_order+1);


    Integer i,j,n;
    Real xpfac, ypfac;
    Real phi;

    int nMacros=0, nMacrosGlobal;

    //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }

    //===Parallel MPI ======stop=======

    //  find averages:
    Real xAvg, yAvg;
    xAvg = yAvg = 0.;

    for(i=1; i<= _mp._nMacros; i++)
      {
	phi=_mp._phi(i);
	if((phi > _phiMin) && (phi < _phiMax)){
         xAvg += _mp._x(i);
         yAvg += _mp._y(i);
	 nMacros++;
	}
      }

    nMacrosGlobal = nMacros;

   //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
      MPI_Allreduce(&nMacros, &nMacrosGlobal, 1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

      double buff_0[2];
      double buff_1[2];
      buff_0[0]= xAvg;
      buff_0[1]= yAvg;
      MPI_Allreduce(buff_0, buff_1, 2,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      xAvg = buff_1[0];
      yAvg = buff_1[1];
    }
    //===Parallel MPI ======stop=======

     xAvg /= Real(nMacrosGlobal);
     yAvg /= Real(nMacrosGlobal);

     _momentSliceXY = 0.;
     _momentSliceXYNorm = 0.;

      momX(1) = 1.;
      momY(1) = 1.;
      for(n = 1; n<= _mp._nMacros; n++)
	{
	  phi=_mp._phi(n);

	  if((phi > _phiMin) && (phi < _phiMax)){
	    for( i = 1; i <= _order; i++)
	      momX(i+1) = momX(i)*(_mp._x(n) - xAvg);

	    for( i = 1; i<= _order; i++)
	      momY(i+1) = momY(i)*(_mp._y(n) - yAvg);

	    for(j=1; j<=_order+1; j++)
	      for(i=1 ; i<= _order+2-j; i++)
		_momentSliceXY(i,j) += momX(i) * momY(j);
	  }
	}


    //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
     double* buff_0 = (double *) malloc (sizeof(double)*(_order+1)*(_order+1));
     double* buff_1 = (double *) malloc (sizeof(double)*(_order+1)*(_order+1));

     int count = 0;
     for(j=1; j<=_order+1; j++){
       for(i=1 ; i<= _order+2-j; i++){
                 buff_0[count]= _momentSliceXY(i,j);
                 count++;
       }
     }

     MPI_Allreduce(buff_0, buff_1, count ,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

     count = 0;
     for(j=1; j<=_order+1; j++){
       for(i=1 ; i<= _order+2-j; i++){
                 _momentSliceXY(i,j) = buff_1[count];
                 count++;
       }
     }
     free(buff_0);
     free(buff_1);
    }
   //===Parallel MPI ======stop=======


       for(i=1; i<= _order+1; i++)
        for(j=1; j<= _order+2-i ; j++)
             _momentSliceXY(i,j) /= Real(nMacrosGlobal);

      _momentSliceXY(1,1) = 1.;  // 0th moment
      _momentSliceXY(2,1) = xAvg;
      _momentSliceXY(1,2) = yAvg;

      betaXPow(1) = 1.;
      betaYPow(1) = 1.;

      for(i=2; i<= _order+1; i++)
	{
	  betaXPow(i) = betaXPow(i-1) * Sqrt(Ring::betaX);
	  betaYPow(i) = betaYPow(i-1) * Sqrt(Ring::betaY);
	}

      for(i=1; i<= _order+1; i++)  // loop over x
        for(j=1; j<= _order+2-i ; j++)  // loop over y
             _momentSliceXYNorm(i,j) = _momentSliceXY(i,j)/
	                                 (betaXPow(i)*betaYPow(j));

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//  MomentSlice::_showDiagnostic
//  MomentSlice::_dumpDiagnostic
//
// DESCRIPTION
//  Dumps sliced moments of a prescribed herd to a stream. These matrices are
//   printed in Mathematica format.
//
// PARAMETERS
//    sout - the stream to dump to
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MomentSlice::_showDiagnostic(Ostream &sout)
{
  sout << _order << "\n";
  sout << _momentSliceXY << "\n";
  sout << _momentSliceXYNorm << "\n";
}

Void MomentSlice::_dumpDiagnostic(Ostream &sout)
{
  sout << _order << "\t";
  sout << _momentSliceXY(2,1) << "\t" << _momentSliceXY(1,2) << "\t";
  for(Integer i=1; i<= _order+1; i++)
    for(Integer j=1; j<= i; j++)
      sout << _momentSliceXYNorm(i+1-j,j) << "\t";

}



///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS DiagnosticNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(DiagnosticNode, Node);

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FracTuneNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(FracTuneNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FracTuneNode::_diagNodeCalculator
//
// DESCRIPTION
//   Dumps herd moments to a file.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FracTuneNode::_diagNodeCalculator(MacroPart &mp)
{
  Integer i;
  Real angle, xval, xpval, yval, ypval;
  Real twopi = 2.0 * Consts::pi;

  cout << "FracTuneNode: mp._nMacros = " << mp._nMacros << "  "
       << "Diagnostic::oldSize = "       << Diagnostic::oldSize << "\n";

  if(mp._nMacros <= 0) return;

  if(mp._nMacros > Diagnostic::oldSize)
  {
    Diagnostic::XPhase_old.resize(mp._nMacros);
    Diagnostic::YPhase_old.resize(mp._nMacros);
    Diagnostic::XPhase.resize(mp._nMacros);
    Diagnostic::YPhase.resize(mp._nMacros);
    Diagnostic::XFrac_Tune.resize(mp._nMacros);
    Diagnostic::YFrac_Tune.resize(mp._nMacros);

    for(i = Diagnostic::oldSize + 1; i <= mp._nMacros; i++)
    {
      Diagnostic::XPhase_old(i) = 0.0;
      Diagnostic::YPhase_old(i) = 0.0;
      Diagnostic::XFrac_Tune(i) = 0.0;
      Diagnostic::YFrac_Tune(i) = 0.0;
    }
  }

  for(i = 1; i <= mp._nMacros; i++)
  {
    xval  = (mp._x(i)  - 1000. * Ring::etaX  * mp._dp_p(i))
            / sqrt(Ring::betaX);
    xpval = (mp._xp(i) - 1000. * Ring::etaPX * mp._dp_p(i))
            * sqrt(Ring::betaX) + xval * Ring::alphaX;

    yval  = mp._y(i)  / sqrt(Ring::betaY);
    ypval = mp._yp(i) * sqrt(Ring::betaY) + yval * Ring::alphaY;

    angle = -atan2(xpval, xval);
    if(angle < 0.) angle += Consts::twoPi;
    Diagnostic::XPhase(i) = angle;

    angle = -atan2(ypval, yval);
    if(angle < 0.) angle += Consts::twoPi;
    Diagnostic::YPhase(i) = angle;
  }

  for(i = 1; i <= Diagnostic::oldSize; i++)
  {
    Diagnostic::XFrac_Tune(i) = (Diagnostic::XPhase(i) -
                                 Diagnostic::XPhase_old(i)) / twopi;
    Diagnostic::YFrac_Tune(i) = (Diagnostic::YPhase(i) -
                                 Diagnostic::YPhase_old(i)) / twopi;
    if(Diagnostic::XFrac_Tune(i) < 0.0) Diagnostic::XFrac_Tune(i) += 1.0;
    if(Diagnostic::YFrac_Tune(i) < 0.0) Diagnostic::YFrac_Tune(i) += 1.0;
  }

  Diagnostic::oldSize = mp._nMacros;
  for(i = 1; i <= mp._nMacros; i++)
  {
    Diagnostic::XPhase_old(i) = Diagnostic::XPhase(i);
    Diagnostic::YPhase_old(i) = Diagnostic::YPhase(i);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FracTuneNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//    sout - stream to print to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FracTuneNode::_showDiagNode(Ostream &sout)
{
  sout << " FracTune Node:"  << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "\n";
}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LongDistNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(LongDistNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LongDistNode::_diagNodeCalculator
//
// DESCRIPTION
//   Dumps longitudinal distribution and dipole moments to a file
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LongDistNode::_diagNodeCalculator(MacroPart &mp)
{
  LongDist LD(mp, _nBins);
  LD._diagCalculator();

  //===Parallel MPI ======start======
  int iMPIini_ = 0;
  MPI_Initialized(&iMPIini_);
  int nMPIsize_ = 1;
  int nMPIrank_ = 0;

  if(iMPIini_ > 0)
  {
    MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
    MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
  }
  //===Parallel MPI ======stop=======

  if(nMPIrank_ == 0)
  {
    // get file ready to dump to:
    OFstream fio(_fName, ios::app);

    for(Integer i = 1; i <= _nBins; i++)
    {
      fio << Ring::nTurnsDone << "\t"
          << LongDist::_PhiVal(i) << "\t"
          << LongDist::_lambda(i) << "\t"
          << LongDist::_xDipole(i) << "\t"
          << LongDist::_xpDipole(i) << "\t"
          << LongDist::_yDipole(i) << "\t"
          << LongDist::_ypDipole(i) << "\n";
    }

    fio.close();
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LongDistNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//   sout - stream to print to
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LongDistNode::_showDiagNode(Ostream &sout)
{
  sout << "LongDist Node:"  << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "  ";
  sout << "nBins =  " << _nBins << "\n";
}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS MomentNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(MomentNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MomentNode::_diagNodeCalculator
//
// DESCRIPTION
//   Dumps herd moments to a file.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MomentNode::_diagNodeCalculator(MacroPart &mp)
{
  Moment mom(mp, _momOrder);
  mom._diagCalculator();

    //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======

  if(nMPIrank_ == 0){
    // get file ready to dump to:

    OFstream fio(_fName, ios::app);

    fio<< Ring::nTurnsDone  << "\t"
       << _position << "\t" << Ring::nTurnsDone*Ring::lRing +  _position << "\t";
    fio << Moment::_momentXY(2,1) << "\t";
    fio << Moment::_momentXY(1,2) << "\t";
    for(Integer i=1; i<= mom._order+1; i++)
      for(Integer j=1; j<= i; j++)
        fio << Moment::_momentXYNorm(i+1-j,j) << "\t";

    fio << "\n";

    fio.close();
  }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MomentNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//    sout - stream to print to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MomentNode::_showDiagNode(Ostream &sout)
{
  sout << " Moment Node:"  << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "  ";
  sout << " Moment order =  " << _momOrder << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS MomentSliceNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(MomentSliceNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MomentSliceNode::_diagNodeCalculator
//
// DESCRIPTION
//   Dumps herd moments to a file.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MomentSliceNode::_diagNodeCalculator(MacroPart &mp)
{
  MomentSlice mom(mp, _momOrder, _phiMin, _phiMax);
  mom._diagCalculator();

  // get file ready to dump to:

  OFstream fio(_fName, ios::app);

  fio << Ring::nTurnsDone  << "\t"
     << _position << "\t" << Ring::nTurnsDone*Ring::lRing +  _position << "\t";

  fio << MomentSlice::_momentSliceXY(2,1) << "\t";
  fio << MomentSlice::_momentSliceXY(1,2) << "\t";
  for(Integer i=1; i<= mom._order+1; i++)
    for(Integer j=1; j<= i; j++)
      fio << MomentSlice::_momentSliceXYNorm(i+1-j,j) << "\t";

  fio << "\n";

  fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MomentSliceNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//    sout - stream to print to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MomentSliceNode::_showDiagNode(Ostream &sout)
{
  sout << " Moment Slice Node:"  << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "  ";
  sout << " Moment order =  " << _momOrder << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BPMNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BPMNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   BPMNode::_diagNodeCalculator
//
// DESCRIPTION
//   Gets _xAvg, _yAvg
//
// PARAMETERS
//   None.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void BPMNode::_diagNodeCalculator(MacroPart &mp)
{
  MomentSlice mom(mp, _momOrder, _phiMin, _phiMax);
  mom._diagCalculator();

  _xAvg = MomentSlice::_momentSliceXY(2,1);
  _yAvg = MomentSlice::_momentSliceXY(1,2);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   BPMNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//   sout - stream to print to.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void BPMNode::_showDiagNode(Ostream &sout)
{
  sout << "BPM Node:" << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "  ";
  sout << "Moment order =  " << _momOrder << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BPMFreqNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BPMFreqNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   BPMFreqNode::_diagNodeCalculator
//
// DESCRIPTION
//   Gets _amplitude of mode number freq in longitudinal distribution.
//
// PARAMETERS
//   None.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void BPMFreqNode::_diagNodeCalculator(MacroPart &mp)
{
  Integer i, freq;
  Vector(Real) phiHist(_nBins), numHist(_nBins),
               xHist(_nBins), yHist(_nBins);
  Real dphi, acn, asn, xmoment, ymoment;

  dphi = (_phiMax - _phiMin) / Real(_nBins);

  phiHist(1) = _phiMin;
  for(i = 2; i <= _nBins; i++) phiHist(i) = phiHist(i-1) + dphi;
  bins(phiHist, numHist, xHist, yHist, mp);

  xmoment = 0.0;
  ymoment = 0.0;
  for(i = 1; i <= _nBins; i++)
  {
    xmoment += xHist(i);
    ymoment += yHist(i);
  }
  xmoment /= _nBins;
  ymoment /= _nBins;
  _momentX = xmoment;
  _momentY = ymoment;

  Integer freqmin = _freq0 - _band;
  Integer freqmax = _freq0 + _band;

  _freq.resize(freqmax - freqmin + 1);
  _amplitudeN.resize(freqmax - freqmin + 1);

  for(freq = freqmin; freq <= freqmax; freq++)
  {
    _freq(freq - freqmin + 1) = freq;

    acn = 0.5 * numHist(1) * cos(freq * phiHist(1));
    for(i = 2; i < _nBins; i++)
      acn = acn + numHist(i) * cos(freq * phiHist(i));
    acn = acn + 0.5 * numHist(_nBins) * cos(freq * phiHist(_nBins));

    asn = 0.5 * numHist(1) * sin(freq * phiHist(1));
    for(i = 2; i < _nBins; i++)
      asn = asn + numHist(i) * sin(freq * phiHist(i));
    asn = asn + 0.5 * numHist(_nBins) * sin(freq * phiHist(_nBins));

    _amplitudeN(freq - freqmin + 1) = (2.0 / (_phiMax - _phiMin)) *
                                      sqrt((acn * acn) + (asn * asn));
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   BPMFreqNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//   sout - stream to print to.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void BPMFreqNode::_showDiagNode(Ostream &sout)
{
  sout << "BPM Frequency Node:" << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "  ";
//  sout << "Moment order =  " << _momOrder << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS QuadBPMNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(QuadBPMNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   QuadBPMNode::_diagNodeCalculator
//
// DESCRIPTION
//   Gets _xSq, _ySq
//
// PARAMETERS
//   None.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void QuadBPMNode::_diagNodeCalculator(MacroPart &mp)
{
  MomentSlice mom(mp, _momOrder, _phiMin, _phiMax);
  mom._diagCalculator();

  _xSq = MomentSlice::_momentSliceXY(3,1);
  _ySq = MomentSlice::_momentSliceXY(1,3);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   QuadBPMNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//   sout - stream to print to.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void QuadBPMNode::_showDiagNode(Ostream &sout)
{
  sout << "Quad BPM Node:" << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "  ";
  sout << "Moment order =  " << _momOrder << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LongMountainNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(LongMountainNode, DiagnosticNode);

LongMountainNode::LongMountainNode(const String &n, const Integer &order, const Integer &nb,
                 const String &fn) :
            DiagnosticNode(n, order, fn)
 {
      _nBins = nb;
      _yBin.resize(_nBins);

    //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======

  if(nMPIrank_ == 0){
     OFstream fio(_fName, ios::out);
     fio << _nBins << "\n";   // put number of bins in 1st line of the file.

   // set up the bins:

      for (Integer i = 1; i <= _nBins; i++)
       {
	  _yBin(i) = 0.0;
       }

      _activeFlag = 1;

      fio.close();
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    LongMountainNode::_diagNodeCalculator
//
// DESCRIPTION
//   Dumps herd lonitudinal distributions to a file.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void LongMountainNode::_diagNodeCalculator(MacroPart &mp)
{
  Integer bin, i;
  Real dx;

    //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======

   dx = Consts::twoPi/Real(_nBins);

   for (i = 1; i <= mp._nMacros; i++)
   {
	  bin = 1+ Integer( (mp._phi(i) + Consts::pi) / dx );
	  bin = (bin <= _nBins ? bin : _nBins);
	  _yBin(bin)++;
   }

    //===Parallel MPI ======start======
    if( nMPIsize_ > 1){
     double* buff_0 = (double *) malloc (sizeof(double)*_nBins);
     double* buff_1 = (double *) malloc (sizeof(double)*_nBins);
     for (i = 1; i <= _nBins; i++){
       buff_0[i-1]=_yBin(i);
     }


     MPI_Allreduce(buff_0, buff_1, _nBins,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

     for (i = 1; i <= _nBins; i++){
      _yBin(i) = buff_0[i-1];
     }

     free(buff_0);
     free(buff_1);
    }
   //===Parallel MPI ======stop=======


 if( nMPIrank_ == 0){

  // get file ready to dump to:

  OFstream fio(_fName, ios::app);

  fio << Ring::nTurnsDone  << "\n";

  for (i=1; i<= _nBins; i++)
      fio << i << "\t" << _yBin(i) << "\n";

  fio.close();

 }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    LongMountainNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//    sout - stream to print to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void LongMountainNode::_showDiagNode(Ostream &sout)
{
  sout << " LongMountain Node:"  << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "  ";
  sout << " N Bins =  =  " << _nBins << "\n";

}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS StatLatNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(StatLatNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    StatLatNode::_diagNodeCalculator
//
// DESCRIPTION
//  Calculates the statistical lattice parameters of a herd and dump them
//   to a file.
//
// PARAMETERS
//    mp - reference to a macroparticle herd.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void StatLatNode::_diagNodeCalculator(MacroPart &mp)
{
  //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
  //===Parallel MPI ======stop=======

  StatLat sl(mp);
  sl._diagCalculator();    // calcu;late the stat lat's of the herd

 if( nMPIrank_ == 0){
  OFstream fio(_fName, ios::app);    // get file ready to dump to:

  fio << Ring::nTurnsDone  << "\t"
      << _position << "\t";          // dump header info

  sl._dumpDiagnostic(fio);       // dump it

  fio.close();
 }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   StatLatNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//    sout - stream to print to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void StatLatNode::_showDiagNode(Ostream &sout)
{
  sout << " Stat-Lat Node:"  << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "\n";

}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS PMTNode
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(PMTNode, DiagnosticNode);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    PMTNode::_diagNodeCalculator
//
// DESCRIPTION
//    Check to see if the main herd has completed a prescribed
//     moment oscillation. If so, dump the cannonical coordinates
//     of any other herds being tracked.
//
// PARAMETERS
//    mp - reference to the herd being tracked.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void PMTNode::_diagNodeCalculator(MacroPart &mp)
{

  if(mp._herdName == "mainHerd")    // check for mainHerd oscillations

    {
     Integer ord = Max(_hIndex, _vIndex);


     Moment mom(mp,ord); // get main herd moments
     mom._diagCalculator();

     Diagnostic::momNew(_hIndex, _vIndex) = Moment::_momentXYNorm(_hIndex, _vIndex);

     if(Diagnostic::mom0(_hIndex, _vIndex) > Diagnostic::momOld(_hIndex, _vIndex) &&
        Diagnostic::mom0(_hIndex, _vIndex) > Diagnostic::momNew(_hIndex, _vIndex) )
         Diagnostic::dumpTime(_hIndex, _vIndex) = 1;
       else
         Diagnostic::dumpTime(_hIndex, _vIndex) = 0;

     Diagnostic::momOld(_hIndex, _vIndex) = Diagnostic::mom0(_hIndex, _vIndex);
     Diagnostic::mom0(_hIndex, _vIndex) = Diagnostic::momNew(_hIndex, _vIndex);
    }

   else // check for dumping test herd info
     {

        if (!Diagnostic::dumpTime(_hIndex, _vIndex) ) return;

        CanonicalCoord cc(mp);   // get canonical coords of the other herd(s)
        cc._diagCalculator();

        OFstream fio(_fName, ios::app);     // get file ready to dump to:
        fio << Ring::nTurnsDone  << "\t"
            << _position << "\t";

        for(Integer i=1; i<= mp._nMacros; i++)
	  {
            fio << CanonicalCoord::_xCanonical(i) / Sqrt(Ring::betaX) << "\t"
                << CanonicalCoord::_yCanonical(i) / Sqrt(Ring::betaY)  << "\t"
                << CanonicalCoord::_pxCanonical(i) * Sqrt(Ring::betaX) << "\t"
                << CanonicalCoord::_pyCanonical(i) * Sqrt(Ring::betaY) << "\t";
	  }

        fio << "\n";

        fio.close();

     }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   PMTNode::_showDiagNode
//
// DESCRIPTION
//   Prints a short 1 liner about this node
//
// PARAMETERS
//    sout - stream to print to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void PMTNode::_showDiagNode(Ostream &sout)
{
  sout << " PMT Node:"  << "  "
       << _oindex << "   "
       << _position << "   "
       << _activeFlag << "  "
       << _fName << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Diagnostic::ctor
//
// DESCRIPTION
//    Initializes the various Diagnostic related stuff.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::ctor()
{

  // set some initial values
  nDiagnosticNodes = 0;
  oldSize = 0;
  deltaEmitBin = 5.;nEmitBins = 30;

  momOld.resize(10,10);
  mom0.resize(10,10);
  momNew.resize(10,10);
  dumpTime.resize(10,10);

  XPhase_old.resize(1);
  YPhase_old.resize(1);
  XPhase.resize(1);
  YPhase.resize(1);
  XFrac_Tune.resize(1);
  YFrac_Tune.resize(1);

  momOld = 1.e10;
  mom0 = Consts::tiny;
  momNew = 0.;
  dumpTime  = 0;

  XPhase_old = 0.0;
  YPhase_old = 0.0;
  XPhase = 0.0;
  YPhase = 0.0;
  XFrac_Tune = 0.0;
  YFrac_Tune = 0.0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpCanonicalCoords
//
// DESCRIPTION
//    Calculates the canonical coordinates for a herd, and dumps them to
//       a prescribed stream.
//
// PARAMETERS
//    Integer
//      herd   - reference to the herd.
//    Ostream
//      sout   - the stream to send the output to.
//
// RETURNS
//
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::dumpCanonicalCoords(const Integer &herdNo, Ostream &sout)
  {
    MacroPart *mp;

    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    CanonicalCoord cc(*mp);
    cc._diagCalculator();
    cc._dumpDiagnostic(sout);
  }

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpSigMat
//
// DESCRIPTION
//  Calculates the coordinate averages and sigma matrix
//  in {m} and {radians} for a herd, and dumps them to
//  a prescribed stream.
//
// PARAMETERS
//  Integer
//    herd   - reference to the herd.
//  Ostream
//    sout   - the stream to send the output to.
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::dumpSigMat(const Integer &herdNo, Ostream &sout)
{
  MacroPart *mp;

  if((herdNo & All_Mask) > Particles::nHerds)
    except(badHerdNo);

  mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

  SigmaMatrix SM(*mp);
  SM._diagCalculator();
  SM._dumpDiagnostic(sout);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpActions
//
// DESCRIPTION
//    Calculates the actions for a herd, and dumps them to
//       a prescribed stream.
//
// PARAMETERS
//    Integer
//      herd   - reference to the herd.
//    Ostream
//      sout   - the stream to send the output to.
//
// RETURNS
//
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::dumpActions(const Integer &herdNo, Ostream &sout)
  {
    MacroPart *mp;

    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    Actions act(*mp);
    act._diagCalculator();
    act._dumpDiagnostic(sout);
  }


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::showMoments
//
// DESCRIPTION
//    Calculates moments for a herd, and dumps them to
//       a prescribed stream.
//
// PARAMETERS
//    Integer
//      herd   - reference to the herd.
//      ord    - order of the moments to calculate
//    Ostream
//      sout   - the stream to send the output to.
//
// RETURNS
//
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::showMoments(const Integer &herdNo, const Integer &ord, Ostream &sout)
  {
    MacroPart *mp;

    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    Moment mom(*mp, ord);
    mom._diagCalculator();
    mom._showDiagnostic(sout);
  }


///////////////////////////////////////////////////////////////////////////
//
// NAME
//  syncPartPercents
//
// DESCRIPTION
//    Gets a temp vector that has gathered all information from all CPUs.
//
// PARAMETERS
//    np = number of parts on a node
//    ...
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void syncPartPercents(Vector(Real) &vect,
                      double &xe50,double &xe90,double &xe95,double &xe99,
                      double &xeMin, double &xeMax)
{

  int np = vect.rows();
  Vector(Real) temp;
  temp.resize(np);
  for( int i = 1; i <= np ; i++){
    temp(i)=vect(i);
  }


  //===Parallel MPI ======start======
  int iMPIini_=0;
  MPI_Initialized(&iMPIini_);
  int nMPIsize_ = 1;
  int nMPIrank_ = 0;

  if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
  }


  if( nMPIsize_ > 1){
     int* np_arr   = (int *) malloc ( sizeof(int)*nMPIsize_);
     int* np_arr_g = (int *) malloc ( sizeof(int)*nMPIsize_);
     int* np_displ   = (int *) malloc ( sizeof(int)*nMPIsize_);

     for( int i = 0; i < nMPIsize_; i++){
       np_arr[i]=0;
     }
     np_arr[nMPIrank_]=np;
     MPI_Allreduce(np_arr,np_arr_g,nMPIsize_,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

     int np_global = 0;
     for( int i = 0; i < nMPIsize_; i++){
          np_global += np_arr_g[i];
     }

     double* temp_g =  (double *) malloc ( sizeof(double)*np_global);

     np_displ[0]=0;
     for( int i = 1; i < nMPIsize_; i++){
          np_displ[i] = np_arr_g[i-1]+np_displ[i-1];
     }

     MPI_Gatherv((double *) &temp.data() , np , MPI_DOUBLE, temp_g, np_arr_g, np_displ, MPI_DOUBLE, 0, MPI_COMM_WORLD);

     if(nMPIrank_ == 0){
        temp. resize(np_global);
        for( int i = 0; i < np_global; i++){
          temp(i+1)= temp_g[i];
        }
      }

      np = np_global;

      free(np_arr);
      free(np_arr_g);
      free(np_displ);
      free(temp_g);
  }

  if(nMPIrank_ == 0){

    MathLib::hpSort(temp);

    xe50 = temp((int) (np * 0.5));
    xe90 = temp((int) (np * 0.9));
    xe95 = temp((int) (np * 0.95));
    xe99 = temp((int) (np * 0.99));
    xeMin = temp(1);
    xeMax = temp(np);
  }

  if( nMPIsize_ > 1){
    MPI_Bcast( &xe50, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast( &xe90, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast( &xe95, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast( &xe99, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast( &xeMin, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast( &xeMax, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  }
  //===Parallel MPI ======stop=======
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::showTunes
//
// DESCRIPTION
//    Show some characteristic information about the tune spread.
//    The tunes must have been calculated already. The tunes
//    for whatever was calculated and stored in xTune and yTune
//    are evaluated.
//
//
// PARAMETERS
//    Ostream
//      sout    - The stream to dump to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::showTunes(Ostream &sout)
{
  if(Particles::xTune.rows() < 1) except(tuneNotCalcd2);

  if(tuneCalcOn)  except(tuneNotCalcd);

  //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }

    //if(nMPIsize_ > 1){
    //  std::cerr <<"Diagnostic::showTunes:\n";
    //  std::cerr <<"The Parallel version of tune diagnostic is not implemented yet.\n";
    //  std::cerr <<"Stop.\n";
    //  MPI_Finalize();
    //  exit(1);
    //}

  //===Parallel MPI ======stop=======

    Integer nPercent;
    Real xt50, xt90, xt95, xt99, yt50, yt90, yt95, yt99;
    Real xtMin, xtMax, ytMin, ytMax;

    syncPartPercents( Particles::xTune, xt50, xt90, xt95, xt99, xtMin, xtMax);
    syncPartPercents( Particles::yTune, yt50, yt90, yt95, yt99, ytMin, ytMax);

    if(nMPIrank_ != 0 ) return;

    subsection(sout, "Tune Spread Distribution");

    sout << "\nDifferent Percentile Tunes:\n";
    sout <<" Tune" << "\t" << "   X" << "\t\t" << "  Y" << "\n" ;
    sout << "------   ----------    ---------- \n";
    long dummy = sout.setf(ios::right, ios::adjustfield);
    sout << setw(6) << "99%" << setw(13) << xt99
         << setw(13) << yt99 << "\n";
    sout << setw(6) << "95%" << setw(13) << xt95
         << setw(13) << yt95 << "\n";
    sout << setw(6) << "90%" << setw(13) << xt90
         << setw(13) << yt90 << "\n";
    sout << setw(6) << "50%" << setw(13) << xt50
         << setw(13) << yt50 << "\n";
    sout << "\n";
    sout << setw(6) << "Max" << setw(13) << xtMax
         << setw(13) << ytMax << "\n";
    sout << setw(6) << "Min" << setw(13) << xtMin
         << setw(13) << ytMin << "\n";


}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//  syncTAndA
//
// DESCRIPTION
//    Dumps the tunes / Actions for a parallel run.
//
// PARAMETERS
//    nParts = number of parts on a node
//    sout  = stream to send info to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void syncTAndA(Integer &nParts, Ostream &sout)

{
  int i,j;

  //===Parallel MPI ======start======
    MPI_Status statusMPI ;
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
  //===Parallel MPI ======stop=======

    if(nMPIrank_ == 0){
      for(j = 1; j<= nParts; j++){
       sout << Particles::xTune(j) << "\t"
            << Particles::yTune(j) << "\t"
            << Diagnostic::xAction(j) << "\t"
            << Diagnostic::yAction(j) << "\n";
      }
    }

  //===Parallel MPI ======start======
  if( nMPIsize_ > 1){
     int* buff_0 = (int *) malloc (sizeof(int)*nMPIsize_);
     int* buff_1 = (int *) malloc (sizeof(int)*nMPIsize_);
     for( i=0; i < nMPIsize_ ; i++) buff_0[i]=0;
     buff_0[nMPIrank_] = nParts;
     MPI_Allreduce(buff_0, buff_1, nMPIsize_ ,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

     int nPartsMax = buff_1[0];
     for(i=0; i < nMPIsize_ ; i++) {if(nPartsMax < buff_1[i]) nPartsMax = buff_1[i]; }

     double* buff_d = (double *) malloc (sizeof(double)*nPartsMax*4);

     if( nMPIrank_ == 0){
       for( i = 1; i <nMPIsize_ ; i++){
        MPI_Recv(buff_d, buff_1[i], MPI_DOUBLE, i , 1111 , MPI_COMM_WORLD, &statusMPI);
          for(j = 0; j< buff_1[i]; j++){
             sout << buff_d[4*j+0] << "\t"
                  << buff_d[4*j+1] << "\t"
                  << buff_d[4*j+2] << "\t"
                  << buff_d[4*j+3] << "\n";
          }
       }
     }

     if( nMPIrank_ != 0){
       for( i = 0; i < nParts; i++){
	 buff_d[4*i+0]=Particles::xTune(i+1);
	 buff_d[4*i+1]=Particles::yTune(i+1);
	 buff_d[4*i+2]=Diagnostic::xAction(i+1);
	 buff_d[4*i+3]=Diagnostic::yAction(i+1);
       }
      MPI_Send(buff_d, nParts, MPI_DOUBLE, 0, 1111 , MPI_COMM_WORLD);
     }


     free(buff_d);
     free(buff_0);
     free(buff_1);
  }

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpTAndA
//
// DESCRIPTION
//    Dumps the tunes and actions of a herd to a file.
//    The tunes must have been calculated already.
//
//
// PARAMETERS
//    Integer
//      herd    - The herd to dump
//    Ostream
//      sout    - The stream to dump to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::dumpTAndA(const Integer &herd, Ostream &sout)
{

    MacroPart *mp;
    if ( (herd & All_Mask) > nHerds) except (badHerdNo);
    mp= MacroPart::safeCast(mParts((herd & All_Mask) - 1));

    if(tuneCalcOn)  except(tuneNotCalcd);

    if(Particles::xTune.rows() < 1) except(tuneNotCalcd2);

    calcActions(herd);

    syncTAndA(mp->_nMacros, sout);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpTAndAi
//
// DESCRIPTION
//    Dumps the tunes and actions of a particle to a file.
//    The tunes must have been calculated already.
//
//
// PARAMETERS
//    Integer
//      herd    - The herd to dump
//    Ostream
//      sout    - The stream to dump to
//    Integer
//      index   - The particle index
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::dumpTAndAi(const Integer &herd, Ostream &sout,
                            const Integer &index)
{
    MacroPart *mp;
    if ( (herd & All_Mask) > nHerds) except (badHerdNo);
    mp = MacroPart::safeCast(mParts((herd & All_Mask) - 1));

    if(tuneCalcOn)  except(tuneNotCalcd);

    if(Particles::xTune.rows() < 1) except(tuneNotCalcd2);

    calcActions(herd);

    sout << Particles::xTune(index) << "\t"
         << Particles::yTune(index) << "\t"
         << Diagnostic::xAction(index) << "\t"
         << Diagnostic::yAction(index) << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpEAndCC
//
// DESCRIPTION
//    Dumps the emittances and Canonical coordinates of a herd to a stream.
//    The tunes must have been calculated already.
//
//
// PARAMETERS
//    Integer
//      herd    - The herd to dump
//    Ostream
//      sout   - The stream to dump to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::dumpEAndCC(const Integer &herd, Ostream &sout)
{

    MacroPart *mp;
    if ( (herd & All_Mask) > nHerds) except (badHerdNo);
    mp= MacroPart::safeCast(mParts((herd & All_Mask) - 1));

    calcEmitBase(herd);
    CanonicalCoord cc(*mp);
    cc._diagCalculator();

    for(Integer i = 1; i<= mp->_nMacros; i++)
      sout << xEmit(i) << "\t"
          << yEmit(i) << "\t"
          << CanonicalCoord::_xCanonical(i) << "\t"
          << CanonicalCoord::_yCanonical(i) << "\t"
          << CanonicalCoord::_pxCanonical(i) << "\t"
          << CanonicalCoord::_pyCanonical(i) << "\t"
          << mp->_dp_p(i) << "\n";

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::calcActions
//
// DESCRIPTION
//    Calculates the actions for a herd.
//
// PARAMETERS
//    Integer
//      herd   - reference to the herd.
//
// RETURNS
//
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::calcActions(const Integer &herdNo)
  {
    MacroPart *mp;

    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    Actions act(*mp);
    act._diagCalculator();
  }

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::showEmitBase
//  Diagnostic::showEmit (same as above, but for the mainHerd)
//
// DESCRIPTION
//  Calculates emittances for a herd and shows the results
//
// PARAMETERS
//  Integer
//    herd     - reference to the herd
//    ostream  - stream to send output to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::showEmit(Ostream &sout)
{
  showEmitBase(mainHerd, sout);
}

Void Diagnostic::showEmitBase(const Integer &herdNo, Ostream &sout)
  {
    MacroPart *mp;
    Real xe50, xe90, xe95, xe99, ye50, ye90, ye95, ye99;
    Real xeMin, xeMax, yeMin, yeMax;

  //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
  //===Parallel MPI ======stop=======

    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    Emittance em(*mp);
    em._diagCalculator();
    binEmit();

    if(nMPIrank_ == 0)   // for parallel runs, only parent gets to speak.
    {
      em._showDiagnostic(sout);

      subsection(sout, "Distribution:");
      sout << "\nPercent of beam with emittance > (pi-mm-mrad):\n";
      sout <<"Emittance" << "\t" << "X" << "\t\t" << "Y" << "\t"
           << "X or Y" << "\t" << "X plus Y" << "\n";
      sout << "------   ----------    ----------    ---------    "
	   <<"----------\n";
      long dummy = sout.setf(ios::right, ios::adjustfield);
      sout << setw(16);
      for(Integer i=1; i<= nEmitBins; i++)
      {
          sout << setw(6) << Real(i) * deltaEmitBin
	       << setw(13) << Diagnostic::xEmitFrac(i)
	       << setw(13) << Diagnostic::yEmitFrac(i)
	       << setw(13) << Diagnostic::xoyEmitFrac(i)
	       << setw(13) << Diagnostic::xpyEmitFrac(i) << "\n";
      }
    }

    syncPartPercents( xEmit, xe50, xe90, xe95, xe99, xeMin, xeMax);
    syncPartPercents( yEmit, ye50, ye90, ye95, ye99, yeMin, yeMax);

    if( nMPIrank_ != 0) return;

    subsection(sout, "Emittance Distribution");

    sout << "\nEmittance Percentile:\n";
    sout <<" Emit:" << "\t" << "   X" << "\t\t" << "  Y" << "\n" ;
    sout << "------   ----------    ---------- \n";
    long dummy = sout.setf(ios::right, ios::adjustfield);
    sout << setw(6) << "99%" << setw(13) << xe99
         << setw(13) << ye99 << "\n";
    sout << setw(6) << "95%" << setw(13) << xe95
         << setw(13) << ye95 << "\n";
    sout << setw(6) << "90%" << setw(13) << xe90
         << setw(13) << ye90 << "\n";
    sout << setw(6) << "50%" << setw(13) << xe50
         << setw(13) << ye50 << "\n";
    sout << "\n";
    sout << setw(6) << "Max" << setw(13) << xeMax
         << setw(13) << yeMax << "\n";
    sout << setw(6) << "Min" << setw(13) << xeMin
         << setw(13) << yeMin << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::showEmitPercent
//
// DESCRIPTION
//  Calculates emittances for a herd, and shows xxx percent emittance
//
// PARAMETERS
//    Integer
//      herd     - reference to the herd
//      ostream  - stream to send output to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::showEmitPercent(const Integer &herdNo, Ostream &sout,
                                 const Integer &Turn, Real &percent)
{
  MacroPart *mp;
  Integer indx, indy;
  Real xepct, yepct, xfrac, yfrac, pctcomp;

  //===Parallel MPI ======start======
  int iMPIini_=0;
  MPI_Initialized(&iMPIini_);
  int nMPIsize_ = 1;
  int nMPIrank_ = 0;

  if(iMPIini_ > 0)
  {
    MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
    MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
  }
  //===Parallel MPI ======stop=======

  if ((herdNo & All_Mask) > Particles::nHerds) except(badHerdNo);

  SyncPart *sp = SyncPart::safeCast(syncP(0));
  mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

  Emittance em(*mp);
  em._diagCalculator();
  binEmit();
  if(nMPIrank_ == 0)   // for parallel runs, only parent gets to speak.
  {
    pctcomp = 100.0 - percent;
    for(Integer i = 1; i <= nEmitBins; i++)
    {
      if(Diagnostic::xEmitFrac(i) >= pctcomp) indx = i;
      if(Diagnostic::yEmitFrac(i) >= pctcomp) indy = i;
    }
    if(indx < 2) indx = 1;
    if(indy < 2) indy = 1;
    if(indx >= nEmitBins) indx = nEmitBins - 1;
    if(indy >= nEmitBins) indy = nEmitBins - 1;

    xfrac = (pctcomp - Diagnostic::xEmitFrac(indx)) /
            (Diagnostic::xEmitFrac(indx + 1) - Diagnostic::xEmitFrac(indx));
    yfrac = (pctcomp - Diagnostic::yEmitFrac(indy)) /
            (Diagnostic::yEmitFrac(indy + 1) - Diagnostic::yEmitFrac(indy));

    xepct = (double(indx) + xfrac) * deltaEmitBin;
    yepct = (double(indy) + yfrac) * deltaEmitBin;

    sout << "Turn = " << Turn << "   "
         << percent << "% X Emittance = " << xepct << "   "
         << percent << "% Y Emittance = " << yepct << "\n";
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::showEmitBaseco
//
// DESCRIPTION
//    Calls an emittance (relative to closed orbit) calc for a herd,
//    and shows the results
//
// PARAMETERS
//    Integer
//      herd     - reference to the herd
//      ostream  - stream to send output to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////



Void Diagnostic::showEmitBaseco(const Integer &herdNo, Ostream &sout)

  {
    MacroPart *mp;
    Real xe50, xe90, xe95, xe99, ye50, ye90, ye95, ye99;
    Real xeMin, xeMax, yeMin, yeMax;

  //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
  //===Parallel MPI ======stop=======

    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    Emittanceco em(*mp);
    em._diagCalculator();
    binEmitco();

    if(nMPIrank_ == 0)   // for parallel runs, only parent gets to speak.
    {
     sout <<"\n\nEMITTANCE OUTPUT \n";
     sout << "X Ave Emittance (pi-mm-mrad): " << Diagnostic::xEmitAveco <<"\n";
     sout << "X max Emittance (pi-mm-mrad): " << Diagnostic::xEmitMaxco <<"\n";
     sout << "Y Ave Emittance (pi-mm-mrad): " << Diagnostic::yEmitAveco <<"\n";
     sout << "Y max Emittance (pi-mm-mrad): " << Diagnostic::yEmitMaxco <<"\n";

     subsection(sout, "Distribution:");
     sout << "\nPercent of beam with emittance > (pi-mm-mrad):\n";
     sout <<"Emittance" << "\t" << "X" << "\t\t" << "Y" << "\t"
          << "X or Y" << "X plus Y" << "\n";
     sout << "------   ----------    ----------    ---------    "
          << "----------\n";
     long dummy = sout.setf(ios::right, ios::adjustfield);
     sout << setw(16);
     for(Integer i=1; i<= nEmitBins; i++)
     {
       sout << setw(6) << Real(i) * deltaEmitBin
            << setw(13) << Diagnostic::xEmitFracco(i)
            << setw(13) << Diagnostic::yEmitFracco(i)
            << setw(13) << Diagnostic::xoyEmitFracco(i)
	    << setw(13) << Diagnostic::xpyEmitFracco(i) << "\n";
     }
    }


    syncPartPercents( xEmitco, xe50, xe90, xe95, xe99, xeMin, xeMax);
    syncPartPercents( yEmitco, ye50, ye90, ye95, ye99, yeMin, yeMax);

    if(nMPIrank_ != 0) return;

    subsection(sout, "Emittance Distribution");

    sout << "\nEmittance Percentile:\n";
    sout <<" Emit:" << "\t" << "   X" << "\t\t" << "  Y" << "\n" ;
    sout << "------   ----------    ---------- \n";
    long dummy = sout.setf(ios::right, ios::adjustfield);
    sout << setw(6) << "99%" << setw(13) << xe99
         << setw(13) << ye99 << "\n";
    sout << setw(6) << "95%" << setw(13) << xe95
         << setw(13) << ye95 << "\n";
    sout << setw(6) << "90%" << setw(13) << xe90
         << setw(13) << ye90 << "\n";
    sout << setw(6) << "50%" << setw(13) << xe50
         << setw(13) << ye50 << "\n";
    sout << "\n";
    sout << setw(6) << "Max" << setw(13) << xeMax
         << setw(13) << yeMax << "\n";
    sout << setw(6) << "Min" << setw(13) << xeMin
         << setw(13) << yeMin << "\n";
  }


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::showEmitSlice
//
// DESCRIPTION
//    Calls an RMS emittance calc for a particular longitudinal slice of
//    a herd, and shows the results.
//
// PARAMETERS
//    Integer
//      herd     - reference to the herd
//      ostream  - stream to send output to
//      _phiMin  - minimum longitudinal emittance of the slice
//      _phiMax  - maximum longitudinal emittance of the slice
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::showEmitSlice(const Integer &herdNo , Ostream &sout,
const Real &phiMin, const Real &phiMax)

{
    MacroPart *mp;
    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    EmittanceSlice ems(*mp, phiMin, phiMax);
    ems._diagCalculator();

    ems._showDiagnostic(sout);
}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpEmitco
//
// DESCRIPTION
//    Calls an emittance calc for a herd (relative to closed orbit), and
//    dumps individual particle emittances to a stream
//
// PARAMETERS
//    Integer
//      herd     - reference to the herd
//      ostream  - stream to send output to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////



Void Diagnostic::dumpEmitco(const Integer &herdNo, Ostream &sout)
{

  //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
  //===Parallel MPI ======stop=======

    MacroPart *mp;
    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    Emittanceco em(*mp);
    em._diagCalculator();
    binEmitco();

    if(nMPIrank_ == 0) {
      for(Integer i = 1; i<= mp->_nMacros; i++){
	sout << Diagnostic::xEmitco(i) << "\t";
	sout << Diagnostic::yEmitco(i) << "\n";
      }
    }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpEmitAveco
//
// DESCRIPTION
//    Calls an emittance calc for a herd (relative to closed orbit), and
//    dumps the average and maximum emittances to a stream
//
// PARAMETERS
//    Integer
//      herd     - reference to the herd
//      ostream  - stream to send output to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////



Void Diagnostic::dumpEmitAveco(const Integer &herdNo, Ostream &sout)
{
  //===Parallel MPI ======start======
    int iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    int nMPIsize_ = 1;
    int nMPIrank_ = 0;

    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
  //===Parallel MPI ======stop=======

    MacroPart *mp;
    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);

    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

    Emittanceco em(*mp);
    em._diagCalculator();
    binEmitco();

    if(nMPIrank_ == 0) {
     sout << Diagnostic::xEmitAveco << "\t";
     sout << Diagnostic::yEmitAveco << "\t";
     sout << Diagnostic::xEmitMaxco << "\t";
     sout << Diagnostic::yEmitMaxco << "\n";
    }

}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::calcEmitBase
//
// DESCRIPTION
//    Calls an RMS emittance calc for a herd.
//
// PARAMETERS
//    Integer
//      herd     - reference to the herd
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::calcEmitBase(const Integer &herdNo)
{
  MacroPart *mp;

  if((herdNo & All_Mask) > Particles::nHerds) except(badHerdNo);

  SyncPart *sp = SyncPart::safeCast(syncP(0));
  mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1));

  Emittance em(*mp);
  em._diagCalculator();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::binEmit
//
// DESCRIPTION
//    Bin the X and Y particle emittances above specified emittance levels.
//
//   Be sure to externally specify:
//    deltaEmitBin - The emittance increment for bins (pi-mm-mrad)
//    nEmitBins     - The number of bins
//
// PARAMETERS
//    none
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::binEmit()
{

    Integer i, k, km, nX, nY, nXpY;
    Integer nParts;

    Vector(Integer) iX, iY, iXoY, iXpY;
    iX.resize(nEmitBins);
    iY.resize(nEmitBins);
    iXoY.resize(nEmitBins);
    iXpY.resize(nEmitBins);
    xEmitFrac.resize(nEmitBins);
    yEmitFrac.resize(nEmitBins);
    xoyEmitFrac.resize(nEmitBins);
    xpyEmitFrac.resize(nEmitBins);


    for (i=1; i<= nEmitBins; i++){
      xEmitFrac(i) = 0.0;
      yEmitFrac(i) = 0.0;
      xoyEmitFrac(i)= 0.0;
      xpyEmitFrac(i)= 0.0;
    }

    nParts = xEmit.rows();

    iX = 0;  iY = 0;  iXoY = 0;  iXpY = 0;

    for(i=1; i<= nParts; i++){
      nX = Integer(xEmit(i) / deltaEmitBin);
      if(nX  > nEmitBins) nX = nEmitBins;
      for(k=1; k<=nX; k++) iX(k)++;

      nY = Integer(yEmit(i) / deltaEmitBin);
      if(nY  > nEmitBins) nY = nEmitBins;
      for(k=1; k<=nY; k++) iY(k)++;

      km = nX;
      if (nY > nX) km = nY;
      for(k=1; k<=km; k++) iXoY(k)++;

      nXpY = Integer((xEmit(i) + yEmit(i)) / deltaEmitBin);
      if(nXpY  > nEmitBins) nXpY = nEmitBins;
      for(k=1; k<=nXpY; k++) iXpY(k)++;
    }

    for (i=1; i<= nEmitBins; i++){
     if(nParts != 0) xEmitFrac(i) = 100. * Real(iX(i))/Real(nParts);
     if(nParts != 0) yEmitFrac(i) = 100. * Real(iY(i))/Real(nParts);
     if(nParts != 0) xoyEmitFrac(i)= 100. * Real(iXoY(i))/Real(nParts);
     if(nParts != 0) xpyEmitFrac(i)= 100. * Real(iXpY(i))/Real(nParts);
    }

    syncEmitBins(xEmitFrac, yEmitFrac, xoyEmitFrac, xpyEmitFrac, nParts);
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::binEmitco
//
// DESCRIPTION
//    Bin the X and Y particle emittances above specified emittance levels.
//    (Emittances calculated relative to closed orbit).
//   Be sure to externally specify:
//    deltaEmitBin - The emittance increment for bins (pi-mm-mrad)
//    nEmitBins    - The number of bins
//
// PARAMETERS
//    none
//
// RETURNS
//    None
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::binEmitco()
{
    Integer i, k, km, nX, nY, nXpY;
    Integer nParts;

    Vector(Integer) iX, iY, iXoY, iXpY;
    iX.resize(nEmitBins);
    iY.resize(nEmitBins);
    iXoY.resize(nEmitBins);
    iXpY.resize(nEmitBins);
    xEmitFracco.resize(nEmitBins);
    yEmitFracco.resize(nEmitBins);
    xoyEmitFracco.resize(nEmitBins);
    xpyEmitFracco.resize(nEmitBins);

    for (i=1; i<= nEmitBins; i++){
      xEmitFracco(i) = 0.0;
      yEmitFracco(i) = 0.0;
      xoyEmitFracco(i)= 0.0;
      xpyEmitFracco(i)= 0.0;
    }

    nParts = xEmitco.rows();

    iX = 0;  iY = 0;  iXoY = 0; iXpY=0;

    for(i=1; i<= nParts; i++){
        nX = Integer(xEmitco(i) / deltaEmitBin);
        if(nX  > nEmitBins) nX = nEmitBins;
        for(k=1; k<=nX; k++) iX(k)++;

        nY = Integer(yEmitco(i) / deltaEmitBin);
        if(nY  > nEmitBins) nY = nEmitBins;
        for(k=1; k<=nY; k++) iY(k)++;

        km = nX;
        if (nY > nX) km = nY;
        for(k=1; k<=km; k++) iXoY(k)++;

	nXpY = Integer((xEmitco(i) + yEmitco(i)) / deltaEmitBin);
        if(nXpY  > nEmitBins) nXpY = nEmitBins;
        for(k=1; k<=nXpY; k++) iXpY(k)++;
    }

    for (i=1; i<= nEmitBins; i++){
      if(nParts != 0) xEmitFracco(i)  = 100. * Real(iX(i))/Real(nParts);
      if(nParts != 0) yEmitFracco(i)  = 100. * Real(iY(i))/Real(nParts);
      if(nParts != 0) xoyEmitFracco(i) = 100. * Real(iXoY(i))/Real(nParts);
      if(nParts != 0) xpyEmitFracco(i) = 100. * Real(iXpY(i))/Real(nParts);
    }

    syncEmitBins(xEmitFracco, yEmitFracco, xoyEmitFracco, xpyEmitFracco,
		 nParts);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//  syncEmitBins
//
// DESCRIPTION
//  syncs info for parallel emittance binning calculation.
//
// PARAMETERS
//   ......
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Integer syncEmitBins(Vector(Real) &xEmitFrac, Vector(Real) &yEmitFrac,
                     Vector(Real) &xoyEmitFrac,
		     Vector(Real) &xpyEmitFrac, Integer &nParts)
{

 //===Parallel MPI ======start======
 int iMPIini_=0;
 MPI_Initialized(&iMPIini_);
 int nMPIsize_ = 1;
 int nMPIrank_ = 0;
 if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
 }
 if( nMPIsize_ == 1) return -1;
 //===Parallel MPI ======stop=======

  int npts = Diagnostic::nEmitBins;

  int np = nParts;
  int npGlob;

  for(int j=1; j<= npts; j++){
   xEmitFrac(j) = xEmitFrac(j) *np;
   yEmitFrac(j) = yEmitFrac(j) *np;
   xoyEmitFrac(j)= xoyEmitFrac(j)*np;
   xpyEmitFrac(j)= xpyEmitFrac(j)*np;
  }

  double*  xef= (double *) malloc (sizeof(double)*npts);
  double*  yef= (double *) malloc (sizeof(double)*npts);
  double* xoyef= (double *) malloc (sizeof(double)*npts);
  double* xpyef= (double *) malloc (sizeof(double)*npts);

  MPI_Allreduce((double *) &xEmitFrac.data(),  xef, npts, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce((double *) &yEmitFrac.data(),  yef, npts, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce((double *) &xoyEmitFrac.data(),xoyef, npts, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce((double *) &xpyEmitFrac.data(),xpyef, npts, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  MPI_Allreduce(&np , &npGlob , 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  for(int j=1; j<= npts; j++){
   xEmitFrac(j) = xef[j-1]/npGlob;
   yEmitFrac(j) = yef[j-1]/npGlob;
   xoyEmitFrac(j)= xoyef[j-1]/npGlob;
   xpyEmitFrac(j)= xpyef[j-1]/npGlob;
  }

  free(xef);
  free(yef);
  free(xoyef);
  free(xpyef);

  return 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Diagnostic::addFracTuneNode
//
// DESCRIPTION
//   adds a fractional tune diagnostic node to the ring
//
// PARAMETERS
//
//   String:  name  - name of the node
//   Integer: order - order index for Ring location
//   String:  fn    - file Name to dump to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addFracTuneNode(const String &name,
                                 const Integer &order,
                                 const String &fn)
{
  //clear the output file in the beginning  ===shishlo===
  OFstream fio(fn, ios::out);
  fio.close();

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
     DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes - 1) = new FracTuneNode(name, order, fn);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes - 1) = nodes(nNodes - 1);

   nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::dumpFracTunes
//
// DESCRIPTION
//    Dumps fractional tunes of a herd to a file.
//    The tunes must have been calculated already.
//
// PARAMETERS
//    Integer
//      herd    - The herd to dump
//    Ostream
//      sout    - The stream to dump to.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::dumpFracTunes(Ostream &sout)
{
  Integer i;
  for(i = 1; i <= Diagnostic::oldSize; i++)
  {
    sout << Diagnostic::XFrac_Tune(i) << "   "
         << Diagnostic::YFrac_Tune(i) << "\n";
  }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Diagnostic::addLongDistNode
//
// DESCRIPTION
//    adds a beam longitudinal distribution node to the ring
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      nBins    - number of longitudinal bins
//      fName    - file Name to dump to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addLongDistNode(const String &name,
                                 const Integer &order,
                                 const  Integer &nBins,
                                 const String &fn)
{

  //clear the output file in the beginning  ===shishlo===
  OFstream fio(fn, ios::out);
  fio.close();

  if(nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
     DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes - 1) = new LongDistNode(name, order, nBins, fn);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes - 1) = nodes(nNodes - 1);

   nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addMomentNode
//
// DESCRIPTION
//    adds a beam moment diagnostic node to the ring
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      momOrder - order of the beam moments to take
//      fName    - file Name to dump to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addMomentNode(const String &name,
     const Integer &order, const  Integer &mo, const String &fn)
{

  //clear the output file in the beginning  ===shishlo===
  OFstream fio(fn, ios::out);
  fio.close();

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
     DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new MomentNode(name, order, mo, fn);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes -1) = nodes(nNodes-1);

   nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addMomentNodeSet
//
// DESCRIPTION
//    Adds a beam moment diagnostic node set to the ring. One moment node
//    is added after each transfer matrix.
//
// PARAMETERS
//    Integer
//      mo      - order of the beam moments to take
//      fn      - file Name to dump to
//      choice  - "all" complete set of nodes
//                "external" leaves out nodes inside of magnets
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addMomentNodeSet(const Integer &mo, const String &fn,
                                  const String &choice)
{
  String wname;
  char num2[10];
  String cons("Mom. Node ");
  MapBase *tmm, *tmp;
  String eletypem, eletypep;
  Integer nm, np;

  //clear the output file in the beginning ===shishlo===
  OFstream fio(fn, ios::out);
  fio.close();

  for (nm = 0; nm < nTransMaps; nm++)
  {
    np = nm + 1;
    tmm = MapBase::safeCast(tMaps(nm));
    sprintf(num2, "(%d)", np);
    wname = cons + num2;
    if (choice == "all")
    {
      addMomentNode(wname, (tmm -> _oindex + 5), mo, fn);
    }
    else
    {
      if (np == nTransMaps) np = 0;
      tmp = MapBase::safeCast(tMaps(np));
      eletypem = tmm ->_et;
      eletypep = tmp ->_et;
      if (eletypem != eletypep)
        addMomentNode(wname, (tmm -> _oindex + 5), mo, fn);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addMomentSliceNode
//
// DESCRIPTION
//    Adds a MomentSliceNode for a particular longitudinal
//    slice to the ring to the ring.
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      momOrder - order of the beam moments to take
//      fName    - file Name to dump to
//      phiMin  - minimum longitudinal phi allowed in moment calcs.
//      phiMax  - maximul longitudinal phi allowed in moment calcs.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addMomentSliceNode(const String &name,
     const Integer &order, const  Integer &mo, const String &fn,
     const Real &phiMin, const Real &phiMax)
{
  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
     DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new MomentSliceNode(name, order, mo, fn,
					 phiMin, phiMax);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes -1) = nodes(nNodes-1);

   nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addMomentSliceNodeSet
//
// DESCRIPTION
//    Adds a beam moment slice diagnostic node set to the ring. One
//    moment node is added after each transfer matrix.
//
// PARAMETERS
//    Integer
//      mo      - order of the beam moments to take
//      fn      - file Name to dump to
//      choice  - "all" complete set of nodes
//                "external" leaves out nodes inside of magnets
//      phiMin  - minimum longitudinal phi allowed in moment calcs.
//      phiMax  - maximul longitudinal phi allowed in moment calcs.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addMomentSliceNodeSet(const  Integer &mo,
                                       const String &fn,
                                       const String &choice,
                                       const Real &phiMin,
                                       const Real &phiMax)
{
  String wname;
  char num2[10];
  String cons("Mom. Node ");
  MapBase *tmm, *tmp;
  String eletypem, eletypep;
  Integer nm, np;

  //clear the output file in the beginning ===shishlo===
  OFstream fio(fn, ios::out);
  fio.close();

  for (nm = 0; nm < nTransMaps; nm++)
  {
    np = nm + 1;
    tmm = MapBase::safeCast(tMaps(nm));
    sprintf(num2, "(%d)", np);
    wname = cons + num2;
    if (choice == "all")
    {
      addMomentSliceNode(wname, (tmm -> _oindex + 5), mo, fn,
                         phiMin, phiMax);
    }
    else
    {
      if (np == nTransMaps) np = 0;
      tmp = MapBase::safeCast(tMaps(np));
      eletypem = tmm ->_et;
      eletypep = tmp ->_et;
      if (eletypem != eletypep)
        addMomentSliceNode(wname, (tmm -> _oindex + 5), mo, fn,
                           phiMin, phiMax);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addBPMNode
//
// DESCRIPTION
//    Adds a BPMNNode for a particular longitudinal
//    slice to the ring to the ring.
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      momOrder - order of the beam moments to take
//      fName    - file Name to dump to
//      phiMin  - minimum longitudinal phi allowed in moment calcs.
//      phiMax  - maximul longitudinal phi allowed in moment calcs.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Integer Diagnostic::addBPMNode(const String &name, const Integer &order,
                            const Integer &mo, const String &fn,
                            const Real &phiMin, const Real &phiMax)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
    DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new BPMNode(name, order, mo, fn, phiMin, phiMax);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes -1) = nodes(nNodes-1);

   nodesInitialized = 0;
   Integer nodeIndex = nDiagnosticNodes;
   return nodeIndex;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addQuadBPMNode
//
// DESCRIPTION
//    Adds a QuadBPMNNode for a particular longitudinal
//    slice to the ring.
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      momOrder - order of the beam moments to take
//      fName    - file Name to dump to
//      phiMin  - minimum longitudinal phi allowed in moment calcs.
//      phiMax  - maximum longitudinal phi allowed in moment calcs.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Integer Diagnostic::addQuadBPMNode(const String &name,
                                   const Integer &order,
                                   const Integer &mo,
                                   const String &fn,
                                   const Real &phiMin,
                                   const Real &phiMax)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
    DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new QuadBPMNode(name, order, mo, fn, phiMin, phiMax);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes -1) = nodes(nNodes-1);

   nodesInitialized = 0;
   Integer nodeIndex = nDiagnosticNodes;
   return nodeIndex;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Diagnostic::BPMSignal
//
// DESCRIPTION
//    Gets Horizontal or Vertical BPM Signal.
//
// PARAMETERS
//    Integer
//      nodeIndex - nDiagnosticNodes for node
//    String
//      XorY - Horizontal or Vertical Returned
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Real Diagnostic::BPMSignal(const Integer &nodeIndex, const String &XorY)
{
  BPMNode *dp;
  Real Signal;

  dp = (BPMNode*) DiagnosticNode::safeCast(DiagnosticPointers(nodeIndex-1));
  if(XorY == "X") Signal = dp->_xAvg;
  if(XorY == "Y") Signal = dp->_yAvg;

  return Signal;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Diagnostic::BPMFreqSignal
//
// DESCRIPTION
//    Gets BPMF Signal.
//
// PARAMETERS
//    Integer
//      nodeIndex - nDiagnosticNodes for node
//
// RETURNS
//    amplitude
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::BPMFreqSignal(const Integer &nodeIndex, Ostream &sout,
                               const String &dist, Vector(Real) &param)
{
  BPMFreqNode *dp;
  Integer i, imax, pa;
  String adist;
  Real pmin, pmax, area, errtest, dx, dy;
  Real gmin, gmax, tol2, gerr, root;
  Vector(Real) aparam;

  dp = (BPMFreqNode*)
       DiagnosticNode::safeCast(DiagnosticPointers(nodeIndex-1));

  Error::drand(param(1));
  adist = dist;

  aparam.resize(7);
  for (pa = 1; pa <= 7; pa++)
  {
     aparam(pa) = param(pa);
  }

  if(adist == "UNIFORM")
  {
    dx = aparam(2) + Error::drand(0) * (aparam(3) - aparam(2));
    dy = aparam(2) + Error::drand(0) * (aparam(3) - aparam(2));
  }
  if(adist == "GAUSS")
  {
    tol2 = 1.0e-14;
    gmin = 0.0;
    gmax = 10.0;
    pmin = 0.0;
    pmax = 1.0;

    if (aparam(4) > 0.)
    {
       pmin = 0.5 - 0.5 * Error::derf((aparam(4)) / pow(2.,0.5));
       pmax = 0.5 + 0.5 * Error::derf((aparam(4)) / pow(2.,0.5));
    }

    area = pmin + (pmax - pmin) * Error::drand(0);
    errtest = 2. * area - 1.0;
    if (errtest < 0.) errtest = -errtest;
    gmax = 10.0;
    while ((Error::derf(gmax) - errtest) < 0.) gmax *= 10.0;
    root = Error::root_normal(errtest, gmin, gmax, tol2);
    if (area >= 0.5)
    {
       dx = aparam(2) + pow(2.,0.5) * aparam(3) * root;
       dy = aparam(2) + pow(2.,0.5) * aparam(3) * root;
    }
    else
    {
       dx = aparam(2) - pow(2.,0.5) * aparam(3) * root;
       dy = aparam(2) + pow(2.,0.5) * aparam(3) * root;
    }
  }

  sout << ((dp -> _momentX) + dx) << "  ";
  sout << ((dp -> _momentY) + dy);

  imax = 2 * (dp -> _band) + 1;
  for(i = 1; i <= imax; i++)
  {
    sout << "  " << dp -> _freq(i) << "  " << dp -> _amplitudeN(i);
  }
  sout << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    Diagnostic::QuadBPMSignal
//
// DESCRIPTION
//    Gets Horizontal or Vertical Quad BPM Signal.
//
// PARAMETERS
//    Integer
//      nodeIndex - nDiagnosticNodes for node
//    String
//      XorY - Horizontal or Vertical Returned
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Real Diagnostic::QuadBPMSignal(const Integer &nodeIndex, const String &XorY)
{
  QuadBPMNode *dp;
  Real Signal;

  dp = (QuadBPMNode*)
        DiagnosticNode::safeCast(DiagnosticPointers(nodeIndex-1));
  if(XorY == "X") Signal = dp->_xSq;
  if(XorY == "Y") Signal = dp->_ySq;

  return Signal;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addBPMFreqNode
//
// DESCRIPTION
//    Adds a BPMFreqNode
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      fn       - filename
//      phiMin   - phiMin
//      phiMax   - phiMax
//      nBins    - number of phi bins
//      freq     - mode number to calculate
//      herdNo   - herd for calculation
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Integer Diagnostic::addBPMFreqNode(const String &n, const Integer &order,
                                   const Real &phiMin, const Real &phiMax,
                                   const Integer &nBins, const Integer &freq,
                                   const Integer &band)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
    DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new BPMFreqNode(n, order, phiMin, phiMax,
                                     nBins, freq, band);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes -1) = nodes(nNodes-1);

   nodesInitialized = 0;
   Integer nodeIndex = nDiagnosticNodes;
   return nodeIndex;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addStatLatNode
//
// DESCRIPTION
//    adds a stat lat diagnostic node to the ring
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      fName    - file Name to dump to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addStatLatNode(const String &name,
     const Integer &order, const String &fn)

{

  //clear the output file in the beginning ===shishlo===
  OFstream fio(fn, ios::out);
  fio.close();

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
     DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new StatLatNode(name, order, fn);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes -1) = nodes(nNodes-1);

   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addStatLatNodeSet
//
// DESCRIPTION
//    Adds a statistical lattice diagnostic node set to the ring.
//    One StatLat node is added after each transfer matrix.
//
// PARAMETERS
//    Integer
//      fn      - file Name to dump to
//      choice  - "all" complete set of nodes
//                "external" leaves out nodes inside of magnets
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addStatLatNodeSet(const String &fn, const String &choice)
{
  String wname;
  char num2[10];
  String cons("Stat-Lat Node");
  MapBase *tmm, *tmp;
  String eletypem, eletypep;
  Integer nm, np;

  //clear the output file in the beginning ===shishlo===
  OFstream fio(fn, ios::out);
  fio.close();

  for (nm = 0; nm < nTransMaps; nm++)
  {
    np = nm + 1;
    tmm = MapBase::safeCast(tMaps(nm));
    sprintf(num2, "(%d)", np);
    wname = cons + num2;
    if (choice == "all")
    {
      addStatLatNode(wname, (tmm -> _oindex + 5), fn);
    }
    else
    {
      if (np == nTransMaps) np = 0;
      tmp = MapBase::safeCast(tMaps(np));
      eletypem = tmm ->_et;
      eletypep = tmp ->_et;
      if (eletypem != eletypep)
        addStatLatNode(wname, (tmm -> _oindex + 5), fn);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addPMTNode
//
// DESCRIPTION
//    adds a PMT (Poincare-Moment-Tracking) diagnostic node to the ring
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      hIndex   - Horizontal index of the moment to use for checking for
//                  core oscillations.
//      vIndex   - Horizontal index of the moment to use for checking for
//                  core oscillations.
//
//    String
//      fName    - file Name to dump to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addPMTNode(const String &name,
     const Integer &order, const String &fn, const Integer &hIndex,
     const Integer&vIndex)

{

  if(hIndex > 10 || vIndex > 10) except(badPMTMoment);

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
     DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new PMTNode(name, order, fn, hIndex, vIndex);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes -1) = nodes(nNodes-1);

   nodesInitialized = 0;

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addPMTNodeSet
//
// DESCRIPTION
//    Adds a PMT  diagnostic node set to the ring. One moment node
//    is added after each transfer matrix.
//
// PARAMETERS
//    Integer
//      hIndex   - Horizontal index of the moment to use for checking for
//                  core oscillations.
//      vIndex   - Horizontal index of the moment to use for checking for
//                  core oscillations.
//
//   String
//      fn      - file Name to dump to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addPMTNodeSet(const String &fn, const Integer &hi,
              const Integer &vi)

{
    String wname;
    char num2[10];
    String cons("PMT Node");
    MapBase *tm;

    //clear the output file in the beginning ===shishlo===
    OFstream fio(fn, ios::out);
    fio.close();

    // Add kick for 1st half of TM number 1:

    for (Integer n=1; n <= nTransMaps; n++)
    {
        tm = MapBase::safeCast(tMaps(n-1));
        sprintf(num2, "(%d)", n);
        wname = cons+num2;
        addPMTNode(wname, (tm->_oindex+5), fn, hi, vi);
    }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Diagnostic::addLongMountainNode
//
// DESCRIPTION
//    adds a LongMountain diagnostic node to the ring.
//    This node will dump longitudinal distribution info to a file. The
//    first line the the number of bins used. Thene every time the node
//    is hit when active, it dumps
//    a) the turn number on a line,
//    b) bin number 1 and the number of parts in it
//    c) bin number 2 and the number of parts in it
//    ...
//   ?) bin number n and the number of parts in it.
//
//   Binning is done between -pi and pi.
//
// PARAMETERS
//    Integer
//      name     - name of the node
//      order    - order index for Ring location
//      nBins   - The number of bins to use in  the longitudinal distribution
//    String
//      fName    - file Name to dump to
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::addLongMountainNode(const String &name,
     const Integer &order,  const Integer &nBins, const String &fn)

{

    //clear the output file in the beginning ===shishlo===
    OFstream fio(fn, ios::out);
    fio.close();

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nDiagnosticNodes == DiagnosticPointers.size())
     DiagnosticPointers.resize(nDiagnosticNodes + 1);

   nNodes++;

   nodes(nNodes-1) = new LongMountainNode(name, order, nBins, fn);
   nDiagnosticNodes++;

   DiagnosticPointers(nDiagnosticNodes -1) = nodes(nNodes-1);

   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Diagnostic::showDiagnostics
//
// DESCRIPTION
//    Prints out the settings of the diagnostic nodes in the ring.
//
// PARAMETERS
//    sout - The stream to send output to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::showDiagnostics(Ostream &sout)
{

  DiagnosticNode *dp;

  sout << " Node Type:"  << "  "
       << "index" << "   "
       << "position" << "   "
       << "active" << "  "
       << " f-name" << "\n";

  if (nDiagnosticNodes == 0)
    {
      sout << "\n No Diagnostics are added \n\n";
      return;
    }

   for (Integer i=1; i<= nDiagnosticNodes; i++)
     {

        dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
        dp->_showDiagNode(sout);
     }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Diagnostic::activateFracTuneNodes
//    Diagnostic::activateMomentNodes
//    Diagnostic::activateMomentSliceNodes
//    Diagnostic::activateBPMNodes
//    Diagnostic::activateBPMFreqNodes
//    Diagnostic::activateQuadBPMNodes
//    Diagnostic::activateStatLatNodes
//    Diagnostic::activatePMTNodes
//
// DESCRIPTION
//    Sets the active Flag to 1 for all moment Nodes
//
// PARAMETERS
//    sout - The stream to send output to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::activateFracTuneNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(FracTuneNode::desc() ) ) dp->_activeFlag = 1;
  }
}

Void Diagnostic::activateMomentNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

   for (Integer i=1; i<= nDiagnosticNodes; i++)
     {

        dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
        if(dp->isKindOf(MomentNode::desc() ) ) dp->_activeFlag = 1;
     }
}

Void Diagnostic::activateMomentSliceNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(MomentSliceNode::desc() ) ) dp->_activeFlag = 1;
  }
}

Void Diagnostic::activateLongDistNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for(Integer i = 1; i <= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i - 1));
    if(dp->isKindOf(LongDistNode::desc() ) ) dp->_activeFlag = 1;
  }
}

Void Diagnostic::activateBPMNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(BPMNode::desc() ) ) dp->_activeFlag = 1;
  }
}

Void Diagnostic::activateBPMFreqNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(BPMFreqNode::desc() ) ) dp->_activeFlag = 1;
  }
}

Void Diagnostic::activateQuadBPMNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(QuadBPMNode::desc() ) ) dp->_activeFlag = 1;
  }
}

Void Diagnostic::activateMomentNode(const Integer &j)
{
  Integer k = 0;

  DiagnosticNode *dp;
  if (nDiagnosticNodes == 0) return;

  Integer i=1;
  while (k < j)
   {
     dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
     i++;
     if(dp->isKindOf(MomentNode::desc() )) k++;
      if (k==j) dp->_activeFlag = 1;
      if (i > nDiagnosticNodes) break;
   }
}


Void Diagnostic::activateStatLatNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
     {

        dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
        if(dp->isKindOf(StatLatNode::desc() ) ) dp->_activeFlag = 1;
     }
}

Void Diagnostic::activateStatLatNode(const Integer &j)
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  Integer k=0;
  Integer i=1;
  while (k < j)
   {
     dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
     i++;
     if(dp->isKindOf(StatLatNode::desc()) ) k++;
      if (k==j) dp->_activeFlag = 1;
      if (i > nDiagnosticNodes) break;
    }
}

Void Diagnostic::activatePMTNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

   for (Integer i=1; i<= nDiagnosticNodes; i++)
     {

        dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
        if(dp->isKindOf(PMTNode::desc() ) ) dp->_activeFlag = 1;
     }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Diagnostic::deactivateFracTuneNodes
//    Diagnostic::deactivateMomentNodes
//    Diagnostic::deactivateMomentSliceNodes
//    Diagnostic::deactivateBPMNodes
//    Diagnostic::deactivateBPMFreqNodes
//    Diagnostic::deactivateQuadBPMNodes
//    Diagnostic::deactivateStatLatNodes
//    Diagnostic::deactivatePMTNodes
//
// DESCRIPTION
//    Sets the active Flag to 0 for all selected Nodes
//
// PARAMETERS
//    sout - The stream to send output to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Diagnostic::deactivateFracTuneNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(FracTuneNode::desc() )) dp->_activeFlag = 0;
  }
}

Void Diagnostic::deactivateLongDistNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for(Integer i = 1; i <= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i - 1));
    if(dp->isKindOf(LongDistNode::desc() ) ) dp->_activeFlag = 0;
  }
}

Void Diagnostic::deactivateMomentNodes()
{

  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

   for (Integer i=1; i<= nDiagnosticNodes; i++)
     {

        dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
        if(dp->isKindOf(MomentNode::desc() ) ) dp->_activeFlag = 0;
     }
}

Void Diagnostic::deactivateMomentSliceNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(MomentSliceNode::desc() ) ) dp->_activeFlag = 0;
  }
}

Void Diagnostic::deactivateBPMNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(BPMNode::desc() ) ) dp->_activeFlag = 0;
  }
}

Void Diagnostic::deactivateBPMFreqNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(BPMFreqNode::desc() ) ) dp->_activeFlag = 0;
  }
}

Void Diagnostic::deactivateQuadBPMNodes()
{
  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

  for (Integer i=1; i<= nDiagnosticNodes; i++)
  {
    dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
    if(dp->isKindOf(QuadBPMNode::desc() ) ) dp->_activeFlag = 0;
  }
}

Void Diagnostic::deactivateStatLatNodes()
{

  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

   for (Integer i=1; i<= nDiagnosticNodes; i++)
     {

        dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
        if(dp->isKindOf(StatLatNode::desc() ) ) dp->_activeFlag = 0;
     }
}

Void Diagnostic::deactivatePMTNodes()
{

  DiagnosticNode *dp;

  if (nDiagnosticNodes == 0) return;

   for (Integer i=1; i<= nDiagnosticNodes; i++)
     {

        dp = DiagnosticNode::safeCast(DiagnosticPointers(i-1));
        if(dp->isKindOf(PMTNode::desc() ) ) dp->_activeFlag = 0;
     }
}

Void bins(Vector(Real) &phiBin, Vector(Real) &nBin,
          Vector(Real) &xBin, Vector(Real) &yBin,
          MacroPart &mp)
{
   //===Parallel MPI ======start======
   int iMPIini_=0;
   MPI_Initialized(&iMPIini_);
   int nMPIsize_ = 1;
   int nMPIrank_ = 0;
   if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    //===Parallel MPI ======stop=======

    Integer i, bin, nBins =phiBin.rows();

    Real dx = phiBin(2) - phiBin(1);
    nBin = 0.;
    xBin = 0.;
    yBin = 0.;

    for(i = 1; i <= mp._nMacros; i++)
    {
      bin = 1 + Integer( (mp._phi(i) - phiBin(1)) / dx );
      bin = (bin <= nBins ? bin : nBins);
      nBin(bin)++;
      xBin(bin) = xBin(bin) + mp._x(i);
      yBin(bin) = yBin(bin) + mp._y(i);
    }

    Real factor = Real(nBins) / Real(mp._nMacros);
    for(bin = 1; bin <= nBins; bin++)
    {
      nBin(bin) = factor * nBin(bin);
      xBin(bin) = factor * xBin(bin);
      yBin(bin) = factor * yBin(bin);
    }

  //===Parallel MPI ======start======
    if( nMPIsize_ > 1) {
      double*  buff= (double *) malloc (sizeof(double)*nBins);
      MPI_Allreduce((double *) &nBin.data(), buff, nBins, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      for (i = 1; i <= nBins; i++){
        nBin(i)=buff[i-1];
      }
      free(buff);
      MPI_Allreduce((double *) &xBin.data(), buff, nBins, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      for (i = 1; i <= nBins; i++){
        xBin(i)=buff[i-1];
      }
      free(buff);
      MPI_Allreduce((double *) &yBin.data(), buff, nBins, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      for (i = 1; i <= nBins; i++){
        yBin(i)=buff[i-1];
      }
      free(buff);
    }
   //===Parallel MPI ======stop=======
}
