/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Parallel.cc
//
// AUTHOR
//    John Galambos
//    ORNL, jdg@ornl.gov
//
// CREATED
//    12/11/97
//
//  DESCRIPTION
//    Module descriptor file for the Parallel module. This module contains
//    Source for the Parallel run related info.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include <ctype.h>
#include "Parallel.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>

#if defined(USE_PVM)
#include "pvm3.h"
#endif

#define CODENAME "ORBIT"

using namespace std;
///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

/// An array of pvmhostinfo structs describing the virtual machine.

#if defined(USE_PVM)

struct  pvmhostinfo hostp[200];
pvmhostinfo *hostpp = (pvmhostinfo *) &hostp;
#endif

///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE Parallel
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Parallel::ctor
//
// DESCRIPTION
//    Initializes the various Parallel related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Parallel::ctor()
{

// set some initial values

    parallelRun = 0;
    nJobs = 0;
    iAmAChild = 0;
    spawnOnParent = 0;
    myParent = 0;
    myParallelID = 0;
    dataEncodeType = 1;// ==  PvmDataDefault
}
///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Parallel::dtor
//
// DESCRIPTION
//    Cleans up the Parallel module.
//
///////////////////////////////////////////////////////////////////////////

Void Parallel::dtor()
{
  
#if defined(USE_PVM)
  if(parallelRun) pvm_exit();
#endif

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Parallel::startParallelRun
//
// DESCRIPTION
//    Initializes the various Parallel related constants.
//
// PARAMETERS
//    filename - String name of input file to use
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Parallel::startParallelRun(const String &fileName)
{
#if !defined(USE_PVM)
   except(noPVM);
#else

  Integer i, info;

  parallelRun = 1;

  myParallelID = pvm_mytid();           // get tid
  if(myParallelID < 0) except(pvmNotRunning);
  myParent = pvm_parent();
  
//  info = pvm_setopt(PvmRoute, PvmRouteDirect); // this sometimes slows
                                                // things down on our cluster!

  if(myParent != PvmNoParent)
    {
      iAmAChild = 1; 
      return;
    }

    // OK I'm the parent, let's set it up

  if(nJobs > 0) except(alreadyParallel); // don't do multiple parallels

  int narch;

  info = pvm_config(&nHosts, &narch, &hostpp);

  if (info < 0) except(pvmdNoRespond);
  if(nHosts < 2) except(tooSmallVM);

  // Set up the TID array.

  TIDs.resize(nHosts);
  TIDs = 0;

  // spawn jobs on all hosts but master 
  // master is assumed to be number host number 1. 

  nJobs = 0;
  Integer iStart = 2;
  if(spawnOnParent) iStart = 1;
  Integer TID;

  char *iFileName[2];
  iFileName[0] = (char *) malloc(80);
  iFileName[1] = 0;

  for(i=0; i< fileName.length(); i++)
      iFileName[0][i] = fileName[i];
  iFileName[0][fileName.length()] = '\0';

  for(i= iStart; i<= nHosts; i++)
    {

      info = 
       pvm_spawn(CODENAME, &iFileName[0], 0, hostp[i-1].hi_name, 
              1, &TID);
       if(info == 0) except(pvmSpawnFailed2);
       TIDs(++nJobs) = TID;
      
    }

  childTID.resize(nJobs);
  for(i=1; i<= nJobs; i++) childTID(i) = TIDs(i);
  

/*
   nJobs =  pvm_spawn(CODENAME, &iFileName[0], 0, "", 
              nHosts, TID);
   for(i=1; i<= nJobs; i++) TIDs(i) = TID[i-1];
 */

  if(nJobs ==0)  except(pvmSpawnFailed);

#endif  // USE_PVM

}
