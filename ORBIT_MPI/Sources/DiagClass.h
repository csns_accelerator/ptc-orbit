///////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    DiagClass.h
//
// AUTHOR
//    John Galambos, Jeff Holmes
//    ORNL, jdg@ornl.gov
//
// CREATED
//    12/21/98
//
//  DESCRIPTION
//    Header file for the Diagnostic classes. This module 
//    contains class declarations for the Diagnostic classes.
//
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "StreamType.h"
#include "MapBase.h"
#include "TransMapHead.h"

//=== Parallel MPI stuff ==========start========
#include "mpi.h"
//=== Parallel MPI stuff ==========stop=========

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   DiagnosticBase 
//
// INHERITANCE RELATIONSHIPS
//    DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a base class for storing Diagnostic information. 
//
// PUBLIC MEMBERS
//
//  _mp - reference to the macro particle herd we want info on
//
//  Virtual routines:
//
//  _diagCalculator  - Routine that does the diagnostic calculation
//  _showDiagnostic  - Routine to show information to a stream
//                   - typically used for "human readable" output
//  _dumpDiagnostic  - Routine to dump data to a stream
//                   - typically used for creating file output for processing
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class DiagnosticBase : public Object {
  Declare_Standard_Members(DiagnosticBase, Object);
public:
    DiagnosticBase(MacroPart &mp): _mp(mp)
        {           
        }
         MacroPart &_mp;
         virtual Void _diagCalculator()=0;
         virtual Void _showDiagnostic(Ostream &sout)=0;
         virtual Void _dumpDiagnostic(Ostream &sout)=0;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  CanonicalCoord 
//
// INHERITANCE RELATIONSHIPS
//     CanonicalCoord -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the canonical coordinates 
//     of a herd. 
//
// PUBLIC MEMBERS
//    static Real Vector
//      _pxCanonical   The horizontal momenta of a herd member [rad]
//      _pyCanonical   The vertical momenta of a herd member [rad]   
//      _xCanonical    The horizontal position at particle energy [mm]
//      _yCanonical    The vertical position at particle energy [mm]
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class CanonicalCoord : public DiagnosticBase {
  Declare_Standard_Members(CanonicalCoord, DiagnosticBase);
public:
  CanonicalCoord(MacroPart &mp): DiagnosticBase(mp)
        {           
        }

        static Vector(Real) _pxCanonical, _pyCanonical, 
               _xCanonical, _yCanonical;
        virtual Void _diagCalculator();
        virtual Void _showDiagnostic(Ostream &sout) {};
	virtual Void _dumpDiagnostic(Ostream &sout);
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  SigmaMatrix
//
// INHERITANCE RELATIONSHIPS
//  SigmaMatrix -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//  None
//
// DESCRIPTION
//  Class for calculating and storing coordinate averages and
//  sigma matrices of a herd
//
// PUBLIC MEMBERS
//  static Vector(Real)
//    XAvg   - 6 vector of average coordinates in {m} and {radians}
//  static Matrix(Real)
//    SigMat - 6x6 sigma matrix in {m} and {radians}
//
// PROTECTED MEMBERS
//  None
//
// PRIVATE MEMBERS
//  None
//
///////////////////////////////////////////////////////////////////////////

class SigmaMatrix : public DiagnosticBase
{
  Declare_Standard_Members(SigmaMatrix, DiagnosticBase);

  public:
  SigmaMatrix(MacroPart &mp): DiagnosticBase(mp)
  {
  }
  
  static Vector(Real) _XAvg;
  static Matrix(Real) _SigMat;
  static Matrix(Real) _CoupMat;
  static        Real  _Couple;

  virtual Void _diagCalculator();
  virtual Void _showDiagnostic(Ostream &sout) {};
  virtual Void _dumpDiagnostic(Ostream &sout);
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  Emittance
//
// INHERITANCE RELATIONSHIPS
//     Emittance -> CanonicalCoord -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the Courant Schneider
//      values of a herd. These are stored in Shell accesible Diagnostic
//      module vectors:
//        Diagnostic::xEmit (pi-mm-mrad)
//        Diagnostic::yEmit (pi-mm-mrad)
//
// PUBLIC MEMBERS
//
//  _xAvg, _yAvg    - average positions of a herd (mm)
//  _pxAvg, _pyAvg  - average canonical momentum of a herd (mrad)
//  _xSig, _ySig    - average (xCanonical-_xAvg)^2 of a herd (mm^2)
//  _pxSig, _pySig  - average (pxCanonical - _pxAvg)^2 of a herd (mrad^2)
//  _xpxCor, _ypyCor- average (xCanonical - _xAvg)*(pxCanonical - _pxAvg)
//                    of a herd (mm-mrad)
//  
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class Emittance : public CanonicalCoord {
//  Declare_Standard_Members(Emittance, CanonicalCoord);
public:
 Emittance(MacroPart &mp): CanonicalCoord(mp)
        { 
        }
        virtual Void _diagCalculator();
        virtual Void _showDiagnostic(Ostream &sout);
        virtual Void _dumpDiagnostic(Ostream &sout);
 protected:

        Real _xAvg, _yAvg, _pxAvg, _pyAvg, _xSig, _ySig, 
             _pxSig, _pySig, _xpxCor, _ypyCor; 

};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  Emittanceco
//
// INHERITANCE RELATIONSHIPS
//     Emittanceco -> CanonicalCoord -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the emittances of a herd,
//    relative to closed orbit . These are stored in Shell accesible 
//    Diagnostic module vectors:
//        Diagnostic::xEmitco (pi-mm-mrad)
//        Diagnostic::yEmitco (pi-mm-mrad)
//
// PUBLIC MEMBERS
//  None.
//  
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

class Emittanceco : public CanonicalCoord {
//  Declare_Standard_Members(Emittanceco, CanonicalCoord);
public:
 Emittanceco(MacroPart &mp): CanonicalCoord(mp)
        { 
        }
        virtual Void _diagCalculator();
      
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  EmittanceSlice
//
// INHERITANCE RELATIONSHIPS
//     EmittanceSlice -> CanonicalCoord -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the Courant Schneider
//    values of a herd, for a particular longitudinal slice of the beam. 
//    These are stored in Shell accesible Diagnostic module vectors:
//        Diagnostic::xEmitSlice (pi-mm-mrad)
//        Diagnostic::yEmitSlice (pi-mm-mrad)
//
// PUBLIC MEMBERS
//
//  _xSliceAvg, _ySliceAvg    - average positions of a herd (mm)
//  _pxSliceAvg, _pySliceAvg  - average canonical momentum of a herd (mrad)
//  _xSliceSig, _ySliceSig    - average (xCanonical-_xAvg)^2 of a herd (mm^2)
//  _pxSliceSig, _pySliceSig  - average (pxCanonical - _pxAvg)^2 of a 
//                              herd (mrad^2)
//  _xpxSliceCor, _ypySliceCor- average (xCanonical - _xAvg)*
//                                      (pxCanonical - _pxAvg) 
//                                      of a herd (mm-mrad)
//  
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class EmittanceSlice : public CanonicalCoord {
//  Declare_Standard_Members(EmittanceSlice, CanonicalCoord);
public:
  EmittanceSlice(MacroPart &mp, const Real &phiMin,
		 const Real &phiMax): CanonicalCoord(mp),
    _phiMin(phiMin), _phiMax(phiMax)
        { 
        }
        virtual Void _diagCalculator();
        virtual Void _showDiagnostic(Ostream &sout);
        virtual Void _dumpDiagnostic(Ostream &sout);
 protected:

        Real _xSliceAvg, _ySliceAvg, _pxSliceAvg, _pySliceAvg, 
	  _xSliceSig, _ySliceSig, _pxSliceSig, _pySliceSig, _xpxSliceCor, 
	  _ypySliceCor; 
	const Real _phiMin, _phiMax;

};


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  LongEmittance
//
// INHERITANCE RELATIONSHIPS
//     LongEmittance -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the longitudinal
//      emittance values of a herd. 
//      These are stored in Shell accesible Diagnostic
//      module vectors:
//        Diagnostic::longEmit (eV-sec)
//
// PUBLIC MEMBERS
//
//  _phiAvg  - average angle  of a herd (rad)
//  _dEAvg   - average delta E (GeV)
//  _phiSig  - average (_phi-_phiAvg)^2 of a herd (rad^2)
//  _dESig   - average (_deltaE - _dEAvg)^2 of a herd (GeV^2)
//  _phiDE   - average (_phi- _phiAvg) * (_deltaE - _dEAvg)
//                    of a herd (GeV-Rad)
//  
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class LongEmittance : public DiagnosticBase {
//  Declare_Standard_Members(LongEmittance, DiagnosticBase);
public:
 LongEmittance(MacroPart &mp): DiagnosticBase(mp)
        { 
        }
        virtual Void _diagCalculator();
        virtual Void _showDiagnostic(Ostream &sout);
        virtual Void _dumpDiagnostic(Ostream &sout);
        Real _dESig;
 protected:

        Real _dEAvg, _phiSig, _phiDE, _phiAvg; 

};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  Actions
//
// INHERITANCE RELATIONSHIPS
//     Actions -> Emittance -> CanonicalCoord -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the action variables
//     of a herd. Results stored in Diagnostic module vectorsfor these:
//
//       Diagnostic::xAction   The horizontal action of a herd member [rad]
//       Diagnostic::yAction   The vertical action of a herd member [rad]   
//
// PUBLIC MEMBERS
//   none
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class Actions : public Emittance {
  Declare_Standard_Members(Actions, Emittance);
public:
  Actions(MacroPart &mp): Emittance(mp)
        {           
        }

        virtual Void _dumpDiagnostic(Ostream &sout);
        virtual Void _diagCalculator();
        virtual Void _showDiagnostic(Ostream &sout){};
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  StatLat
//
// INHERITANCE RELATIONSHIPS
//     StatLat -> Emittance -> CanonicalCoord -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the statistical 
//    lattice parameters of a herd.
//
// PUBLIC MEMBERS
//   Real
//      _betaX    The statistical average horizontal beta [m]
//      _betaY    The statistical average vertical beta [m]
//      _alphaX    The statistical average horizontal alpha
//      _alphaY    The statistical average vertical alpha
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class StatLat : public Emittance {
  Declare_Standard_Members(StatLat, Emittance);
public:
  StatLat(MacroPart &mp): Emittance(mp)
        {           
        }

        virtual Void _dumpDiagnostic(Ostream &sout);
        virtual Void _diagCalculator();
        virtual Void _showDiagnostic(Ostream &sout){};
        Real _betaX, _betaY, _alphaX, _alphaY;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   LongDist
//
// INHERITANCE RELATIONSHIPS
//   LongDist -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   Bins longitudinal beam density and dipole moment distributions
//
// PUBLIC MEMBERS
//
//   static Vector:
//     _PhiVal  - longitudinal coordinate
//     _lambda  - longitudinal density array {particles / m}
//     _xDipole - horizontal dipole moment {m * particles / m}
//     _xpDipole - horizontal prime dipole moment {particles / m}
//     _yDipole - vertical dipole moment {m * particles / m}
//     _yDipole - vertical prime dipole moment {particles / m}
//
//   const Integer:
//     _nBins   - number of longitudinal bins
//  
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class LongDist : public DiagnosticBase
{
  Declare_Standard_Members(LongDist, DiagnosticBase);
  public:
  LongDist(MacroPart &mp, const Integer &nBins):
           DiagnosticBase(mp), _nBins(nBins)
  {           
  }

    static Vector(Real) _PhiVal, _lambda,
                        _xDipole, _xpDipole,
                        _yDipole, _ypDipole;
    virtual Void _diagCalculator();
    virtual Void _showDiagnostic(Ostream &sout);
    virtual Void _dumpDiagnostic(Ostream &sout);
    const Integer _nBins;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  Moment 
//
// INHERITANCE RELATIONSHIPS
//     Moment -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the moments
//    of a herd. The moments are stored in matrices with the first index (i)
//    indicating the order of the horizontal moment and the second index (j)
//    indicating the order of the vertical moment. 
//    The moment order = the index value-1. E.g.
//    _momentXY(1,3) is the moment of (horizontal value) ^0 *
//        (vertical value)^2.
//    The normalized moments _momentXYNorm
//        = regular moments/[Sqrt(betaX)^(i-1) * Sqrt(betaY)^(j-1)]
//
// PUBLIC MEMBERS
//   static Real Matrix
//      _momentXY       The transverse Moment of a herd
//      _momentXYNorm   The transverse Moment of a herd normalized by 
//                      the ring beta
//   Integer
//      _order          The order to which moments are calculated
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class Moment : public DiagnosticBase {
  Declare_Standard_Members(Moment, DiagnosticBase);
public:
  Moment(MacroPart &mp, const Integer &ord): DiagnosticBase(mp), _order(ord)
        {           
        }

        static Matrix(Real) _momentXY, _momentXYNorm;
        virtual Void _diagCalculator();
        virtual Void _showDiagnostic(Ostream &sout);
        virtual Void _dumpDiagnostic(Ostream &sout);
        const Integer _order;
};


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    MomentSlice 
//
// INHERITANCE RELATIONSHIPS
//    MomentSlice -> DiagnosticBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for calculating and storing the moments
//    of a herd for a particular longitudinal slice of beam. The moments 
//    are stored in matrices with the first index (i)
//    indicating the order of the horizontal moment and the second index (j)
//    indicating the order of the vertical moment. 
//    The moment order = the index value-1. E.g.
//    _momentSliceXY(1,3) is the moment of (horizontal value) ^0 *
//        (vertical value)^2.
//    The normalized moments _momentSliceXYNorm
//        = regular moments/[Sqrt(betaX)^(i-1) * Sqrt(betaY)^(j-1)]
//
// PUBLIC MEMBERS
//   static Real Matrix
//      _momentSliceXY       The transverse Moment of a herd
//      _momentSliceXYNorm   The transverse Moment of a herd normalized by 
//                      the ring beta
//   Integer
//      _order          The order to which moments are calculated
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class MomentSlice : public DiagnosticBase {
  Declare_Standard_Members(MomentSlice, DiagnosticBase);
public:
  MomentSlice(MacroPart &mp, const Integer &ord, const Real &phiMin,
	      const Real &phiMax): DiagnosticBase(mp), _order(ord),
              _phiMin(phiMin), _phiMax(phiMax)
        {           
        }

        static Matrix(Real) _momentSliceXY, _momentSliceXYNorm;
        virtual Void _diagCalculator();
        virtual Void _showDiagnostic(Ostream &sout);
        virtual Void _dumpDiagnostic(Ostream &sout);
        const Integer _order;
	const Real _phiMin;
	const Real _phiMax;
};




///////////////////////////////////////////////////////////////////////////
//
// Node Classes:
//
// These classes implement the above diagnostic classes, in Node elements
//  suitable for inclusion in a regular manner within a Ring.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   DiagnosticNode 
//
// INHERITANCE RELATIONSHIPS
//    DiagnosticNode  -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a base class for storing Diagnostic Node information. 
//
// PUBLIC MEMBERS
//
//  Void
//    _diagNodeCalculator - Does the diagnostic node operations.
//    _showDiagNode       - Prints a small blerb about this diag node. 
//  String
//  _fName   - name of a file to dump to.
//  Integer
//  _activeFlag = Flag indicating whether this diagnostic node is active.
//                If == 0, the _nodeCalculator should do nothing
//  
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class DiagnosticNode : public Node {
  Declare_Standard_Members(DiagnosticNode, Node);
public:
    DiagnosticNode(const String &n, const Integer &order, const String &fn) :
            Node(n, 0., order)
    {  
      _fName = fn;
      _activeFlag = 0;  
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp) {};
    virtual Void _nodeCalculator(MacroPart &mp)
      {if(_activeFlag) _diagNodeCalculator(mp);}
    virtual Void _diagNodeCalculator(MacroPart &mp)=0;
    virtual Void _showDiagNode(Ostream &sout)=0;
    String _fName;
    Integer _activeFlag;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   FracTuneNode
//
// INHERITANCE RELATIONSHIPS
//   FracTuneNode -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for providing fractional tunes calculated at
//   any desired lattice location.
//
// PUBLIC MEMBERS
//
//   Vector(Real) _XPhase_old, _YPhase_old
//   Vector(Real) _XPhase,     _YPhase
//   Vector(Real) _XFrac_Tune, _YFrac_Tune
//  
// PROTECTED MEMBERS
//   None.
// PRIVATE MEMBERS
//   None.
//
///////////////////////////////////////////////////////////////////////////

class FracTuneNode : public DiagnosticNode
{
  Declare_Standard_Members(FracTuneNode, DiagnosticNode);
public:
  FracTuneNode(const String &n, const Integer &order,
               const String &fn) :
               DiagnosticNode(n, order, fn)
  {    
  }
  virtual Void nameOut(String &wname) { wname = _name; }
  virtual Void _diagNodeCalculator(MacroPart &mp);
  virtual Void _showDiagNode(Ostream &sout);
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   LongDistNode
//
// INHERITANCE RELATIONSHIPS
//   LongDistNode -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   Longitudinal beam density and dipole moment distributions
//
// PUBLIC MEMBERS
//
//   Integer:
//     _nBins - number of longitudinal bins
//  
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class LongDistNode : public DiagnosticNode
{
  Declare_Standard_Members(LongDistNode, DiagnosticNode);
  public:
    LongDistNode(const String &n, const Integer &order, const Integer &nBins,
                 const String &fn) :
                 DiagnosticNode(n, order, fn)
    {
      _nBins = nBins;
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);
    Integer _nBins;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   MomentNode 
//
// INHERITANCE RELATIONSHIPS
//    MomentNode  -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for dumping beam moment diagnostic information. 
//
// PUBLIC MEMBERS
//
//  Integer:
//  _momOrder - order of Moment to calculate
//  
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class MomentNode : public DiagnosticNode {
  Declare_Standard_Members(MomentNode, DiagnosticNode);
public:
    MomentNode(const String &n, const Integer &order, const Integer &mOrder,
                 const String &fn) :
            DiagnosticNode(n, order, fn)
    {    
      _momOrder = mOrder;
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);
    Integer _momOrder;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    MomentSliceNode
//
// INHERITANCE RELATIONSHIPS
//    MomentSliceNode -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for dumping beam moment diagnostic information of
//    a longitudinal beam slice.
//
// PUBLIC MEMBERS
//
//    Integer:
//    _momOrder - order of Moment to calculate
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class MomentSliceNode : public DiagnosticNode {
    Declare_Standard_Members(MomentSliceNode, DiagnosticNode);
public:
    MomentSliceNode(const String &n, const Integer &order,
		    const Integer &mOrder, const String &fn,
		    const Real &phiMin, const Real &phiMax) :
    DiagnosticNode(n, order, fn), _phiMin(phiMin), _phiMax(phiMax)
    {
      _momOrder = mOrder;
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);
    Integer _momOrder;
    Real _phiMin;
    Real _phiMax;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   BPMNode
//
// INHERITANCE RELATIONSHIPS
//   BPMNode -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for dumping beam moment diagnostic information of
//   a longitudinal beam slice.
//
// PUBLIC MEMBERS
//
//   Integer:
//   _momOrder - order of Moment to calculate
//
//   Real:
//   _xAvg, _yAvg
//  
// PROTECTED MEMBERS
//   None
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class BPMNode : public DiagnosticNode {
    Declare_Standard_Members(MomentSliceNode, DiagnosticNode);
public:
    BPMNode(const String &n, const Integer &order,
	    const Integer &mOrder, const String &fn,
	    const Real &phiMin, const Real &phiMax) :
    DiagnosticNode(n, order, fn), _phiMin(phiMin), _phiMax(phiMax)
    {
      _momOrder = mOrder;
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);
    Integer _momOrder;
    Real _phiMin;
    Real _phiMax;
    Real _xAvg;
    Real _yAvg;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   QuadBPMNode
//
// INHERITANCE RELATIONSHIPS
//   QuadBPMNode -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for dumping beam quadrupole moment
//   diagnostic information of a longitudinal beam slice.
//
// PUBLIC MEMBERS
//
//   Integer:
//   _momOrder - order of Moment to calculate
//
//   Real:
//   _xSq, _ySq
//  
// PROTECTED MEMBERS
//   None
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class QuadBPMNode : public DiagnosticNode {
    Declare_Standard_Members(MomentSliceNode, DiagnosticNode);
public:
    QuadBPMNode(const String &n, const Integer &order,
                const Integer &mOrder, const String &fn,
                const Real &phiMin, const Real &phiMax) :
    DiagnosticNode(n, order, fn), _phiMin(phiMin), _phiMax(phiMax)
    {
      _momOrder = mOrder;
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);
    Integer _momOrder;
    Real _phiMin;
    Real _phiMax;
    Real _xSq;
    Real _ySq;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   BPMFreqNode
//
// INHERITANCE RELATIONSHIPS
//   BPMFreqNode -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//
// PUBLIC MEMBERS
//
//   Integer _nBins     Number of bins for distribution
//   Integer _freq      Central Fourier mode number to monitor
//   Integer _band      Range of Fourier mode numbers to monitor
//   Real _amplitude    amplitude of mode number in distribution
//
// PROTECTED MEMBERS
//   None
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class BPMFreqNode : public DiagnosticNode {
  Declare_Standard_Members(BPMFreqNode, DiagnosticBase);
public:
    BPMFreqNode(const String &n, const Integer &order,
                const Real &phiMin, const Real &phiMax,
                const Integer &nBins,
                const Integer &freq0, const Integer &band) :
      DiagnosticNode(n, order, "null"),
                _phiMin(phiMin), _phiMax(phiMax),
      _nBins(nBins), _freq0(freq0), _band(band)
    {
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);
    Real _phiMin;
    Real _phiMax;
    Integer _nBins;
    Integer _freq0;
    Integer _band;
    Vector(Integer) _freq;
    Vector(Real) _amplitudeN;
    Real _momentX;
    Real _momentY;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   StatLatNode 
//
// INHERITANCE RELATIONSHIPS
//    StatLatNode  -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for dumping statistical lattice diagnostic information. 
//
// PUBLIC MEMBERS
//
//  Integer:
//  
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class StatLatNode : public DiagnosticNode {
  Declare_Standard_Members(StatLatNode, DiagnosticNode);
public:
    StatLatNode(const String &n, const Integer &order, const String &fn) :
            DiagnosticNode(n, order, fn)
    {    
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//  
//   PMTNode  (Poincare-Moment-Tracking-Node)
//
// INHERITANCE RELATIONSHIPS
//     PMTNode -> DiagnosticNode -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for monitoring the oscillations of a presribed 
//     moment of the "core" (assumed 
//     to be the mainHerd here), and dumping the coordidates of another herd
//     (with member _feelsHerd == 1) when the core completes an oscillation.
//    To work right, two herds must be tracked, first the main herd, then 
//     the alternate herd, each herd turned one turn at a time.
//
//   Presently the oscillation bookeeping is limited to moments less
//     than order 10.
//
// PUBLIC MEMBERS
//     
//  _hIndex  - The index of the horizontal moment to use for tracking,
//  _vIndex  - The index of the vertictal moment to use for tracking,
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class PMTNode : public DiagnosticNode {
  Declare_Standard_Members(PMTNode, DiagnosticNode);
public:
    PMTNode(const String &n, const Integer &order, const String &fn,
            const Integer &hi, const Integer &vi) :
            DiagnosticNode(n, order, fn), _hIndex(hi), _vIndex(vi)
        {          
        }
        
        Integer _hIndex, _vIndex;
    
    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);

};


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   LongMountainNode 
//
// INHERITANCE RELATIONSHIPS
//   LongMountainNode  -> DiagnosticNode -> Node -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for dumping longitudinal histogram information
//    suitable for creating  mountain ranges.
//
// PUBLIC MEMBERS
//
//  Integer:
//  _nBins - Number of bins to use per mountain.
//  Real Vector:
//  _xBin = x coordinates of the bins (rad)
//  _yBin = Count for the bin
//  
//  
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class LongMountainNode : public DiagnosticNode {
  Declare_Standard_Members(LongMountainNode, DiagnosticNode);
public:
    LongMountainNode(const String &n, const Integer &order, const Integer &nb,
                 const String &fn);

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _diagNodeCalculator(MacroPart &mp);
    virtual Void _showDiagNode(Ostream &sout);
    Integer _nBins;
    Vector(Real) _xBin, _yBin;
};



























