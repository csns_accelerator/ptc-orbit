/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Parallel.mod
//
// AUTHOR
//    John Galambos,  ORNL, jdg@ornl.gov
//
// CREATED
//    4/8/99
//
//  DESCRIPTION
//    Module descriptor file for the Parallel module. This module contains
//    information about  parallel processing
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Parallel - "Parallel Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts;
 Errors
    noPVM           - "You can't do this without building the code with PVM",
    alreadyParallel - "I'm already doing parallel claculations!",
    pvmdNoRespond   - "The PVM daemon is not responding."
                    - "Check your virtual machine.",
    pvmSpawnFailed  - "Sorry, I couldn't spawn processes on any of the hosts.",
    pvmNotRunning   - "It looks like PVM is not running",
    tooSmallVM      - "Only a master is running on the VM. Get more hosts.";
  Warnings
    pvmSpawnFailed2 - "Sorry, I couldn't spawn processes on one of the hosts.";

public:

  Void ctor() 
                  - "Constructor for the Parallel  module.";
  Void dtor() 
                  - "Shuts down message passing apparatus";

  Integer 
    dataEncodeType - "Switch for data encoding scheme",
    iAmAChild      - "Flag that's true is process is a child",
    myParallelID   - "Job ID number from parallel demeon",
    myParent       - "Parent ID number",
    nJobs          - "Number of jobs spawned",
    nHosts         - "The number of hosts in the virtual machine.",
    parallelRun    - "Flag indicating a parallel run, set automatically",
    spawnOnParent  - "Flag to also spawn a job on parent host"
                   - "nominally false (==0)";
    
          
  IntegerVector
	  TIDs
                    - "The task ID for each host.",
          childTID  - "Task ID of children workers only";

  Void startParallelRun(const String &fileName) 
                  - "Starts up the parallel machine and spawns jobs";
}
