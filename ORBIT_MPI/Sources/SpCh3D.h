/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   SpCh3D.h
//
// AUTHOR
//   Slava Danilov, Jeff Holmes, John Galambos
//   ORNL, vux@ornl.gov
//
// CREATED
//   7/13/2001
//
// DESCRIPTION
//   Header file for the 3D space charge class. 
//   Separated from SpaceCharge3D.cc to use elsewhere.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(__SpCh3D__)
#define __SpCh3D__

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "RealMat.h"
#include "ComplexMat.h"
#include "SCLTypes.h"
#include "RealArr3.h"
#include <fftw.h>
#include <rfftw.h>

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   SpaceCharge3D
//
// INHERITANCE RELATIONSHIPS
//   SpaceCharge3D  -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a base class for transverse  space charge implementations.
//
// PUBLIC MEMBERS
//   _nMacrosMin  Minimum number of macro particles before using this node.
//   _eps         Smoothing parameter for force calculation, fraction of 
//                the bin size, i.e. _eps = 1 => smooth over entire bin
//                If _eps<0, use |_eps| as absoulte smoothing length [mm]
// PIC stuff:
//   _nXBins      Reference to the number of horizontal bins.
//   _nYBins      Reference to the number of vertical bins.
//   _nPhiBins      Reference to the number of longitudinal bins.
//   _lkick       Longitudinal length over which kick is applied (m)
//   _xGrid       The horizontal grid values relative to the closed orbit (mm)
//   _yGrid       The vertical grid values relative to the closed orbit (mm)
//   _potentialSC The space charge potential at the grid point 
//   _lambda      The local line charge coefficient
//
// PROTECTED MEMBERS
//   None
// PRIVATE MEMBERS
//   None.
//
///////////////////////////////////////////////////////////////////////////

class SpCh3D : public Node {
  Declare_Standard_Members(SpCh3D, Node);

  public:

  SpCh3D(const String &n, const Integer &order, 
         Integer &nXBins, Integer &nYBins, Integer &nPhiBins, Real &length,
         const Integer &nMacrosMin, Real &eps) : Node(n, 0., order),
         _nXBins(nXBins), _nYBins(nYBins), _nPhiBins(nPhiBins),
         _lkick(length), _nMacrosMin(nMacrosMin), _eps(eps)
  {
  }

  ~SpCh3D()
  {     
  }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;

    Integer _nXBins;
    Integer _nYBins;
    Integer _nPhiBins;
    Real _lkick;
    Real _eps;
    const Integer _nMacrosMin;
    Real _lambda;
    static Array3(Real) _potentialSC, _rho3D;
    static Vector(Real) _xGrid, _yGrid, _phiGrid;
    static Real _xGridMin, _xGridMax, _yGridMin, _yGridMax, _phiGridMin, _phiGridMax;
    static Matrix(Real) _phisc, _fscx, _fscy;
    Real _resid;

    protected:

    Real _dx, _dy, _dphi;  
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   SpCh3D_BF
//
// INHERITANCE RELATIONSHIPS
//   SpCh3D_BF -> SpCh3D -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for storing 3D kick information,
//   done with the brute force implementation.
//    
//    
//
// PUBLIC MEMBERS
//   None
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None.
//
///////////////////////////////////////////////////////////////////////////

class SpCh3D_BF: public SpCh3D {
  Declare_Standard_Members(SpCh3D_BF, SpCh3D);

   
   
  public:

  SpCh3D_BF(const String &n, const Integer &order, 
            Integer &nXBins, Integer &nYBins, Integer &nPhiBins, Real &length,
            const Integer &nMacrosMin, Real &eps) :
            SpCh3D(n, order, nXBins, nYBins, nPhiBins, length, nMacrosMin, eps)

  {
    _xGrid.resize(_nXBins+1);
    _yGrid.resize(_nYBins+1);
    _phiGrid.resize(_nPhiBins+1);
    _potentialSC.resize(_nXBins+3, _nYBins+3, _nPhiBins+1);
    _rho3D.resize(_nXBins+1, _nYBins+1, _nPhiBins+1);
  }

  ~SpCh3D_BF()
  {
  }

  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _fullBin(MacroPart &mp);  

  private:

};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   SpCh3D_FFT
//
// INHERITANCE RELATIONSHIPS
//   SpCh3D_FFT -> SpCh3D -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None.
//
// DESCRIPTION
//   This is a class for 3D space charge calculation with beam pipe and
//   conducting wall boundary condition.
//   It bins the particles in cells using a second order 27 point
//   scheme, uses a 2D FFT implementaion on longitudinal slices to 
//   calculate space charge potential, and a least squares solution 
//   to satisfy the beam pipe conducting wall boundary condition.
//
///////////////////////////////////////////////////////////////////////////

class SpCh3D_FFT : public SpCh3D
{
  Declare_Standard_Members(SpCh3D_FFT, SpCh3D);

  public:

  SpCh3D_FFT(const String &n, const Integer &order,
             Integer &nXBins, Integer &nYBins, Integer &nPhiBins,
             Real &eps, Real &length,
             const String &BPShape, Real &BP1,
             Real &BP2, Real &BP3, Real &BP4,
             Integer &BPPoints, Integer &BPModes,
             Real &Gridfact,
             const Integer &nMacrosMin);

  ~SpCh3D_FFT();

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
    Void _fullBin(MacroPart &mp);  
    Void _GridExtent(MacroPart &mp);
    Void _LSQBP();
    String _BPShape;
    Real _BPr, _BPa, _BPb, _BPxMin, _BPxMax, _BPyMin, _BPyMax;
    Integer _BPPoints, _BPModes;
    Real _Gridfact;
    Vector(Real) _BPx, _BPy, _BPPhi;
    Matrix(Real) _BPPhiH;
    Vector(Real) _BPH;
    Real _BPrnorm, _BPResid;
    Integer _iXmin, _iXmax, _iYmin, _iYmax;
 
 private:
    static rfftwnd_plan _planForward, _planBackward;
    static FFTW_REAL * _in;
    static FFTW_COMPLEX * _out1;
    static FFTW_COMPLEX * _out2;
    static FFTW_COMPLEX * _out;
    static Matrix(Real) _GreensF;
    static Matrix(Real) _rho;
    static Integer _initialized;

    //inserted by shishlo =====START=====
    static Real _dx_static;
    static Real _dy_static;
    //inserted by shishlo =====STOP======


    //debug ===shishlo === start ==
    static Integer _constructor_counter;
    //debug ===shishlo === stop ==

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif   // __SpCh3D__
