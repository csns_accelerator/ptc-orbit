/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   LSpaceCharge.mod
//
// AUTHOR
//   John Galambos, ORNL, jdg@ornl.gov
//   A. Luccio, BNL
//
// CREATED
//   12/17/97
//
// DESCRIPTION
//   Module descriptor file for the LSpaceCharge module. This module contains
//   information about longitudinal impedance, including space charge.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module LSpaceCharge - "LSpaceCharge Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, Particles, Ring, Injection;

Errors
  badLongSC    - "Sorry, you asked for a non existent Long space charge",
  badImpedSize - "The size of the impedance vector you supplied is too small",
  badFFTLSC    - "This routine only applies to FFTLongSC's",
  tooManyFFTLongs- "Can only use one FFT Long. Space charge node now";

public:

  Void ctor()
                - "Constructor for the Long. Space Charge module."
                - "Initializes build values.";

  Integer
    nLongSCs    - "Number of Longitudinal Space Charge Nodes in the Ring",
    nFFTLongs   - "Number of FFT Longitudinal Space Charge Nodes in the Ring",
    nLongBins   - "Number of longitudinal bins to use";

  Void addFFTLSpaceCharge(const String &n, const Integer &o,
                          ComplexVector &Z, Real &b_a, Integer &useAvg,
                          Integer &nMacrosMin, Integer &useSpaceCharge)
                - "Routine to add an FFT Longitudinal space charge node";
  Void addFreqDepLimp(const String &name, const Integer &order,
                      Integer &nTable,
                      RealVector &fTable, ComplexVector &zTable,
                      Real &b_a, Integer &useAvg,
                      Integer &nMacrosMin, Integer &useSpaceCharge)
                - "Routine to add an FFT TD longitudinal impedance node";
  Void addFreqBetDepLimp(const String &name, const Integer &order,
                         Integer &nbTable, RealVector &bTable,
                         Integer &nfTable, RealVector &fTable,
                         ComplexMatrix &zTable,
                         Real &b_a, Integer &useAvg,
                         Integer &nMacrosMin, Integer &useSpaceCharge)
                - "Routine to add an FFT TD longitudinal impedance node";

  Void showLSpaceCharge(const Integer &n, Ostream &os)
                - "Routine to show the Longitudinal space charge info";
  Real dumpKick(const Integer &n, Real &angle)
                - "Routine to dump kick of lsc node n to at an angle";
  Void dumpPhiCount(const Integer &n, RealVector &p)
                - "Routine to dump phi Bin  information to vector p";
  Void dumpFFTStuff(const Integer &n, RealVector &p1, RealVector &p2)
                - "Routine to dump phi Bin  information to vector p";
  Void dumpZImpedStuff(const Integer &n, RealVector &p1, RealVector &p2)
                - "Routine to dump Z and chi information to vectors p1, p2";
  Void dump_LImped(const Integer &n, Ostream &os)
                - "Routine to dump impedance to output stream";
}
