 /////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    ThinLens.mod
//
// AUTHOR
//    John Galambos,  ORNL, jdg@ornl.gov
//    Sarah Cousineau, Indiana University, scousine@sns.gov
//    Jeff Holmes, ORNL, jzh@ornl.gov
//
// CREATED
//    11/10/98
//
//  DESCRIPTION
//    Module descriptor file for the ThinLens module. 
//    This module contains information about thin lens kickers
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module ThinLens - "ThinLens Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, Particles, Ring;


public:

  Void ctor() 
                  - "Constructor for the ThinLens  module."
                  - "Initializes build values.";

  Integer
   nThinLens - "Number of thin Lens in the ring",
   nBIGKickLens - "Number of BIG kickers in the ring";
	
  Void addIntegrableLens2D(const String &n, const Integer &o,

          const Real &Rad,  const Real &Curr)
                  - "Routine to add an IntegrableLens node ";
  Void addThinLens(const String &name, const Integer &o,
         const Integer &n, const Real &kl, const Integer &skew)
                  - "Routine to add an thin multipole lens node ";
  Void showThinLens(Ostream &os) 
                  - "Routine to print Thin Lens setup to a stream";
  Void addBIGKickLens(const String &name, const Integer &o, 
	const Integer &direction, RealVector &kl, const Integer &size, 
	const Real &dPhi)
		  - "Routine to add a BIG kicker";
  Void addInflateLens(const String &name, const Integer &o,
	              const Real &factor)
		  - "Routine to add a emittance inflator";


}

