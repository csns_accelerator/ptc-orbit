#if !defined(__FNALTMs__)
#define __FNALTMs__

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include "HerdCoords.h"
#include "NodeDat.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    FNALTMs
//
// INHERITANCE RELATIONSHIPS
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for setting up and using 
//    FNAL Beamline transport libraries in ORBIT.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

class FNALTMs
{
  public:
  FNALTMs(char* MADFile, char* MADLine, const int &maporder,
          const double &E0, const double &ETotal,
          const double &Charge);
  ~FNALTMs(){};

  void getLattice(int &nElements, double &gammaTrans,
                  NodeDat FNode[]);
  void stepNodeProtons(int &eltIndex, HerdCoords * Coords);

  char* _MADFile;
  char* _MADLine;
  int _maporder;
  double _E0, _ETotal, _Charge, _Gamma, _Beta;
};

#endif   // __FNALTMS__
