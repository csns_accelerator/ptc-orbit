//# Library            : ORBIT_MPI
//# File               : Boundary.cc
//# Original code      : Jeff Holmes, Slava Danilov, John Galambos 

#include "Boundary.h"
#include <iostream>

using namespace std;

//xBins, yBins - grid size [xBins]X[yBins]
//xSize, ySize - geometry parameters [m]
//BPPoints - number of points on the bounary
//BPShape - shape of the boundary  1 - Circle 2 - Ellipse 3 - Rectangle
//BPModes - number of functions in the LSQ method ( > 10 )


// Constructor for ORBIT_Boundary class

ORBIT_Boundary::ORBIT_Boundary(int xBins   , int yBins   , 
                               double xSize, double ySize,
                               int BPPoints, int BPShape, 
                               int BPModes , double eps )
{

  int i;

  eps_ = eps;
  xSize_ = xSize;
  ySize_ = ySize;
  


  //====== Grid parameters and Green function==============

  xBins_ = xBins;
  yBins_ = yBins;

  greensF_ = new double*[xBins_];
  for( i = 0; i < xBins_ ; i++) {
    greensF_[i] =  new double [yBins_];
  }

  in_  = (FFTW_REAL *)    new char[ xBins_ * yBins_ * sizeof(FFTW_REAL)];
  out_ = (FFTW_COMPLEX *) new char[ xBins_ * (yBins_/2+1)* sizeof(FFTW_COMPLEX)];

  planForward_ = rfftw2d_create_plan(xBins_ , yBins_ , FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE);

  //=================================================================
  //================   Border parameters      =======================
  //=================================================================
  if( BPShape != 1 && BPShape != 2 && BPShape != 3){
         cerr << "ORBIT_Boundary::ORBIT_Boundary - CONSTRUCTOR \n" 
         << "Define the shape! \n" 
         << "Stop. \n";
        _finalize_MPI();
         exit(1);
  }

  BPPoints_ = 4* int(BPPoints/4);
  BPShape_ = BPShape;
    
  BPx_ = new double[BPPoints];
  BPy_ = new double[BPPoints];
  theta_ = new double[BPPoints];
  BPphi_ = new double[BPPoints];

  //BPShape_ = 1 - this is the circle =======START=============
  if (BPShape_ == 1){
    if(xSize != ySize){
         cerr << "ORBIT_Boundary::ORBIT_Boundary - CONSTRUCTOR \n" 
         << "It is not a circle :  xSize != ySize \n"
	 << "xSize and  ySize = " << xSize <<" "<< ySize <<"\n" 
         << "Stop. \n";
         _finalize_MPI();
         exit(1);
    }

    R_cirle_ = xSize/2.0;
    BPrnorm_ = R_cirle_;

    double dtheta =2.0*Consts::pi/BPPoints_;
    for (i = 0; i < BPPoints_; i++){
      theta_[i] = i*dtheta;
      BPx_[i] = R_cirle_*cos(theta_[i]);
      BPy_[i] = R_cirle_*sin(theta_[i]);
    }
  }
  //BPShape_ = 1 - this is the circle =======STOP==============

  //BPShape_ = 2 - this is the Ellipse ======START=============
  if (BPShape_ == 2){
   double ds,denom,resid,th,dtheta;
   int BPP04;
   BPP04 = BPPoints_/4;
   BPa_ = xSize/2.0;
   BPb_ = ySize/2.0;
   BPrnorm_ = sqrt(BPa_ * BPb_);
   ds = 2 * Consts::pi * BPrnorm_ / BPPoints_;
   resid = 1.0;
   while (resid > 1.0e-08 || resid < -1.0e-08){
     denom = BPb_;
     dtheta = ds / denom;
     th = dtheta / 2.;
     theta_[0] = 0.;
      for (i = 0; i < BPP04 ; i++){
        denom = BPb_*BPb_ + (BPa_*BPa_-BPb_*BPb_)*sin(th)*sin(th);
        denom = sqrt(denom);
        dtheta = ds / denom;
        theta_[i+1] = theta_[i] + dtheta;
        th += dtheta;      
      }
    resid = theta_[BPP04] - Consts::pi/ 2.;
    ds *= Consts::pi / (2.*theta_[BPP04]);
   }

   for (i = 0; i < BPP04; i++){
    theta_[2*BPP04-i] = Consts::pi - theta_[i];
   }

   for (i = 0; i < 2*BPP04; i++){
    theta_[2* BPP04 + i] = Consts::pi + theta_[i];
   }
               
   for (i = 0; i < BPPoints_; i++){
     BPx_[i]   = BPa_ * cos(theta_[i]);
     BPy_[i]   = BPb_ * sin(theta_[i]);
     theta_[i] = atan2(BPy_[i],BPx_[i]);
   }
  }
  //BPShape_ = 2 - this is the Ellipse ======STOP==============

  //BPShape_ = 3 - this is the Rectangle ====START=============
  if (BPShape_ == 3){
    double x_min,x_max,y_min,y_max,dx,dy;
    int BPPO4;
    BPx_length_ = xSize;
    BPy_width_  = ySize;
    BPPO4 = BPPoints_/4;
    x_max = xSize/2.0;
    x_min = - x_max;   
    y_max = ySize/2.0;
    y_min = - y_max; 
    dx = (x_max - x_min)/BPPO4;
    dy = (y_max - y_min)/BPPO4;
    BPrnorm_ = sqrt((xSize*xSize + ySize*ySize)/4.);
     for (i = 0; i < BPPO4; i++){
       BPx_[i] = x_max;
       BPy_[i] = y_min + i*dy;
       theta_[i] = atan2(BPy_[i],BPx_[i]);
       
       BPx_[BPPO4 + i] = x_max - i*dx;
       BPy_[BPPO4 + i] = y_max;
       theta_[BPPO4 + i] = atan2(BPy_[BPPO4 + i],BPx_[BPPO4 + i]);

       BPx_[2*BPPO4 + i] = x_min;
       BPy_[2*BPPO4 + i] = y_max - i*dy;
       theta_[2*BPPO4 + i] = atan2(BPy_[2*BPPO4 + i],BPx_[2*BPPO4 + i]);

       BPx_[3*BPPO4 + i] = x_min + i*dx;
       BPy_[3*BPPO4 + i] = y_min;
       theta_[3*BPPO4 + i] = atan2(BPy_[3*BPPO4 + i],BPx_[3*BPPO4 + i]);
     }
  }

  //BPShape_ = 3 - this is the Rectangle ====STOP==============

  double xBrMin,xBrMax,yBrMin,yBrMax;
  xBrMin = 0.0; xBrMax = 0.0; yBrMin = 0.0; yBrMax = 0.0;
  for (i = 0; i < BPPoints_; i++){ 
    if( xBrMin > BPx_[i] ) xBrMin = BPx_[i];
    if( xBrMax < BPx_[i] ) xBrMax = BPx_[i];
    if( yBrMin > BPy_[i] ) yBrMin = BPy_[i];
    if( yBrMax < BPy_[i] ) yBrMax = BPy_[i];
  }

  //extend factor for the grid to use FFT-method for a non-periodic SC-distribution
  double GridFactor = 2.0;

  xGridMin_ = xBrMin * GridFactor;
  xGridMax_ = xBrMax * GridFactor;
  yGridMin_ = yBrMin * GridFactor;
  yGridMax_ = yBrMax * GridFactor;

  dx_ = (xGridMax_ - xGridMin_)/xBins_;
  dy_ = (yGridMax_ - yGridMin_)/yBins_;

  xGrid_ = new double[xBins_+1];
  yGrid_ = new double[yBins_+1];

  for (i = 0; i<= xBins_; i++) {xGrid_[i] = xGridMin_ + i * dx_;}
  for (i = 0; i<= yBins_; i++) {yGrid_[i] = yGridMin_ + i * dy_;}

  //define the LSQM fuction and matrix 

  BPModes_ = BPModes;

  if( 2*BPModes_+1 >= BPPoints_ ){
    cerr << "ORBIT_Boundary::ORBIT_Boundary - CONSTRUCTOR \n" 
         << " Boundary class will not work because\n"
	 << " 2*BPModes_+1 = " << 2*BPModes_+1 <<"\n" 
         << " BPPoints_    = " << BPPoints_    <<"\n" 
         << " Number of points has to be increased.\n"
         << "Stop. \n";
         _finalize_MPI();
         exit(1);
  }


  func_vector_ = new double[2*BPModes_+1];
  cof_vector_  = new double[2*BPModes_+1];

  tmp_matrix_ =new double*[2*BPModes_+1];
  for( i = 0; i < (2*BPModes_+1); i++) {
    tmp_matrix_[i] =  new double [2*BPModes_+1];
  } 

  LSQ_matrix_ =new double*[2*BPModes_+1];
  for( i = 0; i < (2*BPModes_+1); i++) {
    LSQ_matrix_[i] =  new double [BPPoints_];
  }

  //array with the interpolation coefficients for boundary points
  //there 9-points scheme is used
  W_coeff_ = new double* [BPPoints_];
  for( i = 0; i < BPPoints_; i++) {  
   W_coeff_[i] = new double [9];
  }

  //indexes of the boundary points
  iBPx_ = new int [BPPoints_];
  iBPy_ = new int [BPPoints_];
  
  //define FFT of the Green fuction
  _defineGreenF();

  //define tmp_matrix_
  _defineLSQMatrix();

  //define the min and max indexes of XY-plane's grid to operate with potential
  int iX, iY;
  ixMinB_ = xBins_+1;
  iyMinB_ = yBins_+1;
  ixMaxB_ = 0;
  iyMaxB_ = 0;  
    for (i = 0; i < BPPoints_; i++){
      iX = int ( (BPx_[i] - xGridMin_)/dx_ + 0.5 );
      iY = int ( (BPy_[i] - yGridMin_)/dy_ + 0.5 );
      iBPx_[i] = iX;
      iBPy_[i] = iY;
      if(ixMinB_ > iX ){ ixMinB_ = iX;}
      if(ixMaxB_ < iX ){ ixMaxB_ = iX;}
      if(iyMinB_ > iY ){ iyMinB_ = iY;}
      if(iyMaxB_ < iY ){ iyMaxB_ = iY;}
    }
    ixMinB_ = ixMinB_ -1;
    ixMaxB_ = ixMaxB_ +1;
    iyMinB_ = iyMinB_ -1;
    iyMaxB_ = iyMaxB_ +1;
    if(ixMinB_ < 0 ) {ixMinB_ = 0;}
    if(ixMaxB_ > xBins_ ) {ixMaxB_ = xBins_;}
    if(iyMinB_ < 0 ) {iyMinB_ = 0;}
    if(iyMaxB_ > yBins_ ) {iyMaxB_ = yBins_;}

    //Sets array with the interpolation coefficients for boundary points
      _setInterpolationCoeff();

}

// Destructor
ORBIT_Boundary::~ORBIT_Boundary()
{
   int i;

  //delete Green function and FFT input and output arrays

  for( i = 0; i < xBins_ ; i++) {
    delete [] greensF_[i];
  }   
  delete [] greensF_;

  delete [] in_;
  delete [] out_;
  rfftwnd_destroy_plan(planForward_);


  //delete arrays describing boundary
  delete [] BPx_ ;
  delete [] BPy_ ;
  //indexes of the boundary points
  delete [] iBPx_;
  delete [] iBPy_;

  delete [] theta_ ;
  delete [] BPphi_;


  //delete grid
  delete [] xGrid_;
  delete [] yGrid_;


  //delete LSQM related arrays
  delete [] func_vector_;
  delete [] cof_vector_;

  for( i = 0; i < (2*BPModes_+1); i++) {
    delete [] tmp_matrix_[i];
  }
  delete [] tmp_matrix_;

  for( i = 0; i < (2*BPModes_+1); i++) {
    delete [] LSQ_matrix_[i];
  }
  delete [] LSQ_matrix_;


  //array with the interpolation coefficients for boundary points
  for( i = 0; i < BPPoints_; i++) {  
   delete [] W_coeff_[i];
  }
  delete [] W_coeff_;  

  //cerr << "Destructor ORBIT_Boundary was done.   !!!! \n";
}


// Returns the pointers to the FFT array of the Green Function
FFTW_COMPLEX* ORBIT_Boundary::getOutFFTGreenF()
{
  return out_;
}

// Defines the FFT of the Green Function
void ORBIT_Boundary::_defineGreenF()
{

  double epsSq, rTransY, rTransX, rTot2;
  int i, j, iY , iX;

   if(eps_ <  0.)
      epsSq = eps_*eps_;
   else
      epsSq = eps_ * eps_ * dx_ * dy_;

   for (iY = 0; iY <= yBins_/2; iY++)
   {
      rTransY = iY * dy_;

      for (iX = 0; iX <= xBins_/2; iX++)
      {
         rTransX = iX * dx_;
//       rTot2 = rTransX*rTransX + rTransY*rTransY + epsSq;
         rTot2 = rTransX*rTransX + rTransY*rTransY;
         //we can add constant (to get the same numers as in ORBIT)
	 //this constant is + log(1000000.0)
	 //here in the original ORBIT we deleted this constant 
         greensF_[iX][iY] = 0.0;
         if(rTot2 > 0.0)
         {
           greensF_[iX][iY] = log(rTot2);
         }
      }

      for (iX = xBins_/2+1; iX < xBins_; iX++)
      {
         greensF_[iX][iY] = greensF_[xBins_-iX][iY];
      }
   }

   for (iY = yBins_/2+1; iY < yBins_; iY++)
   {
      for (iX = 0; iX < xBins_; iX++)
      {
         greensF_[iX][iY] = greensF_[iX][yBins_-iY];
      }
   }

   //   Calculate the FFT of the Greens Function:
   
   for (i = 0; i < xBins_; i++)
   for (j = 0; j < yBins_; j++)
   {
      in_[j + yBins_*i] = greensF_[i][j];
   }
    
   rfftwnd_one_real_to_complex(planForward_, in_, out_);

}

// Defines LSQM matrix
void ORBIT_Boundary::_defineLSQMatrix()
{

  int i,j;
  for( i = 0; i < (2*BPModes_+1); i++) {
  for( j = 0; j < (2*BPModes_+1); j++) {
    tmp_matrix_[i][j] =  0.0;
  }}
 
  int iBp;
  for ( iBp = 0; iBp < BPPoints_ ; iBp++){
    func_vector_ =  lsq_fuctions( BPx_[iBp],BPy_[iBp]);
     
     for( i = 0; i < (2*BPModes_+1); i++) {
     for( j = 0; j < (2*BPModes_+1); j++) {
       tmp_matrix_[i][j] += func_vector_[i]*func_vector_[j];
     }}
  }

  //inverse matrix
  _gaussjinv(tmp_matrix_,2*BPModes_+1);

  int k;
  for( i = 0; i < (2*BPModes_+1); i++) {
  for( j = 0; j < BPPoints_     ; j++) {
    LSQ_matrix_[i][j] = 0.0;
    func_vector_ =  lsq_fuctions( BPx_[j],BPy_[j]);
    for ( k = 0; k < (2*BPModes_+1); k++) {
      LSQ_matrix_[i][j] += tmp_matrix_[i][k] * func_vector_[k];
    }    
  }} 
}


// Defines LSQM coefficients
void ORBIT_Boundary::_defineLSQcoeff()
{
  int i,j;
  for( i = 0; i < (2*BPModes_+1); i++) {  
    cof_vector_[i] = 0.0;
     for( j = 0; j < BPPoints_ ; j++) {
       cof_vector_[i] += LSQ_matrix_[i][j]*BPphi_[j];
     } 
  }
}

//Defines is a particle into the boundary or not ( returns < 0 - it is not)
int ORBIT_Boundary::isInside(double x, double y)
{
  int isIns = -1;

  if (BPShape_ == 1){
    if( x*x+y*y < R_cirle_*R_cirle_ ) isIns = 1;
  }

  if (BPShape_ == 2){
    double xx,yy;
    xx = x/BPa_;
    yy = y/BPb_;
    if( (xx*xx + yy*yy) < 1.0 ) isIns = 1;
  }

  if (BPShape_ == 3){
    if( fabs(x/BPx_length_) < 0.5 && fabs(y/BPy_width_) < 0.5 ) isIns = 1;
  }

  return isIns;
}

// Calculates additional potential phi
double ORBIT_Boundary::calculatePhi(double x, double y)
{
  //cof_vector_ - should be calculated before
  int i;  
  double phi;
  phi = 0.0;
  func_vector_ =  lsq_fuctions( x, y);
  for( i = 0 ; i < (2*BPModes_+1); i++) {
    phi += cof_vector_[i]*func_vector_[i];
  }
  return phi;
}


// Calculates all of the LSQM functions at one point
double* ORBIT_Boundary::lsq_fuctions(double x, double y)
{
  int i = 0;
  double r,r2;
  r2 = x*x + y*y;
  if( r2 == 0 ) {
    func_vector_[0] = 1.0; 
     for( i = 1; i < (2*BPModes_+1); i++) {
       func_vector_[i] = 0.0;
     }
     return func_vector_;
  }
  
  r = sqrt(r2);
  double sin_f,cos_f, sin_f0,cos_f0, sin_f1,cos_f1;
  double rfac,rj;
  sin_f = y/r;
  cos_f = x/r;  
  sin_f0 = sin_f;
  cos_f0 = cos_f;
  rfac = r / BPrnorm_;
  rj = 1.0;
  func_vector_[0] = 1.;
  for( i = 0; i < BPModes_; i++) {
    rj *= rfac; 
    func_vector_[2*i+1]= rj*cos_f0;
    func_vector_[2*i+2]= rj*sin_f0;
    sin_f1 = sin_f*cos_f0 + cos_f*sin_f0;
    cos_f1 = cos_f*cos_f0 - sin_f*sin_f0;
    sin_f0 = sin_f1;
    cos_f0 = cos_f1;
  }
  return func_vector_;
}

// Adds potential from the boundary to the grid
void ORBIT_Boundary::addBoundaryPotential(double** phisc, 
                                         int iXmin, int iXmax, 
                                         int iYmin, int iYmax)
{
  int iB, iX, iY;
  for (iB = 0; iB < BPPoints_; iB++)
  {
 
   iX = iBPx_[iB];
   iY = iBPy_[iB];

    BPphi_[iB] =  W_coeff_[iB][0] * phisc[iX-1][iY-1] +
                  W_coeff_[iB][1] * phisc[iX-1][iY]   +
                  W_coeff_[iB][2] * phisc[iX-1][iY+1] +
                  W_coeff_[iB][3] * phisc[iX]  [iY-1] +
                  W_coeff_[iB][4] * phisc[iX]  [iY]   +
                  W_coeff_[iB][5] * phisc[iX]  [iY+1] +
                  W_coeff_[iB][6] * phisc[iX+1][iY-1] +
                  W_coeff_[iB][7] * phisc[iX+1][iY]   +
                  W_coeff_[iB][8] * phisc[iX+1][iY+1];
  }

  //define LSQM coefficient
  _defineLSQcoeff();

  int ix , iy;
  double x , y, phi;
  for( ix = iXmin; ix <= iXmax; ix++) {
  for( iy = iYmin; iy <= iYmax; iy++) {
    x = xGrid_[ix];
    y = yGrid_[iy];
    phi = calculatePhi(x,y);
    phisc[ix][iy] -= phi;
  }}

}
// Adds potential from the boundary to the grid 
// for SuperCode Matrix instead of double** phisc, 
void ORBIT_Boundary::addBoundaryPotential(Matrix(Real) &phisc, 
					  int iXmin, int iXmax, 
					  int iYmin, int iYmax){
  iXmin = iXmin -1;
  iYmin = iYmin -1;
  iXmax = iXmax -1;
  iYmax = iYmax -1;

  int iB, iX, iY;
  for (iB = 0; iB < BPPoints_; iB++)
  {
 
   iX = iBPx_[iB];
   iY = iBPy_[iB];

   BPphi_[iB] =  W_coeff_[iB][0] * phisc(iX,iY) +
     W_coeff_[iB][1] * phisc(iX,iY+1) +
     W_coeff_[iB][2] * phisc(iX,iY+2) +
     W_coeff_[iB][3] * phisc(iX+1,iY) +
     W_coeff_[iB][4] * phisc(iX+1,iY+1) +
     W_coeff_[iB][5] * phisc(iX+1,iY+2) +
     W_coeff_[iB][6] * phisc(iX+2,iY) +
     W_coeff_[iB][7] * phisc(iX+2,iY+1) +
     W_coeff_[iB][8] * phisc(iX+2,iY+2);
  }

  //define LSQM coefficient
  _defineLSQcoeff();

  int ix , iy;
  double x , y, phi;
  for( ix = iXmin; ix <= iXmax; ix++) {
  for( iy = iYmin; iy <= iYmax; iy++) {
    x = xGrid_[ix];
    y = yGrid_[iy];
    phi = calculatePhi(x,y);
    phisc(ix+1,iy+1) -= phi;
  }}
}

//Sets array with the interpolation coefficients for boundary points
void ORBIT_Boundary::_setInterpolationCoeff()
{
  int iB, iX, iY;
  double xFract, yFract, Wxm, Wx0, Wxp, Wym, Wy0, Wyp;
  for (iB = 0; iB < BPPoints_; iB++)
  {
     iX =  int ((BPx_[iB]-xGridMin_)/dx_ + 0.5);
     iY =  int ((BPy_[iB]-yGridMin_)/dy_ + 0.5);
 
     xFract = (BPx_[iB] - xGrid_[iX])/dx_;
     yFract = (BPy_[iB] - yGrid_[iY])/dy_;

     // TSC interpolation, see Hockney and Eastwood:
      
     Wxm = 0.5 * (0.5 - xFract) * (0.5 - xFract);
     Wx0 = 0.75 - xFract * xFract;
     Wxp = 0.5 * (0.5 + xFract) * (0.5 + xFract);
     Wym = 0.5 * (0.5 - yFract) * (0.5 - yFract);
     Wy0 = 0.75 - yFract * yFract;
     Wyp = 0.5 * (0.5 + yFract) * (0.5 + yFract);
     W_coeff_[iB][0] = Wxm * Wym;
     W_coeff_[iB][1] = Wxm * Wy0;
     W_coeff_[iB][2] = Wxm * Wyp;
     W_coeff_[iB][3] = Wx0 * Wym;
     W_coeff_[iB][4] = Wx0 * Wy0;
     W_coeff_[iB][5] = Wx0 * Wyp;
     W_coeff_[iB][6] = Wxp * Wym;
     W_coeff_[iB][7] = Wxp * Wy0;
     W_coeff_[iB][8] = Wxp * Wyp;
  }
}

//Define external grid's parameters from inner 
void ORBIT_Boundary::defineExtXYgrid(double *xGridMin, double *xGridMax, 
                                     double *yGridMin, double *yGridMax,
                                     double *dx      , double *dy)
{
  *xGridMin = xGridMin_;
  *xGridMax = xGridMax_;
  *yGridMin = yGridMin_;
  *yGridMax = yGridMax_;
  *dx = dx_;
  *dy = dy_;
//cerr << " debug boundary xGrid_[0] xGrid_[xBins_] = "<< xGrid_[0]  << " " <<  xGrid_[xBins_] <<"\n";
}

//Define external grid's index limits from inner 
void ORBIT_Boundary::defineExtXYlimits(int *ixMinB, int *ixMaxB, 
                                       int *iyMinB, int *iyMaxB)
{
  *ixMinB = ixMinB_;
  *ixMaxB = ixMaxB_;
  *iyMinB = iyMinB_;
  *iyMaxB = iyMaxB_;
}

//Get X-grid
double* ORBIT_Boundary::getXgrid()
{
  return xGrid_;
} 

//Get Y-grid
double* ORBIT_Boundary::getYgrid()
{
  return yGrid_;
} 


//Get the boundary shape
int ORBIT_Boundary::getBoundaryShape()
{
  return BPShape_;
} 

//Get the bounding curve limit for X-coordinate
double ORBIT_Boundary::getBoundaryXsize()
{
  return xSize_;
} 

//Get the bounding curve limit for Y-coordinate
double ORBIT_Boundary::getBoundaryYsize()
{
  return ySize_;
} 

//Get the number of boundary points
int ORBIT_Boundary::getNumbBPoints()
{
  return BPPoints_;
}


// Finalize MPI
void ORBIT_Boundary::_finalize_MPI()
{
 MPI_Finalize();
}



void ORBIT_Boundary::_gaussjinv(double **a, int n)
{
//    Taken from Nrecipes and slightly modified.
//   Get matrix A(nxn) and transform it into A^(-1).
//   Outputs error if A^(-1) doesn't exist

        int *indxc,*indxr,*ipiv;  // indexr and indexc track column permutation
        int i,icol,irow,j,k,l,ll;
        double big,dum,pivinv,temp;

        icol = 0;
        irow = 0;

        indxc= new int[n];
        indxr= new int[n];
        ipiv = new int[n];

        for (j=0;j<n;j++) ipiv[j]=0;
        for (i=0;i<n;i++) {
                big=0.0;
		// Looking for pivot
                for (j=0;j<n;j++)
                        if (ipiv[j] != 1)
                                for (k=0;k<n;k++) {
                                        if (ipiv[k] == 0) {
                                                if (fabs(a[j][k]) >= big) {
                                                        big=fabs(a[j][k]);
                                                        irow=j;
                                                        icol=k;
                                                }
                                        } else if (ipiv[k] > 1) {exit(1);};
                                }
                ++(ipiv[icol]);
// Pivot found - interchanging rows
                if (irow != icol) {
		  for (l=0;l<n;l++) {
		    temp = a[irow][l];
                    a[irow][l] = a[icol][l];
                    a[icol][l] = temp;
		  }
                }
                indxr[i]=irow;
                indxc[i]=icol;
                if (a[icol][icol] == 0.0) exit(1);
                pivinv=1.0/a[icol][icol];
                a[icol][icol]=1.0;
                for (l=0;l<n;l++) a[icol][l] *= pivinv;
                for (ll=0;ll<n;ll++)
                        if (ll != icol) {
                                dum=a[ll][icol];
                                a[ll][icol]=0.0;
                                for (l=0;l<n;l++) a[ll][l] -= a[icol][l]*dum;
                        }
        }
        for (l=n-1;l>=0;l--) {
                if (indxr[l] != indxc[l])
		  for (k=0;k<n;k++){
		    temp = a[k][indxr[l]];
                    a[k][indxr[l]] = a[k][indxc[l]];
                    a[k][indxc[l]] = temp;
		  }
        }
        delete [] ipiv;
        delete [] indxr;
        delete [] indxc;

}

//debug method
void ORBIT_Boundary::checkBoundary()
{

  //define potential at the boundary
  int j;
  for( j = 0; j < BPPoints_ ; j++) {
    BPphi_[j] = sin ((BPx_[j]*BPx_[j]*BPy_[j]*BPy_[j]));
  }

  //define LSQM coefficient
  _defineLSQcoeff();

  //check calculatePhi(x,y) - method
  double phi,x,y;

  for( j = 0; j < BPPoints_ ; j++) {
    x = BPx_[j];
    y = BPy_[j];
    //phi = calculatePhi(x,y);
    //std::cout << " x y = \t" << x << " \t" << y << " \t phi0  phi = \t" << BPphi_[j] << "\t " << phi << "\n";
    std::cout << "j="<< j<<"   x y =  \t" << x << "  \t" << y << std::endl;
  }
}
