/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TImpedance.mod
//
// AUTHOR
//   Slava Danilov,Andrey Shishlo, Jeff Holmes, ORNL, vux@ornl.gov
//
// CREATED
//   06/17/2002
//
// DESCRIPTION
//   Module descriptor file for the TImpedance  module. This module contains
//   information about transverse impedance.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module TFeedBack    - "TFeedBack Module."
                     - "Written by Slava Danilov, ORNL, vux@ornl.gov"
{
  Inherits Consts, Particles, Ring, Injection;

  Errors
    badTFeedBack    - "Sorry, you asked for a non existent TFeedBack",
    badFBSize       - "The size of the impedance vector is too small",
    badFBKick       - "This routine only applies to FBKick",
    badHerdNo       - "Sorry, you asked for an undefined macro Herd",
    tooManyFBKicks  - "Can only use one FFT TImpedance node now";

  public:

    Void ctor() 
                     - "Constructor for the TFeedBack module."
                     - "Initializes build values.";

    Integer
      nTFeedBack     - "Number of TFeedBack Nodes in the Ring",
      nLBinsTFBack   - "Number of longitudinal bins to use",
      useXdimTFB  - "If 1, X collective force is taken into account",
      useYdimTFB  - "If 1, Y collective force is taken into account";

    Real
     _resistivegain - "Gain of the resistive feedback,only for benchmarking "; 


  Void addTFeedBack(const String &n, const Integer &o,Integer &nBins,
Integer &nTurnDelay, Integer &useAvg, RealVector &CoordinateFilter, 
                               RealVector &AngleFilter)
                     - "Routine to add an TFeedBack node";
  Void showTFeedBack(const Integer &n, Ostream &os)
                     - "Routine to show the TFeedBack info";
  Real dumpFeedBackKick(const Integer &n, Real &angle)
                     - "Routine to dump kick at an angle";}
 
