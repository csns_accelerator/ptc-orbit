////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   LSpaceCharge.cc
//
// AUTHOR
//   John Galambos
//   ORNL, jdg@ornl.gov
//
// CREATED
//   12/17/97
//
// DESCRIPTION
//   Module descriptor file for the Longitudinal Space Charge module.
//   This module contains source for the LSpaceCharge related info.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "LongSC.h"
#include "LSpaceCharge.h"
#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "ComplexMat.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>
#include "mpi.h"

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

Array(ObjectPtr) LSpaceChargePointers;
extern Array(ObjectPtr) mParts, nodes;
extern Array(ObjectPtr) syncP;

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LongSC
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(LongSC, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    LongSC::LongSC, ~LongSC
//
// DESCRIPTION
//    Constructor and destructor for LongSC class
//
// PARAMETERS
//    None.
//
// RETURNS
//
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

LongSC::LongSC(const String &n, const Integer &order,
               Integer &nBins, Integer &nMacrosMin):
               Node(n, 0., order), _nBins(nBins), _nMacrosMin(nMacrosMin)
{
  _phiCount.resize(_nBins+1);
  _forceCalculated = 0;
  _local_MPI_Phi.resize(_nBins+1);

}

LongSC::~LongSC()
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LongSC::_longBin
//
// DESCRIPTION
//   Bins the macro particles longitudinally, and calculates the line density.
//   _phiCount(i) is the number of macros in bin i.
//
// PARAMETERS
//   mp - the macro herd to operate on.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void LongSC::_longBin(MacroPart &mp)
{
  Integer nBinsP1 = _nBins + 1;
  Real w, phiFactor;
  Integer i, iLocal, nMacros, im, ip;
  Real position;

  _deltaPhiBin = Consts::twoPi/(_nBins);
  _phiCount=0;

// Bin the particles longitudinally and store particle locations

  mp._phiMin = mp._phiMax = 0;

  for (i=1; i <= mp._nMacros; i++)
  {
    if(mp._phi(i) > mp._phiMax)  mp._phiMax = mp._phi(i);
    if(mp._phi(i) < mp._phiMin)  mp._phiMin = mp._phi(i);
    w = (mp._phi(i) + Consts::pi) / _deltaPhiBin;
    iLocal = Integer(w + 0.5);
    position = w - Real(iLocal);
    iLocal += 1;
    if(iLocal < 1) iLocal = _nBins;
    if(iLocal > _nBins) iLocal = 1;
    im = iLocal - 1;
    if(im < 1) im = _nBins;
    ip = iLocal + 1;
    if(ip > _nBins) ip = 1;
    _phiCount(im) += 0.5 * (0.5 - position) * (0.5 - position);
    _phiCount(iLocal) += 0.75 - position * position;
    _phiCount(ip) += 0.5 * (0.5 + position) * (0.5 + position);
    mp._fractLPosition(i) = position;
    mp._LPositionIndex(i) = iLocal;
  }

// Wrap 1st and last bins:

  _phiCount(1) += _phiCount(nBinsP1);
  _phiCount(nBinsP1) = _phiCount(1);

// Parallel Snchronization: ===MPI===
  int i_flag;
  MPI_Initialized(&i_flag);
  if(i_flag){
    nMacros = mp._nMacros;
    syncLongDist(_phiCount, _nBins, nMacros, mp._phiMin, mp._phiMax);
    mp._globalNMacros = nMacros;
  }
  else {
    mp._globalNMacros = mp._nMacros;
  }

  mp._longBinningDone = 1;

// Calculate a longitudinal weighting factor for Transverse
// space charge calculations: 
// (== local average macroPart density per rad / average density)

  mp._bunchFactor=0.;  // Calculate the bunch factor.
  for (i=1; i <= _nBins; i++)
    if(_phiCount(i) > mp._bunchFactor) mp._bunchFactor  = _phiCount(i);
  mp._bunchFactor = mp._globalNMacros / (mp._bunchFactor * _nBins);

  phiFactor = (mp._phiMax - mp._phiMin) /
	      (Real(mp._globalNMacros) * _deltaPhiBin);

  for (i=1; i <= mp._nMacros; i++)
  {
    iLocal = mp._LPositionIndex(i);
    position = mp._fractLPosition(i);
    im = iLocal - 1;
    if(im < 1) im = _nBins;
    ip = iLocal + 1;
    if(ip > _nBins) ip = 1;
    mp._LPosFactor(i) =
    (
      _phiCount(im) * 0.5 * (0.5 - position) * (0.5 - position) +
      _phiCount(iLocal) * (0.75 - position * position) +
      _phiCount(ip) * 0.5 * (0.5 + position) * (0.5 + position)
    ) * phiFactor;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//  syncLongDist
//
// DESCRIPTION
//    Synchronizes the longitudinal distribution across all parallel jobs
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Integer LongSC::syncLongDist(Vector(Real) &count, Integer &nBins,
                     Integer &nMacros, Real &phiMin, Real &phiMax)
{

   double d_local[2];
   double d_global[2];
   d_local[0]= phiMax;
   d_local[1]=-phiMin;
   MPI_Allreduce ( d_local,d_global,2,MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
   phiMax =   d_global[0];
   phiMin = - d_global[1];
   
   int nMacros_local,nMacros_global;
   nMacros_local = nMacros;
   MPI_Allreduce (&nMacros_local,&nMacros_global,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD);
   nMacros = nMacros_global;
 
   _local_MPI_Phi = count;
   MPI_Allreduce ( (double *) &_local_MPI_Phi.data(), 
                   (double *) &count.data(), 
                   nBins+1, 
		   MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FFTLongSC
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(FFTLongSC, LongSC);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FFTLongSC::FFTLongSC, ~FFTLongSC
//
// DESCRIPTION
//    Constructor and destructor for LongSC class
//
// PARAMETERS
//    None.
//
// RETURNS
//
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

FFTLongSC::FFTLongSC(const String &n, const Integer &order,
                     Integer &nBins, Vector(Complex) &Z, Real &b_a,
                     Integer &useAvg, Integer &nMacrosMin,
                     Integer &useSpaceCharge):
              LongSC(n, order, nBins, nMacrosMin), _zImped_n(Z), _b_a(b_a),
	    _useAverage(useAvg),_useSpaceCharge(useSpaceCharge)
{
  _FFTMagnitude.resize(_nBins);
  _FFTPhase.resize(_nBins);
// Impedance of free space (Ohms)
  _z_0 = Consts::vLight * 4. * Consts::pi * 1.e-07;
  _z.resize(_nBins);
  _chi.resize(_nBins);
  if(_useAverage) _deltaEGrid.resize(_nBins+1);

  _in = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _out =(FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _plan = fftw_create_plan(_nBins, FFTW_FORWARD, FFTW_ESTIMATE);
}

FFTLongSC::~FFTLongSC()
{
  fftw_destroy_plan(_plan);
  delete [] _in;
  delete [] _out;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FFTLongSC::_nodeCalculator
//
// DESCRIPTION
//    Bins the macro particles, and calculates the line density.
//    Does an FFT on the binned distribution, and sets up some
//    stuff for the particle kick calculations.
//
// PARAMETERS
//    mp - the macro herd to operate on.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FFTLongSC::_nodeCalculator(MacroPart &mp)
{
  Real zSpaceCharge_n, w, position;
  Integer i, iLocal;

  if(mp._feelsHerds == 1) // just bin the particles, use existing forces:
  {
    if(_forceCalculated == 0) return;
    for (i = 1; i <= mp._nMacros; i++)
    {
      w = (mp._phi(i) + Consts::pi) / _deltaPhiBin;
      iLocal = Integer(w + 0.5);
      position = w - Real(iLocal);
      iLocal += 1;
      if(iLocal < 1) iLocal = _nBins;
      if(iLocal > _nBins) iLocal = 1;
      mp._fractLPosition(i) = position;
      mp._LPositionIndex(i) = iLocal;
    }
    return;
  }

  _longBin(mp);  // Bin the particles.

  if(mp._globalNMacros < _nMacrosMin) return;

// FFT the beam using the FFTW package

  for (i=1; i<= _nBins; i++)
  {
    c_re(_in[i-1]) = _phiCount(i);
    c_im(_in[i-1]) = 0.;
  }
  fftw(_plan, 1, _in, 1, 0, _out, 1, 0);

  Real realPart, imagPart;
  _FFTMagnitude(1) = c_re(_out[0])/Real(_nBins);
  _FFTPhase(1) = 0.;

  for (i=2; i<= _nBins/2; i++)
  {
    realPart = c_re(_out[i-1])/Real(_nBins);
    imagPart =  c_im(_out[i-1])/Real(_nBins);
    _FFTMagnitude(i) = Sqrt(realPart*realPart + imagPart*imagPart);
    _FFTPhase(i) =  atan2(imagPart,realPart);
  }

// Figure space charge part of kick:

  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart & 
                                      Particles::All_Mask) - 1));

  zSpaceCharge_n = 0.;  // if _useSpaceCharge = 0
  // Otherwise, set negative since space charge is capacitive
  if(_useSpaceCharge != 0)
  {
    zSpaceCharge_n = -_z_0 * (1. + 2.*log(_b_a)) /
                     (2* sp->_betaSync * Sqr(sp->_gammaSync));
    zSpaceCharge_n /= Real(LSpaceCharge::nFFTLongs);
  }

// Figure the impedance as a function of mode. Include factor of n.

  for (Integer n=1; n <= _nBins/2 ; n++)
  {
    // Inductive is positive imaginary, capacitive is negative imaginary.
    _z(n+1) = Real(n) * Ring::harmonicNumber *
              Sqrt(Sqr(_zImped_n(n).re) + 
	           Sqr(_zImped_n(n).im + zSpaceCharge_n));
    _chi(n+1) = atan2((_zImped_n(n).im + zSpaceCharge_n),
                      (_zImped_n(n).re) );
  }

  _charge2Current =
     sp->_charge * Consts::eCharge * sp->_betaSync * Consts::vLight *
     Injection::nReals_Macro * Real(_nBins) * Ring::harmonicNumber /
     (Ring::lRing);

// Use this for longTrackOnly option:

     if(Ring::longTrackOnly)
     {
       if(Ring::gammaTrans < 0.0)
       {
         Ring::etaCompaction = 
           -Sqr(1./Ring::gammaTrans) - Sqr(1./mp._syncPart._gammaSync);
       }
       else
       {
         Ring::etaCompaction = 
            Sqr(1./Ring::gammaTrans) - Sqr(1./mp._syncPart._gammaSync);
       }
     }

// If interpolated kick option is used,
// calculate kick at each grid point:

  if(_useAverage)
  {
    Real dummy, gridAngle, cosArg;

    for (Integer j=1; j <= _nBins; j++)
    {
      gridAngle = (j-1)*Consts::twoPi/(_nBins);
      if((gridAngle-Consts::pi) < mp._phiMin ||
         (gridAngle-Consts::pi) > mp._phiMax) 
      {
        _deltaEGrid(j) =0.; // avoid aliasing outside range of particles
      }
      else
      {
        // dummy = _FFTMagnitude(n) * _z(1) *cos(_FFTPhase(1) + _chi(1));;
        dummy = 0.;  // Do not include n=1 term (i.e. constant with phi)
        for(Integer n=2; n<= _nBins/2; n++)
	{
          cosArg = Real(n-1) * gridAngle + _FFTPhase(n) + _chi(n);
          dummy += 2. * _FFTMagnitude(n) * _z(n) * cos(cosArg);
	}
        _deltaEGrid(j) =  -1.e-9 * sp->_charge * _charge2Current * dummy;
      }
    }
    _deltaEGrid(_nBins+1) = _deltaEGrid(1);
  }
  _forceCalculated = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FFTLongSC::_updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle with an LongSC. The longitudinal space charge kick is
//   added to each macro particle here.
//
// PARAMETERS
//   None.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FFTLongSC::_updatePartAtNode(MacroPart &mp)
{
  Real Kick, angle, position;
  Integer j, iLocal, im, ip;

  if(mp._globalNMacros < _nMacrosMin) return;

  if(!_forceCalculated) return;

// Average kick interpolated from bin location:

  if(_useAverage)
  {
    for (j=1; j<= mp._nMacros; j++)
    {
      iLocal = mp._LPositionIndex(j);
      position = mp._fractLPosition(j);
      im = iLocal - 1;
      if(im < 1) im = _nBins;
      ip = iLocal + 1;
      if(ip > _nBins) ip = 1;
      Kick =
        _deltaEGrid(im) * 0.5 * (0.5 - position) * (0.5 - position) +
        _deltaEGrid(iLocal) * (0.75 - position * position) +
        _deltaEGrid(ip) * 0.5 * (0.5 + position) * (0.5 + position);
      mp._deltaE(j) += Kick;
      mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

// normally phase is updated in TransMap

    if(Ring::longTrackOnly)
      {
        mp._phi(j) += Ring::harmonicNumber * Consts::twoPi *
          Ring::etaCompaction * mp._dp_p(j)/Ring::nLongUpdates;
	if(Abs(mp._phi(j)) > Consts::pi)
        {
          Real sign = -mp._phi(j)/Abs(mp._phi(j));
          mp._phi(j) = Consts::pi * sign + fmod(mp._phi(j),Consts::pi);
        }
      }
    }
  }
  else

//  Calculate the kick explicitly for each particle.

  {        
    for (j=1; j<= mp._nMacros; j++)
    {        
      angle = mp._phi(j);
      mp._deltaE(j) += _kick(angle);
      mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

// normally phase is updated in TransMap

      if(Ring::longTrackOnly)
      {
        mp._phi(j) += Ring::harmonicNumber * Consts::twoPi * 
          Ring::etaCompaction * mp._dp_p(j)/Ring::nLongUpdates;
	if(Abs(mp._phi(j)) > Consts::pi)
        {
          Real sign = -mp._phi(j)/Abs(mp._phi(j));
          mp._phi(j) = Consts::pi * sign + fmod(mp._phi(j),Consts::pi);
        }
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FFTLongSC::_kick
//
// DESCRIPTION
//    Returns the longitudinal space charge kick to a macroparticle.
//    Be sure that the nodeCalculator is called first. (GeV)
//
// PARAMETERS
//    angle - the phase angle of the particle (rad)
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Real FFTLongSC::_kick(Real &angle)
{
// n=1 term has no impact (constant with phi)
// f(phi) = _FFTMagnitude(1) + sum (n=2 -> N/2) of
//   [2* _FFTMagnitude(i) * cos(phi*(n-1) + _FFTPhase(n) + _chi(n))]
  Integer n;
  Real dummy=0.;
  Real cosArg;

  for (n=2; n <= _nBins/2; n++)
  {
    cosArg = Real(n-1) * (angle + Consts::pi) + _FFTPhase(n) + _chi(n);
    dummy += 2 * _FFTMagnitude(n) * _z(n) * cos(cosArg);
  }

  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart & 
                                      Particles::All_Mask) - 1));

  return (-1.e-9 * sp->_charge * _charge2Current * dummy);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FreqDepLimp
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(FreqDepLimp, LongSC);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqDepLimp::FreqDepLimp, ~FreqDepLimp
//
// DESCRIPTION
//   Constructor and destructor for TDLimp class
//
// PARAMETERS
//   None
//
// RETURNS
//
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

FreqDepLimp::FreqDepLimp(const String &n, const Integer &order,
                         Integer &nBins, Integer &nTable,
                         Vector(Real) &fTable, Vector(Complex) &zTable,
                         Real &b_a, Integer &useAvg, Integer &nMacrosMin,
                         Integer &useSpaceCharge):
                         LongSC(n, order, nBins, nMacrosMin),
                          _nTable(nTable), _fTable(fTable), _zTable(zTable),
                         _b_a(b_a),
	                 _useAverage(useAvg), _useSpaceCharge(useSpaceCharge)
{
  _FFTMagnitude.resize(_nBins);
  _FFTPhase.resize(_nBins);
// Impedance of free space (Ohms)
  _z_0 = Consts::vLight * 4. * Consts::pi * 1.e-07;
  _z.resize(_nBins);
  _chi.resize(_nBins);
  if(_useAverage) _deltaEGrid.resize(_nBins+1);

  _in = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _out =(FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _plan = fftw_create_plan(_nBins, FFTW_FORWARD, FFTW_ESTIMATE);
}

FreqDepLimp::~FreqDepLimp()
{
  fftw_destroy_plan(_plan);
  delete [] _in;
  delete [] _out;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqDepLimp::_nodeCalculator
//
// DESCRIPTION
//
//   Calculates the impedance harmonics using the table and the
//   current beam frequency.
//   Bins the macro particles, and calculates the line density.
//   Does an FFT on the binned distribution, and sets up some
//   stuff for the particle kick calculations.
//
// PARAMETERS
//   mp - the macro herd to operate on.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FreqDepLimp::_nodeCalculator(MacroPart &mp)
{
  Real zSpaceCharge_n, w, position;
  Integer i, j, n, iLocal;

  Real Freq0 = mp._syncPart._betaSync * Consts::vLight / Ring::lRing;
  Real Freq, frac;

  for(n = 1; n <= _nBins/2; n++)
  {
    Freq = n * Freq0;
    if(Freq <= _fTable(1))
    {
      _zImped_n(n) = _zTable(1) / Real(n);
    }
    else if(Freq >= _fTable(_nTable))
    {
      _zImped_n(n) = _zTable(_nTable) / Real(n);
    }
    else
    {
      Integer jl, ju;
      for(j = 1; j < _nTable; j++)
      {
        if(_fTable(j) <= Freq)
	{
          jl = j;
          ju = j + 1;
        }
      }
      frac = (_fTable(ju) - Freq) / (_fTable(ju) - _fTable(jl));
      _zImped_n(n) = (_zTable(jl) * frac +
                      _zTable(ju) * (1.0 - frac)) / Real(n);
    }
  }

  if(mp._feelsHerds == 1) // just bin the particles, use existing forces:
  {
    if(_forceCalculated == 0) return;
    for (i = 1; i <= mp._nMacros; i++)
    {
      w = (mp._phi(i) + Consts::pi) / _deltaPhiBin;
      iLocal = Integer(w + 0.5);
      position = w - Real(iLocal);
      iLocal += 1;
      if(iLocal < 1) iLocal = _nBins;
      if(iLocal > _nBins) iLocal = 1;
      mp._fractLPosition(i) = position;
      mp._LPositionIndex(i) = iLocal;
    }
    return;
  }

  _longBin(mp);  // Bin the particles.

  if(mp._globalNMacros < _nMacrosMin) return;

// FFT the beam using the FFTW package

  for (i = 1; i <= _nBins; i++)
  {
    c_re(_in[i-1]) = _phiCount(i);
    c_im(_in[i-1]) = 0.;
  }
  fftw(_plan, 1, _in, 1, 0, _out, 1, 0);

  Real realPart, imagPart;
  _FFTMagnitude(1) = c_re(_out[0]) / Real(_nBins);
  _FFTPhase(1) = 0.;

  for (i = 2; i <= _nBins / 2; i++)
  {
    realPart = c_re(_out[i-1]) / Real(_nBins);
    imagPart = c_im(_out[i-1]) / Real(_nBins);
    _FFTMagnitude(i) = Sqrt(realPart * realPart + imagPart * imagPart);
    _FFTPhase(i) =  atan2(imagPart, realPart);
  }

// Figure space charge part of kick:

  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart &
                                      Particles::All_Mask) - 1));

  zSpaceCharge_n = 0.;  // if _useSpaceCharge = 0
  // Otherwise, set negative since space charge is capacitive
  if(_useSpaceCharge != 0)
  {
    zSpaceCharge_n = -_z_0 * (1. + 2.*log(_b_a)) /
                     (2* sp->_betaSync * Sqr(sp->_gammaSync));
    zSpaceCharge_n /= Real(LSpaceCharge::nFFTLongs);
  }

// Figure the impedance as a function of mode. Include factor of n.

  for (n = 1; n <= _nBins/2 ; n++)
  {
    // Inductive is positive imaginary, capacitive is negative imaginary.
    _z(n+1) = Real(n) * Ring::harmonicNumber *
              Sqrt(Sqr(_zImped_n(n).re) +
	           Sqr(_zImped_n(n).im + zSpaceCharge_n));
    _chi(n+1) = atan2((_zImped_n(n).im + zSpaceCharge_n),
                      (_zImped_n(n).re));
  }

  _charge2Current =
    sp->_charge * Consts::eCharge * sp->_betaSync * Consts::vLight *
    Injection::nReals_Macro * Real(_nBins) * Ring::harmonicNumber /
    (Ring::lRing);

// Use this for longTrackOnly option:

     if(Ring::longTrackOnly)
     {
       if(Ring::gammaTrans < 0.0)
       {
         Ring::etaCompaction =
           -Sqr(1. / Ring::gammaTrans) - Sqr(1. / mp._syncPart._gammaSync);
       }
       else
       {
         Ring::etaCompaction =
            Sqr(1. / Ring::gammaTrans) - Sqr(1. / mp._syncPart._gammaSync);
       }
     }

// If interpolated kick option is used,
// calculate kick at each grid point:

  if(_useAverage)
  {
    Real dummy, gridAngle, cosArg;

    for (j = 1; j <= _nBins; j++)
    {
      gridAngle = (j - 1) * Consts::twoPi / (_nBins);
      if((gridAngle - Consts::pi) < mp._phiMin ||
         (gridAngle - Consts::pi) > mp._phiMax)
      {
        _deltaEGrid(j) = 0.; // avoid aliasing outside range of particles
      }
      else
      {
        // dummy = _FFTMagnitude(n) * _z(1) * cos(_FFTPhase(1) + _chi(1));
        dummy = 0.;  // Do not include n = 1 term (i.e. constant with phi)
        for(n = 2; n <= _nBins / 2; n++)
	{
          cosArg = Real(n - 1) * gridAngle + _FFTPhase(n) + _chi(n);
          dummy += 2. * _FFTMagnitude(n) * _z(n) * cos(cosArg);
	}
        _deltaEGrid(j) =  -1.e-9 * sp->_charge * _charge2Current * dummy;
      }
    }
    _deltaEGrid(_nBins + 1) = _deltaEGrid(1);
  }
  _forceCalculated = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqDepLimp::_updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle with an TDLimp. The longitudinal impedance
//   plus space charge kick is
//   added to each macro particle here.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FreqDepLimp::_updatePartAtNode(MacroPart &mp)
{
  Real Kick, angle, position;
  Integer j, iLocal, im, ip;

  if(mp._globalNMacros < _nMacrosMin) return;

  if(!_forceCalculated) return;

// Average kick interpolated from bin location:

  if(_useAverage)
  {
    for (j = 1; j <= mp._nMacros; j++)
    {
      iLocal = mp._LPositionIndex(j);
      position = mp._fractLPosition(j);
      im = iLocal - 1;
      if(im < 1) im = _nBins;
      ip = iLocal + 1;
      if(ip > _nBins) ip = 1;
      Kick =
        _deltaEGrid(im) * 0.5 * (0.5 - position) * (0.5 - position) +
        _deltaEGrid(iLocal) * (0.75 - position * position) +
        _deltaEGrid(ip) * 0.5 * (0.5 + position) * (0.5 + position);
      mp._deltaE(j) += Kick;
      mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

// normally phase is updated in TransMap

    if(Ring::longTrackOnly)
      {
        mp._phi(j) += Ring::harmonicNumber * Consts::twoPi *
          Ring::etaCompaction * mp._dp_p(j) / Ring::nLongUpdates;
	if(Abs(mp._phi(j)) > Consts::pi)
        {
          Real sign = -mp._phi(j) / Abs(mp._phi(j));
          mp._phi(j) = Consts::pi * sign + fmod(mp._phi(j), Consts::pi);
        }
      }
    }
  }
  else

//  Calculate the kick explicitly for each particle.

  {
    for (j=1; j<= mp._nMacros; j++)
    {
      angle = mp._phi(j);
      mp._deltaE(j) += _kick(angle);
      mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

// normally phase is updated in TransMap

      if(Ring::longTrackOnly)
      {
        mp._phi(j) += Ring::harmonicNumber * Consts::twoPi * 
          Ring::etaCompaction * mp._dp_p(j) / Ring::nLongUpdates;
	if(Abs(mp._phi(j)) > Consts::pi)
        {
          Real sign = -mp._phi(j) / Abs(mp._phi(j));
          mp._phi(j) = Consts::pi * sign + fmod(mp._phi(j), Consts::pi);
        }
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FreqDepLimp::_kick
//
// DESCRIPTION
//    Returns the longitudinal space charge kick to a macroparticle.
//    Be sure that the nodeCalculator is called first. (GeV)
//
// PARAMETERS
//    angle - the phase angle of the particle (rad)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Real FreqDepLimp::_kick(Real &angle)
{
// n=1 term has no impact (constant with phi)
// f(phi) = _FFTMagnitude(1) + sum (n = 2 -> N / 2) of
//   [2 * _FFTMagnitude(i) * cos(phi * (n-1) + _FFTPhase(n) + _chi(n))]

  Integer n;
  Real dummy=0.;
  Real cosArg;

  for (n = 2; n <= _nBins / 2; n++)
  {
    cosArg = Real(n - 1) * (angle + Consts::pi) + _FFTPhase(n) + _chi(n);
    dummy += 2 * _FFTMagnitude(n) * _z(n) * cos(cosArg);
  }

  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart &
                                      Particles::All_Mask) - 1));

  return (-1.e-9 * sp->_charge * _charge2Current * dummy);
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FreqBetDepLimp
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(FreqBetDepLimp, LongSC);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqBetDepLimp::FreqBetDepLimp, ~FreqBetDepLimp
//
// DESCRIPTION
//   Constructor and destructor for TDLimp class
//
// PARAMETERS
//   None
//
// RETURNS
//
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

FreqBetDepLimp::FreqBetDepLimp(const String &n, const Integer &order,
                               Integer &nBins,
                               Integer &nbTable, Vector(Real) &bTable,
                               Integer &nfTable, Vector(Real) &fTable,
                               Matrix(Complex) &zTable,
                               Real &b_a, Integer &useAvg,
                               Integer &nMacrosMin, Integer &useSpaceCharge):
                               LongSC(n, order, nBins, nMacrosMin),
                               _nbTable(nbTable), _bTable(bTable),
                               _nfTable(nfTable), _fTable(fTable),
                               _zTable(zTable),
                               _b_a(b_a), _useAverage(useAvg),
                               _useSpaceCharge(useSpaceCharge)
{
  _zTable.resize(_nbTable, _nfTable);

  _FFTMagnitude.resize(_nBins);
  _FFTPhase.resize(_nBins);
// Impedance of free space (Ohms)
  _z_0 = Consts::vLight * 4. * Consts::pi * 1.e-07;
  _z.resize(_nBins);
  _chi.resize(_nBins);
  if(_useAverage) _deltaEGrid.resize(_nBins+1);

  _in = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _out =(FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _plan = fftw_create_plan(_nBins, FFTW_FORWARD, FFTW_ESTIMATE);
}

FreqBetDepLimp::~FreqBetDepLimp()
{
  fftw_destroy_plan(_plan);
  delete [] _in;
  delete [] _out;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqBetDepLimp::_nodeCalculator
//
// DESCRIPTION
//
//   Calculates the impedance harmonics using the table and the
//   current beam frequency and relativistic beta.
//   Bins the macro particles, and calculates the line density.
//   Does an FFT on the binned distribution, and sets up some
//   stuff for the particle kick calculations.
//
// PARAMETERS
//   mp - the macro herd to operate on.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void FreqBetDepLimp::_nodeCalculator(MacroPart &mp)
{
  Real zSpaceCharge_n, w, position;
  Integer i, j, n, iLocal;

  Real Freq0 = mp._syncPart._betaSync * Consts::vLight / Ring::lRing;
  Real Freq, frac;

  Integer kBeta = 1;
  for(Integer k = 1; k <= _nbTable; k++)
  {
    if(mp._syncPart._betaSync >= _bTable(k)) kBeta = k;
  }
  if(kBeta == _nbTable) kBeta = _nbTable - 1;
  frac = (mp._syncPart._betaSync - _bTable(kBeta)) /
         (_bTable(kBeta + 1) - _bTable(kBeta));
  if(frac > 1.0) frac = 1.0;
  if(frac < 0.0) frac = 0.0;

  Vector(Complex) ZT;

  ZT.resize(_nfTable);

  for(Integer k = 1; k <= _nfTable; k++)
  {
    ZT(k) =  _zTable(kBeta, k) +
            (_zTable(kBeta + 1, k) - _zTable(kBeta, k)) * frac;
  }

  for(n = 1; n <= _nBins/2; n++)
  {
    Freq = n * Freq0;
    if(Freq <= _fTable(1))
    {
      _zImped_n(n) = ZT(1) / Real(n);
    }
    else if(Freq >= _fTable(_nfTable))
    {
      _zImped_n(n) = ZT(_nfTable) / Real(n);
    }
    else
    {
      Integer jl, ju;
      for(j = 1; j < _nfTable; j++)
      {
        if(_fTable(j) <= Freq)
	{
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freq - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zImped_n(n) = (ZT(jl) +
                     (ZT(ju) - ZT(jl)) * frac) / Real(n);
    }
  }

  if(mp._feelsHerds == 1) // just bin the particles, use existing forces:
  {
    if(_forceCalculated == 0) return;
    for (i = 1; i <= mp._nMacros; i++)
    {
      w = (mp._phi(i) + Consts::pi) / _deltaPhiBin;
      iLocal = Integer(w + 0.5);
      position = w - Real(iLocal);
      iLocal += 1;
      if(iLocal < 1) iLocal = _nBins;
      if(iLocal > _nBins) iLocal = 1;
      mp._fractLPosition(i) = position;
      mp._LPositionIndex(i) = iLocal;
    }
    return;
  }

  _longBin(mp);  // Bin the particles.

  if(mp._globalNMacros < _nMacrosMin) return;

// FFT the beam using the FFTW package

  for (i = 1; i <= _nBins; i++)
  {
    c_re(_in[i-1]) = _phiCount(i);
    c_im(_in[i-1]) = 0.;
  }
  fftw(_plan, 1, _in, 1, 0, _out, 1, 0);

  Real realPart, imagPart;
  _FFTMagnitude(1) = c_re(_out[0]) / Real(_nBins);
  _FFTPhase(1) = 0.;

  for (i = 2; i <= _nBins / 2; i++)
  {
    realPart = c_re(_out[i-1]) / Real(_nBins);
    imagPart = c_im(_out[i-1]) / Real(_nBins);
    _FFTMagnitude(i) = Sqrt(realPart * realPart + imagPart * imagPart);
    _FFTPhase(i) =  atan2(imagPart, realPart);
  }

// Figure space charge part of kick:

  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart &
                                      Particles::All_Mask) - 1));

  zSpaceCharge_n = 0.;  // if _useSpaceCharge = 0
  // Otherwise, set negative since space charge is capacitive
  if(_useSpaceCharge != 0)
  {
    zSpaceCharge_n = -_z_0 * (1. + 2.*log(_b_a)) /
                     (2* sp->_betaSync * Sqr(sp->_gammaSync));
    zSpaceCharge_n /= Real(LSpaceCharge::nFFTLongs);
  }

// Figure the impedance as a function of mode. Include factor of n.

  for (n = 1; n <= _nBins/2 ; n++)
  {
    // Inductive is positive imaginary, capacitive is negative imaginary.
    _z(n+1) = Real(n) * Ring::harmonicNumber *
              Sqrt(Sqr(_zImped_n(n).re) +
	           Sqr(_zImped_n(n).im + zSpaceCharge_n));
    _chi(n+1) = atan2((_zImped_n(n).im + zSpaceCharge_n),
                      (_zImped_n(n).re));
  }

  _charge2Current =
    sp->_charge * Consts::eCharge * sp->_betaSync * Consts::vLight *
    Injection::nReals_Macro * Real(_nBins) * Ring::harmonicNumber /
    (Ring::lRing);

// Use this for longTrackOnly option:

     if(Ring::longTrackOnly)
     {
       if(Ring::gammaTrans < 0.0)
       {
         Ring::etaCompaction =
           -Sqr(1. / Ring::gammaTrans) - Sqr(1. / mp._syncPart._gammaSync);
       }
       else
       {
         Ring::etaCompaction =
            Sqr(1. / Ring::gammaTrans) - Sqr(1. / mp._syncPart._gammaSync);
       }
     }

// If interpolated kick option is used,
// calculate kick at each grid point:

  if(_useAverage)
  {
    Real dummy, gridAngle, cosArg;

    for (j = 1; j <= _nBins; j++)
    {
      gridAngle = (j - 1) * Consts::twoPi / (_nBins);
      if((gridAngle - Consts::pi) < mp._phiMin ||
         (gridAngle - Consts::pi) > mp._phiMax)
      {
        _deltaEGrid(j) = 0.; // avoid aliasing outside range of particles
      }
      else
      {
        // dummy = _FFTMagnitude(n) * _z(1) * cos(_FFTPhase(1) + _chi(1));
        dummy = 0.;  // Do not include n = 1 term (i.e. constant with phi)
        for(n = 2; n <= _nBins / 2; n++)
	{
          cosArg = Real(n - 1) * gridAngle + _FFTPhase(n) + _chi(n);
          dummy += 2. * _FFTMagnitude(n) * _z(n) * cos(cosArg);
	}
        _deltaEGrid(j) =  -1.e-9 * sp->_charge * _charge2Current * dummy;
      }
    }
    _deltaEGrid(_nBins + 1) = _deltaEGrid(1);
  }
  _forceCalculated = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqBetDepLimp::_updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle with an TDLimp. The longitudinal impedance
//   plus space charge kick is
//   added to each macro particle here.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FreqBetDepLimp::_updatePartAtNode(MacroPart &mp)
{
  Real Kick, angle, position;
  Integer j, iLocal, im, ip;

  if(mp._globalNMacros < _nMacrosMin) return;

  if(!_forceCalculated) return;

// Average kick interpolated from bin location:

  if(_useAverage)
  {
    for (j = 1; j <= mp._nMacros; j++)
    {
      iLocal = mp._LPositionIndex(j);
      position = mp._fractLPosition(j);
      im = iLocal - 1;
      if(im < 1) im = _nBins;
      ip = iLocal + 1;
      if(ip > _nBins) ip = 1;
      Kick =
        _deltaEGrid(im) * 0.5 * (0.5 - position) * (0.5 - position) +
        _deltaEGrid(iLocal) * (0.75 - position * position) +
        _deltaEGrid(ip) * 0.5 * (0.5 + position) * (0.5 + position);
      mp._deltaE(j) += Kick;
      mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

// normally phase is updated in TransMap

    if(Ring::longTrackOnly)
      {
        mp._phi(j) += Ring::harmonicNumber * Consts::twoPi *
          Ring::etaCompaction * mp._dp_p(j) / Ring::nLongUpdates;
	if(Abs(mp._phi(j)) > Consts::pi)
        {
          Real sign = -mp._phi(j) / Abs(mp._phi(j));
          mp._phi(j) = Consts::pi * sign + fmod(mp._phi(j), Consts::pi);
        }
      }
    }
  }
  else

//  Calculate the kick explicitly for each particle.

  {
    for (j=1; j<= mp._nMacros; j++)
    {
      angle = mp._phi(j);
      mp._deltaE(j) += _kick(angle);
      mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

// normally phase is updated in TransMap

      if(Ring::longTrackOnly)
      {
        mp._phi(j) += Ring::harmonicNumber * Consts::twoPi * 
          Ring::etaCompaction * mp._dp_p(j) / Ring::nLongUpdates;
	if(Abs(mp._phi(j)) > Consts::pi)
        {
          Real sign = -mp._phi(j) / Abs(mp._phi(j));
          mp._phi(j) = Consts::pi * sign + fmod(mp._phi(j), Consts::pi);
        }
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    FreqBetDepLimp::_kick
//
// DESCRIPTION
//    Returns the longitudinal space charge kick to a macroparticle.
//    Be sure that the nodeCalculator is called first. (GeV)
//
// PARAMETERS
//    angle - the phase angle of the particle (rad)
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Real FreqBetDepLimp::_kick(Real &angle)
{
// n=1 term has no impact (constant with phi)
// f(phi) = _FFTMagnitude(1) + sum (n = 2 -> N / 2) of
//   [2 * _FFTMagnitude(i) * cos(phi * (n-1) + _FFTPhase(n) + _chi(n))]

  Integer n;
  Real dummy=0.;
  Real cosArg;

  for (n = 2; n <= _nBins / 2; n++)
  {
    cosArg = Real(n - 1) * (angle + Consts::pi) + _FFTPhase(n) + _chi(n);
    dummy += 2 * _FFTMagnitude(n) * _z(n) * cos(cosArg);
  }

  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart &
                                      Particles::All_Mask) - 1));

  return (-1.e-9 * sp->_charge * _charge2Current * dummy);
}

///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE LSpaceCharge
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LSpaceCharge::ctor
//
// DESCRIPTION
//   Initializes the LSpaceCharge nodes.
//
// PARAMETERS
//   None.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void LSpaceCharge::ctor()
{
// set some initial values
  nLongSCs = 0;
  nLongBins = 32;
  nFFTLongs  = 0;    
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LSpaceCharge::addFFTLSpaceCharge
//
// DESCRIPTION
//   Adds a Longitudinal Space Charge Node
//
// PARAMETERS
//   name:    Name for the TM 
//   order:   Order index
//   nBins    Number of bins to use
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LSpaceCharge::addFFTLSpaceCharge(const String &name, const Integer &order,
  Vector(Complex) &Z , Real &b_a, Integer &useAvg,
  Integer &nMacrosMin, Integer &useSpaceCharge)

{

  if(Z.rows() < nLongBins/2) except(badImpedSize);
  if (nNodes == nodes.size())
    nodes.resize(nNodes + 1);
  if(nLongSCs == LSpaceChargePointers.size())
    LSpaceChargePointers.resize(nLongSCs + 1);

  nNodes++;

  nodes(nNodes-1) = new FFTLongSC(name, order, nLongBins, Z, b_a,
                                  useAvg, nMacrosMin, useSpaceCharge);
  nLongSCs ++;
  nFFTLongs ++;
  Ring::nLongUpdates++;
  LSpaceChargePointers(nLongSCs -1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LSpaceCharge::addFreqDepLimp
//
// DESCRIPTION
//   Adds a Longitudinal Space Charge Node
//
// PARAMETERS
//   name:    Name for the TM
//   order:   Order index
//   nBins    Number of bins to use
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LSpaceCharge::addFreqDepLimp(const String &name, const Integer &order,
                                  Integer &nTable,
                                  Vector(Real) &fTable,
                                  Vector(Complex) &zTable,
                                  Real &b_a, Integer &useAvg,
                                  Integer &nMacrosMin,
                                  Integer &useSpaceCharge)
{
  if(fTable.rows() < nTable) except(badImpedSize);
  if(zTable.rows() < nTable) except(badImpedSize);

  if(nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nLongSCs == LSpaceChargePointers.size())
    LSpaceChargePointers.resize(nLongSCs + 1);

  nNodes++;

  nodes(nNodes-1) = new FreqDepLimp(name, order, nLongBins,
                                    nTable, fTable, zTable, 
                                    b_a, useAvg, nMacrosMin,
                                    useSpaceCharge);
  nLongSCs ++;
  nFFTLongs ++;
  Ring::nLongUpdates++;
  LSpaceChargePointers(nLongSCs -1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LSpaceCharge::addFreqBetDepLimp
//
// DESCRIPTION
//   Adds a Longitudinal Space Charge Node
//
// PARAMETERS
//   name:    Name for the TM
//   order:   Order index
//   nBins    Number of bins to use
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LSpaceCharge::addFreqBetDepLimp(const String &name,
                                     const Integer &order,
                                     Integer &nbTable,
                                     Vector(Real) &bTable,
                                     Integer &nfTable,
                                     Vector(Real) &fTable,
                                     Matrix(Complex) &zTable,
                                     Real &b_a, Integer &useAvg,
                                     Integer &nMacrosMin,
                                     Integer &useSpaceCharge)
{
  if(bTable.rows() < nbTable) except(badImpedSize);
  if(fTable.rows() < nfTable) except(badImpedSize);

  zTable.resize(nbTable, nfTable);

  if(nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nLongSCs == LSpaceChargePointers.size())
    LSpaceChargePointers.resize(nLongSCs + 1);

  nNodes++;

  nodes(nNodes-1) = new FreqBetDepLimp(name, order, nLongBins,
                                       nbTable, bTable,
                                       nfTable, fTable,
                                       zTable, 
                                       b_a, useAvg, nMacrosMin,
                                       useSpaceCharge);
  nLongSCs ++;
  nFFTLongs ++;
  Ring::nLongUpdates++;
  LSpaceChargePointers(nLongSCs -1) = nodes(nNodes-1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LSpaceCharge::showLSpaceCharge
//
// DESCRIPTION
//   Show a longitudinal space charge info:
//
// PARAMETERS
//    
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LSpaceCharge::showLSpaceCharge(const Integer &n, Ostream &os)
{
  Integer i;

  if ((n & Particles::All_Mask) > nLongSCs) except(badLongSC);
  LongSC *lsc = LongSC::safeCast(LSpaceChargePointers((n & Particles::All_Mask)-1));

  os << "\t" << "Bin\t" << "Phi\t" << "Count\n";
  for (i=1; i<=lsc->_nBins; i++)
    os << i << "\t\t"
       << (-pi + (i-1)*twoPi/(lsc->_nBins)) << "\t\t"
       << lsc->_phiCount(i) << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LSpaceCharge::dumpKick
//
// DESCRIPTION
//   These routines dump LSpaceCharge info to vectors accesible from
//   the Shell
//
// PARAMETERS
//   n - the n'th L-space charge node.
//   herd = the herd to calculate kick with.
//   ang - the longitudinal phase angle to provide the kick at (rad).
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Real LSpaceCharge::dumpKick(const Integer &n, Real &ang)
// Dump the space charge kick per particle
{
  MacroPart *mp;
  if ((n & Particles::All_Mask) > nLongSCs) except (badLongSC);
  LongSC *lsc = LongSC::safeCast(LSpaceChargePointers((n & Particles::All_Mask) -1));
  return lsc->_kick(ang);
}

Void LSpaceCharge::dumpPhiCount(const Integer &n, Vector(Real) &p)
// Dump the binned distribution
{
  if ((n & Particles::All_Mask) > nLongSCs) except (badLongSC);
  LongSC *lsc = LongSC::safeCast(LSpaceChargePointers((n & Particles::All_Mask) -1));
  p.resize(lsc->_nBins);
  for (Integer i=1; i<= lsc->_nBins; i++)
    p(i) = lsc->_phiCount(i);
}

Void LSpaceCharge::dumpFFTStuff(const Integer &n, Vector(Real) &F1,
                                Vector(Real) &F2)
// Dump the FFT magnitude and phase vectors
{
  FFTLongSC *flsc;
  if ((n & Particles::All_Mask) > nLongSCs) except (badLongSC);
  LongSC *lsc = LongSC::safeCast(LSpaceChargePointers((n & Particles::All_Mask) -1));
  if (lsc->isKindOf(FFTLongSC::desc()) != 1 ) except(badFFTLSC);
  flsc = FFTLongSC::safeCast(LSpaceChargePointers((n & Particles::All_Mask) -1));
  F1.resize(lsc->_nBins);
  F2.resize(lsc->_nBins);
  for (Integer i=1; i<= flsc->_nBins; i++)
  {
    F1(i) = flsc->_FFTMagnitude(i);
    F2(i) = flsc->_FFTPhase(i);
  }
}

Void LSpaceCharge::dumpZImpedStuff(const Integer &n, Vector(Real) &z,
                                   Vector(Real) &chi)
// Dump the FFT magnitude and phase vectors
{
  if((n & Particles::All_Mask) > nLongSCs) except(badLongSC);
  LongSC *lsc = LongSC::safeCast(LSpaceChargePointers
                                 ((n & Particles::All_Mask) - 1));
  z.resize(lsc->_nBins);
  chi.resize(lsc->_nBins);

  if(lsc->isKindOf(FFTLongSC::desc()) == 1)
  {
    FFTLongSC *flsc;
    flsc = FFTLongSC::safeCast(LSpaceChargePointers
                               ((n & Particles::All_Mask) - 1));
    for (Integer i = 1; i <= flsc->_nBins; i++)
    {
      z(i)   = flsc->_z(i);
      chi(i) = flsc->_chi(i);
    }
  }
  else if(lsc->isKindOf(FreqDepLimp::desc()) == 1)
  {
    FreqDepLimp *flsc;
    flsc = FreqDepLimp::safeCast(LSpaceChargePointers
                                 ((n & Particles::All_Mask) - 1));
    for (Integer i = 1; i <= flsc->_nBins; i++)
    {
      z(i)   = flsc->_z(i);
      chi(i) = flsc->_chi(i);
    }
  }
  else if(lsc->isKindOf(FreqBetDepLimp::desc()) == 1)
  {
    FreqBetDepLimp *flsc;
    flsc = FreqBetDepLimp::safeCast(LSpaceChargePointers
                                    ((n & Particles::All_Mask) - 1));
    for (Integer i = 1; i <= flsc->_nBins; i++)
    {
      z(i)   = flsc->_z(i);
      chi(i) = flsc->_chi(i);
    }
  }
  else
  {
    except(badLongSC);
  }
}

Void LSpaceCharge::dump_LImped(const Integer &n, Ostream &os)
{
// Dump the Impedance Harmonics

  if((n & Particles::All_Mask) > nLongSCs) except(badLongSC);
  LongSC *lsc = LongSC::safeCast(LSpaceChargePointers
                                 ((n & Particles::All_Mask) - 1));

  os << "Mode, _zImped_n.re, _zImped_n.im" << "\n\n";

  if(lsc->isKindOf(FFTLongSC::desc()) == 1)
  {
    FFTLongSC *flsc;
    flsc = FFTLongSC::safeCast(LSpaceChargePointers
                               ((n & Particles::All_Mask) - 1));
    for (Integer i = 1; i <= flsc->_nBins; i++)
    {
      os << i                     << "    "
         << flsc->_zImped_n(i).re << "    "
         << flsc->_zImped_n(i).im << "\n";
    }
  }
  else if(lsc->isKindOf(FreqDepLimp::desc()) == 1)
  {
    FreqDepLimp *flsc;
    flsc = FreqDepLimp::safeCast(LSpaceChargePointers
                                 ((n & Particles::All_Mask) - 1));
    for (Integer i = 1; i <= flsc->_nBins; i++)
    {
      os << i                     << "    "
         << flsc->_zImped_n(i).re << "    "
         << flsc->_zImped_n(i).im << "\n";
    }
  }
  else if(lsc->isKindOf(FreqBetDepLimp::desc()) == 1)
  {
    FreqBetDepLimp *flsc;
    flsc = FreqBetDepLimp::safeCast(LSpaceChargePointers
                                    ((n & Particles::All_Mask) - 1));
    for (Integer i = 1; i <= flsc->_nBins; i++)
    {
      os << i                     << "    "
         << flsc->_zImped_n(i).re << "    "
         << flsc->_zImped_n(i).im << "\n";
    }
  }
  else
  {
    except(badLongSC);
  }
}
