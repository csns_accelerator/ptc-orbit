/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Collimator.mod
//
// AUTHOR
//    Sarah Cousineau, ORNL, scousine@sns.gov
//    Jeff Holmes, ORNL, jzh@sns.gov
//
// CREATED
//    11/1/00
//
//  DESCRIPTION
//    Module descriptor file for collimator file.
//
//  REVISION HISTORY
//    05/09/2001
//	
/////////////////////////////////////////////////////////////////////////////

module Collimator- "Collimator Module."
                 - "Written by Sarah, ORNL, scousine@ornl.gov"
{

  Inherits Consts, Particles, Ring, TransMap, TeaPot, Aperture;

Errors
  badHerdNo         - "Sorry, you asked for an undefined macro Herd",
  badTransMap - "Sorry, you asked for an undefined transfer map";

public:

  Void ctor() 
                  - "Constructor for the Collimate module."
                  - "Initializes build values.";

  Integer
   nCollimators   - "Number of apertures in the Ring";
	
  Void addCollimator(const String &name, const Integer &order, const  
		     Real &l, const Integer &ma, const Real &densityfac,
		     const Integer &shape, const Real &a, const Real &b, 
	 	     const Real &c, const Real &d, const Real &angle)   
  		  - "Routine to add a finite length collimator";
  
  Real ran1(Integer &idum)
  		  - "Routine to generate a random number between 0 and 1";

  Integer checkCollFlag(const Integer &shape, const Real &a, const Real &b,
			const Real &c, const Real &d, const Real &angle, 
			Real &x, Real &y)
		  - "Routine to check to see if a particle is inside a" 
		  - "collimator.  Returns 0 if the particle is not in the"
		  - "collimator, and 1 if it is.";	

  Void checkStep(Real &s, Real &stepsize, const Real &length, 
		 const Integer &shape, const Real &a, const
	   	 Real &b, const Real &c, const Real &d, 
		 const Real &angle, Real &x, Real &y,
		 Real &px, Real &py, Real &theta)
		  - "Routine to check the size of a randomly generated step";

  Void MCS(Real &stepsize, Real &theta,  Integer &idum, Real &p_o,
	   Real &delta_pos, Real &delta_mom)
	          - "Multiple coulomb scattering routine";

  Void MCSJackson(Real &stepsize, Real &Z, Real &A, Real &rho,
                  Integer &idum, Real &beta, Real &pfac,
                  Real &x, Real &y, Real &px, Real &py)
	          - "Multiple coulomb scattering routine"
                  - "following JD Jackson, Chapter 13";

  Real IonEnergyLoss(Real &beta, Real &Z, Real &A)
		  - "Ionization energy loss routine";

  Void getKick(Real &t, Real &p, Real & M, Real &dp_x, Real &dp_y, 
	       Integer &idum)
		  - "Routine to generate random, uniformly distributed"
		  - "2D momentum kicks";

  Real getRuth_t(Real &theta, Real &E, Real &p, Real &R, Real &Z,
	         Integer &idum, Integer &rflag)
		  - "Routine to generate a random momentum transfer"
		  - "for Coulomb/Rutherford scattering";

  Void getRuthJackson(Real &stepsize, Real &Z, Real &A, Real &rho,
                      Integer &idum, Real &beta, Integer &trackit,
                      Real &rcross, Real &thx, Real &thy)
                  - "Rutherford scattering routine"
                  - "following JD Jackson, Chapter 13";

  Real getElast_t(Real &p, Real &A, Integer &idum)
		  - "Routine to generate a random momentum transfer"
		  - "for low energy elastic scattering (<= .4 GeV)";

  Void dumpCollimatorHits(const Integer &n, Ostream &os)
		  - "Routine to dump histogram values of number of particles"
		  - "hitting each collimator node and number of particles"
		  - "lost";

public:

  // parallel stuff  ==MPI==  ==start===
  Integer
    iMPIini       - "inf. about MPI 0-non initialized 1-initialized";
  Integer
    nMPIrank      - "rank of this CPU";
  Integer
    nMPIsize      - "number of CPUs for Parallel run";
  // parallel stuff  ==MPI==  ==stop===

}
