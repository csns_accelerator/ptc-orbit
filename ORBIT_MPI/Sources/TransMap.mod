/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    TransMap.mod
//
// AUTHOR
//    John Galambos,  ORNL, jdg@ornl.gov
//    Jeff Holmes,  ORNL, jzh@ornl.gov
//
// CREATED
//    12/8/97
//
//  DESCRIPTION
//    Module descriptor file for the TransMap module. This module contains
//    Transfer Materix Information
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module TransMap - "TransMap Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, Particles, Ring;

Errors
  tuneNotReady - "The tune calculation is either not started, or done",
  badMADFile   - "Sorry, I can't find the MAD file you specified",
  badLATFile   - "Sorry, I can't find the LAT file you specified",
  badDIMADFile - "Sorry, I can't find the DIMAD file you specified",
  badDIMADFile2- "Sorry, I can't find a matrix in the DIMAD file",
  badHerd      - "You asked to manipulate a nonexistent herd",
  EOFTwiss     - "Oops - I'm at the end of the Twiss file. More TMs than"
               - "twiss sets. Check MAD files for consistency.",
  notAtRingStart- "You must be at the start of the ring for this operation",
  tunePartsBad  - "You added parts during a simple tune calc. Change "
                - " useSimpleTuneCalc to false and try again";
public:

  Void ctor() 
                  - "Constructor for the Transfer Matrix  module."
                  - "Initializes build values.";

  Integer
   nTransMaps       - "Number of Transfer Matrices in the Ring",
   nTransMats1      - "Number of first order Transfer Matrices",
   nTransMats2	    - "Number of second order Transfer Matrices",
   nFNALExacts      - "Number FNAL 'exact' transport nodes",
   nFNALMaps        - "Number FNAL map transport nodes";

  Void addTransferMatrix(const String &name, const Integer &order,
    const RealMatrix &R,  const Real &bx, const Real &by, 
    const Real &ax, const Real &ay, const Real &ex, const Real &epx, 
    const Real &l, const String &et, const Integer &matrep)
			- "Add a 1st order transfer matrix";

  Void addTransferMatrix2(const String &name, const Integer &order, 
    const RealMatrix &R,  const RealArray3 &T, const Real &bx, const Real &by, 
    const Real &ax, const Real &ay, const Real &ex, const Real &epx, 
    const Real &l, const String &et, const Integer &matrep)
			- "Add a 2nd order transfer matrix";

  Void readDIMADFile(const String &DIMADFile) 
  	 		- "Routine to read in matricies from DIMAD";

  Void readMADFile(const String &MADTwissFile,
                   const String &MADMatrixFile) 
  	 		- "Routine to read in twiss parameters from MAD";

  Void readMADFile2(const String &MADTwissFile,
                    const String &MADMatrixFile) 
  	 		- "Routine to read in twiss parameters from MAD";

  Void Uniform_LAT(String &LatType, Real &LengthTunes, 
                   Real &XTune, Real &YTune, 
                   Real &RhoInv, Integer &nElements)
                   - "Routine to define uniform focusing lattice";

  Void FNALMapLine(const String &MADFile, const String &MADLine,
                   const Integer &maporder, const Integer &herdno)
                   - "Routine to set up FNAL Beamline"
                   - "library transport map line";

  Void InitPTC(const String &PTC_File)
                   - "Routine to set up PTC code";

  Void readAccelerationTableForPTC(const String &PTC_acc_File)
                   - "Routine to read acceleration file for PTC";

  Void ReadPTCscript(const String &PTC_script_File)
                   - "Routine to read PTC commands from script file and to execute them";

  Void UpdateTwissFromPTC()
                   - "Updates ORBIT Twiss parameters from PTC"
                   - "It could be needed after acceleration or reading the PTC script";

  Void SynchronousSetPTC(const Integer &i_val)
                   - "Calls the ptc_synchronous_set method of the PTC code";

  Void SynchronousAfterPTC(const Integer &i_val)
                   - "Calls the ptc_synchronous_after method of the PTC code";


// Tune calculation stuff:

  Void startTuneCalc() - "Routine to initiate the particle tunes calculation "
                       - "during the transfer matrix update"; 
  Void startTuneCalcBase(Integer &herd) - "for specific herd";
  Void stopTuneCalc()  - "Routine to stop the particle tunes calculation "
                       - "during the transfer matrix update"; 
  Void stopTuneCalcBase(Integer &herd)  - "For specific herd";

  Integer 
  nTuneTurns        - "Counter for the number of tune turns done",
  nMacroTunes       - "Counter for number of parts being  tune calcculated",
  tuneCalcOn        - "Flag (if !=0) to indicate the tune calc status",
  useSimpleTuneCalc - "Switch to use a simple tune calculation";

}
