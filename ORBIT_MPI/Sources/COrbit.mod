/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    COrbit.mod
//
// AUTHOR
//    Steven Bunch,  University of Tennessee, bunchsc@sns.gov
//
// CREATED
//    7/3/02
//
//  DESCRIPTION
//    Module descriptor file for the Closed Orbit module.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module COrbit - "Closed Orbit Module."
                 - "Written by Steven Bunch, Univ of TN, bunchsc@sns.gov"
{
  Inherits Consts, Particles, Ring;

public:

  Void ctor()
                  - "Constructor for the Closed Orbit  module."
                  - "Initializes build values.";

  Void closedOrbit(Real &xi, Real &xpi, Real &yi,
                   Real &ypi, Real &dE, const Real &ctol)
                  - "Computes Closed Orbit Solution";
}
