/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Parallel_MPI.cc
//
// CREATED
//    03/21/02
//
//  DESCRIPTION
//    Provides interface for the MPI functions
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "mpi.h"
#include "Parallel_MPI.h"
#include "RealMat.h"

#include <iostream>
///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE Parallel
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Parallel_MPI::ctor
//
// DESCRIPTION
//    Initializes the various Parallel related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Parallel_MPI::ctor()
{
// set some initial values
}

Void Parallel_MPI::dtor()
{
// set some initial values
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Parallel_MPI::MPI_size
//
// DESCRIPTION
//    Returns number of CPUs for a parallel run.
//
// PARAMETERS
//    Nothing.
//
// RETURNS
//    N CPUs.
//
///////////////////////////////////////////////////////////////////////////

Integer Parallel_MPI::MPI_size()
{
    int numprocs = 1;
    int i_flag;
    MPI_Initialized(&i_flag);

    if(i_flag){
      MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    }
    return numprocs;
}

Integer Parallel_MPI::MPI_rank()
{
    int nRank = 0;
    int i_flag;
    MPI_Initialized(&i_flag);

    if(i_flag){
      MPI_Comm_rank(MPI_COMM_WORLD,&nRank);
    }
    return nRank;
}

Void Parallel_MPI::MPI_Get_CPU_name(String& CPU_name0)
{
    int  namelen;    
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name,&namelen);
    String CPU_name(processor_name,namelen);
    CPU_name0 = CPU_name;
}

Integer Parallel_MPI::MPI_Allreduce_MAX_Integer(Integer& i_in0)
{
  int i_in,i_out;
  i_in=i_in0;

  int i_flag;
  MPI_Initialized(&i_flag);

   if(i_flag){
     MPI_Allreduce(&i_in,&i_out,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
   }
   else{
     i_out=i_in;
   }

  return i_out;
}

Integer Parallel_MPI::MPI_Allreduce_SUM_Integer(Integer& i_in0)
{
  int i_in,i_out;
  i_in=i_in0;

  int i_flag;
  MPI_Initialized(&i_flag);

   if(i_flag){
     MPI_Allreduce(&i_in,&i_out,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
   }
   else{
     i_out=i_in;
   }

  return i_out;
}

Real Parallel_MPI::MPI_Allreduce_MAX_Real(Real& d_in0)
{
  double d_in,d_out;
  d_in=d_in0;

  int i_flag;
  MPI_Initialized(&i_flag);

   if(i_flag){
     MPI_Allreduce(&d_in,&d_out,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
   }
   else{
     d_out=d_in;
   }

  return d_out;
}

Real Parallel_MPI::MPI_Allreduce_SUM_Real(Real& d_in0)
{
  double d_in,d_out;
  d_in=d_in0;

  int i_flag;
  MPI_Initialized(&i_flag);
   if(i_flag){
     MPI_Allreduce(&d_in,&d_out,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
   }
   else{
     d_out=d_in;     
   }
  return d_out;
}

Void Parallel_MPI::MPI_Barrier0()
{
  int result;

  int i_flag;
  MPI_Initialized(&i_flag);
   if(i_flag){
     result = MPI_Barrier(MPI_COMM_WORLD);
   }
}

Real Parallel_MPI::MPI_Wtime0()
{
  double timeMPI;

  int i_flag;
  MPI_Initialized(&i_flag);
   if(i_flag){
     timeMPI = MPI_Wtime();
   }
   else{
     timeMPI=0.;
   }
  return timeMPI;
}

Integer Parallel_MPI::MPI_Initialized0()
{
    int i_flag;
    MPI_Initialized(&i_flag);
    return i_flag;
}


Void Parallel_MPI::MPI_String_Add_Integer(String& S, Integer& iRank)
{
  char ch[10];
  sprintf(ch,"%d",iRank);
  S=S+ch;
}

Void Parallel_MPI::MPI_test_1_my()
{
  MPI_Status status;
  Vector(Real) s;
  int Nval = 10000;
  s.resize(Nval);
  int i,j;

  int nRank;
  MPI_Comm_rank(MPI_COMM_WORLD,&nRank);
  int numprocs;
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);

  if( nRank == 0){
    for(i=0; i<Nval; i++){s(i+1)=i+1;};
    for(j=0; j<numprocs; j++){
     MPI_Send( (double *) &s.data(), Nval, MPI_DOUBLE,j,111+j, MPI_COMM_WORLD);
    }
  } 
  if( nRank != 0){
    MPI_Recv( (double *) &s.data(), Nval, MPI_DOUBLE,0,111+nRank, MPI_COMM_WORLD,&status );
    std::cout<<"rank="<<nRank <<" should be ="<<Nval<<" s="<<s(Nval)<<std::endl;    
  }
}
