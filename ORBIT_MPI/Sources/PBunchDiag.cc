/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    PBunchDiag.mod
//
// AUTHOR
//   Y. Sato, A. Shishlo
//   
//
// CREATED
//   01/14/2004
//
// DESCRIPTION
//   Module descriptor file for the PBunchDiag module. This module contains
//   diagnostic methods for p-bunch diagnostics.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Particles.h"
#include "MacroPart.h"
#include "Node.h"
#include "MapBase.h"
#include "Ring.h"

#include "PBunchDiag.h"
#include "PBunchDiagNode.hh"
#include "PBunchDiagNodeStore.hh"

#include <iostream>
#include <cstdlib>

extern Array(ObjectPtr) mParts;
extern Array(ObjectPtr) nodes;

Void PBunchDiag::ctor()
{
   
}

Void PBunchDiag::dtor()
{
    
}

//Creates pBunchDiag Node and returns its index
Integer PBunchDiag::PBunchDiagMakeNode(const Integer& order,
				       const Integer&  zSlices,
				       const Integer&  nRecordsMax, 
				       const String& diagTypeName,
				       const String& fileName)
{
  char str_ind[14];
  PBunchDiagNodeStore* diagStore = PBunchDiagNodeStore::getPBunchDiagNodeStore();
  int ind = diagStore->getSize();
  sprintf(str_ind,"(%d)", ind); 

  String wname("pBDiag");
  wname = wname + str_ind;

  Integer diagType = -1;

  if(diagTypeName == "LONG_DENSITY"    )   diagType = 0;
  if(diagTypeName == "LONG_DENSITY_FFT")   diagType = 1;
  if(diagTypeName == "CENTROID_X"      )   diagType = 2;
  if(diagTypeName == "CENTROID_X_FFT"  )   diagType = 3;
  if(diagTypeName == "CENTROID_Y"      )   diagType = 4;
  if(diagTypeName == "CENTROID_Y_FFT"  )   diagType = 5;
  if(diagTypeName == "SQRT_AVG_R2"     )   diagType = 6;
  if(diagTypeName == "SQRT_AVG_R2_FFT" )   diagType = 7;
  if(diagTypeName == "QUAD"            )   diagType = 8;
  if(diagTypeName == "QUAD_FFT"        )   diagType = 9;
  if(diagTypeName == "CENTROID_XW_FFT" )   diagType = 10;
  if(diagTypeName == "CENTROID_YW_FFT" )   diagType = 11;
  if(diagTypeName == "SQRT_AVG_X2"     )   diagType = 12;
  if(diagTypeName == "SQRT_AVG_Y2"     )   diagType = 13;
  if(diagTypeName == "SQRT_AVG_X2_FFT" )   diagType = 14;
  if(diagTypeName == "SQRT_AVG_Y2_FFT" )   diagType = 15;
  if(diagTypeName == "ASRX2plusASRY2"    )   diagType = 16;
  if(diagTypeName == "ASRX2minsASRY2"    )   diagType = 17;
  if(diagTypeName == "ASRX2plusASRY2_FFT")   diagType = 18;
  if(diagTypeName == "ASRX2minsASRY2_FFT")   diagType = 19;

  if(diagType < 0)except(badpBunchDiagType); 



  PBunchDiagNode* pbdNode = new PBunchDiagNode(wname,
					       order,
					       zSlices,
					       nRecordsMax,
					       diagType,
					       diagTypeName.cptr(),
					       fileName.cptr());
  Integer indI = diagStore->addPBunchDiagNode(pbdNode);

  if (Ring::nNodes == nodes.size()) nodes.resize(Ring::nNodes + 100);
  nodes(Ring::nNodes) = pbdNode;
  Ring::nNodes++;

  return indI;
}

//Stops recording for specific node with index nodeIndex
Void PBunchDiag::PBunchDiagStart(const Integer& nodeIndex)
{
  PBunchDiagNodeStore* diagStore = PBunchDiagNodeStore::getPBunchDiagNodeStore();
  PBunchDiagNode* pbdNode = diagStore->getPBunchDiagNode(nodeIndex);
  pbdNode->startRecording();
}

//Stops recording for specific node with index nodeIndex.
Void PBunchDiag::PBunchDiagStop(const Integer& nodeIndex)
{
  PBunchDiagNodeStore* diagStore = PBunchDiagNodeStore::getPBunchDiagNodeStore();
  PBunchDiagNode* pbdNode = diagStore->getPBunchDiagNode(nodeIndex);
  pbdNode->stopRecording();
}

//Dumps diagnostics for specific node with index nodeIndex.
Void PBunchDiag::PBunchDiagDump(const Integer& nodeIndex)
{
  PBunchDiagNodeStore* diagStore = PBunchDiagNodeStore::getPBunchDiagNodeStore();
  PBunchDiagNode* pbdNode = diagStore->getPBunchDiagNode(nodeIndex);
  pbdNode->dumpDiagnostics();
}

//Dumps maximal values for specific node with index nodeIndex.
Void PBunchDiag::PBunchDiagDumpMax(const Integer& nodeIndex)
{
  PBunchDiagNodeStore* diagStore = PBunchDiagNodeStore::getPBunchDiagNodeStore();
  PBunchDiagNode* pbdNode = diagStore->getPBunchDiagNode(nodeIndex);
  pbdNode->dumpMaxDiagnostics(); 
}

////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========START===============
////////////////////////////////////////////////////////////////////////////////

//Performs diagnostics immediately and dump results.
Void PBunchDiag::PBunchDiagPerform(const Integer& nodeIndex)
{
  PBunchDiagNodeStore* diagStore = PBunchDiagNodeStore::getPBunchDiagNodeStore();
  PBunchDiagNode* pbdNode = diagStore->getPBunchDiagNode(nodeIndex);

  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  pbdNode->startRecording();
  pbdNode->_updatePartAtNode(*mp); 
  pbdNode->stopRecording();
}

//Returns the bunch factor for longitudinal diagnostics. For all other types it will
//return 1.
Real PBunchDiag::PBunchDiagGetBunchFactor(const Integer& nodeIndex)
{
	PBunchDiagNodeStore* diagStore = PBunchDiagNodeStore::getPBunchDiagNodeStore();
  PBunchDiagNode* pbdNode = diagStore->getPBunchDiagNode(nodeIndex);
	return pbdNode->getBunchFactor();
}

////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========STOP================
////////////////////////////////////////////////////////////////////////////////
