#if !defined(__SCEllipt__)
#define __SCEllipt__

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <mpi.h>
#include <vector>
#include <complex>

#include "Object.h"
#include "MacroPart.h"

using namespace std;


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   ElliptCalculator
//
// INHERITANCE RELATIONSHIPS
//   None
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   Class for calculating space charge kicks
//   for uniform elliptical charge distributions including
//   a conducting wall boundary
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class ElliptCalculator : public Object
{
  Declare_Standard_Members(ElliptCalculator, Object);

  public:

  // Constructor

  ElliptCalculator(int nZBins, double ZLength,
                   int BPPoints, int BPModes,
                   double BPrnorm, double BPResid,
                   double* BPx , double* BPy,
                   double** BPPhiH1, double** BPPhiH2) :
                   _nZBins(nZBins), _ZLength(ZLength),
                   _BPPoints(BPPoints), _BPModes(BPModes),
                   _BPrnorm(BPrnorm), _BPResid(BPResid),
                   _BPx(BPx), _BPy(BPy),
                   _BPPhiH1(BPPhiH1), _BPPhiH2(BPPhiH2)
  {
    _ZDist = new double[_nZBins];
    _doBoundary = 0;
    _BPCoeffs = new double[2 * _BPModes + 1];
  }

  // Destructor

  ~ElliptCalculator()
  {
    delete _ZDist;
    delete _BPCoeffs;
  }

  // Variables and Methods

  int _nZBins;
  double _ZLength;
  double _ZCenter;
  double* _ZDist;

  int _doBoundary;
  int _BPPoints;
  int _BPModes;
  double _BPrnorm;
  double _BPResid;
  double* _BPx;
  double* _BPy;
  double** _BPPhiH1;
  double** _BPPhiH2;
  double* _BPCoeffs;

  double _xc;
  double _xsig;
  double _pxc;
  double _pxsig;
  double _xpxcor;
  double _xemit;
  double _xalpha;
  double _xbeta;
  double _yc;
  double _ysig;
  double _pyc;
  double _pysig;
  double _ypycor;
  double _yemit;
  double _yalpha;
  double _ybeta;
  double _a;
  double _b;
  double _c;
  double _asq;
  double _bsq;
  double _csq;

  void _SetDistribution(MacroPart& mp);

  double _getu(double x, double y);
  double _gettheta(double x, double y);

  void _BoundaryCoeffs();

  void _ApplyForce(MacroPart& mp, double SCKick_Coeff);

  void _ClearEllipt();
};


#endif   // __SCEllipt__
