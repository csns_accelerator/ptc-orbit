/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    TSpaceCharge.mod
//
// AUTHOR
//    John Galambos,  ORNL, jdg@ornl.gov
//    A. Luccio,  BNL
//    J. Beebe-Wang
//    J. Holmes (4/2001)
//
// CREATED
//    5/20/98 
//    4/2001 conducting wall beam pipe routines added 
//
//  DESCRIPTION
//    Module descriptor file for the TSpaceCharge module. This module contains
//    information about Transverse Space Charge.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module TSpaceCharge - "TSpaceCharge Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, MathLib, Particles, Ring, TransMap, Injection, LSpaceCharge;

Errors
  badTransSC    - "Sorry, you asked for a non existent Trans. space charge",
  tooFewTransMaps   - "Sorry, you need more Transfer Matricies before you"
                - "Add transverse space charge kicks";

public:

  Void ctor() 
                  - "Constructor for the Long. Space Charge module."
                  - "Initializes build values.";

  Integer
   nTransSCs    - "Number of Transverse Space Charge Nodes in the Ring";

  Void addBFTransSC(const String &n, const Integer &o,  Integer &nXbin,
                       Integer  &nYBins, Real &eps, Real &length,
                       const Integer &nMacrosMin, Real &gf)
                  - "Routine to add a brute force PIC Transverse"
                  - "Space charge node";
  Void addBFTransSCSet(Integer &nxb, Integer &nyb, Real &eps, 
                   const Integer &nMacrosMin, Real &gf)
                  - "Routine to build a set of BFPICTransSC"
                  - "nodes for each transfer matrix element present";

  Void addFFTTransSC(const String &n, const Integer &o,  Integer &nXbin,
                       Integer  &nYBins, Real &eps, Real &length,
                       const Integer &nMacrosMin)
                  - "Routine to add an FFT PIC Transverse"
                  - "Space charge node";
  Void addFFTTransSCSet(Integer &nxb, Integer &nyb, Real &eps, 
                   const Integer &nMacrosMin)
                  - "Routine to build a set of FFTTransSC"
                  - "nodes for each transfer matrix element present";

  Void addPWSTransSC(const String &n, const Integer &o, 
                       Real &eps, Real &length, const Integer &nMacrosMin)
                  - "Routine to add a pair-wise sum Trans. space charge node";
  Void addPWSTransSCSet( Real &eps, const Integer &nMacrosMin)
                  - "Routine to add a set of pair-wise sum Transverse "
                  - "space charge nodes, 1 for each transfer matrix present.";

  Void addPotentialTransSC(const String &n, const Integer &o,
                       Integer &nXbins, Integer  &nYBins,
                       Real &eps, Real &length,
                       const String &BPShape, Real &BP1,
                       Real &BP2, Real &BP3, Real &BP4,
                       Integer &BPPoints, Integer &BPModes,
                       Real &Gridfact,
                       const Integer &nMacrosMin)
                  - "Routine to add an FFT PIC Transverse Space"
                  - "Charge node with a conducting wall beam pipe";
  Void addPotentialTransSCSet(Integer &nxb, Integer &nyb, Real &eps,
                       const String &BPShape, Real &BP1,
                       Real &BP2, Real &BP3, Real &BP4,
                       Integer &BPPoints, Integer &BPModes,
                       Real &Gridfact,
                       const Integer &nMacrosMin)
                  - "Routine to build a set of PotentialTransSC"
                  - "nodes for each transfer matrix element present";

  Void addFMMTransSC(const String &n, const Integer &o,
                     Integer &nTerms, Integer &nLevels,
                     Integer &nZBins, Real &eps, Real &length,
                     const String &BPShape,
                     Real &BP1, Real &BP2, Real &BP3, Real &BP4,
                     Integer &BPPoints, Integer &BPModes,
                     Real &BPResid, Real &Gridfact,
                     const Integer &nMacrosMin)
                  - "Routine to add an FMM Transverse Space"
                  - "Charge node with a conducting wall beam pipe";
  Void addFMMTransSCSet(Integer &nTerms, Integer &nLevels,
                        Integer &nZBins, Real &eps,
                        const String &BPShape,
                        Real &BP1, Real &BP2, Real &BP3, Real &BP4,
                        Integer &BPPoints, Integer &BPModes,
                        Real &BPResid, Real &Gridfact,
                        const Integer &nMacrosMin)
                  - "Routine to build a set of FMMTransSC"
                  - "nodes for each lattice element present";

  Void addElliptTransSC(const String &n, const Integer &o,
                        Integer &nZBins, Real &length,
                        const String &BPShape,
                        Real &BP1, Real &BP2, Real &BP3, Real &BP4,
                        Integer &BPPoints, Integer &BPModes,
                        Real &BPResid,
                        const Integer &nMacrosMin)
                  - "Routine to add an Ellipt Transverse Space"
                  - "Charge node with a conducting wall beam pipe";
  Void addElliptTransSCSet(Integer &nZBins,
                           const String &BPShape,
                           Real &BP1, Real &BP2, Real &BP3, Real &BP4,
                           Integer &BPPoints, Integer &BPModes,
                           Real &BPResid,
                           const Integer &nMacrosMin)
                  - "Routine to build a set of ElliptTransSC"
                  - "nodes for each lattice element present";

  Void addCylCoordsTransSC(const String &n, const Integer &o,
                           Integer &nZBins, Integer &nRBins,
                           Integer &nThetas, Integer &nModes,
                           Real &RBnd, Integer &fInterp,
                           Real &length,
                           const Integer &nMacrosMin)
                  - "Routine to add a CylCoords Transverse Space Charge"
                  - "node with a circular conducting wall beam pipe";
  Void addCylCoordsTransSCSet(Integer &nZBins, Integer &nRBins,
                              Integer &nThetas, Integer &nModes,
                              Real &RBnd, Integer &fInterp,
                              const Integer &nMacrosMin)
                  - "Routine to build a set of CylCoordsSC"
                  - "nodes for each lattice element present";

  Void showTransSC( const Integer &n, Ostream &os)
                  - "Routine to show a Transverse space charge node info";

  Void showPotSC( const Integer &n, Ostream &os)
                  - "Routine to show a Transverse space charge node info,"
                  - "including potential";

  Void dumpFSC(const Integer &n, RealMatrix &mx, RealMatrix &my)
                  - "Dump the X and Y transverse space charge fields";
  Void dumpXYGrid(const Integer &n, RealVector &x, RealVector &y)
                  - "Dump the X and Y transverse space grids";
  Void dumpPotSC(const Integer &n, RealMatrix &Pot)
                  - "Dump the transverse space charge potential";
          
}
