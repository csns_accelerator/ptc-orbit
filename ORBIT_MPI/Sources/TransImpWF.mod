/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TransImpWF.mod
//
// CREATED
//   07/23/2002
//
// DESCRIPTION
//   Module descriptor file for the Transverse Impedance (Wake Function) module.
//   This module contains information about Transverse Impedance module based
//   on Mike Blaskiewicz's aproach (Wake Function for resonatn elements).
//   It was implemented as parallel module and needs MPI library.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module TransImpWF - "The Transverse Impedance (Wake Function) module."
                         - "Written by Andrei Shishlo"
{

Inherits Consts, Particles, Ring, TransMap, Injection;

Errors
  badHerdNo             - "Sorry, you asked for an undefined macro Herd",
  badTIelemIndex          - "Sorry, there is no TI element with this index";


public:

  Void ctor()
                        - "Constructor for the TransImpWF module."
                        - "Initializes build values.";

  Void dtor()
                        - "Destructor for the TransImpWF module."
                        - "Finalize the SpaceCharge3D_MPI module";

  Integer addTImpedanceWF( const Integer &posIndex, const Integer &nBins)
                        - "Adds one Transverse Impedance (WF) Node at posIndex position.";

  Void addResElemToTImpedanceWF(const Integer &TImpWFelem,const String &axis, Real &R, Real &Q, Real &Freq)
                        - "Adds a resonant element to the TrImp (WF) Node with number TImpWFelem";

  Void addElemToTImpedanceWF(const Integer &TImpWFelem,const String &axis, Real &W0re, Real &W0im, Real &ETAre, Real &ETAim)
                        - "Adds a arbitrary element to the TrImp (WF) Node with number TImpWFelem";

  Void setRevPeriodToTImpedanceWF(const Integer &TImpWFelem,const Real &tRev)
                         - "Sets the revolution period to the particular TWF node. The value in seconds.";

//-------------debugging methods-----------start-------------------------

  Void TransImpWFshowXY(Integer &TImpWFelem, String &file_name)
                        - "Debugging method."
                        - "Print out the <x> and <y> momenta of bunch.";

  Void TransImpWFshowTKick(Integer &TImpWFelem, String &file_name)
                        - "Debugging method."
                        - "Print out the x and y kick momentum of particles in the bunch.";

  Void TransImpWFpropagate(Integer &TImpWFelem, Real &time)
                        - "Debugging method."
                        - "Propagates herd through the TI_WF Node.";

//-------------debugging methods-----------stop-------------------------


private:

  Integer
    iMPIini_       - "inf. about MPI 0-non initialized 1-initialized";
  Integer
    nMPIrank_      - "rank of this CPU";
  Integer
    nMPIsize_      - "number of CPUs for Parallel run";

}

