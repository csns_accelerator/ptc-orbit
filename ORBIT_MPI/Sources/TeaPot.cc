/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TeaPot.cc
//
// AUTHOR
//   Joshua Abrams, Knox College, jabrams@knox.edu
//   Steven Bunch, University of Tennessee, sbunch2@utk.edu
//
// CREATED
//   09/09/02
//
// MODIFIED
//   03/24/03
//
// DESCRIPTION
//   Stacks thick-thin sequence to emulate long elements
//
// REVISION HISTORY
//   Added Multipole functions for Multipoles, Quadrupoles, and Bends
//
/////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// include files
//
///////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "StreamType.h"
#include "StrClass.h"
#include "TeaPot.h"
#include "ThinLens.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include "Consts.h"
#include "Accelerate.h"

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// Static Definitions
//
///////////////////////////////////////////////////////////////////////////

Array(ObjectPtr) TPPointers;
extern Array(ObjectPtr) syncP, mParts, nodes, tMaps;

// Granularity to allocate entries in work arrays

const Integer Node_Alloc_Chunk = 10;

///////////////////////////////////////////////////////////////////////////
//
// Local Functions:
//
///////////////////////////////////////////////////////////////////////////

Integer TPfactorial(Integer n)
{
   return (n>1) ? n*TPfactorial(n-1) :1;
}

Void TPfindPhases(MapBase &tm, MacroPart& mp, const Integer &i);
Void TPdoTunes(MapBase &tm, MacroPart &mp);

Real TPxPhase, TPyPhase;

Void rotatexy(MacroPart &mp, Real &anglexy);

Void drifti(MacroPart &mp, Integer &i, Real &length);
Void drift(MacroPart &mp, Real &length);

Void kick(MacroPart &mp, Real &kx, Real &ky, Real &kE);

Void multp(MacroPart &mp, const Integer &pole, Real &kl,
          const Integer &skew);
Void multpfringeIN(MacroPart &mp, const Integer &pole, Real &kl,
                   const Integer &skew);
Void multpfringeOUT(MacroPart &mp, const Integer &pole, Real &kl,
                    const Integer &skew);

Void quad1(MacroPart &mp, Real &length, Real &kq);
Void quad2(MacroPart &mp, Real &length);
Void quadfringeIN(MacroPart &mp, Real &kq);
Void quadfringeOUT(MacroPart &mp, Real &kq);

Void wedgerotate(MacroPart &mp, Real &e, Integer &frinout);
Void wedgedrift(MacroPart &mp, Real &e, Integer &inout);
Void wedgebend(MacroPart &mp, Real &e, Integer &inout,
               Real &rho, Integer &nsteps);
Void bend1(MacroPart &mp, Real &length, Real &th);
Void bend2(MacroPart &mp, Real &length);
Void bend3(MacroPart &mp, Real &th);
Void bend4(MacroPart &mp, Real &th);
Void bendfringeIN(MacroPart &mp, Real &rho);
Void bendfringeOUT(MacroPart &mp, Real &rho);

Void soln(MacroPart &mp, Real &length, Real &B);

Void wedgebendCF(MacroPart &mp, Real &e, Integer &inout,
                 Real &rho,
                 Integer &vecnum,
                 Vector(Integer) &pole,
                 Vector(Real) &kl,
                 Vector(Integer) &skew,
                 Integer &nsteps);

Matrix(Real) MAT_Identity();
Matrix(Real) MAT_Tilt(Real &tilt, Matrix(Real) &Min);
Matrix(Real) MAT_Drift(Real &length, Real &gamma);
Matrix(Real) MAT_Thin(Real &kln, Real &kls);
Matrix(Real) MAT_Quad(Real &length, Real &gamma, Real &kq);
Matrix(Real) MAT_Bend(Real &length, Real &gamma, Real &theta,
                      Real &kq, Real &ea1, Real &ea2);
Matrix(Real) MAT_Soln(Real &length, Real &gamma, Real &B);
Matrix(Real) MAT_Mult(Integer &m, Integer &l, Integer &n,
                      Matrix(Real) &A, Matrix(Real) &B);
Matrix(Real) MAT_TW(Matrix(Real) &M);

Matrix(Real) X_Identity();
Matrix(Real) X_Drift(Real &length, Real &gamma);
Matrix(Real) X_Thin(Real &kln, Real &kls);
Matrix(Real) X_Quad(Real &length, Real &gamma, Real &kq);
Matrix(Real) X_Bend(Real &length, Real &gamma, Real &theta,
                    Real &kq, Real &ea1, Real &ea2);
Matrix(Real) X_Soln(Real &length, Real &gamma, Real &B);

Matrix(Real) Y_Identity();
Matrix(Real) Y_Drift(Real &length, Real &gamma);
Matrix(Real) Y_Thin(Real &kln, Real &kls);
Matrix(Real) Y_Quad(Real &length, Real &gamma, Real &kq);
Matrix(Real) Y_Bend(Real &length, Real &gamma, Real &theta,
                    Real &kq, Real &ea1, Real &ea2);
Matrix(Real) Y_Soln(Real &length, Real &gamma, Real &B);
Real findPhase(Real &x, Real &xp, Real &delta,
               Real &beta, Real &alpha, Real &eta, Real &etap);

Void WaveForm00();
Void WaveForm01();
Void WaveForm02();
Void WaveForm03();
Void WaveForm04();
Void WaveForm05();
Void WaveForm06();
Void WaveForm07();
Void WaveForm08();
Void WaveForm09();
Void WaveForm10();
Void WaveForm11();
Void WaveForm12();
Void WaveForm13();
Void WaveForm14();
Void WaveForm15();
Void WaveForm16();
Void WaveForm17();
Void WaveForm18();
Void WaveForm19();


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPBase
//
// INHERITANCE RELATIONSHIPS
//   TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a base class for storing TeaPot Node information.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPBase : public MapBase
{

  Declare_Standard_Members(TPBase, MapBase);

public:
    TPBase(const String &n, const Integer &order,
           const Real &bx, const Real &by,
	       const Real &ax, const Real &ay,
           const Real &ex, const Real &epx,
           const Real &l, const String &et):
    MapBase(n, order, bx, by, ax, ay, ex, epx, l, et)
    {
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Void _showTP(Ostream &sout)=0;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPBase, MapBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPD
//
// INHERITANCE RELATIONSHIPS
//   TPD  -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a drift.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   nsteps(int)--> _nsteps      : Number of drift steps
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPD : public TPBase
{

  Declare_Standard_Members(TPD, TPBase);

  public:

  TPD(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Integer &nsteps):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _nsteps(nsteps)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Integer _nsteps;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPD
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPD, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPD::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPD::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;

  TeaPot::rhoInv = 0.0;

  TeaPot::transportMatrix = MAT_Drift(length, gamma);
  TeaPot::xMatrix         =   X_Drift(length, gamma);
  TeaPot::yMatrix         =   Y_Drift(length, gamma);

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPD *tm = TPD::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPD::_showTP(Ostream &sout)
{
  sout << " Drift " << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of Drift = " << _length << "\n";
  sout << " Number of slices = " << _nsteps << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPD::updatePartAtNode
//
// DESCRIPTION
//   Drifts the particles
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPD::_updatePartAtNode(MacroPart& mp)
{
  Integer k, nsteps;
  Real length;

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  nsteps = _nsteps;
  if(_nsteps < 1) nsteps = 1;

  length = _length / nsteps;

  for (k = 0; k < nsteps; k++)
  {
    drift(mp, length); 
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);

}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPM
//
// INHERITANCE RELATIONSHIPS
//   TPM -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a multipole with length.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   pole (int) --> _pole        : Multipole order.
//                  		       0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Integrated strength (1/m^pole).
//                                 This is the integral of
//                  		       "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Switch to use a skew element.
//                                 If == 1, the thin lens is rotated
//                                 by pi/(2*n + 2)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of thin multipole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPM : public TPBase
{

  Declare_Standard_Members(TPM, TPBase);

  public:

  TPM(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &tilt,
      const Integer &pole, const Real &kl,
      const Integer &skew,
      const Subroutine sub, const Integer &nsteps,
      const Integer &fringeIN, const Integer &fringeOUT):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _tilt(tilt), _pole(pole), _kl(kl), _skew(skew), _sub(sub),
  _nsteps(nsteps), _fringeIN(fringeIN), _fringeOUT(fringeOUT)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Subroutine _sub;
  Integer _pole, _skew, _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _kl;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPM
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPM, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPM::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPM::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;

  TeaPot::rhoInv = 0.0;

  if(_length < 0.0)
  {
    TeaPot::transportMatrix = MAT_Identity();
    TeaPot::xMatrix         =   X_Identity();
    TeaPot::yMatrix         =   Y_Identity();
    return;
  }

  _sub();

  Real kls = 0.0;
  Real kln = 0.0;

  if(_pole == 1)
  {
    if(_skew) kls = _kl * TeaPot::strength;
    else      kln = _kl * TeaPot::strength;
  }

  if((Abs(kln) <= Consts::tiny) && (Abs(kls) <= Consts::tiny))
  {
    TeaPot::transportMatrix = MAT_Drift(length, gamma);
    TeaPot::xMatrix         =   X_Drift(length, gamma);
    TeaPot::yMatrix         =   Y_Drift(length, gamma);
    return;
  }

  Matrix(Real)  M,  TEMP1,  TEMP2;
  Matrix(Real) MX, TEMP1X, TEMP2X;
  Matrix(Real) MY, TEMP1Y, TEMP2Y;
  M.resize(6,6);
  TEMP1.resize(6,6);
  TEMP2.resize(6,6);
  MX.resize(3,3);
  TEMP1X.resize(3,3);
  TEMP2X.resize(3,3);
  MY.resize(2,2);
  TEMP1Y.resize(2,2);
  TEMP2Y.resize(2,2);

  if(Abs(_length) <= Consts::tiny)
  {
    M  = MAT_Thin(kln, kls);
    MX =   X_Thin(kln, kls);
    MY =   Y_Thin(kln, kls);
  }
  else
  {
    Integer nsteps = _nsteps;
    if(_nsteps < 1) nsteps = 1;

    length = _length / nsteps;
    Real lengtho2 = length / 2.0;
    kls /= nsteps;
    kln /= nsteps;
    Integer m  = 6, l  = 6, n  = 6;
    Integer mx = 3, lx = 3, nx = 3;
    Integer my = 2, ly = 2, ny = 2;

    M  = MAT_Drift(lengtho2, gamma);
    MX =   X_Drift(lengtho2, gamma);
    MY =   Y_Drift(lengtho2, gamma);

    for (Integer k = 1; k < nsteps; k++)
    {
      TEMP1  = MAT_Thin(kln, kls);
      TEMP1X =   X_Thin(kln, kls);
      TEMP1Y =   Y_Thin(kln, kls);      
      TEMP2  = MAT_Mult( m,  l,  n,  TEMP1,  M);
      TEMP2X = MAT_Mult(mx, lx, nx, TEMP1X, MX);
      TEMP2Y = MAT_Mult(my, ly, ny, TEMP1Y, MY);
      TEMP1  = MAT_Drift(length, gamma);
      TEMP1X =   X_Drift(length, gamma);
      TEMP1Y =   Y_Drift(length, gamma);
      M      = MAT_Mult( m,  l,  n,  TEMP1,  TEMP2);
      MX     = MAT_Mult(mx, lx, nx, TEMP1X, TEMP2X);
      MY     = MAT_Mult(my, ly, ny, TEMP1Y, TEMP2Y);
    }

    TEMP1  = MAT_Thin(kln, kls);
    TEMP1X =   X_Thin(kln, kls);
    TEMP1Y =   Y_Thin(kln, kls);    
    TEMP2  = MAT_Mult( m,  l,  n,  TEMP1,  M);
    TEMP2X = MAT_Mult(mx, lx, nx, TEMP1X, MX);
    TEMP2Y = MAT_Mult(my, ly, ny, TEMP1Y, MY);
    TEMP1  = MAT_Drift(lengtho2, gamma);
    TEMP1X =   X_Drift(lengtho2, gamma);
    TEMP1Y =   Y_Drift(lengtho2, gamma);
    M      = MAT_Mult( m,  l,  n,  TEMP1,  TEMP2);
    MX     = MAT_Mult(mx, lx, nx, TEMP1X, TEMP2X);
    MY     = MAT_Mult(my, ly, ny, TEMP1Y, TEMP2Y);
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    Real tilt = _tilt;
    TEMP1 = MAT_Tilt(tilt, M);
    M = TEMP1;
    if(Abs(_tilt) > Consts::pi / 8.0)
    {
      length = _length;
      MX = X_Drift(length, gamma);
      MY = Y_Drift(length, gamma);
    }
  }

  TeaPot::transportMatrix = M;
  TeaPot::xMatrix         = MX;
  TeaPot::yMatrix         = MY;

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPM *tm = TPM::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPM::_showTP(Ostream &sout)
{
  sout << " TeaPot Multipole Lens:" << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Multipole order is =  " << _pole << "\n";
  sout << " Length of lens = " << _length << "\n";
  _sub();
  sout << " K*L factor  " << _kl * TeaPot::strength << "\n";
  sout << " Number of slices = " << _nsteps << "\n";
  if(_skew) sout << " Lens is skewed \n";
  if((_fringeIN != 0) || (_fringeOUT != 0))
    sout << "Fringe Fields Activated \n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPM::updatePartAtNode
//
// DESCRIPTION
//   TeaPot Multipole node action
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPM::_updatePartAtNode(MacroPart& mp)
{
  Integer k, nsteps, pole, skew;
  Real length, lengtho2, tilt, kl, rho;

  if(_length < 0.0) return;

  _sub();

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = _tilt;
    rotatexy(mp, tilt);
  }

  if(Abs(_length) <= Consts::tiny)
  {
    pole = _pole;
    skew = _skew;
    kl = _kl * TeaPot::strength;
    multp(mp, pole, kl, skew);
    if(Abs(_tilt) > Consts::tiny)
    {
      tilt = -_tilt;
      rotatexy(mp, tilt);
    }
    return;
  }

  pole = _pole;
  skew = _skew;

  if(_fringeIN != 0)
  {
    kl = _kl * TeaPot::strength / _length;
    if((pole != 0) || (skew != 0))
    {
      multpfringeIN(mp, pole, kl, skew);
    }
    else
    {
      rho = 1.0 / kl;
      bendfringeIN(mp, rho);
    }
  }

  nsteps = _nsteps;
  if(_nsteps < 1) nsteps = 1;

  length = _length / nsteps;
  lengtho2 = length / 2.0;
  kl = _kl * TeaPot::strength / nsteps;

  drift(mp, lengtho2);
  for (k = 1; k < nsteps; k++)
  {
    multp(mp, pole, kl, skew);
    drift(mp, length); 
  }
  multp(mp, pole, kl, skew);
  drift(mp, lengtho2); 

  if(_fringeOUT != 0)
  {
    kl = _kl * TeaPot::strength / _length;
    if((pole != 0) || (skew != 0))
    {
      multpfringeOUT(mp, pole, kl, skew);
    }
    else
    {
      rho = 1.0 / kl;
      bendfringeOUT(mp, rho);
    }
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = -_tilt;
    rotatexy(mp, tilt);
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPQ
//
// INHERITANCE RELATIONSHIPS
//   TPQ -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a non-linear quadrupole.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   kq (real)	--> _kq          : Strength (1/m^2)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of nonlinear quadrupole steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPQ : public TPBase
{

  Declare_Standard_Members(TPQ, TPBase);

  public:

  TPQ(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &tilt,
      const Real &kq,
      const Subroutine sub, const Integer &nsteps,
      const Integer &fringeIN, const Integer &fringeOUT):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _tilt(tilt), _kq(kq), _sub(sub),
  _nsteps(nsteps), _fringeIN(fringeIN), _fringeOUT(fringeOUT)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Subroutine _sub;
  Integer _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _kq;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPQ
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPQ, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPQ::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPQ::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;

  TeaPot::rhoInv = 0.0;

  _sub();
  Real kq = _kq * TeaPot::strength;

  Matrix(Real)  M, TEMP;
  Matrix(Real) MX, MY;
  M.resize(6,6);
  TEMP.resize(6,6);
  MX.resize(3,3);
  MY.resize(2,2);

  M  = MAT_Quad(length, gamma, kq);
  MX =   X_Quad(length, gamma, kq);
  MY =   Y_Quad(length, gamma, kq);

  if(Abs(_tilt) > Consts::tiny)
  {
    Real tilt = _tilt;
    TEMP = MAT_Tilt(tilt, M);
    M = TEMP;
    if(Abs(_tilt) > Consts::pi / 8.0)
    {
      length = _length;
      MX = X_Drift(length, gamma);
      MY = Y_Drift(length, gamma);
    }
  }

  TeaPot::transportMatrix = M;
  TeaPot::xMatrix         = MX;
  TeaPot::yMatrix         = MY;

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPQ *tm = TPQ::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPQ::_showTP(Ostream &sout)
{
  sout << " TeaPot Quadrupole Lens:" << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of lens = " << _length << "\n";
  _sub();
  sout << " K factor  " << _kq * TeaPot::strength << "\n";
  sout << " Number of slices = " << _nsteps << "\n";
  if((_fringeIN != 0) || (_fringeOUT != 0))
    sout << "Fringe Fields Activated \n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPQ::updatePartAtNode
//
// DESCRIPTION
//   Nonlinear TeaPot Quadrupole
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPQ::_updatePartAtNode(MacroPart& mp)
{
  Integer k, poleq = 1, skewq = 0;
  Real length, lengtho2, tilt, kq;

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  _sub();

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = _tilt;
    rotatexy(mp, tilt);
  }

  kq = _kq * TeaPot::strength;

  if(_fringeIN != 0)
  {
    multpfringeIN(mp, poleq, kq, skewq);
    // quadfringeIN(mp, kq);
  }

  if(_nsteps > 0)
  {
    length = _length / _nsteps;
    lengtho2 = length / 2.0;

    quad1(mp, lengtho2, kq);
    for (k = 1; k < _nsteps; k++)
    {
      quad2(mp, length);
      quad1(mp, length, kq);
    }
    quad2(mp, length);
    quad1(mp, lengtho2, kq);
  }
  else
  {
    length = _length;
    quad1(mp, length, kq);
  }

  if(_fringeOUT != 0)
  {
    multpfringeOUT(mp, poleq, kq, skewq);
    // quadfringeOUT(mp, kq);
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = -_tilt;
    rotatexy(mp, tilt);
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPB
//
// INHERITANCE RELATIONSHIPS
//   TPB -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a TeaPot bending magnet.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   theta(real)--> _theta       : Bending angle
//   ea1(real)--> _ea1           : Entrance end angle
//   ea2(real)--> _ea2           : Exit end angle
//   nsteps(int)--> _nsteps      : Number of nonlinear bend steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPB : public TPBase
{

  Declare_Standard_Members(TPB, TPBase);

  public:

  TPB(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &tilt,
      const Real &theta,
      const Real &ea1, const Real &ea2,
      const Integer &nsteps,
      const Integer &fringeIN, const Integer &fringeOUT):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _tilt(tilt), _theta(theta), _ea1(ea1), _ea2(ea2),
  _nsteps(nsteps), _fringeIN(fringeIN), _fringeOUT(fringeOUT)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Integer _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _theta, _ea1, _ea2;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPB
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPB, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPB::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPB::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;
  Real theta = _theta;
  Real kq = 0.0;
  Real ea1 = _ea1;
  Real ea2 = _ea2;

  Matrix(Real) M, TEMP;
  Matrix(Real) MX, MY;
  M.resize(6,6);
  TEMP.resize(6,6);
  MX.resize(3,3);
  MY.resize(2,2);

  TeaPot::rhoInv = theta / length;

  M  = MAT_Bend(length, gamma, theta, kq, ea1, ea2);
  MX =   X_Bend(length, gamma, theta, kq, ea1, ea2);
  MY =   Y_Bend(length, gamma, theta, kq, ea1, ea2);

  if(Abs(_tilt) > Consts::tiny)
  {
    Real tilt = _tilt;
    TEMP = MAT_Tilt(tilt, M);
    M = TEMP;
    if(Abs(_tilt) > Consts::pi / 4.0)
    {
      length = _length;
      MX = X_Drift(length, gamma);
      MY = Y_Drift(length, gamma);
    }
  }

  TeaPot::transportMatrix = M;
  TeaPot::xMatrix         = MX;
  TeaPot::yMatrix         = MY;

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPB *tm = TPB::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPB::_showTP(Ostream &sout)
{
  sout << " TeaPot Bending Lens:" << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of lens = " << _length << "\n";
  sout << " Bending angle  " << _theta << "\n";
  sout << " Entrance end angle  " << _ea1 << "\n";
  sout << " Exit end angle  " << _ea2 << "\n";
  sout << " Number of slices = " << _nsteps << "\n";
  if((_fringeIN != 0) || (_fringeOUT != 0))
    sout << "Fringe Fields Activated \n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPB::updatePartAtNode
//
// DESCRIPTION
//   Causes TeaPot Bending node to act on particles
//   each time they circle the ring
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPB::_updatePartAtNode(MacroPart& mp)
{
  Integer k, nsteps, inout, frinout;
  Real length, lengtho2, tilt, th, tho2, e, me, rho;

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = _tilt;
    rotatexy(mp, tilt);
  }

  nsteps = _nsteps;
  rho = _length / _theta;

  if(_ea1 != 0.0)
  {
    e = _ea1;
    inout = 0;
    wedgedrift(mp, e, inout);
    if(_fringeIN != 0)
    {
      frinout = 0;
      wedgerotate(mp, e, frinout);
      bendfringeIN(mp, rho);
      frinout = 1;
      wedgerotate(mp, e, frinout);
    }
    wedgebend(mp, e, inout, rho, nsteps);
  }
  else
  {
    if(_fringeIN != 0)
    {
      bendfringeIN(mp, rho);
    }
  }

  if(_nsteps > 0)
  {
    length = _length / _nsteps;
    lengtho2 = length / 2.0;
    th = _theta / _nsteps;
    tho2 = th / 2.0;

    bend1(mp, lengtho2, tho2);
    for (k = 1; k < _nsteps; k++)
    {
      bend2(mp, lengtho2);
      bend3(mp, tho2);
      bend4(mp, th);
      bend3(mp, tho2);
      bend2(mp, lengtho2);
      bend1(mp, length, th);
    }
    bend2(mp, lengtho2);
    bend3(mp, tho2);
    bend4(mp, th);
    bend3(mp, tho2);
    bend2(mp, lengtho2);
    bend1(mp, lengtho2, tho2);
  }
  else
  {
    length = _length;
    th = _theta;
    bend1(mp, length, th);
  }

  if(_ea2 != 0.0)
  {
    e = _ea2;
    inout = 1;
    wedgebend(mp, e, inout, rho, nsteps);
    if(_fringeOUT != 0)
    {
      me = -e;
      frinout = 0;
      wedgerotate(mp, me, frinout);
      bendfringeOUT(mp, rho);
      frinout = 1;
      wedgerotate(mp, me, frinout);
    }
    wedgedrift(mp, e, inout);
  }
  else
  {
    if(_fringeOUT != 0)
    {
      bendfringeOUT(mp, rho);
    }
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = -_tilt;
    rotatexy(mp, tilt);
  }
  
  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPS
//
// INHERITANCE RELATIONSHIPS
//   TPS -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a teapot solenoid.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   B (real)	--> _B           : Strength (1/m)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of integration steps
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPS : public TPBase
{

  Declare_Standard_Members(TPS, TPBase);

  public:

  TPS(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &B,
      const Subroutine sub, const Integer &nsteps):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _B(B), _sub(sub), _nsteps(nsteps)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Real _B;
  Subroutine _sub;
  Integer _nsteps;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPS
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPS, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPS::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPS::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;

  TeaPot::rhoInv = 0.0;

  _sub();
  Real B = _B * TeaPot::strength;

  TeaPot::transportMatrix = MAT_Soln(length, gamma, B);
  TeaPot::xMatrix         =   X_Soln(length, gamma, B);
  TeaPot::yMatrix         =   Y_Soln(length, gamma, B);

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPS *tm = TPS::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPS::_showTP(Ostream &sout)
{
  sout << " TeaPot Solenoid:" << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of lens = " << _length << "\n";
  _sub();
  sout << " Magnetic field strength  " << _B * TeaPot::strength << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPS::updatePartAtNode
//
// DESCRIPTION
//   TeaPot Solenoid
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPS::_updatePartAtNode(MacroPart& mp)
{
  Integer k;
  Real length, B;

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  _sub();

  length = _length / _nsteps;
  B = _B * TeaPot::strength;

  for(k = 0; k < _nsteps; k++)
  {
    soln(mp, length, B);
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPMCF
//
// INHERITANCE RELATIONSHIPS
//   TPMCF -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a
//   combined function multipole with length.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		       0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  	    	   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of thin multipole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPMCF : public TPBase
{

  Declare_Standard_Members(TPMCF, TPBase);

  public:

  TPMCF(const String &n, const Integer &order,
        const Real &bx, const Real &by,
        const Real &ax, const Real &ay,
        const Real &ex, const Real &epx,
        const Real &l, const String &et,
        const Real &tilt,
        const Integer &vecnum,
        const Vector(Integer) &pole,
        const Vector(Real) &kl,
        const Vector(Integer) &skew,
        const Subroutine sub, const Integer &nsteps,
        const Integer &fringeIN, const Integer &fringeOUT):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _tilt(tilt),
  _vecnum(vecnum), _pole(pole), _kl(kl), _skew(skew), _sub(sub),
  _nsteps(nsteps), _fringeIN(fringeIN), _fringeOUT(fringeOUT)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Subroutine _sub;
  Integer _vecnum, _nsteps, _fringeIN, _fringeOUT;
  Real _tilt;
  Vector(Integer) _pole, _skew;
  Vector(Real) _kl;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPMCF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPMCF, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPMCF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPMCF::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;

  TeaPot::rhoInv = 0.0;

  if(_length < 0.0)
  {
    TeaPot::transportMatrix = MAT_Identity();
    TeaPot::xMatrix         =   X_Identity();
    TeaPot::yMatrix         =   Y_Identity();
    return;
  }

  _sub();

  Real kls = 0.0;
  Real kln = 0.0;

  for (Integer l = 1; l <= _vecnum; l++)
  {
    if(_pole(l) == 1)
    {
      if(_skew(l)) kls = _kl(l) * TeaPot::strength;
      else         kln = _kl(l) * TeaPot::strength;
    };
  }

  if((Abs(kln) <= Consts::tiny) && (Abs(kls) <= Consts::tiny))
  {
    TeaPot::transportMatrix = MAT_Drift(length, gamma);
    TeaPot::xMatrix         =   X_Drift(length, gamma);
    TeaPot::yMatrix         =   Y_Drift(length, gamma);
    return;
  }

  Matrix(Real)  M,  TEMP1,  TEMP2;
  Matrix(Real) MX, TEMP1X, TEMP2X;
  Matrix(Real) MY, TEMP1Y, TEMP2Y;
  M.resize(6,6);
  TEMP1.resize(6,6);
  TEMP2.resize(6,6);
  MX.resize(3,3);
  TEMP1X.resize(3,3);
  TEMP2X.resize(3,3);
  MY.resize(2,2);
  TEMP1Y.resize(2,2);
  TEMP2Y.resize(2,2);

  if(Abs(_length) <= Consts::tiny)
  {
    M  = MAT_Thin(kln, kls);
    MX =   X_Thin(kln, kls);
    MY =   Y_Thin(kln, kls);
  }
  else
  {
    Integer nsteps = _nsteps;
    if(_nsteps < 1) nsteps = 1;

    length = _length / nsteps;
    Real lengtho2 = length / 2.0;
    kls /= nsteps;
    kln /= nsteps;
    Integer m  = 6, l  = 6, n  = 6;
    Integer mx = 3, lx = 3, nx = 3;
    Integer my = 2, ly = 2, ny = 2;

    M  = MAT_Drift(lengtho2, gamma);
    MX =   X_Drift(lengtho2, gamma);
    MY =   Y_Drift(lengtho2, gamma);

    for (Integer k = 1; k < nsteps; k++)
    {
      TEMP1  = MAT_Thin(kln, kls);
      TEMP1X =   X_Thin(kln, kls);
      TEMP1Y =   Y_Thin(kln, kls);      
      TEMP2  = MAT_Mult( m,  l,  n,  TEMP1,  M);
      TEMP2X = MAT_Mult(mx, lx, nx, TEMP1X, MX);
      TEMP2Y = MAT_Mult(my, ly, ny, TEMP1Y, MY);
      TEMP1  = MAT_Drift(length, gamma);
      TEMP1X =   X_Drift(length, gamma);
      TEMP1Y =   Y_Drift(length, gamma);
      M      = MAT_Mult( m,  l,  n,  TEMP1,  TEMP2);
      MX     = MAT_Mult(mx, lx, nx, TEMP1X, TEMP2X);
      MY     = MAT_Mult(my, ly, ny, TEMP1Y, TEMP2Y);
    }

    TEMP1  = MAT_Thin(kln, kls);
    TEMP1X =   X_Thin(kln, kls);
    TEMP1Y =   Y_Thin(kln, kls);    
    TEMP2  = MAT_Mult( m,  l,  n,  TEMP1,  M);
    TEMP2X = MAT_Mult(mx, lx, nx, TEMP1X, MX);
    TEMP2Y = MAT_Mult(my, ly, ny, TEMP1Y, MY);
    TEMP1  = MAT_Drift(lengtho2, gamma);
    TEMP1X =   X_Drift(lengtho2, gamma);
    TEMP1Y =   Y_Drift(lengtho2, gamma);
    M      = MAT_Mult( m,  l,  n,  TEMP1,  TEMP2);
    MX     = MAT_Mult(mx, lx, nx, TEMP1X, TEMP2X);
    MY     = MAT_Mult(my, ly, ny, TEMP1Y, TEMP2Y);
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    Real tilt = _tilt;
    TEMP1 = MAT_Tilt(tilt, M);
    M = TEMP1;
    if(Abs(_tilt) > Consts::pi / 8.0)
    {
      length = _length;
      MX = X_Drift(length, gamma);
      MY = Y_Drift(length, gamma);
    }
  }

  TeaPot::transportMatrix = M;
  TeaPot::xMatrix         = MX;
  TeaPot::yMatrix         = MY;

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPMCF *tm = TPMCF::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPMCF::_showTP(Ostream &sout)
{
  Integer j;
  sout << " TeaPot Multipole Lens:" << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Multipole order is =  " << _pole << "\n";
  sout << " Length of lens = " << _length << "\n";
  _sub();
  sout << " K*L factor  " << _kl * TeaPot::strength << "\n";
  sout << " Number of slices = " << _nsteps << "\n";
  for (j = 1; j <= _vecnum; j++)
  {
    if(_skew(j)) sout << " Lens is skewed for j = " << j << "\n";
  }
  if((_fringeIN != 0) || (_fringeOUT != 0))
    sout << "Fringe Fields Activated \n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPMCF::updatePartAtNode
//
// DESCRIPTION
//   TeaPot Combined Function Multipole node action
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPMCF::_updatePartAtNode(MacroPart& mp)
{
  Integer k, l, nsteps;
  Vector(Integer) pole(_vecnum), skew(_vecnum);
  Real length, lengtho2, tilt, rho;
  Vector(Real) kl(_vecnum);

  if(_length < 0.0) return;

  _sub();

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = _tilt;
    rotatexy(mp, tilt);
  }

  if(Abs(_length) <= Consts::tiny)
  {
    for (l = 1; l <= _vecnum; l++)
    {
      pole(l) = _pole(l);
      skew(l) = _skew(l);
      kl(l) = _kl(l) * TeaPot::strength;
    }

    for (l = 1; l <= _vecnum; l++)
    {
      multp(mp, pole(l), kl(l), skew(l));
    }
    if(Abs(_tilt) > Consts::tiny)
    {
      tilt = -_tilt;
      rotatexy(mp, tilt);
    }
    return;
  }

  for (l = 1; l <= _vecnum; l++)
  {
    pole(l) = _pole(l);
    skew(l) = _skew(l);
  }

  if(_fringeIN != 0)
  {
    for (l = 1; l <= _vecnum; l++)
    {
      kl(l) = _kl(l) * TeaPot::strength / _length;
      if((pole(l) != 0) || (skew(l) != 0))
      {
        multpfringeIN(mp, pole(l), kl(l), skew(l));
      }
      else
      {
        rho = 1.0 / kl(l);
        bendfringeIN(mp, rho);
      }
    }
  }

  nsteps = _nsteps;
  if(_nsteps < 1) nsteps = 1;

  length = _length / nsteps;
  lengtho2 = length / 2.0;
  for (l = 1; l <= _vecnum; l++)
  {
    kl(l) = _kl(l) * TeaPot::strength / nsteps;
  }

  drift(mp, lengtho2);
  for (k = 1; k < nsteps; k++)
  {
    for (l = 1; l <= _vecnum; l++)
    {
      multp(mp, pole(l), kl(l), skew(l));
    }
    drift(mp, length); 
  }

  for (l = 1; l <= _vecnum; l++)
  {
    multp(mp, pole(l), kl(l), skew(l));
  }
  drift(mp, lengtho2);

  if(_fringeOUT != 0)
  {
    for (l = 1; l <= _vecnum; l++)
    {
      kl(l) = _kl(l) * TeaPot::strength / _length;
      if((pole(l) != 0) || (skew(l) != 0))
      {
        multpfringeOUT(mp, pole(l), kl(l), skew(l));
      }
      else
      {
        rho = 1.0 / kl(l);
        bendfringeOUT(mp, rho);
      }
    }
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = -_tilt;
    rotatexy(mp, tilt);
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPQCF
//
// INHERITANCE RELATIONSHIPS
//   TPQCF -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a non-linear quadrupole.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   kq (real)	--> _kq          : Strength (1/m^2)
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		       0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		       "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of nonlinear quadrupole steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPQCF : public TPBase
{

  Declare_Standard_Members(TPQCF, TPBase);

  public:

  TPQCF(const String &n, const Integer &order,
        const Real &bx, const Real &by,
        const Real &ax, const Real &ay,
        const Real &ex, const Real &epx,
        const Real &l, const String &et,
        const Real &tilt,
        const Real &kq,
        const Integer &vecnum,
        const Vector(Integer) &pole,
        const Vector(Real) &kl,
        const Vector(Integer) &skew,
        const Subroutine sub, const Integer &nsteps,
        const Integer &fringeIN, const Integer &fringeOUT):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _tilt(tilt), _kq(kq),
  _vecnum(vecnum), _pole(pole), _kl(kl), _skew(skew), _sub(sub),
  _nsteps(nsteps), _fringeIN(fringeIN), _fringeOUT(fringeOUT)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Subroutine _sub;
  Integer _vecnum, _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _kq;
  Vector(Integer) _pole, _skew;
  Vector(Real) _kl;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPQ
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPQCF, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPQCF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPQCF::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;

  TeaPot::rhoInv = 0.0;

  if(_length < 0.0)
  {
    TeaPot::transportMatrix = MAT_Identity();
    TeaPot::xMatrix         =   X_Identity();
    TeaPot::yMatrix         =   Y_Identity();
    return;
  }

  _sub();

  Real kq = _kq * TeaPot::strength;
  Real kls = 0.0;
  Real kln = 0.0;

  for (Integer l = 1; l <= _vecnum; l++)
  {
    if(_pole(l) == 1)
    {
      if(_skew(l)) kls = _kl(l) * TeaPot::strength;
      else         kln = _kl(l) * TeaPot::strength;
    };
  }

  Matrix(Real)  M,  TEMP1,  TEMP2;
  Matrix(Real) MX, TEMP1X, TEMP2X;
  Matrix(Real) MY, TEMP1Y, TEMP2Y;
  M.resize(6,6);
  TEMP1.resize(6,6);
  TEMP2.resize(6,6);
  MX.resize(3,3);
  TEMP1X.resize(3,3);
  TEMP2X.resize(3,3);
  MY.resize(2,2);
  TEMP1Y.resize(2,2);
  TEMP2Y.resize(2,2);

  if((Abs(kln) <= Consts::tiny) &&
     (Abs(kls) <= Consts::tiny))
  {
    M  = MAT_Quad(length, gamma, kq);
    MX =   X_Quad(length, gamma, kq);
    MY =   Y_Quad(length, gamma, kq);
  }
  else
  {
    Integer nsteps = _nsteps;
    if(_nsteps < 1) nsteps = 1;

    length = _length / nsteps;
    Real lengtho2 = length / 2.0;
    kls /= nsteps;
    kln /= nsteps;
    Integer m  = 6, l  = 6, n  = 6;
    Integer mx = 3, lx = 3, nx = 3;
    Integer my = 2, ly = 2, ny = 2;

    M  = MAT_Quad(lengtho2, gamma, kq);
    MX =   X_Quad(lengtho2, gamma, kq);
    MY =   Y_Quad(lengtho2, gamma, kq);

    for (Integer k = 1; k < nsteps; k++)
    {
      TEMP1  = MAT_Thin(kln, kls);
      TEMP1X =   X_Thin(kln, kls);
      TEMP1Y =   Y_Thin(kln, kls);      
      TEMP2  = MAT_Mult( m,  l,  n,  TEMP1,  M);
      TEMP2X = MAT_Mult(mx, lx, nx, TEMP1X, MX);
      TEMP2Y = MAT_Mult(my, ly, ny, TEMP1Y, MY);
      TEMP1  = MAT_Quad(length, gamma, kq);
      TEMP1X =   X_Quad(length, gamma, kq);
      TEMP1Y =   Y_Quad(length, gamma, kq);
      M      = MAT_Mult( m,  l,  n,  TEMP1,  TEMP2);
      MX     = MAT_Mult(mx, lx, nx, TEMP1X, TEMP2X);
      MY     = MAT_Mult(my, ly, ny, TEMP1Y, TEMP2Y);
    }

    TEMP1  = MAT_Thin(kln, kls);
    TEMP1X =   X_Thin(kln, kls);
    TEMP1Y =   Y_Thin(kln, kls);      
    TEMP2  = MAT_Mult( m,  l,  n,  TEMP1,  M);
    TEMP2X = MAT_Mult(mx, lx, nx, TEMP1X, MX);
    TEMP2Y = MAT_Mult(my, ly, ny, TEMP1Y, MY);
    TEMP1  = MAT_Quad(lengtho2, gamma, kq);
    TEMP1X =   X_Quad(lengtho2, gamma, kq);
    TEMP1Y =   Y_Quad(lengtho2, gamma, kq);
    M      = MAT_Mult( m,  l,  n,  TEMP1,  TEMP2);
    MX     = MAT_Mult(mx, lx, nx, TEMP1X, TEMP2X);
    MY     = MAT_Mult(my, ly, ny, TEMP1Y, TEMP2Y);
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    Real tilt = _tilt;
    TEMP1 = MAT_Tilt(tilt, M);
    M = TEMP1;
    if(Abs(_tilt) > Consts::pi / 8.0)
    {
      length = _length;
      MX = X_Drift(length, gamma);
      MY = Y_Drift(length, gamma);
    }
  }

  TeaPot::transportMatrix = M;
  TeaPot::xMatrix         = MX;
  TeaPot::yMatrix         = MY;

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPQCF *tm = TPQCF::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPQCF::_showTP(Ostream &sout)
{
  Integer j;

  sout << " TeaPot Quadrupole Lens:" << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of lens = " << _length << "\n";
  sout << " Multipole order is =  " << _pole << "\n";
  _sub();
  sout << " K*L factor  " << _kl * TeaPot::strength << "\n";
  sout << " K factor  " << _kq * TeaPot::strength << "\n";
  sout << " Number of slices = " << _nsteps << "\n";
  for (j = 1; j <= _vecnum; j++)
  {
    if(_skew(j)) sout << " Lens is skewed for j = " << j << "\n";
  }
  if((_fringeIN != 0) || (_fringeOUT != 0))
    sout << "Fringe Fields Activated \n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPQCF::updatePartAtNode
//
// DESCRIPTION
//   Nonlinear TeaPot Combined Function Quadrupole
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPQCF::_updatePartAtNode(MacroPart& mp)
{
  Integer k, l, nsteps, poleq = 1, skewq = 0;
  Real length, lengtho2, tilt, kq;
  Vector(Integer) pole(_vecnum), skew(_vecnum);
  Vector(Real) kl(_vecnum);

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  _sub();

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = _tilt;
    rotatexy(mp, tilt);
  }

  for (l = 1; l <= _vecnum; l++)
  {
    pole(l) = _pole(l);
    skew(l) = _skew(l);
  }

  kq = _kq * TeaPot::strength;

  if(_fringeIN != 0)
  {
    multpfringeIN(mp, poleq, kq, skewq);
    // quadfringeIN(mp, kq);
    for (l = 1; l <= _vecnum; l++)
    {
      kl(l) = _kl(l) * TeaPot::strength / _length;
      multpfringeIN(mp, pole(l), kl(l), skew(l));
    }
  }

  nsteps = _nsteps;

  if(nsteps > 0)
  {
    length = _length / _nsteps;
    lengtho2 = length / 2.0;
    for (l = 1; l <= _vecnum; l++)
    {
      kl(l) = _kl(l) * TeaPot::strength / nsteps;
    }
    quad1(mp, lengtho2, kq);
    for (k = 1; k < nsteps; k++)
    {
      quad2(mp, lengtho2);
      for (l = 1; l <= _vecnum; l++)
      {
        multp(mp, pole(l), kl(l), skew(l));
      }
      quad2(mp, lengtho2);
      quad1(mp, length, kq);
    }
    quad2(mp, lengtho2);
    for (l = 1; l <= _vecnum; l++)
    {
      multp(mp, pole(l), kl(l), skew(l));
    }
    quad2(mp, lengtho2);
    quad1(mp, lengtho2, kq);
  }
  else
  {
    length = _length;
    quad1(mp, length, kq);
  }

  if(_fringeOUT != 0)
  {
    multpfringeOUT(mp, poleq, kq, skewq);
    // quadfringeOUT(mp, kq);
    for (l = 1; l <= _vecnum; l++)
    {
      kl(l) = _kl(l) * TeaPot::strength / _length;
      multpfringeOUT(mp, pole(l), kl(l), skew(l));
    }
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = -_tilt;
    rotatexy(mp, tilt);
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPBCF
//
// INHERITANCE RELATIONSHIPS
//   TPBCF -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a TeaPot bending magnet.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   theta(real)--> _theta       : Bending angle
//   ea1(real)--> _ea1           : Entrance end angle
//   ea2(real)--> _ea2           : Exit end angle
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   nsteps(int)--> _nsteps      : Number of nonlinear bend steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPBCF : public TPBase
{

  Declare_Standard_Members(TPBCF, TPBase);

  public:

  TPBCF(const String &n, const Integer &order,
        const Real &bx, const Real &by,
        const Real &ax, const Real &ay,
        const Real &ex, const Real &epx,
        const Real &l, const String &et,
        const Real &tilt,
        const Real &theta,
        const Real &ea1, const Real &ea2,
        const Integer &vecnum,
        const Vector(Integer) &pole,
        const Vector(Real) &kl,
        const Vector(Integer) &skew,
        const Integer &nsteps,
        const Integer &fringeIN, const Integer &fringeOUT):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _tilt(tilt), _theta(theta), _ea1(ea1), _ea2(ea2),
  _vecnum(vecnum), _pole(pole), _kl(kl), _skew(skew),
  _nsteps(nsteps), _fringeIN(fringeIN), _fringeOUT(fringeOUT)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Integer _vecnum, _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _theta, _ea1, _ea2;
  Vector(Integer) _pole, _skew;
  Vector(Real) _kl;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPBCF
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPBCF, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPBCF::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPBCF::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;
  Real theta = _theta;
  Real kq = 0.0;
  Real ea1 = _ea1;
  Real ea2 = _ea2;

  Real kls = 0.0;
  Real kln = 0.0;

  TeaPot::rhoInv = theta / length;

  for (Integer l = 1; l <= _vecnum; l++)
  {
    if(_pole(l) == 1)
    {
      if(_skew(l)) kls = _kl(l);
      else         kln = _kl(l);
    };
  }

  Matrix(Real)  M,  TEMP1,  TEMP2;
  Matrix(Real) MX, TEMP1X, TEMP2X;
  Matrix(Real) MY, TEMP1Y, TEMP2Y;
  M.resize(6,6);
  TEMP1.resize(6,6);
  TEMP2.resize(6,6);
  MX.resize(3,3);
  TEMP1X.resize(3,3);
  TEMP2X.resize(3,3);
  MY.resize(2,2);
  TEMP1Y.resize(2,2);
  TEMP2Y.resize(2,2);

  if((Abs(kln) <= Consts::tiny) &&
     (Abs(kls) <= Consts::tiny))
  {
    M  = MAT_Bend(length, gamma, theta, kq, ea1, ea2);
    MX =   X_Bend(length, gamma, theta, kq, ea1, ea2);
    MY =   Y_Bend(length, gamma, theta, kq, ea1, ea2);
  }
  else
  {
    Integer nsteps = _nsteps;
    if(_nsteps < 1) nsteps = 1;

    length = _length / nsteps;
    Real lengtho2 = length / 2.0;
    theta = _theta / nsteps;
    Real thetao2 = theta /2.0;
    kls /= nsteps;
    kln /= nsteps;
    Integer m  = 6, l  = 6, n  = 6;
    Integer mx = 3, lx = 3, nx = 3;
    Integer my = 2, ly = 2, ny = 2;

    ea2 = 0.0;
    M  = MAT_Bend(lengtho2, gamma, thetao2, kq, ea1, ea2);
    MX =   X_Bend(lengtho2, gamma, thetao2, kq, ea1, ea2);
    MY =   Y_Bend(lengtho2, gamma, thetao2, kq, ea1, ea2);

    ea1 = 0.0;
    for (Integer k = 1; k < nsteps; k++)
    {
      TEMP1  = MAT_Thin(kln, kls);
      TEMP1X =   X_Thin(kln, kls);
      TEMP1Y =   Y_Thin(kln, kls);      
      TEMP2  = MAT_Mult( m,  l,  n,  TEMP1,  M);
      TEMP2X = MAT_Mult(mx, lx, nx, TEMP1X, MX);
      TEMP2Y = MAT_Mult(my, ly, ny, TEMP1Y, MY);
      TEMP1  = MAT_Bend(length, gamma, theta, kq, ea1, ea2);
      TEMP1X =   X_Bend(length, gamma, theta, kq, ea1, ea2);
      TEMP1Y =   Y_Bend(length, gamma, theta, kq, ea1, ea2);
      M      = MAT_Mult( m,  l,  n,  TEMP1,  TEMP2);
      MX     = MAT_Mult(mx, lx, nx, TEMP1X, TEMP2X);
      MY     = MAT_Mult(my, ly, ny, TEMP1Y, TEMP2Y);
    }

    TEMP1  = MAT_Thin(kln, kls);
    TEMP1X =   X_Thin(kln, kls);
    TEMP1Y =   Y_Thin(kln, kls);      
    TEMP2  = MAT_Mult( m,  l,  n,  TEMP1,  M);
    TEMP2X = MAT_Mult(mx, lx, nx, TEMP1X, MX);
    TEMP2Y = MAT_Mult(my, ly, ny, TEMP1Y, MY);
    ea2 = _ea2;
    TEMP1  = MAT_Bend(lengtho2, gamma, thetao2, kq, ea1, ea2);
    TEMP1X =   X_Bend(lengtho2, gamma, thetao2, kq, ea1, ea2);
    TEMP1Y =   Y_Bend(lengtho2, gamma, thetao2, kq, ea1, ea2);
    M      = MAT_Mult( m,  l,  n,  TEMP1,  TEMP2);
    MX     = MAT_Mult(mx, lx, nx, TEMP1X, TEMP2X);
    MY     = MAT_Mult(my, ly, ny, TEMP1Y, TEMP2Y);
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    Real tilt = _tilt;
    TEMP1 = MAT_Tilt(tilt, M);
    M = TEMP1;
    if(Abs(_tilt) > Consts::pi / 4.0)
    {
      length = _length;
      MX = X_Drift(length, gamma);
      MY = Y_Drift(length, gamma);
    }
  }

  TeaPot::transportMatrix = M;
  TeaPot::xMatrix         = MX;
  TeaPot::yMatrix         = MY;

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPBCF *tm = TPBCF::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPBCF::_showTP(Ostream &sout)
{
  Integer j;

  sout << " TeaPot Bending Lens:" << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of lens = " << _length << "\n";
  sout << " Bending angle  " << _theta << "\n";
  sout << " Entrance end angle  " << _ea1 << "\n";
  sout << " Exit end angle  " << _ea2 << "\n";
  sout << " Multipole order is =  " << _pole << "\n";
  sout << " K*L factor  " << _kl << "\n";
  sout << " Number of slices = " << _nsteps << "\n";
  for (j = 1; j <= _vecnum; j++)
  {
    if(_skew(j)) sout << " Lens is skewed for j = " << j << "\n";
  }

  if((_fringeIN != 0) || (_fringeOUT != 0))
    sout << "Fringe Fields Activated \n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPBCF::updatePartAtNode
//
// DESCRIPTION
//   Causes TeaPot Combined Function Bending node to act on particles
//   each time they circle the ring
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPBCF::_updatePartAtNode(MacroPart& mp)
{
  Integer k, l, nsteps, inout, frinout, vecnum;
  Real length, lengtho2, tilt, th, tho2, e, me, rho;
  Vector(Integer) pole(_vecnum), skew(_vecnum);
  Vector(Real) kl(_vecnum);

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = _tilt;
    rotatexy(mp, tilt);
  }

  vecnum = _vecnum;
  for (l = 1; l <= _vecnum; l++)
  {
    pole(l) = _pole(l);
    skew(l) = _skew(l);
  }

  nsteps = _nsteps;
  rho = _length / _theta;

  if(_ea1 != 0.0)
  {
    e = _ea1;
    inout = 0;
    wedgedrift(mp, e, inout);
    for (l = 1; l <= _vecnum; l++)
    {
      kl(l) = _kl(l) / _length;
    }
    if(_fringeIN != 0)
    {
      frinout = 0;
      wedgerotate(mp, e, frinout);
      bendfringeIN(mp, rho);
      for (l = 1; l <= _vecnum; l++)
      {
        multpfringeIN(mp, pole(l), kl(l), skew(l));
      }
      frinout = 1;
      wedgerotate(mp, e, frinout);
    }
    wedgebendCF(mp, e, inout, rho,
                vecnum, pole, kl, skew,
                nsteps);
  }
  else
  {
    if(_fringeIN != 0)
    {
      bendfringeIN(mp, rho);
      for (l = 1; l <= _vecnum; l++)
      {
        kl(l) = _kl(l) / _length;
        multpfringeIN(mp, pole(l), kl(l), skew(l));
      }
    }
  }

  if(nsteps > 0)
  {
    length = _length / nsteps;
    lengtho2 = length / 2.0;
    th = _theta / nsteps;
    tho2 = th / 2.0;
    for (l = 1; l <= _vecnum; l++)
    {
      kl(l) = _kl(l) / nsteps;
    }

    bend1(mp, lengtho2, tho2);
    for (k = 1; k < nsteps; k++)
    {
      bend2(mp, lengtho2);
      bend3(mp, tho2);
      bend4(mp, tho2);
      for (l = 1; l <= _vecnum; l++)
      {
        multp(mp, pole(l), kl(l), skew(l));
      }
      bend4(mp, tho2);
      bend3(mp, tho2);
      bend2(mp, lengtho2);
      bend1(mp, length, th);
    }
    bend2(mp, lengtho2);
    bend3(mp, tho2);
    bend4(mp, tho2);
    for (l = 1; l <= _vecnum; l++)
    {
      multp(mp, pole(l), kl(l), skew(l));
    }
    bend4(mp, tho2);
    bend3(mp, tho2);
    bend2(mp, lengtho2);
    bend1(mp, lengtho2, tho2);
  }
  else
  {
    length = _length;
    th = _theta;
    bend1(mp, length, th);
  }

  if(_ea2 != 0.0)
  {
    e = _ea2;
    inout = 1;
    for (l = 1; l <= _vecnum; l++)
    {
      kl(l) = _kl(l) / _length;
    }
    wedgebendCF(mp, e, inout, rho,
                vecnum, pole, kl, skew,
                nsteps);
    if(_fringeOUT != 0)
    {
      me = -e;
      frinout = 0;
      wedgerotate(mp, me, frinout);
      bendfringeOUT(mp, rho);
      for (l = 1; l <= _vecnum; l++)
      {
        multpfringeOUT(mp, pole(l), kl(l), skew(l));
      }
      frinout = 1;
      wedgerotate(mp, me, frinout);
    }
    wedgedrift(mp, e, inout);
  }
  else
  {
    if(_fringeOUT != 0)
    {
      bendfringeOUT(mp, rho);
      for (l = 1; l <= _vecnum; l++)
      {
        kl(l) = _kl(l) / _length;
        multpfringeOUT(mp, pole(l), kl(l), skew(l));
      }
    }
  }

  if(Abs(_tilt) > Consts::tiny)
  {
    tilt = -_tilt;
    rotatexy(mp, tilt);
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPK
//
// INHERITANCE RELATIONSHIPS
//   TPK  -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a kicker.
//
// PUBLIC MEMBERS
//   n     (str) --> _name       : Name for this node
//   order  (int)--> _oindex     : node order index
//   bx  (real)  --> _betaX      : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by  (real)  --> _betaY      : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax  (real)  --> _alphaX     : The horizontal alpha value at the
//                                 beginning of the Node
//   ay  (real)  --> _alphaY     : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex  (real)  --> _etaX       : The horizontal dispersion [m]
//   epx  (real) --> _etaPX      : The horizontal dispersion prime
//   l    (real) --> _length     : The length of the lens
//   et  (string)--> _et         : Element Type
//   hkick(real) --> _hkick      : Horizontal kick strength
//   vkick(real) --> _vkick      : Vertical kick strength
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of kick steps
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPK : public TPBase
{
  Declare_Standard_Members(TPK, TPBase);

  public:

  TPK(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &hkick, const Real &vkick,
      const Subroutine sub, const Integer &nsteps):
  TPBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _hkick(hkick), _vkick(vkick), _sub(sub),
  _nsteps(nsteps)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Real _hkick, _vkick;
  Subroutine _sub;
  Integer _nsteps;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TPK
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TPK, TPBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPK::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPK::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX = _betaX;
  Ring::betaY = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX = _etaX;
  Ring::etaPX = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  // Begin Transport Matrix Calculation

  TeaPot::transportMatrix.resize(6,6);
  TeaPot::xMatrix.resize(3,3);
  TeaPot::yMatrix.resize(2,2);

  Real length = _length;
  Real gamma = mp._syncPart._gammaSync;

  TeaPot::rhoInv = 0.0;

  TeaPot::transportMatrix = MAT_Drift(length, gamma);
  TeaPot::xMatrix         =   X_Drift(length, gamma);
  TeaPot::yMatrix         =   Y_Drift(length, gamma);

  // End Transport Matrix Calculation

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    TPK *tm = TPK::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i= oldSize+1; i<= mp._nMacros; i++) // set phases of new guys
    {
      TPfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = TPxPhase;
      Particles::yPhaseOld(i) = TPyPhase;
    }
  }
}

Void TPK::_showTP(Ostream &sout)
{
  sout << " TeaPot Kicker " << _name << "\n";
  sout << " Index in ring " << _oindex << "/n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of Drift = " << _length << "\n";
  sout << " Strength of horizontal kick = " << _hkick << "\n";
  sout << " Strength of vertical kick = " << _vkick << "\n";
  sout << " Number of slices = " << _nsteps << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TPK::updatePartAtNode
//
// DESCRIPTION
//   Takes the particles through a kicker
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPK::_updatePartAtNode(MacroPart& mp)
{
  Integer k, nsteps;
  Real length, lengtho2, kx, ky, kE;

  if(_length < 0.0) return;

  _sub();

  nsteps = _nsteps;
  if(_nsteps < 1) nsteps = 1;

  if(Abs(_length) <= Consts::tiny)
  {
    kx = _hkick * TeaPot::hwaveT;
    ky = _vkick * TeaPot::vwaveT;
    kE = 0.0;
    kick(mp, kx, ky, kE);
    return;
  }

  length = _length / nsteps;
  lengtho2 = length / 2.0;
  kx = _hkick * TeaPot::hwaveT / nsteps;
  ky = _vkick * TeaPot::vwaveT / nsteps;
  kE = 0.0;

  drift(mp, lengtho2);
  kick(mp, kx, ky, kE);
  for(k = 1; k < nsteps; k++)
  {
    drift(mp, length);
    kick(mp, kx, ky, kE);
  }
  drift(mp, lengtho2);

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  TPdoTunes(*this, mp);
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TeaPot::ctor
//
// DESCRIPTION
//   Constructor for the TeaPot Module
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::ctor()
{
// set some initial values

  nTPNodes = 0;
  kickTurn = 0;
  hwaveT = 0.0;
  vwaveT = 0.0;
  hwf = 0.0;
  vwf = 0.0;
  dhwf = 0.0;
  dvwf = 0.0;
  tstart = 0.0;
  tstop = 1.0;
  tinc = 0.1;

  strinitial = 1.0;
  strfinal = 1.0;
  tstrinitial = 0.0;
  tstrfinal = 1.0;
  strength = 1.0;

  dPoP = 0.0;
  rhoInv = 0.0;

  transportMatrix.resize(6,6);
  transportMatrix = 0.0;
  xMatrix.resize(3,3);
  yMatrix.resize(2,2);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::nullOut
//
// DESCRIPTION
//   Zeroes the length of a TransMap Node
//   and grabs original length and lattice functions for substitute
//   node
//
// PARAMETERS
//   n    (str) --> _name   :   Name for this node
//   order (int)--> _oindex :   node order index
//   transmapNumber         :   number of matrix to be nulled
//   bx                     :   betax
//   by                     :   betay
//   ax                     :   alphax
//   ay                     :   alphay
//   ex                     :   etax
//   epx                    :   etapx
//   length                 :   length
//   et (str)   --> _et     :   The type of element added
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::nullOut(const String &n, const Integer &order,
                     Integer &transmapNumber,
                     Real &bx, Real &by,
                     Real &ax, Real &ay,
                     Real &ex, Real &epx,
                     Real &length, const String &et)
{
  //  Set the nearest transfer matrix to the identity

  Integer i;
  MapBase *tm;

  transmapNumber = -1;

  for(i = 1; i <= nTransMaps; i++)
  {
    tm = MapBase::safeCast(tMaps(i - 1));
    if(tm->_oindex == order) transmapNumber = i;
  }

  if(transmapNumber < 0) except(badTeaPotNode);
  if(transmapNumber > nTransMaps) except(badTeaPotNode);

  tm = MapBase::safeCast(tMaps(transmapNumber - 1));

  bx   = tm -> _betaX;
  by   = tm -> _betaY;
  ax   = tm -> _alphaX;
  ay   = tm -> _alphaY;
  ex   = tm -> _etaX;
  epx  = tm -> _etaPX;
  length = tm -> _length;

  tm -> _length = -Consts::tiny;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPD
//
// DESCRIPTION
//   Replaces a Drift node with a teapot Drift node
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : node order index
//   et (str)   --> _et          : The type of element added
//   nsteps(int)--> _nsteps      : Number of thin lens kicks contained
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPD(const String &n, const Integer &order,
                        const String &et,
                        const Integer &nsteps)
{
  Real bx, by, ax, ay, ex, epx, length;

  //  Set the nearest transfer matrix to the identity

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the Drift node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPD(n, order,
                              bx, by, ax, ay, ex, epx,
                              length, et,
                              nsteps);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPM
//
// DESCRIPTION
//   Replaces an existing Multipole node with a teapot multipole node
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   et (str)   --> _et          : The type of element added
//   tilt (real)--> _tilt        : Element roll
//   pole (int) --> _pole        : Multipole order
//                  	           0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Integrated strength (1/m^n)
//                     	           This is the integral of
//                  	           "k" = 1/(B*rho) * dB_y/(dx)^n at x = 0
//                  	           over the element length "l"
//   skew(int) 	--> _skew        : Switch to use a skew element
//                  	           If == 1, the thin lens is
//                                 rotated by pi/(2*n + 2)
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of multipole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPM(const String &n, const Integer &order,
                        const String &et,
                        const Real &tilt,
                        const Integer &pole, const Real &kl,
			            const Integer &skew,
                        const Integer &TPsubindex,
                        const Integer &nsteps,
		                const Integer &fringeIN, const Integer &fringeOUT)
{
  Real bx, by, ax, ay, ex, epx, length;
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  //  Set the nearest transfer matrix to the identity

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the TeaPot Multipole node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPM(n, order, bx, by, ax, ay, ex, epx,
                              length, et, tilt, pole, kl, skew,
                              WaveForm, nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPQ
//
// DESCRIPTION
//   Replaces a linear quadrupole node with a non-linear teapot one
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   et (str)   --> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   kq (real)	--> _kq          : Strength (1/m^2)
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of nonlinear quadrupole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPQ(const String &n, const Integer &order,
                        const String &et,
                        const Real &tilt,
                        const Real &kq,
                        const Integer &TPsubindex,
                        const Integer &nsteps,
                        const Integer &fringeIN, const Integer &fringeOUT)
{
  Real bx, by, ax, ay, ex, epx, length;
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  //  Set the nearest transfer matrix to the identity

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the TeaPot Quadrupole node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPQ(n, order, bx, by, ax, ay, ex, epx,
                              length, et, tilt, kq,
                              WaveForm, nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPB
//
// DESCRIPTION
//   Replaces a bending element with a TeaPot bending element
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   l (real)   --> _l           : Length unless l=0 then use nulled
//                                 element length
//   et (str)   --> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   theta(real)--> _theta       : Bending angle
//   ea1(real)--> _ea1           : Entrance end angle
//   ea2(real)--> _ea2           : Exit end angle
//   nsteps(int)--> _nsteps      : Number of nonlinear bend steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPB(const String &n, const Integer &order,
                        const Real &l, const String &et,
                        const Real &tilt,
                        const Real &theta,
                        const Real &ea1, const Real &ea2,
                        const Integer &nsteps,
                        const Integer &fringeIN, const Integer &fringeOUT)
{
  Real bx, by, ax, ay, ex, epx, length;

  //  Set the nearest transfer matrix to a unitary matrix

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the TeaPot Bending node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  if(l > 0.0) length = l;

  nodes(nNodes - 1) = new TPB(n, order, bx, by, ax, ay, ex, epx,
                              length, et, tilt, theta, ea1, ea2,
                              nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPS
//
// DESCRIPTION
//   Replaces a mad node with a teapot solenoid
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   et (str)   --> _et          : The type of magnet added
//   B (real)	--> _B           : Magnetic field (1/m)
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of integration steps
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPS(const String &n, const Integer &order,
                        const String &et,
                        const Real &B,
                        const Integer &TPsubindex,
                        const Integer &nsteps)
{
  Real bx, by, ax, ay, ex, epx, length;
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  //  Set the nearest transfer matrix to the identity

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the TeaPot Solenoid to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPS(n, order, bx, by, ax, ay, ex, epx,
                              length, et, B,
                              WaveForm, nsteps);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPMCF
//
// DESCRIPTION
//   Replaces an existing Multipole node with a teapot multipole node
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of thin multipole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPMCF(const String &n, const Integer &order,
                          const String &et,
                          const Real &tilt,
                          const Integer &vecnum,
                          const Vector(Integer) &pole,
                          const Vector(Real) &kl,
                          const Vector(Integer) &skew,
                          const Integer &TPsubindex,
                          const Integer &nsteps,
                          const Integer &fringeIN,
                          const Integer &fringeOUT)
{
  Real bx, by, ax, ay, ex, epx, length;
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  //  Set the nearest transfer matrix to the identity

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the TeaPot Multipole node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPMCF(n, order, bx, by, ax, ay, ex, epx,
                                length, et, tilt,
                                vecnum, pole, kl, skew,
                                WaveForm, nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPQCF
//
// DESCRIPTION
//   Replaces a linear quadrupole node with a non-linear teapot one
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   kq (real)	--> _kq          : Strength (1/m^2)
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		       0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		       "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of nonlinear quadrupole steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPQCF(const String &n, const Integer &order,
                          const String &et,
                          const Real &tilt, const Real &kq,
                          const Integer &vecnum,
                          const Vector(Integer) &pole, 
                          const Vector(Real) &kl,
                          const Vector(Integer) &skew,
                          const Integer &TPsubindex,
                          const Integer &nsteps,
                          const Integer &fringeIN,
                          const Integer &fringeOUT)
{
  Real bx, by, ax, ay, ex, epx, length;
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  //  Set the nearest transfer matrix to the identity

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the TeaPot Quadrupole node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPQCF(n, order, bx, by, ax, ay, ex, epx,
                                length, et, tilt, kq,
                                vecnum, pole, kl, skew,
                                WaveForm, nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPBCF
//
// DESCRIPTION
//   Replaces a bending element with a TeaPot bending element
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   theta(real)--> _theta       : Bending angle
//   ea1(real)--> _ea1           : Entrance end angle
//   ea2(real)--> _ea2           : Exit end angle
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   nsteps(int)--> _nsteps      : Number of nonlinear bend steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPBCF(const String &n, const Integer &order,
                          const Real &l, const String &et,
                          const Real &tilt,
                          const Real &theta,
                          const Real &ea1, const Real &ea2,
                          const Integer &vecnum,
                          const Vector(Integer) &pole,
                          const Vector(Real) &kl,
                          const Vector(Integer) &skew,
                          const Integer &nsteps,
                          const Integer &fringeIN,
                          const Integer &fringeOUT)
{
  Real bx, by, ax, ay, ex, epx, length;

  //  Set the nearest transfer matrix to a unitary matrix

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the TeaPot Bending node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  if(l > 0.0) length = l;

  nodes(nNodes - 1) = new TPBCF(n, order, bx, by, ax, ay, ex, epx,
                                length, et, tilt, theta, ea1, ea2,
                                vecnum, pole, kl, skew,
                                nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::replaceTPK
//
// DESCRIPTION
//   Replaces a Drift node with a teapot kicker node
//
// PARAMETERS
//   n    (str)  --> _name       : Name for this node
//   order (int) --> _oindex     : node order index
//   et (str)    --> _et         : The type of element added
//   hkick(real) --> _hkick      : Horizontal kick strength
//   vkick(real) --> _vkick      : Vertical kick strength
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int) --> _nsteps     : Number of thin lens kicks contained
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::replaceTPK(const String &n, const Integer &order,
                        const String &et,
                        const Real &hkick, const Real &vkick,
                        const Integer &TPsubindex,
                        const Integer &nsteps)
{
  Real bx, by, ax, ay, ex, epx, length;
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  //  Set the nearest transfer matrix to the identity

  Integer i;
  Integer transmapNumber;

  TeaPot::nullOut(n, order, transmapNumber,
                  bx, by, ax, ay, ex, epx,
                  length, et);

  // Add the kick node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPK(n, order,
                              bx, by, ax, ay, ex, epx,
                              length, et,
                              hkick, vkick,
                              WaveForm, nsteps);
  nTransMaps++;
  nTPNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPD
//
// DESCRIPTION
//   Adds a Teapot Drift node
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : node order index
//   l  (real)  --> _length      : Length of element
//   et (str)   --> _et          : The type of element
//   nsteps(int)--> _nsteps      : Number of drift steps
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPD(const String &n, const Integer &order,
                    const Real &bx, const Real &by,
                    const Real &ax, const Real &ay,
                    const Real &ex, const Real &epx,
                    const Real &l, const String &et,
                    const Integer &nsteps)
{
  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPD(n, order, bx, by, ax, ay, ex, epx,
                              l, et,
                              nsteps);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPM
//
// DESCRIPTION
//   Adds a teapot multipole node
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   l  (real)  --> _length      : Length of element
//   et (str)   --> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   pole (int) --> _pole        : Multipole order.
//                  	           0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Integrated strength (1/m^n).
//                     	           This is the integral of
//                  	           "k" = 1/(B*rho) * dB_y/(dx)^n at x = 0
//                  	           over the element length "l"
//   skew(int) 	--> _skew        : Switch to use a skew element.
//                  	           If == 1, the thin lens is
//                                 rotated by pi/(2*n + 2)
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of multipole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPM(const String &n, const Integer &order,
                    const Real &bx, const Real &by,
                    const Real &ax, const Real &ay,
                    const Real &ex, const Real &epx,
                    const Real &l, const String &et,
                    const Real &tilt,
                    const Integer &pole,  const Real &kl,
                    const Integer &skew,
                    const Integer &TPsubindex,
                    const Integer &nsteps,
                    const Integer &fringeIN, const Integer &fringeOUT)
{
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPM(n, order, bx, by, ax, ay, ex, epx,
                              l, et, tilt, pole, kl, skew,
                              WaveForm, nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPQ
//
// DESCRIPTION
//   Adds a non-linear teapot quadrupole
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   l  (real)  -->_length       : Length of multipole element
//   et (str)   --> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   kq (real)	--> _kq          : Quadrupole strength (1/m^2).
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of thin lens kicks contained
//                                 inside the lens
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPQ(const String &n, const Integer &order,
                    const Real &bx, const Real &by,
                    const Real &ax, const Real &ay,
                    const Real &ex, const Real &epx,
                    const Real &l, const String &et,
                    const Real &tilt,
                    const Real &kq,
                    const Integer &TPsubindex,
                    const Integer &nsteps,
                    const Integer &fringeIN, const Integer &fringeOUT)
{
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPQ(n, order, bx, by, ax, ay, ex, epx,
                              l, et, tilt, kq,
                              WaveForm, nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPB
//
// DESCRIPTION
//   Adds a TeaPot Bending Element
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   l  (real)  -->_length       : Length of multipole element
//   et (str)   --> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   theta(real)--> _theta       : Bending angle
//   ea1(real)--> _ea1           : Entrance end angle
//   ea2(real)--> _ea2           : Exit end angle
//   nsteps(int)--> _nsteps      : Number of thin lens kicks contained
//                                 inside the lens
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPB(const String &n, const Integer &order,
                    const Real &bx, const Real &by,
                    const Real &ax, const Real &ay,
                    const Real &ex, const Real &epx,
                    const Real &l, const String &et,
                    const Real &tilt,
                    const Real &theta,
                    const Real &ea1, const Real &ea2,
                    const Integer &nsteps,
                    const Integer &fringeIN, const Integer &fringeOUT)
{
  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPB(n, order, bx, by, ax, ay, ex, epx,
                              l, et, tilt, theta, ea1, ea2,
                              nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPS
//
// DESCRIPTION
//   Adds a teapot solenoid
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   l  (real)  -->_length       : Length of multipole element
//   et (str)   --> _et          : The type of magnet added
//   B (real)	--> _B           : Magnetic field strength (1/m).
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of integration steps
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPS(const String &n, const Integer &order,
                    const Real &bx, const Real &by,
                    const Real &ax, const Real &ay,
                    const Real &ex, const Real &epx,
                    const Real &l, const String &et,
                    const Real &B,
                    const Integer &TPsubindex,
                    const Integer &nsteps)
{
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPS(n, order, bx, by, ax, ay, ex, epx,
                              l, et, B,
                              WaveForm, nsteps);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPMCF
//
// DESCRIPTION
//   Adds a teapot multipole node
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of thin multipole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPMCF(const String &n, const Integer &order,
                      const Real &bx, const Real &by,
                      const Real &ax, const Real &ay,
                      const Real &ex, const Real &epx,
                      const Real &l, const String &et,
                      const Real &tilt,
                      const Integer &vecnum,
                      const Vector(Integer) &pole,
                      const Vector(Real) &kl,
                      const Vector(Integer) &skew,
                      const Integer &TPsubindex,
                      const Integer &nsteps,
                      const Integer &fringeIN,
                      const Integer &fringeOUT)
{
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPMCF(n, order, bx, by, ax, ay, ex, epx,
                                l, et, tilt,
                                vecnum, pole, kl, skew,
                                WaveForm, nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPQCF
//
// DESCRIPTION
//   Adds a non-linear teapot quadrupole
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   kq (real)	--> _kq          : Strength (1/m^2)
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of nonlinear quadrupole steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPQCF(const String &n, const Integer &order,
                      const Real &bx, const Real &by,
                      const Real &ax, const Real &ay,
                      const Real &ex, const Real &epx,
                      const Real &l, const String &et,
                      const Real &tilt,
                      const Real &kq,
                      const Integer &vecnum,
                      const Vector(Integer) &pole,
                      const Vector(Real) &kl,
                      const Vector(Integer) &skew,
                      const Integer &TPsubindex,
                      const Integer &nsteps,
                      const Integer &fringeIN,
                      const Integer &fringeOUT)
{
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPQCF(n, order, bx, by, ax, ay, ex, epx,
                                l, et, tilt, kq,
                                vecnum, pole, kl, skew,
                                WaveForm, nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPBCF
//
// DESCRIPTION
//   Adds a TeaPot Bending Element
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt(real)--> _tilt         : Element roll
//   theta(real)--> _theta       : Bending angle
//   ea1(real)--> _ea1           : Entrance end angle
//   ea2(real)--> _ea2           : Exit end angle
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   nsteps(int)--> _nsteps      : Number of nonlinear bend steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPBCF(const String &n, const Integer &order,
                      const Real &bx, const Real &by,
                      const Real &ax, const Real &ay,
                      const Real &ex, const Real &epx,
                      const Real &l, const String &et,
                      const Real &tilt,
                      const Real &theta,
                      const Real &ea1, const Real &ea2,
                      const Integer &vecnum,
                      const Vector(Integer) &pole,
                      const Vector(Real) &kl,
                      const Vector(Integer) &skew,
                      const Integer &nsteps,
                      const Integer &fringeIN,
                      const Integer &fringeOUT)
{
  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPBCF(n, order, bx, by, ax, ay, ex, epx,
                                l, et, tilt, theta, ea1, ea2,
                                vecnum, pole, kl, skew,
                                nsteps, fringeIN, fringeOUT);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::addTPK
//
// DESCRIPTION
//   Adds a Teapot kicker node
//
// PARAMETERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : node order index
//   l  (real)  --> _length      : Length of element
//   et (str)   --> _et          : The type of element
//   hkick(real) --> _hkick      : Horizontal kick strength
//   vkick(real) --> _vkick      : Vertical kick strength
//   TPsubindex  --> _sub        : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of drift steps
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::addTPK(const String &n, const Integer &order,
                    const Real &bx, const Real &by,
                    const Real &ax, const Real &ay,
                    const Real &ex, const Real &epx,
                    const Real &l, const String &et,
                    const Real &hkick, const Real &vkick,
                    const Integer &TPsubindex,
                    const Integer &nsteps)
{
  Subroutine WaveForm;
  if(TPsubindex ==  0) WaveForm = WaveForm00;
  if(TPsubindex ==  1) WaveForm = WaveForm01;
  if(TPsubindex ==  2) WaveForm = WaveForm02;
  if(TPsubindex ==  3) WaveForm = WaveForm03;
  if(TPsubindex ==  4) WaveForm = WaveForm04;
  if(TPsubindex ==  5) WaveForm = WaveForm05;
  if(TPsubindex ==  6) WaveForm = WaveForm06;
  if(TPsubindex ==  7) WaveForm = WaveForm07;
  if(TPsubindex ==  8) WaveForm = WaveForm08;
  if(TPsubindex ==  9) WaveForm = WaveForm09;
  if(TPsubindex == 10) WaveForm = WaveForm10;
  if(TPsubindex == 11) WaveForm = WaveForm11;
  if(TPsubindex == 12) WaveForm = WaveForm12;
  if(TPsubindex == 13) WaveForm = WaveForm13;
  if(TPsubindex == 14) WaveForm = WaveForm14;
  if(TPsubindex == 15) WaveForm = WaveForm15;
  if(TPsubindex == 16) WaveForm = WaveForm16;
  if(TPsubindex == 17) WaveForm = WaveForm17;
  if(TPsubindex == 18) WaveForm = WaveForm18;
  if(TPsubindex == 19) WaveForm = WaveForm19;

  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nTPNodes == TPPointers.size())
    TPPointers.resize(nTPNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new TPK(n, order,
                              bx, by, ax, ay, ex, epx,
                              l, et,
                              hkick, vkick,
                              WaveForm, nsteps);
  nTransMaps++;
  nTPNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  TPPointers(nTPNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::buildTPLattice
//
// DESCRIPTION
//   Reads in Twiss info from a MAD run,
//   and reads parsed .LAT MAD files
//   in order to construct a TeaPot lattice.
//
// PARAMETERS
//   MADTwissFile  - The name of the TWISS file to read
//   MADLATFile - The name of the parsed MAD .LAT file to read
//   nsteps - Number of TeaPot nsteps for elements
//   fringe - Hard edge for multipoles, quads, and bends:
//            1:Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::buildTPlattice(const String &MADTwissFile,
                            const String &MADLATFile,
                            const Integer &nstepTPD,
                            const Integer &nstepTPM,
                            const Integer &fringeM,
                            const Integer &nstepTPQ,
                            const Integer &fringeQ,
                            const Integer &nstepTPB,
                            const Integer &fringeB,
                            const Integer &nstepTPS,
                            const Integer &nstepTPK)
{
  // Local variables:

  String elementName, elementType, test,
         test1, test2, test3, test4, test5;
  Integer line, num, nRead;

  char c='\n';
  String Drift  = "Drift";
  String Mult   = "Mult";
  String MultCF = "MultCF";
  String Quad   = "Quad";
  String QuadCF = "QuadCF";
  String Bend   = "Bend";
  String BendCF = "BendCF";
  String Soln   = "Soln";
  String Kick   = "Kick";
  String Choice = "";

  Integer nElements;

  Real alpha_X, alpha_Y, beta_X, beta_Y, eta_X, etap_X,
       length, phiX, phiY, sPos;

  String TPelement, TPelementm, TPelementp,
                    TPelementh, TPelement0;
  String TPlabel;
  Real TPvalue;
  Integer TPsubindex;

  Real l,
       angle, e1, e2, tilt,
       k1, k2, k3,
       k0l, k1l, k2l, k3l, k4l, k5l, k6l, k7l, k8l, k9l,
       t0, t1, t2, t3, t4, t5, t6, t7, t8, t9,
       k0ls, k1ls, k2ls, k3ls, k4ls, k5ls, k6ls, k7ls, k8ls, k9ls,
       t0s, t1s, t2s, t3s, t4s, t5s, t6s, t7s, t8s, t9s,
       ks, hkick, vkick;

  Real kml, ango2;
  Integer pole, skew, vecnum;
  Vector(Real) kl;
  Vector(Integer) vecpole, vecskew;
  Real sum, suml;
  Integer fringeIN, fringeOUT;

  MacroPart *mp;
  mp = MacroPart::safeCast
       (mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  kl.resize(10);
  vecpole.resize(10);
  vecskew.resize(10);

  IFstream fio1(MADLATFile, ios::in);
  if(fio1.good() != 1) except(badMADFile);

  fio1 >> TPelement0;
  TPelement = TPelement0;

  while(TPelement != "final")
  {
    if(TPelement == "final") {}
    else if(TPelement == "drift") {}
    else if(TPelement == "monitor") {}
    else if(TPelement == "rfcavity") {}
    else if(TPelement == "multipole") {}
    else if(TPelement == "octupole") {}
    else if(TPelement == "sextupole") {}
    else if(TPelement == "quadrupole") {}
    else if(TPelement == "sbend") {}
    else if(TPelement == "rbend") {}
    else if(TPelement == "solenoid") {}
    else if(TPelement == "kicker") {}
    else if(TPelement == "hkicker") {}
    else if(TPelement == "vkicker") {}
    else if(TPelement == "marker") {}
    else
    {
      cerr << "noTeaPotNode: TPelement == " << TPelement << "\n";
      except(noTeaPotNode);
    }

    fio1 >> TPlabel;
    while(TPlabel != "end")
    {
      if(TPlabel != "sub")
        fio1 >> TPvalue;
      else
        fio1 >> TPsubindex;
      fio1 >> TPlabel;
    }
    TPelementm = TPelement;
    fio1 >> TPelement;
  }
  fio1.close();

  // Open the files:

  IFstream fio(MADTwissFile, ios::in);
  if(fio.good() != 1) except(badMADFile);

  IFstream fio2(MADLATFile, ios::in);
  if(fio2.good() != 1) except(badMADFile);

  // Find number of elements

  line = getline(fio, test, c, 0);
  nElements  = atoi(test.between(59,63));
  line = getline(fio, test, c, 0);  // This line is empty
  line = getline(fio, test1, c, 0);
  line = getline(fio, test2, c, 0);
  line = getline(fio, test3, c, 0);
  line = getline(fio, test4, c, 0);
  line = getline(fio, test5, c, 0);
  elementName = test1.between(0,10);
  elementType = elementName.excpt(4,7);
  length = atof(test1.between(20,31));

  // set Ring parameters at beginning:

  Ring::alphaX = atof(test3.between(0,15));
  Ring::betaX  = atof(test3.between(16,31));
  Ring::alphaY = atof(test4.between(0,15));
  Ring::betaY  = atof(test4.between(16,31));
  Ring::etaX = atof(test3.between(48,63));
  Ring::etaPX = atof(test3.between(64,79));
  Ring::etaX = Ring::etaX * mp->_syncPart._betaSync;
  Ring::etaPX = Ring::etaPX * mp->_syncPart._betaSync;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  nRead = 0; // Index for number of Twiss sets read
  num = 10;  // Label index for each TeaPot element we add

  // Loop through file and construct a TeaPot node for each element

  fio2 >> TPelement;

  while(TPelement != "final")
  {
    // get the parameters of the element

    Choice = "";
    l = 0.0;
    angle = 0.0;
    e1 = 0.0;
    e2 = 0.0;
    tilt = 0.0;
    k1 = 0.0;
    k2 = 0.0;
    k3 = 0.0;
    k0l = 0.0;
    k1l = 0.0;
    k2l = 0.0;
    k3l = 0.0;
    k4l = 0.0;
    k5l = 0.0;
    k6l = 0.0;
    k7l = 0.0;
    k8l = 0.0;
    k9l = 0.0;
    t0 = 0.0;
    t1 = 0.0;
    t2 = 0.0;
    t3 = 0.0;
    t4 = 0.0;
    t5 = 0.0;
    t6 = 0.0;
    t7 = 0.0;
    t8 = 0.0;
    t9 = 0.0;
    k0ls = 0.0;
    k1ls = 0.0;
    k2ls = 0.0;
    k3ls = 0.0;
    k4ls = 0.0;
    k5ls = 0.0;
    k6ls = 0.0;
    k7ls = 0.0;
    k8ls = 0.0;
    k9ls = 0.0;
    t0s = 0.0;
    t1s = 0.0;
    t2s = 0.0;
    t3s = 0.0;
    t4s = 0.0;
    t5s = 0.0;
    t6s = 0.0;
    t7s = 0.0;
    t8s = 0.0;
    t9s = 0.0;
    ks = 0.0;
    hkick = 0.0;
    vkick = 0.0;
    TPsubindex = 1;

    kml = 0.0;
    pole = 0;
    skew = 0;
    vecnum = 0;
    kl = 0.0;
    vecpole = 0;
    vecskew = 0;
    sum = 0.0;
    suml = 0.0;
    fringeIN = 1;
    fringeOUT = 1;

    fio2 >> TPlabel;
    while(TPlabel != "end")
    {
      if(TPlabel != "sub")
        fio2 >> TPvalue;
      else
        fio2 >> TPsubindex;
      if(TPlabel == "l") l = TPvalue;
      if(TPlabel == "angle") angle = TPvalue;
      if(TPlabel == "e1") e1 = TPvalue;
      if(TPlabel == "e2") e2 = TPvalue;
      if(TPlabel == "tilt") tilt = TPvalue;
      if(TPlabel == "k1") k1 = TPvalue;
      if(TPlabel == "k2") k2 = TPvalue;
      if(TPlabel == "k3") k3 = TPvalue;
      if(TPlabel == "k0l") k0l = TPvalue;
      if(TPlabel == "k1l") k1l = TPvalue;
      if(TPlabel == "k2l") k2l = TPvalue;
      if(TPlabel == "k3l") k3l = TPvalue;
      if(TPlabel == "k4l") k4l = TPvalue;
      if(TPlabel == "k5l") k5l = TPvalue;
      if(TPlabel == "k6l") k6l = TPvalue;
      if(TPlabel == "k7l") k7l = TPvalue;
      if(TPlabel == "k8l") k8l = TPvalue;
      if(TPlabel == "k9l") k9l = TPvalue;
      if(TPlabel == "t0") t0 = TPvalue;
      if(TPlabel == "t1") t1 = TPvalue;
      if(TPlabel == "t2") t2 = TPvalue;
      if(TPlabel == "t3") t3 = TPvalue;
      if(TPlabel == "t4") t4 = TPvalue;
      if(TPlabel == "t5") t5 = TPvalue;
      if(TPlabel == "t6") t6 = TPvalue;
      if(TPlabel == "t7") t7 = TPvalue;
      if(TPlabel == "t8") t8 = TPvalue;
      if(TPlabel == "t9") t9 = TPvalue;
      if(TPlabel == "k0ls") k0ls = TPvalue;
      if(TPlabel == "k1ls") k1ls = TPvalue;
      if(TPlabel == "k2ls") k2ls = TPvalue;
      if(TPlabel == "k3ls") k3ls = TPvalue;
      if(TPlabel == "k4ls") k4ls = TPvalue;
      if(TPlabel == "k5ls") k5ls = TPvalue;
      if(TPlabel == "k6ls") k6ls = TPvalue;
      if(TPlabel == "k7ls") k7ls = TPvalue;
      if(TPlabel == "k8ls") k8ls = TPvalue;
      if(TPlabel == "k9ls") k9ls = TPvalue;
      if(TPlabel == "t0s") t0s = TPvalue;
      if(TPlabel == "t1s") t1s = TPvalue;
      if(TPlabel == "t2s") t2s = TPvalue;
      if(TPlabel == "t3s") t3s = TPvalue;
      if(TPlabel == "t4s") t4s = TPvalue;
      if(TPlabel == "t5s") t5s = TPvalue;
      if(TPlabel == "t6s") t6s = TPvalue;
      if(TPlabel == "t7s") t7s = TPvalue;
      if(TPlabel == "t8s") t8s = TPvalue;
      if(TPlabel == "t9s") t9s = TPvalue;
      if(TPlabel == "ks") ks = TPvalue;
      if((TPelement == "hkicker") && (TPlabel == "kick"))
        hkick =  TPvalue;
      if((TPelement == "vkicker") && (TPlabel == "kick"))
        vkick =  TPvalue;
      if(TPelement == "kicker")
      {
        if(TPlabel == "hkick") hkick =  TPvalue;
        if(TPlabel == "vkick") vkick =  TPvalue;
      }
      fio2 >> TPlabel;
    }
    //    if(Abs(angle) <= Consts::tiny) angle = k0l;
    if(Abs(k0l) <= Consts::tiny) k0l = angle;
    if(Abs(k1l) <= Consts::tiny) k1l = k1 * l;
    if(Abs(k2l) <= Consts::tiny) k2l = k2 * l;
    if(Abs(k3l) <= Consts::tiny) k3l = k3 * l;

    if(Abs(k0l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k0l;
        pole = 0;
        if(Abs(t0) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k0ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k0ls;
        pole = 0;
        if(Abs(t0s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k1l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k1l;
        pole = 1;
        if(Abs(t1) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k1ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k1ls;
        pole = 1;
        if(Abs(t1s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k2l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k2l;
        pole = 2;
        if(Abs(t2) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k2ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k2ls;
        pole = 2;
        if(Abs(t2s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k3l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k3l;
        pole = 3;
        if(Abs(t3) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k3ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k3ls;
        pole = 3;
        if(Abs(t3s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k4l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k4l;
        pole = 4;
        if(Abs(t4) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k4ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k4ls;
        pole = 4;
        if(Abs(t4s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k5l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k5l;
        pole = 5;
        if(Abs(t5) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k5ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k5ls;
        pole = 5;
        if(Abs(t5s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k6l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k6l;
        pole = 6;
        if(Abs(t6) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k6ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k6ls;
        pole = 6;
        if(Abs(t6s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k7l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k7l;
        pole = 7;
        if(Abs(t7) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k7ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k7ls;
        pole = 7;
        if(Abs(t7s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k8l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k8l;
        pole = 8;
        if(Abs(t8) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k8ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k8ls;
        pole = 8;
        if(Abs(t8s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k9l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k9l;
        pole = 9;
        if(Abs(t9) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k9ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k9ls;
        pole = 9;
        if(Abs(t9s) > Consts::tiny) skew = 1;
      }
    }

    sum  = Abs(angle) * (Abs(k1) + Abs(k2) + Abs(k3)) +
              Abs(k1) * (Abs(k2) + Abs(k3)) +
              Abs(k2) * (Abs(k3));
    suml = Abs(angle) * (Abs(k1l)  + Abs(k2l)  + Abs(k3l)  +
                         Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k0ls) +
                         Abs(k1ls) + Abs(k2ls) + Abs(k3ls) +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          +  Abs(k0l) * (Abs(k1l)  + Abs(k2l)  + Abs(k3l)  +
                         Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k0ls) +
                         Abs(k1ls) + Abs(k2ls) + Abs(k3ls) +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          + Abs(k0ls) * (Abs(k1l)  + Abs(k2l)  + Abs(k3l)  +
                         Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k1ls) + Abs(k2ls) + Abs(k3ls) +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          +  Abs(k1l) * (Abs(k2l)  + Abs(k3l)  + Abs(k4l)  +
                         Abs(k5l)  + Abs(k6l)  + Abs(k7l)  +
                         Abs(k8l)  + Abs(k9l)  +
                         Abs(k1ls) +
                         Abs(k2ls) + Abs(k3ls) + Abs(k4ls) +
                         Abs(k5ls) + Abs(k6ls) + Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          + Abs(k1ls) * (Abs(k2l)  + Abs(k3l)  + Abs(k4l)  +
                         Abs(k5l)  + Abs(k6l)  + Abs(k7l)  +
                         Abs(k8l)  + Abs(k9l)  +
                         Abs(k2ls) + Abs(k3ls) + Abs(k4ls) +
                         Abs(k5ls) + Abs(k6ls) + Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          +  Abs(k2l) * (Abs(k3l)  + Abs(k4l)  + Abs(k5l)  +
                         Abs(k6l)  + Abs(k7l)  + Abs(k8l)  +
                         Abs(k9l)  +
                         Abs(k2ls) +
                         Abs(k3ls) + Abs(k4ls) + Abs(k5ls) +
                         Abs(k6ls) + Abs(k7ls) + Abs(k8ls) +
                         Abs(k9ls))
          + Abs(k2ls) * (Abs(k3l)  + Abs(k4l)  + Abs(k5l)  +
                         Abs(k6l)  + Abs(k7l)  + Abs(k8l)  +
                         Abs(k9l)  +
                         Abs(k3ls) + Abs(k4ls) + Abs(k5ls) +
                         Abs(k6ls) + Abs(k7ls) + Abs(k8ls) +
                         Abs(k9ls))
          +  Abs(k3l) * (Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k3ls) +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          + Abs(k3ls) * (Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          +  Abs(k4l) * (Abs(k5l)  + Abs(k6l)  + Abs(k7l)  +
                         Abs(k8l)  + Abs(k9l)  +
                         Abs(k4ls) +
                         Abs(k5ls) + Abs(k6ls) + Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          + Abs(k4ls) * (Abs(k5l)  + Abs(k6l)  + Abs(k7l)  +
                         Abs(k8l)  + Abs(k9l)  +
                         Abs(k5ls) + Abs(k6ls) + Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          +  Abs(k5l) * (Abs(k6l)  + Abs(k7l)  + Abs(k8l)  +
                         Abs(k9l)  +
                         Abs(k5ls) +
                         Abs(k6ls) + Abs(k7ls) + Abs(k8ls) +
                         Abs(k9ls))
          + Abs(k5ls) * (Abs(k6l)  + Abs(k7l)  + Abs(k8l)  +
                         Abs(k9l)  +
                         Abs(k6ls) + Abs(k7ls) + Abs(k8ls) +
                         Abs(k9ls))
          +  Abs(k6l) * (Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          + Abs(k6ls) * (Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          +  Abs(k7l) * (Abs(k8l)  + Abs(k9l)  +
                         Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          + Abs(k7ls) * (Abs(k8l)  + Abs(k9l)  +
                         Abs(k8ls) + Abs(k9ls))
          +  Abs(k8l) * (Abs(k9l)  +
                         Abs(k8ls) +
                         Abs(k9ls))
          + Abs(k8ls) * (Abs(k9l)  +
                         Abs(k9ls))
          + Abs(k9l)  * (Abs(k9ls));

    fio2 >> TPelementh;
    TPelementp = TPelementh;
    if(TPelementh == "final") TPelementp = TPelement0;

    fringeIN = 1;
    fringeOUT = 1;
    if(TPelementm == TPelement) fringeIN = 0;
    if(TPelement == TPelementp) fringeOUT = 0;

    // find corresponding twiss parameters

    line = getline(fio, test1, c, 0);
    line = getline(fio, test2, c, 0);
    line = getline(fio, test3, c, 0);
    line = getline(fio, test4, c, 0);
    line = getline(fio, test5, c, 0);
    elementName = test1.between(0,10);
    elementType = elementName.excpt(4,7);
    length = atof(test1.between(20,31));
    if(fio.eof()) except(EOFTwiss);

    alpha_X = atof(test3.between(0,15));
    alpha_Y = atof(test4.between(0,15));
    beta_X = atof(test3.between(16,31));
    beta_Y = atof(test4.between(16,31));
    phiX = atof(test3.between(32,47))/twoPi;
    phiY = atof(test4.between(32,47))/twoPi;
    eta_X = atof(test3.between(48,63));
    etap_X = atof(test3.between(64,79));
    eta_X = eta_X * mp->_syncPart._betaSync;
    etap_X = etap_X * mp->_syncPart._betaSync;
    sPos = atof(test1.between(64,79));  // Don't use this here

    if(Abs(l) <= Consts::tiny)
    {
      if(Abs(kml) > Consts::tiny)
      {
        if(Abs(suml) <= Consts::tiny) Choice = Mult;
        if(Abs(suml) >  Consts::tiny) Choice = MultCF;
      }
      else if((Abs(hkick) > Consts::tiny) ||
         (Abs(vkick) > Consts::tiny)) Choice = Kick;
      else if(TPelement == "drift") Choice = Drift;
      else if(TPelement == "rfcavity") Choice = Drift;
      else if(TPelement == "multipole") Choice = Mult;
      else if(TPelement == "octupole") Choice = Mult;
      else if(TPelement == "sextupole") Choice = Mult;
      else if(TPelement == "quadrupole") Choice = Mult;
      else if(TPelement == "sbend") Choice = Mult;
      else if(TPelement == "rbend") Choice = Mult;
      else if(TPelement == "solenoid") Choice = Soln;
      else if(TPelement == "kicker") Choice = Kick;
      else if(TPelement == "hkicker") Choice = Kick;
      else if(TPelement == "vkicker") Choice = Kick;
    }

    if(Abs(l) > Consts::tiny)
    {
      if(Abs(angle) > Consts::tiny)
      {
        if(TPelement == "rbend")
        {
          ango2 = angle / 2.0;
          l = l * ango2 / sin(ango2);
          e1 = e1 + ango2;
          e2 = e2 + ango2;
        }
        if(Abs(suml) <= Consts::tiny) Choice = Bend;
        if(Abs(suml) >  Consts::tiny) Choice = BendCF;
      }
      else if(Abs(k1) > Consts::tiny)
      {
        if(Abs(suml) <= Consts::tiny) Choice = Quad;
        if(Abs(suml) >  Consts::tiny) Choice = QuadCF;
      }
      else if(Abs(kml) > Consts::tiny)
      {
        if(Abs(suml) <= Consts::tiny) Choice = Mult;
        if(Abs(suml) >  Consts::tiny) Choice = MultCF;
      }
      else if(Abs(ks) > Consts::tiny)
      {
        Choice = Soln;
      }
      else if((Abs(hkick) > Consts::tiny) ||
              (Abs(vkick) > Consts::tiny))
      {
        Choice = Kick;
      }
      else
      {
        Choice = Drift;
      }
    }

    if(Choice == Drift)
    {
      addTPD(Drift, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement,
             nstepTPD);
      num += 10;
      nRead++;
    }

    if(Choice == Mult)
    {
      if(fringeM == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPM(Mult, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, tilt, pole, kml, skew,
             TPsubindex, nstepTPM, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == MultCF)
    {
      if(Abs(k0l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k0l;
        vecpole(vecnum) = 0;
        if(Abs(t0) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k0ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k0ls;
        vecpole(vecnum) = 0;
        if(Abs(t0s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k1l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1l;
        vecpole(vecnum) = 1;
        if(Abs(t1) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k1ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1ls;
        vecpole(vecnum) = 1;
        if(Abs(t1s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2l;
        vecpole(vecnum) = 2;
        if(Abs(t2) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2ls;
        vecpole(vecnum) = 2;
        if(Abs(t2s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3l;
        vecpole(vecnum) = 3;
        if(Abs(t3) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3ls;
        vecpole(vecnum) = 3;
        if(Abs(t3s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4l;
        vecpole(vecnum) = 4;
        if(Abs(t4) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4ls;
        vecpole(vecnum) = 4;
        if(Abs(t4s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5l;
        vecpole(vecnum) = 5;
        if(Abs(t5) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5ls;
        vecpole(vecnum) = 5;
        if(Abs(t5s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6l;
        vecpole(vecnum) = 6;
        if(Abs(t6) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6ls;
        vecpole(vecnum) = 6;
        if(Abs(t6s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7l;
        vecpole(vecnum) = 7;
        if(Abs(t7) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7ls;
        vecpole(vecnum) = 7;
        if(Abs(t7s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8l;
        vecpole(vecnum) = 8;
        if(Abs(t8) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8ls;
        vecpole(vecnum) = 8;
        if(Abs(t8s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9l;
        vecpole(vecnum) = 9;
        if(Abs(t9) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9ls;
        vecpole(vecnum) = 9;
        if(Abs(t9s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(fringeM == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPMCF(MultCF, num,
               beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
               l, TPelement, tilt,
               vecnum, vecpole, kl, vecskew,
               TPsubindex, nstepTPM, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == Quad)
    {
      if(fringeQ == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPQ(Quad, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, tilt, k1,
             TPsubindex, nstepTPQ, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == QuadCF)
    {
      if(Abs(k1ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1ls;
        vecpole(vecnum) = 1;
        if(Abs(t1s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2l;
        vecpole(vecnum) = 2;
        if(Abs(t2) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2ls;
        vecpole(vecnum) = 2;
        if(Abs(t2s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3l;
        vecpole(vecnum) = 3;
        if(Abs(t3) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3ls;
        vecpole(vecnum) = 3;
        if(Abs(t3s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4l;
        vecpole(vecnum) = 4;
        if(Abs(t4) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4ls;
        vecpole(vecnum) = 4;
        if(Abs(t4s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5l;
        vecpole(vecnum) = 5;
        if(Abs(t5) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5ls;
        vecpole(vecnum) = 5;
        if(Abs(t5s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6l;
        vecpole(vecnum) = 6;
        if(Abs(t6) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6ls;
        vecpole(vecnum) = 6;
        if(Abs(t6s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7l;
      vecpole(vecnum) = 7;
      if(Abs(t7) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7ls;
      vecpole(vecnum) = 7;
      if(Abs(t7s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8l;
        vecpole(vecnum) = 8;
        if(Abs(t8) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8ls;
        vecpole(vecnum) = 8;
        if(Abs(t8s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9l;
        vecpole(vecnum) = 9;
        if(Abs(t9) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9ls;
        vecpole(vecnum) = 9;
        if(Abs(t9s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(fringeQ == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPQCF(QuadCF, num,
               beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
               l, TPelement, tilt, k1,
               vecnum, vecpole, kl, vecskew,
               TPsubindex, nstepTPQ, fringeIN, fringeOUT);
      num += 10;
      nRead++;
  }

    if(Choice == Bend)
    {
      if(fringeB == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPB(Bend, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, tilt, angle, e1, e2,
             nstepTPB, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == BendCF)
    {
      if(Abs(k0ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k0ls;
        vecpole(vecnum) = 0;
        if(Abs(t0s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k1l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1l;
        vecpole(vecnum) = 1;
        if(Abs(t1) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k1ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1ls;
        vecpole(vecnum) = 1;
        if(Abs(t1s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2l;
        vecpole(vecnum) = 2;
        if(Abs(t2) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2ls;
        vecpole(vecnum) = 2;
        if(Abs(t2s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3l;
        vecpole(vecnum) = 3;
        if(Abs(t3) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3ls;
        vecpole(vecnum) = 3;
        if(Abs(t3s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4l;
        vecpole(vecnum) = 4;
        if(Abs(t4) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4ls;
        vecpole(vecnum) = 4;
        if(Abs(t4s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5l;
        vecpole(vecnum) = 5;
        if(Abs(t5) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5ls;
        vecpole(vecnum) = 5;
        if(Abs(t5s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6l;
        vecpole(vecnum) = 6;
        if(Abs(t6) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6ls;
        vecpole(vecnum) = 6;
        if(Abs(t6s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7l;
        vecpole(vecnum) = 7;
        if(Abs(t7) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7ls;
        vecpole(vecnum) = 7;
        if(Abs(t7s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8l;
        vecpole(vecnum) = 8;
        if(Abs(t8) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8ls;
        vecpole(vecnum) = 8;
        if(Abs(t8s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9l;
        vecpole(vecnum) = 9;
        if(Abs(t9) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9ls;
        vecpole(vecnum) = 9;
        if(Abs(t9s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(fringeB == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPBCF(BendCF, num,
               beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
               l, TPelement, tilt, angle, e1, e2,
               vecnum, vecpole, kl, vecskew,
               nstepTPB, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == Soln)
    {
      addTPS(Soln, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, ks,
             TPsubindex, nstepTPS);
      num += 10;
      nRead++;
    }

    if(Choice == Kick)
    {
      addTPK(Kick, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, hkick, vkick,
             TPsubindex, nstepTPK);

      num += 10;
      nRead++;
    }

    TPelementm = TPelement;
    TPelement = TPelementh;
  }

  fio.close();
  fio2.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::buildTPLatticeNew
//
// DESCRIPTION
//   and reads parsed .LAT files
//   in order to construct a TeaPot lattice.
//
// PARAMETERS
//   LATFile - The name of the parsed .LAT file to read
//   nsteps - Number of TeaPot nsteps for elements
//   fringe - Hard edge for multipoles, quads, and bends:
//            1:Fringe Fields or 0: No Fringe Fields
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::buildTPlatticeNew(const String &LATFile,
                               const String &ring_line,
                               const Integer &nstepTPD,
                               const Integer &nstepTPM,
                               const Integer &fringeM,
                               const Integer &nstepTPQ,
                               const Integer &fringeQ,
                               const Integer &nstepTPB,
                               const Integer &fringeB,
                               const Integer &nstepTPS,
                               const Integer &nstepTPK)
{
  // Local variables:

  String elementName, elementType;
  Integer line, num, nRead;

  char c='\n';
  String Drift  = "Drift";
  String Mult   = "Mult";
  String MultCF = "MultCF";
  String Quad   = "Quad";
  String QuadCF = "QuadCF";
  String Bend   = "Bend";
  String BendCF = "BendCF";
  String Soln   = "Soln";
  String Kick   = "Kick";
  String Choice = "";

  Integer nElements;

  Real alpha_X, alpha_Y, beta_X, beta_Y, eta_X, etap_X;

  String TPelement, TPelementm, TPelementp,
                    TPelementh, TPelement0;
  String TPlabel;
  Real TPvalue;
  Integer TPsubindex;

  Real l,
       angle, e1, e2, tilt,
       k1, k2, k3,
       k0l, k1l, k2l, k3l, k4l, k5l, k6l, k7l, k8l, k9l,
       t0, t1, t2, t3, t4, t5, t6, t7, t8, t9,
       k0ls, k1ls, k2ls, k3ls, k4ls, k5ls, k6ls, k7ls, k8ls, k9ls,
       t0s, t1s, t2s, t3s, t4s, t5s, t6s, t7s, t8s, t9s,
       ks, hkick, vkick;

  Real kml, ango2;
  Integer pole, skew, vecnum;
  Vector(Real) kl;
  Vector(Integer) vecpole, vecskew;
  Real sum, suml;
  Integer fringeIN, fringeOUT;

  MacroPart *mp;
  mp = MacroPart::safeCast
       (mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  kl.resize(10);
  vecpole.resize(10);
  vecskew.resize(10);

  // Open the files and test for OK:

  IFstream fio1(LATFile, ios::in);
  if(fio1.good() != 1) except(badMADFile);

  fio1 >> TPelement0;
  TPelement = TPelement0;

  while(TPelement != "final")
  {
    if(TPelement == "final") {}
    else if(TPelement == "drift") {}
    else if(TPelement == "monitor") {}
    else if(TPelement == "rfcavity") {}
    else if(TPelement == "multipole") {}
    else if(TPelement == "octupole") {}
    else if(TPelement == "sextupole") {}
    else if(TPelement == "quadrupole") {}
    else if(TPelement == "sbend") {}
    else if(TPelement == "rbend") {}
    else if(TPelement == "solenoid") {}
    else if(TPelement == "kicker") {}
    else if(TPelement == "hkicker") {}
    else if(TPelement == "vkicker") {}
    else if(TPelement == "marker") {}
    else
    {
      cerr << "noTeaPotNode: TPelement == " << TPelement << "\n";
      except(noTeaPotNode);
    }

    fio1 >> TPlabel;
    while(TPlabel != "end")
    {
      if(TPlabel != "sub")
        fio1 >> TPvalue;
      else
        fio1 >> TPsubindex;
      fio1 >> TPlabel;
    }
    TPelementm = TPelement;
    fio1 >> TPelement;
  }
  fio1.close();

  // Now Loop through file and construct a TeaPot node for each element

  IFstream fio2(LATFile, ios::in);
  if(fio2.good() != 1) except(badMADFile);

  num = 10;  // Label index for each TeaPot element we add

  // Loop through file and construct a TeaPot node for each element

  fio2 >> TPelement;

  while(TPelement != "final")
  {
    // get the parameters of the element

    Choice = "";
    l = 0.0;
    angle = 0.0;
    e1 = 0.0;
    e2 = 0.0;
    tilt = 0.0;
    k1 = 0.0;
    k2 = 0.0;
    k3 = 0.0;
    k0l = 0.0;
    k1l = 0.0;
    k2l = 0.0;
    k3l = 0.0;
    k4l = 0.0;
    k5l = 0.0;
    k6l = 0.0;
    k7l = 0.0;
    k8l = 0.0;
    k9l = 0.0;
    t0 = 0.0;
    t1 = 0.0;
    t2 = 0.0;
    t3 = 0.0;
    t4 = 0.0;
    t5 = 0.0;
    t6 = 0.0;
    t7 = 0.0;
    t8 = 0.0;
    t9 = 0.0;
    k0ls = 0.0;
    k1ls = 0.0;
    k2ls = 0.0;
    k3ls = 0.0;
    k4ls = 0.0;
    k5ls = 0.0;
    k6ls = 0.0;
    k7ls = 0.0;
    k8ls = 0.0;
    k9ls = 0.0;
    t0s = 0.0;
    t1s = 0.0;
    t2s = 0.0;
    t3s = 0.0;
    t4s = 0.0;
    t5s = 0.0;
    t6s = 0.0;
    t7s = 0.0;
    t8s = 0.0;
    t9s = 0.0;
    ks = 0.0;
    hkick = 0.0;
    vkick = 0.0;
    TPsubindex = 1;

    kml = 0.0;
    pole = 0;
    skew = 0;
    vecnum = 0;
    kl = 0.0;
    vecpole = 0;
    vecskew = 0;
    sum = 0.0;
    suml = 0.0;
    fringeIN = 1;
    fringeOUT = 1;

    fio2 >> TPlabel;
    while(TPlabel != "end")
    {
      if(TPlabel != "sub")
        fio2 >> TPvalue;
      else
        fio2 >> TPsubindex;
      if(TPlabel == "l") l = TPvalue;
      if(TPlabel == "angle") angle = TPvalue;
      if(TPlabel == "e1") e1 = TPvalue;
      if(TPlabel == "e2") e2 = TPvalue;
      if(TPlabel == "tilt") tilt = TPvalue;
      if(TPlabel == "k1") k1 = TPvalue;
      if(TPlabel == "k2") k2 = TPvalue;
      if(TPlabel == "k3") k3 = TPvalue;
      if(TPlabel == "k0l") k0l = TPvalue;
      if(TPlabel == "k1l") k1l = TPvalue;
      if(TPlabel == "k2l") k2l = TPvalue;
      if(TPlabel == "k3l") k3l = TPvalue;
      if(TPlabel == "k4l") k4l = TPvalue;
      if(TPlabel == "k5l") k5l = TPvalue;
      if(TPlabel == "k6l") k6l = TPvalue;
      if(TPlabel == "k7l") k7l = TPvalue;
      if(TPlabel == "k8l") k8l = TPvalue;
      if(TPlabel == "k9l") k9l = TPvalue;
      if(TPlabel == "t0") t0 = TPvalue;
      if(TPlabel == "t1") t1 = TPvalue;
      if(TPlabel == "t2") t2 = TPvalue;
      if(TPlabel == "t3") t3 = TPvalue;
      if(TPlabel == "t4") t4 = TPvalue;
      if(TPlabel == "t5") t5 = TPvalue;
      if(TPlabel == "t6") t6 = TPvalue;
      if(TPlabel == "t7") t7 = TPvalue;
      if(TPlabel == "t8") t8 = TPvalue;
      if(TPlabel == "t9") t9 = TPvalue;
      if(TPlabel == "k0ls") k0ls = TPvalue;
      if(TPlabel == "k1ls") k1ls = TPvalue;
      if(TPlabel == "k2ls") k2ls = TPvalue;
      if(TPlabel == "k3ls") k3ls = TPvalue;
      if(TPlabel == "k4ls") k4ls = TPvalue;
      if(TPlabel == "k5ls") k5ls = TPvalue;
      if(TPlabel == "k6ls") k6ls = TPvalue;
      if(TPlabel == "k7ls") k7ls = TPvalue;
      if(TPlabel == "k8ls") k8ls = TPvalue;
      if(TPlabel == "k9ls") k9ls = TPvalue;
      if(TPlabel == "t0s") t0s = TPvalue;
      if(TPlabel == "t1s") t1s = TPvalue;
      if(TPlabel == "t2s") t2s = TPvalue;
      if(TPlabel == "t3s") t3s = TPvalue;
      if(TPlabel == "t4s") t4s = TPvalue;
      if(TPlabel == "t5s") t5s = TPvalue;
      if(TPlabel == "t6s") t6s = TPvalue;
      if(TPlabel == "t7s") t7s = TPvalue;
      if(TPlabel == "t8s") t8s = TPvalue;
      if(TPlabel == "t9s") t9s = TPvalue;
      if(TPlabel == "ks") ks = TPvalue;
      if((TPelement == "hkicker") && (TPlabel == "kick"))
        hkick =  TPvalue;
      if((TPelement == "vkicker") && (TPlabel == "kick"))
        vkick =  TPvalue;
      if(TPelement == "kicker")
      {
        if(TPlabel == "hkick") hkick =  TPvalue;
        if(TPlabel == "vkick") vkick =  TPvalue;
      }
      fio2 >> TPlabel;
    }
    //    if(Abs(angle) <= Consts::tiny) angle = k0l;
    if(Abs(k0l) <= Consts::tiny) k0l = angle;
    if(Abs(k1l) <= Consts::tiny) k1l = k1 * l;
    if(Abs(k2l) <= Consts::tiny) k2l = k2 * l;
    if(Abs(k3l) <= Consts::tiny) k3l = k3 * l;

    if(Abs(k0l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k0l;
        pole = 0;
        if(Abs(t0) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k0ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k0ls;
        pole = 0;
        if(Abs(t0s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k1l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k1l;
        pole = 1;
        if(Abs(t1) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k1ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k1ls;
        pole = 1;
        if(Abs(t1s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k2l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k2l;
        pole = 2;
        if(Abs(t2) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k2ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k2ls;
        pole = 2;
        if(Abs(t2s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k3l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k3l;
        pole = 3;
        if(Abs(t3) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k3ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k3ls;
        pole = 3;
        if(Abs(t3s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k4l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k4l;
        pole = 4;
        if(Abs(t4) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k4ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k4ls;
        pole = 4;
        if(Abs(t4s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k5l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k5l;
        pole = 5;
        if(Abs(t5) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k5ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k5ls;
        pole = 5;
        if(Abs(t5s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k6l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k6l;
        pole = 6;
        if(Abs(t6) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k6ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k6ls;
        pole = 6;
        if(Abs(t6s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k7l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k7l;
        pole = 7;
        if(Abs(t7) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k7ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k7ls;
        pole = 7;
        if(Abs(t7s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k8l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k8l;
        pole = 8;
        if(Abs(t8) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k8ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k8ls;
        pole = 8;
        if(Abs(t8s) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k9l) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k9l;
        pole = 9;
        if(Abs(t9) > Consts::tiny) skew = 1;
      }
    }
    if(Abs(k9ls) > Consts::tiny)
    {
      if(Abs(kml) <= Consts::tiny)
      {
        kml = k9ls;
        pole = 9;
        if(Abs(t9s) > Consts::tiny) skew = 1;
      }
    }

    sum  = Abs(angle) * (Abs(k1) + Abs(k2) + Abs(k3)) +
              Abs(k1) * (Abs(k2) + Abs(k3)) +
              Abs(k2) * (Abs(k3));
    suml = Abs(angle) * (Abs(k1l)  + Abs(k2l)  + Abs(k3l)  +
                         Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k0ls) +
                         Abs(k1ls) + Abs(k2ls) + Abs(k3ls) +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          +  Abs(k0l) * (Abs(k1l)  + Abs(k2l)  + Abs(k3l)  +
                         Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k0ls) +
                         Abs(k1ls) + Abs(k2ls) + Abs(k3ls) +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          + Abs(k0ls) * (Abs(k1l)  + Abs(k2l)  + Abs(k3l)  +
                         Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k1ls) + Abs(k2ls) + Abs(k3ls) +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          +  Abs(k1l) * (Abs(k2l)  + Abs(k3l)  + Abs(k4l)  +
                         Abs(k5l)  + Abs(k6l)  + Abs(k7l)  +
                         Abs(k8l)  + Abs(k9l)  +
                         Abs(k1ls) +
                         Abs(k2ls) + Abs(k3ls) + Abs(k4ls) +
                         Abs(k5ls) + Abs(k6ls) + Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          + Abs(k1ls) * (Abs(k2l)  + Abs(k3l)  + Abs(k4l)  +
                         Abs(k5l)  + Abs(k6l)  + Abs(k7l)  +
                         Abs(k8l)  + Abs(k9l)  +
                         Abs(k2ls) + Abs(k3ls) + Abs(k4ls) +
                         Abs(k5ls) + Abs(k6ls) + Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          +  Abs(k2l) * (Abs(k3l)  + Abs(k4l)  + Abs(k5l)  +
                         Abs(k6l)  + Abs(k7l)  + Abs(k8l)  +
                         Abs(k9l)  +
                         Abs(k2ls) +
                         Abs(k3ls) + Abs(k4ls) + Abs(k5ls) +
                         Abs(k6ls) + Abs(k7ls) + Abs(k8ls) +
                         Abs(k9ls))
          + Abs(k2ls) * (Abs(k3l)  + Abs(k4l)  + Abs(k5l)  +
                         Abs(k6l)  + Abs(k7l)  + Abs(k8l)  +
                         Abs(k9l)  +
                         Abs(k3ls) + Abs(k4ls) + Abs(k5ls) +
                         Abs(k6ls) + Abs(k7ls) + Abs(k8ls) +
                         Abs(k9ls))
          +  Abs(k3l) * (Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k3ls) +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          + Abs(k3ls) * (Abs(k4l)  + Abs(k5l)  + Abs(k6l)  +
                         Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k4ls) + Abs(k5ls) + Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          +  Abs(k4l) * (Abs(k5l)  + Abs(k6l)  + Abs(k7l)  +
                         Abs(k8l)  + Abs(k9l)  +
                         Abs(k4ls) +
                         Abs(k5ls) + Abs(k6ls) + Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          + Abs(k4ls) * (Abs(k5l)  + Abs(k6l)  + Abs(k7l)  +
                         Abs(k8l)  + Abs(k9l)  +
                         Abs(k5ls) + Abs(k6ls) + Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          +  Abs(k5l) * (Abs(k6l)  + Abs(k7l)  + Abs(k8l)  +
                         Abs(k9l)  +
                         Abs(k5ls) +
                         Abs(k6ls) + Abs(k7ls) + Abs(k8ls) +
                         Abs(k9ls))
          + Abs(k5ls) * (Abs(k6l)  + Abs(k7l)  + Abs(k8l)  +
                         Abs(k9l)  +
                         Abs(k6ls) + Abs(k7ls) + Abs(k8ls) +
                         Abs(k9ls))
          +  Abs(k6l) * (Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k6ls) +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          + Abs(k6ls) * (Abs(k7l)  + Abs(k8l)  + Abs(k9l)  +
                         Abs(k7ls) + Abs(k8ls) + Abs(k9ls))
          +  Abs(k7l) * (Abs(k8l)  + Abs(k9l)  +
                         Abs(k7ls) +
                         Abs(k8ls) + Abs(k9ls))
          + Abs(k7ls) * (Abs(k8l)  + Abs(k9l)  +
                         Abs(k8ls) + Abs(k9ls))
          +  Abs(k8l) * (Abs(k9l)  +
                         Abs(k8ls) +
                         Abs(k9ls))
          + Abs(k8ls) * (Abs(k9l)  +
                         Abs(k9ls))
          + Abs(k9l)  * (Abs(k9ls));

    fio2 >> TPelementh;
    TPelementp = TPelementh;
    if(TPelementh == "final") TPelementp = TPelement0;

    fringeIN = 1;
    fringeOUT = 1;
    if(TPelementm == TPelement) fringeIN = 0;
    if(TPelement == TPelementp) fringeOUT = 0;

    alpha_X = Ring::alphaX;
    alpha_Y = Ring::alphaY;
    beta_X  = Ring::betaX;
    beta_Y  = Ring::betaY;
    eta_X   = Ring::etaX;
    etap_X  = Ring::etaPX;

    if(Abs(l) <= Consts::tiny)
    {
      if(Abs(kml) > Consts::tiny)
      {
        if(Abs(suml) <= Consts::tiny) Choice = Mult;
        if(Abs(suml) >  Consts::tiny) Choice = MultCF;
      }
      else if((Abs(hkick) > Consts::tiny) ||
         (Abs(vkick) > Consts::tiny)) Choice = Kick;
      else if(TPelement == "drift") Choice = Drift;
      else if(TPelement == "rfcavity") Choice = Drift;
      else if(TPelement == "multipole") Choice = Mult;
      else if(TPelement == "octupole") Choice = Mult;
      else if(TPelement == "sextupole") Choice = Mult;
      else if(TPelement == "quadrupole") Choice = Mult;
      else if(TPelement == "sbend") Choice = Mult;
      else if(TPelement == "rbend") Choice = Mult;
      else if(TPelement == "solenoid") Choice = Soln;
      else if(TPelement == "kicker") Choice = Kick;
      else if(TPelement == "hkicker") Choice = Kick;
      else if(TPelement == "vkicker") Choice = Kick;
    }

    if(Abs(l) > Consts::tiny)
    {
      if(Abs(angle) > Consts::tiny)
      {
        if(TPelement == "rbend")
        {
          ango2 = angle / 2.0;
          l = l * ango2 / sin(ango2);
          e1 = e1 + ango2;
          e2 = e2 + ango2;
        }
        if(Abs(suml) <= Consts::tiny) Choice = Bend;
        if(Abs(suml) >  Consts::tiny) Choice = BendCF;
      }
      else if(Abs(k1) > Consts::tiny)
      {
        if(Abs(suml) <= Consts::tiny) Choice = Quad;
        if(Abs(suml) >  Consts::tiny) Choice = QuadCF;
      }
      else if(Abs(kml) > Consts::tiny)
      {
        if(Abs(suml) <= Consts::tiny) Choice = Mult;
        if(Abs(suml) >  Consts::tiny) Choice = MultCF;
      }
      else if(Abs(ks) > Consts::tiny)
      {
        Choice = Soln;
      }
      else if((Abs(hkick) > Consts::tiny) ||
              (Abs(vkick) > Consts::tiny))
      {
        Choice = Kick;
      }
      else
      {
        Choice = Drift;
      }
    }

    if(Choice == Drift)
    {
      addTPD(Drift, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement,
             nstepTPD);
      num += 10;
      nRead++;
    }

    if(Choice == Mult)
    {
      if(fringeM == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPM(Mult, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, tilt, pole, kml, skew,
             TPsubindex, nstepTPM, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == MultCF)
    {
      if(Abs(k0l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k0l;
        vecpole(vecnum) = 0;
        if(Abs(t0) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k0ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k0ls;
        vecpole(vecnum) = 0;
        if(Abs(t0s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k1l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1l;
        vecpole(vecnum) = 1;
        if(Abs(t1) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k1ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1ls;
        vecpole(vecnum) = 1;
        if(Abs(t1s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2l;
        vecpole(vecnum) = 2;
        if(Abs(t2) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2ls;
        vecpole(vecnum) = 2;
        if(Abs(t2s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3l;
        vecpole(vecnum) = 3;
        if(Abs(t3) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3ls;
        vecpole(vecnum) = 3;
        if(Abs(t3s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4l;
        vecpole(vecnum) = 4;
        if(Abs(t4) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4ls;
        vecpole(vecnum) = 4;
        if(Abs(t4s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5l;
        vecpole(vecnum) = 5;
        if(Abs(t5) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5ls;
        vecpole(vecnum) = 5;
        if(Abs(t5s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6l;
        vecpole(vecnum) = 6;
        if(Abs(t6) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6ls;
        vecpole(vecnum) = 6;
        if(Abs(t6s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7l;
        vecpole(vecnum) = 7;
        if(Abs(t7) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7ls;
        vecpole(vecnum) = 7;
        if(Abs(t7s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8l;
        vecpole(vecnum) = 8;
        if(Abs(t8) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8ls;
        vecpole(vecnum) = 8;
        if(Abs(t8s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9l;
        vecpole(vecnum) = 9;
        if(Abs(t9) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9ls;
        vecpole(vecnum) = 9;
        if(Abs(t9s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(fringeM == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPMCF(MultCF, num,
               beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
               l, TPelement, tilt,
               vecnum, vecpole, kl, vecskew,
               TPsubindex, nstepTPM, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == Quad)
    {
      if(fringeQ == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPQ(Quad, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, tilt, k1,
             TPsubindex, nstepTPQ, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == QuadCF)
    {
      if(Abs(k1ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1ls;
        vecpole(vecnum) = 1;
        if(Abs(t1s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2l;
        vecpole(vecnum) = 2;
        if(Abs(t2) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2ls;
        vecpole(vecnum) = 2;
        if(Abs(t2s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3l;
        vecpole(vecnum) = 3;
        if(Abs(t3) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3ls;
        vecpole(vecnum) = 3;
        if(Abs(t3s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4l;
        vecpole(vecnum) = 4;
        if(Abs(t4) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4ls;
        vecpole(vecnum) = 4;
        if(Abs(t4s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5l;
        vecpole(vecnum) = 5;
        if(Abs(t5) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5ls;
        vecpole(vecnum) = 5;
        if(Abs(t5s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6l;
        vecpole(vecnum) = 6;
        if(Abs(t6) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6ls;
        vecpole(vecnum) = 6;
        if(Abs(t6s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7l;
      vecpole(vecnum) = 7;
      if(Abs(t7) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7ls;
      vecpole(vecnum) = 7;
      if(Abs(t7s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8l;
        vecpole(vecnum) = 8;
        if(Abs(t8) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8ls;
        vecpole(vecnum) = 8;
        if(Abs(t8s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9l;
        vecpole(vecnum) = 9;
        if(Abs(t9) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9ls;
        vecpole(vecnum) = 9;
        if(Abs(t9s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(fringeQ == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPQCF(QuadCF, num,
               beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
               l, TPelement, tilt, k1,
               vecnum, vecpole, kl, vecskew,
               TPsubindex, nstepTPQ, fringeIN, fringeOUT);
      num += 10;
      nRead++;
  }

    if(Choice == Bend)
    {
      if(fringeB == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPB(Bend, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, tilt, angle, e1, e2,
             nstepTPB, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == BendCF)
    {
      if(Abs(k0ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k0ls;
        vecpole(vecnum) = 0;
        if(Abs(t0s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k1l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1l;
        vecpole(vecnum) = 1;
        if(Abs(t1) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k1ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k1ls;
        vecpole(vecnum) = 1;
        if(Abs(t1s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2l;
        vecpole(vecnum) = 2;
        if(Abs(t2) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k2ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k2ls;
        vecpole(vecnum) = 2;
        if(Abs(t2s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3l;
        vecpole(vecnum) = 3;
        if(Abs(t3) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k3ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k3ls;
        vecpole(vecnum) = 3;
        if(Abs(t3s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4l;
        vecpole(vecnum) = 4;
        if(Abs(t4) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k4ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k4ls;
        vecpole(vecnum) = 4;
        if(Abs(t4s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5l;
        vecpole(vecnum) = 5;
        if(Abs(t5) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k5ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k5ls;
        vecpole(vecnum) = 5;
        if(Abs(t5s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6l;
        vecpole(vecnum) = 6;
        if(Abs(t6) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k6ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k6ls;
        vecpole(vecnum) = 6;
        if(Abs(t6s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7l;
        vecpole(vecnum) = 7;
        if(Abs(t7) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k7ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k7ls;
        vecpole(vecnum) = 7;
        if(Abs(t7s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8l;
        vecpole(vecnum) = 8;
        if(Abs(t8) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k8ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k8ls;
        vecpole(vecnum) = 8;
        if(Abs(t8s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9l) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9l;
        vecpole(vecnum) = 9;
        if(Abs(t9) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(Abs(k9ls) > Consts::tiny)
      {
        vecnum += 1;
        kl(vecnum) = k9ls;
        vecpole(vecnum) = 9;
        if(Abs(t9s) > Consts::tiny) vecskew(vecnum) = 1;
      }
      if(fringeB == 0)
      {
        fringeIN = 0;
        fringeOUT = 0;
      }
      addTPBCF(BendCF, num,
               beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
               l, TPelement, tilt, angle, e1, e2,
               vecnum, vecpole, kl, vecskew,
               nstepTPB, fringeIN, fringeOUT);
      num += 10;
      nRead++;
    }

    if(Choice == Soln)
    {
      addTPS(Soln, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, ks,
             TPsubindex, nstepTPS);
      num += 10;
      nRead++;
    }

    if(Choice == Kick)
    {
      addTPK(Kick, num,
             beta_X, beta_Y, alpha_X, alpha_Y, eta_X, etap_X,
             l, TPelement, hkick, vkick,
             TPsubindex, nstepTPK);

      num += 10;
      nRead++;
    }

    TPelementm = TPelement;
    TPelement = TPelementh;
  }

  fio2.close();

  Integer testHerd;
  testHerd = Particles::addMacroHerdBase(1, "testHerd"); // small test herd
  setHerdFeelLevel(testHerd, 1); // this herd only feels herds.
  addMacroPartBase2(testHerd, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  OFstream fio3("LatFuncs_Init", ios::out);
  TeaPot::calcLatFuncs(testHerd, ring_line, fio3);
  fio3.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TeaPot::showTP
//
// DESCRIPTION
//   Prints out the settings of the active TeaPot Nodes
//
// PARAMETERS
//   sout - The output stream
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::showTP(Ostream &sout)
{
  TPBase *tlp;

  if (nTPNodes == 0)
  {
    sout << "\n No TeaPot Nodes are present \n\n";
      return;
  }

  sout << "\n\n TeaPot Nodes present in ring:\n\n";

  for (Integer i=1; i<= nTPNodes; i++)
  {
    tlp = TPBase::safeCast(TPPointers(i-1));
    tlp->_showTP(sout);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TeaPot::dumpTPLAT
//
// DESCRIPTION
//   Dumps the TeaPot Lattice
//
// PARAMETERS
//   sout - The output stream
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::dumpTPLAT(Ostream &sout)
{
  TPBase *tp;
  TPD *tpd;
  TPM *tpm;
  TPMCF *tpmcf;
  TPQ *tpq;
  TPQCF *tpqcf;
  TPB *tpb;
  TPBCF *tpbcf;
  TPS *tps;
  TPK *tpk;

  String EleStr, TPStr("\nTP"), Zeros, Spacer = " &\n";
  char numStr[5];
  String Drift  = "Drift";
  String Mult   = "Mult";
  String MultCF = "MultCF";
  String Quad   = "Quad";
  String QuadCF = "QuadCF";
  String Bend   = "Bend";
  String BendCF = "BendCF";
  String Soln   = "Soln";
  String Kick   = "Kick";

  String lSROT  = "SROT";
  String lDRIFT = "DRIFT";
  String lMULT  = "MULTIPOLE";
  String lQUAD  = "QUADRUPOLE";
  String lBEND  = "SBEND";
  String lSEXT  = "SEXTUPOLE";
  String lOCT   = "OCTUPOLE";
  String lSOLN  = "SOLENOID";
  String lKICK  = "KICKER";

  String lANGLE = ", ANGLE=";
  String lTILT  = ", TILT=";
  String lK0    = ", K0=";
  String lK1    = ", K1=";
  String lK2    = ", K2=";
  String lK3    = ", K3=";
  String lK0L   = ", K0L=";
  String lK1L   = ", K1L=";
  String lK2L   = ", K2L=";
  String lK3L   = ", K3L=";
  String lK4L   = ", K4L=";
  String lK5L   = ", K5L=";
  String lK6L   = ", K6L=";
  String lK7L   = ", K7L=";
  String lK8L   = ", K8L=";
  String lK9L   = ", K9L=";
  String lT0    = ", T0=";
  String lT1    = ", T1=";
  String lT2    = ", T2=";
  String lT3    = ", T3=";
  String lT4    = ", T4=";
  String lT5    = ", T5=";
  String lT6    = ", T6=";
  String lT7    = ", T7=";
  String lT8    = ", T8=";
  String lT9    = ", T9=";
  String lE1    = ", E1=";
  String lE2    = ", E2=";
  String lK     = ", K=";
  String lHKICK = ", HKICK=";
  String lVKICK = ", VKICK=";

  String Choice, llabel, klabel, tlabel;
  llabel = ", L=";

  Real tilt, l, l2, k, kl, dpole, e1, e2;
  Integer pole, skew, minpole, maxpole, edgemult;
  Integer vecnum, i, j, index = 1;

  if (nTPNodes == 0)
  {
    sout << "\n No TeaPot Nodes are present \n\n";
    return;
  }

  sout << "! Element definitions:\n";

  for (i = 1; i <= nTPNodes; i++)
  {
    Zeros = "";
    if(index < 10000) Zeros =    "0";
    if(index <  1000) Zeros =   "00";
    if(index <   100) Zeros =  "000";
    if(index <    10) Zeros = "0000";
    sprintf(numStr,"%d",index);
    EleStr = TPStr + Zeros + numStr + ": ";
    sout << EleStr;
    index++;

    Choice = "";

    tp = TPBase::safeCast(TPPointers(i-1));
    if(tp->_name == Drift)
    {
      Choice = lDRIFT;
      tpd = TPD::safeCast(TPPointers(i-1));
      l = tpd->_length;
      sout << Choice
           << llabel << setprecision(15) << l;
    }

    if(tp->_name == Mult)
    {
      tpm = TPM::safeCast(TPPointers(i-1));
      pole = tpm->_pole;
      dpole = 2.0 * (Real(pole) + 1.0);
      l = tpm->_length;
      l2 = l / 2.0;
      kl = tpm->_kl;
      tilt = tpm->_tilt;
      skew = tpm->_skew;
      if(skew) tilt += Consts::pi / dpole;
      if(Abs(l) > Consts::tiny)
      {
        if(pole <= 3)
        {
          Choice = "";
          tlabel = lTILT;
          if(pole == 0)
          {
            Choice = lBEND;
            klabel = lANGLE;
            k = kl;
          }
          k = kl / l;
          if(pole == 1)
          {
            Choice = lQUAD;
            klabel = lK1;
          }
          if(pole == 2)
          {
            Choice = lSEXT;
            klabel = lK2;
          }
          if(pole == 3)
          {
            Choice = lOCT;
            klabel = lK3;
          }
          sout << Choice
               << llabel << setprecision(15) << l
               << Spacer
               << klabel << setprecision(15) << k
	       << tlabel << setprecision(15) << tilt;
        }
        else
        {
          Choice = lDRIFT;
          sout << Choice
               << llabel << setprecision(15) << l2;
          Zeros = "";
          if(index < 10000) Zeros =    "0";
          if(index <  1000) Zeros =   "00";
          if(index <   100) Zeros =  "000";
          if(index <    10) Zeros = "0000";
          sprintf(numStr,"%d",index);
          EleStr = TPStr + Zeros + numStr + ": ";
          sout << EleStr;
          index++;
          Choice = lMULT;
          k = tpm->_kl;
          if(pole == 4)
	  {
            klabel = lK4L;
            tlabel = lT4;
	  }
          if(pole == 5)
	  {
            klabel = lK5L;
            tlabel = lT5;
	  }
          if(pole == 6)
	  {
            klabel = lK6L;
            tlabel = lT6;
	  }
          if(pole == 7)
	  {
            klabel = lK7L;
            tlabel = lT7;
	  }
          if(pole == 8)
	  {
            klabel = lK8L;
            tlabel = lT8;
	  }
          if(pole == 9)
          {
            klabel = lK9L;
            tlabel = lT9;
	  }
          sout << Choice
               << klabel << setprecision(15) << k
	       << tlabel << setprecision(15) << tilt;
          Zeros = "";
          if(index < 10000) Zeros =    "0";
          if(index <  1000) Zeros =   "00";
          if(index <   100) Zeros =  "000";
          if(index <    10) Zeros = "0000";
          sprintf(numStr,"%d",index);
          EleStr = TPStr + Zeros + numStr + ": ";
          sout << EleStr;
          index++;
          Choice = lDRIFT;
          sout << Choice
               << llabel << setprecision(15) << l2;
	}
      }
      else
      {
        Choice = lMULT;
        k = tpm->_kl;
        if(pole == 0)
        {
          klabel = lK0L;
          tlabel = lT0;
        }
        if(pole == 1)
        {
          klabel = lK1L;
          tlabel = lT1;
        }
        if(pole == 2)
        {
          klabel = lK2L;
          tlabel = lT2;
        }
        if(pole == 3)
        {
          klabel = lK3L;
          tlabel = lT3;
        }
        if(pole == 4)
        {
          klabel = lK4L;
          tlabel = lT4;
        }
        if(pole == 5)
        {
          klabel = lK5L;
          tlabel = lT5;
        }
        if(pole == 6)
        {
          klabel = lK6L;
          tlabel = lT6;
        }
        if(pole == 7)
        {
          klabel = lK7L;
          tlabel = lT7;
        }
        if(pole == 8)
        {
          klabel = lK8L;
          tlabel = lT8;
	  }
        if(pole == 9)
        {
          klabel = lK9L;
          tlabel = lT9;
        }
        sout << Choice
             << klabel << setprecision(15) << k
             << tlabel << setprecision(15) << tilt;
      }
    }

    if(tp->_name == MultCF)
    {
      tpmcf = TPMCF::safeCast(TPPointers(i-1));
      vecnum = tpmcf->_vecnum;
      l = tpmcf->_length;
      l2 = l / 2.0;
      if(Abs(l) > Consts::tiny)
      {
        minpole = 100;
        maxpole = -1;
        edgemult = 0;
        for(j = 1; j <= vecnum; j++)
        {
          pole = tpmcf->_pole(j);
          skew = tpmcf->_skew(j);
          if(skew) edgemult = 1;
          if((minpole > pole) &&
             (skew == 0)) minpole = pole;
          if(maxpole < pole) maxpole = pole;
        }
        if(maxpole > 3) edgemult = 1;
        if(minpole <= 3)
        {
          if(edgemult)
          {
            Choice = lMULT;
            sout << Choice;
            for(j = 1; j <= vecnum; j++)
            {
              pole = tpmcf->_pole(j);
              skew = tpmcf->_skew(j);
              if((pole > 3) || skew)
              {
                kl = tpmcf->_kl(j);
                k = kl / 2.0;
                dpole = 2.0 * (Real(pole) + 1.0);
                tilt = tpmcf->_tilt;
                if(skew) tilt += Consts::pi / dpole;
                if(pole == 0)
	        {
                  klabel = lK0L;
                  tlabel = lT0;
	        }
                if(pole == 1)
	        {
                  klabel = lK1L;
                  tlabel = lT1;
	        }
                if(pole == 2)
	        {
                  klabel = lK2L;
                  tlabel = lT2;
	        }
                if(pole == 3)
	        {
                  klabel = lK3L;
                  tlabel = lT3;
	        }
                if(pole == 4)
	        {
                  klabel = lK4L;
                  tlabel = lT4;
	        }
                if(pole == 5)
	        {
                  klabel = lK5L;
                  tlabel = lT5;
	        }
                if(pole == 6)
	        {
                  klabel = lK6L;
                  tlabel = lT6;
	        }
                if(pole == 7)
	        {
                  klabel = lK7L;
                  tlabel = lT7;
	        }
                if(pole == 8)
	        {
                  klabel = lK8L;
                  tlabel = lT8;
	        }
                if(pole == 9)
                {
                  klabel = lK9L;
                  tlabel = lT9;
	        }
                sout << Spacer
                     << klabel << setprecision(15) << k
	             << tlabel << setprecision(15) << tilt;
              }
            }
            Zeros = "";
            if(index < 10000) Zeros =    "0";
            if(index <  1000) Zeros =   "00";
            if(index <   100) Zeros =  "000";
            if(index <    10) Zeros = "0000";
            sprintf(numStr,"%d",index);
            EleStr = TPStr + Zeros + numStr + ": ";
            sout << EleStr;
            index++;
	  }
          Choice = "";
          if(pole == 0) Choice = lBEND;
          if(pole == 1) Choice = lQUAD;
          if(pole == 2) Choice = lSEXT;
          if(pole == 3) Choice = lOCT;
          sout << Choice
               << llabel << setprecision(15) << l;
          for(j = 1; j <= vecnum; j++)
          {
            pole = tpmcf->_pole(j);
            skew = tpmcf->_skew(j);
            if((pole <= 3) && (skew == 0))
            {
              kl = tpmcf->_kl(j);
              k = kl / l;
              tilt = tpmcf->_tilt;
              if(pole == 0) klabel = lANGLE;
              if(pole == 1) klabel = lK1;
              if(pole == 2) klabel = lK2;
              if(pole == 3) klabel = lK3;
              tlabel = lTILT;
              sout << Spacer
                   << klabel << setprecision(15) << k
                   << tlabel << setprecision(15) << tilt;
            }
          }
          if(edgemult)
          {
            Zeros = "";
            if(index < 10000) Zeros =    "0";
            if(index <  1000) Zeros =   "00";
            if(index <   100) Zeros =  "000";
            if(index <    10) Zeros = "0000";
            sprintf(numStr,"%d",index);
            EleStr = TPStr + Zeros + numStr + ": ";
            sout << EleStr;
            index++;
            Choice = lMULT;
            sout << Choice;
            for(j = 1; j <= vecnum; j++)
            {
              pole = tpmcf->_pole(j);
              skew = tpmcf->_skew(j);
              if((pole > 3) || skew)
              {
                kl = tpmcf->_kl(j);
                k = kl / 2.0;
                dpole = 2.0 * (Real(pole) + 1.0);
                tilt = tpmcf->_tilt;
                if(skew) tilt += Consts::pi / dpole;
                if(pole == 0)
	        {
                  klabel = lK0L;
                  tlabel = lT0;
	        }
                if(pole == 1)
	        {
                  klabel = lK1L;
                  tlabel = lT1;
	        }
                if(pole == 2)
	        {
                  klabel = lK2L;
                  tlabel = lT2;
	        }
                if(pole == 3)
	        {
                  klabel = lK3L;
                  tlabel = lT3;
	        }
                if(pole == 4)
	        {
                  klabel = lK4L;
                  tlabel = lT4;
	        }
                if(pole == 5)
	        {
                  klabel = lK5L;
                  tlabel = lT5;
	        }
                if(pole == 6)
	        {
                  klabel = lK6L;
                  tlabel = lT6;
	        }
                if(pole == 7)
	        {
                  klabel = lK7L;
                  tlabel = lT7;
	        }
                if(pole == 8)
	        {
                  klabel = lK8L;
                  tlabel = lT8;
	        }
                if(pole == 9)
                {
                  klabel = lK9L;
                  tlabel = lT9;
	        }
                sout << Spacer
                     << klabel << setprecision(15) << k
	             << tlabel << setprecision(15) << tilt;
              }
            }
	  }
        }
        else
        {
          Choice = lDRIFT;
          sout << Choice
               << llabel << setprecision(15) << l2;
          Zeros = "";
          if(index < 10000) Zeros =    "0";
          if(index <  1000) Zeros =   "00";
          if(index <   100) Zeros =  "000";
          if(index <    10) Zeros = "0000";
          sprintf(numStr,"%d",index);
          EleStr = TPStr + Zeros + numStr + ": ";
          sout << EleStr;
          index++;
          Choice = lMULT;
          sout << Choice;
          for(j = 1; j <= vecnum; j++)
          {
            pole = tpmcf->_pole(j);
            kl = tpmcf->_kl(j);
            k = kl;
            skew = tpmcf->_skew(j);
            dpole = 2.0 * (Real(pole) + 1.0);
            tilt = tpmcf->_tilt;
            if(skew) tilt += Consts::pi / dpole;
            if(pole == 0)
	    {
              klabel = lK0L;
              tlabel = lT0;
	    }
            if(pole == 1)
	    {
              klabel = lK1L;
              tlabel = lT1;
	    }
            if(pole == 2)
	    {
              klabel = lK2L;
              tlabel = lT2;
	    }
            if(pole == 3)
	    {
              klabel = lK3L;
              tlabel = lT3;
	    }
            if(pole == 4)
	    {
              klabel = lK4L;
              tlabel = lT4;
            }
            if(pole == 5)
            {
              klabel = lK5L;
              tlabel = lT5;
	    }
            if(pole == 6)
	    {
              klabel = lK6L;
              tlabel = lT6;
	    }
            if(pole == 7)
	    {
              klabel = lK7L;
              tlabel = lT7;
            }
            if(pole == 8)
            {
              klabel = lK8L;
              tlabel = lT8;
            }
            if(pole == 9)
            {
              klabel = lK9L;
              tlabel = lT9;
            }
            sout << Spacer
                 << klabel << setprecision(15) << k
                 << tlabel << setprecision(15) << tilt;
          }
          Zeros = "";
          if(index < 10000) Zeros =    "0";
          if(index <  1000) Zeros =   "00";
          if(index <   100) Zeros =  "000";
          if(index <    10) Zeros = "0000";
          sprintf(numStr,"%d",index);
          EleStr = TPStr + Zeros + numStr + ": ";
          sout << EleStr;
          index++;
          Choice = lDRIFT;
          sout << Choice
               << llabel << setprecision(15) << l2;
	}
      }
      else
      {
        Choice = lMULT;
        sout << Choice;
        for(j = 1; j <= vecnum; j++)
        {
          pole = tpmcf->_pole(j);
          kl = tpmcf->_kl(j);
          k = kl;
          skew = tpmcf->_skew(j);
          dpole = 2.0 * (Real(pole) + 1.0);
          tilt = tpmcf->_tilt;
          if(skew) tilt += Consts::pi / dpole;
          if(pole == 0)
          {
            klabel = lK0L;
            tlabel = lT0;
          }
          if(pole == 1)
          {
            klabel = lK1L;
            tlabel = lT1;
          }
          if(pole == 2)
          {
            klabel = lK2L;
            tlabel = lT2;
          }
          if(pole == 3)
          {
            klabel = lK3L;
            tlabel = lT3;
          }
          if(pole == 4)
          {
            klabel = lK4L;
            tlabel = lT4;
          }
          if(pole == 5)
          {
            klabel = lK5L;
            tlabel = lT5;
          }
          if(pole == 6)
          {
            klabel = lK6L;
            tlabel = lT6;
          }
          if(pole == 7)
          {
            klabel = lK7L;
            tlabel = lT7;
          }
          if(pole == 8)
          {
            klabel = lK8L;
            tlabel = lT8;
          }
          if(pole == 9)
          {
            klabel = lK9L;
            tlabel = lT9;
          }
          sout << Spacer
               << klabel << setprecision(15) << k
               << tlabel << setprecision(15) << tilt;
        }
      }
    }

    if(tp->_name == Quad)
    {
      tpq = TPQ::safeCast(TPPointers(i-1));
      Choice = lQUAD;
      l = tpq->_length;
      k = tpq->_kq;
      tilt = tpq->_tilt;
      klabel = lK1;
      tlabel = lTILT;
      sout << Choice << llabel << setprecision(15) << l
                     << Spacer
                     << klabel << setprecision(15) << k
                     << tlabel << setprecision(15) << tilt;
    }

    if(tp->_name == QuadCF)
    {
      tpqcf = TPQCF::safeCast(TPPointers(i-1));
      vecnum = tpqcf->_vecnum;
      maxpole = -1;
      edgemult = 0;
      for(j = 1; j <= vecnum; j++)
      {
        pole = tpqcf->_pole(j);
        skew = tpqcf->_skew(j);
        if(skew == 1) edgemult = 1;
        if(maxpole < pole) maxpole = pole;
      }
      if(maxpole > 3) edgemult = 1;
      if(edgemult)
      {
        Choice = lMULT;
        sout << Choice;
        for(j = 1; j <= vecnum; j++)
        {
          pole = tpqcf->_pole(j);
          skew = tpqcf->_skew(j);
          if((pole > 3) || skew)
          {
            kl = tpqcf->_kl(j);
            k = kl / 2.0;
            dpole = 2.0 * (Real(pole) + 1.0);
            tilt = tpqcf->_tilt;
            if(skew) tilt += Consts::pi / dpole;
            if(pole == 0)
            {
              klabel = lK0L;
              tlabel = lT0;
            }
            if(pole == 1)
            {
              klabel = lK1L;
              tlabel = lT1;
            }
            if(pole == 2)
            {
              klabel = lK2L;
              tlabel = lT2;
            }
            if(pole == 3)
            {
              klabel = lK3L;
              tlabel = lT3;
            }
            if(pole == 4)
            {
              klabel = lK4L;
              tlabel = lT4;
            }
            if(pole == 5)
            {
              klabel = lK5L;
              tlabel = lT5;
            }
            if(pole == 6)
            {
              klabel = lK6L;
              tlabel = lT6;
            }
            if(pole == 7)
            {
              klabel = lK7L;
              tlabel = lT7;
            }
            if(pole == 8)
            {
              klabel = lK8L;
              tlabel = lT8;
            }
            if(pole == 9)
            {
              klabel = lK9L;
              tlabel = lT9;
            }
            sout << Spacer
                 << klabel << setprecision(15) << k
                 << tlabel << setprecision(15) << tilt;
          }
        }
        Zeros = "";
        if(index < 10000) Zeros =    "0";
        if(index <  1000) Zeros =   "00";
        if(index <   100) Zeros =  "000";
        if(index <    10) Zeros = "0000";
        sprintf(numStr,"%d",index);
        EleStr = TPStr + Zeros + numStr + ": ";
        sout << EleStr;
        index++;
      }
      Choice = lQUAD;
      l = tpqcf->_length;
      k = tpqcf->_kq;
      tilt = tpqcf->_tilt;
      klabel = lK1;
      tlabel = lTILT;
      sout << Choice
           << llabel << setprecision(15) << l
           << Spacer
	   << klabel << setprecision(15) << k
	   << tlabel << setprecision(15) << tilt;
      for(j = 1; j <= vecnum; j++)
      {
        pole = tpqcf->_pole(j);
        skew = tpqcf->_skew(j);
        if((pole <= 3) && (skew == 0))
        {
          kl = tpqcf->_kl(j);
          k = kl / l;
          if(pole == 0) klabel = lANGLE;
          if(pole == 1) klabel = lK1;
          if(pole == 2) klabel = lK2;
          if(pole == 3) klabel = lK3;
          sout << Spacer
               << klabel << setprecision(15) << k;
        }
      }
      if(edgemult)
      {
        Zeros = "";
        if(index < 10000) Zeros =    "0";
        if(index <  1000) Zeros =   "00";
        if(index <   100) Zeros =  "000";
        if(index <    10) Zeros = "0000";
        sprintf(numStr,"%d",index);
        EleStr = TPStr + Zeros + numStr + ": ";
        sout << EleStr;
        index++;
        Choice = lMULT;
        sout << Choice;
        for(j = 1; j <= vecnum; j++)
        {
          pole = tpqcf->_pole(j);
          skew = tpqcf->_skew(j);
          if((pole > 3) || skew)
          {
            kl = tpqcf->_kl(j);
            k = kl / 2.0;
            dpole = 2.0 * (Real(pole) + 1.0);
            tilt = tpqcf->_tilt;
            if(skew) tilt += Consts::pi / dpole;
            if(pole == 0)
            {
              klabel = lK0L;
              tlabel = lT0;
            }
            if(pole == 1)
            {
              klabel = lK1L;
              tlabel = lT1;
            }
            if(pole == 2)
            {
              klabel = lK2L;
              tlabel = lT2;
            }
            if(pole == 3)
            {
              klabel = lK3L;
              tlabel = lT3;
            }
            if(pole == 4)
            {
              klabel = lK4L;
              tlabel = lT4;
            }
            if(pole == 5)
            {
              klabel = lK5L;
              tlabel = lT5;
            }
            if(pole == 6)
            {
              klabel = lK6L;
              tlabel = lT6;
            }
            if(pole == 7)
            {
              klabel = lK7L;
              tlabel = lT7;
            }
            if(pole == 8)
            {
              klabel = lK8L;
              tlabel = lT8;
            }
            if(pole == 9)
            {
              klabel = lK9L;
              tlabel = lT9;
            }
            sout << Spacer
                 << klabel << setprecision(15) << k
                 << tlabel << setprecision(15) << tilt;
          }
        }
      }
    }

    if(tp->_name == Bend)
    {
      tpb = TPB::safeCast(TPPointers(i-1));
      Choice = lBEND;
      l = tpb->_length;
      tilt = tpb->_tilt;
      k = tpb->_theta;
      e1 = tpb->_ea1;
      e2 = tpb->_ea2;
      tlabel = lTILT;
      klabel = lANGLE;
      sout << Choice
           << tlabel << setprecision(15) << tilt
	   << klabel << setprecision(15) << k
           << Spacer
           << llabel << setprecision(15) << l
           << lE1 << setprecision(15) << e1
           << lE2 << setprecision(15) << e2;
    }

    if(tp->_name == BendCF)
    {
      tpbcf = TPBCF::safeCast(TPPointers(i-1));
      vecnum = tpbcf->_vecnum;
      maxpole = -1;
      edgemult = 0;
      for(j = 1; j <= vecnum; j++)
      {
        pole = tpbcf->_pole(j);
        skew = tpbcf->_skew(j);
        if(skew == 1) edgemult = 1;
        if(maxpole < pole) maxpole = pole;
      }
      if(maxpole > 3) edgemult = 1;
      if(edgemult)
      {
        Choice = lMULT;
        sout << Choice;
        for(j = 1; j <= vecnum; j++)
        {
          pole = tpbcf->_pole(j);
          skew = tpbcf->_skew(j);
          if((pole > 3) || skew)
          {
            kl = tpbcf->_kl(j);
            k = kl / 2.0;
            dpole = 2.0 * (Real(pole) + 1.0);
            tilt = tpbcf->_tilt;
            if(skew) tilt += Consts::pi / dpole;
            if(pole == 0)
            {
              klabel = lK0L;
              tlabel = lT0;
            }
            if(pole == 1)
            {
              klabel = lK1L;
              tlabel = lT1;
            }
            if(pole == 2)
            {
              klabel = lK2L;
              tlabel = lT2;
            }
            if(pole == 3)
            {
              klabel = lK3L;
              tlabel = lT3;
            }
            if(pole == 4)
            {
              klabel = lK4L;
              tlabel = lT4;
            }
            if(pole == 5)
            {
              klabel = lK5L;
              tlabel = lT5;
            }
            if(pole == 6)
            {
              klabel = lK6L;
              tlabel = lT6;
            }
            if(pole == 7)
            {
              klabel = lK7L;
              tlabel = lT7;
            }
            if(pole == 8)
            {
              klabel = lK8L;
              tlabel = lT8;
            }
            if(pole == 9)
            {
              klabel = lK9L;
              tlabel = lT9;
            }
            sout << Spacer
                 << klabel << setprecision(15) << k
                 << tlabel << setprecision(15) << tilt;
          }
        }
        Zeros = "";
        if(index < 10000) Zeros =    "0";
        if(index <  1000) Zeros =   "00";
        if(index <   100) Zeros =  "000";
        if(index <    10) Zeros = "0000";
        sprintf(numStr,"%d",index);
        EleStr = TPStr + Zeros + numStr + ": ";
        sout << EleStr;
        index++;
      }
      Choice = lBEND;
      l = tpbcf->_length;
      tilt = tpbcf->_tilt;
      k = tpbcf->_theta;
      e1 = tpbcf->_ea1;
      e2 = tpbcf->_ea2;
      tlabel = lTILT;
      klabel = lANGLE;
      sout << Choice
           << tlabel << setprecision(15) << tilt
	   << klabel << setprecision(15) << k
           << Spacer
           << llabel << setprecision(15) << l
           << lE1 << setprecision(15) << e1
           << lE2 << setprecision(15) << e2;
      for(j = 1; j <= vecnum; j++)
      {
        pole = tpbcf->_pole(j);
        skew = tpbcf->_skew(j);
        if((pole <= 3) && (skew == 0))
        {
          kl = tpbcf->_kl(j);
          k = kl / l;
          if(pole == 0) klabel = lANGLE;
          if(pole == 1) klabel = lK1;
          if(pole == 2) klabel = lK2;
          if(pole == 3) klabel = lK3;
          sout << Spacer
               << klabel << setprecision(15) << k;
        }
      }
      if(edgemult)
      {
        Zeros = "";
        if(index < 10000) Zeros =    "0";
        if(index <  1000) Zeros =   "00";
        if(index <   100) Zeros =  "000";
        if(index <    10) Zeros = "0000";
        sprintf(numStr,"%d",index);
        EleStr = TPStr + Zeros + numStr + ": ";
        sout << EleStr;
        index++;
        Choice = lMULT;
        sout << Choice;
        for(j = 1; j <= vecnum; j++)
        {
          pole = tpbcf->_pole(j);
          skew = tpbcf->_skew(j);
          if((pole > 3) || skew)
          {
            kl = tpbcf->_kl(j);
            k = kl / 2.0;
            dpole = 2.0 * (Real(pole) + 1.0);
            tilt = tpbcf->_tilt;
            if(skew) tilt += Consts::pi / dpole;
            if(pole == 0)
            {
              klabel = lK0L;
              tlabel = lT0;
            }
            if(pole == 1)
            {
              klabel = lK1L;
              tlabel = lT1;
            }
            if(pole == 2)
            {
              klabel = lK2L;
              tlabel = lT2;
            }
            if(pole == 3)
            {
              klabel = lK3L;
              tlabel = lT3;
            }
            if(pole == 4)
            {
              klabel = lK4L;
              tlabel = lT4;
            }
            if(pole == 5)
            {
              klabel = lK5L;
              tlabel = lT5;
            }
            if(pole == 6)
            {
              klabel = lK6L;
              tlabel = lT6;
            }
            if(pole == 7)
            {
              klabel = lK7L;
              tlabel = lT7;
            }
            if(pole == 8)
            {
              klabel = lK8L;
              tlabel = lT8;
            }
            if(pole == 9)
            {
              klabel = lK9L;
              tlabel = lT9;
            }
            sout << Spacer
                 << klabel << setprecision(15) << k
                 << tlabel << setprecision(15) << tilt;
          }
        }
      }
    }

    if(tp->_name == Soln)
    {
      tps = TPS::safeCast(TPPointers(i-1));
      Choice = lSOLN;
      l = tps->_length;
      k = tps->_B;
      klabel = lK;
      sout << Choice
           << llabel << setprecision(15) << l
           << klabel << setprecision(15) << k;
    }

    if(tp->_name == Kick)
    {
      tpk = TPK::safeCast(TPPointers(i-1));
      Choice = lKICK;
      l = tpk->_length;
      sout << Choice
           << llabel << setprecision(15) << l;
      k = tpk->_hkick;
      klabel = lHKICK;
      sout << Spacer
           << klabel << setprecision(15) << k;
      k = tpk->_vkick;
      klabel = lVKICK;
      sout << Spacer
           << klabel << setprecision(15) << k;
    }
  }

  String LatStr = "";
  String LatTot = "\n\nLATTICE: LINE = (";
  TPStr = "TP";
  Integer Lowlim, Uplim, Numlat = 400;

  for(j = 0; j < 10; j++)
  {
    Lowlim = Numlat * j + 1;
    Uplim = Numlat * (j + 1);
    if(nTPNodes <= Uplim) Uplim = nTPNodes;
    if(Lowlim <= nTPNodes)
    {
      if(j == 0) LatStr = "\n\nLATT00: LINE = ( &\n";
      if(j == 1) LatStr = "\n\nLATT01: LINE = ( &\n";
      if(j == 2) LatStr = "\n\nLATT02: LINE = ( &\n";
      if(j == 3) LatStr = "\n\nLATT03: LINE = ( &\n";
      if(j == 4) LatStr = "\n\nLATT04: LINE = ( &\n";
      if(j == 5) LatStr = "\n\nLATT05: LINE = ( &\n";
      if(j == 6) LatStr = "\n\nLATT06: LINE = ( &\n";
      if(j == 7) LatStr = "\n\nLATT07: LINE = ( &\n";
      if(j == 8) LatStr = "\n\nLATT08: LINE = ( &\n";
      if(j == 9) LatStr = "\n\nLATT09: LINE = ( &\n";
      sout << LatStr;
      for (i = Lowlim; i <= Uplim; i++)
      {
        Zeros = "";
        if(i < 10000) Zeros =    "0";
        if(i <  1000) Zeros =   "00";
        if(i <   100) Zeros =  "000";
        if(i <    10) Zeros = "0000";
        sprintf(numStr,"%d",i);
        EleStr = TPStr + Zeros + numStr;
        sout << EleStr;
        if(i == Uplim)
        {
          sout << ")";
        }
        else if(i == 8 * (i / 8))
        {
          sout << ", &\n";
        }
        else
        {
          sout << ", ";
        }
      }
      if(j == 0) LatStr = "LATT00";
      if(j == 1) LatStr = "LATT01";
      if(j == 2) LatStr = "LATT02";
      if(j == 3) LatStr = "LATT03";
      if(j == 4) LatStr = "LATT04";
      if(j == 5) LatStr = "LATT05";
      if(j == 6) LatStr = "LATT06";
      if(j == 7) LatStr = "LATT07";
      if(j == 8) LatStr = "LATT08";
      if(j == 9) LatStr = "LATT09";
      if(nTPNodes > Uplim) LatStr = LatStr + ", ";
      else LatStr = LatStr + ")\n\n";
      LatTot = LatTot + LatStr;
    }
  }
  sout << LatTot;

  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart & 
                                      Particles::All_Mask) - 1));
  sout << "BEAM, PARTICLE=PROTON,ENERGY=" << sp->_eTotal << "\n\n";

  sout << "USE, LATTICE\n\n";
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   TeaPot::calcLatFuncs
//
// DESCRIPTION
//   Calculates lattice functions
//
// PARAMETERS
//   ringline (str) : Periodic (Ring) or initial value (Line)
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void TeaPot::calcLatFuncs(const Integer &herdNo, const String &ring_line,
                             Ostream &sout)
{
//
// Need a herd to run TeaPot methods.
//

  if (nTPNodes == 0)
  {
    sout << "\n No TeaPot Nodes are present \n\n";
    return;
  }

  sout << "\n\n TeaPot Nodes present in ring. nTPNodes = "
       << nTPNodes << "\n\n";

  MacroPart *mp;

  if((herdNo & All_Mask) > Particles::nHerds) except (badHerdNo);
  SyncPart *sp = SyncPart::safeCast(syncP(0));
  mp = MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));

//
// Initialization and transport matrix setup.
//

  Integer inuX = Integer(Ring::nuX);
  Integer inuY = Integer(Ring::nuY);
  Real alphaX0       = Ring::alphaX;
  Real betaX0        = Ring::betaX;
  Real gammaX0       = (1.0 + Ring::alphaX * Ring::alphaX) / Ring::betaX;
  Real alphaY0       = Ring::alphaY;
  Real betaY0        = Ring::betaY;
  Real gammaY0       = (1.0 + Ring::alphaY * Ring::alphaY) / Ring::betaY;
  Real etaX0         = Ring::etaX;
  Real etaPX0        = Ring::etaPX;
  Real nuXf          = Ring::nuX - inuX;
  Real nuYf          = Ring::nuY - inuY;
  Real chromaticityX = Ring::chromaticityX;
  Real chromaticityY = Ring::chromaticityY;

  TPBase *tlp;

  Integer i, j, k;
  Integer m, l, n;

  Vector(Real) length, s,
               betaX, alphaX, phaseX,
               betaY, alphaY, phaseY,
               etaX, etaPX, rhoInv;
  length.resize(nTPNodes);
  s.resize(nTPNodes);
  betaX.resize(nTPNodes);
  alphaX.resize(nTPNodes);
  phaseX.resize(nTPNodes);
  betaY.resize(nTPNodes);
  alphaY.resize(nTPNodes);
  phaseY.resize(nTPNodes);
  etaX.resize(nTPNodes);
  etaPX.resize(nTPNodes);
  rhoInv.resize(nTPNodes);

  Array3(Real) X0toi, Y0toi;
  X0toi.resize(nTPNodes,3,3);
  Y0toi.resize(nTPNodes,2,2);

  Matrix(Real) TEMP31, TEMP32, TEMP21, TEMP22;
  TEMP31.resize(3,3);
  TEMP32.resize(3,3);
  TEMP21.resize(2,2);
  TEMP22.resize(2,2);

  Real ss = 0.0;
  TEMP31 = X_Identity();
  TEMP21 = Y_Identity();
  for (i = 1; i <= nTPNodes; i++)
  {
    tlp = TPBase::safeCast(TPPointers(i-1));
    length(i) = tlp->_length;
    ss += length(i);
    s(i) = ss;
    betaX(i)  = tlp->_betaX;
    alphaX(i) = tlp->_alphaX;
    betaY(i)  = tlp->_betaY;
    alphaY(i) = tlp->_alphaY;
    etaX(i)    = tlp->_etaX;
    etaPX(i)   = tlp->_etaPX;

    tlp->_nodeCalculator(*mp);

    rhoInv(i) = TeaPot::rhoInv;

    m = 3; l = 3; n = 3;
    TEMP32 = MAT_Mult(m, l, n, TeaPot::xMatrix, TEMP31);
    m = 2; l = 2; n = 2;
    TEMP22 = MAT_Mult(m, l, n, TeaPot::yMatrix, TEMP21);

    for(j = 1; j <= 3; j++)
    {
      for(k = 1; k <= 3; k++)
      {
        X0toi(i,j,k) = TEMP32(j,k);
      }
    }
    for(j = 1; j <= 2; j++)
    {
      for(k = 1; k <= 2; k++)
      {
        Y0toi(i,j,k) = TEMP22(j,k);
      }
    }
    TEMP31 = TEMP32;
    TEMP21 = TEMP22;
  }

/*
//
// Print intial values.
//

  sout << "\n"
       << "alphaX0 = "       << alphaX0       << "\n"
       << "betaX0  = "       << betaX0        << "\n"
       << "gammaX0 = "       << gammaX0       << "\n"
       << "alphaY0 = "       << alphaY0       << "\n"
       << "betaY0  = "       << betaY0        << "\n"
       << "gammaY0 = "       << gammaY0       << "\n"
       << "etaX0   = "       << etaX0         << "\n"
       << "etaPX0  = "       << etaPX0        << "\n"
       << "nuXf    = "       << nuXf          << "\n"
       << "nuYf    = "       << nuYf          << "\n"
       << "chromaticityX = " << chromaticityX << "\n"
       << "chromaticityX = " << chromaticityY << "\n"
       << "BG-A2_X = "       << betaX0 * gammaX0 - alphaX0 * alphaX0 - 1.0
                                              << "\n"
       << "BG-A2_Y = "       << betaY0 * gammaY0 - alphaY0 * alphaY0 - 1.0
                                              << "\n\n";

  for(i = 1; i <= nTPNodes; i++)
  {
    sout << s(i)      << "   " << length(i) << "   "
         << betaX(i)  << "   " << alphaX(i) << "   " << phaseX(i) << "   "
         << betaY(i)  << "   " << alphaY(i) << "   " << phaseY(i) << "   "
         << etaX(i)   << "   " << etaPX(i)  << "\n";
  }
*/

//
// Calculate tunes and twiss parameters at lattice start.
//

  Matrix(Real) MX3, MX2, MY, TWX, TWY;
  MX3.resize(3,3);
  MX2.resize(2,2);
  MY.resize(2,2);
  TWX.resize(3,3);
  TWY.resize(3,3);

  if(ring_line == "Line")
  {
  }
  else if(ring_line == "Ring")
  {
    for(j = 1; j <= 2; j++)
    {
      for(k = 1; k <= 2; k++)
      {
        MX2(j,k) = X0toi(nTPNodes,j,k);
        MY(j,k)  = Y0toi(nTPNodes,j,k);
      }
    }
    Real csX = (MX2(1,1) + MX2(2,2)) / 2.0;
    Real snX = pow(1.0 - csX * csX, 0.5);
    nuXf = acos(csX) / (2.0 * Consts:: pi);
    if(MX2(1,2) < 0.0)
    {
      nuXf = 1.0 - nuXf;
      snX  = -snX;
    }
    alphaX0 = (MX2(1,1) - MX2(2,2)) / (2.0 * snX);
    betaX0  =  MX2(1,2) / snX;
    gammaX0 = -MX2(2,1) / snX;
    Real csY = (MY(1,1) + MY(2,2)) / 2.0;
    Real snY = pow(1.0 - csY * csY, 0.5);
    nuYf = acos(csY) / (2.0 * Consts:: pi);
    if(MY(1,2) < 0.0)
    {
      nuYf = 1.0 - nuYf;
      snY  = -snY;
    }
    alphaY0 = (MY(1,1) - MY(2,2)) / (2.0 * snY);
    betaY0  =  MY(1,2) / snY;
    gammaY0 = -MY(2,1) / snY;
  }
  else
  {
    sout << "\n Ring or Line? \n\n";
    return;
  }

//
// Calculate twiss parameters throughout the lattice.
//

  Matrix (Real) BAGX0, BAGY0, BAG;
  BAGX0.resize(3,1);
  BAGY0.resize(3,1);
  BAG.resize(3,1);
  BAGX0(1,1) = betaX0;
  BAGX0(2,1) = alphaX0;
  BAGX0(3,1) = gammaX0;
  BAGY0(1,1) = betaY0;
  BAGY0(2,1) = alphaY0;
  BAGY0(3,1) = gammaY0;

  for(i = 1; i <= nTPNodes; i++)
  {
    for(j = 1; j <= 2; j++)
    {
      for(k = 1; k <= 2; k++)
      {
        MX2(j,k) = X0toi(i,j,k);
        MY(j,k)  = Y0toi(i,j,k);
      }
    }
    TWX = MAT_TW(MX2);
    m= 3; l = 3; n = 1;
    BAG = MAT_Mult(m, l, n, TWX, BAGX0);
    betaX(i)  = BAG(1,1);
    alphaX(i) = BAG(2,1);
    TWY = MAT_TW(MY);
    BAG = MAT_Mult(m, l, n, TWY, BAGY0);
    betaY(i)  = BAG(1,1);
    alphaY(i) = BAG(2,1);
  }

//
// Calculate dispersion at lattice start.
//

  if(ring_line == "Line")
  {
  }
  else if(ring_line == "Ring")
  {
    m = 2; l = 2; n = 1;
    Matrix (Real) DISP0, DISP;
    DISP0.resize(2,1);
    DISP.resize(2,1);
    TEMP21(1,1) =  X0toi(nTPNodes,1,1) - 1.0;
    TEMP21(1,2) =  X0toi(nTPNodes,1,2);
    TEMP21(2,1) =  X0toi(nTPNodes,2,1);
    TEMP21(2,2) =  X0toi(nTPNodes,2,2) - 1.0;
    Real DET    =  TEMP21(1,1) * TEMP21(2,2) - TEMP21(1,2) * TEMP21(2,1);
    TEMP22(1,1) =  TEMP21(2,2) / DET;
    TEMP22(1,2) = -TEMP21(1,2) / DET;
    TEMP22(2,1) = -TEMP21(2,1) / DET;
    TEMP22(2,2) =  TEMP21(1,1) / DET;
    DISP0(1,1)  = -X0toi(nTPNodes,1,3);
    DISP0(2,1)  = -X0toi(nTPNodes,2,3);
    DISP        =  MAT_Mult(m, l, n, TEMP22, DISP0);
    etaX0       =  DISP(1,1);
    etaPX0      =  DISP(2,1);
  }

//
// Calculate dispersion throughout the lattice.
//

  Real u, up, delta;

  Matrix (Real) XXPD0, XXPD;
  XXPD0.resize(3,1);
  XXPD.resize(3,1);

  u     = etaX0;
  up    = etaPX0;
  delta = 1.0;

  XXPD0(1,1) = u;
  XXPD0(2,1) = up;
  XXPD0(3,1) = delta;

  for(i = 1; i <= nTPNodes; i++)
  {
    m = 3; l = 3; n = 1;
    for(j = 1; j <= 3; j++)
    {
      for(k = 1; k <= 3; k++)
      {
        MX3(j,k) = X0toi(i,j,k);
      }
    }
    XXPD     = MAT_Mult(m, l, n, MX3, XXPD0);
    etaX(i)  = XXPD(1,1);
    etaPX(i) = XXPD(2,1);
  }

//
// Calculate Gamma Transition.
//

  Real dm, dp, Compaction, gammaTrans;
  Compaction = 0.0;
  dm = 0.5 * etaX(nTPNodes);
  for(i = 1; i <= nTPNodes; i++)
  {
    dp = 0.5 * etaX(i);
    Compaction += (dm + dp) * rhoInv(i) * length(i);
    dm = dp;
  }
  Compaction /= s(nTPNodes);
  gammaTrans = 1.0e+20;
  if(Compaction > 0.0) gammaTrans =  1.0 / pow( Compaction, 0.5);
  if(Compaction < 0.0) gammaTrans = -1.0 / pow(-Compaction, 0.5);

  Ring::gammaTrans = gammaTrans;

//
// Calculate phase advances throughout the lattice.
//

  Real beta, alpha, eta, etap, phaseX0, phaseY0;

  Matrix (Real) YYP, YYP0;
  YYP0.resize(2,1);
  YYP.resize(2,1);

  u     = 1.0;
  up    = 0.0;
  delta = 0.0;

  XXPD0(1,1) = u;
  XXPD0(2,1) = up;
  XXPD0(3,1) = delta;
  YYP0(1,1)  = u;
  YYP0(2,1)  = up;

  beta    = betaX0;
  alpha   = alphaX0;
  eta     = etaX0;
  etap    = etaPX0;
  phaseX0 = findPhase(u, up, delta, beta, alpha, eta, etap);
  if(phaseX0 < 0.0) phaseX0 += 1.0;
  if(phaseX0 > 1.0) phaseX0 -= 1.0;

  beta    = betaY0;
  alpha   = alphaY0;
  eta     = 0.0;
  etap    = 0.0;
  phaseY0 = findPhase(u, up, delta, beta, alpha, eta, etap);
  if(phaseY0 < 0.0) phaseY0 += 1.0;
  if(phaseY0 > 1.0) phaseY0 -= 1.0;

  for(i = 1; i <= nTPNodes; i++)
  {
    m = 3; l = 3; n = 1;
    for(j = 1; j <= 3; j++)
    {
      for(k = 1; k <= 3; k++)
      {
        MX3(j,k) = X0toi(i,j,k);
      }
    }
    XXPD  = MAT_Mult(m, l, n, MX3, XXPD0);
    u     = XXPD(1,1);
    up    = XXPD(2,1);
    beta  = betaX(i);
    alpha = alphaX(i);
    eta   = etaX(i);
    etap  = etaPX(i);
    phaseX(i) = findPhase(u, up, delta, beta, alpha, eta, etap) - phaseX0;
    if(phaseX(i) < 0.0) phaseX(i) += 1.0;
    if(phaseX(i) > 1.0) phaseX(i) -= 1.0;

    m = 2; l = 2; n = 1;
    for(j = 1; j <= 2; j++)
    {
      for(k = 1; k <= 2; k++)
      {
        MY(j,k) = Y0toi(i,j,k);
      }
    }
    YYP   = MAT_Mult(m, l, n, MY, YYP0);
    u     = YYP(1,1);
    up    = YYP(2,1);
    beta  = betaY(i);
    alpha = alphaY(i);
    eta = 0.0;
    etap = 0.0;
    phaseY(i) = findPhase(u, up, delta, beta, alpha, eta, etap) - phaseY0;
    if(phaseY(i) < 0.0) phaseY(i) += 1.0;
    if(phaseY(i) > 1.0) phaseY(i) -= 1.0;
  }

  if(ring_line == "Line")
  {
    nuXf = phaseX(nTPNodes);
    nuYf = phaseY(nTPNodes);
  }
  else if(ring_line == "Ring")
  {
  }

//
// Now get the chromaticities.
//

  Real phaseXPlus, phaseYPlus, phaseXMinus, phaseYMinus;
  Real dPoPPlus = 0.001, dPoPMinus = -0.001;

  TeaPot::dPoP = dPoPPlus;

  u     = 1.0;
  up    = 0.0;
  delta = TeaPot::dPoP;

  XXPD0(1,1) = u;
  XXPD0(2,1) = up;
  XXPD0(3,1) = delta;
  YYP0(1,1)  = u;
  YYP0(2,1)  = up;

  beta    = betaX0;
  alpha   = alphaX0;
  eta     = etaX0;
  etap    = etaPX0;
  phaseX0 = findPhase(u, up, delta, beta, alpha, eta, etap);
  if(phaseX0 < 0.0) phaseX0 += 1.0;
  if(phaseX0 > 1.0) phaseX0 -= 1.0;

  beta    = betaY0;
  alpha   = alphaY0;
  eta     = 0.0;
  etap    = 0.0;
  phaseY0 = findPhase(u, up, delta, beta, alpha, eta, etap);
  if(phaseY0 < 0.0) phaseY0 += 1.0;
  if(phaseY0 > 1.0) phaseY0 -= 1.0;

  TEMP31 = X_Identity();
  TEMP21 = Y_Identity();
  for (i = 1; i <= nTPNodes; i++)
  {
    tlp = TPBase::safeCast(TPPointers(i-1));

    tlp->_nodeCalculator(*mp);

    m = 3; l = 3; n = 3;
    TEMP32 = MAT_Mult(m, l, n, TeaPot::xMatrix, TEMP31);
    m = 2; l = 2; n = 2;
    TEMP22 = MAT_Mult(m, l, n, TeaPot::yMatrix, TEMP21);

    TEMP31 = TEMP32;
    TEMP21 = TEMP22;
  }

  m = 3; l = 3; n = 1;
  MX3 = TEMP31;
  XXPD  = MAT_Mult(m, l, n, MX3, XXPD0);
  u     = XXPD(1,1);
  up    = XXPD(2,1);
  beta    = betaX0;
  alpha   = alphaX0;
  eta     = etaX0;
  etap    = etaPX0;
  phaseXPlus = findPhase(u, up, delta, beta, alpha, eta, etap) - phaseX0;
  if(phaseXPlus < 0.0) phaseXPlus += 1.0;
  if(phaseXPlus > 1.0) phaseXPlus -= 1.0;

  m = 2; l = 2; n = 1;
  MY = TEMP21;
  YYP   = MAT_Mult(m, l, n, MY, YYP0);
  u     = YYP(1,1);
  up    = YYP(2,1);
  beta    = betaY0;
  alpha   = alphaY0;
  eta     = 0.0;
  etap    = 0.0;
  phaseYPlus = findPhase(u, up, delta, beta, alpha, eta, etap) - phaseY0;
  if(phaseYPlus < 0.0) phaseYPlus += 1.0;
  if(phaseYPlus > 1.0) phaseYPlus -= 1.0;

  TeaPot::dPoP = dPoPMinus;

  u     = 1.0;
  up    = 0.0;
  delta = TeaPot::dPoP;

  XXPD0(1,1) = u;
  XXPD0(2,1) = up;
  XXPD0(3,1) = delta;
  YYP0(1,1)  = u;
  YYP0(2,1)  = up;

  beta    = betaX0;
  alpha   = alphaX0;
  eta     = etaX0;
  etap    = etaPX0;
  phaseX0 = findPhase(u, up, delta, beta, alpha, eta, etap);
  if(phaseX0 < 0.0) phaseX0 += 1.0;
  if(phaseX0 > 1.0) phaseX0 -= 1.0;

  beta    = betaY0;
  alpha   = alphaY0;
  eta     = 0.0;
  etap    = 0.0;
  phaseY0 = findPhase(u, up, delta, beta, alpha, eta, etap);
  if(phaseY0 < 0.0) phaseY0 += 1.0;
  if(phaseY0 > 1.0) phaseY0 -= 1.0;

  TEMP31 = X_Identity();
  TEMP21 = Y_Identity();
  for (i = 1; i <= nTPNodes; i++)
  {
    tlp = TPBase::safeCast(TPPointers(i-1));

    tlp->_nodeCalculator(*mp);

    m = 3; l = 3; n = 3;
    TEMP32 = MAT_Mult(m, l, n, TeaPot::xMatrix, TEMP31);
    m = 2; l = 2; n = 2;
    TEMP22 = MAT_Mult(m, l, n, TeaPot::yMatrix, TEMP21);

    TEMP31 = TEMP32;
    TEMP21 = TEMP22;
  }

  m = 3; l = 3; n = 1;
  MX3 = TEMP31;
  XXPD  = MAT_Mult(m, l, n, MX3, XXPD0);
  u     = XXPD(1,1);
  up    = XXPD(2,1);
  beta    = betaX0;
  alpha   = alphaX0;
  eta     = etaX0;
  etap    = etaPX0;
  phaseXMinus = findPhase(u, up, delta, beta, alpha, eta, etap) - phaseX0;
  if(phaseXMinus < 0.0) phaseXMinus += 1.0;
  if(phaseXMinus > 1.0) phaseXMinus -= 1.0;

  m = 2; l = 2; n = 1;
  MY = TEMP21;
  YYP   = MAT_Mult(m, l, n, MY, YYP0);
  u     = YYP(1,1);
  up    = YYP(2,1);
  beta    = betaY0;
  alpha   = alphaY0;
  eta     = 0.0;
  etap    = 0.0;
  phaseYMinus = findPhase(u, up, delta, beta, alpha, eta, etap) - phaseY0;
  if(phaseYMinus < 0.0) phaseYMinus += 1.0;
  if(phaseYMinus > 1.0) phaseYMinus -= 1.0;

  TeaPot::dPoP = 0.0;

  chromaticityX = (phaseXPlus - phaseXMinus) / (dPoPPlus - dPoPMinus);
  chromaticityY = (phaseYPlus - phaseYMinus) / (dPoPPlus - dPoPMinus);

//
// Reset the lattice functions in ORBIT Ring module and TeaPot nodes
// and print final values.
//

  Ring::alphaX        = alphaX0;
  Ring::betaX         = betaX0;
  Ring::gammaX        = gammaX0;
  Ring::alphaY        = alphaY0;
  Ring::betaY         = betaY0;
  Ring::gammaY        = gammaY0;
  Ring::etaX          = etaX0;
  Ring::etaPX         = etaPX0;
  Ring::nuX           = inuX + nuXf;
  Ring::nuY           = inuY + nuYf;
  Ring::chromaticityX = chromaticityX;
  Ring::chromaticityY = chromaticityY;
  Ring::lRing         = s(nTPNodes);

  sout << "\n"
       << "Ring::alphaX        = " << Ring::alphaX        << "\n"
       << "Ring::betaX         = " << Ring::betaX         << "\n"
       << "Ring::gammaX        = " << Ring::gammaX        << "\n"
       << "Ring::alphaY        = " << Ring::alphaY        << "\n"
       << "Ring::betaY         = " << Ring::betaY         << "\n"
       << "Ring::gammaY        = " << Ring::gammaY        << "\n"
       << "Ring::etaX          = " << Ring::etaX          << "\n"
       << "Ring::etaPX         = " << Ring::etaPX         << "\n"
       << "Ring::nuX           = " << Ring::nuX           << "\n"
       << "Ring::nuY           = " << Ring::nuY           << "\n"
       << "Ring::chromaticityX = " << Ring::chromaticityX << "\n"
       << "Ring::chromaticityY = " << Ring::chromaticityY << "\n"
       << "BG-A2_X             = "
       << betaX0 * gammaX0 - alphaX0 * alphaX0 - 1.0      << "\n"
       << "BG-A2_Y             = "
       << betaY0 * gammaY0 - alphaY0 * alphaY0 - 1.0      << "\n"
       << "Momentum Compaction = " << Compaction          << "\n"
       << "Gamma Transition    = " << gammaTrans          << "\n\n";

  for(i = 1; i <= nTPNodes; i++)
  {
    tlp = TPBase::safeCast(TPPointers(i-1));
    tlp->_betaX  = betaX(i);
    tlp->_alphaX = alphaX(i);
    tlp->_betaY  = betaY(i);
    tlp->_alphaY = alphaY(i);
    tlp->_etaX   = etaX(i);
    tlp->_etaPX  = etaPX(i);
    sout << s(i)         << "   "
         << length(i)    << "   "
         << tlp->_betaX  << "   "
         << tlp->_alphaX << "   "
         << phaseX(i)    << "   "
         << tlp->_betaY  << "   "
         << tlp->_alphaY << "   "
         << phaseY(i)    << "   "
         << tlp->_etaX   << "   "
         << tlp->_etaPX  << "\n";
  }
}

///////////////////////////////////////////////////////////////////////////
//
// LOCAL FUNCTIONS
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TPfindPhases
//
// DESCRIPTION
//    Finds the betatron X and Y phases
//
// PARAMETERS
//    tm = reference to the TransMap member
//    mp = reference to the macro-particle main herd
//    i  = index to  the herd member
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPfindPhases(MapBase &tm, MacroPart &mp, const Integer &i)
{
    Real angle, xval, xpval, yval, ypval;

    // Find normalized coordinates on ellipse transformed to circle:

    xval = (mp._x(i) - 1000. * tm._etaX * mp._dp_p(i)) / sqrt(tm._betaX);
    xpval = (mp._xp(i) - 1000. * tm._etaPX * mp._dp_p(i)) * sqrt(tm._betaX)
             + xval * tm._alphaX;

    yval = mp._y(i) / sqrt(tm._betaY);
    ypval = mp._yp(i) * sqrt(tm._betaY) + yval * tm._alphaY;

    angle = atan2(xpval, xval);
    if(angle < 0.) angle += Consts::twoPi;
    TPxPhase = angle;

    angle = atan2(ypval, yval);
    if(angle < 0.) angle += Consts::twoPi;
    TPyPhase = angle;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TPdoTunes
//
// DESCRIPTION
//    Increments the tunes of a macroParticle herd. It is used as
//    the herd is stepped through transfer matrices.
//
// PARAMETERS
//    tm = reference to the TransMatrix1 member
//    mp = reference to the macro-particle herd
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TPdoTunes(MapBase &tm, MacroPart &mp)
{
  Integer i;
  Real dPhase;
  Real phiCrit = Consts::pi;

  for(i=1; i<=mp._nMacros; i++)
  {

    TPfindPhases(tm, mp, i);   // find new betatron phase

    // Horizontal Phase:

    dPhase = Particles::xPhaseOld(i) - TPxPhase;
    if(dPhase < (-Consts::pi)) dPhase += Consts::twoPi;
    //  Subtract out false 0 angle crossings from s.c. "kick-backs":
    if(dPhase > Consts::pi && Particles::xPhaseOld(i) > phiCrit)
      dPhase -= Consts::twoPi;
    Particles::xPhaseTot(i) += dPhase;
    Particles::xPhaseOld(i) = TPxPhase;

    // Vertical phase:

    dPhase = Particles::yPhaseOld(i) - TPyPhase;
    if(dPhase < (-Consts::pi)) dPhase += Consts::twoPi;
    //  Subtract out fake 0 angle crossings from s.c. "kick-backs"
    if(dPhase > Consts::pi && Particles::yPhaseOld(i) > phiCrit)
      dPhase -= Consts::twoPi;
    Particles::yPhaseTot(i) += dPhase;
    Particles::yPhaseOld(i) = TPyPhase;

    //Tune tracking length:

    Particles::lTuneTrack(i) += tm._length;
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  rotatexy
//
// DESCRIPTION
//  rotates particle herd
//
// PARAMETERS
//  mp = reference to the macro-particle herd
//  anglexy = rotation angle
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void rotatexy(MacroPart &mp, Real &anglexy)
{
  Real xtemp, xptemp, ytemp, yptemp;
  Integer i;
  Real cs = cos(anglexy);
  Real sn = sin(anglexy);

  for(i = 1; i <= mp._nMacros; i++)
  {
    xtemp  = mp._x(i);
    xptemp = mp._xp(i);
    ytemp  = mp._y(i);
    yptemp = mp._yp(i);

    mp._x(i)  =  cs * xtemp  - sn * ytemp;
    mp._xp(i) =  cs * xptemp - sn * yptemp;
    mp._y(i)  =  sn * xtemp  + cs * ytemp;
    mp._yp(i) =  sn * xptemp + cs * yptemp;
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  drifti
//
// DESCRIPTION
//  drifts a single particle
//
// PARAMETERS
//  mp = reference to the macro-particle herd
//  i = particle index
//  length = length of the drift
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void drifti(MacroPart &mp, Integer &i, Real &length)
{
  Real x_init, y_init, phi_init;
  Real KNL, px, py, phifac;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);

  x_init = mp._x(i);
  y_init = mp._y(i);
  phi_init = mp._phi(i);
  KNL = 1.0 / (1.0 + mp._dp_p(i));
  px = mp._xp(i) / 1000.0;
  py = mp._yp(i) / 1000.0;

  mp._x(i) = x_init + KNL * length * mp._xp(i);
  mp._y(i) = y_init + KNL * length * mp._yp(i);
  phifac = (px * px +
            py * py +
            mp._dp_p(i) * mp._dp_p(i) * gamma2i
           ) / 2.0;
  phifac = (phifac * KNL - mp._dp_p(i) * gamma2i) * KNL;
  mp._phi(i) = phi_init + Factor * length * phifac;
  if(Abs(mp._phi(i)) > Consts::pi)
  {
    Real sign = -mp._phi(i) / Abs(mp._phi(i));
    mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  drift
//
// DESCRIPTION
//  drifts a particle herd
//
// PARAMETERS
//  mp = reference to the macro-particle herd
//  length = length of the drift
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void drift(MacroPart &mp, Real &length)
{
  Real x_init, y_init, phi_init;
  Real KNL, px, py, phifac;
  Integer i;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init = mp._x(i);
    y_init = mp._y(i);
    phi_init = mp._phi(i);
    KNL = 1.0 / (1.0 + mp._dp_p(i));
    px = mp._xp(i) / 1000.0;
    py = mp._yp(i) / 1000.0;

    mp._x(i) = x_init + KNL * length * mp._xp(i);
    mp._y(i) = y_init + KNL * length * mp._yp(i);
    phifac = (px * px +
              py * py +
              mp._dp_p(i) * mp._dp_p(i) * gamma2i
             ) / 2.0;
    phifac = (phifac * KNL - mp._dp_p(i) * gamma2i) * KNL;
    mp._phi(i) = phi_init + Factor * length * phifac;
    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  kick
//
// DESCRIPTION
//  kicks a particle herd
//
// PARAMETERS
//  mp = reference to the macro-particle herd
//  kx = strength of the horizontal kick
//  ky = strength of the vertical kick
//  kE = strength of the energy kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void kick(MacroPart &mp, Real &kx, Real &ky, Real &kE)
{
  Integer i;
  Real kx1000, ky1000;

  if((Abs(kx) <= Consts::tiny) && (Abs(ky) <= Consts::tiny)
                               && (Abs(kE) <= Consts::tiny)) return;

  kx1000 = 1000. * kx;
  ky1000 = 1000. * ky;

  for(i = 1; i <= mp._nMacros; i++)
  {
    mp._xp(i) += kx1000;
    mp._yp(i) += ky1000;
    mp._deltaE(i) += kE;
    mp._dp_p(i) = mp._deltaE(i) * mp._syncPart._dppFac;
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  multpi
//
// DESCRIPTION
//  gives particle a multipole momentum kick
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  i = particle index
//  pole = multipole number
//  kl = integrated strength of the kick [m^(-pole)]
//  skew = 0 - normal, 1 - skew
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void multpi(MacroPart &mp, Integer &i, const Integer &pole,
           Real &kl, const Integer &skew)
{
  Integer k;
  Complex z, zn;
  Real kl1000factl;

  if(Abs(kl) <= Consts::tiny) return;

  kl1000factl = 1.e3 * kl / TPfactorial(pole);

  z = Complex(1.e-3 * mp._x(i), 1.e-3 * mp._y(i));
      // in meters since pole strength is in meters

  // take power of z to the n

  zn = Complex(1., 0.);
  for (k = 0; k < pole; k++)
  {
    zn = zn * z;
  }

  // MAD Conventions on signs of multipole terms

  if(skew)
  {
    mp._xp(i) += kl1000factl * zn.im;
    mp._yp(i) += kl1000factl * zn.re;
  }
  else
  {
    mp._xp(i) -= kl1000factl * zn.re;
    mp._yp(i) += kl1000factl * zn.im;
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  multp
//
// DESCRIPTION
//  gives particle a multipole momentum kick
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  pole = multipole number
//  kl = integrated strength of the kick [m^(-pole)]
//  skew = 0 - normal, 1 - skew
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void multp(MacroPart &mp, const Integer &pole, Real &kl,
           const Integer &skew)
{
  Integer i, k;
  Complex z, zn;
  Real kl1000factl;

  if(Abs(kl) <= Consts::tiny) return;

  kl1000factl = 1.e3 * kl / TPfactorial(pole);

  for(i = 1; i <= mp._nMacros; i++)
  {
    z = Complex(1.e-3 * mp._x(i), 1.e-3 * mp._y(i));
        // in meters since pole strength is in meters

    // take power of z to the n

    zn = Complex(1., 0.);
    for (k = 0; k < pole; k++)
    {
      zn = zn * z;
    }

  // MAD Conventions on signs of multipole terms

    if(skew)
    {
      mp._xp(i) += kl1000factl * zn.im;
      mp._yp(i) += kl1000factl * zn.re;
    }
    else
    {
      mp._xp(i) -= kl1000factl * zn.re;
      mp._yp(i) += kl1000factl * zn.im;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  multpfringeIN
//
// DESCRIPTION
//  Hard edge fringe field for a multipole
//  Etienne Forest, Chapter 13.2
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  pole = multipole number
//  kl = multipole strength
//  skew = multipole skew
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void multpfringeIN(MacroPart &mp, const Integer &pole, Real &kl,
                   const Integer &skew)
{
  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Complex rootm1 = Complex(0.0, 1.0);

  Integer lm1 = pole;
  Integer l   = pole + 1;
  Integer lp1 = pole + 2;
  Integer lp2 = pole + 3;

  Real klfactlp1 = kl / (4.0 * TPfactorial(lp1));

  // MAD Conventions on signs of multipole terms

  Complex kterm;
  if(skew)
  {
    kterm = Complex(0.0, klfactlp1);
  }
  else
  {
    kterm = Complex(klfactlp1, 0.0);
  }

  for(Integer i = 1; i <= mp._nMacros; i++)
  {
    Real x    = 1.e-3 * mp._x(i);
    Real y    = 1.e-3 * mp._y(i);
    Complex z = Complex(x, y);
    Real px   = 1.e-3 * mp._xp(i);
    Real py   = 1.e-3 * mp._yp(i);

    // take power of z to the lm1, l

    Complex zlm1 = Complex(1., 0.);
    for (Integer k = 0; k < lm1; k++)
    {
      zlm1 *= z;
    }
    Complex zl = zlm1 * z;

    Complex fxterm   = Complex(l * x, -lp2 * y);
    Complex dxfxterm = l * (fxterm + z);
    Complex dyfxterm = rootm1 * (l * fxterm - lp2 * z);
    Complex fyterm   = Complex(l * y, lp2 * x);
    Complex dxfyterm = l * fyterm + rootm1 * lp2 * z;
    Complex dyfyterm = l * (rootm1 * fyterm + z);

    Complex fxcx   = -kterm * zl   * fxterm;
    Complex dxfxcx = -kterm * zlm1 * dxfxterm;
    Complex dyfxcx = -kterm * zlm1 * dyfxterm;
    Complex fycx   = -kterm * zl   * fyterm;
    Complex dxfycx = -kterm * zlm1 * dxfyterm;
    Complex dyfycx = -kterm * zlm1 * dyfyterm;

    mp._x(i) -= 1.e+3 * fxcx.re / (1.0 + mp._dp_p(i));
    mp._y(i) -= 1.e+3 * fycx.re / (1.0 + mp._dp_p(i));

    Real M11 = 1.0 - dxfxcx.re / (1.0 + mp._dp_p(i));
    Real M12 =     - dxfycx.re / (1.0 + mp._dp_p(i));
    Real M21 =     - dyfxcx.re / (1.0 + mp._dp_p(i));
    Real M22 = 1.0 - dyfycx.re / (1.0 + mp._dp_p(i));
    Real detM = M11 * M22 - M12 * M21;

    Real pxnew = ( M22 * px - M12 * py) / detM;
    Real pynew = (-M21 * px + M11 * py) / detM;

    mp._xp(i) = 1.e+3 * pxnew;
    mp._yp(i) = 1.e+3 * pynew;

    mp._phi(i) += Factor * (pxnew * fxcx.re + pynew * fycx.re) /
                  ((1.0 + mp._dp_p(i)) * (1.0 + mp._dp_p(i)));
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  multpfringeOUT
//
// DESCRIPTION
//  Hard edge fringe field for a multipole
//  Etienne Forest, Chapter 13.2
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  pole = multipole number
//  kl = multipole strength
//  skew = multipole skew
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void multpfringeOUT(MacroPart &mp, const Integer &pole, Real &kl,
                    const Integer &skew)
{
  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Complex rootm1 = Complex(0.0, 1.0);

  Integer lm1 = pole;
  Integer l   = pole + 1;
  Integer lp1 = pole + 2;
  Integer lp2 = pole + 3;

  Real klfactlp1 = kl / (4.0 * TPfactorial(lp1));

  // MAD Conventions on signs of multipole terms

  Complex kterm;
  if(skew)
  {
    kterm = Complex(0.0, klfactlp1);
  }
  else
  {
    kterm = Complex(klfactlp1, 0.0);
  }

  for(Integer i = 1; i <= mp._nMacros; i++)
  {
    Real x    = 1.e-3 * mp._x(i);
    Real y    = 1.e-3 * mp._y(i);
    Complex z = Complex(x, y);
    Real px   = 1.e-3 * mp._xp(i);
    Real py   = 1.e-3 * mp._yp(i);

    // take power of z to the lm1, l

    Complex zlm1 = Complex(1., 0.);
    for (Integer k = 0; k < lm1; k++)
    {
      zlm1 *= z;
    }
    Complex zl = zlm1 * z;

    Complex fxterm   = Complex(l * x, -lp2 * y);
    Complex dxfxterm = l * (fxterm + z);
    Complex dyfxterm = rootm1 * (l * fxterm - lp2 * z);
    Complex fyterm   = Complex(l * y, lp2 * x);
    Complex dxfyterm = l * fyterm + rootm1 * lp2 * z;
    Complex dyfyterm = l * (rootm1 * fyterm + z);

    Complex fxcx   = kterm * zl   * fxterm;
    Complex dxfxcx = kterm * zlm1 * dxfxterm;
    Complex dyfxcx = kterm * zlm1 * dyfxterm;
    Complex fycx   = kterm * zl   * fyterm;
    Complex dxfycx = kterm * zlm1 * dxfyterm;
    Complex dyfycx = kterm * zlm1 * dyfyterm;

    mp._x(i) -= 1.e+3 * fxcx.re / (1.0 + mp._dp_p(i));
    mp._y(i) -= 1.e+3 * fycx.re / (1.0 + mp._dp_p(i));

    Real M11 = 1.0 - dxfxcx.re / (1.0 + mp._dp_p(i));
    Real M12 =     - dxfycx.re / (1.0 + mp._dp_p(i));
    Real M21 =     - dyfxcx.re / (1.0 + mp._dp_p(i));
    Real M22 = 1.0 - dyfycx.re / (1.0 + mp._dp_p(i));
    Real detM = M11 * M22 - M12 * M21;

    Real pxnew = ( M22 * px - M12 * py) / detM;
    Real pynew = (-M21 * px + M11 * py) / detM;

    mp._xp(i) = 1.e+3 * pxnew;
    mp._yp(i) = 1.e+3 * pynew;

    mp._phi(i) += Factor * (pxnew * fxcx.re + pynew * fycx.re) /
                  ((1.0 + mp._dp_p(i)) * (1.0 + mp._dp_p(i)));
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  quad1
//
// DESCRIPTION
//  quadrupole element one: linear transport matrix
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  length = length of transport
//  kq = quadrupole field strength [m^(-2)]
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void quad1(MacroPart &mp, Real &length, Real &kq)
{
  Real x_init, xp_init, y_init, yp_init, phi_init;
  Real sqrt_kq, kqlength;
  Real cx, sx, cy, sy, m11, m12, m21, m22, m33, m34, m43, m44;
  Integer i;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);

  if(kq >= 0.)
  {
    sqrt_kq = pow(kq, 0.5);
    kqlength = sqrt_kq * length;
    cx = cos(kqlength);
    sx = sin(kqlength);
    cy = cosh(kqlength);
    sy = sinh(kqlength);
    m11 = cx;
    m12 = sx / sqrt_kq;
    m21 = -sx * sqrt_kq;
    m22 = cx;
    m33 = cy;
    m34 = sy / sqrt_kq;
    m43 = sy * sqrt_kq;
    m44 = cy;
  }
  else if(kq < 0.)
  {
    sqrt_kq = pow(-kq, 0.5);
    kqlength = sqrt_kq * length;
    cx = cosh(kqlength);
    sx = sinh(kqlength);
    cy = cos(kqlength);
    sy = sin(kqlength);
    m11 = cx;
    m12 = sx / sqrt_kq;
    m21 = sx * sqrt_kq;
    m22 = cx;
    m33 = cy;
    m34 = sy / sqrt_kq;
    m43 = -sy * sqrt_kq;
    m44 = cy;
  }

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init = mp._x(i);
    xp_init = mp._xp(i);
    y_init = mp._y(i);
    yp_init = mp._yp(i);
    phi_init = mp._phi(i);

    mp._x(i) = x_init * m11 + xp_init * m12;
    mp._xp(i) = x_init * m21 + xp_init * m22;
    mp._y(i) = y_init * m33 + yp_init * m34;
    mp._yp(i) = y_init * m43 + yp_init * m44;
    mp._phi(i) = phi_init - Factor * mp._dp_p(i) * gamma2i * length;
    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  } 
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  quad2
//
// DESCRIPTION
//  non-linear quadrupole transport
//
// PARAMETERS
//  mp     = reference to the macro-particle herd
//  length = length of the element
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void quad2(MacroPart &mp, Real &length)
{
  Real KNL, px, py, phifac;
  Integer i;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);

  for(i = 1; i <= mp._nMacros; i++)
  {
    KNL = 1.0 / (1.0 + mp._dp_p(i));
    px  = mp._xp(i) / 1000.0;
    py  = mp._yp(i) / 1000.0;

    mp._x(i)   -= KNL * length * mp._dp_p(i) * mp._xp(i);
    mp._y(i)   -= KNL * length * mp._dp_p(i) * mp._yp(i);
    phifac      = (px * px +
                   py * py +
                   mp._dp_p(i) * mp._dp_p(i) * gamma2i
                  ) / 2.0;
    phifac      = (phifac * KNL + mp._dp_p(i) * mp._dp_p(i) * gamma2i) * KNL;
    mp._phi(i) += Factor * length * phifac;
    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign  = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  quadfringeIN
//
// DESCRIPTION
//  Hard edge fringe field for a quad
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  kq  = strength of quad
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void quadfringeIN(MacroPart &mp, Real &kq)
{
  Integer i;
  Real x_init, xp_init, y_init, yp_init, phi_init, dp_init;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init = mp._x(i) / 1000.;
    xp_init = mp._xp(i) / 1000.;
    y_init = mp._y(i) / 1000.;
    yp_init = mp._yp(i) / 1000.;
    phi_init = mp._phi(i);
    dp_init = mp._dp_p(i);

    mp._x(i) = 1000. * (x_init +
                        kq / (12. * (1. + dp_init)) * x_init
                        * (x_init * x_init + 3. * y_init * y_init));

    mp._xp(i) = 1000. * (xp_init -
                         kq / (4. * (1. + dp_init))
                         * (xp_init * (x_init * x_init + y_init * y_init)
	                    - 2. * yp_init * x_init * y_init));

    mp._y(i) = 1000. * (y_init -
                        kq / (12. * (1. + dp_init)) * y_init
                        * (y_init * y_init + 3. * x_init * x_init));

    mp._yp(i) = 1000. * (yp_init -
                         kq / (4. * (1. + dp_init))
                         * (-yp_init * (x_init * x_init + y_init * y_init)
	                    + 2. * xp_init * x_init * y_init));

    mp._phi(i) = phi_init -
                 Factor * kq / (12. * (1. + dp_init) * (1. + dp_init))
                        * (xp_init * x_init *
                           (x_init * x_init + 3. * y_init * y_init) -
                           yp_init * y_init *
                           (y_init * y_init + 3. * x_init * x_init));
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  quadfringeOUT
//
// DESCRIPTION
//  Hard edge fringe field for a quad
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  kq  = strength of quad
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void quadfringeOUT(MacroPart &mp, Real &kq)
{
  Integer i;
  Real x_init, xp_init, y_init, yp_init, phi_init, dp_init;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init = mp._x(i) / 1000.;
    xp_init = mp._xp(i) / 1000.;
    y_init = mp._y(i) / 1000.;
    yp_init = mp._yp(i) / 1000.;
    phi_init = mp._phi(i);
    dp_init = mp._dp_p(i);

    mp._x(i) = 1000. * (x_init -
                        kq / (12. * (1. + dp_init)) * x_init
                        * (x_init * x_init + 3. * y_init * y_init));

    mp._xp(i) = 1000. * (xp_init +
                         kq / (4. * (1. + dp_init))
                         * (xp_init * (x_init * x_init + y_init * y_init)
	                    - 2. * yp_init * x_init * y_init));

    mp._y(i) = 1000. * (y_init +
                        kq / (12. * (1. + dp_init)) * y_init
                        * (y_init * y_init + 3. * x_init * x_init));

    mp._yp(i) = 1000. * (yp_init +
                         kq / (4. * (1. + dp_init))
                         * (-yp_init * (x_init * x_init + y_init * y_init)
	                    + 2. * xp_init * x_init * y_init));

    mp._phi(i) = phi_init +
                 Factor * kq / (12. * (1. + dp_init) * (1. + dp_init))
                        * (xp_init * x_init *
                           (x_init * x_init + 3. * y_init * y_init) -
                           yp_init * y_init *
                           (y_init * y_init + 3. * x_init * x_init));
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  wedgerotate
//
// DESCRIPTION
//  Rotates coordinates by e for fringe fields at non-SBEND
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  e = rotation angle
//  frinout = 0 before fringe, 1 after fringe
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void wedgerotate(MacroPart &mp, Real &e, Integer &frinout)
{
  Real cs, sn;
  Real xp_temp, p0_temp, p0;
  Integer i;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;

  cs = cos(e);
  sn = sin(e);
  Real coefxphi = Factor / 1000.;

  for(i = 1; i <= mp._nMacros; i++)
  {
    if(frinout == 0)
    {
      xp_temp  = mp._xp(i);
      p0_temp  = 1000. * (1.0 + mp._dp_p(i));

      mp._x(i)   /=  cs;
      mp._xp(i)   =  xp_temp * cs + p0_temp * sn;
      p0          = -xp_temp * sn + p0_temp * cs;
      mp._dp_p(i) =  p0 / 1000. - 1.0;
      mp._phi(i)  =  (mp._x(i) * Factor / 1000. * sn + mp._phi(i)) / cs;
    }
    else
    {
      p0 = 1000. * (1.0 + mp._dp_p(i));

      mp._phi(i)  = -mp._x(i) * Factor / 1000. * sn + mp._phi(i) * cs;
      mp._x(i)   *= cs;
      xp_temp     = mp._xp(i) * cs - p0 * sn;
      p0_temp     = mp._xp(i) * sn + p0 * cs;
      mp._xp(i)   = xp_temp;
      mp._dp_p(i) = p0_temp / 1000. - 1.0;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  wedgedrift
//
// DESCRIPTION
//  Drifts particles through wedge for non-SBEND
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  e = wedge angle
//  inout = 0 for in, 1 for out
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void wedgedrift(MacroPart &mp, Real &e, Integer &inout)
{
  Real cs, sn, ct, tn;
  Real x_init, p0_init, s;
  Integer i;

  cs = cos(e);
  sn = sin(e);
  ct = cs / sn;

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init = mp._x(i) / 1000.;

    if(inout == 0)
    {
      p0_init = 1000. * (1.0 + mp._dp_p(i));
      tn = mp._xp(i) / p0_init;
      s = x_init / (ct - tn);
    }
    else
    {
      s = x_init / ct;
    }

    drifti(mp, i, s);
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  wedgebend
//
// DESCRIPTION
//  Straight bends particles through wedge for non-SBEND
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  e = wedge angle
//  inout = 0 for in, 1 for out
//  rho = radius of curvature
//  nsteps = number of integraton steps
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void wedgebend(MacroPart &mp, Real &e, Integer &inout,
               Real &rho, Integer &nsteps)
{
  Real kappa, cs, sn, ct, tn;
  Real x_init, p0_init, s, sm, sm2;
  Integer i, j, nst;

  nst = nsteps / 2;
  if(nst < 1) nst = 1;
  kappa = 1000. / rho;
  cs = cos(e);
  sn = sin(e);
  ct = cs / sn;

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init = mp._x(i) / 1000.;

    if(inout == 0)
    {
      s = -x_init / ct;
    }
    else
    {
      p0_init = 1000. * (1.0 + mp._dp_p(i));
      tn = mp._xp(i) / p0_init;
      s = -x_init / (ct + tn);
    }

    sm = s / Real(nst);
    sm2 = sm / 2.0;

    drifti(mp, i, sm2);
    mp._xp(i) = mp._xp(i) - sm * kappa;
    for(j = 1; j < nst; j++)
    {
      drifti(mp, i, sm);
      mp._xp(i) = mp._xp(i) - sm * kappa;
    }
    drifti(mp, i, sm2);
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  bend1
//
// DESCRIPTION
//  Linear bend transport
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  length = length of transport
//  th = bending angle
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void bend1(MacroPart &mp, Real &length, Real &th)
{
  Real x_init, xp_init;
  Real cx, sx, rho;
  Real m11, m12, m16,
       m21, m22, m26,
       m51, m52, m56;
  Integer i;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real betasq = mp._syncPart._betaSync * mp._syncPart._betaSync;

  rho = length / th;
  cx = cos(th);
  sx = sin(th);
  m11 = cx;
  m12 = rho * sx;
  m16 = 1000. * rho * (1.0 - cx);
  m21 = -sx / rho;
  m22 = cx;
  m26 = 1000. * sx;
  m51 = sx * Factor / 1000.;
  m52 = rho * (1.0 - cx) * Factor / 1000.;
  m56 = (betasq * length - rho * sx) * Factor;

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init   = mp._x(i);
    xp_init  = mp._xp(i);

    mp._x(i)    = x_init * m11 + xp_init * m12 + mp._dp_p(i) * m16;
    mp._xp(i)   = x_init * m21 + xp_init * m22 + mp._dp_p(i) * m26;
    mp._y(i)   += length * mp._yp(i);
    mp._phi(i) += x_init * m51 + xp_init * m52 +
                  mp._dp_p(i) * m56;

    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  bend2
//
// DESCRIPTION
//  Kinetic bend transport (same as nonlinear quad transport - quad2)
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  length = length of element (either full of half step)
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void bend2(MacroPart &mp, Real &length)
{
  Real KNL, px, py, phifac;
  Integer i;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);

  for(i = 1; i <= mp._nMacros; i++)
  {
    KNL = 1.0 / (1.0 + mp._dp_p(i));
    px  = mp._xp(i) / 1000.0;
    py  = mp._yp(i) / 1000.0;

    mp._x(i)   -= KNL * length * mp._dp_p(i) * mp._xp(i);
    mp._y(i)   -= KNL * length * mp._dp_p(i) * mp._yp(i);
    phifac      = (px * px +
                   py * py +
                   mp._dp_p(i) * mp._dp_p(i) * gamma2i
                  ) / 2.0;
    phifac      = (phifac * KNL + mp._dp_p(i) * mp._dp_p(i) * gamma2i) * KNL;
    mp._phi(i) += Factor * length * phifac;
    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign  = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  bend3
//
// DESCRIPTION
//  Nonlinear curvature bend transport
//  depending on py and dE in Hamiltonian
//
// PARAMETERS
//  mp = reference to the macro-particle herd
//  th = bending angle
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void bend3(MacroPart &mp, Real &th)
{
  Real KNL, py, phifac;
  Integer i;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);

  for(i = 1; i <= mp._nMacros; i++)
  {
    KNL = 1.0 / (1.0 + mp._dp_p(i));
    py  = mp._yp(i) / 1000.0;

    phifac      = (py * py +
                   mp._dp_p(i) * mp._dp_p(i) * gamma2i
                   ) / 2.0;
    mp._xp(i)  -= 1000. * phifac * KNL * th;
    mp._y(i)   += KNL * py * mp._x(i) * th;
    phifac      = (phifac * KNL - mp._dp_p(i) * gamma2i) * KNL;
    mp._phi(i) += Factor * th * phifac * mp._x(i) / 1000.;
    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign  = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  bend4
//
// DESCRIPTION
//  Nonlinear curvature bend transport
//  depending on px in Hamiltonian
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  th = bending angle
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void bend4(MacroPart &mp, Real &th)
{
  Real KNL, px, phifac, xfac;
  Integer i;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;

  for(i = 1; i <= mp._nMacros; i++)
  {
    KNL = 1.0 / (1.0 + mp._dp_p(i));
    px = mp._xp(i) / 1000.0;

    xfac   = 1.0 + KNL * px * th / 2.0;
    phifac = KNL * KNL * px * px / 2.0;
    mp._x(i)   *= xfac * xfac;
    mp._xp(i)  /= xfac;
    mp._phi(i) += Factor * th * phifac * mp._x(i) / 1000.;
    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign  = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  bendfringeIN
//
// DESCRIPTION
//  Hard edge fringe field for a bend
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  rho = radius of curvature for bending
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void bendfringeIN(MacroPart &mp, Real &rho)
{
  Integer i;
  Real x_init, xp_init, y_init, yp_init, phi_init, dp_init, kappa;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  kappa = 1.0 / rho;

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init   = mp._x(i)  / 1000.;
    xp_init  = mp._xp(i) / 1000.;
    y_init   = mp._y(i)  / 1000.;
    yp_init  = mp._yp(i) / 1000.;
    phi_init = mp._phi(i);
    dp_init  = mp._dp_p(i);

    mp._x(i)   = 1000. * (x_init
                 + (kappa * y_init * y_init) / (2. * (1. + dp_init)));
    mp._yp(i)  = 1000. * (yp_init
                 - (kappa * xp_init * y_init) / (1. + dp_init));
    mp._phi(i) = phi_init + Factor * kappa * xp_init * y_init * y_init
                 / (2. * (1. + dp_init) * (1. + dp_init));
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  bendfringeOUT
//
// DESCRIPTION
//  Hard edge fringe field for a bend
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  rho = radius of curvature for bending
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void bendfringeOUT(MacroPart &mp, Real &rho)
{
  Integer i;
  Real x_init, xp_init, y_init, yp_init, phi_init, dp_init, kappa;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  kappa = 1.0 / rho;

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init   = mp._x(i)  / 1000.;
    xp_init  = mp._xp(i) / 1000.;
    y_init   = mp._y(i)  / 1000.;
    yp_init  = mp._yp(i) / 1000.;
    phi_init = mp._phi(i);
    dp_init  = mp._dp_p(i);

    mp._x(i)   = 1000. * (x_init
                 - (kappa * y_init * y_init) / (2. * (1. + dp_init)));
    mp._yp(i)  = 1000. * (yp_init
                 + (kappa * xp_init * y_init) / (1. + dp_init));
    mp._phi(i) = phi_init - Factor * kappa * xp_init * y_init * y_init 
                 / (2. * (1. + dp_init) * (1. + dp_init));
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  soln
//
// DESCRIPTION
//  Integration through a solenoid
//
// PARAMETERS
//  mp     = reference to the macro-particle herd
//  length = integration length
//  B      = magnetic field (1/m)
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void soln(MacroPart &mp, Real &length, Real &B)
{
  Integer i;
  Real x_init, px_init, y_init, py_init;
  Real KNL, phase, cs, sn;
  Real cu, cpu, u_init, pu_init, u, pu, phifac;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init   = mp._x(i) / 1000.;
    px_init  = mp._xp(i) / 1000.;
    y_init   = mp._y(i) / 1000.;
    py_init  = mp._yp(i) / 1000.;
    KNL      = 1.0 / (1.0 + mp._dp_p(i));

    cu      =  y_init / 2.     - px_init / B;
    cpu     =  x_init * B / 2. + py_init;
    u_init  =  y_init / 2.     + px_init / B;
    pu_init = -x_init * B / 2. + py_init;
    phase = KNL * B * length;
    cs = cos(phase);
    sn = sin(phase);

    u  =  u_init * cs      + pu_init * sn / B;
    pu = -u_init * B * sn  + pu_init * cs;

    mp._x(i)  = 1000. * (-pu + cpu) / B;
    mp._xp(i) =  500. * (u - cu) * B;
    mp._y(i)  = 1000. * (u + cu);
    mp._yp(i) =  500. * (pu + cpu);

    phifac = (pu_init * pu_init +
              B * B * u_init * u_init +
              mp._dp_p(i) * mp._dp_p(i) * gamma2i
             ) / 2.0;
    phifac = (phifac * KNL - mp._dp_p(i) * gamma2i) * KNL;
    mp._phi(i) += Factor * length * phifac;
    if(Abs(mp._phi(i)) > Consts::pi)
    {
      Real sign = -mp._phi(i) / Abs(mp._phi(i));
      mp._phi(i) = Consts::pi * sign + fmod(mp._phi(i),Consts::pi);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
// NAME
//
//  wedgebendCF
//
// DESCRIPTION
//  Straight bends particles through wedge for Combined Function non-SBEND
//
// PARAMETERS
//  mp  = reference to the macro-particle herd
//  e = wedge angle
//  inout = 0 for in, 1 for out
//  rho = radius of curvature
//  vecnum = number of multipole terms
//  pole = multipolarities of multipole terms
//  kl = integrated strengths of multipole terms
//  skew = skewness  of multipole terms
//  nsteps = number of integraton steps
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void wedgebendCF(MacroPart &mp, Real &e, Integer &inout,
                 Real &rho,
                 Integer &vecnum,
                 Vector(Integer) &pole,
                 Vector(Real) &kl,
                 Vector(Integer) &skew,
                 Integer &nsteps)
{
  Real kappa, cs, sn, ct, tn;
  Real x_init, p0_init, s, sm, sm2, klint;
  Integer i, j, l, nst;

  nst = nsteps / 2;
  if(nst < 1) nst = 1;
  kappa = 1000. / rho;
  cs = cos(e);
  sn = sin(e);
  ct = cs / sn;

  for(i = 1; i <= mp._nMacros; i++)
  {
    x_init = mp._x(i) / 1000.;

    if(inout == 0)
    {
      s = -x_init / ct;
    }
    else
    {
      p0_init = 1000. * (1.0 + mp._dp_p(i));
      tn = mp._xp(i) / p0_init;
      s = -x_init / (ct + tn);
    }

    sm = s / Real(nst);
    sm2 = sm / 2.0;

    drifti(mp, i, sm2);
    mp._xp(i) = mp._xp(i) - sm * kappa;
    for (l = 1; l <= vecnum; l++)
    {
      klint = kl(l) * sm;
      multpi(mp, i, pole(l), klint, skew(l));
    }
    for(j = 1; j < nst; j++)
    {
      drifti(mp, i, sm);
      mp._xp(i) = mp._xp(i) - sm * kappa;
      for (l = 1; l <= vecnum; l++)
      {
        klint = kl(l) * sm;
        multpi(mp, i, pole(l), klint, skew(l));
      }
    }
    drifti(mp, i, sm2);
  }
}

Matrix(Real) MAT_Identity()
{
  Matrix(Real) M;
  M.resize(6,6);

  M = 0.0;
  M(1,1) = 1.0;
  M(2,2) = 1.0;
  M(3,3) = 1.0;
  M(4,4) = 1.0;
  M(5,5) = 1.0;
  M(6,6) = 1.0;

  return M;
}

Matrix(Real) MAT_Tilt(Real &tilt, Matrix(Real) &Min)
{
  Matrix(Real) M;
  M.resize(6,6);

  if(abs(tilt) <= Consts::tiny)
  {
    M = Min;
    return M;
  }

  Matrix(Real) TILT, TEMP;
  Integer m = 6, l = 6, n = 6;

  TILT.resize(6,6);
  TEMP.resize(6,6);

  TILT = MAT_Identity();

  Real cs = cos(tilt);
  Real sn = sin(tilt);
  TILT(1,1)  =  cs;
  TILT(1,3)  = -sn;
  TILT(3,1)  =  sn;
  TILT(3,3)  =  cs;
  TILT(2,2)  =  cs;
  TILT(2,4)  = -sn;
  TILT(4,2)  =  sn;
  TILT(4,4)  =  cs;
  TEMP = MAT_Mult(m, l, n, Min, TILT);

  TILT(1,1) =  cs;
  TILT(1,3) =  sn;
  TILT(3,1) = -sn;
  TILT(3,3) =  cs;
  TILT(2,2) =  cs;
  TILT(2,4) =  sn;
  TILT(4,2) = -sn;
  TILT(4,4) =  cs;

  M = MAT_Mult(m, l, n, TILT, TEMP);

  return M;
}

Matrix(Real) MAT_Drift(Real &length, Real &gamma)
{
  Matrix(Real) M;
  M.resize(6,6);

  M = MAT_Identity();

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;

  Real gammaloc = gamma + (gamma - 1.0 / gamma) * TeaPot::dPoP;

  M(1,2) = length;
  M(3,4) = length;
  M(5,6) = -length / (gammaloc * gammaloc);

  return M;
}

Matrix(Real) MAT_Thin(Real &kln, Real &kls)
{
  Matrix(Real) M;
  M.resize(6,6);

  M = MAT_Identity();

  if(Abs(kln) > Consts::tiny)
  {
    M(2,1) = -kln / (1.0 + TeaPot::dPoP);
    M(4,3) =  kln / (1.0 + TeaPot::dPoP);
  }

  if(Abs(kls) > Consts::tiny)
  {
    M(2,3) =  kls / (1.0 + TeaPot::dPoP);
    M(4,1) =  kls / (1.0 + TeaPot::dPoP);
  }

  return M;
}

Matrix(Real) MAT_Quad(Real &length, Real &gamma, Real &kq)
{
  Matrix(Real) M;
  M.resize(6,6);

  Real sqrt_kq, kqlength, cs, sn;

  M = MAT_Drift(length, gamma);

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;
  if(Abs(kq) <= Consts::tiny) return M;

  Real gammaloc = gamma + (gamma - 1.0 / gamma) * TeaPot::dPoP;
  Real kqloc = kq / (1.0 + TeaPot::dPoP);

  M = 0.0;
  M(5,5) =  1.0;
  M(5,6) = -length / (gammaloc * gammaloc);
  M(6,6) =  1.0;

  if(kqloc > 0.0)
  {
    sqrt_kq = pow(kqloc, 0.5);
    kqlength = sqrt_kq * length;
    cs = cos(kqlength);
    sn = sin(kqlength);
    M(1,1) =  cs;
    M(1,2) =  sn / sqrt_kq;
    M(2,1) = -sn * sqrt_kq;
    M(2,2) =  cs;
    cs = cosh(kqlength);
    sn = sinh(kqlength);    
    M(3,3) =  cs;
    M(3,4) =  sn / sqrt_kq;
    M(4,3) =  sn * sqrt_kq;
    M(4,4) =  cs;
  }
  else if(kqloc < 0.0)
  {
    sqrt_kq = pow(-kqloc, 0.5);
    kqlength = sqrt_kq * length;
    cs = cosh(kqlength);
    sn = sinh(kqlength);
    M(1,1) =  cs;
    M(1,2) =  sn / sqrt_kq;
    M(2,1) =  sn * sqrt_kq;
    M(2,2) =  cs;
    cs = cos(kqlength);
    sn = sin(kqlength);
    M(3,3) =  cs;
    M(3,4) =  sn / sqrt_kq;
    M(4,3) = -sn * sqrt_kq;
    M(4,4) =  cs;
  }

  return M;
}

Matrix(Real) MAT_Bend(Real &length, Real &gamma, Real &theta,
                      Real &kq, Real &ea1, Real &ea2)
{
  Matrix(Real) M;
  M.resize(6,6);

  M = MAT_Quad(length, gamma, kq);

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;
  if(abs(theta) <= Consts::tiny) return M;

  Real gammaloc = gamma + (gamma - 1.0 / gamma) * TeaPot::dPoP;
  Real kqloc = kq / (1.0 + TeaPot::dPoP);
  Real rho = length / theta;

  M = 0.0;
  M(5,5) = 1.0;
  M(5,6) = -length / (gammaloc * gammaloc);
  M(6,6) = 1.0;

  if((Abs(kq) <= Consts::tiny) && (Abs(TeaPot::dPoP) <= Consts::tiny))
  {
    Real cs = cos(theta);
    Real sn = sin(theta);
    M(1,1)  =  cs;
    M(1,2)  =  sn * rho;           //  sn / sqrt_kq
    M(1,6)  =  (1.0 - cs) * rho;   //  (1.0 - cs) / (kqx * rho)
    M(2,1)  = -sn / rho;           // -sn * sqrt_kq
    M(2,2)  =  cs;
    M(2,6)  =  sn;                 //  sn / (sqrt_kq * rho)
    M(3,3)  =  1.0;
    M(3,4)  =  length;
    M(4,4)  =  1.0;
    M(5,1)  =  sn;                 //  sn / (sqrt_kq * rho)
    M(5,2)  =  (1.0 - cs) * rho;   //  (1.0 - cs) / (kqx * rho)
    M(5,6) +=  (length - sn * rho);
           //  (length - sn / sqrt_kq) / (kqx * rho * rho)
  }
  else
  {
    Real kqx =  kqloc +
                (1.0 - TeaPot::dPoP) / (rho * rho * (1.0 + TeaPot::dPoP));
    Real kqy = -kqloc;
    if(kqx > 0.0)
    {
      Real sqrt_kq = pow(kqx, 0.5);
      Real kllength = sqrt_kq * length;
      Real cs =  cos(kllength);
      Real sn =  sin(kllength);
      M(1,1)  =  cs;
      M(1,2)  =  sn / sqrt_kq;
      M(1,6)  =  (1.0 - cs) / (kqx * rho * (1.0 + TeaPot::dPoP));
      M(2,1)  = -sn * sqrt_kq;
      M(2,2)  =  cs;
      M(2,6)  =  sn / (sqrt_kq * rho * (1.0 + TeaPot::dPoP));
      M(5,1)  =  sn / (sqrt_kq * rho);
      M(5,2)  =  (1.0 - cs) / (kqx * rho);
      M(5,6) +=  (length - sn / sqrt_kq) /
                 (kqx * rho * rho * (1.0 + TeaPot::dPoP));
    }
    else if(kqx < 0.0)
    {
      Real sqrt_kq = pow(-kqx, 0.5);
      Real kllength = sqrt_kq * length;
      Real cs = cosh(kllength);
      Real sn = sinh(kllength);
      M(1,1)  =  cs;
      M(1,2)  =  sn / sqrt_kq;
      M(1,6)  =  (1.0 - cs) / (kqx * rho * (1.0 + TeaPot::dPoP));
      M(2,1)  =  sn * sqrt_kq;
      M(2,2)  =  cs;
      M(2,6)  =  sn / (sqrt_kq * rho * (1.0 + TeaPot::dPoP));
      M(5,1)  =  sn / (sqrt_kq * rho);
      M(5,2)  =  (1.0 - cs) / (kqx * rho);
      M(5,6) +=  (length - sn / sqrt_kq) /
                 (kqx * rho * rho * (1.0 + TeaPot::dPoP));
    }
    else if(kqx == 0.0)
    {
      M(1,1)  =  1.0;
      M(1,2)  =  length;
      M(1,6)  =  length * length / (2.0 * rho * (1.0 + TeaPot::dPoP));
      M(2,2)  =  1.0;
      M(2,6)  =  length / (rho * (1.0 + TeaPot::dPoP));
      M(5,1)  =  length / rho;
      M(5,2)  =  length * length / (2.0 * rho);
      M(5,6) +=  length * length * length /
                 (6.0 * rho * rho * (1.0 + TeaPot::dPoP));
    }
    if(kqy > 0.0)
    {
      Real sqrt_kq = pow(kqy, 0.5);
      Real kllength = sqrt_kq * length;
      Real cs = cos(kllength);
      Real sn = sin(kllength);
      M(3,3)  =  cs;
      M(3,4)  =  sn / sqrt_kq;
      M(4,3)  = -sn * sqrt_kq;
      M(4,4)  =  cs;
    }
    else if(kqy < 0.0)
    {
      Real sqrt_kq = pow(-kqy, 0.5);
      Real kllength = sqrt_kq * length;
      Real cs = cosh(kllength);
      Real sn = sinh(kllength);
      M(3,3)  =  cs;
      M(3,4)  =  sn / sqrt_kq;
      M(4,3)  =  sn * sqrt_kq;
      M(4,4)  =  cs;
    }
    else if(kqy == 0.0)
    {
      M(3,3)  =  1.0;
      M(3,4)  =  length;
      M(4,4)  =  1.0;
    }
  }

  Matrix(Real) M_IN_OUT, TEMP;

  if(Abs(ea1) > Consts::tiny)
  {
    Real tn = tan(ea1);

    M_IN_OUT.resize(6,6);
    M_IN_OUT = MAT_Identity();
    M_IN_OUT(2,1) =  tn / (rho * (1.0 + TeaPot::dPoP));
    M_IN_OUT(4,3) = -tn / (rho * (1.0 + TeaPot::dPoP));

    TEMP.resize(6,6);
    Integer m = 6, l = 6, n = 6;
    TEMP = MAT_Mult(m, l, n, M, M_IN_OUT);
    M = TEMP;
  }

  if(Abs(ea2) > Consts::tiny)
  {
    Real tn = tan(ea2);

    M_IN_OUT.resize(6,6);
    M_IN_OUT = MAT_Identity();
    M_IN_OUT(2,1) =  tn / (rho * (1.0 + TeaPot::dPoP));
    M_IN_OUT(4,3) = -tn / (rho * (1.0 + TeaPot::dPoP));

    TEMP.resize(6,6);
    Integer m = 6, l = 6, n = 6;
    TEMP = MAT_Mult(m, l, n, M_IN_OUT, M);
    M = TEMP;
  }

  return M;
}

Matrix(Real) MAT_Soln(Real &length, Real &gamma, Real &B)
{
  Matrix(Real) M;
  M.resize(6,6);

  M = MAT_Drift(length, gamma);

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;
  if(abs(B) <= Consts::tiny) return M;

  Real gammaloc = gamma + (gamma - 1.0 / gamma) * TeaPot::dPoP;
  Real Bloc = B / (1.0 + TeaPot::dPoP);

  Real cs = cos(Bloc * length);
  Real sn = sin(Bloc * length);

  M = 0.0;
  M(1,1) =  1.0;
  M(1,2) =  sn / Bloc;
  M(1,4) =  (1.0 - cs) / Bloc;
  M(2,2) =  cs;
  M(2,4) =  sn;
  M(3,2) = -(1.0 - cs) / Bloc;
  M(3,3) =  1.0;
  M(3,4) =  sn / Bloc;
  M(4,2) = -sn;
  M(4,4) =  cs;
  M(5,5) =  1.0;
  M(5,6) = -length / (gammaloc * gammaloc);
  M(6,6) =  1.0;

  return M;
}

Matrix(Real) MAT_Mult(Integer &m, Integer &l, Integer &n,
                      Matrix(Real) &A, Matrix(Real) &B)
{
  Integer i, k, j;
  Matrix(Real) M;

  M.resize(m,n);
  M = 0.0;
  for(i = 1; i <= m; i++)
  {
    for(j = 1; j <= n; j++)
    {
      for(k = 1; k <= l; k++)
      {
        M(i,j) += A(i,k) * B(k,j);
      }
    }
  }
  return M;
}

Matrix(Real) MAT_TW(Matrix(Real) &M)
{
  Matrix (Real) TW;
  TW.resize(3,3);
  TW(1,1) =        M(1,1) * M(1,1);
  TW(1,2) = -2.0 * M(1,1) * M(1,2);
  TW(1,3) =        M(1,2) * M(1,2);
  TW(2,1) =       -M(1,1) * M(2,1);
  TW(2,2) =        M(1,2) * M(2,1) + M(1,1) * M(2,2);
  TW(2,3) =       -M(1,2) * M(2,2);
  TW(3,1) =        M(2,1) * M(2,1);
  TW(3,2) = -2.0 * M(2,1) * M(2,2);
  TW(3,3) =        M(2,2) * M(2,2);
  return TW;
}

Matrix(Real) X_Identity()
{
  Matrix(Real) M;
  M.resize(3,3);

  M = 0.0;
  M(1,1) = 1.0;
  M(2,2) = 1.0;
  M(3,3) = 1.0;

  return M;
}

Matrix(Real) X_Drift(Real &length, Real &gamma)
{
  Matrix(Real) M;
  M.resize(3,3);

  M = X_Identity();

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;

  M(1,2) = length;

  return M;
}

Matrix(Real) X_Thin(Real &kln, Real &kls)
{
  Matrix(Real) M;
  M.resize(3,3);

  M = X_Identity();

  if(Abs(kln) > Consts::tiny)
  {
      M(2,1) = -kln / (1.0 + TeaPot::dPoP);
  }

  return M;
}

Matrix(Real) X_Quad(Real &length, Real &gamma, Real &kq)
{
  Matrix(Real) M;
  M.resize(3,3);

  real sqrt_kq, kqlength, cs, sn;

  M = X_Drift(length, gamma);

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;
  if(Abs(kq) <= Consts::tiny) return M;

  Real kqloc = kq / (1.0 + TeaPot::dPoP);

  M = 0.0;
  M(3,3) =  1.0;

  if(kqloc > 0.0)
  {
    sqrt_kq = pow(kqloc, 0.5);
    kqlength = sqrt_kq * length;
    cs = cos(kqlength);
    sn = sin(kqlength);
    M(1,1) =  cs;
    M(1,2) =  sn / sqrt_kq;
    M(2,1) = -sn * sqrt_kq;
    M(2,2) =  cs;
  }
  else if(kqloc < 0.0)
  {
    sqrt_kq = pow(-kqloc, 0.5);
    kqlength = sqrt_kq * length;
    cs = cosh(kqlength);
    sn = sinh(kqlength);
    M(1,1) =  cs;
    M(1,2) =  sn / sqrt_kq;
    M(2,1) =  sn * sqrt_kq;
    M(2,2) =  cs;
  }

  return M;
}

Matrix(Real) X_Bend(Real &length, Real &gamma, Real &theta,
                      Real &kq, Real &ea1, Real &ea2)
{
  Matrix(Real) M;
  M.resize(3,3);
  
  M = X_Quad(length, gamma, kq);

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;
  if(abs(theta) <= Consts::tiny) return M;

  Real kqloc = kq / (1.0 + TeaPot::dPoP);
  Real rho = length / theta;

  M = 0.0;
  M(3,3) = 1.0;
  if((Abs(kq) <= Consts::tiny) && (Abs(TeaPot::dPoP) <= Consts::tiny))
  {
    Real cs = cos(theta);
    Real sn = sin(theta);
    M(1,1)  =  cs;
    M(1,2)  =  sn * rho;           //  sn / sqrt_kq
    M(1,3)  =  (1.0 - cs) * rho;   //  (1.0 - cs) / (kqx * rho)
    M(2,1)  = -sn / rho;           // -sn * sqrt_kq
    M(2,2)  =  cs;
    M(2,3)  =  sn;                 //  sn / (sqrt_kq * rho)
  }
  else
  {
    Real kqx =  kqloc +
                (1.0 - TeaPot::dPoP) / (rho * rho * (1.0 + TeaPot::dPoP));
    if(kqx > 0.0)
    {
      Real sqrt_kq = pow(kqx, 0.5);
      Real kllength = sqrt_kq * length;
      Real cs =  cos(kllength);
      Real sn =  sin(kllength);
      M(1,1)  =  cs;
      M(1,2)  =  sn / sqrt_kq;
      M(1,3)  =  (1.0 - cs) / (kqx * rho * (1.0 + TeaPot::dPoP));
      M(2,1)  = -sn * sqrt_kq;
      M(2,2)  =  cs;
      M(2,3)  =  sn / (sqrt_kq * rho * (1.0 + TeaPot::dPoP));
    }
    else if(kqx < 0.0)
    {
      Real sqrt_kq = pow(-kqx, 0.5);
      Real kllength = sqrt_kq * length;
      Real cs = cosh(kllength);
      Real sn = sinh(kllength);
      M(1,1)  =  cs;
      M(1,2)  =  sn / sqrt_kq;
      M(1,3)  =  (1.0 - cs) / (kqx * rho * (1.0 + TeaPot::dPoP));
      M(2,1)  =  sn * sqrt_kq;
      M(2,2)  =  cs;
      M(2,3)  =  sn / (sqrt_kq * rho * (1.0 + TeaPot::dPoP));
    }
    else if(kqx == 0.0)
    {
      M(1,1)  =  1.0;
      M(1,2)  =  length;
      M(1,3)  =  length * length / (2.0 * rho * (1.0 + TeaPot::dPoP));
      M(2,2)  =  1.0;
      M(2,3)  =  length / (rho * (1.0 + TeaPot::dPoP));
    }
  }

  Matrix(Real) M_IN_OUT, TEMP;

  if(Abs(ea1) > Consts::tiny)
  {
    Real tn = tan(ea1);

    M_IN_OUT.resize(3,3);
    M_IN_OUT = X_Identity();
    M_IN_OUT(2,1) =  tn / (rho * (1.0 + TeaPot::dPoP));

    TEMP.resize(3,3);
    Integer m = 3, l = 3, n = 3;
    TEMP = MAT_Mult(m, l, n, M, M_IN_OUT);
    M = TEMP;
  }

  if(Abs(ea2) > Consts::tiny)
  {
    Real tn = tan(ea2);

    M_IN_OUT.resize(3,3);
    M_IN_OUT = X_Identity();
    M_IN_OUT(2,1) =  tn / (rho * (1.0 + TeaPot::dPoP));

    TEMP.resize(3,3);
    Integer m = 3, l = 3, n = 3;
    TEMP = MAT_Mult(m, l, n, M_IN_OUT, M);
    M = TEMP;
  }

  return M;
}

Matrix(Real) X_Soln(Real &length, Real &gamma, Real &B)
{
  Matrix(Real) M;
  M.resize(3,3);

  M = X_Drift(length, gamma);

  return M;
}

Matrix(Real) Y_Identity()
{
  Matrix(Real) M;
  M.resize(2,2);

  M = 0.0;
  M(1,1) = 1.0;
  M(2,2) = 1.0;

  return M;
}

Matrix(Real) Y_Drift(Real &length, Real &gamma)
{
  Matrix(Real) M;
  M.resize(2,2);

  M = Y_Identity();

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;

  M(1,2) = length;

  return M;
}

Matrix(Real) Y_Thin(Real &kln, Real &kls)
{
  Matrix(Real) M;
  M.resize(2,2);

  M = Y_Identity();

  if(Abs(kln) > Consts::tiny)
  {
    M(2,1) =  kln / (1.0 + TeaPot::dPoP);
  }

  return M;
}

Matrix(Real) Y_Quad(Real &length, Real &gamma, Real &kq)
{
  Matrix(Real) M;
  M.resize(2,2);

  real sqrt_kq, kqlength, cs, sn;

  M = Y_Drift(length, gamma);

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;
  if(Abs(kq) <= Consts::tiny) return M;

  Real kqloc = kq / (1.0 + TeaPot::dPoP);

  M = 0.0;

  if(kqloc > 0.0)
  {
    sqrt_kq = pow(kqloc, 0.5);
    kqlength = sqrt_kq * length;
    cs = cosh(kqlength);
    sn = sinh(kqlength);    
    M(1,1) =  cs;
    M(1,2) =  sn / sqrt_kq;
    M(2,1) =  sn * sqrt_kq;
    M(2,2) =  cs;
  }
  else if(kqloc < 0.0)
  {
    sqrt_kq = pow(-kqloc, 0.5);
    kqlength = sqrt_kq * length;
    cs = cos(kqlength);
    sn = sin(kqlength);
    M(1,1) =  cs;
    M(1,2) =  sn / sqrt_kq;
    M(2,1) = -sn * sqrt_kq;
    M(2,2) =  cs;
  }

  return M;
}

Matrix(Real) Y_Bend(Real &length, Real &gamma, Real &theta,
                      Real &kq, Real &ea1, Real &ea2)
{
  Matrix(Real) M;
  M.resize(2,2);
  
  M = Y_Quad(length, gamma, kq);

  if(length < 0.0) return M;
  if(Abs(length) <= Consts::tiny) return M;
  if(abs(theta) <= Consts::tiny) return M;

  Real kqloc = kq / (1.0 + TeaPot::dPoP);
  Real rho = length / theta;

  M = 0.0;

  if(Abs(kqloc) <= Consts::tiny)
  {
    M = Y_Drift(length, gamma);
  }
  else
  {
    Real kqy = -kqloc;
    if(kqy > 0.0)
    {
      Real sqrt_kq = pow(kqy, 0.5);
      Real kllength = sqrt_kq * length;
      Real cs = cos(kllength);
      Real sn = sin(kllength);
      M(1,1)  =  cs;
      M(1,2)  =  sn / sqrt_kq;
      M(2,1)  = -sn * sqrt_kq;
      M(2,2)  =  cs;
    }
    else if(kqy < 0.0)
    {
      Real sqrt_kq = pow(-kqy, 0.5);
      Real kllength = sqrt_kq * length;
      Real cs = cosh(kllength);
      Real sn = sinh(kllength);
      M(1,1)  =  cs;
      M(1,2)  =  sn / sqrt_kq;
      M(2,1)  =  sn * sqrt_kq;
      M(2,2)  =  cs;
    }
    else if(kqy == 0.0)
    {
      M = Y_Drift(length, gamma);
    }
  }

  Matrix(Real) M_IN_OUT, TEMP;

  if(Abs(ea1) > Consts::tiny)
  {
    Real tn = tan(ea1);

    M_IN_OUT.resize(2,2);
    M_IN_OUT = Y_Identity();
    M_IN_OUT(2,1) = -tn / (rho * (1.0 + TeaPot::dPoP));

    TEMP.resize(2,2);
    Integer m = 2, l = 2, n = 2;
    TEMP = MAT_Mult(m, l, n, M, M_IN_OUT);
    M = TEMP;
  }

  if(Abs(ea2) > Consts::tiny)
  {
    Real tn = tan(ea2);

    M_IN_OUT.resize(2,2);
    M_IN_OUT = Y_Identity();
    M_IN_OUT(2,1) = -tn / (rho * (1.0 + TeaPot::dPoP));

    TEMP.resize(2,2);
    Integer m = 2, l = 2, n = 2;
    TEMP = MAT_Mult(m, l, n, M_IN_OUT, M);
    M = TEMP;
  }

  return M;
}

Matrix(Real) Y_Soln(Real &length, Real &gamma, Real &B)
{
  Matrix(Real) M;
  M.resize(2,2);

  M = Y_Drift(length, gamma);

  return M;
}

Real findPhase(Real &x, Real &xp, Real &delta,
               Real &beta, Real &alpha, Real &eta, Real &etap)
{
  Real xnorm, xpnorm, phase;
  xnorm  =  (x  - eta  * delta);
  xpnorm = -(xp - etap * delta) * beta - xnorm * alpha;

  phase = atan2(xpnorm, xnorm) / Consts::twoPi;
  if(phase < 0.0) phase += 1.0;
  if(phase > 1.0) phase -= 1.0;

  return phase;
}

Void WaveForm00()
{
 /*
  * Constant Zero Waveform
  */

  TeaPot::hwaveT = 0.0;
  TeaPot::vwaveT = 0.0;
}

Void WaveForm01()
{
 /*
  * Constant Full Strength Waveform
  */

  TeaPot::hwaveT = 1.0;
  TeaPot::vwaveT = 1.0;
}

Void WaveForm02()
{
 /*
  * Constant Kick Strength Adjustments
  */

  TeaPot::hwaveT = TeaPot::hwf;
  TeaPot::vwaveT = TeaPot::vwf;
}

Void WaveForm03()
{
 /*
  * Square Root Waveform
  */

  Real tfac = (Ring::time - TeaPot::tstart) /
              (TeaPot::tstop - TeaPot::tstart);
  if(tfac < 0.0) tfac = 0.0;
  if(tfac > 1.0) tfac = 1.0;
  tfac = sqrt(tfac);

  TeaPot::hwaveT = TeaPot::hwf * (1.0 - tfac) + TeaPot::dhwf;
  TeaPot::vwaveT = TeaPot::vwf * (1.0 - tfac) + TeaPot::dvwf;
}

Void WaveForm04()
{
 /*
  * Stepped Square Root Waveform
  */

  Real useTime = 0.0;
  Real plusTime = TeaPot::tinc;
  while((Ring::time - TeaPot::tstart) > plusTime)
  {
    useTime = plusTime;
    plusTime += TeaPot::tinc;
  }

  Real tfac  = sqrt(useTime / (TeaPot::tstop - TeaPot::tstart));

  TeaPot::hwaveT = TeaPot::hwf * (1.0 - tfac) + TeaPot::dhwf;
  TeaPot::vwaveT = TeaPot::vwf * (1.0 - tfac) + TeaPot::dvwf;
}

Void WaveForm05()
{
  Real v0 = 0.693; Real t0=0;
  Real v1 = 0.599; Real t1=0.01;
  Real v2 = 0.273; Real t2=0.066;
  Real v3 = 0; Real t3=0.076;
  Real t_n=0; Real t_n1=0;
  Real v_n=0; Real v_n1=0;
  Real m=0;
  Real intercept=0;
  Real vfac = 0;

  if(Ring::time >= t0 && Ring::time <= t1)
  {
    t_n=t0; t_n1=t1;
    v_n=v0; v_n1=v1;
  }
  if(Ring::time > t1 && Ring::time <= t2)
  {
    t_n=t1; t_n1=t2;
    v_n=v1; v_n1=v2;
  }
  if(Ring::time > t2 && Ring::time <= t3)
  {
    t_n=t2; t_n1=t3;
    v_n=v2; v_n1=v3;
  }
  if(Ring::time > t3)
  {
    t_n=t3;
    v_n=v3;
  }
 
  m=(v_n1-v_n)/(t_n1-t_n);
  intercept=v_n-m*t_n;
  vfac = m*(Ring::time-t_n)+v_n;
  
  TeaPot::hwaveT = vfac;
  TeaPot::vwaveT = 0;
}

Void WaveForm06()
{
 /*
  * Linear PSR Ramp
  */

  Real tfac = (Ring::time - TeaPot::tstart) /
              (TeaPot::tstop - TeaPot::tstart);

  TeaPot::hwaveT = 0.0 * (1.0 -tfac) + TeaPot::dhwf;
  TeaPot::vwaveT = TeaPot::vwf * (1.0 - tfac) + TeaPot::dvwf;
}

Void WaveForm07()
{
 /*
  * One time kick on a chosen turn
  */

  TeaPot::hwaveT = 0.0;
  TeaPot::vwaveT = 0.0;
  if(TeaPot::kickTurn == Ring::nTurnsDone)
  {
    TeaPot::hwaveT = 1.0;
    TeaPot::vwaveT = 1.0;
  }
}

Void WaveForm08()
{
 /*
  * Time Power Waveform
  */

  Real tfac = (Ring::time - TeaPot::tstart) /
              (TeaPot::tstop - TeaPot::tstart);
  if(tfac < 0.0) tfac = 0.0;
  if(tfac > 1.0) tfac = 1.0;
  tfac = pow(tfac, TeaPot::tinc);

  TeaPot::hwaveT = TeaPot::hwf * (1.0 - tfac) + TeaPot::dhwf;
  TeaPot::vwaveT = TeaPot::vwf * (1.0 - tfac) + TeaPot::dvwf;
}

Void WaveForm09()
{
 /*
  * For Future Use
  */
}

Void WaveForm10()
{
 /*
  * Constant lattice focusing strength = 1.0
  */
  TeaPot::strength = 1.0;
}

Void WaveForm11()
{
 /*
  * Linear lattice strength variation between t1 and t2
  */
  if(Ring::time < TeaPot::tstrinitial) TeaPot::strength = TeaPot::strinitial;
  else if(Ring::time > TeaPot::tstrfinal) TeaPot::strength = TeaPot::strfinal;
  else
  {
    Real tfac = (Ring::time - TeaPot::tstrinitial) /
                (TeaPot::tstrfinal - TeaPot::tstrinitial);
    TeaPot::strength = TeaPot::strinitial +
                       tfac * (TeaPot::strfinal - TeaPot::strinitial);
  }
}

Void WaveForm12()
{
 /*
  * For Future Use
  */
  Real x = Accelerate::BSynch;
}

Void WaveForm13()
{
 /*
  * Square Root Waveform for multipole hkickers
  */

  Real tfac = sqrt((Ring::time - TeaPot::tstart) /
                   (TeaPot::tstop - TeaPot::tstart));
  if(tfac < 0.0) tfac = 0.0;
  if(tfac > 1.0) tfac = 1.0;

  TeaPot::hwaveT = TeaPot::hwf * (1.0 - tfac) + TeaPot::dhwf;
  TeaPot::vwaveT = TeaPot::vwf * (1.0 - tfac) + TeaPot::dvwf;
  TeaPot::strength = TeaPot::hwaveT;
}

Void WaveForm14()
{
 /*
  * For Future Use
  */
  TeaPot::strength = TeaPot::strfinal;
//  cerr << "WF14: TeaPot::strength = " << TeaPot::strength << "\n";
}

Void WaveForm15()
{
 /*
  * For Future Use
  */
  TeaPot::strength = TeaPot::strinitial;
//  cerr << "WF15: TeaPot::strength = " << TeaPot::strength << "\n";
}

Void WaveForm16()
{
 /*
  * For Future Use
  */
  TeaPot::strength = TeaPot::tstrfinal;
//  cerr << "WF16: TeaPot::strength = " << TeaPot::strength << "\n";
}

Void WaveForm17()
{
 /*
  * For Future Use
  */
  TeaPot::strength = TeaPot::tstrinitial;
//  cerr << "WF17: TeaPot::strength = " << TeaPot::strength << "\n";
}

Void WaveForm18()
{
 /*
  * For Future Use
  */
}

Void WaveForm19()
{
 /*
  * For Future Use
  */
}

