/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   ECloud.mod
//
// AUTHOR
//   Y. Sato, A. Shishlo
//   
//
// CREATED
//   01/14/2004
//
// DESCRIPTION
//   Module descriptor file for the ECloud module. This module contains
//   information about e-cloud calculations. The MPI parallel implementation.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Particles.h"
#include "MacroPart.h"
#include "Node.h"
#include "MapBase.h"
#include "TransMapHead.h"

#include "PartsDistributor_MPI.h"
#include "EP_NodeCalculator.hh"
#include "CntrlSecEmSurface.hh"
#include "absrbSurface.hh"
#include "perfectRflctSurface.hh"
#include "UniformField.hh"
#include "QuadMagField.hh"
#include "eBeamTracker.hh"

#include "ECloud.h"
#include "EP_NodeStore.hh"
#include "EP_Node.hh"

#include <iostream>
#include <cstdlib>

extern Array(ObjectPtr) mParts;
extern Array(ObjectPtr) nodes;
extern Array(ObjectPtr) tMaps;


eBunch* ebunch_external_tmp;


Void ECloud::ctor()
{
   
}

Void ECloud::dtor()
{
    
}

//===================================================================
//EP_NodeCalculator related methods
//===================================================================

//Creates MacroPartDistributor with nSlices_total longitudinal slices
Void ECloud::ECloudCreatePartsDistibutor(const Integer& nSlices_total)
{
  Integer nCountLM = 1000;
  MacroPartDistributor* PartDistr = MacroPartDistributor::GetMacroPartDistributor(nCountLM,nSlices_total);
}


Void ECloud::ECloudCreateCalculator(const Integer &xBins, const Integer &yBins, const Integer &nZ_slices,
				    const Real &xSize, const Real &ySize,
				    const Integer &BPPoints, const String &BPShape, const Integer &BPModes,
                                    const Integer& minProtonBunchSize)
{

    Integer BPShape_ind;
    if( BPShape != "Circle" && BPShape != "Ellipse" && BPShape != "Rectangle"){
      except(badPipeShape);
    }

    if(BPShape == "Circle") BPShape_ind=1;
    if(BPShape == "Ellipse") BPShape_ind=2;
    if(BPShape == "Rectangle") BPShape_ind=3;  

    EP_NodeCalculator* epCalc = 
      EP_NodeCalculator::getNodeCalculator(xBins,yBins,nZ_slices,
					   xSize,ySize,BPPoints,BPShape_ind,BPModes,
                                           minProtonBunchSize);
}

//Sets the number of steps in longitudinal direction (default 1500)
Void ECloud::ECloudSetNumberLongSteps( const Integer& nLongSteps)
{
  EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
  epCalc->setNumberLongSteps(nLongSteps);
}

//Sets the number of steps for pBunch field updates (default 1)
Void ECloud::ECloudSetNumberPFieldUpdates(const Integer& PBunchFiledUpdateSteps)
{  
  EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
  epCalc->setNumberPBunchFieldUpdates(PBunchFiledUpdateSteps);
}

//Sets the number of steps of integration in the slice (default 20)
Void ECloud::ECloudSetNumberStepsInSlice(const Integer& nTrackerSteps)
{
  EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
  epCalc->setNumberStepsInSlice(nTrackerSteps);
}

//Sets using simplectic or non-simplectic tracking (default 0)
Void ECloud::ECloudUseSimplecticTracker(const Integer& useSimplecticTracker)
{
   EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
   epCalc->setSimplecticTraking(useSimplecticTracker);
}

//Use boundary conditions for fields or not ( 1-yes 0-no) for tracker
Void ECloud::ECloudSetAddBoundaryCondsForCalc(const Integer& addBoundary)
{
   EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
   epCalc->setAddBoundaryPotentialsInfo(addBoundary);  
}

//Will the b-bunch kick from e-cloud apply 1-yes (default) 0-no
Void ECloud::ECloudSetpBunchKickApply(const Integer& pBunchKickApply)
{
   EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
   epCalc->setInfoPBunchKicksApply(pBunchKickApply);
}

//Sets the effective length coefficient.it will affect on p-Bunch kick.
Void ECloud::ECloudSetEffLengthCoeffForCalc(const Real& effLengthCoeff)
{
   EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
   epCalc->setEffLengthCoeff(effLengthCoeff);
}

//Sets the transverse x-shift function for x-coords. of the p-bunch
Void ECloud::ECloudSetTransvShiftFuncX(Vector(Real) &phiV, Vector(Real) &xShiftV )
{
  int nP = phiV.rows();
  if(phiV.rows() == xShiftV.rows()){
    Function* func = new Function();
    for(int i = 1; i <= nP; i++){
      func->add(phiV(i),xShiftV(i));
    }
   EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
   epCalc->setPBunchTransFunctionX(func);
  }
  else{
    std::cerr<< "ALL Calculations WRONG! ECloudSetTransvShiftFuncX: sizes of arrays are different!!!!!"<<std::endl;
  }
}

//Sets the transverse y-shift function for y-coords. of the p-bunch
Void ECloud::ECloudSetTransvShiftFuncY(Vector(Real) &phiV, Vector(Real) &yShiftV )
{
  int nP = phiV.rows();
  if(phiV.rows() == yShiftV.rows()){
    Function* func = new Function();
    for(int i = 1; i <= nP; i++){
      func->add(phiV(i),yShiftV(i));
    }
   EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
   epCalc->setPBunchTransFunctionY(func);
  }
  else{
    std::cerr<< "ALL Calculations WRONG! ECloudSetTransvShiftFuncY: sizes of arrays are different!!!!!"<<std::endl;
  }
}

//Sets the longitudinal density modulation function the p-bunch
Void ECloud::ECloudSetLongDensModFuncY(Vector(Real) &phiV, Vector(Real) &densCoeffV )
{
  int nP = phiV.rows();
  if(phiV.rows() == densCoeffV.rows()){
    Function* func = new Function();
    for(int i = 1; i <= nP; i++){
      func->add(phiV(i),densCoeffV(i));
    }
   EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
   epCalc->setPBunchDensityFunction(func);
  }
  else{
    std::cerr<< "ALL Calculations WRONG! ECloudSetLongDensModFuncY: sizes of arrays are different!!!!!"<<std::endl;
  }
}

//===================================================================
//EP_Node related methods
//===================================================================

//Creates EP_Node and returns the its index in the Store.
Integer ECloud::ECloudMakeEP_Node(const Integer &orderIn, const Real &lengthIn)
{
  char str_ind[14];
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  int ind = epStore->getSize();
  sprintf(str_ind,"(%d)", ind); 

  String wname("EP_Node");
  wname = wname + str_ind;

  Integer order = orderIn;
  Real length   = lengthIn;

  EP_Node* epNode = new EP_Node(wname,order,length);
  Integer indI = epStore->addEP_Node(epNode);

  if (Ring::nNodes == nodes.size()) nodes.resize(Ring::nNodes + 100);
  nodes(Ring::nNodes) = epNode;
  Ring::nNodes++;

  return indI;
}

//Set a surface to the EP_Node with particular index.
//There will be small memory leak realted to the death_func in the EP_Node
//because the baseSurface class does not delete this death_func for 
//classes different from CntrlSecEmSurface. But we can live with it.
//To fix the problem we should move the death_func deleting from CntrlSecEmSurface
//to the baseSurface class. We are going to do this in the future.
Void ECloud::ECloudSetSurfaceForEP_Node(const Integer& nodeIndex, const String& surfType)
{

  if(surfType != "BLACK" && surfType != "MIRROR" && 
     surfType != "COPPER" && surfType != "STEEL" && 
     surfType != "TITAN") except(badSurfaceType);

  baseSurface* surface = NULL;
  if(surfType == "BLACK")  surface = new absrbSurface();
  if(surfType == "MIRROR") surface = new perfectRflctSurface();
  if(surfType == "COPPER") surface = new  CntrlSecEmSurface(0); 
  if(surfType == "STEEL")  surface = new  CntrlSecEmSurface(1); 
  if(surfType == "TITAN")  surface = new  CntrlSecEmSurface(2);

  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  epNode->resetSurface(surface);
}

//Add an uniform magnetic field to the EP_Node. Magnetic field in Tesla.
Void ECloud::ECloudAddUniformMagnField(const Integer& nodeIndex, const Real& zMin, const Real& zMax, 
                                       const Real& comp_x, const Real& comp_y,const Real& comp_z)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  
  UniformField* fld = new UniformField();
  fld->setMagneticField(comp_x,comp_y,comp_z);
  fld->setZ_MinMax(zMin,zMax);

  epNode->addFieldSource(fld);
}

//Add an uniform electric field to the EP_Node. Electric field in V/m.
Void ECloud::ECloudAddUniformElectricField(const Integer& nodeIndex, const Real& zMin, const Real& zMax, 
                                       const Real& comp_x, const Real& comp_y,const Real& comp_z)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  
  UniformField* fld = new UniformField();
  fld->setElectricField(comp_x,comp_y,comp_z);
  fld->setZ_MinMax(zMin,zMax);

  epNode->addFieldSource(fld);
}

//Add a quadrupole magnetic field to the EP_Node. Magnetic field gradient in Tesla/mm.
Void ECloud::ECloudAddQuadMagnField(const Integer& nodeIndex, const Real& zMin, const Real& zMax, 
				    const Real& quad_mag_gradient)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  
  QuadMagField* fld = new QuadMagField();
  fld->setMagneticField(quad_mag_gradient);
  fld->setZ_MinMax(zMin,zMax);

  epNode->addFieldSource(fld);
}


//Sets the surface and volume electron generation probabilities
Void ECloud::ECloudSetElectrGenProbab( const Integer& nodeIndex, const Real& surfProb, 
                                       const Real& volProb)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  epNode->setElectronGenerationProbab(surfProb,volProb);
}

//Sets the parameters of the electron production.
Void ECloud::ECloudSetElectrGenParams( const Integer& nodeIndex, 
                                       const Integer& newMacroPerBunch, 
				       const Real& electronsPerProton, 
				       const Real& lossRate)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);

  epNode->setElectronGenerationParams(newMacroPerBunch,
				      electronsPerProton,
				      lossRate);
}

//Use boundary conditions for fields or not ( 1-yes 0-no)
Void ECloud::ECloudSetAddBoundaryConds(const Integer& nodeIndex, const Integer& addBoundary)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  epNode->setAddBoundaryPotentialsInfo(addBoundary);  
}

//Sets the effective length coefficient.it will affect on p-Bunch kick.
Void ECloud::ECloudSetEffLengthCoeff(const Integer& nodeIndex, const Real& effLengthCoeff)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  epNode->setEffLengthCoeff(effLengthCoeff);
}

//Propagates p-Bunch through the EP_Node with index = nodeIndex in the EP_NodeStore.
Void ECloud::ECloudPropagateThroughEP_Node(const Integer& nodeIndex)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);

  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  epNode->_updatePartAtNode(*mp);
}

//Removes all macro-electrons from the e-bunch.
Void ECloud::ECloudClearEBunchEP_Node(const Integer& nodeIndex)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  epNode->clearEBunch();
}

//Read macro-electrons' coordinates and put them into the e-bunch.
Void ECloud::ECloudReadEBunchEP_Node(const Integer& nodeIndex, const String& fileName)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  epNode->readEBunch(fileName.cptr());
}

//Dump macro-electrons' coordinates from the e-bunch.
Void ECloud::ECloudDumpEBunchEP_Node(const Integer& nodeIndex, const String& fileName)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  epNode->dumpEBunch(fileName.cptr());
}

//Adds an e-bunch diagnostics to the EP-node. time in [ns]
Void ECloud::ECloudAddEBunchDiagToEP_Node(const Integer& nodeIndex, const Integer& nTurn, 
                                          const Real& time, const String& fileName)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  
  SimpleEBunchDiag* eBunchDiag = new SimpleEBunchDiag(nTurn,time,fileName.cptr());
  epNode->getEBunchDiag()->addDiagnostics(eBunchDiag);
}

//Adds a surface diagnostics to the EP-node. time in [ns]
Void ECloud::ECloudAddSurfaceDiagToEP_Node(const Integer& nodeIndex, const Integer& nTurn, 
                                           const Real& timeStart, const Real& timeStop , const String& fileName)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  
 SimpleSurfaceDiag* surfDiag = new SimpleSurfaceDiag(nTurn,timeStart,timeStop,fileName.cptr()); 
 epNode->getSurfDiag()->addDiagnostics(surfDiag);
}

//Starts recording the profile during the propagation through the EP-node.
Void ECloud::ECloudStartProfileRecordEP_Node(const Integer& nodeIndex, const String& profileType)
{

  if( profileType != "PBUNCH_DENSITY" && profileType != "EBUNCH_DENSITY" &&
      profileType != "EBUNCH_XC"      && profileType != "EBUNCH_YC" &&
      profileType != "EBUNCH_SIZE"    && profileType != "ED_SURFACE" &&
      profileType != "ENERGY_SURFACE") except(badProfileType);

  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  
  int profileIndex = -1;
  if(profileType == "PBUNCH_DENSITY") profileIndex = 0;
  if(profileType == "EBUNCH_DENSITY") profileIndex = 1;
  if(profileType == "EBUNCH_XC") profileIndex = 2;
  if(profileType == "EBUNCH_YC") profileIndex = 3;
  if(profileType == "EBUNCH_SIZE") profileIndex = 4;
  if(profileType == "ED_SURFACE") profileIndex = 5;
  if(profileType == "ENERGY_SURFACE") profileIndex = 6;

  epNode->startRecordingProfiles(profileIndex);
}

//Dump the profile in the disk file.
Void ECloud::ECloudDumpProfileRecordEP_Node(const Integer& nodeIndex, 
                                    const String&  profileType,  
                                    const String&  fileName)
{
  if( profileType != "PBUNCH_DENSITY" && profileType != "EBUNCH_DENSITY" &&
      profileType != "EBUNCH_XC"      && profileType != "EBUNCH_YC" &&
      profileType != "EBUNCH_SIZE"    && profileType != "ED_SURFACE" &&
      profileType != "ENERGY_SURFACE") except(badProfileType);

  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);

  int profileIndex = -1;
  if(profileType == "PBUNCH_DENSITY") profileIndex = 0;
  if(profileType == "EBUNCH_DENSITY") profileIndex = 1;
  if(profileType == "EBUNCH_XC") profileIndex = 2;
  if(profileType == "EBUNCH_YC") profileIndex = 3;
  if(profileType == "EBUNCH_SIZE") profileIndex = 4;
  if(profileType == "ED_SURFACE") profileIndex = 5;
  if(profileType == "ENERGY_SURFACE") profileIndex = 6;

  epNode->dumpRecordingProfiles(profileIndex,fileName.cptr());
}

//Stops recording the profile during the propagation through the EP-node.
Void ECloud::ECloudStopProfileRecordEP_Node(const Integer& nodeIndex, 
                                    const String&  profileType)
{
  if( profileType != "PBUNCH_DENSITY" && profileType != "EBUNCH_DENSITY" &&
      profileType != "EBUNCH_XC"      && profileType != "EBUNCH_YC" &&
      profileType != "EBUNCH_SIZE"    && profileType != "ED_SURFACE" &&
      profileType != "ENERGY_SURFACE") except(badProfileType);

  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);

  int profileIndex = -1;
  if(profileType == "PBUNCH_DENSITY") profileIndex = 0;
  if(profileType == "EBUNCH_DENSITY") profileIndex = 1;
  if(profileType == "EBUNCH_XC") profileIndex = 2;
  if(profileType == "EBUNCH_YC") profileIndex = 3;
  if(profileType == "EBUNCH_SIZE") profileIndex = 4;
  if(profileType == "ED_SURFACE") profileIndex = 5;
  if(profileType == "ENERGY_SURFACE") profileIndex = 6;

  epNode->stopRecordingProfiles(profileIndex);
}

//Set the death probability as constant value on the surface
//to the EP_Node with particular index.
Void ECloud::ECloudSetDeathProbability(const Integer& nodeIndex,const Real& p_death_default)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  //death probability on the surface
  baseSurface* surface = epNode->getSurface();
  surface->setDeathProbability(p_death_default);
  surface->setDeathFunction(NULL);
}

//Sets death probability on the surface as function of electron energy in eV 
//Example in sc file:
//  Integer nlists = 5;
//  RealVector eV(nlists), deathProb_eV(nlists);
//  eV(1) = 0.0; deathProb_eV(1) = 0.8;
//  eV(2) = 1.0; deathProb_eV(2) = 0.5;
//  eV(3) = 2.0; deathProb_eV(3) = 0.3;
//  eV(4) = 3.0; deathProb_eV(4) = 0.1;
//  eV(5) = 4.0; deathProb_eV(5) = 0.0;
//  ECloudSetDeathFunction(epNodeIndex,eV,deathProb_eV);
Void ECloud::ECloudSetDeathFunction(const Integer& nodeIndex, const Vector(Real) &eV, const Vector(Real) &deathProb_eV )
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  baseSurface* surface = epNode->getSurface();

  int nP = eV.rows();
  if(eV.rows() == deathProb_eV.rows()){
    Function* death_func = new Function();
    for(int i = 1; i <= nP; i++){
      death_func->add(eV(i),deathProb_eV(i));
    }
    death_func->setConstStep(1);
    surface->setDeathFunction(death_func);
  }
  else{
    std::cerr<< "ALL Calculations WRONG! ECloudSetDeathFunction: sizes of arrays are different!!!!!"<<std::endl;
  }
}

//Set the number of new born macroelectrons on the surface as const value
//to the EP_Node with particular index.
Void ECloud::ECloudSetNewBornNumber(const Integer& nodeIndex,const Integer& n_new_born_default)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  baseSurface* surface = epNode->getSurface();
  surface->setNewBornNumber(n_new_born_default);
  surface->setNewBornNumberFunction(NULL);
}

//Set the maximal desirable number of macroelectrons inside e-cloud
//to the EP_Node with particular index.
Void ECloud::ECloudSetMaxMacroPartsN(const Integer& nodeIndex,const Integer& macroPartsMaxN)
{
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);
  baseSurface* surface = epNode->getSurface();
  surface->setMacroPartsHardLimitN(macroPartsMaxN);
}


////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========START===============
////////////////////////////////////////////////////////////////////////////////

//define will it be there the debugging output (default 0)
Void ECloud::ECloudSetTrackerDebugOutput(const Integer& infDebugOutput)
{
   EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
   epCalc->setTrackerDebugOutput(infDebugOutput);
}

//Propagates the empty electron bunch through the e-cloud region.
Void ECloud::ECloudDebugStartCalculator(const Real& length)
{
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  //surface
  //surfaceType = 0 for Copper, 1 for Stainless Steal, 2 for Titanium nitride coated 
  int surfaceType=2;
  CntrlSecEmSurface* surface = new CntrlSecEmSurface(surfaceType);

  //death probability on the surface
  //it is table (electrons energy in eV) - Probability
  Function* death_func = new Function();
  death_func->add(0.  , 0.8);
  death_func->add(1.  , 0.5);
  death_func->add(2.  , 0.3);
  death_func->add(3.  , 0.1);
  death_func->add(4.  , 0. );
  death_func->setConstStep(1);
  surface->setDeathFunction(death_func);

  //make empty e-bunch
  eBunch* eb = new eBunch();

  //get calculator
  EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();

  double surfProb = 1.0;
  double volProb  = 0.0;
  epCalc->setElectronGenerationProbab(surfProb,volProb);

  int newMacroPerBunch = 50000;
  double electronsPerProton = 100.;
  double lossRate = 4.0e-6;

  epCalc->setElectronGenerationParams(newMacroPerBunch,
				      electronsPerProton,
				      lossRate);

  //propagate p-bunch through the e-cloud region
  epCalc->propagate(length, *mp, eb, surface,NULL);

  delete surface;
}

//Scan the modulation frequencies of the proton bunch.
Void ECloud::ECloudDebugScanModulation(const Real& length)
{

  int iMPIini  = 0;
  int size_MPI = 1;
  int rank_MPI = 0;

  MPI_Initialized( &iMPIini);

  if (iMPIini) {
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  }


  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();
  epCalc->setInfoPBunchKicksApply(0);

  double surfProb = 1.0;
  double volProb  = 0.0;
  epCalc->setElectronGenerationProbab(surfProb,volProb);

  int newMacroPerBunch = 50000;
  double electronsPerProton = 100.;
  double lossRate = 4.0e-6;

  epCalc->setElectronGenerationParams(newMacroPerBunch,
				      electronsPerProton,
				      lossRate);


  //define e-bunch
  eBunch* eb = new eBunch();

  //surface
  //surfaceType = 0 for Copper, 1 for Stainless Steal, 2 for Titanium nitride coated 
  int surfaceType=2;
  CntrlSecEmSurface* surface = new CntrlSecEmSurface(surfaceType);

  //death probability on the surface
  //it is table (electrons energy in eV) - Probability
  Function* death_func = new Function();
  death_func->add(0.  , 0.8);
  death_func->add(1.  , 0.5);
  death_func->add(2.  , 0.3);
  death_func->add(3.  , 0.1);
  death_func->add(4.  , 0. );
  death_func->setConstStep(1);
  surface->setDeathFunction(death_func);

  Function* amp_func = new Function();
  amp_func->add(0.   , 0. );
  amp_func->add(0.2  , 0.25*1.5 );
  amp_func->add(1.0  , 0.75*1.5  );
  amp_func->add(2.0  , 1.0*1.5  );
  amp_func->add(3.0  , 1.25*1.5  );
  amp_func->add(4.0  , 1.5*1.5  );
  
  Function* mod_func = new Function();
  int nP_modF = 4000;
  double step_mod_f = 3.2/nP_modF;
  double phi_f = 0.;
  double shift_f = 0.;
  double harmon = 0.;
  
  double maxDens = 0.;


  //file for dumping information on the disk
  std::ofstream F_out;
  if(rank_MPI == 0) F_out.open ("resonance_curve.dat", std::ios::out);


  for(int i = 0; i < 1; i++){

    harmon = i*1.0;

    mod_func->clean();
    for(int i_mf = 0;  i_mf < nP_modF; i_mf++){
      phi_f = i_mf*step_mod_f;
      shift_f = amp_func->getY(phi_f)*sin(2.0*harmon*phi_f);
      mod_func->add(phi_f,shift_f);
    }
    mod_func->setConstStep(1);
    mod_func->print("shift_function.dat");
    epCalc->setPBunchTransFunctionY(mod_func);

    eb->deleteAllParticles();

    epCalc->propagate(length, *mp, eb, surface,NULL);

    maxDens = epCalc->getMaxECloudDensity();

    if(rank_MPI == 0) F_out<< harmon <<" "<< maxDens <<std::endl;
    if(rank_MPI == 0) std::cout<< "harmonics="<<harmon <<" max_den[nC/m]="<< maxDens <<std::endl;
    if(rank_MPI == 0) F_out.flush();
  }

  if(rank_MPI == 0) F_out.close(); 

  delete surface;
}


//Scan the modulation frequencies of the proton bunch.
Void ECloud::ECloudDebugEBunchInfPrint(){

  int iMPIini  = 0;
  int size_MPI = 1;
  int rank_MPI = 0;

  MPI_Initialized( &iMPIini);

  if (iMPIini) {
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  }

  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();

  int nNodes = epStore->getSize();

  EP_Node* epNode = NULL;
  eBunch* eb = NULL;

  double x, y, r, x_sum, y_sum, r_max;
  double x_sum_MPI, y_sum_MPI, r_max_MPI;

  for(int i = 0; i < nNodes; i++){
    epNode = epStore->getEP_Node(i);
    eb = epNode->getEBunch();

    int nMacroEl       = eb->getSize();
    int nMacroElGlobal = eb->getSizeGlobal();
    double totalMacroSizeGlobal = eb->getTotalMacroSizeGlobal();

    x_sum = 0.;
    y_sum = 0.;
    r_max = 0.;

    for(int j = 0; j < nMacroEl; j++){
      x = eb->x(j);
      y = eb->y(j);
      x_sum += x;
      y_sum += y;
      r = sqrt(x*x+y*y);
      if(r > r_max) r_max = r;
    }
    
    MPI_Allreduce(&x_sum,&x_sum_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(&y_sum,&y_sum_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(&r_max,&r_max_MPI,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);

    if(nMacroElGlobal > 0){
      x_sum = x_sum_MPI/nMacroElGlobal;
      y_sum = y_sum_MPI/nMacroElGlobal;
    }
    r_max = r_max_MPI;

    if( rank_MPI == 0){
      std::cout<<"epNode = "<<i
	       <<" xc = "<<x_sum
	       <<" yc = "<<y_sum
	       <<" r_max = "<<r_max
	       <<" nEl = "<<nMacroElGlobal
	       <<" totalMaco = "<<totalMacroSizeGlobal
	       <<std::endl;
    }

  }
}

//Reads e-bunch and keeps it as external object
Void ECloud::ECloudReadTempEBunch(const String&  fileName){

  ebunch_external_tmp = new eBunch();
  ebunch_external_tmp->readParticles(fileName.cptr());

}

//Restores e-bunch from external e-bunch
Void ECloud::ECloudRestoreEBunchFromExt(const Integer& nodeIndex){
  EP_NodeStore* epStore = EP_NodeStore::getEP_NodeStore();
  EP_Node* epNode = epStore->getEP_Node(nodeIndex);

  eBunch* eb = epNode->getEBunch();
  eb->deleteAllParticles();

  int size = ebunch_external_tmp->getSize();
  for(int i = 0; i < size; i++){
    eb->addParticle(
		    ebunch_external_tmp->macroSize(i),
		    ebunch_external_tmp->x(i),
		    ebunch_external_tmp->y(i),
		    ebunch_external_tmp->z(i),
		    ebunch_external_tmp->px(i),
		    ebunch_external_tmp->py(i),
		    ebunch_external_tmp->pz(i));
  }
  eb->compress();
}


Void ECloud::EBeamTrackingPBunch(const Real& _kinetic, const Real& _phi_init, 
				 const Real& _delta_phi, const Real& _rho_init,
				 const Real& _delta_rho, 
				 const Integer& _nstep, const Real& _pipe_r, 
				 const String& _posfile, 
				 const String& _angfile){

  //Variable declaration.
  double phi, rho;
  double x, y, z;
  double xnew, ynew;
  double xnew_final=0;
  double ynew_final=0;
  double px, py, pz;
  double pxnew, pynew;
  double pxnew_final=0;
  double pynew_final=0;
  double magx, magy, magz;
  double ex, ey, ez;
  double ctime;
  double macrosize;
  double kinetic;
  double egamma;
  double emass;
  double ebeta;
  double emomentum;
  double pbeta;
  double radius = _pipe_r;
  int nstep = _nstep;
 
  //First set up proton beam field and boundary

  eBeamTracker* tracker = new eBeamTracker();
  int xBins = 128;
  int yBins = 128;
  int nZ = 20;
  pbeta = 0.875;
 
  double xSize = 200.0;
  double ySize = 200.0;

  int BPPoints=100;
  //int BPPoints=1000;
  int BPShape=1; 
  int BPModes=15; 

  EP_Boundary* bndry = new EP_Boundary(xBins,yBins,xSize,ySize, 
				       BPPoints,BPShape,BPModes);

  PhiGrid3D* pBunchPhiGrid = new PhiGrid3D(nZ, bndry);  
  RhoGrid3D* pBunchRhoGrid = new RhoGrid3D(nZ, bndry);

  int nMaxMacroParticles = Injection::nMaxMacroParticles;
  double ring_length = 248.0*1000.0;
  double coeffPhiToZ = ring_length/(2.0*epModuleConstants::PI);

  MacroPart *mp;

  mp= MacroPart::safeCast
      (mParts((Particles::mainHerd & Particles::All_Mask) - 1));
  
  double phimin = -epModuleConstants::PI;
  double phimax = epModuleConstants::PI;
  for(int ip = 1; ip <= nMaxMacroParticles; ip++){
    if(mp->_phi(ip) < phimin) phimin = mp->_phi(ip);
    if(mp->_phi(ip) > phimax) phimax = mp->_phi(ip);
  }

  double zmin =  phimin*coeffPhiToZ;
  double zmax =  phimax*coeffPhiToZ;
  
  pBunchRhoGrid->setMinMaxZ(zmin, zmax);
  pBunchPhiGrid->setMinMaxZ(zmin, zmax);
  pBunchRhoGrid->makePeriodicRing();
  pBunchPhiGrid->clean();
  pBunchRhoGrid->clean();
   
  for(int ip = 1; ip <= nMaxMacroParticles; ip++){
    pBunchRhoGrid->binParticleIntoArr3D(Injection::nReals_Macro, mp->_x(ip), 
					mp->_y(ip), mp->_phi(ip)*coeffPhiToZ);
  }
  pBunchRhoGrid->findPotential(pBunchPhiGrid);
  pBunchPhiGrid->addBoundaryPotential();
 
  pBunchFieldSource* pbF = new pBunchFieldSource(1,-10.,10.,bndry);
  pbF->setBeta(pbeta);

  //int nZslice =  int ((z - zmin)/(ring_length/nZ) + 0.5);
  int nZslice= 0;
  pBunchPhiGrid->setValuesForSlice(z,nZslice,pbF->getPhiGrid3D());

  tracker->addMagneticFieldSource(pbF);
  tracker->addElectricFieldSource(pbF);
   //Done setting up proton beam.


  //Now set up e particle and track it in the field.

  std::ofstream fio1(_posfile, std::ios::out);
  std::ofstream fio2(_angfile, std::ios::out);
  eBunch* eb = new eBunch();

  std::cout<<"here1\n";
  emass = 0.511;
  kinetic = _kinetic;
  egamma = kinetic/emass + 1;
  ebeta = sqrt(1 - 1/(egamma*egamma));
  emomentum = (kinetic + emass) * ebeta;
  macrosize = 1;
  ctime = 3e8*(2*(0.110)/3e8/ebeta)*1000.0;
  //ctime = 3e8*(2*(1.10)/3e8/ebeta)*1000.0;

  phi = _phi_init;
  rho = _rho_init;
  
  //Declare the starting conditions in rotated, translated frame of ref.
  xnew = 0;
  //ynew = -radius;
  ynew = -110;
  pxnew = 0;
  pynew = emomentum;
  pz = 0;
  
  while(phi < epModuleConstants::PI){
    while(rho <= -_rho_init){
        eb->deleteAllParticles();

	//Go from rotated, translated ref frame to regular ref frame.
	x = xnew*cos(phi) - ynew*sin(phi) + rho*cos(phi);
	y = xnew*sin(phi) + ynew*cos(phi) + rho*sin(phi);
	z = 0;
	px = pxnew*cos(phi) - pynew*sin(phi);
	py = pxnew*sin(phi) + pynew*cos(phi);
 
	std::cout<<"phi = "<<phi<<", rho= "<<rho<<"\n";
	//cout<<"x, y, px, py = "<<x<<" "<<y<<" "<<px<<" "<<py<<"\n";

	eb->addParticle(macrosize,x,y,z,px,py,pz);
	eb->compress();

	double xfinal = 0; double yfinal = 0; double zfinal = 0;
	double pxfinal = 0; double pyfinal = 0; double pzfinal = 0;  
 
	tracker->moveParticlesWeakH(ctime, nstep, eb, kinetic,
				    xfinal, yfinal,zfinal, pxfinal, 
				    pyfinal, pzfinal);

	//Now transfer the final coords into the rotated and translated 
	//ref frame.
 
	double x_0 = rho*cos(phi);
	double y_0 = rho*sin(phi);
	xnew_final = (xfinal - x_0)*cos(phi) + (yfinal - y_0)*sin(phi);
	ynew_final = -(xfinal - x_0)*sin(phi) +  (yfinal - y_0)*cos(phi);
	pxnew_final = pxfinal*cos(phi) + pyfinal*sin(phi);
	pynew_final = -pxfinal*sin(phi) + pyfinal*cos(phi);
	
	fio1<<phi<<" "<<rho<<" "<<xnew_final<<"\n";
	fio2<<phi<<" "<<rho<<" "<<pxnew_final/pynew_final<<"\n";
	rho += _delta_rho;
    }
    phi+=_delta_phi;
    std::cout<<"phi is "<<phi<<"  and delta_phi is "<<_delta_phi<<"\n";
    rho = _rho_init;
  }
	fio1.close();
	fio2.close();
}


//Scales the dE coordinate in the proton Herd with sqrt(voltCoeff)
Void ECloud::ECloudScaleHerdEnergy(const Real& voltCoeff){
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));

  int nParts = mp->_nMacros;
  double dE = 0.;
  double dppFac = mp->_syncPart._dppFac;
  for(int ip = 1; ip <= nParts; ip++){
    dE = mp->_deltaE(ip)*sqrt(voltCoeff);
    mp->_deltaE(ip) = dE;
    mp->_dp_p(ip) = dE * dppFac;
  }
}

////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========STOP================
////////////////////////////////////////////////////////////////////////////////
