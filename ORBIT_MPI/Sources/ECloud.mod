/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   ECloud.mod
//
// AUTHOR
//   Y. Sato, A. Shishlo
//   
//
// CREATED
//   01/14/2004
//
// DESCRIPTION
//   Module descriptor file for the ECloud module. This module contains
//   information about e-cloud calculations. The MPI parallel implementation.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module ECloud - "ECloud module."
                         - "Written by  Y.Sato, A.Shishlo"
{

Inherits Consts;

Errors
  badHerdNo             - "Sorry, you asked for an undefined macro Herd",
  tooFewTransMaps       - "Sorry, you need more Transfer Matricies before you",
  badPipeShape          - "Sorry, BPShape parameters should be Circle, Ellipse, or Rectangle",
  badSurfaceType        - "Sorry, surfType should be BLACK,MIRROR,COPPER,STEEL or TITAN only",
  badProfileType        - "Sorry, profileType should be PBUNCH_DENSITY,EBUNCH_DENSITY,EBUNCH_XC,EBUNCH_YC,EBUNCH_SIZE, ED_SURFACE or ENERGY_SURFACE only"; 

public:

  Void ctor() 
                        - "Constructor for the ECloud module."
                        - "Initializes build values.";

  Void dtor() 
                        - "Destructor for the ECloud module."
                        - "Finalize the ECloud module";

  Void ECloudCreatePartsDistibutor(const Integer& nSlices_total)
                        - "Creates MacroPartDistributor with nSlices_total longitudinal slices.";

  Void ECloudCreateCalculator(const Integer &xBins,  const Integer &yBins,  const Integer &nZ_slices,
                              const Real &xSize, const Real &ySize,
                              const Integer &BPPoints,  const String &BPShape,  const Integer &BPModes,
                              const Integer& minProtonBunchSize)

                        - "Creates a calculator for EP_Nodes Nodes.";

  Void ECloudSetNumberLongSteps( const Integer& nLongSteps)
                        - "Sets the number of steps in longitudinal direction (default 1500)";

  Void ECloudSetNumberPFieldUpdates(const Integer& PBunchFiledUpdateSteps)
                        - "Sets the number of steps for pBunch field updates (default 1)";

  Void ECloudSetNumberStepsInSlice(const Integer& nTrackerSteps)
                        - "Sets the number of steps of integration in the slice (default 20)";

  Void ECloudUseSimplecticTracker(const Integer& useSimplecticTracker)
                        - "Sets using simplectic or non-simplectic tracking (default 0)";

  Void ECloudSetAddBoundaryCondsForCalc(const Integer& addBoundary)
                        - "Use boundary conditions for fields or not ( 1-yes 0-no) for tracker.";

  Void ECloudSetpBunchKickApply(const Integer& pBunchKickApply)
                        - "Will the b-bunch kick from e-cloud apply 1-yes (default) 0-no";

  Void ECloudSetEffLengthCoeffForCalc(const Real& effLengthCoeff)
                        - "set the effective length coefficient.it will affect on p-Bunch kick.";

  Void ECloudSetTransvShiftFuncX(RealVector &phiV, RealVector &xShiftV )
                        - "set the transverse x-shift function for x-coords. of the p-bunch";

  Void ECloudSetTransvShiftFuncY(RealVector &phiV, RealVector &yShiftV )
                        - "set the transverse y-shift function for y-coords. of the p-bunch";

  Void ECloudSetLongDensModFuncY(RealVector &phiV, RealVector &densCoeffV )
                        - "set the longitudinal density modulation function the p-bunch";

//--------------------------------------------------------------------------------------------
//      METHODS RELATED TO EP_Node
//---------------------------------------------------------------------------------------------

  Integer ECloudMakeEP_Node(const Integer &order, const Real &length)
                        - "Creates EP_Node and returns the its index in the Store.";

  Void ECloudSetSurfaceForEP_Node(const Integer& nodeIndex, const String& surfType) 
            - "Set a surface to the EP_Node with particular index.";

  Void ECloudAddUniformMagnField(const Integer& nodeIndex, const Real& zMin, const Real& zMax, 
                                 const Real& comp_x, const Real& comp_y,const Real& comp_z)
             - "Add an uniform magnetic field to the EP_Node. Magnetic field in Tesla.";  

	Void ECloudAddUniformElectricField(const Integer& nodeIndex, const Real& zMin, const Real& zMax, 
                                 const Real& comp_x, const Real& comp_y,const Real& comp_z)
             - "Add an uniform electric field to the EP_Node. Electric field in V/m.";				 
						 
  Void ECloudAddQuadMagnField(const Integer& nodeIndex, const Real& zMin, const Real& zMax, 
			      const Real& quad_mag_gradient)
             - "Add a quadrupole magnetic field to the EP_Node. Magnetic field gradient in Tesla/mm.";  

  Void ECloudSetElectrGenProbab( const Integer& nodeIndex, const Real& surfProb, const Real& volProb)
                        - "Sets the surface and volume electron generation probabilities.";

  Void ECloudSetElectrGenParams( const Integer& nodeIndex, 
                                 const Integer& newMacroPerBunch, 
                                 const Real& electronsPerProton, 
                                 const Real& lossRate)
                        - "Sets the parameters of the electron production.";

  Void ECloudSetAddBoundaryConds(const Integer& nodeIndex, const Integer& addBoundary)
                        - "Use boundary conditions for fields or not ( 1-yes 0-no).";

  Void ECloudSetEffLengthCoeff(const Integer& nodeIndex, const Real& effLengthCoeff)
                        - "set the effective length coefficient.it will affect on p-Bunch kick.";

  Void ECloudPropagateThroughEP_Node(const Integer& nodeIndex)
            - "Propagates p-Bunch through the EP_Node with index = nodeIndex in the EP_NodeStore.";

  Void ECloudClearEBunchEP_Node(const Integer& nodeIndex)
            - "Removes all macro-electrons from the e-bunch.";

  Void ECloudReadEBunchEP_Node(const Integer& nodeIndex, const String& fileName)
            - "Read macro-electrons' coordinates and put them into the e-bunch.";

  Void ECloudDumpEBunchEP_Node(const Integer& nodeIndex, const String& fileName)
            - "Dump macro-electrons' coordinates from the e-bunch.";

  Void ECloudAddEBunchDiagToEP_Node(const Integer& nodeIndex, const Integer& nTurn, 
                                    const Real& time, const String& fileName)
            - "Adds an e-bunch diagnostics to the EP-node. time in [ns]";

  Void ECloudAddSurfaceDiagToEP_Node(const Integer& nodeIndex, const Integer& nTurn, 
                                     const Real& timeStart, const Real& timeStop , const String& fileName)
            - "Adds a surface diagnostics to the EP-node. time in [ns]";

  //profile recording, profileType  can be:
  // PBUNCH_DENSITY - p-bunch line density
  // EBUNCH_DENSITY - e-bunch line density
  // EBUNCH_XC      - x-centroid of the e-bunch
  // EBUNCH_YC      - y-centroid of the e-bunch
  // EBUNCH_SIZE    - size of the e-bunch
  // ED_SURFACE     - electron, hitting surface, current [A/m]
  // ENERGY_SURFACE - energy absorbed by surface [MeV/s/m]
  Void ECloudStartProfileRecordEP_Node(const Integer& nodeIndex, const String& profileType)
            - "Starts recording a profile during the propagation through the EP-node.";

  Void ECloudDumpProfileRecordEP_Node(const Integer& nodeIndex, 
                                      const String&  profileType,  
                                      const String&  fileName)
            - "Dumps the profile in the disk file.";


  Void ECloudStopProfileRecordEP_Node(const Integer& nodeIndex, 
                                      const String&  profileType)
            - "Stops recording the profile during the propagation through the EP-node.";

  Void ECloudSetDeathProbability(const Integer& nodeIndex,
	                         const Real& p_death_default)
            - "Sets a constant death probability on the surface.";

  Void ECloudSetDeathFunction(const Integer& nodeIndex, 
  	                      const RealVector &eV, const RealVector &deathProb_eV )
            - "Sets a death probability as a funtion of electron energy in eV.";

  Void ECloudSetNewBornNumber(const Integer& nodeIndex,
	                      const Integer& n_new_bron_default)
            - "Sets a constant number of new born macroelectrons on the surface.";

  Void ECloudSetMaxMacroPartsN(const Integer& nodeIndex,const Integer& macroPartsMaxN)
            - "Set the maximal desirable number of macroelectrons inside e-cloud.";

//-------------debugging methods-----------start-------------------------

  Void ECloudSetTrackerDebugOutput(const Integer& infDebugOutput)
                        - "Debugging method."
                        - "Defines will it be there the debugging output (default 0 - no).";

  Void ECloudDebugStartCalculator(const Real& length)
                        - "Debugging method."
                        - "Propagates the empty electron bunch through the e-cloud region.";

  Void ECloudDebugScanModulation(const Real& length)
                        - "Debugging method."
                        - "Scan the modulation frequencies of the proton bunch.";

  Void ECloudDebugEBunchInfPrint()
                        - "Debugging method."
                        - "Prints information about e-bunches inside EP_Nodes.";


  Void EBeamTrackingPBunch(const Real& kinetic, const Real& rho_init, 
			const Real& delta_rho, const Real& phi_init, 
			const Real & delta_phi, const Integer & nstep, 
			const Real & pipe_r, const String & posfile, 
			const String & angfile)
			- "Debugging for electron tomography with real"
			- " proton fields";


  Void ECloudReadTempEBunch(const String&  fileName)
                        - "Debugging method."
                        - "Reads e-bunch and keeps it as external object.";

  Void ECloudRestoreEBunchFromExt(const Integer& nodeIndex)
                        - "Debugging method."
                        - "Restores e-bunch from external e-bunch.";


  Void ECloudScaleHerdEnergy(const Real& voltCoeff)
                        - "Debugging method."
                        - "Scales the dE coordinate in the proton Herd with sqrt(voltCoeff).";


//-------------debugging methods-----------stop-------------------------

}

