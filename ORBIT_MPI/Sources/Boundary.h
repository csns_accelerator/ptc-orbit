//# Library            : ORBIT_MPI
//# File               : Boundary.h
//# Original code      : Jeff Holmes, Slava Danilov, John Galambos 

#ifndef ORBIT_SC3D_BOUNDARY_H
#define ORBIT_SC3D_BOUNDARY_H

#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <mpi.h>
#include "rfftw.h"
#include "fftw.h"

#include "RealMat.h"

//Consts.h - header from SuperCode with Consts::pi
#include "Consts.h"

/** The Boundary class is used to define boundary condition.
*/
    
class  ORBIT_Boundary
{
public:

  /// Constructor
  ORBIT_Boundary(int xBins   , int yBins   , 
                 double xSize, double ySize,
                 int BPPoints, int BPShape, 
                 int BPModes , double eps );

  /// Destructor
  virtual ~ORBIT_Boundary();

  /// Adds potential from the boundary to the grid
  void addBoundaryPotential(double** phisc, 
                           int iXmin, int iXmax, 
                           int iYmin, int iYmax);

  void addBoundaryPotential(Matrix(Real) &phisc,
                           int iXmin, int iXmax, 
                           int iYmin, int iYmax);

  /// Returns the pointers to the FFT array of the Green Function
  FFTW_COMPLEX* getOutFFTGreenF();

  ///Defines is a particle into the boundary or not ( returns < 0 - it is not)
  int isInside(double x, double y);

  ///Define external parameters from inner 
  void defineExtXYgrid(double *xGridMin, double *xGridMax, 
                       double *yGridMin, double *yGridMax,
                       double *dx      , double *dy );

  ///Define external grid's index limits from inner 
  void defineExtXYlimits(int *ixMinB, int *ixMaxB, 
                         int *iyMinB, int *iyMaxB);

  ///Get X-grid
  double* getXgrid();

  ///Get Y-grid
  double* getYgrid();

  ///Get the boundary shape
  int getBoundaryShape();

  ///Get the bounding curve limit for X-coordinate
  double getBoundaryXsize();


  ///Get the bounding curve limit for Y-coordinate
  double getBoundaryYsize();

  ///Get the number of boundary points
  int getNumbBPoints();

  ///debug method
  void checkBoundary();
  
protected:

  /// Finalize MPI
  void  _finalize_MPI();

  /// Calculates inverse matrix
  void  _gaussjinv(double **a, int n);

  /// Calculates all of the LSQM functions at one point
  double* lsq_fuctions(double x, double y);

  /// Calculates additional potential phi at one point
  double calculatePhi(double x, double y);

  /// Defines the FFT of the Green Function
  void _defineGreenF();

  /// Defines LSQM matrix
  void _defineLSQMatrix();

  /// Defines LSQM coefficients
  void _defineLSQcoeff();

  ///Sets array with the interpolation coefficients for boundary points
  void _setInterpolationCoeff();


protected:

  //Grid size
  int xBins_;
  int yBins_;

  //Green function 
  double** greensF_;

  //Epsilon smoothing parameter
  double eps_;
  
  //FFT arrays
  FFTW_REAL* in_;
  FFTW_COMPLEX* out_;

  rfftwnd_plan planForward_;

  //boundary points
  //BPPoints_ - number of boundary points
  //shape of the boundary  BPShape_ = 1 - Circle 2 - Ellipse 3 - Rectangle
  int BPPoints_;
  int BPShape_;

  //Size of the bounding curve from 0 to max [m]
  //The center is the point with X,Y coordinates (xSize_/2.,ySize_/2.)
  double xSize_;
  double ySize_; 

  //R_cirle_ - radius of the circle, BPa_,BPb_ - ellipse parameters, BPx_length_,BPy_width_ - rectangle parameters
  double R_cirle_,BPa_,BPb_,BPx_length_,BPy_width_;
  double BPrnorm_;
  
  double* BPx_;
  double* BPy_;
  int* iBPx_;
  int* iBPy_;  
  double* theta_;
  double* BPphi_; //external potential at the boundary points   
  
  //Grid array and parameters
  double xGridMin_, xGridMax_, yGridMin_, yGridMax_;
  double  dx_  ,  dy_;

  //Min and max indexes of XY-plane's grid to operate with potential
  int ixMinB_,ixMaxB_,iyMinB_,iyMaxB_;
  
  double* xGrid_;
  double* yGrid_; 

  //LSQM array and matrix 
  //2*BPModes_+1 - number of functions in LSQM 
  int BPModes_; 
  double* func_vector_;
  double* cof_vector_;
  
  double** LSQ_matrix_;
  double** tmp_matrix_;

  //number of boundary points
  int nBpoints_;

  //array with the interpolation coefficients for boundary points
  //(former Wxm, Wx0, Wxp, Wym, Wy0, Wyp for 9-points scheme)
  double** W_coeff_;
  
};

#endif
