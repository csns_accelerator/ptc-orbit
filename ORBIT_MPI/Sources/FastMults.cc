/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   FastMults.cc
//
// AUTHOR
//   Jeff Holmes
//   ORNL, jzh@ornl.gov
//
// CREATED
//   10/2013
//
// DESCRIPTION
//   File for Fast Multipole Methods
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "FastMults.h"


///////////////////////////////////////////////////////////////////////////
// The Cell class:
///////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------
// Constructor

//-------------------------------------------------------------------------
Cell::Cell(int nTerms, int level,
           int yIndex, int xIndex)
{
  _nTerms      = nTerms;
  _level       = level;
  _yIndex      = yIndex;
  _xIndex      = xIndex;

  _EmptyM      = 1;
  _EmptyT      = 1;

  _MultCoeffs = new std::complex<double>[_nTerms];
  _TaylorCoeffs = new std::complex<double>[_nTerms];

  int k;
  for(k = 0; k < _nTerms; k++)
  {
    _MultCoeffs[k] = std::complex<double>(0.0, 0.0);
    _TaylorCoeffs[k] = std::complex<double>(0.0, 0.0);
  }

  _nCellMacros = 0;
  _nCellBPs    = 0;
}

//-------------------------------------------------------------------------
// Destructor

//-------------------------------------------------------------------------
Cell::~Cell()
{
  delete[] _MultCoeffs;
  delete[] _TaylorCoeffs;
}

//-------------------------------------------------------------------------
// Other Cell Methods

//-------------------------------------------------------------------------
void Cell::_MultCoeffsfromMacros(MacroPart& mp)
{
  int n, i, k;

  _EmptyM = 1;
  _EmptyT = 1;

  for(k = 0; k < _nTerms; k++)
  {
    _MultCoeffs[k] = std::complex<double>(0.0, 0.0);
    _TaylorCoeffs[k] = std::complex<double>(0.0, 0.0);
  }

  if(_nCellMacros == 0) return;

  _EmptyM = 0;

  double x, y;
  std::complex<double> z;
  std::complex<double> zk;

  _MultCoeffs[0] = -std::complex<double>(double(_nCellMacros), 0.0);

  for(n = 0; n < _nCellMacros; n++)
  {
    i = _CellMacroIndex[n];
    x = mp._x(i) - _xCenter;
    y = mp._y(i) - _yCenter;
    z = std::complex<double>(x, y);
    zk = std::complex<double>(1.0, 0.0);
    for(k = 1; k < _nTerms; k++)
    {
      zk *= z;
      _MultCoeffs[k] += zk / double(k);
    }
  }
}

//-------------------------------------------------------------------------
void Cell::_addToMultCoeffs(std::complex<double>* MultCoeffs)
{
  _EmptyM = 0;

  int k;

  for(k = 0; k < _nTerms; k++)
  {
    _MultCoeffs[k] += MultCoeffs[k];
  }
}

//-------------------------------------------------------------------------
void Cell::_addToTaylorCoeffs(std::complex<double>* TaylorCoeffs)
{
  _EmptyT = 0;

  int k;

  for(k = 0; k < _nTerms; k++)
  {
    _TaylorCoeffs[k] += TaylorCoeffs[k];
  }
}

///////////////////////////////////////////////////////////////////////////
// The Row class:
///////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------
// Constructor

//-------------------------------------------------------------------------
Row::Row(int nTerms, int level,
         int yIndex, int nXYBins)
{
  _nTerms  = nTerms;
  _level   = level;
  _yIndex  = yIndex;
  _nXYBins  = nXYBins;

  _CellPtr = new Cell*[_nXYBins];

  int xIndex;
  for(xIndex = 0; xIndex < _nXYBins; xIndex++)
  {
    _CellPtr[xIndex] = new Cell(_nTerms, _level,
                                _yIndex, xIndex);
  }
}

//-------------------------------------------------------------------------
// Destructor

//-------------------------------------------------------------------------
Row::~Row()
{
  int xIndex;
  for(xIndex = 0; xIndex < _nXYBins; xIndex++)
  {
    delete _CellPtr[xIndex];
  }
  delete[] _CellPtr;
}


///////////////////////////////////////////////////////////////////////////
// The Level class:
///////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------
// Constructor

//-------------------------------------------------------------------------
Level::Level(int nTerms, int level, int nXYBins)
{
  _nTerms  = nTerms;
  _level   = level;
  _nXYBins = nXYBins;

  _RowPtr = new Row*[_nXYBins];

  int yIndex;
  for(yIndex = 0; yIndex < _nXYBins; yIndex++)
  {
    _RowPtr[yIndex] = new Row(_nTerms, _level,
                               yIndex, _nXYBins);
  }
}

//-------------------------------------------------------------------------
// Destructor

//-------------------------------------------------------------------------
Level::~Level()
{
  int yIndex;
  for(yIndex = 0; yIndex < _nXYBins; yIndex++)
  {
    delete _RowPtr[yIndex];
  }
  delete[] _RowPtr;
}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS CalculatorBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(FMMCalculator, Object);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   FMMCalculator
//
// INHERITANCE RELATIONSHIPS
//   FMMCalculator -> CalculatorBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   Class for calculating space charge kicks
//   by the fast multipole method including
//   a conducting wall boundary
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------
void FMMCalculator::_SetCoords(double xMin, double xMax,
                               double yMin, double yMax)
{
  int level, xIndex, yIndex, nXYB;
  double xMinLoc, xCenterLoc, xMaxLoc;
  double yMinLoc, yCenterLoc, yMaxLoc;
  double dx, dy;

  _xMin = xMin;
  _xMax = xMax;
  _yMin = yMin;
  _yMax = yMax;

  nXYB = _nXYBins;
  level = 0;
  while(nXYB > 1)
  {
    dy = (_yMax - _yMin) / double(nXYB);
    for(yIndex = 0; yIndex < nXYB; yIndex++)
    {
      yMinLoc = _yMin + dy * double(yIndex);
      yMaxLoc = _yMin + dy * double(yIndex + 1);
      yCenterLoc = (yMinLoc + yMaxLoc) / 2.0;
      dx = (_xMax - _xMin) / double(nXYB);
      for(xIndex = 0; xIndex < nXYB; xIndex++)
      {
        xMinLoc = _xMin + dx * double(xIndex);
        xMaxLoc = _xMin + dx * double(xIndex + 1);
        xCenterLoc = (xMinLoc + xMaxLoc) / 2.0;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_xMin = xMinLoc;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_xCenter = xCenterLoc;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_xMax = xMaxLoc;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_yMin = yMinLoc;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_yCenter = yCenterLoc;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_yMax = yMaxLoc;
      }
    }
    nXYB /= 2;
    level++;
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_DistributeParts(MacroPart& mp)
{
  int i, n, xIndex, yIndex, zIndex, zIm, zIp;
  double dx = (_xMax - _xMin) / double(_nXYBins);
  double dy = (_yMax - _yMin) / double(_nXYBins);
  double dz = _ZLength / double(_nZBins);

  double zmax = mp._phi(1);
  double zmin = mp._phi(1);
  double zi, zfrac;

  for(i = 1; i <= mp._nMacros; i++)
  {
    xIndex = int((mp._x(i) - _xMin) / dx);
    yIndex = int((mp._y(i) - _yMin) / dy);
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroIndex.push_back(i);
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcex.push_back(0.0);
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcey.push_back(0.0);
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellMacros++;
    if(mp._phi(i) > zmax) zmax = mp._phi(i);
    if(mp._phi(i) < zmin) zmin = mp._phi(i);
  }

  for(n = 0; n < _nZBins; n++)
  {
    _ZDist[n] = 0;
  }

  _ZCenter = (zmax + zmin) / 2.0;

  for(i = 1; i <= mp._nMacros; i++)
  {
    zi = mp._phi(i) - _ZCenter;
    if(zi >  _ZLength / 2.0) zi -= _ZLength;
    if(zi < -_ZLength / 2.0) zi += _ZLength;
    zi += _ZLength / 2.0;
    zIndex = int(zi / dz + 0.5);
    zfrac = zi / dz - double(zIndex);
    if(zIndex < 0) zIndex = zIndex = _nZBins - 1;
    if(zIndex > _nZBins - 1) zIndex = 0;
    zIm = zIndex - 1;
    if(zIm < 0) zIm = _nZBins - 1;
    zIp = zIndex + 1;
    if(zIp > _nZBins - 1)  zIp = 0;
    _ZDist[zIm]   += 0.5 * (0.5 - zfrac) * (0.5 - zfrac);
    _ZDist[zIndex] += 0.75 - zfrac * zfrac;
    _ZDist[zIp]   += 0.5 * (0.5 + zfrac) * (0.5 + zfrac);
  }

  for(n = 0; n < _nZBins; n++)
  {
    _ZDist[n] *= double(_nZBins) / double(mp._nMacros);
  }

  for(i = 0; i < _BPPoints; i++)
  {
    xIndex = int((_BPx[i] - _xMin) / dx);
    yIndex = int((_BPy[i] - _yMin) / dy);
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPIndex.push_back(i);
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPPot.push_back(0.0);
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellBPs++;
  }
}

void FMMCalculator::_SeedMultipoles(MacroPart& mp)
{
  int xIndex, yIndex;
  
  for(yIndex = 0; yIndex < _nXYBins; yIndex++)
  {
    for(xIndex = 0; xIndex < _nXYBins; xIndex++)
    {
      _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_MultCoeffsfromMacros(mp);
    }
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_UpSweep()
{
  int leveli, yIndexi, xIndexi, levelf, yIndexf, xIndexf;
  int nXYB = _nXYBins;

  for(levelf = 1; levelf < _nLevels - 1; levelf++)
  {
    nXYB /= 2;
    for(yIndexf = 0; yIndexf < nXYB; yIndexf++)
    {
      for(xIndexf = 0; xIndexf < nXYB; xIndexf++)
      {
        leveli = levelf - 1;
        yIndexi = 2 * yIndexf;
        xIndexi = 2 * xIndexf;
        _shiftMultipoles(leveli, yIndexi, xIndexi,
                         levelf, yIndexf, xIndexf);
        yIndexi = 2 * yIndexf;
        xIndexi = 2 * xIndexf + 1;
        _shiftMultipoles(leveli, yIndexi, xIndexi,
                         levelf, yIndexf, xIndexf);
        yIndexi = 2 * yIndexf + 1;
        xIndexi = 2 * xIndexf;
        _shiftMultipoles(leveli, yIndexi, xIndexi,
                         levelf, yIndexf, xIndexf);
        yIndexi = 2 * yIndexf + 1;
        xIndexi = 2 * xIndexf + 1;
        _shiftMultipoles(leveli, yIndexi, xIndexi,
                         levelf, yIndexf, xIndexf);
      }
    }
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_DownSweep()
{

  int leveli, yIndexi, xIndexi, levelf, yIndexf, xIndexf;
  int nXYB = _nXYBins;

  int levelp, nXYBp;
  nXYBp = 1;

  for(levelp = _nLevels - 1; levelp > 0; levelp--)
  {
    nXYBp *= 2;
    nXYB = 2 * nXYBp;
    leveli = levelp;
    levelf = levelp - 1;

    for(yIndexi = 0; yIndexi < nXYBp; yIndexi++)
    {
      for(xIndexi = 0; xIndexi < nXYBp; xIndexi++)
      {
        xIndexf = 2 * xIndexi;
        yIndexf = 2 * yIndexi;
        _shiftTaylor(leveli, yIndexi, xIndexi,
                     levelf, yIndexf, xIndexf);
        xIndexf = 2 * xIndexi + 1;
        yIndexf = 2 * yIndexi;
        _shiftTaylor(leveli, yIndexi, xIndexi,
                     levelf, yIndexf, xIndexf);
        xIndexf = 2 * xIndexi;
        yIndexf = 2 * yIndexi + 1;
        _shiftTaylor(leveli, yIndexi, xIndexi,
                     levelf, yIndexf, xIndexf);
        xIndexf = 2 * xIndexi + 1;
        yIndexf = 2 * yIndexi + 1;
        _shiftTaylor(leveli, yIndexi, xIndexi,
                     levelf, yIndexf, xIndexf);
      }
    }

    leveli = levelp - 1;

    int yIndexL, yIndexU, xIndexL, xIndexU, on;

    for(yIndexf = 0; yIndexf < nXYB; yIndexf++)
    {
      if(yIndexf == 2 * (yIndexf / 2))
      {
        yIndexL = yIndexf - 2;
        yIndexU = yIndexf + 4;
      }
      else
      {
        yIndexL = yIndexf - 3;
        yIndexU = yIndexf + 3;
      }
      if(yIndexL < 0) yIndexL = 0;
      if(yIndexU > nXYB) yIndexU = nXYB;
      for(xIndexf = 0; xIndexf < nXYB; xIndexf++)
      {
        if(xIndexf == 2 * (xIndexf / 2))
        {
          xIndexL = xIndexf - 2;
          xIndexU = xIndexf + 4;
        }
        else
        {
          xIndexL = xIndexf - 3;
          xIndexU = xIndexf + 3;
        }
        if(xIndexL < 0) xIndexL = 0;
        if(xIndexU > nXYB) xIndexU = nXYB;
        for(yIndexi = yIndexL; yIndexi < yIndexU; yIndexi++)
        {
          for(xIndexi = xIndexL; xIndexi < xIndexU; xIndexi++)
          {
            on = 1;
            if((abs(yIndexf - yIndexi) < 2) &&
               (abs(xIndexf - xIndexi) < 2)) on = 0;
            if(on) _TaylorFromMultipoles(leveli, yIndexi, xIndexi,
                                         levelf, yIndexf, xIndexf);
          }
        }
      }
    }
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_ForceFromBeam(MacroPart& mp)
{
  int yIndexi, xIndexi, yIndexf, xIndexf;

  for(yIndexi = 0; yIndexi < _nXYBins; yIndexi++)
  {
    for(xIndexi = 0; xIndexi < _nXYBins; xIndexi++)
    {
      _evaluateMultipoles(mp, yIndexi, xIndexi);
      _PairWiseOneCell(mp, yIndexi, xIndexi);
      yIndexf = yIndexi;
      xIndexf = xIndexi + 1;
      if(xIndexf < _nXYBins)
      {
        _PairWiseTwoCells(mp, yIndexi, xIndexi, yIndexf, xIndexf);
      }
      yIndexf = yIndexi + 1;
      if(yIndexf < _nXYBins)
      {
        xIndexf = xIndexi + 1;
        if(xIndexf < _nXYBins)
        {
          _PairWiseTwoCells(mp, yIndexi, xIndexi, yIndexf, xIndexf);
        }
        xIndexf = xIndexi;
        _PairWiseTwoCells(mp, yIndexi, xIndexi, yIndexf, xIndexf);
        xIndexf = xIndexi - 1;
        if(xIndexf >= 0)
        {
          _PairWiseTwoCells(mp, yIndexi, xIndexi, yIndexf, xIndexf);
        }
      }
    }
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_ForceFromBoundary(MacroPart& mp)
{
  int n, i, yIndex, xIndex;
  int nCellBPs, nCellMacros;

  double* BPPhi;
  BPPhi = new double[_BPPoints];
  for(i = 0; i < _BPPoints; i++)
  {
    BPPhi[i] = 0.0;
  }

  for(yIndex = 0; yIndex < _nXYBins; yIndex++)
  {
    for(xIndex = 0; xIndex < _nXYBins; xIndex++)
    {
      nCellBPs = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellBPs;
      for(n = 0; n < nCellBPs; n++)
      {
        i = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPIndex[n];
        BPPhi[i] = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPPot[n];
      }
    }
  }

  double* RHS;
  RHS = new double[2 * _BPModes + 1];
  for(n = 0; n < 2 * _BPModes + 1; n++)
  {
    RHS[n] = 0.0;
    for(i = 0; i < _BPPoints; i++)
    {
      RHS[n] -= _BPPhiH1[i][n] * BPPhi[i];
    }
  }

  double* BPCoeffs;
  BPCoeffs = new double[2 * _BPModes + 1];
  for(n = 0; n < 2 * _BPModes + 1; n++)
  {
    BPCoeffs[n] = 0.0;
    for(i = 0; i < 2 * _BPModes + 1; i++)
    {
      BPCoeffs[n] += _BPPhiH2[n][i] * RHS[i];
    }
  }

  double BPresi, BPresf;
  BPresi = 0.0;
  BPresf = 0.0;
  for(i = 0; i < _BPPoints; i++)
  {
    BPresi += BPPhi[i] * BPPhi[i];
    for(n = 0; n < 2 * _BPModes + 1; n++)
    {
      BPPhi[i] += _BPPhiH1[i][n] * BPCoeffs[n];
    }
    BPresf += BPPhi[i] * BPPhi[i];
  }

/*
  double BPrat = 1.0;
  if(BPresi != 0.0) BPrat = BPresf / BPresi;
  cout << "Boundary residual BPresi, BPresf, BPrat = "
       << BPresi << "   " << BPresf << "   " << BPrat << "\n";
*/

  int m, mc, ms;
  double x, y, r, rmc, rms, rsq, xcoeff, ycoeff, Fx, Fy;
  std::complex<double> z;
  std::complex<double> zm;

  for(yIndex = 0; yIndex < _nXYBins; yIndex++)
  {
    for(xIndex = 0; xIndex < _nXYBins; xIndex++)
    {
      nCellMacros = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellMacros;
      for(n = 0; n < nCellMacros; n++)
      {
        i = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroIndex[n];
        x = mp._x(i);
        y = mp._y(i);
        rsq = x * x + y * y;
        xcoeff = x / rsq;
        ycoeff = y / rsq;
        z = std::complex<double>(x / _BPrnorm, y / _BPrnorm);
        zm = std::complex<double>(1.0, 0.0);
        Fx = 0.0;
        Fy = 0.0;
        for(m = 1; m <= _BPModes; m++)
        {
          zm *= z;
          rmc = std::real(zm);
          rms = std::imag(zm);
          mc = 2 * m - 1;
          ms = 2 * m;
          Fx -= double(m) *
                (BPCoeffs[mc] * (rmc * xcoeff + rms * ycoeff) +
                 BPCoeffs[ms] * (rms * xcoeff - rmc * ycoeff));
          Fy -= double(m) *
                (BPCoeffs[mc] * (rmc * ycoeff - rms * xcoeff) +
                 BPCoeffs[ms] * (rms * ycoeff + rmc * xcoeff));
        }
        _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcex[n] += Fx;
        _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcey[n] += Fy;
      }
      nCellBPs = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellBPs;
      for(n = 0; n < nCellBPs; n++)
      {
        i = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPIndex[n];
        _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPPot[n] = BPPhi[i];
      }
    }
  }

  delete BPPhi;
  delete RHS;
  delete BPCoeffs;
}

//-------------------------------------------------------------------------
void FMMCalculator::_ApplyForce(MacroPart& mp, double SCKick_Coeff)
{
  _SCKick_Coeff = SCKick_Coeff;

  int nCellMacros;
  double kickx, kicky;

  int i, n, xIndex, yIndex, zIndex, zIm, zIp;
  double dz = _ZLength / double(_nZBins);
  double zi, zfrac, zfactor;

  for(yIndex = 0; yIndex < _nXYBins; yIndex++)
  {
    for(xIndex = 0; xIndex < _nXYBins; xIndex++)
    {
      nCellMacros = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellMacros;
      for(n = 0; n < nCellMacros; n++)
      {
        i = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroIndex[n];

        zi = mp._phi(i) - _ZCenter;
        if(zi >  _ZLength / 2.0) zi -= _ZLength;
        if(zi < -_ZLength / 2.0) zi += _ZLength;
        zi += _ZLength / 2.0;
        zIndex = int(zi / dz + 0.5);
        zfrac = zi / dz - double(zIndex);
        if(zIndex < 0) zIndex = zIndex = _nZBins - 1;
        if(zIndex > _nZBins - 1) zIndex = 0;
        zIm = zIndex - 1;
        if(zIm < 0) zIm = _nZBins - 1;
        zIp = zIndex + 1;
        if(zIp > _nZBins - 1)  zIp = 0;
        zfactor = 0.5 * (0.5 - zfrac) * (0.5 - zfrac) * _ZDist[zIm] +
                  (0.75 - zfrac * zfrac) * _ZDist[zIndex] +
                  0.5 * (0.5 + zfrac) * (0.5 + zfrac) * _ZDist[zIp];
        zfactor *= _SCKick_Coeff;
        kickx = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcex[n];
        kicky = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcey[n];
        mp._xp(i) += zfactor * kickx;
        mp._yp(i) += zfactor * kicky;
      }
    }
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_ClearFMM()
{
  int level, xIndex, yIndex, nXYB, k;

  nXYB = _nXYBins;
  for(level = 0; level < _nLevels; level++)
  {
    for(yIndex = 0; yIndex < nXYB; yIndex++)
    {
      for(xIndex = 0; xIndex < nXYB; xIndex++)
      {
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_EmptyM = 1;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_EmptyT = 1;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroIndex.clear();
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcex.clear();
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcey.clear();
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellMacros = 0;
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPIndex.clear();
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPPot.clear();
        _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellBPs = 0;
        for(k = 0; k < _nTerms; k++)
        {
          _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_MultCoeffs[k] =
            std::complex<double>(0.0, 0.0);
          _LevelPtr[level]->_RowPtr[yIndex]->_CellPtr[xIndex]->_TaylorCoeffs[k] =
            std::complex<double>(0.0, 0.0);
        }
      }
    }
    nXYB /= 2;
  }

  for(k = 0; k < _nZBins; k++)
  {
    _ZDist[k] = 0.0;
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_shiftMultipoles(int leveli, int yIndexi, int xIndexi,
                                     int levelf, int yIndexf, int xIndexf)
{
  if(_LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_EmptyM) return;

  int k, l;

  double x0i, y0i, x0f, y0f;
  x0i = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_xCenter;
  y0i = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_yCenter;
  x0f = _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_xCenter;
  y0f = _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_yCenter;
  std::complex<double> zshift = std::complex<double>(x0i - x0f, y0i - y0f);
  std::complex<double>* z0;
  z0 = new std::complex<double>[_nTerms];

  z0[0] = std::complex<double>(1.0, 0.0);
  for(l = 1; l < _nTerms; l++)
  {
    z0[l] = z0[l - 1] * zshift;
  }

  std::complex<double>* MultCoeffsi;
  std::complex<double>* MultCoeffsf;
  MultCoeffsi = new std::complex<double>[_nTerms];
  MultCoeffsf = new std::complex<double>[_nTerms];

  for(k = 0; k < _nTerms; k++)
  {
    MultCoeffsi[k] = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_MultCoeffs[k];
  }

  MultCoeffsf[0] = MultCoeffsi[0];
  for(l = 1; l < _nTerms; l++)
  {
    MultCoeffsf[l] = -MultCoeffsi[0] * z0[l] / double(l);
    for(k = 1; k <= l; k++)
    {
      MultCoeffsf[l] += MultCoeffsi[k] * _Binom[l - 1][k - 1] * z0[l - k];
    }
  }

  _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_addToMultCoeffs(MultCoeffsf);

    delete[] z0;
    delete[] MultCoeffsi;
    delete[] MultCoeffsf;
}

//-------------------------------------------------------------------------
void FMMCalculator::_TaylorFromMultipoles(int leveli, int yIndexi, int xIndexi,
                                          int levelf, int yIndexf, int xIndexf)
{
  if(_LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_EmptyM) return;

  int k, l;

  double x0i, y0i, x0f, y0f;
  x0i = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_xCenter;
  y0i = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_yCenter;
  x0f = _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_xCenter;
  y0f = _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_yCenter;
  std::complex<double> zshift = std::complex<double>(x0i - x0f, y0i - y0f);
  std::complex<double>* z0;
  z0 = new std::complex<double>[_nTerms];

  z0[0] = std::complex<double>(1.0, 0.0);
  for(l = 1; l < _nTerms; l++)
  {
    z0[l] = z0[l - 1] * zshift;
  }

  std::complex<double>* MultCoeffsi;
  std::complex<double>* TaylorCoeffsf;
  MultCoeffsi = new std::complex<double>[_nTerms];
  TaylorCoeffsf = new std::complex<double>[_nTerms];

  for(k = 0; k < _nTerms; k++)
  {
    MultCoeffsi[k] = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_MultCoeffs[k];
  }

  TaylorCoeffsf[0] = MultCoeffsi[0] * log(-zshift);
  double minusonek = 1.0;
  for(k = 1; k < _nTerms; k++)
  {
    minusonek *= -1.0;
    TaylorCoeffsf[0] += MultCoeffsi[k] * minusonek / z0[k];
  }

  for(l = 1; l < _nTerms; l++)
  {
    minusonek = 1.0;
    TaylorCoeffsf[l] = std::complex<double>(0.0, 0.0);
    for(k = 1; k < _nTerms; k++)
    {
      minusonek *= -1.0;
      TaylorCoeffsf[l] += MultCoeffsi[k] * _Binom[l + k - 1][k - 1] *
                          minusonek / z0[k];
    }
    TaylorCoeffsf[l] = (TaylorCoeffsf[l] -
                        MultCoeffsi[0] / double(l)) / z0[l];
  }

  _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_addToTaylorCoeffs(TaylorCoeffsf);

  delete[] z0;
  delete[] MultCoeffsi;
  delete[] TaylorCoeffsf;
}

//-------------------------------------------------------------------------
void FMMCalculator::_shiftTaylor(int leveli, int yIndexi, int xIndexi,
                                 int levelf, int yIndexf, int xIndexf)
{
  if(_LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_EmptyT) return;

  int k, l;

  double x0i, y0i, x0f, y0f;
  x0i = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_xCenter;
  y0i = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_yCenter;
  x0f = _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_xCenter;
  y0f = _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_yCenter;
  std::complex<double> zshift = std::complex<double>(x0i - x0f, y0i - y0f);
  std::complex<double>* mz0;
  mz0 = new std::complex<double>[_nTerms];

  mz0[0] = std::complex<double>(1.0, 0.0);
  for(l = 1; l < _nTerms; l++)
  {
    mz0[l] = -mz0[l - 1] * zshift;
  }

  std::complex<double>* TaylorCoeffsi;
  std::complex<double>* TaylorCoeffsf;
  TaylorCoeffsi = new std::complex<double>[_nTerms];
  TaylorCoeffsf = new std::complex<double>[_nTerms];

  for(k = 0; k < _nTerms; k++)
  {
    TaylorCoeffsi[k] = _LevelPtr[leveli]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_TaylorCoeffs[k];
  }

  for(l = 0; l < _nTerms; l++)
  {
    TaylorCoeffsf[l] = std::complex<double>(0.0, 0.0);
    for(k = l; k < _nTerms; k++)
    {
      TaylorCoeffsf[l] += TaylorCoeffsi[k] * _Binom[k][l] * mz0[k - l];
    }
  }

  _LevelPtr[levelf]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_addToTaylorCoeffs(TaylorCoeffsf);

    delete[] mz0;
    delete[] TaylorCoeffsi;
    delete[] TaylorCoeffsf;
}

//-------------------------------------------------------------------------
void FMMCalculator::_evaluateMultipoles(MacroPart& mp,
                                        int yIndex, int xIndex)
{
  int n, i, k;
  double x, y;
  std::complex<double> z;
  std::complex<double> zk;
  std::complex<double> Sum;
  double Fx, Fy, Pot;

  int nCellMacros = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellMacros;
  int nCellBPs    = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellBPs;


  for(n = 0; n < nCellMacros; n++)
  {
    i =  _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroIndex[n];
    x = mp._x(i) - _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_xCenter;
    y = mp._y(i) - _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_yCenter;
    z = std::complex<double>(x, y);
    zk = std::complex<double>(1.0, 0.0);
    Sum = std::complex<double>(0.0, 0.0);
    for(k = 1; k < _nTerms; k++)
    {
      Sum += double(k) * _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_TaylorCoeffs[k] * zk;
      zk *= z;
    }
    Fx = -std::real(Sum);
    Fy =  std::imag(Sum);
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcex[n] += Fx;
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcey[n] += Fy;
  }

  for(n = 0; n < nCellBPs; n++)
  {
    i = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPIndex[n];
    x = _BPx[i] - _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_xCenter;
    y = _BPy[i] - _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_yCenter;
    z = std::complex<double>(x, y);
    zk = std::complex<double>(1.0, 0.0);
    Sum = std::complex<double>(0.0, 0.0);
    for(k = 0; k < _nTerms; k++)
    {
      Sum += _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_TaylorCoeffs[k] * zk;
      zk *= z;
    }
    _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPPot[n] += std::real(Sum);
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_PairWiseOneCell(MacroPart& mp,
                                     int yIndex, int xIndex)
{
  int nf, ni, indf, indi;
  double dx, dy, r2;
  double xterm, yterm, Pot;
  double eps2 = 0.0;
  if(_eps > 0.0) eps2 = _eps * _eps;

  int nCellMacros = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellMacros;
  int nCellBPs    = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_nCellBPs;

  for(nf = 1; nf < nCellMacros; nf++)
  {
    indf = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroIndex[nf];
    for(ni = 0; ni < nf; ni++)
    {
      indi = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroIndex[ni];
      dx = mp._x(indf) - mp._x(indi);
      dy = mp._y(indf) - mp._y(indi);
      r2 = dx * dx + dy * dy + eps2;
      xterm = dx / r2;
      yterm = dy / r2;
      _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcex[nf] += xterm;
      _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcey[nf] += yterm;
      _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcex[ni] -= xterm;
      _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroForcey[ni] -= yterm;
    }
  }

  for(nf = 0; nf < nCellBPs; nf++)
  {
    indf = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPIndex[nf];
    for(ni = 0; ni < nCellMacros; ni++)
    {
      indi = _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellMacroIndex[ni];
      dx = _BPx[indf] - mp._x(indi);
      dy = _BPy[indf] - mp._y(indi);
      r2 = dx * dx + dy * dy + eps2;
      Pot = -log(r2) / 2.0;
      _LevelPtr[0]->_RowPtr[yIndex]->_CellPtr[xIndex]->_CellBPPot[nf] += Pot;
    }
  }
}

//-------------------------------------------------------------------------
void FMMCalculator::_PairWiseTwoCells(MacroPart& mp,
                                      int yIndexi, int xIndexi,
                                      int yIndexf, int xIndexf)
{
  int nf, ni, indf, indi;
  double x, y, dx, dy, r2;
  double xterm, yterm, Pot;
  double eps2 = 0.0;
  if(_eps > 0.0) eps2 = _eps * _eps;

  int nCellMacrosi = _LevelPtr[0]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_nCellMacros;
  int nCellMacrosf = _LevelPtr[0]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_nCellMacros;
  int nCellBPsi    = _LevelPtr[0]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_nCellBPs;
  int nCellBPsf    = _LevelPtr[0]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_nCellBPs;
  for(nf = 0; nf < nCellMacrosf; nf++)
  {
    indf = _LevelPtr[0]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_CellMacroIndex[nf];
    for(ni = 0; ni < nCellMacrosi; ni++)
    {
      indi = _LevelPtr[0]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_CellMacroIndex[ni];
      dx = mp._x(indf) - mp._x(indi);
      dy = mp._y(indf) - mp._y(indi);
      r2 = dx * dx + dy * dy + eps2;
      xterm = dx / r2;
      yterm = dy / r2;
      _LevelPtr[0]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_CellMacroForcex[nf] += xterm;
      _LevelPtr[0]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_CellMacroForcey[nf] += yterm;
      _LevelPtr[0]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_CellMacroForcex[ni] -= xterm;
      _LevelPtr[0]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_CellMacroForcey[ni] -= yterm;
    }
  }

  for(nf = 0; nf < nCellBPsf; nf++)
  {
    indf = _LevelPtr[0]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_CellBPIndex[nf];
    for(ni = 0; ni < nCellMacrosi; ni++)
    {
      indi = _LevelPtr[0]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_CellMacroIndex[ni];
      dx = _BPx[indf] - mp._x(indi);
      dy = _BPy[indf] - mp._y(indi);
      r2 = dx * dx + dy * dy + eps2;
      Pot = -log(r2) / 2.0;
      _LevelPtr[0]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_CellBPPot[nf] += Pot;
    }
  }

  for(ni = 0; ni < nCellBPsi; ni++)
  {
    indi = _LevelPtr[0]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_CellBPIndex[ni];
    for(nf = 0; nf < nCellMacrosf; nf++)
    {
      indf = _LevelPtr[0]->_RowPtr[yIndexf]->_CellPtr[xIndexf]->_CellMacroIndex[nf];
      dx = _BPx[indi] - mp._x(indf);
      dy = _BPy[indi] - mp._y(indf);
      r2 = dx * dx + dy * dy + eps2;
      Pot = -log(r2) / 2.0;
      _LevelPtr[0]->_RowPtr[yIndexi]->_CellPtr[xIndexi]->_CellBPPot[ni] += Pot;
    }
  }
}

