////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TImpedance.cc
//
// AUTHOR
//   Slava Danilov, Jeff Holmes, John Galambos
//   ORNL, vux@ornl.gov
//
// CREATED
//   8/11/2000
//   Revisited by Jeff Holmes 6/2014
//
//  DESCRIPTION
//   Source code for transverse impedance module
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "TImp.h"
#include "TImpedance.h"
#include "Node.h"
#include "Particles.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "ComplexMat.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>

#include "mpi.h"

using namespace std;

// STATIC DEFINITIONS

Array(ObjectPtr) TImpedancePointers;
extern Array(ObjectPtr) mParts, nodes;
extern Array(ObjectPtr) syncP;


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS TImpedance
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(TImp, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TImp::TImp, ~TImp
//
// DESCRIPTION
//   Constructor and destructor for TImp base class
//
// PARAMETERS
//   None
//
// RETURNS
//
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

TImp::TImp(const String &n, const Integer &order,
           Integer &nBins, Integer &nMacrosMin):
           Node(n, 0., order), _nBins(nBins), _nMacrosMin(nMacrosMin)
{
  _xCentroid.resize(_nBins + 1);
  _xpCentroid.resize(_nBins + 1);
  _yCentroid.resize(_nBins + 1);
  _ypCentroid.resize(_nBins + 1);
  _phiCount.resize(_nBins + 1);
  _forceCalculated = 0;

  //=== MPI stuff =====begin=====

  iMPIini_ = 0;
  MPI_Initialized(&iMPIini_);
  nMPIsize_ = 1;
  nMPIrank_ = 0;
  if(iMPIini_ > 0)
  {
    MPI_Comm_size(MPI_COMM_WORLD, &nMPIsize_);
    MPI_Comm_rank(MPI_COMM_WORLD, &nMPIrank_);
  }
  if( nMPIsize_ > 1)
  {
    // buffers to conform x, xp, y, and yp centroids
    buff_MPI_0_ = (double *) malloc(sizeof(double) * 4 * (_nBins + 1));
    buff_MPI_1_ = (double *) malloc(sizeof(double) * 4 * (_nBins + 1));
  }

  //=== MPI stuff =====end=======
}

TImp::~TImp()
{
  if(nMPIsize_ > 1)
  {
    free(buff_MPI_0_);
    free(buff_MPI_1_);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TImp::_longbin
//
// DESCRIPTION
//   Bins the macro particles and transverse displacements longitudinally
//   Calculates the dipole moments at every bin
//   _phiCount(i) is the number of macros in bin i
//
// PARAMETERS
//   mp - the bunch to operate on
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TImp::_longBin(MacroPart &mp)
{
  Integer nBinsP1 = _nBins + 1;
  Real zTot, phiFactor;
  Integer i, nBm, nBp, nMacros;
  Real cfm, cfp;

  _deltaPhiBin = Consts::twoPi / Real(_nBins);

  _phiCount   = 0.0;
  _xCentroid  = 0.0;
  _xpCentroid = 0.0;
  _yCentroid  = 0.0;
  _ypCentroid = 0.0;

  // Bin the particles longitudinally and tag each particle's bin

  mp._phiMin = mp._phiMax = 0.0;

  for(i = 1; i <= mp._nMacros; i++)
  {
    if(mp._phi(i) > mp._phiMax) mp._phiMax = mp._phi(i);
    if(mp._phi(i) < mp._phiMin) mp._phiMin = mp._phi(i);

    zTot = (mp._phi(i) + Consts::pi) / _deltaPhiBin;
    nBm = Integer(zTot);
    cfp = zTot - Real(nBm);
    cfm = 1.0 - cfp;

    nBm += 1;
    if(nBm < 1) nBm = _nBins;
    if(nBm > _nBins) nBm = 1;
    nBp = nBm + 1;

    _phiCount(nBm)   += cfm;
    _phiCount(nBp)   += cfp;
    _xCentroid(nBm)  += cfm * mp._x(i);
    _xCentroid(nBp)  += cfp * mp._x(i);
    _xpCentroid(nBm) += cfm * mp._xp(i);
    _xpCentroid(nBp) += cfp * mp._xp(i);
    _yCentroid(nBm)  += cfm * mp._y(i);
    _yCentroid(nBp)  += cfp * mp._y(i);
    _ypCentroid(nBm) += cfm * mp._yp(i);
    _ypCentroid(nBp) += cfp * mp._yp(i);

    mp._fractLPosition(i) = cfp;
    mp._LPositionIndex(i) = nBm;
  }
 
  // Periodicity: wrap 1st and last bins:

  _phiCount(1)        += _phiCount(nBinsP1);
  _xCentroid(1)       += _xCentroid(nBinsP1);
  _xpCentroid(1)      += _xpCentroid(nBinsP1);
  _yCentroid(1)       += _yCentroid(nBinsP1);
  _ypCentroid(1)      += _ypCentroid(nBinsP1);
  _phiCount(nBinsP1)   = _phiCount(1);
  _xCentroid(nBinsP1)  = _xCentroid(1);
  _xpCentroid(nBinsP1) = _xpCentroid(1);
  _yCentroid(nBinsP1)  = _yCentroid(1);
  _ypCentroid(nBinsP1) = _ypCentroid(1);

  //=== MPI stuff =====begin=====

  if(nMPIsize_ <= 1) return;
  int ind, ind1;
  for(i = 0; i < nBinsP1; i++)
  {
    ind = 4 * i;
    ind1 = i + 1;
    buff_MPI_0_[ind + 0] = _xCentroid(ind1);
    buff_MPI_0_[ind + 1] = _xpCentroid(ind1);
    buff_MPI_0_[ind + 2] = _yCentroid(ind1);
    buff_MPI_0_[ind + 3] = _ypCentroid(ind1);
  }

  MPI_Allreduce(buff_MPI_0_, buff_MPI_1_, 4 * nBinsP1,
                MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  for(i = 0; i < nBinsP1; i++)
  {
    ind  = 4 * i;
    ind1 = i + 1;
    _xCentroid(ind1)  = buff_MPI_1_[ind + 0];
    _xpCentroid(ind1) = buff_MPI_1_[ind + 1];
    _yCentroid(ind1)  = buff_MPI_1_[ind + 2];
    _ypCentroid(ind1) = buff_MPI_1_[ind + 3];
  }

  //=== MPI stuff =====end=======
}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FFTTImpedance
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(FFTTImpedance, TImp);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FFTTImpedance::FFTTImpedance, ~TImpedance
//
// DESCRIPTION
//   Constructor and destructor for FFTTImpedance class
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

FFTTImpedance::FFTTImpedance(const String &n, const Integer &order,
                             Integer &nBins,
                             Vector(Complex) &Zxplus,
                             Vector(Complex) &Zxminus,
                             Vector(Complex) &Zyplus,
                             Vector(Complex) &Zyminus,
                             Real &b_a, Integer &useAvg,
                             Integer &nMacrosMin):
                             TImp(n, order, nBins, nMacrosMin),
                             _zXImped_nplus(Zxplus),
                             _zXImped_nminus(Zxminus),
                             _zYImped_nplus(Zyplus),
                             _zYImped_nminus(Zyminus) ,
                             _b_a(b_a), _useAverage(useAvg)
{
  if(TImpedance::useXdimension)
  {
    _FFTResultX1.resize(_nBins);
    _FFTResultX2.resize(_nBins);
  }

  if(TImpedance::useYdimension)
  {
    _FFTResultY1.resize(_nBins);
    _FFTResultY2.resize(_nBins);
  }

  if(_useAverage) _deltaXpGrid.resize(_nBins + 1);
  if(_useAverage) _deltaYpGrid.resize(_nBins + 1);

  _in  = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _out = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _plan = fftw_create_plan(_nBins, FFTW_FORWARD, FFTW_ESTIMATE);
}

FFTTImpedance::~FFTTImpedance()
{
  fftw_destroy_plan(_plan);
  delete [] _in;
  delete [] _out;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FFTTImpedance::_nodeCalculator
//
// DESCRIPTION
//   Bins the macro particles, and calculates the line density
//   Does an FFT on the binned distribution
//   Sets up the particle kick calculations
//
// PARAMETERS
//   mp - the macroparticle bunch
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FFTTImpedance::_nodeCalculator(MacroPart &mp)
{
  _forceCalculated = 0;

  Integer i;
  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart &
                                           Particles::All_Mask) - 1));

  // coefficient to transform charge harmonics to a transverse kick
  // Transverse impedance is not yet included

  _charge2TKick = Injection::nReals_Macro * 1.e-9 * sp->_charge *
                  sp->_charge * Consts::eCharge * Consts::vLight /
                  (sp->_betaSync * Ring::lRing *
                   Real(sp->_eTotal));

  mp._globalNMacros = mp._nMacros;

  //=== MPI stuff =====begin=====

  if(nMPIsize_ > 1)
  {
    int nMacros, nMacrosGlobal;
    nMacros = mp._nMacros;
    MPI_Allreduce(&nMacros, &nMacrosGlobal, 1, MPI_INT,
                  MPI_SUM, MPI_COMM_WORLD);
    mp._globalNMacros = nMacrosGlobal;
  }

  //=== MPI stuff =====end=======

  if(mp._globalNMacros < _nMacrosMin ) return;

  _longBin(mp);  // Bin the particles

  // FFT of beam

  Real deltaX, deltaY, Phasei;

  // Get local betatron frequencies at the impedance point

  TImpedance::nturnCount++;

  deltaX = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuX;
  deltaY = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuY;

  // FFT harmonics for X

  Real dPhaseX, dummy;
  if(TImpedance::useXdimension)
  {
    // calculate the coefficient of cos(omega_betatron * t)

    dPhaseX = Consts::twoPi * Ring::nuX / Real(_nBins);
    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseX + deltaX;
      dummy = _xCentroid(i) * cos(Phasei) -
              (Ring::betaX  * _xpCentroid(i) +
               Ring::alphaX * _xCentroid(i)) * sin(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for(i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultX1(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultX1(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }

    // calculate the coefficient of sin(omega_betatron * t)

    for (i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseX + deltaX;
      dummy = _xCentroid(i) * sin(Phasei) +
              (Ring::betaX  * _xpCentroid(i) +
               Ring::alphaX * _xCentroid(i)) * cos(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for (i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultX2(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultX2(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }
  }
  // end X dimension

  // FFT harmonics for Y

  Real dPhaseY;
  if(TImpedance::useYdimension)
  {
    // calculate the coefficient of cos(omega_betatron * t)

    dPhaseY = Consts::twoPi * Ring::nuY / Real(_nBins);
    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseY + deltaY;
      dummy = _yCentroid(i) * cos(Phasei) -
              (Ring::betaY  * _ypCentroid(i) +
               Ring::alphaY * _yCentroid(i)) *
               sin(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for (i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultY1(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultY1(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }

    // calculate the coefficient of sin(omega_betatron * t)

    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseY + deltaY;
      dummy = _yCentroid(i) * sin(Phasei) +
              (Ring::betaY  * _ypCentroid(i) +
               Ring::alphaY * _yCentroid(i)) *
               cos(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for(i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultY2(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultY2(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }
  }
  // end Y dimension

  // If bin interpolated kick option is used,
  // calculate kick at each grid point:

  if(_useAverage)
  {
    Real deltaPhi, angle;
    deltaPhi = Consts::twoPi / Real(_nBins);

    for(Integer j = 1; j <= _nBins; j++)
    {
      angle = (j - 1) * deltaPhi - Consts::pi;

      // deltaPhi added to incorporate boundary points

      if((angle + deltaPhi) < mp._phiMin ||
         (angle - deltaPhi) > mp._phiMax)
      {
        if(TImpedance::useXdimension) _deltaXpGrid(j) = 0.;
        if(TImpedance::useYdimension) _deltaYpGrid(j) = 0.;

        // Avoid aliasing outside range of particles
      }
      else
      {
        if(TImpedance::useXdimension)
        {
          _deltaXpGrid(j) = _kickx(angle);
        }

        if(TImpedance::useYdimension)
        {
          _deltaYpGrid(j) = _kicky(angle);
        }
      }
    }
    _deltaXpGrid(_nBins + 1) = _deltaXpGrid(1);
    _deltaYpGrid(_nBins + 1) = _deltaYpGrid(1);
  }

  _forceCalculated = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FFTTImpedance::_updatePartAtNode
//
// DESCRIPTION
//   Calculates the transverse kick on a MacroParticle with an FFTTImpedance
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FFTTImpedance::_updatePartAtNode(MacroPart &mp)
{
  Real Kick, angle;
  Integer j;

  mp._globalNMacros = mp._nMacros;

  //=== MPI stuff =====begin=====

  int i_MPI_flag;
  MPI_Initialized(&i_MPI_flag);
  if(i_MPI_flag > 0)
  {
    MPI_Allreduce(&mp._nMacros, &mp._globalNMacros, 1,
                  MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }

  //=== MPI stuff =====end=======

  if(mp._globalNMacros < _nMacrosMin) return;
  if(!_forceCalculated) return;

  // Average kick interpolated from bin location:

  if(_useAverage)
  {
    if(TImpedance::useXdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        Kick = _deltaXpGrid(mp._LPositionIndex(j)) *
               (1.0 - mp._fractLPosition(j)) +
               _deltaXpGrid(mp._LPositionIndex(j) + 1) *
               mp._fractLPosition(j);
        mp._xp(j) += Kick;
      }
    }

    if(TImpedance::useYdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        Kick = _deltaYpGrid(mp._LPositionIndex(j)) *
               (1.0 - mp._fractLPosition(j)) +
               _deltaYpGrid(mp._LPositionIndex(j) + 1) *
               mp._fractLPosition(j);
        mp._yp(j) += Kick;
      }
    }
  }

  else

  // Calculate kick explicitly for each particle

  {
    if(TImpedance::useXdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        angle = mp._phi(j);
        mp._xp(j) += _kickx(angle);
      }
    }
    if(TImpedance::useYdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        angle= mp._phi(j);
        mp._yp(j) += _kicky(angle);
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FFTTImpedance::_kickx/y(angle)
//
// DESCRIPTION
//   Returns the transverse kick to a macroparticle
//
// PARAMETERS
//   angle - the phase angle of the particle (rad)
//
// RETURNS
//   Transverse kick
//
///////////////////////////////////////////////////////////////////////////

Real FFTTImpedance::_kickx(Real &angle)
{
  Real dummy = 0.;
  Real dPhaseXp, dPhaseXm, deltaX;

  deltaX = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuX;
  // deltaX is betatron phase advance from all previous turns

  for(Integer n = 1; n <= _nBins / 2; n++)
  {
    dPhaseXp = ( Ring::nuX + n - 1) * (angle + Consts::pi);
    dPhaseXm = (-Ring::nuX + n - 1) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultX1(n) -
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nplus(n) *
                   (cos(dPhaseXp + deltaX) +
                    Complex(0., 1.) * sin(dPhaseXp + deltaX)) +
                   (_FFTResultX1(n) +
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nminus(n) *
                   (cos(dPhaseXm - deltaX) +
                    Complex(0., 1.) * sin(dPhaseXm - deltaX)));
  }

  // now assign negative mode numbers to the upper half modes

  for(Integer n = _nBins / 2; n <= _nBins; n++)
  {
    dPhaseXp = ( Ring::nuX + n - 1 - _nBins) * (angle + Consts::pi);
    dPhaseXm = (-Ring::nuX + n - 1 - _nBins) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultX1(n) -
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nplus(n) *
                   (cos(dPhaseXp + deltaX) +
                    Complex(0., 1.) * sin(dPhaseXp + deltaX)) +
                   (_FFTResultX1(n) +
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nminus(n) *
                   (cos(dPhaseXm - deltaX) +
                    Complex(0., 1.) * sin(dPhaseXm - deltaX)));
  }

  return (-_charge2TKick * dummy / 2.);
  // the - sign above gives instability for positive real Zminus
}

///////////////////////////////////////////////////////////////////////////

Real FFTTImpedance::_kicky(Real &angle)
{
  Real dummy = 0.;
  Real dPhaseYp, dPhaseYm, deltaY;

  deltaY = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuY;
  // deltaY is betatron phase advance from all previous turns

  for(Integer n = 1; n <= _nBins / 2; n++)
  {
    dPhaseYp = ( Ring::nuY + n - 1) * (angle + Consts::pi);
    dPhaseYm = (-Ring::nuY + n - 1) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultY1(n) -
                    Complex(0., 1.) * _FFTResultY2(n)) *
                   _zYImped_nplus(n) *
                   (cos(dPhaseYp + deltaY) +
                    Complex(0., 1.) * sin(dPhaseYp + deltaY)) +
                   (_FFTResultY1(n) +
                    Complex(0., 1.) * _FFTResultY2(n)) *
                    _zYImped_nminus(n) *
                    (cos(dPhaseYm - deltaY) +
                     Complex(0., 1.) * sin(dPhaseYm - deltaY)));
  }
 
  // now assign negative mode numbers to the upper half modes

  for(Integer n = _nBins / 2; n <= _nBins; n++)
  {
    dPhaseYp = ( Ring::nuY + n - 1 -_nBins) * (angle + Consts::pi);
    dPhaseYm = (-Ring::nuY + n - 1 -_nBins) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultY1(n) -
                    Complex(0., 1.) * _FFTResultY2(n)) *
                   _zYImped_nplus(n) *
                   (cos(dPhaseYp + deltaY) +
                    Complex(0., 1.) * sin(dPhaseYp + deltaY)) +
                   (_FFTResultY1(n) +
                    Complex(0., 1.) * _FFTResultY2(n))  *
                    _zYImped_nminus(n) *
                    (cos(dPhaseYm - deltaY) +
                     Complex(0., 1.) * sin(dPhaseYm - deltaY)));
  }

  return (-_charge2TKick * dummy / 2.);
  // the - sign above gives instability for positive real Zminus
}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FreqDepTImp
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(FreqDepTImp, TImp);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqDepTImp::FreqDepTImp, ~FreqDepTImp
//
// DESCRIPTION
//   Constructor and destructor for FreqDepTImp class
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

FreqDepTImp::FreqDepTImp(const String &n, const Integer &order,
                         Integer &nBins,
                         Integer &nTable, Vector(Real) &fTable,
                         Vector(Complex) &zxTable,
                         Vector(Complex) &zyTable,
                         Real &b_a, Integer &useAvg,
                         Integer &nMacrosMin):
                         TImp(n, order, nBins, nMacrosMin),
                         _nTable(nTable), _fTable(fTable),
                         _zxTable(zxTable), _zyTable(zyTable),
                         _b_a(b_a), _useAverage(useAvg)
{
  _zXImped_nplus.resize(_nBins);
  _zXImped_nminus.resize(_nBins);
  _zYImped_nplus.resize(_nBins);
  _zYImped_nminus.resize(_nBins);

  if(TImpedance::useXdimension)
  {
    _FFTResultX1.resize(_nBins);
    _FFTResultX2.resize(_nBins);
  }
  if(TImpedance::useYdimension)
  {
    _FFTResultY1.resize(_nBins);
    _FFTResultY2.resize(_nBins);
  }

  if(_useAverage) _deltaXpGrid.resize(_nBins + 1);
  if(_useAverage) _deltaYpGrid.resize(_nBins + 1);

  _in  = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _out = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _plan = fftw_create_plan(_nBins, FFTW_FORWARD, FFTW_ESTIMATE);
}

FreqDepTImp::~FreqDepTImp()
{
  fftw_destroy_plan(_plan);
  delete [] _in;
  delete [] _out;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqDepTImp::_nodeCalculator
//
// DESCRIPTION
//   Bins the macro particles, and calculates the line density
//   Does an FFT on the binned distribution
//   Sets up the particle kick calculations
//
// PARAMETERS
//   mp - the macroparticle bunch
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FreqDepTImp::_nodeCalculator(MacroPart &mp)
{
  Real Freq0 = mp._syncPart._betaSync * Consts::vLight / Ring::lRing;
  Real Freqmx, Freqpx, signmx,  Freqmy, Freqpy, signmy, frac;

  for(Integer n = 1; n <= _nBins / 2; n++)
  {
    Freqmx = (n - 1 - Ring::nuX) * Freq0;
    signmx = 1.0;
    if(Freqmx < 0.0)
    {
      Freqmx = abs(Freqmx);
      signmx = -1.0;
    }
    Freqpx = (n - 1 + Ring::nuX) * Freq0;

    Freqmy = (n - 1 - Ring::nuY) * Freq0;
    signmy = 1.0;
    if(Freqmy < 0.0)
    {
      Freqmy = abs(Freqmy);
      signmy = -1.0;
    }
    Freqpy = (n - 1 + Ring::nuY) * Freq0;

    if(Freqmx <= _fTable(1))
    {
      _zXImped_nminus(n) = _zxTable(1);
    }
    else if(Freqmx >= _fTable(_nTable))
    {
      _zXImped_nminus(n) = _zxTable(_nTable);
    }
    else
    {
      Integer jl, ju;
      for(Integer j = 1; j < _nTable; j++)
      {
        if(_fTable(j) <= Freqmx)
        {
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freqmx - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zXImped_nminus(n) =  _zxTable(jl) +
                           (_zxTable(ju) - _zxTable(jl)) * frac;
    }
    _zXImped_nminus(n).re *= signmx;

    if(Freqpx <= _fTable(1))
    {
      _zXImped_nplus(n) = _zxTable(1);
    }
    else if(Freqpx >= _fTable(_nTable))
    {
      _zXImped_nplus(n) = _zxTable(_nTable);
    }
    else
    {
      Integer jl, ju;
      for(Integer j = 1; j < _nTable; j++)
      {
        if(_fTable(j) <= Freqpx)
        {
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freqpx - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zXImped_nplus(n) =  _zxTable(jl) +
                          (_zxTable(ju) - _zxTable(jl)) * frac;
    }

    if(Freqmy <= _fTable(1))
    {
      _zYImped_nminus(n) = _zyTable(1);
    }
    else if(Freqmy >= _fTable(_nTable))
    {
      _zYImped_nminus(n) = _zyTable(_nTable);
    }
    else
    {
      Integer jl, ju;
      for(Integer j = 1; j < _nTable; j++)
      {
        if(_fTable(j) <= Freqmy)
        {
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freqmy - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zYImped_nminus(n) =  _zyTable(jl) +
                           (_zyTable(ju) - _zyTable(jl)) * frac;
    }
    _zYImped_nminus(n).re *= signmy;

    if(Freqpy <= _fTable(1))
    {
      _zYImped_nplus(n) = _zyTable(1);
    }
    else if(Freqpy >= _fTable(_nTable))
    {
      _zYImped_nplus(n) = _zyTable(_nTable);
    }
    else
    {
      Integer jl, ju;
      for(Integer j = 1; j < _nTable; j++)
      {
        if(_fTable(j) <= Freqpy)
        {
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freqpy - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zYImped_nplus(n) =  _zyTable(jl) +
                          (_zyTable(ju) - _zyTable(jl)) * frac;
    }
  }

  for(Integer j = 1; j <= _nBins / 2 - 1; j++)
  {
    _zXImped_nplus(_nBins - j  + 1) = -_zXImped_nminus(j + 1).conj();
    _zXImped_nminus(_nBins - j + 1) = -_zXImped_nplus(j  + 1).conj();
    _zYImped_nplus(_nBins - j  + 1) = -_zYImped_nminus(j + 1).conj();
    _zYImped_nminus(_nBins - j + 1) = -_zYImped_nplus(j  + 1).conj();
  }

  _zXImped_nplus(_nBins / 2)      = _zXImped_nminus(_nBins / 2);
  _zXImped_nplus(_nBins / 2 + 1)  = 0.;  // odd point
  _zXImped_nminus(_nBins / 2)     = _zXImped_nminus(_nBins / 2);
  _zXImped_nminus(_nBins / 2 + 1) = 0.; // odd point

  _zYImped_nplus(_nBins / 2)      = _zYImped_nminus(_nBins / 2);
  _zYImped_nplus(_nBins / 2 + 1)  = 0.;  // odd point
  _zYImped_nminus(_nBins / 2)     = _zYImped_nminus(_nBins / 2);
  _zYImped_nminus(_nBins / 2 + 1) = 0.; // odd point

  _forceCalculated = 0;

  Integer i;
  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart &
                                           Particles::All_Mask) - 1));

  // coefficient to transform charge harmonics to a transverse kick
  // Transverse impedance is not yet included

  _charge2TKick = Injection::nReals_Macro * 1.e-9 * sp->_charge *
                  sp->_charge * Consts::eCharge * Consts::vLight /
                  (sp->_betaSync * Ring::lRing *
                   Real(sp->_eTotal));

  mp._globalNMacros = mp._nMacros;

  //=== MPI stuff =====begin=====

  if(nMPIsize_ > 1)
  {
    int nMacros, nMacrosGlobal;
    nMacros = mp._nMacros;
    MPI_Allreduce(&nMacros, &nMacrosGlobal, 1, MPI_INT,
                  MPI_SUM, MPI_COMM_WORLD);
    mp._globalNMacros = nMacrosGlobal;
  }

  //=== MPI stuff =====end=======

  if(mp._globalNMacros < _nMacrosMin ) return;

  _longBin(mp);  // Bin the particles

  // FFT of beam

  Real deltaX, deltaY, Phasei;

  // Get local betatron frequencies at the impedance point

  TImpedance::nturnCount++;

  deltaX = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuX;
  deltaY = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuY;

  // FFT harmonics for X

  Real dPhaseX, dummy;
  if(TImpedance::useXdimension)
  {
    // calculate the coefficient of cos(omega_betatron * t)

    dPhaseX = Consts::twoPi * Ring::nuX / Real(_nBins);
    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseX + deltaX;
      dummy = _xCentroid(i) * cos(Phasei) -
              (Ring::betaX  * _xpCentroid(i) +
               Ring::alphaX * _xCentroid(i)) * sin(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for(i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultX1(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultX1(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }

    // calculate the coefficient of sin(omega_betatron * t)

    for (i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseX + deltaX;
      dummy = _xCentroid(i) * sin(Phasei) +
              (Ring::betaX  * _xpCentroid(i) +
               Ring::alphaX * _xCentroid(i)) * cos(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for (i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultX2(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultX2(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }
  }
  // end X dimension

  // FFT harmonics for Y

  Real dPhaseY;
  if(TImpedance::useYdimension)
  {
    // calculate the coefficient of cos(omega_betatron * t)

    dPhaseY = Consts::twoPi * Ring::nuY / Real(_nBins);
    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseY + deltaY;
      dummy = _yCentroid(i) * cos(Phasei) -
              (Ring::betaY  * _ypCentroid(i) +
               Ring::alphaY * _yCentroid(i)) *
               sin(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for (i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultY1(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultY1(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }

    // calculate the coefficient of sin(omega_betatron * t)

    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseY + deltaY;
      dummy = _yCentroid(i) * sin(Phasei) +
              (Ring::betaY  * _ypCentroid(i) +
               Ring::alphaY * _yCentroid(i)) *
               cos(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for(i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultY2(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultY2(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }
  }
  // end Y dimension

  // If bin interpolated kick option is used,
  // calculate kick at each grid point:

  if(_useAverage)
  {
    Real deltaPhi, angle;
    deltaPhi = Consts::twoPi / Real(_nBins);

    for(Integer j = 1; j <= _nBins; j++)
    {
      angle = (j - 1) * deltaPhi - Consts::pi;

      // deltaPhi added to incorporate boundary points

      if((angle + deltaPhi) < mp._phiMin ||
         (angle - deltaPhi) > mp._phiMax)
      {
        if(TImpedance::useXdimension) _deltaXpGrid(j) = 0.;
        if(TImpedance::useYdimension) _deltaYpGrid(j) = 0.;

        // Avoid aliasing outside range of particles
      }
      else
      {
        if(TImpedance::useXdimension)
        {
          _deltaXpGrid(j) = _kickx(angle);
        }

        if(TImpedance::useYdimension)
        {
          _deltaYpGrid(j) = _kicky(angle);
        }
      }
    }
    _deltaXpGrid(_nBins + 1) = _deltaXpGrid(1);
    _deltaYpGrid(_nBins + 1) = _deltaYpGrid(1);
  }

  _forceCalculated = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqDepTImp::_updatePartAtNode
//
// DESCRIPTION
//   Calculates the transverse kick on a MacroParticle with a FreqDepTImp
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FreqDepTImp::_updatePartAtNode(MacroPart &mp)
{
  Real Kick, angle;
  Integer j;

  mp._globalNMacros = mp._nMacros;

  //=== MPI stuff =====begin=====

  int i_MPI_flag;
  MPI_Initialized(&i_MPI_flag);
  if(i_MPI_flag > 0)
  {
    MPI_Allreduce(&mp._nMacros, &mp._globalNMacros, 1,
                  MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }

  //=== MPI stuff =====end=======

  if(mp._globalNMacros < _nMacrosMin) return;
  if(!_forceCalculated) return;

  // Average kick interpolated from bin location:

  if(_useAverage)
  {
    if(TImpedance::useXdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        Kick = _deltaXpGrid(mp._LPositionIndex(j)) *
               (1.0 - mp._fractLPosition(j)) +
               _deltaXpGrid(mp._LPositionIndex(j) + 1) *
               mp._fractLPosition(j);
        mp._xp(j) += Kick;
      }
    }

    if(TImpedance::useYdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        Kick = _deltaYpGrid(mp._LPositionIndex(j)) *
               (1.0 - mp._fractLPosition(j)) +
               _deltaYpGrid(mp._LPositionIndex(j) + 1) *
               mp._fractLPosition(j);
        mp._yp(j) += Kick;
      }
    }
  }

  else

  // Calculate kick explicitly for each particle

  {
    if(TImpedance::useXdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        angle = mp._phi(j);
        mp._xp(j) += _kickx(angle);
      }
    }
    if(TImpedance::useYdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        angle= mp._phi(j);
        mp._yp(j) += _kicky(angle);
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqDepTImp::_kickx/y(angle)
//
// DESCRIPTION
//   Returns the transverse kick to a macroparticle
//
// PARAMETERS
//   angle - the phase angle of the particle (rad)
//
// RETURNS
//   Transverse kick
//
///////////////////////////////////////////////////////////////////////////

Real FreqDepTImp::_kickx(Real &angle)
{
  Real dummy = 0.;
  Real dPhaseXp, dPhaseXm, deltaX;

  deltaX = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuX;
  // deltaX is betatron phase advance from all previous turns

  for(Integer n = 1; n <= _nBins / 2; n++)
  {
    dPhaseXp = ( Ring::nuX + n - 1) * (angle + Consts::pi);
    dPhaseXm = (-Ring::nuX + n - 1) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultX1(n) -
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nplus(n) *
                   (cos(dPhaseXp + deltaX) +
                    Complex(0., 1.) * sin(dPhaseXp + deltaX)) +
                   (_FFTResultX1(n) +
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nminus(n) *
                   (cos(dPhaseXm - deltaX) +
                    Complex(0., 1.) * sin(dPhaseXm - deltaX)));
  }

  // now assign negative mode numbers to the upper half modes

  for(Integer n = _nBins / 2; n <= _nBins; n++)
  {
    dPhaseXp = ( Ring::nuX + n - 1 - _nBins) * (angle + Consts::pi);
    dPhaseXm = (-Ring::nuX + n - 1 - _nBins) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultX1(n) -
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nplus(n) *
                   (cos(dPhaseXp + deltaX) +
                    Complex(0., 1.) * sin(dPhaseXp + deltaX)) +
                   (_FFTResultX1(n) +
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nminus(n) *
                   (cos(dPhaseXm - deltaX) +
                    Complex(0., 1.) * sin(dPhaseXm - deltaX)));
  }

  return (-_charge2TKick * dummy / 2.);
  // the - sign above gives instability for positive real Zminus
}

///////////////////////////////////////////////////////////////////////////

Real FreqDepTImp::_kicky(Real &angle)
{
  Real dummy = 0.;
  Real dPhaseYp, dPhaseYm, deltaY;

  deltaY = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuY;
  // deltaY is betatron phase advance from all previous turns

  for(Integer n = 1; n <= _nBins / 2; n++)
  {
    dPhaseYp = ( Ring::nuY + n - 1) * (angle + Consts::pi);
    dPhaseYm = (-Ring::nuY + n - 1) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultY1(n) -
                    Complex(0., 1.) * _FFTResultY2(n)) *
                   _zYImped_nplus(n) *
                   (cos(dPhaseYp + deltaY) +
                    Complex(0., 1.) * sin(dPhaseYp + deltaY)) +
                   (_FFTResultY1(n) +
                    Complex(0., 1.) * _FFTResultY2(n)) *
                    _zYImped_nminus(n) *
                    (cos(dPhaseYm - deltaY) +
                     Complex(0., 1.) * sin(dPhaseYm - deltaY)));
  }
 
  // now assign negative mode numbers to the upper half modes

  for(Integer n = _nBins / 2; n <= _nBins; n++)
  {
    dPhaseYp = ( Ring::nuY + n - 1 -_nBins) * (angle + Consts::pi);
    dPhaseYm = (-Ring::nuY + n - 1 -_nBins) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultY1(n) -
                    Complex(0., 1.) * _FFTResultY2(n)) *
                   _zYImped_nplus(n) *
                   (cos(dPhaseYp + deltaY) +
                    Complex(0., 1.) * sin(dPhaseYp + deltaY)) +
                   (_FFTResultY1(n) +
                    Complex(0., 1.) * _FFTResultY2(n))  *
                    _zYImped_nminus(n) *
                    (cos(dPhaseYm - deltaY) +
                     Complex(0., 1.) * sin(dPhaseYm - deltaY)));
  }

  return (-_charge2TKick * dummy / 2.);
  // the - sign above gives instability for positive real Zminus
}


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FreqBetDepTImp
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(FreqBetDepTImp, TImp);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqBetDepTImp::FreqBetDepTImp, ~FreqBetDepTImp
//
// DESCRIPTION
//   Constructor and destructor for FreqBetDepTImp class
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

FreqBetDepTImp::FreqBetDepTImp(const String &n, const Integer &order,
                               Integer &nBins,
                               Integer &nbTable, Vector(Real) &bTable,
                               Integer &nfTable, Vector(Real) &fTable,
                               Matrix(Complex) &zxTable,
                               Matrix(Complex) &zyTable,
                               Real &b_a, Integer &useAvg,
                               Integer &nMacrosMin):
                               TImp(n, order, nBins, nMacrosMin),
                               _nbTable(nbTable), _bTable(bTable),
                               _nfTable(nfTable), _fTable(fTable),
                               _zxTable(zxTable), _zyTable(zyTable),
                               _b_a(b_a), _useAverage(useAvg)
{
  _zxTable.resize(_nbTable, _nfTable);
  _zyTable.resize(_nbTable, _nfTable);

  _zXImped_nplus.resize(_nBins);
  _zXImped_nminus.resize(_nBins);
  _zYImped_nplus.resize(_nBins);
  _zYImped_nminus.resize(_nBins);

  if(TImpedance::useXdimension)
  {
    _FFTResultX1.resize(_nBins);
    _FFTResultX2.resize(_nBins);
  }
  if(TImpedance::useYdimension)
  {
    _FFTResultY1.resize(_nBins);
    _FFTResultY2.resize(_nBins);
  }

  if(_useAverage) _deltaXpGrid.resize(_nBins + 1);
  if(_useAverage) _deltaYpGrid.resize(_nBins + 1);

  _in  = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _out = (FFTW_COMPLEX *) new char[_nBins * sizeof(FFTW_COMPLEX)];
  _plan = fftw_create_plan(_nBins, FFTW_FORWARD, FFTW_ESTIMATE);
}

FreqBetDepTImp::~FreqBetDepTImp()
{
  fftw_destroy_plan(_plan);
  delete [] _in;
  delete [] _out;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqBetDepTImp::_nodeCalculator
//
// DESCRIPTION
//   Bins the macro particles, and calculates the line density
//   Does an FFT on the binned distribution
//   Sets up the particle kick calculations
//
// PARAMETERS
//   mp - the macroparticle bunch
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FreqBetDepTImp::_nodeCalculator(MacroPart &mp)
{
  Real Freq0 = mp._syncPart._betaSync * Consts::vLight / Ring::lRing;
  Real Freqmx, Freqpx, signmx,  Freqmy, Freqpy, signmy, frac;

  Integer kBeta = 1;
  for(Integer k = 1; k <= _nbTable; k++)
  {
    if(mp._syncPart._betaSync >= _bTable(k)) kBeta = k;
  }
  if(kBeta == _nbTable) kBeta = _nbTable - 1;
  frac = (mp._syncPart._betaSync - _bTable(kBeta)) /
         (_bTable(kBeta + 1) - _bTable(kBeta));
  if(frac > 1.0) frac = 1.0;
  if(frac < 0.0) frac = 0.0;

  Vector(Complex) ZXT;
  Vector(Complex) ZYT;

  ZXT.resize(_nfTable);
  ZYT.resize(_nfTable);

  for(Integer k = 1; k <= _nfTable; k++)
  {
    ZXT(k) =  _zxTable(kBeta, k) +
             (_zxTable(kBeta + 1, k) - _zxTable(kBeta, k)) * frac;
    ZYT(k) =  _zyTable(kBeta, k) +
             (_zyTable(kBeta + 1, k) - _zyTable(kBeta, k)) * frac;
  }

  for(Integer n = 1; n <= _nBins / 2; n++)
  {
    Freqmx = (n - 1 - Ring::nuX) * Freq0;
    signmx = 1.0;
    if(Freqmx < 0.0)
    {
      Freqmx = abs(Freqmx);
      signmx = -1.0;
    }
    Freqpx = (n - 1 + Ring::nuX) * Freq0;

    Freqmy = (n - 1 - Ring::nuY) * Freq0;
    signmy = 1.0;
    if(Freqmy < 0.0)
    {
      Freqmy = abs(Freqmy);
      signmy = -1.0;
    }
    Freqpy = (n - 1 + Ring::nuY) * Freq0;

    if(Freqmx <= _fTable(1))
    {
      _zXImped_nminus(n) = ZXT(1);
    }
    else if(Freqmx >= _fTable(_nfTable))
    {
      _zXImped_nminus(n) = ZXT(_nfTable);
    }
    else
    {
      Integer jl, ju;
      for(Integer j = 1; j < _nfTable; j++)
      {
        if(_fTable(j) <= Freqmx)
	{
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freqmx - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zXImped_nminus(n) =  ZXT(jl) +
                           (ZXT(ju) - ZXT(jl)) * frac;
    }
    _zXImped_nminus(n).re *= signmx;

    if(Freqpx <= _fTable(1))
    {
      _zXImped_nplus(n) = ZXT(1);
    }
    else if(Freqpx >= _fTable(_nfTable))
    {
      _zXImped_nplus(n) = ZXT(_nfTable);
    }
    else
    {
      Integer jl, ju;
      for(Integer j = 1; j < _nfTable; j++)
      {
        if(_fTable(j) <= Freqpx)
	{
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freqpx - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zXImped_nplus(n) =  ZXT(jl) +
                          (ZXT(ju) - ZXT(jl)) * frac;
    }

    if(Freqmy <= _fTable(1))
    {
      _zYImped_nminus(n) = ZYT(1);
    }
    else if(Freqmy >= _fTable(_nfTable))
    {
      _zYImped_nminus(n) = ZYT(_nfTable);
    }
    else
    {
      Integer jl, ju;
      for(Integer j = 1; j < _nfTable; j++)
      {
        if(_fTable(j) <= Freqmy)
	{
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freqmy - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zYImped_nminus(n) =  ZYT(jl) +
                           (ZYT(ju) - ZYT(jl)) * frac;
    }
    _zYImped_nminus(n).re *= signmy;

    if(Freqpy <= _fTable(1))
    {
      _zYImped_nplus(n) = ZYT(1);
    }
    else if(Freqpy >= _fTable(_nfTable))
    {
      _zYImped_nplus(n) = ZYT(_nfTable);
    }
    else
    {
      Integer jl, ju;
      for(Integer j = 1; j < _nfTable; j++)
      {
        if(_fTable(j) <= Freqpy)
	{
          jl = j;
          ju = j + 1;
        }
      }
      frac = (Freqpy - _fTable(jl)) / (_fTable(ju) - _fTable(jl));
      _zYImped_nplus(n) =  ZYT(jl) +
                          (ZYT(ju) - ZYT(jl)) * frac;
    }
  }

  for(Integer j = 1; j <= _nBins / 2 - 1; j++)
  {
    _zXImped_nplus(_nBins - j  + 1) = -_zXImped_nminus(j + 1).conj();
    _zXImped_nminus(_nBins - j + 1) = -_zXImped_nplus(j  + 1).conj();
    _zYImped_nplus(_nBins - j  + 1) = -_zYImped_nminus(j + 1).conj();
    _zYImped_nminus(_nBins - j + 1) = -_zYImped_nplus(j  + 1).conj();
  }

  _zXImped_nplus(_nBins / 2)      = _zXImped_nminus(_nBins / 2);
  _zXImped_nplus(_nBins / 2 + 1)  = 0.;  // odd point
  _zXImped_nminus(_nBins / 2)     = _zXImped_nminus(_nBins / 2);
  _zXImped_nminus(_nBins / 2 + 1) = 0.; // odd point

  _zYImped_nplus(_nBins / 2)      = _zYImped_nminus(_nBins / 2);
  _zYImped_nplus(_nBins / 2 + 1)  = 0.;  // odd point
  _zYImped_nminus(_nBins / 2)     = _zYImped_nminus(_nBins / 2);
  _zYImped_nminus(_nBins / 2 + 1) = 0.; // odd point

  _forceCalculated = 0;

  Integer i;
  SyncPart *sp = SyncPart::safeCast(syncP((Particles::syncPart &
                                           Particles::All_Mask) - 1));

  // coefficient to transform charge harmonics to a transverse kick
  // Transverse impedance is not yet included

  _charge2TKick = Injection::nReals_Macro * 1.e-9 * sp->_charge *
                  sp->_charge * Consts::eCharge * Consts::vLight /
                  (sp->_betaSync * Ring::lRing *
                   Real(sp->_eTotal));

  mp._globalNMacros = mp._nMacros;

  //=== MPI stuff =====begin=====

  if(nMPIsize_ > 1)
  {
    int nMacros, nMacrosGlobal;
    nMacros = mp._nMacros;
    MPI_Allreduce(&nMacros, &nMacrosGlobal, 1, MPI_INT,
                  MPI_SUM, MPI_COMM_WORLD);
    mp._globalNMacros = nMacrosGlobal;
  }

  //=== MPI stuff =====end=======

  if(mp._globalNMacros < _nMacrosMin ) return;

  _longBin(mp);  // Bin the particles

  // FFT of beam

  Real deltaX, deltaY, Phasei;

  // Get local betatron frequencies at the impedance point

  TImpedance::nturnCount++;

  deltaX = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuX;
  deltaY = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuY;

  // FFT harmonics for X

  Real dPhaseX, dummy;
  if(TImpedance::useXdimension)
  {
    // calculate the coefficient of cos(omega_betatron * t)

    dPhaseX = Consts::twoPi * Ring::nuX / Real(_nBins);
    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseX + deltaX;
      dummy = _xCentroid(i) * cos(Phasei) -
              (Ring::betaX  * _xpCentroid(i) +
               Ring::alphaX * _xCentroid(i)) * sin(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for(i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultX1(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultX1(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }

    // calculate the coefficient of sin(omega_betatron * t)

    for (i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseX + deltaX;
      dummy = _xCentroid(i) * sin(Phasei) +
              (Ring::betaX  * _xpCentroid(i) +
               Ring::alphaX * _xCentroid(i)) * cos(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for (i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultX2(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultX2(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }
  }
  // end X dimension

  // FFT harmonics for Y

  Real dPhaseY;
  if(TImpedance::useYdimension)
  {
    // calculate the coefficient of cos(omega_betatron * t)

    dPhaseY = Consts::twoPi * Ring::nuY / Real(_nBins);
    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseY + deltaY;
      dummy = _yCentroid(i) * cos(Phasei) -
              (Ring::betaY  * _ypCentroid(i) +
               Ring::alphaY * _yCentroid(i)) *
               sin(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for (i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultY1(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultY1(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }

    // calculate the coefficient of sin(omega_betatron * t)

    for(i = 1; i <= _nBins; i++)
    {
      Phasei = (i - 1) * dPhaseY + deltaY;
      dummy = _yCentroid(i) * sin(Phasei) +
              (Ring::betaY  * _ypCentroid(i) +
               Ring::alphaY * _yCentroid(i)) *
               cos(Phasei);

      c_re(_in[i - 1]) = dummy * Real(_nBins);
      c_im(_in[i - 1]) = 0.;
    }

    // Do the Forward FFT:

    fftw(_plan, 1, _in, 1, 0, _out, 1, 0);
    for(i = 1; i <= _nBins; i++)
    {
      c_re(_FFTResultY2(i)) = c_re(_out[i - 1]) / Real(_nBins);
      c_im(_FFTResultY2(i)) = c_im(_out[i - 1]) / Real(_nBins);
    }
  }
  // end Y dimension

  // If bin interpolated kick option is used,
  // calculate kick at each grid point:

  if(_useAverage)
  {
    Real deltaPhi, angle;
    deltaPhi = Consts::twoPi / Real(_nBins);

    for(Integer j = 1; j <= _nBins; j++)
    {
      angle = (j - 1) * deltaPhi - Consts::pi;

      // deltaPhi added to incorporate boundary points

      if((angle + deltaPhi) < mp._phiMin ||
         (angle - deltaPhi) > mp._phiMax)
      {
        if(TImpedance::useXdimension) _deltaXpGrid(j) = 0.;
        if(TImpedance::useYdimension) _deltaYpGrid(j) = 0.;

        // Avoid aliasing outside range of particles
      }
      else
      {
        if(TImpedance::useXdimension)
        {
          _deltaXpGrid(j) = _kickx(angle);
        }

        if(TImpedance::useYdimension)
        {
          _deltaYpGrid(j) = _kicky(angle);
        }
      }
    }
    _deltaXpGrid(_nBins + 1) = _deltaXpGrid(1);
    _deltaYpGrid(_nBins + 1) = _deltaYpGrid(1);
  }

  _forceCalculated = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqBetDepTImp::_updatePartAtNode
//
// DESCRIPTION
//   Calculates the transverse kick on a MacroParticle with a FreqBetDepTImp
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FreqBetDepTImp::_updatePartAtNode(MacroPart &mp)
{
  Real Kick, angle;
  Integer j;

  mp._globalNMacros = mp._nMacros;

  //=== MPI stuff =====begin=====

  int i_MPI_flag;
  MPI_Initialized(&i_MPI_flag);
  if(i_MPI_flag > 0)
  {
    MPI_Allreduce(&mp._nMacros, &mp._globalNMacros, 1,
                  MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }

  //=== MPI stuff =====end=======

  if(mp._globalNMacros < _nMacrosMin) return;
  if(!_forceCalculated) return;

  // Average kick interpolated from bin location:

  if(_useAverage)
  {
    if(TImpedance::useXdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        Kick = _deltaXpGrid(mp._LPositionIndex(j)) *
               (1.0 - mp._fractLPosition(j)) +
               _deltaXpGrid(mp._LPositionIndex(j) + 1) *
               mp._fractLPosition(j);
        mp._xp(j) += Kick;
      }
    }

    if(TImpedance::useYdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        Kick = _deltaYpGrid(mp._LPositionIndex(j)) *
               (1.0 - mp._fractLPosition(j)) +
               _deltaYpGrid(mp._LPositionIndex(j) + 1) *
               mp._fractLPosition(j);
        mp._yp(j) += Kick;
      }
    }
  }

  else

  // Calculate kick explicitly for each particle

  {
    if(TImpedance::useXdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        angle = mp._phi(j);
        mp._xp(j) += _kickx(angle);
      }
    }
    if(TImpedance::useYdimension)
    {
      for(j = 1; j <= mp._nMacros; j++)
      {
        angle= mp._phi(j);
        mp._yp(j) += _kicky(angle);
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FreqBetDepTImp::_kickx/y(angle)
//
// DESCRIPTION
//   Returns the transverse kick to a macroparticle
//
// PARAMETERS
//   angle - the phase angle of the particle (rad)
//
// RETURNS
//   Transverse kick
//
///////////////////////////////////////////////////////////////////////////

Real FreqBetDepTImp::_kickx(Real &angle)
{
  Real dummy = 0.;
  Real dPhaseXp, dPhaseXm, deltaX;

  deltaX = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuX;
  // deltaX is betatron phase advance from all previous turns

  for(Integer n = 1; n <= _nBins / 2; n++)
  {
    dPhaseXp = ( Ring::nuX + n - 1) * (angle + Consts::pi);
    dPhaseXm = (-Ring::nuX + n - 1) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultX1(n) -
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nplus(n) *
                   (cos(dPhaseXp + deltaX) +
                    Complex(0., 1.) * sin(dPhaseXp + deltaX)) +
                   (_FFTResultX1(n) +
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nminus(n) *
                   (cos(dPhaseXm - deltaX) +
                    Complex(0., 1.) * sin(dPhaseXm - deltaX)));
  }

  // now assign negative mode numbers to the upper half modes

  for(Integer n = _nBins / 2; n <= _nBins; n++)
  {
    dPhaseXp = ( Ring::nuX + n - 1 - _nBins) * (angle + Consts::pi);
    dPhaseXm = (-Ring::nuX + n - 1 - _nBins) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultX1(n) -
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nplus(n) *
                   (cos(dPhaseXp + deltaX) +
                    Complex(0., 1.) * sin(dPhaseXp + deltaX)) +
                   (_FFTResultX1(n) +
                    Complex(0., 1.) * _FFTResultX2(n)) *
                   _zXImped_nminus(n) *
                   (cos(dPhaseXm - deltaX) +
                    Complex(0., 1.) * sin(dPhaseXm - deltaX)));
  }

  return (-_charge2TKick * dummy / 2.);
  // the - sign above gives instability for positive real Zminus
}

///////////////////////////////////////////////////////////////////////////

Real FreqBetDepTImp::_kicky(Real &angle)
{
  Real dummy = 0.;
  Real dPhaseYp, dPhaseYm, deltaY;

  deltaY = (TImpedance::nturnCount - 1) * Consts::twoPi * Ring::nuY;
  // deltaY is betatron phase advance from all previous turns

  for(Integer n = 1; n <= _nBins / 2; n++)
  {
    dPhaseYp = ( Ring::nuY + n - 1) * (angle + Consts::pi);
    dPhaseYm = (-Ring::nuY + n - 1) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultY1(n) -
                    Complex(0., 1.) * _FFTResultY2(n)) *
                   _zYImped_nplus(n) *
                   (cos(dPhaseYp + deltaY) +
                    Complex(0., 1.) * sin(dPhaseYp + deltaY)) +
                   (_FFTResultY1(n) +
                    Complex(0., 1.) * _FFTResultY2(n)) *
                    _zYImped_nminus(n) *
                    (cos(dPhaseYm - deltaY) +
                     Complex(0., 1.) * sin(dPhaseYm - deltaY)));
  }
 
  // now assign negative mode numbers to the upper half modes

  for(Integer n = _nBins / 2; n <= _nBins; n++)
  {
    dPhaseYp = ( Ring::nuY + n - 1 -_nBins) * (angle + Consts::pi);
    dPhaseYm = (-Ring::nuY + n - 1 -_nBins) * (angle + Consts::pi);
    dummy += -c_im((_FFTResultY1(n) -
                    Complex(0., 1.) * _FFTResultY2(n)) *
                   _zYImped_nplus(n) *
                   (cos(dPhaseYp + deltaY) +
                    Complex(0., 1.) * sin(dPhaseYp + deltaY)) +
                   (_FFTResultY1(n) +
                    Complex(0., 1.) * _FFTResultY2(n))  *
                    _zYImped_nminus(n) *
                    (cos(dPhaseYm - deltaY) +
                     Complex(0., 1.) * sin(dPhaseYm - deltaY)));
  }

  return (-_charge2TKick * dummy / 2.);
  // the - sign above gives instability for positive real Zminus
}


///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE TImpedance
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   TImpedance::ctor
//
// DESCRIPTION
//   Initializes the various Bump related constants
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TImpedance::ctor()
{
// set some initial values

   nTImpedance = 0;
   nLBinsTImp = 32;
   nFFTTImpedance  = 0;
   checkangle = 1.;    
   useXdimension = 0;
   useYdimension = 0;
   nturnCount = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TImpedance::addFFTTImpedance
//
// DESCRIPTION
//  Adds an FFT Transverse Impedance Node
//
// PARAMETERS
//  name:    Name for the TM 
//  order:   Order index
//  nBins    Number of bins to use
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TImpedance::addFFTTImpedance(const String &name, const Integer &order,
                                  Vector(Complex) &Zxplus ,
                                  Vector(Complex) &Zxminus,
                                  Vector(Complex) &Zyplus,
                                  Vector(Complex) &Zyminus,
                                  Real &b_a, Integer &useAvg,
                                  Integer &nMacrosMin)
{
  if(Zxplus.rows()  < nLBinsTImp / 2) except(badImpedSize);
  if(Zyplus.rows()  < nLBinsTImp / 2) except(badImpedSize);
  if(Zxminus.rows() < nLBinsTImp / 2) except(badImpedSize);
  if(Zyminus.rows() < nLBinsTImp / 2) except(badImpedSize);
  if(nNodes == nodes.size())
    nodes.resize(nNodes + 1);
  if(nTImpedance == TImpedancePointers.size())
    TImpedancePointers.resize(nTImpedance + 1);
  if(nFFTTImpedance == 1) except(tooManyFFTTImpedances);

  // reconstruct the upper halves of the impedance arrays in order to
  // satisfy the upper half of the FFT output array,
  // which stands for negative frequencies

  Zxplus.resize(nLBinsTImp);
  Zxminus.resize(nLBinsTImp);
  Zyplus.resize(nLBinsTImp);
  Zyminus.resize(nLBinsTImp);

  for(Integer _j = 1; _j <= nLBinsTImp / 2 - 1; _j++)
  {
    Zxplus(nLBinsTImp - _j + 1)  = -Zxminus(_j + 1).conj();
    Zxminus(nLBinsTImp - _j + 1) = -Zxplus(_j + 1).conj();
    Zyplus(nLBinsTImp - _j + 1)  = -Zyminus(_j + 1).conj();
    Zyminus(nLBinsTImp - _j + 1) = -Zyplus(_j + 1).conj();
  }

  Zxplus(nLBinsTImp / 2)      = Zxminus(nLBinsTImp / 2);
  Zxplus(nLBinsTImp / 2 + 1)  = 0.; // odd point
  Zxminus(nLBinsTImp / 2)     = Zxminus(nLBinsTImp / 2);
  Zxminus(nLBinsTImp / 2 + 1) = 0.; // odd point
 
  Zyplus(nLBinsTImp / 2)      = Zyminus(nLBinsTImp / 2);
  Zyplus(nLBinsTImp / 2 + 1)  = 0.; // odd point
  Zyminus(nLBinsTImp / 2)     = Zyminus(nLBinsTImp / 2);
  Zyminus(nLBinsTImp / 2 + 1) = 0.; // odd point

  // change the imaginary part of the impedance - in order to
  // comply with Chao's conventions

  for(Integer _k = 1; _k <= nLBinsTImp; _k++)
  {
    Zxplus(_k)  = Zxplus(_k).conj();
    Zxminus(_k) = Zxminus(_k).conj();
    Zyplus(_k)  = Zyplus(_k).conj();
    Zyminus(_k) = Zyminus(_k).conj();
  }
  nNodes++;
  nodes(nNodes - 1) = new FFTTImpedance(name, order, nLBinsTImp,
                                        Zxplus, Zxminus,
                                        Zyplus, Zyminus,
                                        b_a, useAvg,
                                        nMacrosMin);
  nTImpedance++;
  nFFTTImpedance++;
  TImpedancePointers(nTImpedance - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TImpedance::addFreqDepTImp
//
// DESCRIPTION
//  Adds a frequency dependent Transverse Impedance Node
//
// PARAMETERS
//  name:    Name for the TM 
//  order:   Order index
//  nBins    Number of bins to use
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TImpedance::addFreqDepTImp(const String &name, const Integer &order,
                               Integer &nTable, Vector(Real) &fTable,
                               Vector(Complex) &zxTable,
                               Vector(Complex) &zyTable,
                               Real &b_a, Integer &useAvg,
                               Integer &nMacrosMin)
{
  if(fTable.rows() < nTable) except(badImpedSize);
  if(zxTable.rows() < nTable) except(badImpedSize);
  if(zyTable.rows() < nTable) except(badImpedSize);

  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nTImpedance == TImpedancePointers.size())
    TImpedancePointers.resize(nTImpedance + 1);
  if(nFFTTImpedance == 1) except(tooManyFFTTImpedances);

  // change the imaginary part of the impedance -
  // in order to comply with Chao's convention

  for(Integer k = 1; k <= nTable; k++)
  {
    zxTable(k) = zxTable(k).conj();
    zyTable(k) = zyTable(k).conj();
  }

   nNodes++;

   nodes(nNodes - 1) = new FreqDepTImp(name, order, nLBinsTImp,
                                       nTable, fTable, zxTable, zyTable,
                                       b_a, useAvg, nMacrosMin);

   nTImpedance ++;
   nFFTTImpedance ++;
   
   TImpedancePointers(nTImpedance - 1) = nodes(nNodes - 1);
   
   nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TImpedance::addFreqBetDepTImp
//
// DESCRIPTION
//  Adds a frequency and beta dependent Transverse Impedance Node
//
// PARAMETERS
//  name:    Name for the TM 
//  order:   Order index
//  nBins    Number of bins to use
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TImpedance::addFreqBetDepTImp(const String &name, const Integer &order,
                                   Integer &nbTable, Vector(Real) &bTable,
                                   Integer &nfTable, Vector(Real) &fTable,
                                   Matrix(Complex) &zxTable,
                                   Matrix(Complex) &zyTable,
                                   Real &b_a, Integer &useAvg,
                                   Integer &nMacrosMin)
{
  if(bTable.rows() < nbTable) except(badImpedSize);
  if(fTable.rows() < nfTable) except(badImpedSize);

  zxTable.resize(nbTable, nfTable);
  zyTable.resize(nbTable, nfTable);

  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nTImpedance == TImpedancePointers.size())
    TImpedancePointers.resize(nTImpedance + 1);
  if(nFFTTImpedance == 1) except(tooManyFFTTImpedances);

  // change the imaginary part of the impedance -
  // in order to comply with Chao's convention

  for(Integer k = 1; k <= nbTable; k++)
  {
    for(Integer j = 1; j <= nfTable; j++)
    {
      zxTable(k, j) = zxTable(k, j).conj();
      zyTable(k, j) = zyTable(k, j).conj();
    }
  }

   nNodes++;

   nodes(nNodes - 1) = new FreqBetDepTImp(name, order, nLBinsTImp,
                                          nbTable, bTable, nfTable, fTable,
                                          zxTable, zyTable,
                                          b_a, useAvg, nMacrosMin);

   nTImpedance ++;
   nFFTTImpedance ++;
   
   TImpedancePointers(nTImpedance - 1) = nodes(nNodes - 1);
   
   nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TImpedance::showTImpedance
//
// DESCRIPTION
//  Show a transverse impedance info:
//
// PARAMETERS
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Void TImpedance::showTImpedance(const Integer &n, Ostream &os)
{
  Integer i;

  if (n > nTImpedance) except(badTImpedance);
  TImp *ti = TImp::safeCast(TImpedancePointers(n - 1));

  os << "\t" << "Bin\t" << "Phi\t" << "Count\n";
  for (i = 1; i <= ti->_nBins; i++)
    os << i << "\t\t"
       << (-pi + (i - 1) * twoPi / (ti->_nBins)) << "\t\t"
       << ti->_phiCount(i) << "\n";
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  TImpedance::dumpxxxx
//
// DESCRIPTION
//  These routines dump TIMpedance info to vectors accesible from
//  the Shell
//
// PARAMETERS
//  n - the n'th L-space charge node
//  herd = the herd used to calculate kick
//  ang - the longitudinal phase angle to provide the kick at (rad).
//
// RETURNS
//  Nothing
//
///////////////////////////////////////////////////////////////////////////

Real TImpedance::dumpKick( const Integer &n, Real &ang)
// Dump the space charge kick per particle
{
  MacroPart *mp;
  if (n > nTImpedance) except(badTImpedance);
  TImp *ti = TImp::safeCast(TImpedancePointers(n - 1));

  return ti->_kickx(ang);
}

Void TImpedance::dumpPhiCount(const Integer &n, Vector(Real) &p)
// Dump the binned distribution
{
  if (n > nTImpedance) except (badTImpedance);
  TImp *ti = TImp::safeCast(TImpedancePointers(n - 1));
  p.resize(ti->_nBins);

  for(Integer i = 1; i <= ti->_nBins; i++)
  {
    p(i) = ti->_phiCount(i);
  }
}

Void TImpedance::dumpFFTStuff(const Integer &n, const Integer &maxharm,
                              const Integer &nturn, Ostream &osharmonics,
                              const Integer &nparticles)
// Dump the FFT magnitude and phase vectors
{
  MacroPart *mp; 
  FFTTImpedance *fti;

  if ((nparticles & All_Mask) > nHerds) except (badHerdNo);
  mp = MacroPart::safeCast(mParts((nparticles & All_Mask) - 1));
  if (n > nTImpedance) except (badTImpedance);
  TImp *ti = TImp::safeCast(TImpedancePointers(n - 1));
  if (ti->isKindOf(FFTTImpedance::desc()) != 1 ) except(badFFTTImp);
  fti = FFTTImpedance::safeCast(TImpedancePointers(n - 1));

  osharmonics << setw(6) << nturn << " ";

  // write the average harmonic amplitudes in milimeters

  for (Integer i = 1; i <= maxharm; i++)
  {
    if(TImpedance::useXdimension)
    {
      osharmonics << setw(12)
                  << c_re(fti->_FFTResultX1(i)/mp->_globalNMacros)
                  << " "
                  << setw(12)
                  << c_re(fti->_FFTResultX2(i)/mp->_globalNMacros)
                  << " "; 
    } 
    if(TImpedance::useYdimension)
    {
      osharmonics << setw(12)
                  << c_re(fti->_FFTResultY1(i)/mp->_globalNMacros)
                  << " "
                  << setw(12)
                  << c_re(fti->_FFTResultY2(i)/mp->_globalNMacros)
                  << " ";
    } 
  }
  osharmonics << "\n";
}

Void TImpedance::dump_TImped(const Integer &n, Ostream &os)
{
// Dump the Impedance Harmonics

  if (n > nTImpedance) except(badTImpedance);
  TImp *ti = TImp::safeCast(TImpedancePointers(n - 1));

  os << "Mode, "
     << "_zXImped_nplus.re , _zXImped_nplus.im , "
     << "_zXImped_nminus.re, _zXImped_nminus.im, "
     << "_zYImped_nplus.re , _zYImped_nplus.im , "
     << "_zYImped_nminus.re, _zYImped_nminus.im"
     << "\n\n";

  if(ti->isKindOf(FFTTImpedance::desc()) == 1)
  {
    FFTTImpedance *fti;
    fti = FFTTImpedance::safeCast(TImpedancePointers(n - 1));
    for (Integer i = 1; i <= fti->_nBins; i++)
    {
      os << i                          << "    "
         << fti->_zXImped_nplus(i).re  << "    "
         << fti->_zXImped_nplus(i).im  << "    "
         << fti->_zXImped_nminus(i).re << "    "
         << fti->_zXImped_nminus(i).im << "    "
         << fti->_zYImped_nplus(i).re  << "    "
         << fti->_zYImped_nplus(i).im  << "    "
         << fti->_zYImped_nminus(i).re << "    "
         << fti->_zYImped_nminus(i).im << "\n";
    }
  }
  else if(ti->isKindOf(FreqDepTImp::desc()) == 1)
  {
    FreqDepTImp *fti;
    fti = FreqDepTImp::safeCast(TImpedancePointers(n - 1));
    for (Integer i = 1; i <= fti->_nBins; i++)
    {
      os << i                          << "    "
         << fti->_zXImped_nplus(i).re  << "    "
         << fti->_zXImped_nplus(i).im  << "    "
         << fti->_zXImped_nminus(i).re << "    "
         << fti->_zXImped_nminus(i).im << "    "
         << fti->_zYImped_nplus(i).re  << "    "
         << fti->_zYImped_nplus(i).im  << "    "
         << fti->_zYImped_nminus(i).re << "    "
         << fti->_zYImped_nminus(i).im << "\n";
    }
  }
  else if(ti->isKindOf(FreqBetDepTImp::desc()) == 1)
  {
    FreqBetDepTImp *fti;
    fti = FreqBetDepTImp::safeCast(TImpedancePointers(n - 1));
    for (Integer i = 1; i <= fti->_nBins; i++)
    {
      os << i                          << "    "
         << fti->_zXImped_nplus(i).re  << "    "
         << fti->_zXImped_nplus(i).im  << "    "
         << fti->_zXImped_nminus(i).re << "    "
         << fti->_zXImped_nminus(i).im << "    "
         << fti->_zYImped_nplus(i).re  << "    "
         << fti->_zYImped_nplus(i).im  << "    "
         << fti->_zYImped_nminus(i).re << "    "
         << fti->_zYImped_nminus(i).im << "\n";
    }
  }
  else
  {
    except(badTImpedance);
  }
}
