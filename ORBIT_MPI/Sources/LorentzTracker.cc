/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   LorentzTracker.cc
//
// AUTHOR
//   Jeff Holmes
//
// CREATED
//   03/03/2009
//
// MODIFIED
//
// DESCRIPTION
//   Code for module for tracking through time-dependent electromagnetic fields
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////
//
// include files
//
///////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "StreamType.h"
#include "StrClass.h"
#include "TeaPot.h"
#include "ThinLens.h"
#include "LorentzTracker.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include "Consts.h"

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// Static Definitions
//
///////////////////////////////////////////////////////////////////////////

Array(ObjectPtr) LZPointers;
extern Array(ObjectPtr) syncP, mParts, nodes, tMaps;

// Granularity to allocate entries in work arrays

const Integer Node_Alloc_Chunk = 10;


///////////////////////////////////////////////////////////////////////////
//
// Global Variables for Subroutine:
//
///////////////////////////////////////////////////////////////////////////

Real  xField3DT = 0.0,  yField3DT = 0.0,  zField3DT = 0.0,
     ExField3DT = 0.0, EyField3DT = 0.0, EzField3DT = 0.0,
     BxField3DT = 0.0, ByField3DT = 0.0, BzField3DT = 0.0,
     tField3DT;

Real   phiE3DTx,   phiE3DTy,   phiE3DTz,
      phiE3DTxx,  phiE3DTxy,  phiE3DTxz,  phiE3DTyy,  phiE3DTyz,  phiE3DTzz,
     phiE3DTxxx, phiE3DTxxy, phiE3DTxxz, phiE3DTxyy, phiE3DTxyz, phiE3DTxzz,
     phiE3DTyyy, phiE3DTyyz, phiE3DTyzz, phiE3DTzzz,
       phiB3DTx,   phiB3DTy,   phiB3DTz,
      phiB3DTxx,  phiB3DTxy,  phiB3DTxz,  phiB3DTyy,  phiB3DTyz,  phiB3DTzz,
     phiB3DTxxx, phiB3DTxxy, phiB3DTxxz, phiB3DTxyy, phiB3DTxyz, phiB3DTxzz,
     phiB3DTyyy, phiB3DTyyz, phiB3DTyzz, phiB3DTzzz;

Integer        nXGrid3DT,   nYGrid3DT,   nZGrid3DT;
Vector(Real)    XGrid3DT,    YGrid3DT,    ZGrid3DT;
Array3(Real)   EXGrid3DT,   EYGrid3DT,   EZGrid3DT;
Array3(Real)   BXGrid3DT,   BYGrid3DT,   BZGrid3DT;
Array3(Real) PhEXGrid3DT, PhEYGrid3DT, PhEZGrid3DT;
Array3(Real) PhBXGrid3DT, PhBYGrid3DT, PhBZGrid3DT;

///////////////////////////////////////////////////////////////////////////
//
// Local Functions:
//
///////////////////////////////////////////////////////////////////////////

Integer LZfactorial(Integer n)
{
   return (n>1) ? n*LZfactorial(n-1) :1;
}

Void LZfindPhases(MapBase &tm, MacroPart& mp, const Integer &i);
Void LZdoTunes(MapBase &tm, MacroPart &mp);

Real LZxPhase, LZyPhase;

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   LZBase
//
// INHERITANCE RELATIONSHIPS
//   LZBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a base class for storing LorentzTracker Node information.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class LZBase : public MapBase
{
  Declare_Standard_Members(LZBase, MapBase);

public:
    LZBase(const String &n, const Integer &order,
           const Real &bx, const Real &by,
	       const Real &ax, const Real &ay,
           const Real &ex, const Real &epx,
           const Real &l, const String &et):
    MapBase(n, order, bx, by, ax, ay, ex, epx, l, et)
    {
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Void _showLZ(Ostream &sout)=0;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LZBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(LZBase, MapBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   LZ3DT
//
// INHERITANCE RELATIONSHIPS
//   LZ3DT -> LZBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class to track in time-dependent electromagnetic fields.
//
// PUBLIC MEMBERS
//   n       (str) --> _name    : Name for this node
//   order   (int) --> _oindex  : node order index
//   bx     (real) --> _betaX   : The horizontal beta value at the
//                                beginning of the Node [m]
//   by     (real) --> _betaY   : The vertical beta value at the
//                                beginning of the Node [m]
//   ax     (real) --> _alphaX  : The horizontal alpha value at the
//                                beginning of the Node
//   ay     (real) --> _alphaY  : The horizontal alpha  value at the
//                                beginning of the Node
//   ex     (real) --> _etaX    : The horizontal dispersion [m]
//   epx    (real) --> _etaPX   : The horizontal dispersion prime
//   l      (real) --> _length  : The length of the node
//   et   (string) --> _et      : Element Type
//   sub     (sub) --> _sub     : Subroutine to acquire fields
//   zi     (real) --> _zi      : Initial tracking position [m]
//   zf     (real) --> _zf      : Final tracking position [m]
//   ds     (real) --> _ds      : Integration step size
//   niters  (int) --> _niters  : Number predictor-corrector iterations
//   resid  (real) --> _resid   : Predictor-corrector residual
//   xrefi  (real) --> _xrefi   : Initial reference particle x [mm]
//   yrefi  (real) --> _yrefi   : Initial reference particle y [mm]
//   eulerai(real) --> _eulerai : Initial reference Euler angle alpha [mr]
//   eulerbi(real) --> _eulerbi : Initial reference Euler angle beta [mr]
//   eulergi(real) --> _eulergi : Initial reference Euler angle gamma [mr]
//   xreff  (real) --> _xreff   : Final reference particle x [mm]
//   yreff  (real) --> _yreff   : Final reference particle y [mm]
//   euleraf(real) --> _euleraf : Final reference Euler angle alpha [mr]
//   eulerbf(real) --> _eulerbf : Final reference Euler angle beta [mr]
//   eulergf(real) --> _eulergf : Final reference Euler angle gamma [mr]
//   sref   (real) --> _sref    : Path length of reference particle [m]
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class LZ3DT : public LZBase
{
  Declare_Standard_Members(LZ3DT, LZBase);
  public:

  LZ3DT(const String &n, const Integer &order,
        const Real &bx, const Real &by,
        const Real &ax, const Real &ay,
        const Real &ex, const Real &epx,
        const Real &l, const String &et,
        const Subroutine sub,
        const Real &zi, const Real &zf,
        const Real &ds, const Integer &niters,
        const Real &resid,
        const Real &xrefi, const Real &yrefi,
        const Real &eulerai, const Real &eulerbi,
        const Real &eulergi):
  LZBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _xrefi(xrefi), _yrefi(yrefi),
  _eulerai(eulerai), _eulerbi(eulerbi), _eulergi(eulergi),
  _zi(zi), _zf(zf), _sub(sub), _ds(ds),
  _niters(niters), _resid(resid)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showLZ(Ostream &sout);

  Subroutine _sub;
  Integer _niters;
  Real  _zi, _zf, _ds, _resid;
  Real _xrefi, _yrefi, _eulerai, _eulerbi, _eulergi;
  Real _xreff, _yreff, _euleraf, _eulerbf, _eulergf, _sref;
  Real _eTotalf, _gammaf, _betaf, _eKineticf, _dppFacf;
  Real _timef;

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS LZ3DT
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(LZ3DT, LZBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LZ3DT::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LZ3DT::_nodeCalculator(MacroPart &mp)
{
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX  = _betaX;
  Ring::betaY  = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX   = _etaX;
  Ring::etaPX  = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  Real coeff = 1.e-09 * Consts::vLight * mp._syncPart._charge;

  Real eTotal = mp._syncPart._eTotal;
  Real gamma = mp._syncPart._gammaSync;
  Real beta = mp._syncPart._betaSync;
  Real pmag = Sqrt(Sqr(eTotal) - Sqr(mp._syncPart._e0));
  Real betac = beta * Consts::vLight;
  Real tref = 0.0;

  Real  ds, resid;
  Real  dx,  dy,  dz,  dxo,  dyo,  dzo;
  Real dpx, dpy, dpz, dpxo, dpyo, dpzo;

  Real eulera = 1.0e-03 * _eulerai;
  Real eulerb = 1.0e-03 * _eulerbi;
  Real eulerg = 1.0e-03 * _eulergi;

  Real  xref = 1.0e-03 * _xrefi;
  Real  yref = 1.0e-03 * _yrefi;
  Real  zref = _zi;
  Real pxref = pmag * sin(eulerb) * cos(eulera);
  Real pyref = pmag * sin(eulerb) * sin(eulera);
  Real pzref = pmag * cos(eulerb);

  _sref = 0.0;
  Integer iquit = 0;

  xField3DT = xref;
  yField3DT = yref;
  zField3DT = zref;
  tField3DT = tref;
  _sub();
  ExField3DT *= LorentzTracker::EBScale;
  EyField3DT *= LorentzTracker::EBScale;
  EzField3DT *= LorentzTracker::EBScale;
  BxField3DT *= LorentzTracker::EBScale;
  ByField3DT *= LorentzTracker::EBScale;
  BzField3DT *= LorentzTracker::EBScale;

  OFstream fio("RefPath", ios::app);
  fio <<      _sref << "  " <<       tref << "  "
      <<       xref << "  " <<       yref << "  " <<       zref << "  "
      <<      pxref << "  " <<      pyref << "  " <<      pzref << "  "
      << ExField3DT << "  " << EyField3DT << "  " << EzField3DT << "  "
      << BxField3DT << "  " << ByField3DT << "  " << BzField3DT << "\n";

  while(iquit == 0)
  {
    ds = _ds;
    dz = ds * pzref / pmag;
    if(dz > (_zf - zref))
    {
      dz = _zf - zref;
      ds = dz * pmag / pzref;
      iquit = 1;
    }
    dx = ds * pxref / pmag;
    dy = ds * pyref / pmag;

    Real dt = ds / betac;
    xField3DT = xref + dx / 2.0;
    yField3DT = yref + dy / 2.0;
    zField3DT = zref + dz / 2.0;
    tField3DT = tref + dt / 2.0;
    _sub();
    ExField3DT *= LorentzTracker::EBScale;
    EyField3DT *= LorentzTracker::EBScale;
    EzField3DT *= LorentzTracker::EBScale;
    BxField3DT *= LorentzTracker::EBScale;
    ByField3DT *= LorentzTracker::EBScale;
    BzField3DT *= LorentzTracker::EBScale;
    dpx = coeff * (ExField3DT * dt + dy * BzField3DT - dz * ByField3DT);
    dpy = coeff * (EyField3DT * dt + dz * BxField3DT - dx * BzField3DT);
    dpz = coeff * (EzField3DT * dt + dx * ByField3DT - dy * BxField3DT);
    pmag = Sqrt(Sqr(pxref + dpx / 2.0) +
                Sqr(pyref + dpy / 2.0) +
                Sqr(pzref + dpz / 2.0));

    eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pmag));
    gamma = eTotal / mp._syncPart._e0;
    beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
    betac = beta * Consts::vLight;

    for(i = 1; i < _niters; i++)
    {
      dxo = dx;
      dyo = dy;
      dzo = dz;
      dpxo = dpx;
      dpyo = dpy;
      dpzo = dpz;

      ds = _ds;
      dz = ds * (pzref + dpz / 2.0) / pmag;
      if(dz > (_zf - zref))
      {
        dz = _zf - zref;
        ds = dz * pmag / (pzref + dpz / 2.0);
        iquit = 1;
      }
      dx = ds * (pxref + dpx / 2.0) / pmag;
      dy = ds * (pyref + dpy / 2.0) / pmag;

      dt = ds / betac;
      xField3DT = xref + dx / 2.0;
      yField3DT = yref + dy / 2.0;
      zField3DT = zref + dz / 2.0;
      tField3DT = tref + dt / 2.0;
      _sub();
      ExField3DT *= LorentzTracker::EBScale;
      EyField3DT *= LorentzTracker::EBScale;
      EzField3DT *= LorentzTracker::EBScale;
      BxField3DT *= LorentzTracker::EBScale;
      ByField3DT *= LorentzTracker::EBScale;
      BzField3DT *= LorentzTracker::EBScale;
      dpx = coeff * (ExField3DT * dt + dy * BzField3DT - dz * ByField3DT);
      dpy = coeff * (EyField3DT * dt + dz * BxField3DT - dx * BzField3DT);
      dpz = coeff * (EzField3DT * dt + dx * ByField3DT - dy * BxField3DT);
      pmag = Sqrt(Sqr(pxref + dpx / 2.0) +
                  Sqr(pyref + dpy / 2.0) +
                  Sqr(pzref + dpz / 2.0));

      eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pmag));
      gamma = eTotal / mp._syncPart._e0;
      beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
      betac = beta * Consts::vLight;

      resid = Sqrt((dx  -  dxo) * (dx - dxo)   +
                   (dpx - dpxo) * (dpx - dpxo) +
                   (dy  -  dyo) * (dy - dyo)   +
                   (dpy - dpyo) * (dpy - dpyo) +
                   (dz  -  dzo) * (dz - dzo)   +
                   (dpz - dpzo) * (dpz - dpzo) );
      if(resid < _resid) break;
    }

    _sref +=  ds;
    tref  +=  dt;
    xref  +=  dx;
    yref  +=  dy;
    zref  +=  dz;
    pxref += dpx;
    pyref += dpy;
    pzref += dpz;

    fio <<      _sref << "  " <<       tref << "  "
        <<       xref << "  " <<       yref << "  " <<       zref << "  "
        <<      pxref << "  " <<      pyref << "  " <<      pzref << "  "
        << ExField3DT << "  " << EyField3DT << "  " << EzField3DT << "  "
        << BxField3DT << "  " << ByField3DT << "  " << BzField3DT << "\n";

    pmag = Sqrt(pxref * pxref + pyref * pyref + pzref * pzref);
    eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pmag));
    gamma = eTotal / mp._syncPart._e0;
    beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
    betac = beta * Consts::vLight;
  }

  fio.close();

  _eTotalf = eTotal;
  _gammaf = gamma;
  _betaf = beta;
  _eKineticf = eTotal - mp._syncPart._e0;
  _dppFacf = 1.0 / (Sqr(beta) * eTotal);
  _timef = tref;

  _xreff = 1.e+03 * xref;
  _yreff = 1.e+03 * yref;

  eulera = atan2(pyref, pxref);
  eulerb = atan2(Sqrt(pyref * pyref + pxref * pxref), pzref);
  _euleraf = 1.0e+03 * eulera;
  _eulerbf = 1.0e+03 * eulerb;
  _eulergf = -_euleraf;

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    LZ3DT *tm = LZ3DT::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i = oldSize + 1; i <= mp._nMacros; i++) // set phases of new guys
    {
      LZfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = LZxPhase;
      Particles::yPhaseOld(i) = LZyPhase;
    }
  }
}

Void LZ3DT::_showLZ(Ostream &sout)
{
  sout << " LZ3DT " << _name << "\n";
  sout << " Index in ring " << _oindex << "\n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of LZ3DT = " << _length << "\n";
  sout << " Initial reference particle x = " << _xrefi << "\n";
  sout << " Initial reference particle y = " << _yrefi << "\n";
  sout << " Initial reference particle Euler alpha = " << _eulerai << "\n";
  sout << " Initial reference particle Euler beta = " << _eulerbi << "\n";
  sout << " Initial reference particle Euler gamma = " << _eulergi << "\n";
  sout << " Initial tracking position = " << _zi << "\n";
  sout << " Final reference particle x = " << _xreff << "\n";
  sout << " Final reference particle y = " << _yreff << "\n";
  sout << " Final reference particle Euler alpha = " << _euleraf << "\n";
  sout << " Final reference particle Euler beta = " << _eulerbf << "\n";
  sout << " Final reference particle Euler gamma = " << _eulergf << "\n";
  sout << " Final reference particle path length = " << _sref << "\n";
  sout << " Final tracking position = " << _zf << "\n";
  sout << " Integration step size = " << _ds << "\n";
  sout << " Predictor corrector iterations = " << _niters << "\n";
  sout << " Predictor corrector residual =  " << _resid << "\n";

  Real eulera = _euleraf / 1.e+03;
  Real eulerb = _eulerbf / 1.e+03;
  Real eulerg = _eulergf / 1.e+03;

  Real pxref = 1.e+03 * sin(eulerb) * cos(eulera);
  Real pyref = 1.e+03 * sin(eulerb) * sin(eulera);
  Real pzref = cos(eulerb);

  sout << " Final reference particle px = " << pxref << "\n";
  sout << " Final reference particle py = " << pyref << "\n";
  sout << " Final reference particle pz = " << pzref << "\n";
  
  sout << " Final Reference Total Energy = " << _eTotalf << "\n";
  sout << " Final Reference Relativistic gamma = " << _gammaf << "\n";
  sout << " Final Reference Relativistic beta = " << _betaf << "\n";
  sout << " Final Reference Kinetic Energy = " << _eKineticf << "\n";
  sout << " Final Reference dppFac = " << _dppFacf << "\n";
  sout << " Final Reference time = " << _timef << "\n";

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LZ3DT::updatePartAtNode
//
// DESCRIPTION
//   Takes the particles through a time-dependent electromagnetic field
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LZ3DT::_updatePartAtNode(MacroPart& mp)
{
  Integer i, j;

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  Real  ds, resid;
  Real  dx,  dy,  dz,  dxo,  dyo,  dzo;
  Real dpx, dpy, dpz, dpxo, dpyo, dpzo;

  Real xrefi = 1.0e-03 * _xrefi;
  Real yrefi = 1.0e-03 * _yrefi;
  Real xreff = 1.0e-03 * _xreff;
  Real yreff = 1.0e-03 * _yreff;

  Real ca, sa, cb, sb, cg, sg;

  Real eulera = 1.0e-03 * _eulerai;
  Real eulerb = 1.0e-03 * _eulerbi;
  Real eulerg = 1.0e-03 * _eulergi;

  ca = cos(eulera);
  sa = sin(eulera);
  cb = cos(eulerb);
  sb = sin(eulerb);
  cg = cos(eulerg);
  sg = sin(eulerg);

  Real cxx   =  cb * ca * cg - sa * sg;
  Real cxy   = -cb * ca * sg - sa * cg;
  Real cxz   =  sb * ca;
  Real cyx   =  cb * sa * cg + ca * sg;
  Real cyy   = -cb * sa * sg + ca * cg;
  Real cyz   =  sb * sa;
  Real czx   = -sb * cg;
  Real czy   =  sb * sg;
  Real czz   =  cb;

  eulera = 1.0e-03 * _euleraf;
  eulerb = 1.0e-03 * _eulerbf;
  eulerg = 1.0e-03 * _eulergf;

  ca = cos(eulera);
  sa = sin(eulera);
  cb = cos(eulerb);
  sb = sin(eulerb);
  cg = cos(eulerg);
  sg = sin(eulerg);

  Real dxx =  cb * ca * cg - sa * sg;
  Real dxy =  cb * sa * cg + ca * sg;
  Real dxz = -sb * cg;
  Real dyx = -cb * ca * sg - sa * cg;
  Real dyy = -cb * sa * sg + ca * cg;
  Real dyz =  sb * sg;
  Real dzx =  sb * ca;
  Real dzy =  sb * sa;
  Real dzz =  cb;

  Real eTotal;
  Real gamma;
  Real beta;
  Real pmag;
  Real betac;
  Real timemp;

  Real coeff = 1.e-09 * Consts::vLight * mp._syncPart._charge;
  eTotal = mp._syncPart._eTotal;
  gamma = eTotal / mp._syncPart._e0;
  beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
  betac = beta * Consts::vLight;

  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber * betac / Ring::lRing;

  Real pmult;
  Real dt;

  Real mpx, mpy, mpz, mpxp, mpyp, mpzp;
  Real xj, yj, zj, pxj, pyj, pzj, sj;

  for(j = 1; j <= mp._nMacros; j++)
  {
    eTotal = mp._syncPart._eTotal + mp._deltaE(j);
    gamma = eTotal / mp._syncPart._e0;
    beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
    betac = beta * Consts::vLight;
    pmag = Sqrt(Sqr(eTotal) - Sqr(mp._syncPart._e0));
    pmult = pmag / (1.0 + mp._dp_p(j));
    timemp = mp._phi(j) / Factor;

    mpx  = 1.0e-03 * mp._x(j);
    mpy  = 1.0e-03 * mp._y(j);
    mpz  = -(czx * mpx + czy * mpy) / czz;
    mpxp = 1.0e-03 * pmult * mp._xp(j);
    mpyp = 1.0e-03 * pmult * mp._yp(j);
    mpzp = pmag;
    mpx  += mpxp * mpz / mpzp;
    mpy  += mpyp * mpz / mpzp;

    xj = xrefi + cxx * mpx + cxy * mpy + cxz * mpz;
    yj = yrefi + cyx * mpx + cyy * mpy + cyz * mpz;
    zj = _zi;
    pxj = cxx * mpxp + cxy * mpyp + cxz * mpzp;
    pyj = cyx * mpxp + cyy * mpyp + cyz * mpzp;
    pzj = czx * mpxp + czy * mpyp + czz * mpzp;
    pmag = Sqrt(pxj * pxj + pyj * pyj + pzj * pzj);

    eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pmag));
    gamma = eTotal / mp._syncPart._e0;
    beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
    betac = beta * Consts::vLight;

    sj = 0.0;
    Integer iquit = 0;

    while(iquit == 0)
    {
      ds = _ds;
      dz = ds * pzj / pmag;
      if(dz > (_zf - zj))
      {
        dz = _zf - zj;
        ds = dz * pmag / pzj;
        iquit = 1;
      }
      dx = ds * pxj / pmag;
      dy = ds * pyj / pmag;
      dt = ds / betac;

      xField3DT = xj + dx / 2.0;
      yField3DT = yj + dy / 2.0;
      zField3DT = zj + dz / 2.0;
      tField3DT = timemp + dt / 2.0;
      _sub();
      ExField3DT *= LorentzTracker::EBScale;
      EyField3DT *= LorentzTracker::EBScale;
      EzField3DT *= LorentzTracker::EBScale;
      BxField3DT *= LorentzTracker::EBScale;
      ByField3DT *= LorentzTracker::EBScale;
      BzField3DT *= LorentzTracker::EBScale;
      dpx = coeff * (ExField3DT * dt + dy * BzField3DT - dz * ByField3DT);
      dpy = coeff * (EyField3DT * dt + dz * BxField3DT - dx * BzField3DT);
      dpz = coeff * (EzField3DT * dt + dx * ByField3DT - dy * BxField3DT);
      pmag = Sqrt(Sqr(pxj + dpx / 2.0) +
                  Sqr(pyj + dpy / 2.0) +
                  Sqr(pzj + dpz / 2.0));

      eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pmag));
      gamma = eTotal / mp._syncPart._e0;
      beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
      betac = beta * Consts::vLight;

      for(i = 1; i < _niters; i++)
      {
        dxo = dx;
        dyo = dy;
        dzo = dz;
        dpxo = dpx;
        dpyo = dpy;
        dpzo = dpz;

        ds = _ds;
        dz = ds * (pzj + dpz / 2.0) / pmag;
        if(dz > (_zf - zj))
        {
          dz = _zf - zj;
          ds = dz * pmag / (pzj + dpz / 2.0);
          iquit = 1;
        }
        dx = ds * (pxj + dpx / 2.0) / pmag;
        dy = ds * (pyj + dpy / 2.0) / pmag;
        dt = ds / betac;

        xField3DT = xj + dx / 2.0;
        yField3DT = yj + dy / 2.0;
        zField3DT = zj + dz / 2.0;
        tField3DT = timemp + dt / 2.0;
        _sub();
        ExField3DT *= LorentzTracker::EBScale;
        EyField3DT *= LorentzTracker::EBScale;
        EzField3DT *= LorentzTracker::EBScale;
        BxField3DT *= LorentzTracker::EBScale;
        ByField3DT *= LorentzTracker::EBScale;
        BzField3DT *= LorentzTracker::EBScale;
        dpx = coeff * (ExField3DT * dt + dy * BzField3DT - dz * ByField3DT);
        dpy = coeff * (EyField3DT * dt + dz * BxField3DT - dx * BzField3DT);
        dpz = coeff * (EzField3DT * dt + dx * ByField3DT - dy * BxField3DT);
        pmag = Sqrt(Sqr(pxj + dpx / 2.0) +
                    Sqr(pyj + dpy / 2.0) +
                    Sqr(pzj + dpz / 2.0));

        eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pmag));
        gamma = eTotal / mp._syncPart._e0;
        beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
        betac = beta * Consts::vLight;

        resid = Sqrt((dx  -  dxo) * (dx - dxo)   +
                     (dpx - dpxo) * (dpx - dpxo) +
                     (dy  -  dyo) * (dy - dyo)   +
                     (dpy - dpyo) * (dpy - dpyo) +
                     (dz  -  dzo) * (dz - dzo)   +
                     (dpz - dpzo) * (dpz - dpzo) );
        if(resid < _resid) break;
      }

      xj  +=  dx;
      yj  +=  dy;
      zj  +=  dz;
      pxj += dpx;
      pyj += dpy;
      pzj += dpz;
      sj  +=  ds;
      timemp += dt;
      pmag = Sqrt(Sqr(pxj) + Sqr(pyj) + Sqr(pzj));

      eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pmag));
      gamma = eTotal / mp._syncPart._e0;
      beta = Sqrt(1.0 - 1.0 / (gamma * gamma));
      betac = beta * Consts::vLight;
    }

    dpx  = dxx * pxj + dxy * pyj + dxz * pzj;
    dpy  = dyx * pxj + dyy * pyj + dyz * pzj;
    dpz  = dzx * pxj + dzy * pyj + dzz * pzj;
    dxo = xj - xreff;
    dyo = yj - yreff;
    dzo = dzx * dxo + dzy * dyo;

    dx = dxx * dxo + dxy * dyo - dpx * dzo / dpz;
    dy = dyx * dxo + dyy * dyo - dpy * dzo / dpz;

    mp._deltaE(j) = eTotal - _eTotalf;
    mp._dp_p(j) = mp._deltaE(j) * _dppFacf;
    pmult = pmag / (1.0 + mp._dp_p(j));

    mp._x(j)  = 1.0e+03 * dx;
    mp._xp(j) = 1.0e+03 * dpx / pmult;
    mp._y(j)  = 1.0e+03 * dy;
    mp._yp(j) = 1.0e+03 * dpy / pmult;
    mp._phi(j) = (timemp - _timef) * Factor;
    mp._deltaE(j) = eTotal - _eTotalf;
    mp._dp_p(j) = mp._deltaE(j) * _dppFacf;

    if(mp._phi(j) >  Consts::pi) mp._phi(j) -= Consts::twoPi;
    if(mp._phi(j) < -Consts::pi) mp._phi(j) += Consts::twoPi;
  }

  mp._syncPart._eTotal = _eTotalf;
  mp._syncPart._gammaSync = _gammaf;
  mp._syncPart._betaSync = _betaf;
  mp._syncPart._eKinetic = _eKineticf;
  mp._syncPart._dppFac = _dppFacf;
  Ring::time += 1.e+03 * _timef;

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  LZdoTunes(*this, mp);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LorentzTracker::ctor
//
// DESCRIPTION
//   Constructor for the LorentzTracker Module
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::ctor()
{
// set some initial values
  nLZNodes = 0;
  EBScale = 1.0;
  OmegaRes = 0.0;
  PhRes = 0.0;
  
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::nullOut
//
// DESCRIPTION
//   Zeroes the length of a TransMap Node
//   and grabs original length and lattice functions for substitute
//   node
//
// PARAMETERS
//   n    (str) --> _name   : Name for this node
//   order (int)--> _oindex : node order index
//   transmapNumber         : number of matrix to be nulled
//   bx                     : betax
//   by                     : betay
//   ax                     : alphax
//   ay                     : alphay
//   ex                     : etax
//   epx                    : etapx
//   length                 : length
//   et (str)   --> _et     : The type of element added
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::nullOut(const String &n, const Integer &order,
                             Integer &transmapNumber,
                             Real &bx, Real &by,
                             Real &ax, Real &ay,
                             Real &ex, Real &epx,
                             Real &length, const String &et)
{
  //  Set the nearest transfer map to negative length

  Integer i;
  MapBase *tm;

  transmapNumber = -1;

  for(i = 1; i <= nTransMaps; i++)
  {
    tm = MapBase::safeCast(tMaps(i - 1));
    if(tm->_oindex == order) transmapNumber = i;
  }

  if(transmapNumber < 0) except(badNode);
  if(transmapNumber > nTransMaps) except(badNode);

  tm = MapBase::safeCast(tMaps(transmapNumber - 1));

  bx   = tm -> _betaX;
  by   = tm -> _betaY;
  ax   = tm -> _alphaX;
  ay   = tm -> _alphaY;
  ex   = tm -> _etaX;
  epx  = tm -> _etaPX;
  length = tm -> _length;

  tm -> _length = -Consts::tiny;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::replaceLZ3DT
//
// DESCRIPTION
//   Replaces an existing transMap with an LZ3DT node
//
// PARAMETERS
//   n      (str)  --> _name    : Name for this node
//   order  (int)  --> _oindex  : node order index
//   et  (string)  --> _et      : Element Type
//   sub    (sub)  --> _sub     : Subroutine to acquire fields
//   zi    (real)  --> _zi      : Initial tracking position [m]
//   zf    (real)  --> _zf      : Final tracking position [m]
//   ds    (real)  --> _ds      : Integration step size
//   niters (int)  --> _niters  : Number predictor-corrector iterations
//   resid (real)  --> _resid   : Predictor-corrector residual
//   xrefi (real)  --> _xrefi   : Initial reference particle x [mm]
//   yrefi (real)  --> _yrefi   : Initial reference particle y [mm]
//   eulerai(real) --> _eulerai : Initial reference Euler angle alpha [mr]
//   eulerbi(real) --> _eulerbi : Initial reference Euler angle beta [mr]
//   eulergi(real) --> _eulergi : Initial reference Euler angle gamma [mr]
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::replaceLZ3DT(const String &n, const Integer &order,
                                 const String &et,
                                 const Subroutine sub,
                                 const Real &zi, const Real &zf,
                                 const Real &ds, const Integer &niters,
                                 const Real &resid,
                                 const Real &xrefi, const Real &yrefi,
                                 const Real &eulerai, const Real &eulerbi,
                                 const Real &eulergi)
{
  Real bx, by, ax, ay, ex, epx, l;

  //  Set the nearest transmap length negative

  Integer i;
  Integer transmapNumber;

  LorentzTracker::nullOut(n, order, transmapNumber,
                          bx, by, ax, ay, ex, epx,
                          l, et);

  // Add the LZ3DT node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nLZNodes == LZPointers.size())
    LZPointers.resize(nLZNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new LZ3DT(n, order,
                                bx, by, ax, ay, ex, epx,
                                l, et,
                                sub, zi, zf, ds,
                                niters, resid,
                                xrefi, yrefi,
                                eulerai, eulerbi, eulergi);
  nTransMaps++;
  nLZNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  LZPointers(nLZNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::addLZ3DT
//
// DESCRIPTION
//   Adds a Lorentz tracker node
//
// PARAMETERS
//   n      (str) --> _name   : Name for this node
//   order  (int) --> _oindex : node order index
//   bx    (real) --> _betaX  : The horizontal beta value at the
//                              beginning of the Node [m]
//   by    (real) --> _betaY  : The vertical beta value at the
//                              beginning of the Node [m]
//   ax    (real) --> _alphaX : The horizontal alpha value at the
//                              beginning of the Node
//   ay    (real) --> _alphaY : The horizontal alpha  value at the
//                              beginning of the Node
//   ex    (real) --> _etaX   : The horizontal dispersion [m]
//   epx   (real) --> _etaPX  : The horizontal dispersion prime
//   l     (real) --> _length : The length of the node
//   et  (string) --> _et     : Element Type
//   sub    (sub)  --> _sub     : Subroutine to acquire fields
//   zi    (real)  --> _zi      : Initial tracking position [m]
//   zf    (real)  --> _zf      : Final tracking position [m]
//   ds    (real)  --> _ds      : Integration step size
//   niters (int)  --> _niters  : Number predictor-corrector iterations
//   resid (real)  --> _resid   : Predictor-corrector residual
//   xrefi (real)  --> _xrefi   : Initial reference particle x [mm]
//   yrefi (real)  --> _yrefi   : Initial reference particle y [mm]
//   eulerai(real) --> _eulerai : Initial reference Euler angle alpha [mr]
//   eulerbi(real) --> _eulerbi : Initial reference Euler angle beta [mr]
//   eulergi(real) --> _eulergi : Initial reference Euler angle gamma [mr]
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::addLZ3DT(const String &n, const Integer &order,
                             const Real &bx, const Real &by,
                             const Real &ax, const Real &ay,
                             const Real &ex, const Real &epx,
                             const Real &l, const String &et,
                             const Subroutine sub,
                             const Real &zi, const Real &zf,
                             const Real &ds, const Integer &niters,
                             const Real &resid,
                             const Real &xrefi, const Real &yrefi,
                             const Real &eulerai, const Real &eulerbi,
                             const Real &eulergi)
{
  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nLZNodes == LZPointers.size())
    LZPointers.resize(nLZNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new LZ3DT(n, order,
                                bx, by, ax, ay, ex, epx,
                                l, et,
                                sub, zi, zf, ds,
                                niters, resid,
                                xrefi, yrefi,
                                eulerai, eulerbi, eulergi);
  nTransMaps++;
  nLZNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  LZPointers(nLZNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   LorentzTracker::showLZ
//
// DESCRIPTION
//   Prints out the settings of the active LorentzTracker Nodes
//
// PARAMETERS
//   sout - The output stream
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::showLZ(Ostream &sout)
{
  LZBase *tlp;

  if (nLZNodes == 0)
  {
    sout << "\n No LorentzTracker Nodes are present \n\n";
    return;
  }

  sout << "\n\n LorentzTracker Nodes present in ring:\n\n";

  for (Integer i = 1; i <= nLZNodes; i++)
  {
    tlp = LZBase::safeCast(LZPointers(i-1));
    tlp->_showLZ(sout);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::ParseTest3DT
//
// DESCRIPTION
//   Routine to parse Lorentz 3DT test field file.
//
// PARAMETERS
//   fileName -> File containing input field information.
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::ParseTest3DT(const String &fileName)
{
  IFstream fio(fileName, ios::in);
  if(fio.good() != 1) except(badEBFile);

  String Dummy;
  fio >> Dummy >> OmegaRes
      >> Dummy >> PhRes
      >> Dummy >> phiE3DTx
      >> Dummy >> phiE3DTy
      >> Dummy >> phiE3DTz
      >> Dummy >> phiE3DTxx
      >> Dummy >> phiE3DTxy
      >> Dummy >> phiE3DTxz
      >> Dummy >> phiE3DTyy
      >> Dummy >> phiE3DTyz
      >> Dummy >> phiE3DTxxx
      >> Dummy >> phiE3DTxxy
      >> Dummy >> phiE3DTxxz
      >> Dummy >> phiE3DTxyy
      >> Dummy >> phiE3DTxyz
      >> Dummy >> phiE3DTyyy
      >> Dummy >> phiE3DTyyz
      >> Dummy >> phiB3DTx
      >> Dummy >> phiB3DTy
      >> Dummy >> phiB3DTz
      >> Dummy >> phiB3DTxx
      >> Dummy >> phiB3DTxy
      >> Dummy >> phiB3DTxz
      >> Dummy >> phiB3DTyy
      >> Dummy >> phiB3DTyz
      >> Dummy >> phiB3DTxxx
      >> Dummy >> phiB3DTxxy
      >> Dummy >> phiB3DTxxz
      >> Dummy >> phiB3DTxyy
      >> Dummy >> phiB3DTxyz
      >> Dummy >> phiB3DTyyy
      >> Dummy >> phiB3DTyyz;

  OmegaRes *= 2.0 * Consts::pi;
  phiE3DTzz  = -phiE3DTxx  - phiE3DTyy;
  phiE3DTxzz = -phiE3DTxxx - phiE3DTxyy;
  phiE3DTyzz = -phiE3DTxxy - phiE3DTyyy;
  phiE3DTzzz = -phiE3DTxxz - phiE3DTyyz;
  phiB3DTzz  = -phiB3DTxx  - phiB3DTyy;
  phiB3DTxzz = -phiB3DTxxx - phiB3DTxyy;
  phiB3DTyzz = -phiB3DTxxy - phiB3DTyyy;
  phiB3DTzzz = -phiB3DTxxz - phiB3DTyyz;

  fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::ParseMults3DT
//
// DESCRIPTION
//   Routine to parse Lorentz 3DT multipole field file.
//
// PARAMETERS
//   fileName -> File containing input field information.
//   zmin     -> Minimum z for data.
//   zmax     -> Maximum z for data.
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::ParseMults3DT(const String &fileName,
                                const Real &zmin, const Real &zmax)
{
  IFstream fio(fileName, ios::in);
  if(fio.good() != 1) except(badEBFile);

  fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::ParseGrid3DT
//
// DESCRIPTION
//   Routine to parse Lorentz 3DT grid field file.
//
// PARAMETERS
//   fileName -> File containing input field information.
//   xmin     -> Minimum x for data.
//   xmax     -> Maximum x for data.
//   ymin     -> Minimum y for data.
//   ymax     -> Maximum y for data.
//   zmin     -> Minimum z for data.
//   zmax     -> Maximum z for data.
//   skipX    -> Determines number of x gridpoints used.
//   skipY    -> Determines number of y gridpoints used.
//   skipZ    -> Determines number of z gridpoints used.
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::ParseGrid3DT(const String &fileName,
                               const Real &xmin, const Real &xmax,
                               const Real &ymin, const Real &ymax,
                               const Real &zmin, const Real &zmax,
                               const Integer &skipX,
                               const Integer &skipY,
                               const Integer &skipZ)
{
  Integer nXTab, nYTab, nZTab;
  Integer iDummy, i, j, k;
  Integer xindex, yindex, zindex;
  String Dummy;
  Real frequency, x, y, z;
  Real  Ex, Ey, Ez, PhEx, PhEy, PhEz;
  Real  Bx, By, Bz, PhBx, PhBy, PhBz;

  IFstream fio(fileName, ios::in);
  if(fio.good() != 1) except(badEBFile);

  fio >> frequency >> nXTab >> nYTab >> nZTab;

  nZGrid3DT = 0;
  for(k = 0; k < nZTab; k++)
  {
    nYGrid3DT = 0;
    for(j = 0; j < nYTab; j++)
    {
      nXGrid3DT = 0;
      for(i = 0; i < nXTab; i++)
      {
        fio >> x >> y >> z;
        fio >> Ex >> Ey >> Ez;
        fio >> PhEx >> PhEy >> PhEz;
        fio >> Bx >> By >> Bz;
        fio >> PhBx >> PhBy >> PhBz;
        if((x >= xmin) && (x <= xmax))
        {
          if((i == (i / skipX) * skipX) || (i == nXTab - 1))
          {
            nXGrid3DT++;
          }
        }
      }
      if((y >= ymin) && (y <= ymax))
      {
        if((j == (j / skipY) * skipY) || (j == nYTab - 1))
        {
          nYGrid3DT++;
        }
      }
    }
    if((z >= zmin) && (z <= zmax))
    {
      if((k == (k / skipZ) * skipZ) || (k == nZTab - 1))
      {
        nZGrid3DT++;
      }
    }
  }
  fio.close();

  XGrid3DT.resize(nXGrid3DT);
  YGrid3DT.resize(nYGrid3DT);
  ZGrid3DT.resize(nZGrid3DT);
  EXGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  EYGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  EZGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  PhEXGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  PhEYGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  PhEZGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  BXGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  BYGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  BZGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  PhBXGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  PhBYGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);
  PhBZGrid3DT.resize(nXGrid3DT, nYGrid3DT, nZGrid3DT);

  IFstream fio2(fileName, ios::in);
  if(fio2.good() != 1) except(badEBFile);

  fio2 >> frequency >> nXTab >> nYTab >> nZTab;

  OmegaRes = 2.0 * Consts::pi * frequency;

  zindex = 1;
  for(k = 0; k < nZTab; k++)
  {
    yindex = 1;
    for(j = 0; j < nYTab; j++)
    {
      xindex = 1;
      for(i = 0; i < nXTab; i++)
      {
        fio2 >> x >> y >> z;
        fio2 >> Ex >> Ey >> Ez;
        fio2 >> PhEx >> PhEy >> PhEz;
        fio2 >> Bx >> By >> Bz;
        fio2 >> PhBx >> PhBy >> PhBz;
        if((z >= zmin) && (z <= zmax))
        {
          if((k == (k / skipZ) * skipZ) || (k == nZTab - 1))
          {
            if((y >= ymin) && (y <= ymax))
            {
              if((j == (j / skipY) * skipY) || (j == nYTab - 1))
              {
                if((x >= xmin) && (x <= xmax))
                {
                  if((i == (i / skipX) * skipX) || (i == nXTab - 1))
                  {
                    XGrid3DT(xindex) = x;
                    YGrid3DT(yindex) = y;
                    ZGrid3DT(zindex) = z;
                    EXGrid3DT(xindex, yindex, zindex) = Ex;
                    EYGrid3DT(xindex, yindex, zindex) = Ey;
                    EZGrid3DT(xindex, yindex, zindex) = Ez;
                    PhEXGrid3DT(xindex, yindex, zindex) = PhEx;
                    PhEYGrid3DT(xindex, yindex, zindex) = PhEy;
                    PhEZGrid3DT(xindex, yindex, zindex) = PhEz;
                    BXGrid3DT(xindex, yindex, zindex) = Bx;
                    BYGrid3DT(xindex, yindex, zindex) = By;
                    BZGrid3DT(xindex, yindex, zindex) = Bz;
                    PhBXGrid3DT(xindex, yindex, zindex) = PhBx;
                    PhBYGrid3DT(xindex, yindex, zindex) = PhBy;
                    PhBZGrid3DT(xindex, yindex, zindex) = PhBz;
                    xindex++;
                  }
                }
              }
            }
          }
        }
      }
      if((y >= ymin) && (y <= ymax))
      {
        if((j == (j / skipY) * skipY) || (j == nYTab - 1))
        {
          yindex++;
        }
      }
    }
    if((z >= zmin) && (z <= zmax))
    {
      if((k == (k / skipZ) * skipZ) || (k == nZTab - 1))
      {
        zindex++;
      }
    }
  }

  fio2.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::EBTest3DT
//
// DESCRIPTION
//   Routine to test various aspects of Lorentz 3DT Tracker.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::EBTest3DT()
{
  Real xF, yF, zF, COEFF;

  xF = xField3DT;
  yF = yField3DT;
  zF = zField3DT;

  ExField3DT = phiE3DTx +
               phiE3DTxx * xF +
               phiE3DTxy * yF +
               phiE3DTxz * zF +
               phiE3DTxxx * xF * xF / 2.0 +
               phiE3DTxxy * xF * yF +
               phiE3DTxxz * xF * zF +
               phiE3DTxyy * yF * yF / 2.0 +
               phiE3DTxyz * yF * zF +
               phiE3DTxzz * zF * zF / 2.0;

  EyField3DT = phiE3DTy +
               phiE3DTxy * xF +
               phiE3DTyy * yF +
               phiE3DTyz * zF +
               phiE3DTxxy * xF * xF / 2.0 +
               phiE3DTxyy * xF * yF +
               phiE3DTxyz * xF * zF +
               phiE3DTyyy * yF * yF / 2.0 +
               phiE3DTyyz * yF * zF +
               phiE3DTyzz * zF * zF / 2.0;

  EzField3DT = phiE3DTz +
               phiE3DTxz * xF +
               phiE3DTyz * yF +
               phiE3DTzz * zF +
               phiE3DTxxz * xF * xF / 2.0 +
               phiE3DTxyz * xF * yF +
               phiE3DTxzz * xF * zF +
               phiE3DTyyz * yF * yF / 2.0 +
               phiE3DTyzz * yF * zF +
               phiE3DTzzz * zF * zF / 2.0;
  BxField3DT = phiB3DTx +
               phiB3DTxx * xF +
               phiB3DTxy * yF +
               phiB3DTxz * zF +
               phiB3DTxxx * xF * xF / 2.0 +
               phiB3DTxxy * xF * yF +
               phiB3DTxxz * xF * zF +
               phiB3DTxyy * yF * yF / 2.0 +
               phiB3DTxyz * yF * zF +
               phiB3DTxzz * zF * zF / 2.0;

  ByField3DT = phiB3DTy +
               phiB3DTxy * xF +
               phiB3DTyy * yF +
               phiB3DTyz * zF +
               phiB3DTxxy * xF * xF / 2.0 +
               phiB3DTxyy * xF * yF +
               phiB3DTxyz * xF * zF +
               phiB3DTyyy * yF * yF / 2.0 +
               phiB3DTyyz * yF * zF +
               phiB3DTyzz * zF * zF / 2.0;

  BzField3DT = phiB3DTz +
               phiB3DTxz * xF +
               phiB3DTyz * yF +
               phiB3DTzz * zF +
               phiB3DTxxz * xF * xF / 2.0 +
               phiB3DTxyz * xF * yF +
               phiB3DTxzz * xF * zF +
               phiB3DTyyz * yF * yF / 2.0 +
               phiB3DTyzz * yF * zF +
               phiB3DTzzz * zF * zF / 2.0;

  COEFF = sin(OmegaRes * tField3DT + PhRes);
  ExField3DT *= COEFF;
  EyField3DT *= COEFF;
  EzField3DT *= COEFF;
  BxField3DT *= COEFF;
  ByField3DT *= COEFF;
  BzField3DT *= COEFF;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::EBMults3DT
//
// DESCRIPTION
//   Routine to use multipole expansion in Lorentz 3DT Tracker.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::EBMults3DT()
{
  Real xF, yF, zF;

  xF = xField3DT;
  yF = yField3DT;
  zF = zField3DT;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LorentzTracker::EBGrid3DT
//
// DESCRIPTION
//   Routine to interpolate E and B from grid for Lorentz 3DT Tracker.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LorentzTracker::EBGrid3DT()
{
  Real xF, yF, zF;

  xF = xField3DT;
  yF = yField3DT;
  zF = zField3DT;

  Integer ig, jg, kg, igp, jgp, kgp;
  Real dx, dy, dz, xfrac, yfrac, zfrac, xfracc, yfracc, zfracc;
  Real V, PhV, Vmmm, Vmmp, Vmpm, Vmpp, Vpmm, Vpmp, Vppm, Vppp;
  Real TPhase = OmegaRes * tField3DT + PhRes;

  dx = (XGrid3DT(nXGrid3DT) - XGrid3DT(1)) / Real(nXGrid3DT - 1);
  dy = (YGrid3DT(nYGrid3DT) - YGrid3DT(1)) / Real(nYGrid3DT - 1);
  dz = (ZGrid3DT(nZGrid3DT) - ZGrid3DT(1)) / Real(nZGrid3DT - 1);

  ig = Integer((xF - XGrid3DT(1)) / dx) + 1;
  jg = Integer((yF - YGrid3DT(1)) / dy) + 1;
  kg = Integer((zF - ZGrid3DT(1)) / dz) + 1;

  if(ig < 1) ig = 1;
  if(jg < 1) jg = 1;
  if(kg < 1) kg = 1;
  if(ig > nXGrid3DT - 1) ig = nXGrid3DT - 1;
  if(jg > nYGrid3DT - 1) jg = nYGrid3DT - 1;
  if(kg > nZGrid3DT - 1) kg = nZGrid3DT - 1;

  igp = ig + 1;
  jgp = jg + 1;
  kgp = kg + 1;

  xfrac = (xF - XGrid3DT(ig)) / dx;
  yfrac = (yF - YGrid3DT(jg)) / dy;
  zfrac = (zF - ZGrid3DT(kg)) / dz;

  xfracc = 1.0 - xfrac;
  yfracc = 1.0 - yfrac;
  zfracc = 1.0 - zfrac;

  V = EXGrid3DT(ig , jg , kg );
  PhV = PhEXGrid3DT(ig , jg , kg );
  Vmmm = V * sin(TPhase + PhV);
  V = EXGrid3DT(ig , jg , kgp);
  PhV = PhEXGrid3DT(ig , jg , kgp);
  Vmmp = V * sin(TPhase + PhV);
  V = EXGrid3DT(ig , jgp, kg );
  PhV = PhEXGrid3DT(ig , jgp, kg );
  Vmpm = V * sin(TPhase + PhV);
  V = EXGrid3DT(ig , jgp, kgp);
  PhV = PhEXGrid3DT(ig , jgp, kgp);
  Vmpp = V * sin(TPhase + PhV);
  V = EXGrid3DT(igp, jg , kg );
  PhV = PhEXGrid3DT(igp, jg , kg );
  Vpmm = V * sin(TPhase + PhV);
  V = EXGrid3DT(igp, jg , kgp);
  PhV = PhEXGrid3DT(igp, jg , kgp);
  Vpmp = V * sin(TPhase + PhV);
  V = EXGrid3DT(igp, jgp, kg );
  PhV = PhEXGrid3DT(igp, jgp, kg );
  Vppm = V * sin(TPhase + PhV);
  V = EXGrid3DT(igp, jgp, kgp);
  PhV = PhEXGrid3DT(igp, jgp, kgp);
  Vppp = V * sin(TPhase + PhV);

  ExField3DT =   Vmmm * xfracc * yfracc * zfracc +
               + Vmmp * xfracc * yfracc * zfrac  +
               + Vmpm * xfracc * yfrac  * zfracc +
               + Vmpp * xfracc * yfrac  * zfrac  +
               + Vpmm * xfrac  * yfracc * zfracc +
               + Vpmp * xfrac  * yfracc * zfrac  +
               + Vppm * xfrac  * yfrac  * zfracc +
               + Vppp * xfrac  * yfrac  * zfrac;

  V = EYGrid3DT(ig , jg , kg );
  PhV = PhEYGrid3DT(ig , jg , kg );
  Vmmm = V * sin(TPhase + PhV);
  V = EYGrid3DT(ig , jg , kgp);
  PhV = PhEYGrid3DT(ig , jg , kgp);
  Vmmp = V * sin(TPhase + PhV);
  V = EYGrid3DT(ig , jgp, kg );
  PhV = PhEYGrid3DT(ig , jgp, kg );
  Vmpm = V * sin(TPhase + PhV);
  V = EYGrid3DT(ig , jgp, kgp);
  PhV = PhEYGrid3DT(ig , jgp, kgp);
  Vmpp = V * sin(TPhase + PhV);
  V = EYGrid3DT(igp, jg , kg );
  PhV = PhEYGrid3DT(igp, jg , kg );
  Vpmm = V * sin(TPhase + PhV);
  V = EYGrid3DT(igp, jg , kgp);
  PhV = PhEYGrid3DT(igp, jg , kgp);
  Vpmp = V * sin(TPhase + PhV);
  V = EYGrid3DT(igp, jgp, kg );
  PhV = PhEYGrid3DT(igp, jgp, kg );
  Vppm = V * sin(TPhase + PhV);
  V = EYGrid3DT(igp, jgp, kgp);
  PhV = PhEYGrid3DT(igp, jgp, kgp);
  Vppp = V * sin(TPhase + PhV);

  EyField3DT =   Vmmm * xfracc * yfracc * zfracc +
               + Vmmp * xfracc * yfracc * zfrac  +
               + Vmpm * xfracc * yfrac  * zfracc +
               + Vmpp * xfracc * yfrac  * zfrac  +
               + Vpmm * xfrac  * yfracc * zfracc +
               + Vpmp * xfrac  * yfracc * zfrac  +
               + Vppm * xfrac  * yfrac  * zfracc +
               + Vppp * xfrac  * yfrac  * zfrac;

  V = EZGrid3DT(ig , jg , kg );
  PhV = PhEZGrid3DT(ig , jg , kg );
  Vmmm = V * sin(TPhase + PhV);
  V = EZGrid3DT(ig , jg , kgp);
  PhV = PhEZGrid3DT(ig , jg , kgp);
  Vmmp = V * sin(TPhase + PhV);
  V = EZGrid3DT(ig , jgp, kg );
  PhV = PhEZGrid3DT(ig , jgp, kg );
  Vmpm = V * sin(TPhase + PhV);
  V = EZGrid3DT(ig , jgp, kgp);
  PhV = PhEZGrid3DT(ig , jgp, kgp);
  Vmpp = V * sin(TPhase + PhV);
  V = EZGrid3DT(igp, jg , kg );
  PhV = PhEZGrid3DT(igp, jg , kg );
  Vpmm = V * sin(TPhase + PhV);
  V = EZGrid3DT(igp, jg , kgp);
  PhV = PhEZGrid3DT(igp, jg , kgp);
  Vpmp = V * sin(TPhase + PhV);
  V = EZGrid3DT(igp, jgp, kg );
  PhV = PhEZGrid3DT(igp, jgp, kg );
  Vppm = V * sin(TPhase + PhV);
  V = EZGrid3DT(igp, jgp, kgp);
  PhV = PhEZGrid3DT(igp, jgp, kgp);
  Vppp = V * sin(TPhase + PhV);

  EzField3DT =   Vmmm * xfracc * yfracc * zfracc +
               + Vmmp * xfracc * yfracc * zfrac  +
               + Vmpm * xfracc * yfrac  * zfracc +
               + Vmpp * xfracc * yfrac  * zfrac  +
               + Vpmm * xfrac  * yfracc * zfracc +
               + Vpmp * xfrac  * yfracc * zfrac  +
               + Vppm * xfrac  * yfrac  * zfracc +
               + Vppp * xfrac  * yfrac  * zfrac;

  V = BXGrid3DT(ig , jg , kg );
  PhV = PhBXGrid3DT(ig , jg , kg );
  Vmmm = V * sin(TPhase + PhV);
  V = BXGrid3DT(ig , jg , kgp);
  PhV = PhBXGrid3DT(ig , jg , kgp);
  Vmmp = V * sin(TPhase + PhV);
  V = BXGrid3DT(ig , jgp, kg );
  PhV = PhBXGrid3DT(ig , jgp, kg );
  Vmpm = V * sin(TPhase + PhV);
  V = BXGrid3DT(ig , jgp, kgp);
  PhV = PhBXGrid3DT(ig , jgp, kgp);
  Vmpp = V * sin(TPhase + PhV);
  V = BXGrid3DT(igp, jg , kg );
  PhV = PhBXGrid3DT(igp, jg , kg );
  Vpmm = V * sin(TPhase + PhV);
  V = BXGrid3DT(igp, jg , kgp);
  PhV = PhBXGrid3DT(igp, jg , kgp);
  Vpmp = V * sin(TPhase + PhV);
  V = BXGrid3DT(igp, jgp, kg );
  PhV = PhBXGrid3DT(igp, jgp, kg );
  Vppm = V * sin(TPhase + PhV);
  V = BXGrid3DT(igp, jgp, kgp);
  PhV = PhBXGrid3DT(igp, jgp, kgp);
  Vppp = V * sin(TPhase + PhV);

  BxField3DT =   Vmmm * xfracc * yfracc * zfracc +
               + Vmmp * xfracc * yfracc * zfrac  +
               + Vmpm * xfracc * yfrac  * zfracc +
               + Vmpp * xfracc * yfrac  * zfrac  +
               + Vpmm * xfrac  * yfracc * zfracc +
               + Vpmp * xfrac  * yfracc * zfrac  +
               + Vppm * xfrac  * yfrac  * zfracc +
               + Vppp * xfrac  * yfrac  * zfrac;

  V = BYGrid3DT(ig , jg , kg );
  PhV = PhBYGrid3DT(ig , jg , kg );
  Vmmm = V * sin(TPhase + PhV);
  V = BYGrid3DT(ig , jg , kgp);
  PhV = PhBYGrid3DT(ig , jg , kgp);
  Vmmp = V * sin(TPhase + PhV);
  V = BYGrid3DT(ig , jgp, kg );
  PhV = PhBYGrid3DT(ig , jgp, kg );
  Vmpm = V * sin(TPhase + PhV);
  V = BYGrid3DT(ig , jgp, kgp);
  PhV = PhBYGrid3DT(ig , jgp, kgp);
  Vmpp = V * sin(TPhase + PhV);
  V = BYGrid3DT(igp, jg , kg );
  PhV = PhBYGrid3DT(igp, jg , kg );
  Vpmm = V * sin(TPhase + PhV);
  V = BYGrid3DT(igp, jg , kgp);
  PhV = PhBYGrid3DT(igp, jg , kgp);
  Vpmp = V * sin(TPhase + PhV);
  V = BYGrid3DT(igp, jgp, kg );
  PhV = PhBYGrid3DT(igp, jgp, kg );
  Vppm = V * sin(TPhase + PhV);
  V = BYGrid3DT(igp, jgp, kgp);
  PhV = PhBYGrid3DT(igp, jgp, kgp);
  Vppp = V * sin(TPhase + PhV);

  ByField3DT =   Vmmm * xfracc * yfracc * zfracc +
               + Vmmp * xfracc * yfracc * zfrac  +
               + Vmpm * xfracc * yfrac  * zfracc +
               + Vmpp * xfracc * yfrac  * zfrac  +
               + Vpmm * xfrac  * yfracc * zfracc +
               + Vpmp * xfrac  * yfracc * zfrac  +
               + Vppm * xfrac  * yfrac  * zfracc +
               + Vppp * xfrac  * yfrac  * zfrac;

  V = BZGrid3DT(ig , jg , kg );
  PhV = PhBZGrid3DT(ig , jg , kg );
  Vmmm = V * sin(TPhase + PhV);
  V = BZGrid3DT(ig , jg , kgp);
  PhV = PhBZGrid3DT(ig , jg , kgp);
  Vmmp = V * sin(TPhase + PhV);
  V = BZGrid3DT(ig , jgp, kg );
  PhV = PhBZGrid3DT(ig , jgp, kg );
  Vmpm = V * sin(TPhase + PhV);
  V = BZGrid3DT(ig , jgp, kgp);
  PhV = PhBZGrid3DT(ig , jgp, kgp);
  Vmpp = V * sin(TPhase + PhV);
  V = BZGrid3DT(igp, jg , kg );
  PhV = PhBZGrid3DT(igp, jg , kg );
  Vpmm = V * sin(TPhase + PhV);
  V = BZGrid3DT(igp, jg , kgp);
  PhV = PhBZGrid3DT(igp, jg , kgp);
  Vpmp = V * sin(TPhase + PhV);
  V = BZGrid3DT(igp, jgp, kg );
  PhV = PhBZGrid3DT(igp, jgp, kg );
  Vppm = V * sin(TPhase + PhV);
  V = BZGrid3DT(igp, jgp, kgp);
  PhV = PhBZGrid3DT(igp, jgp, kgp);
  Vppp = V * sin(TPhase + PhV);

  BzField3DT =   Vmmm * xfracc * yfracc * zfracc +
               + Vmmp * xfracc * yfracc * zfrac  +
               + Vmpm * xfracc * yfrac  * zfracc +
               + Vmpp * xfracc * yfrac  * zfrac  +
               + Vpmm * xfrac  * yfracc * zfracc +
               + Vpmp * xfrac  * yfracc * zfrac  +
               + Vppm * xfrac  * yfrac  * zfracc +
               + Vppp * xfrac  * yfrac  * zfrac;
}

///////////////////////////////////////////////////////////////////////////
//
// LOCAL FUNCTIONS
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LZfindPhases
//
// DESCRIPTION
//   Finds the betatron X and Y phases
//
// PARAMETERS
//   tm = reference to the TransMap member
//   mp = reference to the macro-particle main herd
//   i  = index to  the herd member
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LZfindPhases(MapBase &tm, MacroPart &mp, const Integer &i)
{
    Real angle, xval, xpval, yval, ypval;

    // Find normalized coordinates on ellipse transformed to circle:

    xval = (mp._x(i) - 1000. * tm._etaX * mp._dp_p(i)) / sqrt(tm._betaX);
    xpval = (mp._xp(i) - 1000. * tm._etaPX * mp._dp_p(i)) * sqrt(tm._betaX)
             + xval * tm._alphaX;

    yval = mp._y(i) / sqrt(tm._betaY);
    ypval = mp._yp(i) * sqrt(tm._betaY) + yval * tm._alphaY;

    angle = atan2(xpval, xval);
    if(angle < 0.) angle += Consts::twoPi;
    LZxPhase = angle;

    angle = atan2(ypval, yval);
    if(angle < 0.) angle += Consts::twoPi;
    LZyPhase = angle;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   LZdoTunes
//
// DESCRIPTION
//   Increments the tunes of a macroParticle herd. It is used as
//   the herd is stepped through transfer matrices.
//
// PARAMETERS
//   tm = reference to the TransMatrix1 member
//   mp = reference to the macro-particle herd
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void LZdoTunes(MapBase &tm, MacroPart &mp)
{
  Integer i;
  Real dPhase;
  Real phiCrit = Consts::pi;

  for(i = 1; i <= mp._nMacros; i++)
  {
    LZfindPhases(tm, mp, i);   // find new betatron phase

    // Horizontal Phase:

    dPhase = Particles::xPhaseOld(i) - LZxPhase;
    if(dPhase < (-Consts::pi)) dPhase += Consts::twoPi;
    //  Subtract out false 0 angle crossings from s.c. "kick-backs":
    if(dPhase > Consts::pi && Particles::xPhaseOld(i) > phiCrit)
      dPhase -= Consts::twoPi;
    Particles::xPhaseTot(i) += dPhase;
    Particles::xPhaseOld(i) = LZxPhase;

    // Vertical phase:

    dPhase = Particles::yPhaseOld(i) - LZyPhase;
    if(dPhase < (-Consts::pi)) dPhase += Consts::twoPi;
    //  Subtract out fake 0 angle crossings from s.c. "kick-backs"
    if(dPhase > Consts::pi && Particles::yPhaseOld(i) > phiCrit)
      dPhase -= Consts::twoPi;
    Particles::yPhaseTot(i) += dPhase;
    Particles::yPhaseOld(i) = LZyPhase;

    // Tune tracking length:

    Particles::lTuneTrack(i) += tm._length;
  }
}
