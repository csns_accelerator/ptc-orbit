/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   SC_CylCoords.cc
//
// AUTHOR
//   Jeff Holmes
//   ORNL, jzh@ornl.gov
//
// CREATED
//   05/2015
//
// DESCRIPTION
//   File for space charge by cylindrical coordinates
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>

#include "SC_CylCoords.h"


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS CylCoordsCalculator
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(CylCoordsCalculator, Object);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   CylCoordsCalculator
//
// INHERITANCE RELATIONSHIPS
//   CylCoordsCalculator -> CalculatorBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   Class for calculating space charge kicks
//   in cylindrical coordinates with a
//   circular conducting wall boundary
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------
void CylCoordsCalculator::_SetUp()
{
  int j, l, m;

  _pi    = 4.0 * atan2(1.0, 1.0);
  _twopi = _pi * 2.0;

  _dZ = _ZLength / double(_nZBins);
  _dR = _RBnd / double(_nRBins);
  _dTH = _twopi / double(_nThetas);

  double _dTHo2 = _dTH / 2.0;
  double twoDRsq = 2.0 * _dR * _dR;
  double Ratio;

  for(j = 0; j < _nRBins + 1; j++)
  {
    _Radius[j] = j * _dR;
  }

  for(l = 0; l < _nThetas; l++)
  {
    _Theta[l] = l * _dTH;
  }

  double RM = 0.0;
  double RP;
  for(j = 0; j < _nRBins; j++)
  {
    RP         = (_Radius[j + 1] + _Radius[j]) / 2.0;
    _dA[j]     = _dTHo2 * (RP - RM)
                        * (RP + RM);
    RM         = RP;
  }

  for(m = 0; m < _nModes + 1; m++)
  {
    _L[m][0] =  0.0;
    _D[m][0] =  1.0;
    _U[m][0] =  0.0;
    if(m == 0)
    {
      _D[m][0] =  4.0 / (_Radius[1] * _Radius[1]);
      _U[m][0] = -4.0 / (_Radius[1] * _Radius[1]);
    }

    for(j = 1; j < _nRBins; j++)
    {
      _L[m][j] = -(_Radius[j] + _Radius[j - 1]) / (_Radius[j] * twoDRsq);
      _U[m][j] = -(_Radius[j] + _Radius[j + 1]) / (_Radius[j] * twoDRsq);
      _D[m][j] = -_L[m][j] - _U[m][j] + m * m / (_Radius[j] * _Radius[j]);
    }
  }

  for(m = 0; m < _nModes + 1; m++)
  {
    for(j = 1; j < _nRBins; j++)
    {
      Ratio     = _L[m][j] / _D[m][j - 1];
      _D[m][j] -= Ratio * _U[m][j - 1];
      _L[m][j]  = Ratio;
    }
    if(m != 0)
    {
      _L[m][1] = 0.0;
    }
  }

  for(m = 0; m < _nModes + 1; m++)
  {
    for(l = 0; l < _nThetas; l++)
    {
      _cs[m][l] = cos(m * _Theta[l]);
      _sn[m][l] = sin(m * _Theta[l]);
    }
  }
}

//-------------------------------------------------------------------------
void CylCoordsCalculator::_DistributeParts(MacroPart& mp)
{
  int i, j, k, l, m;
  int Zkm, Zk, Zkp, Rj, THlm, THlp;

  double Zmax = mp._phi(1);
  double Zmin = mp._phi(1);
  double Z, Zfrac, R, TH, THfrac;
  double rhoSum, rhoFac, rhoFacm;

  for(k = 0; k < _nZBins; k++)
  {
    _ZDist[k] = 0;
  }

  for(l = 0; l < _nThetas; l++)
  {
    for(j = 0; j < _nRBins + 1; j++)
    {
      _RhoGrid[l][j] = 0.0;
    }
  }

  for(m = 0; m < 2 * _nModes + 1; m++)
  {
    for(j = 0; j < _nRBins + 1; j++)
    {
      _RhoModes[m][j] = 0.0;
    }
  }

  for(i = 1; i <= mp._nMacros; i++)
  {
    if(mp._phi(i) > Zmax) Zmax = mp._phi(i);
    if(mp._phi(i) < Zmin) Zmin = mp._phi(i);
  }

  _ZCenter = (Zmax + Zmin) / 2.0;
  _ZSum = 0.0;

  for(i = 1; i <= mp._nMacros; i++)
  {
    R = mp._x(i) * mp._x(i) + mp._y(i) * mp._y(i);
    R = pow(R, 0.5);
    if(R <= _RBnd)
    {
      _ZSum += 1.0;
      Z = mp._phi(i) - _ZCenter;
      if(Z >  _ZLength / 2.0) Z -= _ZLength;
      if(Z < -_ZLength / 2.0) Z += _ZLength;
      Z += _ZLength / 2.0;
      Zk = int(Z / _dZ + 0.5);
      Zfrac = Z / _dZ - double(Zk);
      if(Zk < 0) Zk = _nZBins - 1;
      if(Zk > _nZBins - 1) Zk = 0;
      Zkm = Zk - 1;
      if(Zkm < 0) Zkm = _nZBins - 1;
      Zkp = Zk + 1;
      if(Zkp > _nZBins - 1)  Zkp = 0;
      _ZDist[Zkm] += 0.5 * (0.5 - Zfrac) * (0.5 - Zfrac);
      _ZDist[Zk]  += 0.75 - Zfrac * Zfrac;
      _ZDist[Zkp] += 0.5 * (0.5 + Zfrac) * (0.5 + Zfrac);

      Rj = int(R / _dR + 0.5);
      if(Rj > _nRBins) Rj = _nRBins;
      TH = atan2(mp._y(i), mp._x(i));
      if(TH < 0.0) TH += _twopi;
      THlm = int(TH / _dTH);
      THfrac = (TH - _Theta[THlm]) / _dTH;
      if(THlm > _nThetas - 1)  THlm  = 0;
      THlp = THlm + 1;
      if(THlp > _nThetas - 1) THlp = 0;

      _RhoGrid[THlm][Rj] += (1.0 - THfrac);
      _RhoGrid[THlp][Rj] += THfrac;
    }
  }

  for(l = 0; l < _nThetas; l++)
  {
    for(j = 0; j < _nRBins; j++)
    {
      _RhoGrid[l][j] /= _dA[j];
    }
    _RhoGrid[l][_nRBins] /= _dA[_nRBins - 1];
  }

//////////////////////////////////////////////////////
/*
  ofstream fiorg("INFO_RhoGrid", ios::out);
  for(j = 0; j < _nRBins + 1; j++)
  {
    fiorg << j << "  " << _Radius[j] << "  ";
    for(l = 0; l < _nThetas; l++)
    {
      fiorg <<_RhoGrid[l][j] << "  ";
    }
    fiorg << "\n";
  }
  fiorg.close();
*/
//////////////////////////////////////////////////////

  for(j = 0; j < _nRBins + 1; j++)
  {
    rhoSum = 0.0;
    for(l = 0; l < _nThetas; l++)
    {
      rhoSum += _RhoGrid[l][j];
    }
    _RhoModes[0][j] = rhoSum / _nThetas;
  }

  rhoFac  = _Radius[1] / _Radius[2];
  rhoFacm = rhoFac * rhoFac;
  _RhoModes[0][1] = _RhoModes[0][0] +
                    rhoFacm * (_RhoModes[0][2] - _RhoModes[0][0]);

  rhoFacm = rhoFac;
  int m2m, m2p;
  for(m = 1; m < _nModes + 1; m++)
  {
    m2m = 2 * m - 1;
    m2p = 2 * m;

    _RhoModes[m2m][0] = 0.0;
    for(j = 1; j < _nRBins + 1; j++)
    {
      rhoSum = 0.0;
      for(l = 0; l < _nThetas; l++)
      {
        rhoSum += _RhoGrid[l][j] * _sn[m][l];
      }
      _RhoModes[m2m][j] = rhoSum * 2.0 / _nThetas;
    }
    _RhoModes[m2m][1] = _RhoModes[m2m][0] +
                        rhoFacm * (_RhoModes[m2m][2] - _RhoModes[m2m][0]);
 
    _RhoModes[m2p][0] = 0.0;
    for(j = 1; j < _nRBins + 1; j++)
    {
      rhoSum = 0.0;
      for(l = 0; l < _nThetas; l++)
      {
        rhoSum += _RhoGrid[l][j] * _cs[m][l];
      }
      _RhoModes[m2p][j] = rhoSum * 2.0 / _nThetas;
    }
    _RhoModes[m2p][1] = _RhoModes[m2p][0] +
                        rhoFacm * (_RhoModes[m2p][2] - _RhoModes[m2p][0]);
 
    rhoFacm *= rhoFac;
  }

//////////////////////////////////////////////////////
/*
  ofstream fiorm("INFO_RhoModes", ios::out);
  for(j = 0; j < _nRBins + 1; j++)
  {
    fiorm << j << "  " << _Radius[j] << "  ";
    for(m = 0; m < 2 * _nModes + 1; m++)
    {
      fiorm <<_RhoModes[m][j] << "  ";
    }
    fiorm << "\n";
  }
  fiorm.close();
*/
//////////////////////////////////////////////////////

}

//-------------------------------------------------------------------------
void CylCoordsCalculator::_GetPotential()
{
  int j, m;
  double potFac, potFacm;

  for(m = 0; m < 2 * _nModes + 1; m++)
  {
    for(j = 0; j < _nRBins + 1; j++)
    {
      _Pot[m][j] = 0.0;
    }
  }

  for(j = 1; j < _nRBins; j++)
  {
    _RhoModes[0][j] -= _L[0][j] * _RhoModes[0][j - 1];
  }

  int m2m, m2p;
  for(m = 1; m < _nModes + 1; m++)
  {
    m2m = 2 * m - 1;
    m2p = 2 * m;
    for(j = 1; j < _nRBins; j++)
    {
      _RhoModes[m2m][j] -= _L[m][j] * _RhoModes[m2m][j - 1];
      _RhoModes[m2p][j] -= _L[m][j] * _RhoModes[m2p][j - 1];
    }
  }

  for(j = _nRBins - 1; j >= 0; j--)
  {
    _Pot[0][j] = (_RhoModes[0][j] - _U[0][j] * _Pot[0][j + 1])
                 / _D[0][j];
  }
  potFac  = _Radius[1] / _Radius[2];
  potFacm = potFac * potFac;
  _Pot[0][1] = _Pot[0][0] +
               potFacm * (_Pot[0][2] - _Pot[0][0]);

  potFacm = potFac;
  for(m = 1; m < _nModes + 1; m++)
  {
    m2m = 2 * m - 1;
    m2p = 2 * m;
    for(j = _nRBins - 1; j > 0; j--)
    {
      _Pot[m2m][j] = (_RhoModes[m2m][j] - _U[m][j] * _Pot[m2m][j + 1])
                     / _D[m][j];
      _Pot[m2p][j] = (_RhoModes[m2p][j] - _U[m][j] * _Pot[m2p][j + 1])
                     / _D[m][j];
    }
    _Pot[m2m][1] = _Pot[m2m][0] +
                   potFacm * (_Pot[m2m][2] - _Pot[m2m][0]);
    _Pot[m2p][1] = _Pot[m2p][0] +
                   potFacm * (_Pot[m2p][2] - _Pot[m2p][0]);
 
    potFacm *= potFac;
  }

//////////////////////////////////////////////////////
/*
  ofstream fiopot("INFO_Pot", ios::out);
  for(j = 0; j < _nRBins + 1; j++)
  {
    fiopot << j << "  " << _Radius[j] << "  ";
    for(m = 0; m < 2 * _nModes + 1; m++)
    {
      fiopot <<_Pot[m][j] << "  ";
    }
    fiopot << "\n";
  }
  fiopot.close();
*/
//////////////////////////////////////////////////////

}

//-------------------------------------------------------------------------
void CylCoordsCalculator::_ApplyForce1(MacroPart& mp, double SCKick_Coeff)
{
  int i, m;
  int Zkm, Zk, Zkp, Rjm, Rj, Rjp;

  double Z, Zfrac, R, Rfrac, Rinv, TH;
  double Zfactor;

  double Rcfm, Rcf, Rcfp, dRcfm, dRcf, dRcfp;
  double csloc, snloc;

  double kickR, kickTH;
  double kickx, kicky;

  double Kick_Coeff = SCKick_Coeff / _ZSum;

  for(i = 1; i <= mp._nMacros; i++)
  {
    R = mp._x(i) * mp._x(i) + mp._y(i) * mp._y(i);
    R = pow(R, 0.5);
    if(R <= _RBnd)
    {
      Rinv = 0.0;
      if(R != 0.0) Rinv = 1 / R;
      Rj = int(R / _dR + 0.5);
      if(Rj < 1)           Rj = 1;
      if(Rj > _nRBins - 1) Rj = _nRBins - 1;
      Rfrac = R / _dR - double(Rj);
      Rjm = Rj - 1;
      Rjp = Rj + 1;

      TH = atan2(mp._y(i), mp._x(i));
      if(TH < 0.0) TH += _twopi;

      Rcfm  = 0.5 * (0.5 - Rfrac) * (0.5 - Rfrac);
      Rcf   = 0.75 - Rfrac * Rfrac;
      Rcfp  = 0.5 * (0.5 + Rfrac) * (0.5 + Rfrac);
      dRcfm = -(0.5 - Rfrac) / _dR;
      dRcf  = -2.0 * Rfrac / _dR;
      dRcfp =  (0.5 + Rfrac) / _dR;

      kickR  = -(dRcfm * _Pot[0][Rjm] +
                 dRcf  * _Pot[0][Rj]  +
                 dRcfp * _Pot[0][Rjp]);
      kickTH = 0.0;

      int m2m, m2p;
      for(m = 1; m < _nModes + 1; m++)
      {
        csloc = cos(m * TH);
        snloc = sin(m * TH);
        m2m = 2 * m - 1;
        m2p = 2 * m;
        kickR  -= (dRcfm * _Pot[m2m][Rjm] +
                   dRcf  * _Pot[m2m][Rj]  +
                   dRcfp * _Pot[m2m][Rjp]) * snloc;
        kickR  -= (dRcfm * _Pot[m2p][Rjm] +
                   dRcf  * _Pot[m2p][Rj]  +
                   dRcfp * _Pot[m2p][Rjp]) * csloc;
        kickTH -= (Rcfm * _Pot[m2m][Rjm] +
                   Rcf  * _Pot[m2m][Rj]  +
                   Rcfp * _Pot[m2m][Rjp]) * m * csloc * Rinv;
        kickTH += (Rcfm * _Pot[m2p][Rjm] +
                   Rcf  * _Pot[m2p][Rj]  +
                   Rcfp * _Pot[m2p][Rjp]) * m * snloc * Rinv;
      }
      csloc = cos(TH);
      snloc = sin(TH);
      kickx = csloc * kickR - snloc * kickTH;
      kicky = snloc * kickR + csloc * kickTH;

      Z = mp._phi(i) - _ZCenter;
      if(Z >  _ZLength / 2.0) Z -= _ZLength;
      if(Z < -_ZLength / 2.0) Z += _ZLength;
      Z += _ZLength / 2.0;
      Zk = int(Z / _dZ + 0.5);
      Zfrac = Z / _dZ - double(Zk);
      if(Zk < 0) Zk = _nZBins - 1;
      if(Zk > _nZBins - 1) Zk = 0;
      Zkm = Zk - 1;
      if(Zkm < 0) Zkm = _nZBins - 1;
      Zkp = Zk + 1;
      if(Zkp > _nZBins - 1)  Zkp = 0;
      Zfactor = 0.5 * (0.5 - Zfrac) * (0.5 - Zfrac) * _ZDist[Zkm] +
                (0.75 - Zfrac * Zfrac) * _ZDist[Zk] +
                0.5 * (0.5 + Zfrac) * (0.5 + Zfrac) * _ZDist[Zkp];
      Zfactor *= Kick_Coeff;

      mp._xp(i) +=  Zfactor * kickx;
      mp._yp(i) +=  Zfactor * kicky;
    }
  }
}

//-------------------------------------------------------------------------
void CylCoordsCalculator::_ApplyForce2(MacroPart& mp, double SCKick_Coeff)
{
  int i, j, l, m;
  int Zkm, Zk, Zkp, Rjm, Rj, Rjp, THlm, THl, THlp;

  double Z, Zfrac, R, Rfrac, Rinv, TH, THfrac;
  double Zfactor;

  double Rcfm, Rcf, Rcfp, dRcfm, dRcf, dRcfp;
  double THcfm, THcf, THcfp;

  double csloc, snloc;

  double kickR, kickTH;
  double kickx, kicky;

  double Kick_Coeff = SCKick_Coeff / _ZSum;

  for(j = 0; j < _nRBins + 1; j++)
  {
    R = _Radius[j];
    if(R <= _RBnd)
    {
      Rinv = 0.0;
      if(R != 0.0) Rinv = 1 / R;
      Rj = int(R / _dR + 0.5);
      if(Rj < 1)           Rj = 1;
      if(Rj > _nRBins - 1) Rj = _nRBins - 1;
      Rfrac = R / _dR - double(Rj);
      Rjm = Rj - 1;
      Rjp = Rj + 1;

      Rcfm  = 0.5 * (0.5 - Rfrac) * (0.5 - Rfrac);
      Rcf   = 0.75 - Rfrac * Rfrac;
      Rcfp  = 0.5 * (0.5 + Rfrac) * (0.5 + Rfrac);
      dRcfm = -(0.5 - Rfrac) / _dR;
      dRcf  = -2.0 * Rfrac / _dR;
      dRcfp =  (0.5 + Rfrac) / _dR;

      for(l = 0; l < _nThetas; l++)
      {
        kickR  = -(dRcfm * _Pot[0][Rjm] +
                   dRcf  * _Pot[0][Rj]  +
                   dRcfp * _Pot[0][Rjp]);
        kickTH = 0.0;

        TH = _Theta[l];
        if(TH < 0.0) TH += _twopi;

        int m2m, m2p;
        for(m = 1; m < _nModes + 1; m++)
        {
          csloc = cos(m * TH);
          snloc = sin(m * TH);
          m2m = 2 * m - 1;
          m2p = 2 * m;
          kickR  -= (dRcfm * _Pot[m2m][Rjm] +
                     dRcf  * _Pot[m2m][Rj]  +
                     dRcfp * _Pot[m2m][Rjp]) * snloc;
          kickR  -= (dRcfm * _Pot[m2p][Rjm] +
                     dRcf  * _Pot[m2p][Rj]  +
                     dRcfp * _Pot[m2p][Rjp]) * csloc;
          kickTH -=  (Rcfm * _Pot[m2m][Rjm] +
                      Rcf  * _Pot[m2m][Rj]  +
                      Rcfp * _Pot[m2m][Rjp]) * m * csloc * Rinv;
          kickTH +=  (Rcfm * _Pot[m2p][Rjm] +
                      Rcf  * _Pot[m2p][Rj]  +
                      Rcfp * _Pot[m2p][Rjp]) * m * snloc * Rinv;
        }

        _kRGrid[l][j]  = kickR;
        _kTHGrid[l][j] = kickTH;
        csloc = cos(TH);
        snloc = sin(TH);
        _kxGrid[l][j] = csloc * kickR - snloc * kickTH;
        _kyGrid[l][j] = snloc * kickR + csloc * kickTH;
      }
    }
  }

  for(l = 0; l < _nThetas; l++)
  {
    TH = _Theta[l];
    if(TH < 0.0) TH += _twopi;
    csloc = cos(TH);
    snloc = sin(TH);
    kickR = _kRGrid[l][0];
    kickTH = (_Radius[2] * _kTHGrid[l][1] -
              _Radius[1] * _kTHGrid[l][2]) /
             (_Radius[2] - _Radius[1]);
    _kTHGrid[l][0] = kickTH;
    _kxGrid[l][0] = csloc * kickR - snloc * kickTH;
    _kyGrid[l][0] = snloc * kickR + csloc * kickTH;
  }

//////////////////////////////////////////////////////
/*
  ofstream fiokick("INFO_kicks", ios::out);

  for(j = 0; j < _nRBins + 1; j++)
  {
    fiokick << j << "  " << _Radius[j] << "  ";
    for(l = 0; l < _nThetas; l++)
    {
        fiokick << _Theta[l]      << "  "
                << _kRGrid[l][j]  << "  "
                << _kTHGrid[l][j] << "  "
                << _kxGrid[l][j]  << "  "
                << _kyGrid[l][j]  << "  ";
    }
    fiokick << "\n";
  }
  fiokick.close();
*/
//////////////////////////////////////////////////////

  for(i = 1; i <= mp._nMacros; i++)
  {
    R = mp._x(i) * mp._x(i) + mp._y(i) * mp._y(i);
    R = pow(R, 0.5);
    if(R <= _RBnd)
    {
      Rj = int(R / _dR + 0.5);
      if(Rj < 1)           Rj = 1;
      if(Rj > _nRBins - 1) Rj = _nRBins - 1;
      Rfrac = R / _dR - double(Rj);
      Rjm = Rj - 1;
      Rjp = Rj + 1;

      TH = atan2(mp._y(i), mp._x(i));
      if(TH < 0.0) TH += _twopi;
      THl = int(TH / _dTH + 0.5);
      THfrac = TH / _dTH - double(THl);
      if(THl < 0) THl = _nThetas - 1;
      if(THl > _nThetas - 1) THl = 0;
      THlm = THl - 1;
      if(THlm < 0) THlm = _nThetas - 1;
      THlp = THl + 1;
      if(THlp > _nThetas - 1) THlp = 0;

      Rcfm  = 0.5 * (0.5 - Rfrac)  * (0.5 - Rfrac);
      Rcf   = 0.75 - Rfrac * Rfrac;
      Rcfp  = 0.5 * (0.5 + Rfrac)  * (0.5 + Rfrac);

      THcfm = 0.5 * (0.5 - THfrac) * (0.5 - THfrac);
      THcf  = 0.75 - THfrac * THfrac;
      THcfp = 0.5 * (0.5 + THfrac) * (0.5 + THfrac);

      kickx = Rcfm * THcfm * _kxGrid[THlm][Rjm] +
              Rcf  * THcfm * _kxGrid[THlm][Rj]  +
              Rcfp * THcfm * _kxGrid[THlm][Rjp] +
              Rcfm * THcf  * _kxGrid[THl][Rjm]  +
              Rcf  * THcf  * _kxGrid[THl][Rj]   +
              Rcfp * THcf  * _kxGrid[THl][Rjp]  +
              Rcfm * THcfp * _kxGrid[THlp][Rjm] +
              Rcf  * THcfp * _kxGrid[THlp][Rj]  +
              Rcfp * THcfp * _kxGrid[THlp][Rjp];
              
      kicky = Rcfm * THcfm * _kyGrid[THlm][Rjm] +
              Rcf  * THcfm * _kyGrid[THlm][Rj]  +
              Rcfp * THcfm * _kyGrid[THlm][Rjp] +
              Rcfm * THcf  * _kyGrid[THl][Rjm]  +
              Rcf  * THcf  * _kyGrid[THl][Rj]   +
              Rcfp * THcf  * _kyGrid[THl][Rjp]  +
              Rcfm * THcfp * _kyGrid[THlp][Rjm] +
              Rcf  * THcfp * _kyGrid[THlp][Rj]  +
              Rcfp * THcfp * _kyGrid[THlp][Rjp];

      Z = mp._phi(i) - _ZCenter;
      if(Z >  _ZLength / 2.0) Z -= _ZLength;
      if(Z < -_ZLength / 2.0) Z += _ZLength;
      Z += _ZLength / 2.0;
      Zk = int(Z / _dZ + 0.5);
      Zfrac = Z / _dZ - double(Zk);
      if(Zk < 0) Zk = _nZBins - 1;
      if(Zk > _nZBins - 1) Zk = 0;
      Zkm = Zk - 1;
      if(Zkm < 0) Zkm = _nZBins - 1;
      Zkp = Zk + 1;
      if(Zkp > _nZBins - 1)  Zkp = 0;
      Zfactor = 0.5 * (0.5 - Zfrac) * (0.5 - Zfrac) * _ZDist[Zkm] +
                (0.75 - Zfrac * Zfrac) * _ZDist[Zk] +
                0.5 * (0.5 + Zfrac) * (0.5 + Zfrac) * _ZDist[Zkp];
      Zfactor *= Kick_Coeff;

      mp._xp(i) +=  Zfactor * kickx;
      mp._yp(i) +=  Zfactor * kicky;
    }
  }
}
