//# Library            : ORBIT
//# File               : FFT3DSpaceCharge.cc
//# Original code      : Jeff Holmes, Slava Danilov, John Galambos

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "FFT3DSpaceCharge_MPI.h"

extern Array(ObjectPtr) syncP;

// Constructor

ORBIT_FFT3DSpaceCharge* ORBIT_FFT3DSpaceCharge::m_Instance=0;

ORBIT_FFT3DSpaceCharge::ORBIT_FFT3DSpaceCharge(int nXBins, int nYBins, int nZBins, 
                                               int minBunchSize)
{

  setvbuf( stdout , NULL, _IOLBF, BUFSIZ);

  MacroPartDistributor* PartDistr;
  PartDistr = MacroPartDistributor::GetMacroPartDistributor();

  nXBins_ = nXBins;
  nYBins_ = nYBins;
  nZBins_ = nZBins;
  minBunchSize_ = minBunchSize;
  maxBunchSize_ = 1;
  

    MPI_Initialized( &iMPIini);

    size_MPI = 1;
    rank_MPI = 0;

    if (iMPIini) {
      MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
      MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
    }

     if( size_MPI > nZBins_ ) {
            std::cerr << " You use too many CPU's." 
                 << std::endl
                 << " N(CPU) > longitudinal grid size" 
                 << std::endl 
                 << " Stop.";
            _finalize_MPI();
            exit(1);
     }

  //define longitudinal size of bunches
  phi_min_  = PartDistr->getPhiMin();
  phi_max_  = PartDistr->getPhiMax();
  phi_step_ = PartDistr->getPhiStep();

  //array of particle's position and fraction coefficient
  xBin_ = new int[maxBunchSize_];
  yBin_ = new int[maxBunchSize_];
  LPositionIndex_ = new int[maxBunchSize_];
  xFractBin_ = new double[maxBunchSize_];
  yFractBin_ = new double[maxBunchSize_]; 
  fractLPosition_ = new double[maxBunchSize_];

  //define longitudinal position indexes belonged to this CPU
  longIndex_min_ = PartDistr->getSliceIndMin();
  longIndex_max_ = PartDistr->getSliceIndMax();

  //number of the longitudinal slices belonged to this CPU
  //there are two additional slices - left and right, because
  //of 3-points interpolation scheme
  nSlices_ = (longIndex_max_ - longIndex_min_ +1)+2;

  //Allocate memory for the 3D charge distrubution
  rho3D_ = new double**[nSlices_];
  int iz,ix,iy;
  for( iz=0 ; iz < nSlices_ ; iz++){
    rho3D_[iz] = new double*[nXBins_+1];
     for( ix=0 ; ix <= nXBins_  ; ix++){
      rho3D_[iz][ix] = new double[nYBins_+1];
       for( iy=0 ; iy <= nYBins_  ; iy++){
         rho3D_[iz][ix][iy] = 0.0;
       }
     }
  }

  //Allocate memory for the 3D potential
  phiSC_ = new double**[nSlices_];
  for( iz=0 ; iz < nSlices_ ; iz++){
    phiSC_[iz] = new double*[nXBins_+1];
     for( ix=0 ; ix <= nXBins_  ; ix++){
      phiSC_[iz][ix] = new double[nYBins_+1];
       for( iy=0 ; iy <= nYBins_  ; iy++){
         phiSC_[iz][ix][iy] = 0.0;
       }
     }
  }

  //Allocate memory for exchange buffer for intiger variables
  buff_int = (int *) malloc ( sizeof(int)*4);
  buff_int_MPI = (int *) malloc ( sizeof(int)*4); 
  
  //Allocate memory for exchange buffer for XY-plane's SC distribution
  buff_double_size_ = (nXBins_+3)*(nYBins_+3);
  buff_double = (double *) malloc ( sizeof(double)*buff_double_size_);
  buff_double_MPI = (double *) malloc ( sizeof(double)*buff_double_size_);

  //Allocate memory for FFT
  in_   = (FFTW_REAL *) new char[nXBins_ * nYBins_ * sizeof(FFTW_REAL)];
  in1_  = (FFTW_REAL *) new char[nXBins_ * nYBins_ * sizeof(FFTW_REAL)];
  out_  =(FFTW_COMPLEX *) new char[nXBins_ * (nYBins_/2+1) * sizeof(FFTW_COMPLEX)];
  out1_ =(FFTW_COMPLEX *) new char[nXBins_ * (nYBins_/2+1) * sizeof(FFTW_COMPLEX)];
  planForward_  = rfftw2d_create_plan(nXBins_, nYBins_, FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE);
  planBackward_ = rfftw2d_create_plan(nXBins_, nYBins_, FFTW_COMPLEX_TO_REAL, FFTW_ESTIMATE);

  //Define index of region to operate with 3D distribution from the previous run
  //to set pho3D_ to zero value
  ixMin_old_ = 0;
  ixMax_old_ = nXBins_;
  iyMin_old_ = 0;
  iyMax_old_ = nYBins_;
  
}

//get instance of itself
ORBIT_FFT3DSpaceCharge* ORBIT_FFT3DSpaceCharge::getORBIT_FFT3DSpaceCharge(
		        int nXBins, int nYBins, int nZBins, int minBuchSize)
{
 if(!m_Instance){
    m_Instance = new ORBIT_FFT3DSpaceCharge(nXBins, nYBins, 
                                            nZBins, minBuchSize) ;
 }
 return  m_Instance;
}

//get instance of itself
ORBIT_FFT3DSpaceCharge* ORBIT_FFT3DSpaceCharge::getORBIT_FFT3DSpaceCharge(){
 if(!m_Instance){
   std::cerr << " You should create ORBIT_FFT3DSpaceCharge* object first!\n" 
             << " Stop.";
   exit(1);
 }
 return  m_Instance;
}

// Destructor
ORBIT_FFT3DSpaceCharge::~ORBIT_FFT3DSpaceCharge()
{
  delete [] xBin_ ;
  delete [] yBin_ ;
  delete [] LPositionIndex_ ;
  delete [] xFractBin_ ;
  delete [] yFractBin_ ;
  delete [] fractLPosition_ ;

  int iz,ix;
  for( iz=0 ; iz < nSlices_ ; iz++){
      for( ix=0 ; ix <= nXBins_  ; ix++){
        delete [] rho3D_[iz][ix];
      }   
      delete [] rho3D_[iz];
  }  
  delete [] rho3D_;

  for( iz=0 ; iz < nSlices_ ; iz++){
      for( ix=0 ; ix <= nXBins_  ; ix++){
        delete [] phiSC_[iz][ix];
      }   
      delete [] phiSC_[iz];
  }  
  delete [] phiSC_;


  free(buff_int);
  free(buff_int_MPI);
 
  free(buff_double);
  free(buff_double_MPI);

  //Deallocate memory for FFT
  rfftwnd_destroy_plan(planForward_);
  rfftwnd_destroy_plan(planBackward_);
  delete [] in_;
  delete [] in1_;
  delete [] out_;
  delete [] out1_;

}

//Resize inner arrays if the longitudinal size has been changed
void ORBIT_FFT3DSpaceCharge::_resizeLongAndPartArrays(int nPart)
{
  //redefine size of the particles' information arrays
  int nPartBacklash;
  nPartBacklash = (int) (maxBunchSize_*0.05);
  if(nPartBacklash < 1000) nPartBacklash = 1000;
  if(nPart < maxBunchSize_ - nPartBacklash ||  nPart > maxBunchSize_){
    maxBunchSize_ = (int) (nPart + 0.5*nPartBacklash);

    //delete old
     delete [] xBin_ ;
     delete [] yBin_ ;
     delete [] LPositionIndex_ ;
     delete [] xFractBin_ ;
     delete [] yFractBin_ ;
     delete [] fractLPosition_ ;
    //create new
     xBin_ = new int[maxBunchSize_];
     yBin_ = new int[maxBunchSize_];
     LPositionIndex_ = new int[maxBunchSize_];
     xFractBin_ = new double[maxBunchSize_];
     yFractBin_ = new double[maxBunchSize_]; 
     fractLPosition_ = new double[maxBunchSize_];    
  }

  //redefine size of the arrays with information about longitudinal slices
  int iz,ix,iy;
  MacroPartDistributor* PartDistr;
  PartDistr = MacroPartDistributor::GetMacroPartDistributor();

  if(longIndex_min_ != PartDistr->getSliceIndMin() || 
     longIndex_max_ != PartDistr->getSliceIndMax()   ){

      //delete old
      for( iz=0 ; iz < nSlices_ ; iz++){
          for( ix=0 ; ix <= nXBins_  ; ix++){
            delete [] rho3D_[iz][ix];
          }   
          delete [] rho3D_[iz];
      }  
      delete [] rho3D_;
     
      for( iz=0 ; iz < nSlices_ ; iz++){
          for( ix=0 ; ix <= nXBins_  ; ix++){
            delete [] phiSC_[iz][ix];
          }   
          delete [] phiSC_[iz];
      }  
      delete [] phiSC_;

      //create new arrays

      //define longitudinal position indexes belonged to this CPU
      longIndex_min_ = PartDistr->getSliceIndMin();
      longIndex_max_ = PartDistr->getSliceIndMax();

      //number of the longitudinal slices belonged to this CPU
      //there are two additional slices - left and right, because
      //of 3-points interpolation scheme
      nSlices_ = (longIndex_max_ - longIndex_min_ +1)+2;

      //Allocate memory for the 3D charge distrubution
      rho3D_ = new double**[nSlices_];
      for( iz=0 ; iz < nSlices_ ; iz++){
        rho3D_[iz] = new double*[nXBins_+1];
        for( ix=0 ; ix <= nXBins_  ; ix++){
        rho3D_[iz][ix] = new double[nYBins_+1];
          for( iy=0 ; iy <= nYBins_  ; iy++){
            rho3D_[iz][ix][iy] = 0.0;
          }
        }
      }

      //Allocate memory for the 3D potential
      phiSC_ = new double**[nSlices_];
      for( iz=0 ; iz < nSlices_ ; iz++){
        phiSC_[iz] = new double*[nXBins_+1];
         for( ix=0 ; ix <= nXBins_  ; ix++){
          phiSC_[iz][ix] = new double[nYBins_+1];
           for( iy=0 ; iy <= nYBins_  ; iy++){
             phiSC_[iz][ix][iy] = 0.0;
           }
         }
      }
  }
}


//Bin macroparticles
void ORBIT_FFT3DSpaceCharge::_binMacroParticles(MacroPart &mp,
                                                ORBIT_Boundary& boundary,
                                                double shift_X, double shift_Y)
{


  MacroPartDistributor* PartDistr;
  PartDistr = MacroPartDistributor::GetMacroPartDistributor();

  PartDistr->distributeParticles(mp);

  int nPart =mp._nMacros ;
  bunchSize_ = nPart;
  nPartAll_ = mp._globalNMacros;

  if( nPartAll_ < minBunchSize_ ){ return;}

  _resizeLongAndPartArrays(nPart); 

  boundary.defineExtXYgrid(&xGridMin_,&xGridMax_, 
                           &yGridMin_,&yGridMax_,
                           &dx_      ,&dy_      ); 

  xGrid_ = boundary.getXgrid();
  yGrid_ = boundary.getYgrid();

  phi_min_  = PartDistr->getPhiMin();
  phi_max_  = PartDistr->getPhiMax();
  phi_step_ = PartDistr->getPhiStep();

  int ip,ip1;
  int iX,iY,iCT;
  double x,y,phi;

   ixMin_ = nXBins_;
   ixMax_ = 0;
   iyMin_ = nYBins_;
   iyMax_ = 0;  

   //std::cout<<"debug dx_ dy_ phi_step_ ="<< dx_ <<" "<< dy_ <<" "<< phi_step_ <<"\n";

   //Loading Manager definition (this is singleton)
   NodeLoadingManager* LoadManager = NodeLoadingManager::GetNodeLoadingManager();
 
  //Loading Manager timing start
   LoadManager->measureStart();

   for(ip = 0; ip < nPart; ip++){
     ip1 = ip + 1;
     xBin_[ip] = -1;
     x   = mp._x(ip1) + shift_X ;
     y   = mp._y(ip1) + shift_Y;
     phi = mp._phi(ip1); 
 
     // is particle inside the boundary or not  

     if( boundary.isInside(x,y) < 0 ) {
          xBin_[ip] = -1;
          continue;                 
     }

     iX  = int ( (x - xGridMin_)/dx_      + 0.5 );
     iY  = int ( (y - yGridMin_)/dy_      + 0.5 );
     iCT = int ( (phi - phi_min_  )/phi_step_ + 0.5 );
  
     xBin_[ip] = iX;
     yBin_[ip] = iY;
     LPositionIndex_[ip] = iCT;

     xFractBin_[ip]      = (x - xGrid_[iX])/dx_;
     yFractBin_[ip]      = (y - yGrid_[iY])/dy_;
     fractLPosition_[ip] = (phi - (iCT *phi_step_ + phi_min_))/phi_step_;
 
       //finding limits on ix and iy
       if(iX  < ixMin_) ixMin_ = iX;
       if(iX  > ixMax_) ixMax_ = iX;
       if(iY  < iyMin_) iyMin_ = iY;
       if(iY  > iyMax_) iyMax_ = iY;      
   
   } 

   //Loading Manager timing stop
   LoadManager->measureStop();

 //set min and max indexes for the XY-plane
   if(iMPIini > 0){
     buff_int[0] = - ixMin_;
     buff_int[1] =   ixMax_;
     buff_int[2] = - iyMin_;
     buff_int[3] =   iyMax_;
     MPI_Allreduce(buff_int,buff_int_MPI, 4 , MPI_INT, MPI_MAX, MPI_COMM_WORLD);
     ixMin_ = - buff_int_MPI[0]-1;
     ixMax_ =   buff_int_MPI[1]+1;
     iyMin_ = - buff_int_MPI[2]-1;
     iyMax_ =   buff_int_MPI[3]+1;
   }

   if( ixMin_ < 0      ) ixMin_ = 0;
   if( ixMax_ > nXBins_) ixMax_ = nXBins_;
   if( iyMin_ < 0      ) iyMin_ = 0;
   if( iyMax_ > nYBins_) iyMax_ = nYBins_;
   
   //Set 3D pho equaling to zero
   for(iCT = 0; iCT < nSlices_; iCT++){
     for(iX = ixMin_old_; iX <= ixMax_old_ ; iX++){
      for(iY = iyMin_old_; iY <= iyMax_old_ ; iY++){
	rho3D_[iCT][iX][iY] = 0.0;
      }
     }
   }

   ixMin_old_ = ixMin_;
   ixMax_old_ = ixMax_;
   iyMin_old_ = iyMin_;
   iyMax_old_ = iyMax_;
   
   //Bins the macroparticles in all 3 dimensions
   double Wxm, Wx0, Wxp, Wym, Wy0, Wyp, Wctm, Wct0, Wctp; 
   int iCTp,iCTm;   

   //Loading Manager timing start
   LoadManager->measureStart();

   for(ip = 0; ip < nPart; ip++){
    if( xBin_[ip] >= 0) {
      iX = xBin_[ip];
      iY = yBin_[ip];
      iCT =  LPositionIndex_[ip];

      Wxm = 0.5 * (0.5 - xFractBin_[ip]) * (0.5 - xFractBin_[ip]);
      Wx0 = 0.75 - xFractBin_[ip] * xFractBin_[ip];
      Wxp = 0.5 * (0.5 + xFractBin_[ip]) * (0.5 + xFractBin_[ip]);
      Wym = 0.5 * (0.5 - yFractBin_[ip]) * (0.5 - yFractBin_[ip]);
      Wy0 = 0.75 - yFractBin_[ip] * yFractBin_[ip];
      Wyp = 0.5 * (0.5 + yFractBin_[ip]) * (0.5 + yFractBin_[ip]);
      Wctm = 0.5 * (0.5 - fractLPosition_[ip]) * (0.5 - fractLPosition_[ip]);
      Wct0 = 0.75 - fractLPosition_[ip] * fractLPosition_[ip];
      Wctp = 0.5 * (0.5 + fractLPosition_[ip]) * (0.5 + fractLPosition_[ip]);

      //make iCT for this CPU, we add 1 because we 
      //use two additional slices - left and right
      iCT = iCT - longIndex_min_ +1; 
      iCTp = iCT +1;
      iCTm = iCT -1;

      //std::cout<<"debug ip ="<<ip<<" iX iY iCT ="<< iX <<" "<< iY <<" "<<  iCT <<"\n";
      //std::cout<<"debug freactions ="<< xFractBin_[ip] <<" "<< yFractBin_[ip] <<" "<<fractLPosition_[ip]<<"\n";
      //std::cout<<"debug ====================================\n";

     rho3D_[iCTm][iX-1][iY-1] += Wxm * Wym * Wctm;
     rho3D_[iCTm][iX  ][iY-1] += Wx0 * Wym * Wctm;
     rho3D_[iCTm][iX+1][iY-1] += Wxp * Wym * Wctm;
     rho3D_[iCTm][iX-1][iY  ] += Wxm * Wy0 * Wctm;
     rho3D_[iCTm][iX  ][iY  ] += Wx0 * Wy0 * Wctm;
     rho3D_[iCTm][iX+1][iY  ] += Wxp * Wy0 * Wctm;
     rho3D_[iCTm][iX-1][iY+1] += Wxm * Wyp * Wctm;
     rho3D_[iCTm][iX  ][iY+1] += Wx0 * Wyp * Wctm;
     rho3D_[iCTm][iX+1][iY+1] += Wxp * Wyp * Wctm;
     rho3D_[ iCT][iX-1][iY-1] += Wxm * Wym * Wct0;
     rho3D_[ iCT][iX  ][iY-1] += Wx0 * Wym * Wct0;
     rho3D_[ iCT][iX+1][iY-1] += Wxp * Wym * Wct0;
     rho3D_[ iCT][iX-1][iY  ] += Wxm * Wy0 * Wct0;
     rho3D_[ iCT][iX  ][iY  ] += Wx0 * Wy0 * Wct0;
     rho3D_[ iCT][iX+1][iY  ] += Wxp * Wy0 * Wct0;
     rho3D_[ iCT][iX-1][iY+1] += Wxm * Wyp * Wct0;
     rho3D_[ iCT][iX  ][iY+1] += Wx0 * Wyp * Wct0;
     rho3D_[ iCT][iX+1][iY+1] += Wxp * Wyp * Wct0;
     rho3D_[iCTp][iX-1][iY-1] += Wxm * Wym * Wctp;
     rho3D_[iCTp][iX  ][iY-1] += Wx0 * Wym * Wctp;
     rho3D_[iCTp][iX+1][iY-1] += Wxp * Wym * Wctp;
     rho3D_[iCTp][iX-1][iY  ] += Wxm * Wy0 * Wctp;
     rho3D_[iCTp][iX  ][iY  ] += Wx0 * Wy0 * Wctp;
     rho3D_[iCTp][iX+1][iY  ] += Wxp * Wy0 * Wctp;
     rho3D_[iCTp][iX-1][iY+1] += Wxm * Wyp * Wctp;
     rho3D_[iCTp][iX  ][iY+1] += Wx0 * Wyp * Wctp;
     rho3D_[iCTp][iX+1][iY+1] += Wxp * Wyp * Wctp;  
    }
   }

   //Loading Manager timing stop
   LoadManager->measureStop();

   //conform the space charge distribution on all of CPUs

   _conformSCDistribution();

   //std::cout<<"debug ORBIT_FFT3DSpaceCharge::_binMacroParticles point5 done.\n";
   //debug =====start============
   //for(iCT=0;iCT < nSlices_ ; iCT++){
   //  std::cout<<" Slice sl="<<iCT<<"================\n";
   //  for( iX = 0; iX < nXBins_; iX++){
   //    std::cout<<"  ix="<< iX<<" pho=";
   //    for( iY = 0; iY < nYBins_; iY++){
   //     std::cout<<rho3D_[iCT][iX][iY]<<" ";
   //    }
   //    std::cout<<"\n";
   //  }
   //} 
   //debug =====start============

//============================================================================
}

//Conforms the space charge distribution on all of CPUs
void ORBIT_FFT3DSpaceCharge::_conformSCDistribution()
{
  int ix,iy, counter, iSlice0, iSlice1;

  if(size_MPI == 1) {
    for(ix = ixMin_ ; ix <= ixMax_; ix++){
    for(iy = iyMin_ ; iy <= iyMax_; iy++){

      //add right and left slices to the second right and left one
      rho3D_[2][ix][iy] += rho3D_[nSlices_-1][ix][iy];
      rho3D_[nSlices_-1][ix][iy] = rho3D_[2][ix][iy];
      rho3D_[nSlices_-3][ix][iy] += rho3D_[0][ix][iy];
      rho3D_[0][ix][iy] = rho3D_[nSlices_-3][ix][iy];

      // Periodic boundary conditions
      rho3D_[1][ix][iy] += rho3D_[nSlices_-2][ix][iy];
      rho3D_[nSlices_-2][ix][iy] = rho3D_[1][ix][iy];  

    }}  
    return;
  }

  MPI_Status statusMPI;

  //clockwise sending - add right slice to the second left one at the next CPU
  int send_to_ID, recive_from_ID;

  send_to_ID = rank_MPI +1;
  recive_from_ID = rank_MPI - 1;
  if( rank_MPI == (size_MPI-1)) { send_to_ID = 0; }
  if( rank_MPI == 0           ) { recive_from_ID =(size_MPI-1); } 

  int exchange_size = (ixMax_ - ixMin_+1)*(iyMax_ - iyMin_ + 1);

  if( exchange_size > buff_double_size_ ){
   std::cerr << "ORBIT_FFT3DSpaceCharge::_conformSCDistribution ====STOP=== \n "
        << " Exchange buffer size too small. \n" 
        << " Stop.";
        _finalize_MPI();
        exit(1);    
  }

  counter = 0;
  iSlice1 = nSlices_ -1;  
  for(ix = ixMin_ ; ix <= ixMax_; ix++){
  for(iy = iyMin_ ; iy <= iyMax_; iy++){
    buff_double[counter] = rho3D_[iSlice1][ix][iy];
    counter++;
  }}
  
  MPI_Sendrecv(buff_double    , exchange_size, MPI_DOUBLE, send_to_ID    , 1, 
               buff_double_MPI, exchange_size, MPI_DOUBLE, recive_from_ID, 1,
               MPI_COMM_WORLD , &statusMPI);

  if(rank_MPI == 0){
    iSlice1 = 2;
  }
  else{
    iSlice1 = 1;
  }


  counter = 0;
  for(ix = ixMin_ ; ix <= ixMax_; ix++){
  for(iy = iyMin_ ; iy <= iyMax_; iy++){
    rho3D_[iSlice1][ix][iy] += buff_double_MPI[counter];
    counter++;
  }}

  //anti-clockwise sending - add left slice to the second right one at the next CPU
  counter = 0;
  iSlice0 = 0;  
  for(ix = ixMin_ ; ix <= ixMax_; ix++){
  for(iy = iyMin_ ; iy <= iyMax_; iy++){
    buff_double[counter] = rho3D_[iSlice0][ix][iy];
    counter++;
  }}

  MPI_Sendrecv(buff_double    , exchange_size, MPI_DOUBLE, recive_from_ID   , 1, 
               buff_double_MPI, exchange_size, MPI_DOUBLE, send_to_ID       , 1,
               MPI_COMM_WORLD , &statusMPI);

  if(rank_MPI == (size_MPI-1)){
    iSlice0 = nSlices_ -3;
  }
  else{
    iSlice0 = nSlices_ -2;
  }

  counter = 0;
  for(ix = ixMin_ ; ix <= ixMax_; ix++){
  for(iy = iyMin_ ; iy <= iyMax_; iy++){
    rho3D_[iSlice0][ix][iy] += buff_double_MPI[counter];
    counter++;
  }}


 //Satisfy periodic conditions
  if(rank_MPI == (size_MPI-1)){

    counter = 0;
    iSlice1 = nSlices_ -2;  
    for(ix = ixMin_ ; ix <= ixMax_; ix++){
      for(iy = iyMin_ ; iy <= iyMax_; iy++){
	buff_double[counter] = rho3D_[iSlice1][ix][iy];
	counter++;
      }}
    MPI_Send(buff_double, exchange_size, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD);  
  }
  
  if(rank_MPI == 0){

    MPI_Recv(buff_double_MPI, exchange_size, MPI_DOUBLE,size_MPI-1, 2,MPI_COMM_WORLD, &statusMPI);

    iSlice1 = 1;
    counter = 0;
    for(ix = ixMin_ ; ix <= ixMax_; ix++){
      for(iy = iyMin_ ; iy <= iyMax_; iy++){
	rho3D_[iSlice1][ix][iy] += buff_double_MPI[counter];
        buff_double[counter] = rho3D_[iSlice1][ix][iy];
	counter++;
      }}
    MPI_Send(buff_double, exchange_size, MPI_DOUBLE, size_MPI-1, 3, MPI_COMM_WORLD);  
  }

  if(rank_MPI == (size_MPI-1)){

    MPI_Recv(buff_double_MPI, exchange_size, MPI_DOUBLE,0 , 3 ,MPI_COMM_WORLD, &statusMPI);

    iSlice1 = nSlices_ -2;
    counter = 0;
    for(ix = ixMin_ ; ix <= ixMax_; ix++){
      for(iy = iyMin_ ; iy <= iyMax_; iy++){
	rho3D_[iSlice1][ix][iy] = buff_double_MPI[counter];
	counter++;
      }}
  }
}

//Calculate FFT of the SC distribution and the potential
void ORBIT_FFT3DSpaceCharge::_claculatePotential( ORBIT_Boundary& boundary)
{
   //Loading Manager definition (this is singleton)
   NodeLoadingManager* LoadManager = NodeLoadingManager::GetNodeLoadingManager();

   //Loading Manager timing start
   LoadManager->measureStart();

  //get the Green function FFT
  out_green_ = boundary.getOutFFTGreenF();
  
  //do FFT and calculate potential on all longitudinal slices
  int iCT;
  int i,j, index;

  double denom = 1.0 / (nXBins_ * nYBins_);

  for(i = 0; i < nXBins_ * nYBins_; i++){ in_[i] = 0.0;}

  //Define grid's index limits for boundary potential
  int ixMinB, ixMaxB, iyMinB, iyMaxB;
  boundary.defineExtXYlimits( &ixMinB, &ixMaxB, &iyMinB, &iyMaxB);
  if(ixMaxB >= nXBins_ ) ixMaxB = nXBins_ -1;
  if(iyMaxB >= nYBins_ ) iyMaxB = nYBins_ -1;  
 
  //right and left slices are not considered  
  // for (iCT = nSlices_-2; iCT > 0; iCT--){
  for (iCT = 1; iCT < nSlices_-1; iCT++){

      for (i = ixMin_; i <= ixMax_; i++)
      for (j = iyMin_; j <= iyMax_; j++){{
        in_[j + nYBins_*i] = rho3D_[iCT][i][j];        
      }}
     rfftwnd_one_real_to_complex(planForward_, in_, out1_);

     // Do Convolution:
     for (i = 0; i < nXBins_; i++)
     for (j = 0; j < nYBins_/2+1; j++)
     {
        index = j + (nYBins_/2+1)*i;
        c_re(out_[index]) = c_re(out1_[index])*c_re(out_green_[index]) -
                            c_im(out1_[index])*c_im(out_green_[index]);
        c_im(out_[index]) = c_re(out1_[index])*c_im(out_green_[index]) +
                            c_im(out1_[index])*c_re(out_green_[index]);
     }     
    rfftwnd_one_complex_to_real(planBackward_, out_, in1_);

    for (i = ixMinB ; i <= ixMaxB; i++)
    for (j = iyMinB ; j <= iyMaxB; j++){{
      index = j + nYBins_ * i;      
      phiSC_[iCT][i][j] = in1_[index] * denom;
    }}

    //debug ======start=========
    //std::cout<<" iCT="<<iCT<<"=====before=====ixMin_, ixMax_, iyMin_, iyMax_="
    //         <<ixMin_<<" "<<ixMax_<<" "<<iyMin_<<" "<<iyMax_<<" "<<"\n";
    //for (i = ixMinB ; i <= ixMaxB; i++){
    //  std::cout<<" iX="<<i<<" ";
    //   for (j = iyMinB ; j <= iyMaxB; j++){
    //	   std::cout<<" iY="<<j<<" p="<<phiSC_[iCT][i][j]<<" ";
    //   }
    //   std::cout<<"\n";
    //}
    //debug ======stop==========
    
    //add the boundary potential
    boundary.addBoundaryPotential(phiSC_[iCT],ixMin_, ixMax_, iyMin_, iyMax_);
  }

   //Loading Manager timing stop
   LoadManager->measureStop();

  //Conforms SC potential across all CPUs
   _conformSCPotential();

}

//Conforms SC potential across all CPUs
void ORBIT_FFT3DSpaceCharge::_conformSCPotential()
{

  int ix,iy, counter, iSlice0, iSlice1;

  if(size_MPI == 1) {
    for(ix = ixMin_ ; ix <= ixMax_; ix++){
    for(iy = iyMin_ ; iy <= iyMax_; iy++){
      phiSC_[0][ix][iy] = phiSC_[nSlices_-3][ix][iy];
      phiSC_[nSlices_-1][ix][iy] = phiSC_[2][ix][iy];      
    }}    
    return;
  }

  MPI_Status statusMPI;

  //clockwise sending - copy second right slice to the left one at the next CPU
  int send_to_ID, recive_from_ID;

  send_to_ID = rank_MPI +1;
  recive_from_ID = rank_MPI - 1;
  if( rank_MPI == (size_MPI-1)) { send_to_ID = 0; }
  if( rank_MPI == 0           ) { recive_from_ID =(size_MPI-1); } 

  int exchange_size = (ixMax_ - ixMin_+1)*(iyMax_ - iyMin_ + 1);

  if( exchange_size > buff_double_size_ ){
   std::cerr << "ORBIT_FFT3DSpaceCharge::_conformSCPotential ====STOP=== \n "
        << " Exchange buffer size too small. \n" 
        << " Stop.";
        _finalize_MPI();
        exit(1);    
  }

  counter = 0;
  if(rank_MPI == (size_MPI-1) ){
   iSlice1 = nSlices_-3;
  }
  else{    
   iSlice1 = nSlices_-2;
  }
  
  for(ix = ixMin_ ; ix <= ixMax_; ix++){
  for(iy = iyMin_ ; iy <= iyMax_; iy++){
    buff_double[counter] = phiSC_[iSlice1][ix][iy];
    counter++;
  }}
  
  MPI_Sendrecv(buff_double    , exchange_size, MPI_DOUBLE, send_to_ID    , 1, 
               buff_double_MPI, exchange_size, MPI_DOUBLE, recive_from_ID, 1,
               MPI_COMM_WORLD , &statusMPI);

  counter = 0;
  iSlice1 = 0;
  for(ix = ixMin_ ; ix <= ixMax_; ix++){
  for(iy = iyMin_ ; iy <= iyMax_; iy++){
    phiSC_[iSlice1][ix][iy] = buff_double_MPI[counter];
    counter++;
  }}

  //anti-clockwise sending - copy the second left slice to the right one at the next CPU
  counter = 0;
  if(rank_MPI == 0){
   iSlice0 = 2;
  }
  else{
   iSlice0 = 1;   
  }
  
  for(ix = ixMin_ ; ix <= ixMax_; ix++){
  for(iy = iyMin_ ; iy <= iyMax_; iy++){
    buff_double[counter] = phiSC_[iSlice0][ix][iy];
    counter++;
  }}

  MPI_Sendrecv(buff_double    , exchange_size, MPI_DOUBLE, recive_from_ID   , 1, 
               buff_double_MPI, exchange_size, MPI_DOUBLE, send_to_ID       , 1,
               MPI_COMM_WORLD , &statusMPI);

  counter = 0;
  iSlice0 = nSlices_-1; 
  for(ix = ixMin_ ; ix <= ixMax_; ix++){
  for(iy = iyMin_ ; iy <= iyMax_; iy++){
    phiSC_[iSlice0][ix][iy] = buff_double_MPI[counter];
    counter++;
  }}
  
}

//Propagates particles through 3DSC element
void ORBIT_FFT3DSpaceCharge::propagate(MacroPart &mp,
                                       ORBIT_Boundary &boundary,
                                       double element_length,
                                       double shift_X, double shift_Y)
{
  MacroPartDistributor* PartDistr;
  PartDistr = MacroPartDistributor::GetMacroPartDistributor();

  //bin macroparticles
  _binMacroParticles(mp, boundary, shift_X,shift_Y);

   if( nPartAll_ < minBunchSize_ ){ return;}

  //calculate 3D potential
  _claculatePotential(boundary);

  // The coefficients for the kicks
  //==================================================================
  // Only this part of the code deals with ORBIT classes ====start====

  //Loading Manager definition (this is singleton)
  NodeLoadingManager* LoadManager = NodeLoadingManager::GetNodeLoadingManager();

  //Loading Manager timing start
  LoadManager->measureStart();

  double  coefT,coefTX,coefTY,coefL;
  double lambda;
  double rClassical = 1.534698e-18; // Classical p radius

  SyncPart *sp = SyncPart::safeCast
                 (syncP((Particles::syncPart & Particles::All_Mask)-1));

  lambda  = Injection::nReals_Macro * Ring::harmonicNumber * Consts::twoPi 
            / (Ring::lRing*phi_step_);

  coefL   = Sqr(sp->_charge) * lambda * rClassical * element_length/
            (sp->_mass * Sqr(sp->_betaSync) * Cube(sp->_gammaSync));

  coefT   = -1000. * 1000. * coefL;

  coefL  *= Ring::harmonicNumber * Consts::twoPi / (Ring::lRing);

  // Only this part of the code deals with ORBIT classes ====stop=====
  //==================================================================

  coefTX = coefT/dx_;
  coefTY = coefT/dy_;
  coefL  = coefL/phi_step_;

  //propagate macroparticles  

  int ip,ip1,iX,iY,iCT,iCTp,iCTm; 
  double Wxm,   Wx0,  Wxp,  Wym,  Wy0,  Wyp,  Wctm,  Wct0,  Wctp;
  double dWxm, dWx0, dWxp, dWym, dWy0, dWyp, dWctm, dWct0, dWctp;
  double kickX, kickY, kickE;
  
  for(ip = 0; ip < bunchSize_; ip++){
   if( xBin_[ip] < 0) { continue;}
   ip1 = ip +1;
   iX = xBin_[ip];
   iY = yBin_[ip];
   iCT =  LPositionIndex_[ip];

   iCT = iCT - longIndex_min_ +1; 
   iCTp = iCT +1;
   iCTm = iCT -1;  

      Wxm = 0.5 * (0.5 - xFractBin_[ip]) * (0.5 - xFractBin_[ip]);
      Wx0 = 0.75 - xFractBin_[ip] * xFractBin_[ip];
      Wxp = 0.5 * (0.5 + xFractBin_[ip]) * (0.5 + xFractBin_[ip]);
      Wym = 0.5 * (0.5 - yFractBin_[ip]) * (0.5 - yFractBin_[ip]);
      Wy0 = 0.75 - yFractBin_[ip] * yFractBin_[ip];
      Wyp = 0.5 * (0.5 + yFractBin_[ip]) * (0.5 + yFractBin_[ip]);
      Wctm = 0.5 * (0.5 - fractLPosition_[ip]) * (0.5 - fractLPosition_[ip]);
      Wct0 = 0.75 - fractLPosition_[ip] * fractLPosition_[ip];
      Wctp = 0.5 * (0.5 + fractLPosition_[ip]) * (0.5 + fractLPosition_[ip]);

      dWxm = (0.5 - xFractBin_[ip]);
      dWx0 = 2. * xFractBin_[ip];
      dWxp = -(0.5 + xFractBin_[ip]);
      dWym = (0.5 - yFractBin_[ip]);
      dWy0 = 2. * yFractBin_[ip];
      dWyp = -(0.5 + yFractBin_[ip]);
      dWctm = (0.5 - fractLPosition_[ip]);
      dWct0 = 2. * fractLPosition_[ip];
      dWctp = -(0.5 + fractLPosition_[ip]);

       kickX = Wctm *
            (
            dWxm * Wym * phiSC_[iCTm ][iX-1][iY-1] +
            dWx0 * Wym * phiSC_[iCTm ][iX  ][iY-1] +
            dWxp * Wym * phiSC_[iCTm ][iX+1][iY-1] +
            dWxm * Wy0 * phiSC_[iCTm ][iX-1][iY  ] +
            dWx0 * Wy0 * phiSC_[iCTm ][iX  ][iY  ] +
            dWxp * Wy0 * phiSC_[iCTm ][iX+1][iY  ] +
            dWxm * Wyp * phiSC_[iCTm ][iX-1][iY+1] +
            dWx0 * Wyp * phiSC_[iCTm ][iX  ][iY+1] +
            dWxp * Wyp * phiSC_[iCTm ][iX+1][iY+1]
            )
            +
            Wct0 *
            (
            dWxm * Wym * phiSC_[iCT  ][iX-1][iY-1] +
            dWx0 * Wym * phiSC_[iCT  ][iX  ][iY-1] +
            dWxp * Wym * phiSC_[iCT  ][iX+1][iY-1] +
            dWxm * Wy0 * phiSC_[iCT  ][iX-1][iY  ] +
            dWx0 * Wy0 * phiSC_[iCT  ][iX  ][iY  ] +
            dWxp * Wy0 * phiSC_[iCT  ][iX+1][iY  ] +
            dWxm * Wyp * phiSC_[iCT  ][iX-1][iY+1] +
            dWx0 * Wyp * phiSC_[iCT  ][iX  ][iY+1] +
            dWxp * Wyp * phiSC_[iCT  ][iX+1][iY+1]
            )
            +
            Wctp *
            (
            dWxm * Wym * phiSC_[iCTp ][iX-1][iY-1] +
            dWx0 * Wym * phiSC_[iCTp ][iX  ][iY-1] +
            dWxp * Wym * phiSC_[iCTp ][iX+1][iY-1] +
            dWxm * Wy0 * phiSC_[iCTp ][iX-1][iY  ] +
            dWx0 * Wy0 * phiSC_[iCTp ][iX  ][iY  ] +
            dWxp * Wy0 * phiSC_[iCTp ][iX+1][iY  ] +
            dWxm * Wyp * phiSC_[iCTp ][iX-1][iY+1] +
            dWx0 * Wyp * phiSC_[iCTp ][iX  ][iY+1] +
            dWxp * Wyp * phiSC_[iCTp ][iX+1][iY+1]
            );

       kickY = Wctm *
            (
            Wxm * dWym * phiSC_[iCTm ][iX-1][iY-1] +
            Wx0 * dWym * phiSC_[iCTm ][iX  ][iY-1] +
            Wxp * dWym * phiSC_[iCTm ][iX+1][iY-1] +
            Wxm * dWy0 * phiSC_[iCTm ][iX-1][iY  ] +
            Wx0 * dWy0 * phiSC_[iCTm ][iX  ][iY  ] +
            Wxp * dWy0 * phiSC_[iCTm ][iX+1][iY  ] +
            Wxm * dWyp * phiSC_[iCTm ][iX-1][iY+1] +
            Wx0 * dWyp * phiSC_[iCTm ][iX  ][iY+1] +
            Wxp * dWyp * phiSC_[iCTm ][iX+1][iY+1]
            )
            +
            Wct0 *
            (
            Wxm * dWym * phiSC_[iCT  ][iX-1][iY-1] +
            Wx0 * dWym * phiSC_[iCT  ][iX  ][iY-1] +
            Wxp * dWym * phiSC_[iCT  ][iX+1][iY-1] +
            Wxm * dWy0 * phiSC_[iCT  ][iX-1][iY  ] +
            Wx0 * dWy0 * phiSC_[iCT  ][iX  ][iY  ] +
            Wxp * dWy0 * phiSC_[iCT  ][iX+1][iY  ] +
            Wxm * dWyp * phiSC_[iCT  ][iX-1][iY+1] +
            Wx0 * dWyp * phiSC_[iCT  ][iX  ][iY+1] +
            Wxp * dWyp * phiSC_[iCT  ][iX+1][iY+1]
            )
            +
            Wctp *
            (
            Wxm * dWym * phiSC_[iCTp ][iX-1][iY-1] +
            Wx0 * dWym * phiSC_[iCTp ][iX  ][iY-1] +
            Wxp * dWym * phiSC_[iCTp ][iX+1][iY-1] +
            Wxm * dWy0 * phiSC_[iCTp ][iX-1][iY  ] +
            Wx0 * dWy0 * phiSC_[iCTp ][iX  ][iY  ] +
            Wxp * dWy0 * phiSC_[iCTp ][iX+1][iY  ] +
            Wxm * dWyp * phiSC_[iCTp ][iX-1][iY+1] +
            Wx0 * dWyp * phiSC_[iCTp ][iX  ][iY+1] +
            Wxp * dWyp * phiSC_[iCTp ][iX+1][iY+1]
            );

       kickE = dWctm *
            (
            Wxm * Wym * phiSC_[iCTm ][iX-1][iY-1] +
            Wx0 * Wym * phiSC_[iCTm ][iX  ][iY-1] +
            Wxp * Wym * phiSC_[iCTm ][iX+1][iY-1] +
            Wxm * Wy0 * phiSC_[iCTm ][iX-1][iY  ] +
            Wx0 * Wy0 * phiSC_[iCTm ][iX  ][iY  ] +
            Wxp * Wy0 * phiSC_[iCTm ][iX+1][iY  ] +
            Wxm * Wyp * phiSC_[iCTm ][iX-1][iY+1] +
            Wx0 * Wyp * phiSC_[iCTm ][iX  ][iY+1] +
            Wxp * Wyp * phiSC_[iCTm ][iX+1][iY+1]
            )
            +
            dWct0 *
            (
            Wxm * Wym * phiSC_[iCT  ][iX-1][iY-1] +
            Wx0 * Wym * phiSC_[iCT  ][iX  ][iY-1] +
            Wxp * Wym * phiSC_[iCT  ][iX+1][iY-1] +
            Wxm * Wy0 * phiSC_[iCT  ][iX-1][iY  ] +
            Wx0 * Wy0 * phiSC_[iCT  ][iX  ][iY  ] +
            Wxp * Wy0 * phiSC_[iCT  ][iX+1][iY  ] +
            Wxm * Wyp * phiSC_[iCT  ][iX-1][iY+1] +
            Wx0 * Wyp * phiSC_[iCT  ][iX  ][iY+1] +
            Wxp * Wyp * phiSC_[iCT  ][iX+1][iY+1]
            )
            +
            dWctp *
            (
            Wxm * Wym * phiSC_[iCTp ][iX-1][iY-1] +
            Wx0 * Wym * phiSC_[iCTp ][iX  ][iY-1] +
            Wxp * Wym * phiSC_[iCTp ][iX+1][iY-1] +
            Wxm * Wy0 * phiSC_[iCTp ][iX-1][iY  ] +
            Wx0 * Wy0 * phiSC_[iCTp ][iX  ][iY  ] +
            Wxp * Wy0 * phiSC_[iCTp ][iX+1][iY  ] +
            Wxm * Wyp * phiSC_[iCTp ][iX-1][iY+1] +
            Wx0 * Wyp * phiSC_[iCTp ][iX  ][iY+1] +
            Wxp * Wyp * phiSC_[iCTp ][iX+1][iY+1]
            );

       //std::cout<<"debug ip="<<ip1<<" kick x y de ="<< kickX <<" "<< kickY <<" "<< kickE <<"\n";

       // px() is in [rad]
       // py() is in [rad]
       // _deltaE is defined d(E)/p0
       mp._xp(ip1)     += coefTX*kickX;
       mp._yp(ip1)     += coefTY*kickY;
       mp._dp_p(ip1)   += coefL * kickE;
       mp._deltaE(ip1)  = mp._dp_p(ip1) / mp._syncPart._dppFac;

       // cout << "debug delta px py dp_p  = " << coefTX * kickX
       //      << " " << coefTY * kickY << " " << coefL*kickE << "\n ";
  }

  //Loading Manager timing stop
  LoadManager->measureStop();

}


// Finalize MPI
void ORBIT_FFT3DSpaceCharge::_finalize_MPI()
{
  if(iMPIini > 0 ){
   MPI_Finalize();
  }
}
