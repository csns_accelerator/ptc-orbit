/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   PartsDistributor_MPI.h
//
// CREATED
//   5/20/2002
//
// DESCRIPTION
//   Header file for the NodeLoadingManager and MacroPartDistributor classes. 
//   These classes define the longitudinal slices distribution between CPUs 
//   and distribute macro particles between CPUs according slices' distribution.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(__PARTS_DISTRIBUTION_MPI__)
#define __PARTS_DISTRIBUTION_MPI__

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "mpi.h"
#include "MacroPart.h"

//Consts.h - header from SuperCode with Consts::pi
#include "Consts.h"

/////////////////////////////////////////////////////////////////////////////
//The header for  the NodeLoadingManager class
/////////////////////////////////////////////////////////////////////////////

class NodeLoadingManager
{
 public:

  static NodeLoadingManager* GetNodeLoadingManager(int nCount, int nSl);
  static NodeLoadingManager* GetNodeLoadingManager();

  int getTotalNumberOfSlices();
  int getNumberOfSlices();
  int getNumberOfSlices(int iRank);

  //set number of slices that belonges to each CPU
  //independently from external source
  //nSlices_Ext - array 
  //nMPIsize_Ext - numbers of elements in array
  void setNumberOfSlices(int* nSlices_Ext, int nMPIsize_Ext);


  void setNumberOfCount(int nCount);
  void debugPrint();

  //This method starts timing measures
  void measureStart();

  //This method stops timing measures
  void measureStop();

  //Returns 0 if nothing changed 1 otherwise
  int getMeasure();

  //The method for debugging 
  void printDistribution();

  //Switch Load Menager On
  void switchOn(); 

  //Switch Load Menager Off
  void switchOff(); 

  ~NodeLoadingManager();
 protected:
  NodeLoadingManager(int nCount, int nSl);
 private:
  static NodeLoadingManager *m_Instance;
  int nSlices_Total_;
  int iMPIini_;
  int nMPIsize_;
  int nMPIrank_;
  int* nSlices_;
  int* nSlices_new_;
  double* tLoading_;
  double* tLoading_MPI_;

  double tm_new_;
  double tm_old_;

  int switchOn_;

  int nCountStep_;
  int iCounter_;
};

/////////////////////////////////////////////////////////////////////////////
//The header for  the MacroPartDistributor class
/////////////////////////////////////////////////////////////////////////////
class MacroPartDistributor
{
 public:

  static MacroPartDistributor* GetMacroPartDistributor(int nCountLM,int nSlices_total);
  static MacroPartDistributor* GetMacroPartDistributor();

  ~MacroPartDistributor();

  void distributeParticles(MacroPart &mp);

  void distributeParticlesEvenly(MacroPart &mp);

  double getPhiMax();
  double getPhiMin();
  double getPhiStep();
  int getPhiIndex(double phi);
  int getNodeIndex(double phi);
  int getNodeIndex(int iPhiInd);
  int getSliceIndMin();
  int getSliceIndMax();

  void setPartNumbChunk( int partNumbChunkIn);

 protected:
  MacroPartDistributor(int nCountLM,int nSlices_total);
 private:
  void setLoadingDist();
  void resetLoadingDist();
  void findPhiExtrema(MacroPart &mp);
  void resizeSendRecvBuffers(int send_size, int recv_size);
  void resizePartIndBuffers(int nMacros);
  void defSendRecvSequence();
  void updateHerd(MacroPart &mp, int n_sent, int n_recived);
 private:
  static MacroPartDistributor *d_Instance;
  int nSlices_Total_;
  int iMPIini_;
  int nMPIsize_;
  int nMPIrank_;
  //exchange table actually the size is (nMPIsize_ X nMPIsize_)
  int* table_exch_;
  int* table_exch_MPI_;

  //arrays for even distribution
  int* nPartInSlice;
  int* nPartInSlice_MPI;
  int* nSlices_Arr;

  //particle's buffers
  int n_particles_send_buff_;
  int n_particles_recv_buff_;
  int n_part_del_;
  double* part_buffer_send_;
  double* part_buffer_recv_;

  //step in increasing of buffers sizes
  int partNumbChunk_;

  //ind_part_del_[iPart] - indexes of CPU for every particle in the Herd
  int* ind_part_del_;
  //nodeStartIndex[j-recv] - array of nMPIsize_ size with initial index 
  //for particles that are sent from this CPU to another - j-recv
  int* nodeStartIndex;
  int* numberOfPartToSend;

  //Arrays with dependences (Slice Index Min or Max <- rank of CPU)
  int* sliceIndMin_;
  int* sliceIndMax_;  


  //The arrays defining the oder of sending and receiving
  //this is the two tables of size 0..(nMPIsize_-1)*nMPIsize_
  int* i_node_send_;
  int* j_node_recv_;

  //nodes ID table defines the rank of CPU for a particular slice
  int* node_id_4sl_;

  //Loading manager
  NodeLoadingManager* LM_;

  //phi extrema
  double phi_min_;
  double phi_max_;
  double step_phi_;
  double phiSenseRange_;
};




///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif   // __PARTS_DISTRIBUTION_MPI__


