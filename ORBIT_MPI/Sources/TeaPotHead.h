/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TeaPotHead.h
//
// AUTHOR
//   Joshua Abrams, Knox College, jabrams@knox.edu
//   Steven Bunch, University of Tennessee, sbunch2@utk.edu
//
// CREATED
//   09/09/02
//
// MODIFIED
//   03/24/03
//
// DESCRIPTION
//   Stacks thick-thin sequence to emulate long elements
//
// REVISION HISTORY
//   Added Multipole functions for Multipoles, Quadrupoles, and Bends
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(__TeaPotMap__)
#define __TeaPotMap__

#include "Object.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPBase
//
// INHERITANCE RELATIONSHIPS
//   TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a base class for storing TeaPot Node information.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPBase : public MapBase
{
  Declare_Standard_Members(TPBase, MapBase);
  public:
  TPBase(const String &n, const Integer &order,
         const Real &bx, const Real &by,
         const Real &ax, const Real &ay,
         const Real &ex, const Real &epx,
         const Real &l, const String &et);

  virtual Void nameOut(String &wname) { wname = _name; }
  virtual Void _updatePartAtNode(MacroPart &mp)=0;
  virtual Void _nodeCalculator(MacroPart &mp)=0;
  virtual Void _showTP(Ostream &sout)=0;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPD
//
// INHERITANCE RELATIONSHIPS
//   TPD  -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a drift.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   nsteps(int)--> _nsteps      : Number of drift steps
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPD : public TPBase
{
  Declare_Standard_Members(TPD, TPBase);
  public:

  TPD(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Integer &nsteps);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Integer _nsteps;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPM
//
// INHERITANCE RELATIONSHIPS
//   TPM -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a multipole with length.
//
// PUBLIC MEMBERS
//   n    (str) --> _name     : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   pole (int) --> _pole        : Multipole order.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Integrated strength (1/m^pole).
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Switch to use a skew element.
//                                 If == 1, the thin lens is rotated
//                                 by pi/(2*n + 2)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of thin multipole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPM : public TPBase
{
  Declare_Standard_Members(TPM, TPBase);
  public:
  TPM(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &tilt,
      const Integer &pole, const Real &kl,
      const Integer &skew,
      const Subroutine sub, const Integer &nsteps,
      const Integer &fringeIN, const Integer &fringeOUT);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Subroutine _sub;
  Integer _pole, _skew, _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _kl;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPQ
//
// INHERITANCE RELATIONSHIPS
//   TPQ -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a non-linear quadrupole.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   kq (real)	--> _kq          : Strength (1/m^2)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of nonlinear quadrupole steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPQ : public TPBase
{
  Declare_Standard_Members(TPQ, TPBase);
  public:

  TPQ(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &tilt,
      const Real &kq,
      const Subroutine sub, const Integer &nsteps,
      const Integer &fringeIN, const Integer &fringeOUT);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Subroutine _sub;
  Integer _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _kq;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPB
//
// INHERITANCE RELATIONSHIPS
//   TPB -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a TeaPot bending magnet.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   theta(real)--> _theta       : Bending angle
//   ea1(real)--> _ea1           : Entrance end angle
//   ea2(real)--> _ea2           : Exit end angle
//   nsteps(int)--> _nsteps      : Number of nonlinear bend steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPB : public TPBase
{
  Declare_Standard_Members(TPB, TPBase);
  public:
  TPB(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &tilt,
      const Real &theta,
      const Real &ea1, const Real &ea2,
      const Integer &nsteps,
      const Integer &fringeIN, const Integer &fringeOUT);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Integer _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _theta, _ea1, _ea2;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPS
//
// INHERITANCE RELATIONSHIPS
//   TPS -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a teapot solenoid.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   B (real)	--> _B           : Strength (1/m)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of integration steps
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPS : public TPBase
{
  Declare_Standard_Members(TPS, TPBase);
  public:
  TPS(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &B,
      const Subroutine sub, const Integer &nsteps);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Real _B;
  Subroutine _sub;
  Integer _nsteps;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPMCF
//
// INHERITANCE RELATIONSHIPS
//   TPMCF -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a multipole with length.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of thin multipole kicks
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPMCF : public TPBase
{
  Declare_Standard_Members(TPMCF, TPBase);
  public:
  TPMCF(const String &n, const Integer &order,
        const Real &bx, const Real &by,
        const Real &ax, const Real &ay,
        const Real &ex, const Real &epx,
        const Real &l, const String &et,
        const Real &tilt,
        const Integer &vecnum,
        const Vector(Integer) &pole,
        const Vector(Real) &kl,
        const Vector(Integer) &skew,
        const Subroutine sub, const Integer &nsteps,
        const Integer &fringeIN, const Integer &fringeOUT);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Subroutine _sub;
  Integer _vecnum, _nsteps, _fringeIN, _fringeOUT;
  Real _tilt;
  Vector(Integer) _pole, _skew;
  Vector(Real) _kl;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPQCF
//
// INHERITANCE RELATIONSHIPS
//   TPQCF -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a non-linear quadrupole.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   kq (real)	--> _kq          : Strength (1/m^2)
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of nonlinear quadrupole steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPQCF : public TPBase
{
  Declare_Standard_Members(TPQCF, TPBase);
  public:

  TPQCF(const String &n, const Integer &order,
        const Real &bx, const Real &by,
        const Real &ax, const Real &ay,
        const Real &ex, const Real &epx,
        const Real &l, const String &et,
        const Real &tilt,
        const Real &kq,
        const Integer &vecnum,
        const Vector(Integer) &pole,
        const Vector(Real) &kl,
        const Vector(Integer) &skew,
        const Subroutine sub, const Integer &nsteps,
        const Integer &fringeIN, const Integer &fringeOUT);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Subroutine _sub;
  Integer _vecnum, _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _kq;
  Vector(Integer) _pole, _skew;
  Vector(Real) _kl;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPBCF
//
// INHERITANCE RELATIONSHIPS
//   TPBCF -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a TeaPot bending magnet.
//
// PUBLIC MEMBERS
//   n    (str) --> _name        : Name for this node
//   order (int)--> _oindex      : Node order index
//   bx (real)  --> _betaX       : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by (real)  --> _betaY       : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax (real)  --> _alphaX      : The horizontal alpha value at the
//                                 beginning of the Node
//   ay (real)  --> _alphaY      : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex (real)  --> _etaX        : The horizontal dispersion [m]
//   epx (real) --> _etaPX       : The horizontal dispersion prime
//   l   (real) --> _length      : The length of the lens
//   et (string)--> _et          : Element Type
//   tilt (real)--> _tilt        : Element roll
//   theta(real)--> _theta       : Bending angle
//   ea1(real)--> _ea1           : Entrance end angle
//   ea2(real)--> _ea2           : Exit end angle
//   vecnum(int)--> _vecnum      : Number of multipole terms.
//   pole (int) --> _pole        : Vector of orders of multipole terms.
//                  		   0 = dipole, 1=quad, 2=sextupole,
//                                 3=octupole,...
//   kl (real)	--> _kl          : Vector of integrated strengths
//                                 (1/m^pole) of multipole terms.
//                                 This is the integral of
//                  		   "k" = 1/(B*rho) * d(m)B_y/dx(m)
//                                 at x = 0
//                                 over the element length "l"
//   skew(int) 	--> _skew        : Vector of switches for skewness of
//                                 multipole terms.
//                                 If == 1, the thin lens is rotated
//                                 by pi / (2 * n + 2)
//   nsteps(int)--> _nsteps      : Number of nonlinear bend steps
//   fringeIN(int)--> _fringeIN  : 1: Fringe Fields or 0: No Fringe Fields
//   fringeOUT(int)--> _fringeOUT: 1: Fringe Fields or 0: No Fringe Fields
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPBCF : public TPBase
{
  Declare_Standard_Members(TPBCF, TPBase);
  public:
  TPBCF(const String &n, const Integer &order,
        const Real &bx, const Real &by,
        const Real &ax, const Real &ay,
        const Real &ex, const Real &epx,
        const Real &l, const String &et,
        const Real &tilt,
        const Real &theta,
        const Real &ea1, const Real &ea2,
        const Integer &vecnum,
        const Vector(Integer) &pole,
        const Vector(Real) &kl,
        const Vector(Integer) &skew,
        const Integer &nsteps,
        const Integer &fringeIN, const Integer &fringeOUT);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Integer _vecnum, _nsteps, _fringeIN, _fringeOUT;
  Real _tilt, _theta, _ea1, _ea2;
  Vector(Integer) _pole, _skew;
  Vector(Real) _kl;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   TPK
//
// INHERITANCE RELATIONSHIPS
//   TPK  -> TPBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class that creates a kicker.
//
// PUBLIC MEMBERS
//   n     (str) --> _name       : Name for this node
//   order  (int)--> _oindex     : node order index
//   bx  (real)  --> _betaX      : The horizontal beta value at the
//                                 beginning of the Node [m]
//   by  (real)  --> _betaY      : The vertical beta value at the
//                                 beginning of the Node [m]
//   ax  (real)  --> _alphaX     : The horizontal alpha value at the
//                                 beginning of the Node
//   ay  (real)  --> _alphaY     : The horizontal alpha  value at the
//                                 beginning of the Node
//   ex  (real)  --> _etaX       : The horizontal dispersion [m]
//   epx  (real) --> _etaPX      : The horizontal dispersion prime
//   l    (real) --> _length     : The length of the lens
//   et  (string)--> _et         : Element Type
//   hkick(real) --> _hkick      : Horizontal kick strength
//   vkick(real) --> _vkick      : Vertical kick strength
//   sub (sub)  --> _sub         : Time dependent strength factor
//   nsteps(int)--> _nsteps      : Number of kick steps
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class TPK : public TPBase
{
  Declare_Standard_Members(TPK, TPBase);

  public:

  TPK(const String &n, const Integer &order,
      const Real &bx, const Real &by,
      const Real &ax, const Real &ay,
      const Real &ex, const Real &epx,
      const Real &l, const String &et,
      const Real &hkick, const Real &vkick,
      const Subroutine sub, const Integer &nsteps);

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showTP(Ostream &sout);

  Real _hkick, _vkick;
  Subroutine _sub;
};

#endif   // __TeaPotMap__
