////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   PartsDistributor_MPI.cc
//
// CREATED
//   5/20/2002
//
// DESCRIPTION
//   Source code for the NodeLoadingManager and MacroPartDistributor classes
//   These classes define the longitudinal slices distribution between CPUs
//   and distribute macro particles between CPUs according slices' distribution
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Particles.h"
#include "MacroPart.h"
#include "PartsDistributor_MPI.h"
#include <iostream>
#include <cstdlib>

extern Array(ObjectPtr) mParts;

/////////////////////////////////////////////////////////////////////////////
//The Methods of the NodeLoadingManager class
/////////////////////////////////////////////////////////////////////////////

NodeLoadingManager* NodeLoadingManager::m_Instance=0;

NodeLoadingManager::NodeLoadingManager(int nCount, int nSl)
{

    nSlices_Total_ = nSl;
    nCountStep_ = nCount;
    iCounter_ = 0;

    switchOn_ = 0;

    // set some initial values
    iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    nMPIsize_ = 1;
    nMPIrank_ = 0;
    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }

    if(nSlices_Total_ < nMPIsize_){
     std::cerr << "class NodeLoadingManager, constructor:\n";
     std::cerr << "You try to defune the number of slices less than the number of CPUs.\n";
     std::cerr << "nSlices ="<< nSlices_Total_ <<"\n";
     std::cerr << "nCPUs ="<<nMPIsize_<<"\n";
     if(iMPIini_ > 0)  MPI_Finalize();
     exit(1);
    }


    //define arrays for the loading measurements
    nSlices_     = (int *) malloc ( sizeof(int)*nMPIsize_);
    nSlices_new_ = (int *) malloc ( sizeof(int)*nMPIsize_);
    tLoading_    = (double *) malloc ( sizeof(double)*nMPIsize_);
    tLoading_MPI_= (double *) malloc ( sizeof(double)*nMPIsize_);

    int i;
    for( i=0; i<nMPIsize_; i++){
      nSlices_[i] = nSlices_Total_/nMPIsize_;
      tLoading_[i] = 0;
      tLoading_MPI_[i] = 0;
    }

    for( i=0; i<nSlices_Total_%nMPIsize_; i++){
      nSlices_[i]++;
    }

    for( i=0; i<nMPIsize_; i++){
      nSlices_new_[i]=nSlices_[i];
    }    
}

NodeLoadingManager::~NodeLoadingManager()
{
  free(nSlices_);
  free(nSlices_new_);
  free(tLoading_);
  free(tLoading_MPI_);
}

NodeLoadingManager* NodeLoadingManager::GetNodeLoadingManager(int nCount, int nSl)
{

  if(!m_Instance){
    m_Instance = new  NodeLoadingManager(nCount, nSl);
    return  m_Instance;
  }

  std::cerr << "Class NodeLoadingManager, method GetNodeLoadingManager(int nCount, int nSl):\n";
  std::cerr << "This method can be used only once. \n";
  std::cerr << "Stop.\n";
  int iMPIini;
  MPI_Initialized(&iMPIini);
  if(iMPIini > 0) MPI_Finalize();
  exit(1);
}

NodeLoadingManager* NodeLoadingManager::GetNodeLoadingManager()
{
  if(!m_Instance){
    std::cerr << "class NodeLoadingManager, method GetNodeLoadingManager():\n";
    std::cerr << "You may use this method only after: \n";
    std::cerr << "GetNodeLoadingManager(nCount, nSl) .\n";
    std::cerr << "Stop.\n";
    int iMPIini;
    MPI_Initialized(&iMPIini);
    if(iMPIini > 0) MPI_Finalize();
    exit(1); 
  }
  return m_Instance;
}

int NodeLoadingManager::getTotalNumberOfSlices()
{
  return nSlices_Total_;
}


int NodeLoadingManager::getNumberOfSlices()
{
  return nSlices_[nMPIrank_];
}

int NodeLoadingManager::getNumberOfSlices(int iRank)
{
  if(iRank > (nMPIsize_-1) || iRank < 0){
    std::cerr << "class NodeLoadingManager, method getNumberOfSlices(int iRank):\n";
    std::cerr << "You try to get the number of slices for non existing CPU's rank.\n";
    std::cerr << "iRank ="<<iRank<<"\n";
    std::cerr << "nCPUs ="<<nMPIsize_<<"\n";
    if(iMPIini_ > 0)   MPI_Finalize();
    exit(1);
  }
  else{
   return nSlices_[iRank];
  }
}

//set number of slices that belonges to each CPU
//independently from external source
//nSlices_Ext - array 
//nMPIsize_Ext - numbers of elements in array
void NodeLoadingManager::setNumberOfSlices(int* nSlices_Ext, int nMPIsize_Ext)
{
  int nSumLocal = 0;
  for(int i = 0; i < nMPIsize_Ext; i++){
    nSumLocal += nSlices_Ext[i];
  }

  if(nMPIsize_Ext != nMPIsize_ || nSumLocal !=nSlices_Total_ ){
    std::cerr << "class NodeLoadingManager, method setNumberOfSlices(int* nSlices_Ext, int nMPIsize_Ext):\n";
    std::cerr << "You try to set the number of slices not equal to existing number or\n";
    std::cerr << "the size of array is not equal to numbers of CPUs.\n";
    std::cerr << "nMPIsize_Ext ="<< nMPIsize_Ext <<" nMPIsize_="<< nMPIsize_  <<"\n";
    std::cerr << "nSumLocal ="<< nSumLocal <<" nSlices_Total_="<< nSlices_Total_ <<"\n";
    if(iMPIini_ > 0)   MPI_Finalize();
    exit(1);
  }

  for(int i = 0; i < nMPIsize_Ext; i++){
    nSlices_[i] = nSlices_Ext[i];
  }

}


void NodeLoadingManager::setNumberOfCount(int nCount)
{
  nCountStep_ = nCount;
  iCounter_ = 0;
}

void NodeLoadingManager::debugPrint()
{
 std::cerr <<"Debug Printing -----------start-----------\n";
 std::cerr <<" nSlices_Total_="<< nSlices_Total_ <<"\n";
 std::cerr <<" iMPIini_="<<  iMPIini_ <<"\n";
 std::cerr <<" nMPIsize_="<< nMPIsize_ <<"\n";
 std::cerr <<" nMPIrank_="<< nMPIrank_ <<"\n";
 std::cerr <<" tm_new_="<< tm_new_ <<"\n";
 std::cerr <<" tm_old_="<< tm_old_ <<"\n";
 std::cerr <<" nCountStep_="<< nCountStep_ <<"\n";
 std::cerr <<" iCounter_="<< iCounter_ <<"\n";
 int i;
 std::cerr <<" nSlices_=";  for(i=0; i < nMPIsize_; i++){std::cerr << nSlices_[i]<<" ";} ; std::cerr <<"\n";
 std::cerr <<" nSlices_new_=";  for(i=0; i < nMPIsize_; i++){std::cerr << nSlices_new_[i]<<" ";} ; std::cerr <<"\n";
 std::cerr <<" tLoading_=";  for(i=0; i < nMPIsize_; i++){std::cerr << tLoading_[i]<<" ";} ; std::cerr <<"\n";
 std::cerr <<" tLoading_MPI_=";  for(i=0; i < nMPIsize_; i++){std::cerr << tLoading_MPI_[i]<<" ";} ; std::cerr <<"\n";
 std::cerr <<"Debug Printing -----------stop------------\n";
}

void NodeLoadingManager::measureStart()
{
  if(nMPIsize_ < 2 || switchOn_ == 0){ return;}
  tm_old_ = MPI_Wtime();
}

void NodeLoadingManager::measureStop()
{
  if(nMPIsize_ < 2 || switchOn_ == 0){ return;}

  tm_new_ = MPI_Wtime();
  tLoading_[nMPIrank_] = tLoading_[nMPIrank_] + (tm_new_ - tm_old_);
  tm_old_ = tm_new_;
}

int NodeLoadingManager::getMeasure()
{
  int i;
  int nSl;
  double tm_sum = 0;
  int i_measure = 0;

  if(nMPIsize_ < 2 || switchOn_ == 0){ return i_measure;}

  if(iCounter_ == 0){
   tm_old_ = MPI_Wtime();
   tm_new_ = tm_old_;
     for( i = 0; i < nMPIsize_; i++){
      tLoading_[i] = 0.0;
      nSlices_new_[i] = nSlices_[i];
     }   
  }

  iCounter_++;

  if(iCounter_ == nCountStep_){

    iCounter_ = 0;

    MPI_Allreduce(tLoading_,tLoading_MPI_,nMPIsize_,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    tm_sum = 0;
    for( i = 0; i < nMPIsize_; i++){
      if(tLoading_MPI_[i] != 0.0 ){
        tm_sum = tm_sum + 1.0/tLoading_MPI_[i];
      }
    }

    if(tm_sum == 0.0) {return i_measure;}

    int nSl_sum = 0;
    for( i = 0; i < nMPIsize_; i++){
      nSlices_new_[i] = nSlices_[i];
      nSl = 1;
      if(tLoading_MPI_[i] != 0.0 ){
        nSl = (int) (nSlices_Total_*((1.0/tLoading_MPI_[i])/tm_sum));
      }
      if(nSl < 1) nSl = 1;
      nSl_sum = nSl_sum + nSl + nSlices_new_[i];
      nSlices_new_[i] = nSl + nSlices_new_[i];      
    }

    while(nSl_sum != nSlices_Total_) {
      for( i = 0; i < nMPIsize_; i++){
	if(nSl_sum > nSlices_Total_ && nSlices_new_[i] > 1) {
          nSlices_new_[i]--;
          nSl_sum--;
	}
	if(nSl_sum < nSlices_Total_) {
          nSlices_new_[i]++;
          nSl_sum++;
	}  
	if( nSl_sum == nSlices_Total_){ break;}  
      }      
    }

    int iChange = 0;
    for( i = 0; i < nMPIsize_; i++){
      if( (nSlices_new_[i] - nSlices_[i]) > 1 || (nSlices_new_[i] - nSlices_[i]) < -1){
	iChange = 1;
      }
    }

    if(iChange == 1){
      for( i = 0; i < nMPIsize_; i++){
	nSlices_[i] = nSlices_new_[i];
      } 
      i_measure = 1;
      return i_measure;
    }
  }

    return i_measure;
}

void NodeLoadingManager::printDistribution()
{
  int i;
  if(nMPIrank_ == 0){
    std::cout<<"---------------slices' distribution----------------\n";
    for( i = 0; i < nMPIsize_; i++){
      std::cout<<"i="<<i<<" nSl="<<nSlices_[i]<<" time="<<tLoading_MPI_[i]<<"\n";
    }
  }
}

void NodeLoadingManager::switchOn()
{
  switchOn_ = 1;
}

void NodeLoadingManager::switchOff()
{
  switchOn_ = 0;
}

/////////////////////////////////////////////////////////////////////////////
//----END------ The Methods of the NodeLoadingManager class
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
//The Methods of the MacroPartDistributor class
/////////////////////////////////////////////////////////////////////////////
MacroPartDistributor* MacroPartDistributor::d_Instance=0;

MacroPartDistributor::MacroPartDistributor(int nCountLM,int nSlices_total)
{
    //remember that nSlices_Total_ =_nPhiBins + 1 
    //where _nPhiBins var from original ORBIT
    nSlices_Total_ = nSlices_total;

    // set some initial values
    iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    nMPIsize_ = 1;
    nMPIrank_ = 0;
    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
    else{

    }

    //===========Memory allocation ========START=====

       //exchange table
       table_exch_     = (int *) malloc ( sizeof(int)*nMPIsize_*nMPIsize_);
       table_exch_MPI_ = (int *) malloc ( sizeof(int)*nMPIsize_*nMPIsize_);
       
       //receiving and sending buffers 
       //array to hold the coordinates of the sent and received particles
       //6 means 6D coordinates
       //access - part_buffer_send_[coord_index + 6*(i_part)], coord_index=1..6
       n_particles_send_buff_ = 1;
       n_particles_recv_buff_ = 1;
       n_part_del_ = 1;
       part_buffer_send_ = (double*) malloc (sizeof(double)*n_particles_send_buff_*6);
       part_buffer_recv_ = (double*) malloc (sizeof(double)*n_particles_recv_buff_*6);
       ind_part_del_ = (int*) malloc (sizeof(int)*n_part_del_);

       nodeStartIndex = (int*) malloc (sizeof(int)*nMPIsize_);
       numberOfPartToSend = (int*) malloc (sizeof(int)*nMPIsize_);

       i_node_send_ = (int*) malloc (sizeof(int)*(nMPIsize_*nMPIsize_));
       j_node_recv_ = (int*) malloc (sizeof(int)*(nMPIsize_*nMPIsize_));

       //definition of the i_node_send_ and j_node_recv_ arrays
       defSendRecvSequence();

       
       //the (CPU ID <- slice ID) accordance array
       //node_id_4sl_(i_slice) - is the rank of CPU that deal 
       //                        with this longitudinal slice
       node_id_4sl_ = (int *) malloc ( sizeof(int) * nSlices_Total_);

       //Arrays with dependences (Slice Index Min or Max <- rank of CPU)
       sliceIndMin_ = (int*) malloc (sizeof(int)*nMPIsize_);
       sliceIndMax_ = (int*) malloc (sizeof(int)*nMPIsize_);
    //===========Memory allocation ========STOP======

    //Loading Manager definition
    LM_ = NodeLoadingManager::GetNodeLoadingManager(nCountLM,nSlices_Total_);

    //define node_id_4sl_ array by using LM_ (NodeLoadingManager)
    setLoadingDist();

    //set initial phi extrema
    phi_min_ = -Consts::pi;
    phi_max_ =  Consts::pi;
    step_phi_ = (phi_max_ - phi_min_)/(nSlices_Total_-1);
    phiSenseRange_ = 0.05;


    //array for even distribution
    nPartInSlice = (int*) malloc (sizeof(int)*nSlices_Total_);
    nPartInSlice_MPI = (int*) malloc (sizeof(int)*nSlices_Total_);
    nSlices_Arr = (int*) malloc (sizeof(int)*nMPIsize_);

    //set the step in increasing of buffers sizes
    partNumbChunk_ = 10000;
}

MacroPartDistributor::~MacroPartDistributor()
{

   free(table_exch_);
   free(table_exch_MPI_);
   free(part_buffer_send_);
   free(part_buffer_recv_);
   free(ind_part_del_);

   free(nodeStartIndex);
   free(numberOfPartToSend);

   free(i_node_send_);
   free(j_node_recv_);

   free(node_id_4sl_);

   free(sliceIndMin_);
   free(sliceIndMax_);

   free(nPartInSlice);
   free(nPartInSlice_MPI);
   free(nSlices_Arr);

   delete LM_;

}


MacroPartDistributor* MacroPartDistributor::GetMacroPartDistributor(int nCountLM,int nSlices_total)
{

  if(!d_Instance){
    d_Instance = new  MacroPartDistributor(nCountLM,nSlices_total);
    return  d_Instance;
  }
  return d_Instance;
}

MacroPartDistributor* MacroPartDistributor::GetMacroPartDistributor()
{

  if(!d_Instance){
    std::cerr << "class MacroPartDistributor, method GetMacroPartDistributor():\n";
    std::cerr << "You may use this method only after: \n";
    std::cerr << "GetMacroPartDistributor(int nCountLM,int nSlices_total) .\n";
    std::cerr << "Stop.\n";
    int iMPIini;
    MPI_Initialized(&iMPIini);
    if(iMPIini > 0) MPI_Finalize();
    exit(1);    
  }
  return d_Instance;
}

void MacroPartDistributor::resetLoadingDist()
{
  if( LM_->getMeasure() > 0){
    setLoadingDist();
  }
  //debug
  //LM_->printDistribution();
}

void MacroPartDistributor::setLoadingDist()
{
    int i,j;
    int j0 = 0;
    int j1 = 0;
    for( i = 0 ; i < nMPIsize_; i++){
      j1 = j0 + LM_->getNumberOfSlices(i);
      for( j = j0; j < j1; j++){
        node_id_4sl_[j] = i;
      }
      j0 = j1;
    }
    //-----------------------
    sliceIndMin_[0] = 0;
    sliceIndMax_[0] = sliceIndMin_[0] + LM_->getNumberOfSlices(0) - 1;
    for( i = 1 ; i < nMPIsize_; i++){
      sliceIndMin_[i] = sliceIndMax_[i-1] + 1;
      sliceIndMax_[i] = sliceIndMin_[i] + LM_->getNumberOfSlices(i) - 1;
    }
}


void MacroPartDistributor::findPhiExtrema(MacroPart &mp)
{
  //if the new longitudinal grid limits is changed 
  //more than by phiSenseRange_ relative value they 
  //should be recalculated

  int i;
  double phiMin,phiMax,phiMinGlob,phiMaxGlob;
  phiMin = mp._phi(1);
  phiMax = mp._phi(1);
  for( i = 2 ; i <= mp._nMacros; i++){
    if(phiMin > mp._phi(i)) phiMin = mp._phi(i);
    if(phiMax < mp._phi(i)) phiMax = mp._phi(i);
  }
  if(nMPIsize_ > 1){
    MPI_Allreduce(&phiMin,&phiMinGlob,1,MPI_DOUBLE,MPI_MIN,MPI_COMM_WORLD);
    MPI_Allreduce(&phiMax,&phiMaxGlob,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
    phiMin = phiMinGlob;
    phiMax = phiMaxGlob;
  }

  mp._phiMin = phiMin;
  mp._phiMax = phiMax;

  //cout.precision(15);
  //cout.width(15);
  //cout <<"debug MacroPartDistributor::findPhiExtrema initial phiMin phiMax="
  //     << phi_min_ <<" "<<  phi_max_ <<"\n";
  //cout <<"debug MacroPartDistributor::findPhiExtrema before extra phiMin phiMax="
  //     << phiMin <<" "<<phiMax<<"\n";

  double phiExtra = 0.0;

  if(nSlices_Total_ > 3) phiExtra = (phiMax - phiMin)/(nSlices_Total_ - 3);
  phiMax = phiMax + 2.0*phiExtra;
  phiMin = phiMin - 2.0*phiExtra;
  
  double phiWidth = phi_max_ - phi_min_;

  //cout <<"debug MacroPartDistributor::findPhiExtrema after extra phiMin phiMax="<< phiMin <<" "<<phiMax<<"\n";
  
  if( phi_min_ > phiMin ||
      phi_max_ < phiMax ||
      phiMin   > phi_min_ + phiSenseRange_*phiWidth ||
      phiMax   < phi_max_ - phiSenseRange_*phiWidth )
  {
    phiWidth = phiMax - phiMin;
    phi_min_ = phiMin - 0.5*phiSenseRange_*phiWidth;
    phi_max_ = phiMax + 0.5*phiSenseRange_*phiWidth;
    if(phi_min_ < -Consts::pi) phi_min_ = -Consts::pi;
    if(phi_max_ >  Consts::pi) phi_max_ =  Consts::pi;
    step_phi_ = (phi_max_ - phi_min_)/(nSlices_Total_-1);
  }

  //cout <<"debug MacroPartDistributor::findPhiExtrema final phiMin phiMax="
  //     << phi_min_ <<" "<< phi_max_ <<"\n";

  int nMacrosGlobal;
  int nMacros = mp._nMacros;
  nMacrosGlobal = nMacros;
  if(nMPIsize_ > 1){
    MPI_Allreduce(&nMacros,&nMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  }
  mp._globalNMacros = nMacrosGlobal;

}

double MacroPartDistributor::getPhiMax()
{
  return phi_max_;
}

double MacroPartDistributor::getPhiMin()
{
  return phi_min_;
}

double MacroPartDistributor::getPhiStep()
{
  return step_phi_;
}

int MacroPartDistributor::getSliceIndMin()
{
  return sliceIndMin_[nMPIrank_];
}

int MacroPartDistributor::getSliceIndMax()
{
  return sliceIndMax_[nMPIrank_];
}

int MacroPartDistributor::getPhiIndex(double phi)
{
  //PhiIndex - 0...nSlices_Total_ - 1 
  //it is different than ORBIT index 1 ... nSlices_Total_ 
  static int iInd;
  iInd = ((int) ((phi - phi_min_)/step_phi_ + 0.5));
  if(iInd < 1 ) {
    iInd = 0;
  }
  else{
    if(iInd > nSlices_Total_ -1 ) iInd = nSlices_Total_ - 1;
  }
  return iInd;
}

int MacroPartDistributor::getNodeIndex(int iPhiInd)
{
  return node_id_4sl_[iPhiInd];
}

int MacroPartDistributor::getNodeIndex(double phi)
{
  int iInd = getPhiIndex(phi);
  return node_id_4sl_[iInd];
}

void MacroPartDistributor::setPartNumbChunk(int partNumbChunkIn)
{
  partNumbChunk_ = partNumbChunkIn;
}

void MacroPartDistributor::resizeSendRecvBuffers(int send_size, int recv_size)
{
  //resize send buffer
  if( send_size > n_particles_send_buff_ || send_size < (n_particles_send_buff_ - partNumbChunk_)){
    n_particles_send_buff_ = send_size + partNumbChunk_/2;
    free(part_buffer_send_);
    part_buffer_send_ = (double*) malloc (sizeof(double)*n_particles_send_buff_*6);
  }

  //resize recv buffer
  if( recv_size > n_particles_recv_buff_ || recv_size < (n_particles_recv_buff_ -partNumbChunk_)){
    n_particles_recv_buff_ = recv_size + partNumbChunk_/2;
    free(part_buffer_recv_);
    part_buffer_recv_ = (double*) malloc (sizeof(double)*n_particles_recv_buff_*6);
  }
}

void MacroPartDistributor::resizePartIndBuffers(int nMacros)
{
  //resize to be deleted particles' index array buffer
  if( nMacros > n_part_del_  || nMacros < (n_part_del_ - partNumbChunk_)){
    n_part_del_ = nMacros + partNumbChunk_/2;
    free(ind_part_del_);
    ind_part_del_ = (int*) malloc (sizeof(int)*n_part_del_);
  } 
}


void MacroPartDistributor::distributeParticles(MacroPart &mp)
{
  int i,j;

  int nMacros = mp._nMacros;
  int nMacrosGlobal = nMacros;
  mp._globalNMacros = nMacrosGlobal;

  resetLoadingDist();

  findPhiExtrema(mp);

  if( nMPIsize_ < 2 ) return;

  MPI_Allreduce(&nMacros,&nMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD); 
  mp._globalNMacros = nMacrosGlobal;

  //Analysis of the Herd

  for ( i=0; i < nMPIsize_; i++){
    nodeStartIndex[i] = 0; 
    for ( j=0; j < nMPIsize_; j++) {
      table_exch_[i+nMPIsize_*j] = 0;
    }
  }

  int ip,id_cpu,ip_non_zero;
  double phi;
  ip_non_zero = 0;
  
  resizePartIndBuffers(nMacros);

  for(ip = 1; ip <= nMacros; ip++  ){
    phi = mp._phi(ip);
    id_cpu = getNodeIndex(phi);
    ind_part_del_[ip-1] = id_cpu;
    if(id_cpu != nMPIrank_){
      ip_non_zero = ip;
      table_exch_[nMPIrank_+nMPIsize_*id_cpu]++;
    }
  }

  MPI_Allreduce(table_exch_,table_exch_MPI_,nMPIsize_*nMPIsize_,MPI_INT,MPI_SUM,MPI_COMM_WORLD);  

  int recv_size = 0;
  int send_size = 0;

  int n_exch = 0;
  int n_exch_total = 0;

  //here i - node who sends j - node who receives
  for ( i=0; i < nMPIsize_; i++){
    for ( j=0; j < nMPIsize_; j++) {
      n_exch = table_exch_MPI_[i+nMPIsize_*j];
      n_exch_total += n_exch;
	if( i == nMPIrank_ ) send_size = send_size + n_exch;
	if( j == nMPIrank_ ) recv_size = recv_size + n_exch;
    }
  }

  if(n_exch_total == 0) return;

  //debug =================start=======================
  //if( nMPIrank_ == 0 ){
  //  for ( i=0; i < nMPIsize_; i++){
  //    std::cout<<"debug i="<<i<<" table= ";
  //  for ( j=0; j < nMPIsize_; j++){
  //    std::cout<<table_exch_MPI_[i+nMPIsize_*j]<<" ";
  //  }
  //    std::cout<<"\n";
  //  }
  //}
  //debug =================stop=======================


  //defining the  nodeStartIndex array

   nodeStartIndex[0]=0;
   for ( j=1; j < nMPIsize_; j++){
     nodeStartIndex[j] = nodeStartIndex[j-1] + table_exch_MPI_[nMPIrank_ + nMPIsize_*(j-1)];
   } 

  resizeSendRecvBuffers(send_size,recv_size);


  //define "send" buffer
  int part_ind;
  int node_ind;

  for(i=0;i< nMPIsize_;i++){numberOfPartToSend[i]=0;} 


  for(ip = 1; ip <= nMacros; ip++  ){
    node_ind = ind_part_del_[ip-1];
    if( node_ind != nMPIrank_){
      part_ind = 6*(numberOfPartToSend[node_ind]+ nodeStartIndex[node_ind]);
      part_buffer_send_[part_ind+0] = mp._x(ip);
      part_buffer_send_[part_ind+1] = mp._xp(ip);
      part_buffer_send_[part_ind+2] = mp._y(ip);
      part_buffer_send_[part_ind+3] = mp._yp(ip);
      part_buffer_send_[part_ind+4] = mp._deltaE(ip);
      part_buffer_send_[part_ind+5] = mp._phi(ip);
      numberOfPartToSend[node_ind]++;
    }
  }

  //========sending and receiving buffers process starts====================

  MPI_Status statusMPI ; 
  int k;
  int n_recived = 0;
  int n_sent    = 0;

  for(k=0; k < nMPIsize_*(nMPIsize_-1); k++){
   i = i_node_send_[k];
   j = j_node_recv_[k];
   if( table_exch_MPI_[i+nMPIsize_*j] != 0){
       if( i == nMPIrank_){
        MPI_Send(&part_buffer_send_[6*nodeStartIndex[j]], 
                 6*table_exch_MPI_[i+nMPIsize_*j], 
                 MPI_DOUBLE, j, 1111, MPI_COMM_WORLD);
        n_sent += table_exch_MPI_[i+nMPIsize_*j];
	//std::cout<<"debug node="<<i<<" sends to the node="<<j<<"\n";
       }
       if( j == nMPIrank_){
        MPI_Recv(&part_buffer_recv_[6*n_recived], 
                 6*table_exch_MPI_[i+nMPIsize_*j], 
                 MPI_DOUBLE, i , 1111 , MPI_COMM_WORLD, &statusMPI);
        n_recived += table_exch_MPI_[i+nMPIsize_*j];
        //std::cout<<"debug node="<<j<<" receive from the node="<<i<<"\n";
       }
   }   
  }
  //debug =================start=======================
  //  std::cout<<"debug rank="<<nMPIrank_<<" nMacro_ini="
  //           <<nMacros<<" send="<<n_sent<<" recived="
  //           <<n_recived<<"\n";
  //int n_recived_MPI_max,n_sent_MPI_max;
  //MPI_Allreduce(&n_recived,&n_recived_MPI_max,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
  //MPI_Allreduce(&n_sent,&n_sent_MPI_max,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
  //if( nMPIrank_ == 0 ){
  //  std::cout<<"NmacrGlobal="<< mp._globalNMacros  <<" send recv="<< n_sent_MPI_max <<" "
  //           << n_recived_MPI_max <<"\n";
  //}

  //debug =================stop========================

  //========sending and receiving buffers process stops=====================

  //update the Herd -----start----

    updateHerd(mp, n_sent, n_recived);

  //update the Herd -----stop-----

}

void MacroPartDistributor::distributeParticlesEvenly(MacroPart &mp)
{

  if(nMPIsize_ < 2) return;

   findPhiExtrema(mp);

   int nMacros = mp._nMacros;
   int nMacrosGlobal = mp._globalNMacros;

   for(int i = 0; i < nSlices_Total_; i++){
     nPartInSlice[i] = 0.;
   }

   int ip,id_cpu;
   double phi;
   for(ip = 1; ip <= nMacros; ip++  ){
     phi = mp._phi(ip);
     id_cpu = getPhiIndex(phi);
     nPartInSlice[id_cpu]++;
   }

   MPI_Allreduce(nPartInSlice,nPartInSlice_MPI,nSlices_Total_,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

   for(int i = 0; i < nSlices_Total_; i++){
     nPartInSlice[i] = nPartInSlice_MPI[i];
   }
   
   //initial distribution - it is not perfect!
   for(int ir = 0; ir < nMPIsize_; ir++){
       nSlices_Arr[ir] = nSlices_Total_/nMPIsize_;
       if(ir < nSlices_Total_ % nMPIsize_) nSlices_Arr[ir]++;
   }    
   
   //we will try to fix this distribution
   //to get ivenly distrubuted particles between CPUs
   for(int i = 0; i < nSlices_Total_; i++){
       
       int iCPU_from = -1;
       int delta_max = 0;
        for(int ir = 0; ir < nMPIsize_; ir++){
            if(nSlices_Arr[ir] > 1){
                int delta = 0;
                int i_sl_start = 0;
                for(int jr = 0; jr < ir; jr++){
                    i_sl_start += nSlices_Arr[jr];
                }    
                int i_sl_stop = i_sl_start + nSlices_Arr[ir];
                for(int i_sl = i_sl_start; i_sl < i_sl_stop; i_sl++){
                    delta += nPartInSlice[i_sl];
                }  
                if(  iCPU_from < 0){
                    delta_max = delta;
                    iCPU_from = ir;
                }    
                else{
                    if( delta > delta_max){
                       delta_max = delta;
                       iCPU_from = ir;                        
                    }    
                }       
            }    
        }    
       
       int iCPU_to = -1;
       int delta_min = 0;
        for(int ir = 0; ir < nMPIsize_; ir++){
                int delta = 0;
                int i_sl_start = 0;
                for(int jr = 0; jr < ir; jr++){
                    i_sl_start += nSlices_Arr[jr];
                }    
                int i_sl_stop = i_sl_start + nSlices_Arr[ir];
                for(int i_sl = i_sl_start; i_sl < i_sl_stop; i_sl++){
                    delta += nPartInSlice[i_sl];
                }  
                if(  iCPU_to < 0){
                    delta_min = delta;
                    iCPU_to = ir;
                }    
                else{
                    if( delta < delta_min){
                       delta_min = delta;
                       iCPU_to = ir;                        
                    }    
                }          
        }    
       
       if(iCPU_to >= 0 && iCPU_from >= 0 && iCPU_to != iCPU_from){
           nSlices_Arr[iCPU_from]--;
           nSlices_Arr[iCPU_to]++;
       }         
   }


   //std::cout<<"debug before n_part="<< mp._nMacros <<" Nslices="<< LM_->getNumberOfSlices()<<std::endl;

   LM_->setNumberOfSlices(nSlices_Arr, nMPIsize_);

   setLoadingDist();

   distributeParticles(mp);  

   //std::cout<<"debug after n_part="<< mp._nMacros <<" Nslices="<< LM_->getNumberOfSlices()<<std::endl;
    
}



void MacroPartDistributor::updateHerd(MacroPart &mp, int n_sent, int n_recived){
  if(n_sent == 0 && n_recived == 0) return;

  int ip,ip1,ip2;
  int ind_p;
  int count_part;
  int mpSize = mp._x.rows();
  int nMacros = mp._nMacros;

  if(mpSize < (nMacros + n_recived - n_sent)){
      mp._resize(nMacros + n_recived - n_sent - mpSize + 100);
  }

  //case - n_sent == 0  -----start--------------------
  if(n_sent == 0){
    for(ip=0; ip <  n_recived; ip++){
      ind_p = 6*ip;
      mp._insertMacroPart(
			  part_buffer_recv_[ind_p + 0],
			  part_buffer_recv_[ind_p + 1],
			  part_buffer_recv_[ind_p + 2],
			  part_buffer_recv_[ind_p + 3],
			  part_buffer_recv_[ind_p + 4],
			  part_buffer_recv_[ind_p + 5]
                          );
    }
    mp._nMacros= nMacros - (n_sent - n_recived);
    return;
  }
  //case - n_sent == 0  -----stop---------------------

  //case - n_sent < n_recived -----start--------------
  if( n_sent <= n_recived ){
    count_part = 0;
    for(ip=0; ip < nMacros ; ip++){
      if(ind_part_del_[ip] != nMPIrank_){
	ip1 = ip + 1;
        ind_p = 6*count_part;
        mp._x(ip1)     =part_buffer_recv_[ind_p + 0];
        mp._xp(ip1)    =part_buffer_recv_[ind_p + 1];
        mp._y(ip1)     =part_buffer_recv_[ind_p + 2];
        mp._yp(ip1)    =part_buffer_recv_[ind_p + 3];
        mp._deltaE(ip1)=part_buffer_recv_[ind_p + 4];
        mp._phi(ip1)   =part_buffer_recv_[ind_p + 5];
        mp._foilHits(ip1) =0;
        mp._dp_p(ip1) = mp._deltaE(ip1) * mp._syncPart._dppFac;
        count_part++;
      }
    }
    for(ip = count_part ; ip <  n_recived; ip++){
      ind_p = 6*ip;
      mp._insertMacroPart(
			  part_buffer_recv_[ind_p + 0],
			  part_buffer_recv_[ind_p + 1],
			  part_buffer_recv_[ind_p + 2],
			  part_buffer_recv_[ind_p + 3],
			  part_buffer_recv_[ind_p + 4],
			  part_buffer_recv_[ind_p + 5]
                          );
    }
    mp._nMacros= nMacros - (n_sent - n_recived);
    return;
  }
  //case - n_sent < n_recived -----stop--------------- 

  //case - n_sent > n_recived -----start--------------
  if( n_sent > n_recived ){
    int ip_start = 0;
    count_part = 0;
    for(ip=0; ip < nMacros ; ip++){ 
      if(ind_part_del_[ip] != nMPIrank_){
        if(count_part == n_recived ) { ip_start = ip;  break;}
	ip1 = ip + 1;
        ind_p = 6*count_part;
        mp._x(ip1)     =part_buffer_recv_[ind_p + 0];
        mp._xp(ip1)    =part_buffer_recv_[ind_p + 1];
        mp._y(ip1)     =part_buffer_recv_[ind_p + 2];
        mp._yp(ip1)    =part_buffer_recv_[ind_p + 3];
        mp._deltaE(ip1)=part_buffer_recv_[ind_p + 4];
        mp._phi(ip1)   =part_buffer_recv_[ind_p + 5];
        mp._foilHits(ip1) =0;
        mp._dp_p(ip1) = mp._deltaE(ip1) * mp._syncPart._dppFac;
        count_part++;
      }
    }

    int i_shift = 0;
    ip = ip_start;
    while((ip + i_shift) < nMacros){
      while(ind_part_del_[ip+i_shift] != nMPIrank_ && (ip + i_shift) < nMacros){
        i_shift++;
      }
      if(ip + i_shift == nMacros) break;
        ip1 = ip+1;
        ip2 = ip+1 + i_shift;
        if( ip1 != ip2 ){
         mp._x(ip1)     = mp._x(ip2);
         mp._xp(ip1)    = mp._xp(ip2);
         mp._y(ip1)     = mp._y(ip2);
         mp._yp(ip1)    = mp._yp(ip2);
         mp._deltaE(ip1)= mp._deltaE(ip2);
         mp._phi(ip1)   = mp._phi(ip2);
         mp._foilHits(ip1) = mp._foilHits(ip2);
         mp._dp_p(ip1) = mp._dp_p(ip2); 
	}
      ip++;
    }

  //debug =================start=======================
  //  std::cout<<"debug rank="<<nMPIrank_<<" ip="
  //	       <<ip
  //	       <<" i_shift="<<i_shift
  //           <<"\n";
  //debug =================stop======================== 

    nMacros = nMacros - (n_sent - n_recived);
    mp._nMacros= nMacros;
    return; 
  }
  //case - n_sent > n_recived -----stop---------------

}


void MacroPartDistributor::defSendRecvSequence()
{
  //this method prepares the tables i_node_send_ and j_node_recv_ defining 
  //the sequence of the exchanging of data

  if(nMPIsize_ == 1){
    i_node_send_[0]=0;
    j_node_recv_[0]=0;
    return;
  }


  int* table_m = (int*) malloc (sizeof(int)*nMPIsize_*nMPIsize_);
  int* mask = (int*) malloc (sizeof(int)*nMPIsize_);
   
  int i,j,k;
  int count = 0;
  int inf = 0;

  for(i=0;i<nMPIsize_;i++){
      mask[i]=0;
  for(j=0;j<nMPIsize_;j++){
      table_m[i+nMPIsize_*j]=0;
  }}

  for(k=0; k < nMPIsize_/2 ;k++){
    i = 2*k;
    j = i+1;
     if(i < nMPIsize_ && j < nMPIsize_ && table_m[i+nMPIsize_*j] == 0){
       i_node_send_[count]=i;
       j_node_recv_[count]=j;
       table_m[i+nMPIsize_*j]++;
       count++;
     }     
  }

  for(k=0; k < nMPIsize_/2 ;k++){
    j = 2*k;
    i = j+1;
    if(i < nMPIsize_ && j < nMPIsize_ && table_m[i+nMPIsize_*j] == 0){
      i_node_send_[count]=i;
      j_node_recv_[count]=j;
      table_m[i+nMPIsize_*j]++;
      count++;
    }     
  }

  if( nMPIsize_%2 != 0 ){
    i= nMPIsize_ - 2;
    j= i+1;
    if(i < nMPIsize_ && j < nMPIsize_ && table_m[i+nMPIsize_*j] == 0){
      i_node_send_[count]=i;
      j_node_recv_[count]=j;
      table_m[i+nMPIsize_*j]++;
      count++;
    }
    j= nMPIsize_ - 2;
    i= j+1;
    if(i < nMPIsize_ && j < nMPIsize_ && table_m[i+nMPIsize_*j] == 0){
      i_node_send_[count]=i;
      j_node_recv_[count]=j;
      table_m[i+nMPIsize_*j]++;
      count++;
    }        
  }

 
  i= nMPIsize_ - 1;
  j= 0;
  if(i < nMPIsize_ && j < nMPIsize_ && table_m[i+nMPIsize_*j] == 0){
      i_node_send_[count]=i;
      j_node_recv_[count]=j;
      table_m[i+nMPIsize_*j]++;
      count++;
  }     

  j= nMPIsize_ - 1;
  i= 0;
  if(i < nMPIsize_ && j < nMPIsize_ && table_m[i+nMPIsize_*j] == 0){
      i_node_send_[count]=i;
      j_node_recv_[count]=j;
      table_m[i+nMPIsize_*j]++;
      count++;
  }     

  while(count < nMPIsize_*(nMPIsize_-1)) {
    
      for(i=0;i<nMPIsize_;i++){mask[i]=0;}

      for(i=0;i<nMPIsize_;i++){
      for(j=0;j<nMPIsize_;j++){
	if(table_m[i+nMPIsize_*j] == 0 && i != j){
           inf = 0;
           for(k=0;k<nMPIsize_;k++){
	     if(mask[i] != 0) inf = 1;
	     if(mask[j] != 0) inf = 1;
	   }
           if(inf == 0){
             i_node_send_[count]=i;
             j_node_recv_[count]=j;
             mask[i]=1;
             mask[j]=1;
             table_m[i+nMPIsize_*j]++;
             count++;
	   }
	}
      }
      }
    }

  //for(i=0;i< nMPIsize_*(nMPIsize_-1) ;i++){ 
  // std::cout<<" n="<<i<<" "<<"i j ="<<i_node_send_[i]<<" "<<j_node_recv_[i]<<"\n";
  //}

  //for(i=0;i<nMPIsize_;i++){   
  //  std::cout <<"i="<<i<<" mat=";
  //for(j=0;j<nMPIsize_;j++){   std::cout <<table_m[i+nMPIsize_*j]<<" ";}
  //  std::cout<<"\n";
  //}

    free(table_m);
    free(mask);
}
