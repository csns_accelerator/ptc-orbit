#if !defined(__SCCylCoords__)
#define __SCCylCoords__

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <mpi.h>
#include <vector>
#include <complex>

#include "Object.h"
#include "MacroPart.h"

using namespace std;


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   CylCoordsCalculator
//
// INHERITANCE RELATIONSHIPS
//   None
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   Class for calculating space charge kicks
//   in cylindrical coordinates with a
//   circular conducting wall boundary
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class CylCoordsCalculator : public Object
{
  Declare_Standard_Members(CylCoordsCalculator, Object);

  public:

  // Constructor

  CylCoordsCalculator(int nZBins, int nRBins, int nThetas, int nModes,
                      double ZLength, double RBnd) :
                      _nZBins(nZBins), _nRBins(nRBins),
                      _nThetas(nThetas), _nModes(nModes),
                      _ZLength(ZLength), _RBnd(RBnd)
  {
    int l, m;

    _ZDist    = new double[_nZBins];
    _Radius   = new double[_nRBins + 1];
    _Theta    = new double[_nThetas];
    _dA       = new double[_nRBins];

    _L        = new double*[_nModes + 1];
    _D        = new double*[_nModes + 1];
    _U        = new double*[_nModes + 1];
    _cs       = new double*[_nModes + 1];
    _sn       = new double*[_nModes + 1];

    _RhoGrid  = new double*[_nThetas];
    _RhoModes = new double*[2 * _nModes + 1];
    _Pot      = new double*[2 * _nModes + 1];
    _kRGrid   = new double*[_nThetas];
    _kTHGrid  = new double*[_nThetas];
    _kxGrid   = new double*[_nThetas];
    _kyGrid   = new double*[_nThetas];

    for(m = 0; m < _nModes + 1; m++)
    {
      _L[m]        = new double[_nRBins];
      _D[m]        = new double[_nRBins];
      _U[m]        = new double[_nRBins];
      _cs[m]       = new double[_nThetas];
      _sn[m]       = new double[_nThetas];
    }

    for(l = 0; l < _nThetas; l++)
    {
      _RhoGrid[l]  = new double[_nRBins + 1];
      _kRGrid[l]   = new double[_nRBins + 1];
      _kTHGrid[l]  = new double[_nRBins + 1];
      _kxGrid[l]   = new double[_nRBins + 1];
      _kyGrid[l]   = new double[_nRBins + 1];
    }

    for(m = 0; m < 2 * _nModes + 1; m++)
    {
      _RhoModes[m] = new double[_nRBins + 1];
      _Pot[m]      = new double[_nRBins + 1];
    }

    _SetUp();
  }

  // Destructor

  ~CylCoordsCalculator()
  {
    int l, m;

    delete _ZDist;
    delete _Radius;
    delete _Theta;
    delete _dA;

    for(m = 0; m < _nModes + 1; m++)
    {
      delete _L[m];
      delete _D[m];
      delete _U[m];
      delete _cs[m];
      delete _sn[m];
    }

    for(l = 0; l < _nThetas; l++)
    {
      delete _RhoGrid[l];
      delete _kRGrid[l];
      delete _kTHGrid[l];
      delete _kxGrid[l];
      delete _kyGrid[l];
    }

    for(m = 0; m < 2 * _nModes + 1; m++)
    {
      delete _RhoModes[m];
      delete _Pot[m];
    }

    delete[] _L;
    delete[] _D;
    delete[] _U;
    delete[] _cs;
    delete[] _sn;
    delete[] _RhoGrid;
    delete[] _RhoModes;
    delete[] _Pot;
    delete[] _kRGrid;
    delete[] _kTHGrid;
    delete[] _kxGrid;
    delete[] _kyGrid;
  }

  // Variables and Methods

  double _pi;
  double _twopi;

  int _nZBins;
  int _nRBins;
  int _nThetas;
  int _nModes;

  double _ZLength;
  double _RBnd;

  double _ZCenter;
  double _ZSum;
  double _dZ;
  double _dR;
  double _dTH;
  double _dTHo2;

  double* _ZDist;
  double* _Radius;
  double* _Theta;
  double* _dA;

  double** _L;
  double** _D;
  double** _U;
  double** _cs;
  double** _sn;
  double** _RhoGrid;
  double** _RhoModes;
  double** _Pot;
  double** _kRGrid;
  double** _kTHGrid;
  double** _kxGrid;
  double** _kyGrid;

  void _SetUp();
  void _DistributeParts(MacroPart& mp);
  void _GetPotential();
  void _ApplyForce1(MacroPart& mp, double SCKick_Coeff);
  void _ApplyForce2(MacroPart& mp, double SCKick_Coeff);
};

#endif   // __SCCylCoords__
