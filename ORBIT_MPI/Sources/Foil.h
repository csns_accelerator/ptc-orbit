///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    Foil
//
// INHERITANCE RELATIONSHIPS
//    Foil -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing foil information.
//
// PUBLIC MEMBERS
//   _xMin  - The minimum X position (mm)
//   _xMax  - The maximum X position (mm)
//   _yMin  - The minimum Y position (mm)
//   _yMax  - The maximum Y position (mm)
//   _thick - foil thickness (micro-gram/cm2)
//     
//
// PROTECTED MEMBERS
//  
//   _thetaScatMin  - minimum scatering angle [rad]
//
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class Foil : public Node
{
  Declare_Standard_Members(Foil, Node);

  public:

  Foil(const String &n, const Integer &order,
       const Real &xmin, const Real &xmax, const Real &ymin,
       const Real &ymax, const Real &thick) :
       Node(n, 0., order), _xMin(xmin), _xMax(xmax),
       _yMin(ymin), _yMax(ymax), _thick(thick)
  {
    crossEn.resize(57);
    crossEl.resize(57);
    crossInel.resize(57);
    crossRuth.resize(57);

    crossEn(1)=0.0005; crossEn(2)=.001; crossEn(3)=.0015;
    crossEn(4)=0.002; crossEn(5)=.0025; crossEn(6)=.003;
    crossEn(7)=0.0035; crossEn(8)=.004; crossEn(9)=.0045;
    crossEn(10)=0.005; crossEn(11)=.0055; crossEn(12)=.006;
    crossEn(13)=0.0065; crossEn(14)=.007; crossEn(15)=.0075;
    crossEn(16)=0.008; crossEn(17)=.009; crossEn(18)=.01;
    crossEn(19)=0.011; crossEn(20)=.012; crossEn(21)=.013;
    crossEn(22)=0.014; crossEn(23)=.015; crossEn(24)=.0175;
    crossEn(25)=0.02; crossEn(26)=.0225; crossEn(27)=.025;
    crossEn(28)=.030; crossEn(29)=0.035; crossEn(30)=.040;
    crossEn(31)=.045; crossEn(32)=.050; crossEn(33)=.0510;
    crossEn(34)=.055; crossEn(35)=.060; crossEn(36)=.070;
    crossEn(37)=.080; crossEn(38)=.090; crossEn(39)=.100;
    crossEn(40)=.110; crossEn(41)=.120; crossEn(42)=.140;
    crossEn(43)=.160; crossEn(44)=.180; crossEn(45)=.200;
    crossEn(46)=.225; crossEn(47)=.250; crossEn(48)=.275;
    crossEn(49)=.300; crossEn(50)=.325; crossEn(51)=.350;
    crossEn(52)=.375; crossEn(53)=.400; crossEn(54)=.500;
    crossEn(55)=.700; crossEn(56)=1.000; crossEn(57)=1.500;

    crossEl(1)=0.; crossEl(2)=0.; crossEl(3)=0.;
    crossEl(4)=0.; crossEl(5)=0.; crossEl(6)=.321;
    crossEl(7)=.335; crossEl(8)=.349; crossEl(9)=.363;
    crossEl(10)=.377; crossEl(11)=.386; crossEl(12)=.395;
    crossEl(13)=.404; crossEl(14)=.413; crossEl(15)=.423;
    crossEl(16)=.434; crossEl(17)=.456; crossEl(18)=.479;
    crossEl(19)=.509; crossEl(20)=.539; crossEl(21)=.569;
    crossEl(22)=.598; crossEl(23)=.628; crossEl(24)=.685;
    crossEl(25)=.743; crossEl(26)=.783; crossEl(27)=.822;
    crossEl(28)=.878; crossEl(29)=.915; crossEl(30)=.938;
    crossEl(31)=.813; crossEl(32)=.688; crossEl(33)=.678;
    crossEl(34)=.641; crossEl(35)=.594; crossEl(36)=.515;
    crossEl(37)=.448; crossEl(38)=.392; crossEl(39)=.345;
    crossEl(40)=.305; crossEl(41)=.272; crossEl(42)=.214;
    crossEl(43)=.167; crossEl(44)=.138; crossEl(45)=.117;
    crossEl(46)=.098; crossEl(47)=.085; crossEl(48)=.077;
    crossEl(49)=.072; crossEl(50)=.068; crossEl(51)=.067;
    crossEl(52)=.066; crossEl(53)=.067; crossEl(54)=.077;
    crossEl(55)=.09; crossEl(56)=.102; crossEl(57)=.108;

    crossInel(1)=0.; crossInel(2)=0.; crossInel(3)=0.;
    crossInel(4)=0.; crossInel(5)=0.; crossInel(6)=0.;
    crossInel(7)=0.; crossInel(8)=0.; crossInel(9)=0.;
    crossInel(10)=.013; crossInel(11)=.078; crossInel(12)=.131;
    crossInel(13)=.175; crossInel(14)=.212; crossInel(15)=.244;
    crossInel(16)=.271; crossInel(17)=.314; crossInel(18)=.346;
    crossInel(19)=.370; crossInel(20)=.389; crossInel(21)=.403;
    crossInel(22)=.413; crossInel(23)=.421; crossInel(24)=.432;
    crossInel(25)=.434; crossInel(26)=.432; crossInel(27)=.427;
    crossInel(28)=.412; crossInel(29)=.394; crossInel(30)=.376;
    crossInel(31)=.359; crossInel(32)=.344; crossInel(33)=.288;
    crossInel(34)=.281; crossInel(35)=.272; crossInel(36)=.257;
    crossInel(37)=.245; crossInel(38)=.235; crossInel(39)=.228;
    crossInel(40)=.223; crossInel(41)=.220; crossInel(42)=.223;
    crossInel(43)=.213; crossInel(44)=.212; crossInel(45)=.212;
    crossInel(46)=.212; crossInel(47)=.213; crossInel(48)=.214;
    crossInel(49)=.216; crossInel(50)=.217; crossInel(51)=.219;
    crossInel(52)=.221; crossInel(53)=.223; crossInel(54)=.229;
    crossInel(55)=.246; crossInel(56)=.261; crossInel(57)=.261;

    crossRuth(1)=0.0; crossRuth(2)=0.0; crossRuth(3)=0.0;
    crossRuth(4)=0.0; crossRuth(5)=0.0; crossRuth(6)=0.0;
    crossRuth(7)=0.0; crossRuth(8)=0.0; crossRuth(9)=0.0;
    crossRuth(10)=0.0; crossRuth(11)=0.0; crossRuth(12)=0.0;
    crossRuth(13)=0.0; crossRuth(14)=0.0; crossRuth(15)=0.0;
    crossRuth(16)=0.0; crossRuth(17)=0.0; crossRuth(18)=0.0;
    crossRuth(19)=0.0; crossRuth(20)=0.0; crossRuth(21)=0.0;
    crossRuth(22)=0.0; crossRuth(23)=0.0; crossRuth(24)=0.0;
    crossRuth(25)=0.00167; crossRuth(26)=0.00190; crossRuth(27)=0.00211;
    crossRuth(28)=0.00250; crossRuth(29)=0.00285; crossRuth(30)=0.00316;
    crossRuth(31)=0.00344; crossRuth(32)=0.00369; crossRuth(33)=0.00373;
    crossRuth(34)=0.00391; crossRuth(35)=0.00411; crossRuth(36)=0.00446;
    crossRuth(37)=0.00476; crossRuth(38)=0.00500; crossRuth(39)=0.00522;
    crossRuth(40)=0.00540; crossRuth(41)=0.00557; crossRuth(42)=0.00584;
    crossRuth(43)=0.00606; crossRuth(44)=0.00624; crossRuth(45)=0.00640;
    crossRuth(46)=0.00656; crossRuth(47)=0.00670; crossRuth(48)=0.00680;
    crossRuth(49)=0.00690; crossRuth(50)=0.00698; crossRuth(51)=0.00706;
    crossRuth(52)=0.00712; crossRuth(53)=0.00718; crossRuth(54)=0.00735;
    crossRuth(55)=0.00757; crossRuth(56)=0.00773; crossRuth(57)=0.00785;

  }

  virtual Void nameOut(String &wname) { wname = _name; }
  virtual Void _updatePartAtNode(MacroPart &mp);
  virtual Void _nodeCalculator(MacroPart &mp);

  Real _xMin, _xMax, _yMin, _yMax, _thick;
  Vector(Real) crossEn, crossEl, crossInel, crossRuth;

  protected:
};
