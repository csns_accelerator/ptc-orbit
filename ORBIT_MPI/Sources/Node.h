//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    Node.h
//
// AUTHOR
//    J. Galambos
//
// CREATED
//    8/7/97
//
//
// DESCRIPTION
//    Specification and inline functions for a class used  to
//    contain Element information about Ring components. 
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#if !defined(__Node__)
#define __Node__

#include "Object.h"
#include "MacroPart.h"
#include "StrClass.h"
#include "RealMat.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    Node
//
// INHERITANCE RELATIONSHIPS
//    Node -> Object -> IOSystem
//
// USING/CONTAINING RELATIONSHIPS
//    Object (U), BSpline (C)
//
// DESCRIPTION
//    An abstract class for storing Node information. 
//
// PUBLIC MEMBERS
//    Node:        Constructor for making Node objects
//    ~Node:       Destructor for the Node class.
//    _name        Name of node
//    _oindex      Order index
//    _position    Poistion in ring of the end of the node (m)
//    _length      Length of node (m)
//    _nodeCalculator    Routine to do preliminary calculation for this Node.
//    _updatePartAtNode  Routine to do macroparticle updates
//    _nameOut     Routine to return the node name
//
//  Integer
//    _nodeError   If == 0, calculation worked (default)
//                 If == -1, calc failed, bail out
//  String        
//    _errMessage  A    message describing the error.
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class Node : public Object {
  Declare_Standard_Members(Node, Object);
public:
  
  Node(const String &n, const Real &l, const Integer &oindex)
          : _name(n), _length(l), _oindex(oindex) 
    {
     _nodeError = 0;
    }
  virtual ~Node();

    virtual Void nameOut(String &wname)=0;
    String _name;
    Real _length, _position;
    Integer _oindex;
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Void _updatePartAtNode(MacroPart &mp)=0; 
    Integer _nodeError;
    String _errMessage;   

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
