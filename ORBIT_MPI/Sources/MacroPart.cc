//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MacroPart.cc
//
// AUTHOR
//    J. Galambos
//
// CREATED
//    8/7/97
//
// DESCRIPTION
//    Static and non-inline functions for for a class used to 
//    keep track of macro particles
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include "MacroPart.h"
#include "Consts.h"
 
#if defined(macintosh)
   #pragma segment MacroPart
#endif


///////////////////////////////////////////////////////////////////////////
//
// STANDARD AND STATIC MEMBER DEFINITIONS FOR MacroPart AND RELATED CLASSES
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(SyncPart, Object);

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PUBLIC MEMBER FUNCTIONS FOR CLASS SyncPart
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    SyncPart::SyncPart
//    SyncPart::~SyncPart
//
// DESCRIPTION
//    Constructor and destructor for the SyncPart class. 
//                
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

SyncPart::SyncPart(const Real& m, const Integer& c, Real &E)
{
    _mass = m;
    _charge = c ; // * Consts::eCharge;
    _eKinetic = E;
    _e0 = m * 0.93827231;
    _eTotal = _e0 + _eKinetic;
    _gammaSync = 1. + _eKinetic/_e0;
    _betaSync = Sqrt(1. - 1./Sqr(_gammaSync));
    _dppFac = 1./(Sqr(_betaSync)*_eTotal);
    _phase = 0.;
}

SyncPart::~SyncPart()
{
}

Void SyncPart::update(const Real& m, const Integer& c, Real &E)
{
    _mass = m;
    _charge = c ; // * Consts::eCharge;
    _eKinetic = E;
    _e0 = m * 0.93827231;
    _eTotal = _e0 + _eKinetic;
    _gammaSync = 1. + _eKinetic/_e0;
    _betaSync = Sqrt(1. - 1./Sqr(_gammaSync));
    _dppFac = 1./(Sqr(_betaSync)*_eTotal);
    _phase = 0.;
}

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PUBLIC MEMBER FUNCTIONS FOR CLASS LostMacroPart
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(LostMacroParts, Object);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    LostMacroParts::LostMacroParts
//    LostMacroParts::~LostMacroParts
//
// DESCRIPTION
//    Constructor and destructor for the LostMacroPart class. 
//                
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

LostMacroParts::LostMacroParts()
{
  _nLostMacros = 0;
}

LostMacroParts::~LostMacroParts()
{
}

Define_Standard_Members(MacroPart, Object);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MacroPart::MacroPart
//
// DESCRIPTION
//    Constructors for the MacroPart class. 
//                
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////
MacroPart::MacroPart(SyncPart &s, Integer &chunkSize, const String  &name) : _syncPart(s)
{
         _nMacros = 0;
         _resize(chunkSize);
         _feelsHerds=0;_longBinningDone = 0;
         _currentNode = 1;
         _nTurnsDone = 0; _nPartTurnsDone = 0;
         _bunchFactor = 1.;
         _herdName = name;
}

MacroPart::MacroPart(SyncPart &s, const Integer &chunkSize, const String &name) :_syncPart(s)
{
         _nMacros = 0;
         _resize(chunkSize);
         _feelsHerds=0;_longBinningDone = 0;
         _currentNode = 1;
         _nTurnsDone = 0; _nPartTurnsDone = 0;
         _bunchFactor = 1.;
         _herdName = name;
}
  
///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MacroPart::~MacroPart
//
// DESCRIPTION
//    Destructor for the MacroPart class. 
//    Currently doesn't need to do anything.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

MacroPart::~MacroPart()
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MacroPart::_addLostMacro
//
// DESCRIPTION
//    Moves a particular macro from the herd to the LostMacroPart
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MacroPart::_addLostMacro(const Integer &i, const Real &LPos)
{

   _lostOnes._nLostMacros++;
   if(_lostOnes._nLostMacros >= _lostOnes._xLost.rows())
     {
       _lostOnes._xLost.resize(_lostOnes._nLostMacros + 10);
       _lostOnes._xpLost.resize(_lostOnes._nLostMacros + 10);
       _lostOnes._yLost.resize(_lostOnes._nLostMacros + 10);
       _lostOnes._ypLost.resize(_lostOnes._nLostMacros + 10);
       _lostOnes._phiLost.resize(_lostOnes._nLostMacros + 10);
       _lostOnes._deltaELost.resize(_lostOnes._nLostMacros + 10);
       _lostOnes._LPosLost.resize(_lostOnes._nLostMacros + 10);
       _lostOnes._nodeLost.resize(_lostOnes._nLostMacros + 10);
       _lostOnes._turnLost.resize(_lostOnes._nLostMacros + 10);
     }

   // store some info on it:

   _lostOnes._xLost(_lostOnes._nLostMacros) = _x(i);
   _lostOnes._xpLost(_lostOnes._nLostMacros) = _xp(i);
   _lostOnes._yLost(_lostOnes._nLostMacros) = _y(i);
   _lostOnes._ypLost(_lostOnes._nLostMacros) = _yp(i);
   _lostOnes._phiLost(_lostOnes._nLostMacros) = _phi(i);
   _lostOnes._deltaELost(_lostOnes._nLostMacros) = _deltaE(i);
   _lostOnes._LPosLost(_lostOnes._nLostMacros) = LPos;
   _lostOnes._nodeLost(_lostOnes._nLostMacros) = _currentNode;
   _lostOnes._turnLost(_lostOnes._nLostMacros) = _nTurnsDone;

   // pull it from the herd:

   for(Integer j=i; j < _nMacros; j++)
     {
        _x(j) = _x(j+1);
        _xp(j) = _xp(j+1);
        _y(j) = _y(j+1);
        _yp(j) = _yp(j+1);
        _phi(j) = _phi(j+1);
        _deltaE(j) = _deltaE(j+1);
        _dp_p(j) = _dp_p(j+1);
        _LPositionIndex(j) = _LPositionIndex(j+1);
        _fractLPosition(j) = _fractLPosition(j+1);
        _LPosFactor(j) = _LPosFactor(j+1);    
        _foilHits(j) = _foilHits(j+1);
        _xFractBin(j)  = _xFractBin(j+1); 
        _yFractBin(j) = _yFractBin(j+1);
        _xBin(j) = _xBin(j+1); 
        _yBin(j) = _yBin(j+1);
     }
     _nMacros--;

}
///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MacroPart::_resize
//
// DESCRIPTION
//    Resizes the vectors for  the macro particles.
//
// PARAMETERS
//    chunkSize - amount to  increase vector sizes by.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MacroPart::_resize(const Integer &chunkSize)
{

  if(chunkSize <1) return;
  _x.resize(_nMacros + chunkSize);
  _xp.resize(_nMacros + chunkSize);
  _y.resize(_nMacros + chunkSize);
  _yp.resize(_nMacros + chunkSize);
  _phi.resize(_nMacros + chunkSize);
  _deltaE.resize(_nMacros + chunkSize);
  _dp_p.resize(_nMacros + chunkSize);
  _LPositionIndex.resize(_nMacros + chunkSize);
  _fractLPosition.resize(_nMacros + chunkSize);
  _xFractBin.resize(_nMacros + chunkSize);
  _yFractBin.resize(_nMacros + chunkSize);
  _xBin.resize(_nMacros + chunkSize);
  _yBin.resize(_nMacros + chunkSize);
  _LPosFactor.resize(_nMacros + chunkSize);
  _foilHits.resize(_nMacros + chunkSize);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MacroPart::_insertMacroPart
//
// DESCRIPTION
//    Inserts information into MacroPart Vectors for a single particle
//
// PARAMETERS
//  ...
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MacroPart::_insertMacroPart(const Real& x, const Real& xp, const Real& y, 
                      const Real& yp)
        {
           _nMacros++;

	   //check the size  ===start====
           int mpSize = _x.rows();
           if(mpSize <= _nMacros ){
	     _resize(1000);
	   }
	   //check the size  ===stop=====

          _x(_nMacros) = x;
          _xp(_nMacros) = xp;
          _y(_nMacros) = y;
          _yp(_nMacros) = yp;
          _phi(_nMacros)=0.;
          _deltaE(_nMacros)=0.;
          _dp_p(_nMacros) = 0.;
          _foilHits(_nMacros) =0;
        }
            
 Void MacroPart::_insertMacroPart(const Real& x, const Real& xp, const Real& y,
             const Real& yp, const Real &dE, const Real &phi)
        {
           _nMacros++; 

	   //check the size  ===start====
           int mpSize = _x.rows();
           if(mpSize <= _nMacros ){
	     _resize(1000);
	   }
	   //check the size  ===stop=====           

          _x(_nMacros) = x;
          _xp(_nMacros) = xp;
          _y(_nMacros) = y;
          _yp(_nMacros) = yp;
          _deltaE(_nMacros) = dE;
          _phi(_nMacros) = phi;
          _foilHits(_nMacros) =0;
          _dp_p(_nMacros) = _deltaE(_nMacros) * _syncPart._dppFac;
        }


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MacroPart::_findXYPExtrema
//
// DESCRIPTION
//    Finds min and max of phase space parameters x,y,and phi
//
// PARAMETERS
//  None
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MacroPart::_findXYPExtrema()

{
   Integer i;

    _xMax = _x(1);
    _yMax = _y(1);
    _xMin = _x(1);
    _yMin = _y(1);
    _phiMin = _phi(1);
    _phiMax = _phi(1);

    for (i=2; i <= _nMacros; i++)
    {
      if( _x(i) > _xMax) _xMax = _x(i);
      if( _y(i) > _yMax) _yMax = _y(i);
      if( _x(i) < _xMin) _xMin = _x(i);
      if( _y(i) < _yMin) _yMin = _y(i);
      if(_phi(i) > _phiMax)  _phiMax = _phi(i);
      if(_phi(i) < _phiMin)  _phiMin = _phi(i);
    }

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MacroPart::_findDEExtrema
//
// DESCRIPTION
//    Finds min and max of phase space parameters delta E
//
// PARAMETERS
//  None
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MacroPart::_findDEExtrema()

{
   Integer i;

    _dEMax = _deltaE(1);
    _dEMin = _deltaE(1);

    for (i=2; i <= _nMacros; i++)
    {
      if( _deltaE(i) > _dEMax) _dEMax = _deltaE(i);
      if( _deltaE(i) < _dEMin) _dEMin = _deltaE(i);
    }

}



