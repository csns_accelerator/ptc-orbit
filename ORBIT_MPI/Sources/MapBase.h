
///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    MapBase
//
// INHERITANCE RELATIONSHIPS
//    MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a base class for storing map information.
//
// PUBLIC MEMBERS
//
// Real:
//   _betaX  The horizontal beta value at the beginning of the Node [m]
//   _betaY  The verticalbeta value at the beginning of the Node [m]
//   _alphaX The horizontal alpha value at the beginning of the Node
//   _alphaY The horizontal alpha  value at the beginning of the Node
//   _etaX   The horizontal dispersion [m]
//   _etaPX  The horizontal dispersion prime
//   _et     Element Type
//
// PROTECTED MEMBERS
//  None
//
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

#if !defined(__Map__)
#define __Map__

#include "Object.h"

class MapBase : public Node {
  Declare_Standard_Members(MapBase, Node);
public:
    MapBase(const String &n, const Integer &order,
            const Real &bx, const Real &by, const Real &ax,
            const Real &ay, const Real &ex, const Real &epx,
            const Real &l, const String &et) : Node(n, l, order),
            _betaX(bx), _betaY(by), _alphaX(ax), _alphaY(ay),
            _etaX(ex), _etaPX(epx), _et(et)
    {
    }

    Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;

    Real _betaX, _betaY, _alphaX, _alphaY, _etaX, _etaPX;
    String _et;
};

#endif   // __Map__

