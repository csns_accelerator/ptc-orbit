//=========================================================================
//  TImpedanceWF.cc - source file of the Tr. Imp. Module (Wake Function)
//=========================================================================

#include "TImpedanceWF.h"

#include <fstream>
#include <cstdio>
#include <cmath>
#include <iostream>

#include <mpi.h>

using namespace std;

extern Array(ObjectPtr) syncP;

//=========================================================================
//=============== TImpedanceWF class           ============start===========
// Constructor TImpedanceWF

MB_TImpedanceWF::MB_TImpedanceWF(int nBins, int nImpElements)
  : coefficient_(0.0) , time_min_prev_(0.0)
{
  int i,j;

  nBins_   = nBins;

  maxBunchSize_ = 1;

  nElem_   = nImpElements;

  xyM_     = new double* [2];
  xyM_[0]  = new double[nBins_ + 1]; // x-coordinate
  xyM_[1]  = new double[nBins_ + 1]; // y-coordinate

  for(j = 0; j <= nBins_; j++) {
    xyM_[0][j] = 0.0;
    xyM_[1][j] = 0.0;
  }

  tSumKick_    = new double* [2];
  tSumKick_[0] = new double[nBins_ + 1];
  tSumKick_[1] = new double[nBins_ + 1];

    for(i = 0; i <= nBins_; i++){
     tSumKick_ [0][i] = 0.0;
     tSumKick_ [1][i] = 0.0;
    }

  xy_mask_ = new int [2];
  xy_mask_[0] = 0; // don't calulate x-axis kick
  xy_mask_[1] = 0; // don't calulate y-axis kick

  tKick_    = new MB_CmplxVector** [2];
  tKick_[0] = new MB_CmplxVector* [nBins_ + 1]; // x-axis kick array
  tKick_[1] = new MB_CmplxVector* [nBins_ + 1]; // y-axis kick array

    for(i = 0; i <= nBins_; i++){
     tKick_ [0][i]  = new MB_CmplxVector(nElem_);
     tKick_ [1][i]  = new MB_CmplxVector(nElem_);
    }

  //current part of kick from previous bunches and "memory" for kick

  tfKick_   =  new MB_CmplxVector* [2];
  tfKick_[0]=  new MB_CmplxVector(nElem_);
  tfKick_[1]=  new MB_CmplxVector(nElem_);

  tFFKick_   =  new MB_CmplxVector* [2];
  tFFKick_[0]=  new MB_CmplxVector(nElem_);
  tFFKick_[1]=  new MB_CmplxVector(nElem_);

  //additional memory for to store the tFFKick_ memory
  tFFmemory_   =  new MB_CmplxVector* [2];
  tFFmemory_[0]=  new MB_CmplxVector(nElem_);
  tFFmemory_[1]=  new MB_CmplxVector(nElem_);

  // Characteristics of Wake Fileds
  //    wake field at zero distance [Ohms/(m^2)] and
  //    eta-coff [1/sec] for shifting wake fields

  wake_zero_   =  new MB_CmplxVector* [2];
  wake_zero_[0]=  new MB_CmplxVector(nElem_);
  wake_zero_[1]=  new MB_CmplxVector(nElem_);

  eta_coff_   =  new MB_CmplxVector* [2];
  eta_coff_[0]=  new MB_CmplxVector(nElem_);
  eta_coff_[1]=  new MB_CmplxVector(nElem_);

  eta_fact_   =  new MB_CmplxVector* [2];
  eta_fact_[0]=  new MB_CmplxVector(nElem_);
  eta_fact_[1]=  new MB_CmplxVector(nElem_);

  // number of actual elements
  nElemExist_ = new int [2];
  nElemExist_[0] = 0;
  nElemExist_[1] = 0;

  // bins_[ip] is the index of the bin position as function
  //           of the  macro-particle index.

  bins_         = new int[maxBunchSize_];

  fractBins_    = new double[maxBunchSize_];

  for(j = 0; j < maxBunchSize_; j++) {
    bins_[j]      = 0;
    fractBins_[j] = 0.0;
  }

  // set some initial values for MPI
  iMPIini_=0;
  MPI_Initialized(&iMPIini_);
  nMPIsize_ = 1;
  nMPIrank_ = 0;
  if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
  }

  //buffer definition
  if( iMPIini_ > 0){
     buff     = (double *) malloc ( sizeof(double)*2*(nBins_+2));
     buff_MPI = (double *) malloc ( sizeof(double)*2*(nBins_+2));
  }

	//definition of the ring period of revolution
	//In the beginning it is negative. It means we are going to use the value from
	//the Ring class of ORBIT. If somebody defines it as positive than it will be used
	//as is.
	T = -1.;
}

// Destructor

MB_TImpedanceWF::~MB_TImpedanceWF()
{
  int i;

  delete [] xyM_[0];
  delete [] xyM_[1];
  delete [] xyM_;

  delete [] tSumKick_[0];
  delete [] tSumKick_[1];
  delete [] tSumKick_;

  delete [] xy_mask_;

    for(i = 0; i <= nBins_; i++){
     delete tKick_ [0][i];
     delete tKick_ [1][i];
    }
  delete [] tKick_[0];
  delete [] tKick_[1];


  delete tfKick_[0];
  delete tfKick_[1];
  delete [] tfKick_;

  delete tFFKick_[0];
  delete tFFKick_[1];
  delete [] tFFKick_;

  delete tFFmemory_[0];
  delete tFFmemory_[1];
  delete [] tFFmemory_;

  delete wake_zero_[0];
  delete wake_zero_[1];
  delete [] wake_zero_;

  delete eta_coff_[0];
  delete eta_coff_[1];
  delete [] eta_coff_;

  delete eta_fact_[0];
  delete eta_fact_[1];
  delete [] eta_fact_;


  delete [] nElemExist_;


  delete [] bins_;
  delete [] fractBins_;

  if( iMPIini_ > 0){
    free(buff);
    free(buff_MPI);
  }
  //std::cerr << "Destructor was done. TImpedanceWF  !!!! \n";
}

// Resize arrays for bins index and fraction coefficient for every particle
void MB_TImpedanceWF::_resizeFractCoeff( int bunchSizeLocal)
{
  if( bunchSizeLocal == 0 ) return;
  int nSens = 1000;
  if( bunchSizeLocal > maxBunchSize_ || bunchSizeLocal < (maxBunchSize_ - 2*nSens) ){
    maxBunchSize_ = bunchSizeLocal + nSens;
    delete [] bins_;
    delete [] fractBins_;
    bins_         = new int[maxBunchSize_];
    fractBins_    = new double[maxBunchSize_];
    for(int j = 0; j < maxBunchSize_; j++) {
      bins_[j]      = 0;
      fractBins_[j] = 0.0;
    }
  }
}

// Set the parameters of the impedance element's wake field
void MB_TImpedanceWF::addElement( int i_xy,
                                  double wake_zero_re,
                                  double wake_zero_im,
                                  double eta_coff_re,
                                  double eta_coff_im )
{
  int i;
  i = nElemExist_[i_xy];
  if(i != nElem_ ){
     wake_zero_[i_xy]->setRe(i, wake_zero_re);
     wake_zero_[i_xy]->setIm(i, wake_zero_im);
     eta_coff_[i_xy]->setRe(i, eta_coff_re);
     eta_coff_[i_xy]->setIm(i, eta_coff_im);
  }
  else
    {
      	std::cerr << "You try to add more Generic Transverse Impedance Elements \n";
        std::cerr << "    than the container MB_TImpedanceWF  has. Stop.  \n";
	_finalize_MPI();
	exit(1);
    }
  nElemExist_[i_xy]++;
  xy_mask_[i_xy]=1;
  setRange_();
}

// Print the parameters of the impedance element's wake fields
void MB_TImpedanceWF::printElementParameters()
{
  int i_xy, xy_mask, r_min, r_max;
  int i;
  char s[120];

  std::cerr << " ------Parameters of the impedance element's wake fields-----start-----\n";

  for( i_xy = 0 ; i_xy <= 1 ; i_xy++){
    xy_mask = xy_mask_[i_xy];
    if(xy_mask != 0 ) {
      r_min =  0;
      r_max =  nElemExist_[i_xy]-1;
      if(i_xy == 0 ) { std::cerr << " ----------x-dimension parameters----------\n";}
      if(i_xy == 1 ) { std::cerr << " ----------y-dimension parameters----------\n";}
      for( i=r_min; i<=r_max; i++){
       sprintf(s, " %-2d W(0)(wake field  ) Re Im : %-15.8e  %-15.8e", i, wake_zero_[i_xy]->getRe(i),
                                                            wake_zero_[i_xy]->getIm(i));
       std::cerr << s << "\n";
       sprintf(s, " %-2d Eta (shift vector) Re Im : %-15.8e  %-15.8e", i, eta_coff_[i_xy]->getRe(i),
                                                            eta_coff_[i_xy]->getIm(i));
       std::cerr << s << "\n";
      }
    }
  }
  std::cerr << " ------Parameters of the impedance element's wake fields-----stop------\n";
}

// Set the parameters of the resonant impedance element's wake field
//     r  - resistance parameter [ohms/m^2]
//     q  - quality factor
//     fr - resonant frequency [Hz]
void MB_TImpedanceWF::addResonantElement( int i_xy,
                                          double r,
                                          double q,
                                          double fr)
{
  int i;
  double w0_re,w0_im,eta_re,eta_im;
  double f,f_res,z;

  z = 1.0 - 1.0/(4*q*q);

  if(z == 0 ){
    std::cerr << "Class - MB_TImpedanceWF, method - addResonantElement:\n";
    std::cerr << "MB_TImpedanceWF::addResonantElement:\n";
    std::cerr << "Q=1/2 creates a wake function that does not\n";
    std::cerr << "satisfy phasor condition. Sorry.\n";
    std::cerr << "It is not good resonant element. \n";
    std::cerr << "Stop. \n";
    _finalize_MPI();
    exit(1);
  }


  f_res = 2*PI*fr;
  f = f_res*sqrt(fabs(z));


  if( z > 0 ) {
   // case Q>1/2 - that was considered in A. Chao book
   w0_re  = -r*f_res/(q*f);
   w0_im  = 0.0;
   eta_re = -f_res/(2.*q);
   eta_im = f;

   i = nElemExist_[i_xy];
   if(i != nElem_ ){
     wake_zero_[i_xy]->setRe(i, w0_re);
     wake_zero_[i_xy]->setIm(i, w0_im);
     eta_coff_[i_xy]->setRe(i, eta_re);
     eta_coff_[i_xy]->setIm(i, eta_im);
   }
   else
   {
     std::cerr << "You try to add more Transvese Resonant Impedance Elements \n";
     std::cerr << "than the container MB_TImpedanceWF  has. Stop.  \n";
     _finalize_MPI();
     exit(1);
   }
   nElemExist_[i_xy]++;
   xy_mask_[i_xy]=1;
   setRange_();
  }
  else{
    // case Q < 1/2 - expression for wake function has been modified
    // we need add two wake function elements.
    // A. Shishlo
    i = nElemExist_[i_xy];
     if( (i+2) > nElem_ ){
      std::cerr << "You try to add more Transvese Resonant Impedance Elements \n";
      std::cerr << "than the container MB_TImpedanceWF  has. \n";
      std::cerr << "Please increase maximal number of elements\n";
      std::cerr << "Stop.  \n";
      _finalize_MPI();
      exit(1);
     }

    w0_re  = 0.;
    w0_im  = -r*f_res/(q*f)*0.5;
    eta_re = -f_res/(2.*q)+f;
    eta_im = 0.;
    wake_zero_[i_xy]->setRe(i, w0_re);
    wake_zero_[i_xy]->setIm(i, w0_im);
    eta_coff_[i_xy]->setRe(i, eta_re);
    eta_coff_[i_xy]->setIm(i, eta_im);
    nElemExist_[i_xy]++;
    xy_mask_[i_xy]=1;
    setRange_();

    w0_re  = 0.;
    w0_im  = r*f_res/(q*f)*0.5;
    eta_re = -f_res/(2.*q)-f;
    eta_im = 0.;
    wake_zero_[i_xy]->setRe(i+1, w0_re);
    wake_zero_[i_xy]->setIm(i+1, w0_im);
    eta_coff_[i_xy]->setRe(i+1, eta_re);
    eta_coff_[i_xy]->setIm(i+1, eta_im);
    nElemExist_[i_xy]++;
    xy_mask_[i_xy]=1;
    setRange_();
  }

}


//Set the range of operation for CmplxVector's arrays.
//There should be placed all CmplxVector variables
void MB_TImpedanceWF::setRange_()
{
  int i, j, xy_mask, r_min, r_max;
  for( i = 0 ; i <= 1 ; i++){
    xy_mask = xy_mask_[i];
    if(xy_mask != 0 ) {
      r_min =  0;
      r_max =  nElemExist_[i]-1;
      tfKick_[i]->setRange(r_min, r_max);
      tFFKick_[i]->setRange(r_min, r_max);
      tFFmemory_[i]->setRange(r_min, r_max);
      wake_zero_[i]->setRange(r_min, r_max);
      eta_fact_[i]->setRange(r_min, r_max);
      eta_coff_[i]->setRange(r_min, r_max);
       for(j = 0; j <= nBins_; j++){
        tKick_[i][j]->setRange(r_min, r_max);
       }
    }
  }
}

//Get wake function
double MB_TImpedanceWF::getWF(int i_xy, int j, double t)
{

  if(i_xy < 0 || i_xy > 1) {
   std::cerr << "MB_TImpedanceWF::getWF : there are only x and y coordinates (0 or 1 indexes)  \n";
   _finalize_MPI();
   exit(1);
  }
  int nEl;
  nEl = nElemExist_[i_xy]-1;
  if(j < 0 || j > nEl) {
    std::cerr << "MB_TImpedanceWF::getWF : There is not WF-elements with index =" << j <<"\n";
    _finalize_MPI();
    exit(1);
  }

  tfKick_[i_xy]->copy( *wake_zero_[i_xy]);
  tfKick_[i_xy]->shift( *eta_coff_[i_xy], t);
  double w;
  w = tfKick_[i_xy]->getIm(j);
  return w;

}



// Bin the macro particles longitudinally
void MB_TImpedanceWF::_defineLongBin(const MacroPart &mp)
{
  // Define t_min_ and t_max_
  double ct, ct_min, ct_max;

  //ORBIT specific part =========start============

  SyncPart *sp = SyncPart::safeCast
                 (syncP((Particles::syncPart & Particles::All_Mask)-1));

  double beta, gamma;
  beta = sp->_betaSync;
  gamma = sp->_gammaSync;

  double coeff_orbit_ct2phi;
  //for ORBIT negative phi are for the head of herd
  //for (-ct) positive (-ct) for the same
  coeff_orbit_ct2phi = - Ring::lRing/(2.*PI*beta*Ring::harmonicNumber);

  bunchSize_       = mp._nMacros;
  bunchSizeGlobal_ = bunchSize_;

  double macroSize,charge;
  macroSize = Injection::nReals_Macro;
  charge = sp->_charge;

  //ORBIT specific part =========stop=============


  //parallel part   =====start======
  if( iMPIini_ > 0 && nMPIsize_ > 1) {
    MPI_Allreduce(&bunchSize_,&bunchSizeGlobal_,1,MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }
  //parallel part   =====stop=======

  ct_min =  1.0e+21;
  ct_max = -1.0e+21;
  int ip,ip1;
  for(ip = 0; ip < bunchSize_ ; ip++){
    ip1 = ip + 1;
    ct = coeff_orbit_ct2phi * mp._phi(ip1);
    if( ct > ct_max ) ct_max = ct;
    if( ct < ct_min ) ct_min = ct;
  }

  _resizeFractCoeff(bunchSize_);

  if(bunchSizeGlobal_ == 0 ) {return;}

   if( ct_min == ct_max ) {
    std::cerr << "MB_TImpedanceWF::_defineLongBin : There is zero longitudinal spread in the bunch. Stop.\n";
    _finalize_MPI();
    exit(1);
   }

   _grid_synchronize( ct_min, ct_max);

  double ct_step;
  ct_step = 1.00000001*(ct_max - ct_min)/nBins_;

  //coefficient has 1/1000 factor
  //dp was in [rad] , ORBIT has _xp and _yp in [mrad]

  coefficient_  = -4*PI*MB_PROTON_RADIUS/(MB_Z0*beta*beta*gamma);
  coefficient_ *=  macroSize*charge/1000.;

  t_min_ = ct_min/(MB_CLIGHT);
  t_max_ = ct_max/(MB_CLIGHT);

  t_step_ = 1.00000001*(t_max_ - t_min_)/nBins_;

  for(int i = 0 ; i <= nBins_ ; i++){
   xyM_[0][i] = 0.0;
   xyM_[1][i] = 0.0;
  }

  // binning on the base of variable ct_step
  int iT;
  double  x , y;
  double tt, fract, fract1;
  for(ip = 0; ip < bunchSize_; ip++){
      ip1 = ip + 1;
      ct = coeff_orbit_ct2phi * mp._phi(ip1);
      tt = (ct - ct_min)/ct_step;
      iT = int (tt);
      bins_[ip] = iT;
      fract = tt - iT;
      fract1 = 1.0 - fract;
      fractBins_[ip] = fract;
      //x coordinate in ORBIT - [mm]
      x  = mp._x(ip1)/1000.;
      xyM_[0][iT]   += fract1*x;
      xyM_[0][iT+1] += fract*x;
      //y coordinate in ORBIT - [mm]
      y  = mp._y(ip1)/1000.;
      xyM_[1][iT]   += fract1*y;
      xyM_[1][iT+1] += fract*y;
  }

  //for parallel version
  _sum_distribution();

}

// Print out the <x> and <y> momenta of bunch
void MB_TImpedanceWF::showXY(char* f)
{
  ofstream file;
  file.open(f);
  if(!file) {
    std::cerr << "MB_TImpedanceWF::showXY: Cannot open " << f << " for output \n";
    _finalize_MPI();
    exit(1);
  }
 char s[120];
  int i;
  file << "nBins t_min t_max t_step [sec]:" << nBins_ << "  " << t_min_
                                                 << "  " << t_max_
                                                 << "  " << t_step_ <<  "\n";
  file << "i            <x> [m]           <y> [m]  \n";
  for (i = 0; i <= nBins_; i++){
   sprintf(s, "%-4d %-20.13e %-20.13e", i, xyM_[0][i], xyM_[1][i]);
   file << s << "\n";
  }
  file.close();
}

// Print out the <x> and <y> momenta of bunch
void MB_TImpedanceWF::showTKick(char* f)
{
  ofstream file;
  file.open(f);
  if(!file) {
    std::cerr << "MB_TImpedanceWF::showTKick: Cannot open " << f << " for output \n";
    _finalize_MPI();
    exit(1);
  }
 char s[120];
  int i;
  file << "nBins t_min t_max t_step [sec]:" << nBins_ << "  " << t_min_
                                                 << "  " << t_max_
                                                 << "  " << t_step_ <<  "\n";
  file << "i        xKick [rad]        yKick [rad]  \n";
  for (i = 0; i <= nBins_; i++){
   sprintf(s, "%-4d %-20.13e %-20.13e", i, tSumKick_ [0][i],
                                           tSumKick_ [1][i]);
   file << s << "\n";
  }
  file.close();
}

//Restore initial element's state
void MB_TImpedanceWF::restore()
{
  tFFKick_[0]->zero();
  tFFKick_[1]->zero();
}



// Transverse Kick calculation
void MB_TImpedanceWF::_tKickCalc( double t)
{

 // parameters needed : t_min_ ; t_max_ ; t_step_ ; z = xyM_[1..2][0..nBins_]
 // wake_zero_[1..2] ; eta_fact_[1..2] ; eta_coff_[1..2]

 int i_xy, i;
 double z;
 for ( i_xy = 0; i_xy <= 1 ; i_xy++){
   if( xy_mask_[i_xy] == 1){

     eta_fact_[i_xy]->defShift(  *eta_coff_[i_xy], t_step_);

     tfKick_[i_xy]->copy(*tFFKick_[i_xy]);
     tfKick_[i_xy]->shift(  *eta_coff_[i_xy], (t - t_max_));

     tKick_[i_xy][nBins_]->zero();

      for( i = (nBins_-1) ; i >= 0 ; i--){
       tKick_[i_xy][i]->copy(*wake_zero_[i_xy]);
       z = xyM_[i_xy][i+1];
       tKick_[i_xy][i]->multR(z);
       tKick_[i_xy][i]->sum(*tKick_[i_xy][i+1]);
       tKick_[i_xy][i]->mult(*eta_fact_[i_xy]);
       tKick_[i_xy][i+1]->sum(*tfKick_[i_xy]);
       tfKick_[i_xy]->mult(*eta_fact_[i_xy]);
      }

     tKick_[i_xy][0]->sum(*tfKick_[i_xy]);

     tfKick_[i_xy]->copy(*wake_zero_[i_xy]);
     z = xyM_[i_xy][0];
     tfKick_[i_xy]->multR(z);
     tfKick_[i_xy]->sum(*tKick_[i_xy][0]);
     tfKick_[i_xy]->shift(*eta_coff_[i_xy], t_min_);
     tFFKick_[i_xy]->copy(*tfKick_[i_xy]);

     for( i = 0 ; i <= nBins_ ; i++){
       tSumKick_ [i_xy][i] = coefficient_*tKick_[i_xy][i]->sumIm();
     }

   }
 }
}

//Propagate the bunch through the transvers impedance element
void MB_TImpedanceWF::propagate( MacroPart &mp, double t)
{
  // t - time after the previous bunch had passed through the element [sec]
  _defineLongBin(mp);

  if(bunchSizeGlobal_ == 0 ) {return;}

  //if time is equal to zero we suppose that there is only one bunch
   if( t == 0.0) {
    tFFKick_[0]->zero();
    tFFKick_[1]->zero();
   }

  _tKickCalc(t);

  if( ((t+time_min_prev_ - t_max_ ) < 0.0) & (t != 0.0) ) {
    std::cerr << "MB_TImpedanceWF::propagate : This and previous bunches are overlaped. Error. Stop.\n";
    _finalize_MPI();
    exit(1);
  }

  int iT,ip,ip1;
  double  dp;
  double fract, fract1;
  for(ip = 0; ip < bunchSize_; ip++){
    ip1 = ip+1;
    iT = bins_[ip];
    fract = fractBins_[ip];
    fract1 = 1.0 - fract;

    // x-axis kick
    if( xy_mask_[0] == 1){
      dp = fract1*tSumKick_[0][iT]+fract*tSumKick_[0][iT+1];
      mp._xp(ip1) += dp;
    }

    // y-axis kick
    if( xy_mask_[1] == 1){
      dp = fract1*tSumKick_[1][iT]+fract*tSumKick_[1][iT+1];
      mp._yp(ip1) += dp;
    }
  }
  time_min_prev_ = t_min_;
}


//memorize the state of the transverse impedance element
void MB_TImpedanceWF::memorizeState()
{
 int i_xy;
 for ( i_xy = 0; i_xy <= 1 ; i_xy++){
   if( xy_mask_[i_xy] == 1){
      tFFmemory_[i_xy]->copy(*tFFKick_[i_xy]);
   }
 }
}

//restore the memorized state of the transverse impedance element
void MB_TImpedanceWF::restoreState()
{
 int i_xy;
 for ( i_xy = 0; i_xy <= 1 ; i_xy++){
   if( xy_mask_[i_xy] == 1){
      tFFKick_[i_xy]->copy(*tFFmemory_[i_xy]);
   }
 }
}

// Synchronize longitudinal grid size for parallel calculation
void MB_TImpedanceWF::_grid_synchronize(double& ct_min, double& ct_max)
{
 // if MPI has been initialized ===start====
  if( iMPIini_ > 0 && nMPIsize_ > 1) {
   double ct_min_max [2], ct_min_max_MPI [2];
   ct_min_max [0] = - ct_min;
   ct_min_max [1] =   ct_max;
   MPI_Allreduce(&ct_min_max, &ct_min_max_MPI, 2 , MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
   ct_min_max_MPI[0] = - ct_min_max_MPI[0];
   ct_min =  ct_min_max_MPI[0];
   ct_max =  ct_min_max_MPI[1];
  }
 // if MPI has been initialized  ===stop=====
}

// Sum longitudinal distribution for parallel calculations
void MB_TImpedanceWF::_sum_distribution()
{

 int i,i_xy;

 // if MPI has been initialized ===start====
 if( iMPIini_ > 0 && nMPIsize_ > 1) {

  int counter = 0;
  for ( i_xy = 0 ; i_xy <= 1; i_xy++){
    if(xy_mask_[i_xy] != 0){
      for ( i = 0 ; i <= nBins_; i++){
	buff[counter] = xyM_[i_xy][i];
        counter++;
      }
    }
  }

   if(counter > 0 ) {
    MPI_Allreduce( buff, buff_MPI , counter, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
   }

  counter = 0;
  for ( i_xy = 0 ; i_xy <= 1; i_xy++){
    if(xy_mask_[i_xy] != 0){
      for ( i = 0 ; i <= nBins_; i++){
	xyM_[i_xy][i] = buff_MPI[counter];
        counter++;
      }
    }
  }
 }
 // if MPI has been initialized ===stop=====
}

// Finalize MPI
void MB_TImpedanceWF::_finalize_MPI()
{
  if(iMPIini_ > 0){
    MPI_Finalize();
  }
}


// Set period for ring calculation. This value will not be used inside this class.
void MB_TImpedanceWF::setPeriod(double T_in){
		T = T_in;
}

// Get period for ring calculation. [sec]
double MB_TImpedanceWF::getPeriod(){
		return T;
}

//=============== TImpedanceWF class           ============stop============
//=========================================================================
//=============== MB_CmplxVector class         ============start===========

// Constructor MB_CmplxVector class

MB_CmplxVector::MB_CmplxVector(int size)
{
  size_ = size;

  re_ = new double[size_];
  im_ = new double[size_];

  int i;
  for ( i = 0 ; i < size_ ; i++){
    re_[i]= 0.0;
    im_[i]= 0.0;
  }

  //Set range for operation
  i_min_ = 0;
  i_max_ = size_ -1;

}


// Destructor

MB_CmplxVector::~MB_CmplxVector()
{
  delete [] re_;
  delete [] im_;
  //std::cerr << "Destructor was done. CmplxVector  !!!! \n";
}


//Set the range of operation
void MB_CmplxVector::setRange(int i_min, int i_max)
{
  i_min_ = i_min;
  i_max_ = i_max;
  if(i_min < 0 ) i_min_ = 0;
  if(i_max > (size_ - 1)) i_max_ = size_ - 1;
   if( i_min_ > i_max_ ) {
     std::cerr << "CmplxVector::setRange : i_min > i_max - error. Stop.\n";
     exit(1);
   }
}

//Returns the min range of operation
int MB_CmplxVector::getMinRange()
{
  return i_min_;
}

//Returns the max range of operation
int MB_CmplxVector::getMaxRange()
{
  return i_max_;
}

//Set the real and imaginary parts equal to zero
void MB_CmplxVector::zero()
{
  int i;
  for ( i = i_min_ ; i <= i_max_ ; i++){
    re_[i]= 0.0;
    im_[i]= 0.0;
  }
}

//Set the real part of one element
void MB_CmplxVector::setRe(int j, double value)
{
  re_[j] = value;
}

//Set the imaginary part of one element
void MB_CmplxVector::setIm(int j, double value)
{
  im_[j] = value;
}

//Get the real part of one element
double MB_CmplxVector::getRe(int j)
{
  return re_[j];
}

//Get the imaginary part of one element
double MB_CmplxVector::getIm(int j)
{
  return im_[j];
}

//Retirns the real part of the sum all components of the complex vector
double MB_CmplxVector::sumRe()
{
  int i;
  double d = 0.0;
  for( i = i_min_ ; i <= i_max_ ; i++){
    d +=re_[i];
  }
  return d;
}

//Retirns the Im part of the sum all components of the complex vector
double MB_CmplxVector::sumIm()
{
  int i;
  double d = 0.0;
  for( i = i_min_ ; i <= i_max_ ; i++){
    d +=im_[i];
  }
  return d;
}

//Sum two complex vectors
void MB_CmplxVector::sum( MB_CmplxVector& cv )
{
  int i;
  for( i = i_min_ ; i <= i_max_ ; i++){
    re_[i] = re_[i] + cv.getRe(i);
    im_[i] = im_[i] + cv.getIm(i);
  }
}

//Multiply two complex vectors
void MB_CmplxVector::mult( MB_CmplxVector& cv )
{
  int i;
  double x;
  for( i = i_min_ ; i <= i_max_ ; i++){

    // debugging
    // std::cerr << "i = " << i << "\n";
    // std::cerr << "Var 1  re and im ===== " << re_[i] << "  " << im_[i] << "\n";
    // std::cerr << "Var 2  re and im ===== " << cv.getRe(i) << "  " << cv.getIm(i) << "\n";

    x = re_[i];
    re_[i] = re_[i]*cv.getRe(i) - im_[i]*cv.getIm(i);
    im_[i] = x*cv.getIm(i) + im_[i]*cv.getRe(i);

  }
}

//Multiply by real value
void MB_CmplxVector::multR( double x )
{
  int i;
  for( i = i_min_ ; i <= i_max_ ; i++){
    re_[i] = re_[i]*x;
    im_[i] = im_[i]*x;
  }
}

//Copy operator
void MB_CmplxVector::copy( MB_CmplxVector& cv )
{
  int i;
  for( i = i_min_ ; i <= i_max_ ; i++){
    re_[i] = cv.getRe(i);
    im_[i] = cv.getIm(i);
  }
}

//Defines this complex vector as shift one (exp(eta*time))
void MB_CmplxVector::defShift( MB_CmplxVector& eta , double time_shift )
{
  int i;
  double x1,x2;
  for( i = i_min_ ; i <= i_max_ ; i++){
    x1 = exp(eta.getRe(i)*time_shift);
    x2 = eta.getIm(i)*time_shift;
    re_[i] = x1*cos(x2);
    im_[i] = x1*sin(x2);
  }
}

//Shifts this complex vector by Multiplying by (exp(eta*time))
void MB_CmplxVector::shift(MB_CmplxVector& eta , double time_shift )
{
  int i;
  double x,x1,x2, shift_re,shift_im;
  for( i = i_min_ ; i <= i_max_ ; i++){
    x1 = exp(eta.getRe(i)*time_shift);
    x2 = eta.getIm(i)*time_shift;
    shift_re = x1*cos(x2);
    shift_im = x1*sin(x2);
    x = re_[i];
    re_[i] = re_[i]*shift_re - im_[i]*shift_im;
    im_[i] = x*shift_im + im_[i]*shift_re;
  }
}

//Print vector
void MB_CmplxVector::print_()
{
  int i;
  std::cerr << "==CmplxVector:print Re and Im parts. \n";
  std::cerr << "  range i_min = " << i_min_ <<"  i_max =" << i_max_ <<" \n";

  for( i = i_min_ ; i <= i_max_ ; i++){
    std::cerr << "  index = " << i << "  Re = " << re_[i] << "  Im = " << im_[i] << " \n";
  }
  std::cerr << "==CmplxVector:print ====stop=====. \n";
}

//=============== MB_CmplxVector class         ============stop============
//=========================================================================
//=============== MB_TI_manager class             ============start===========
MB_TI_manager* MB_TI_manager::m_Instance=0;

MB_TI_manager::MB_TI_manager()
{
  nNodes_ = 0;
}

MB_TI_manager::~MB_TI_manager()
{
  if(nNodes_ > 0) {
    int i;
    for( i = 0; i < nNodes_ ; i++){
      delete refArr[i];
    }
    delete [] refArr;
    delete [] tmp_refArr;
  }
}

MB_TI_manager* MB_TI_manager::GetMB_TI_manager()
{
  if(!m_Instance){
    m_Instance = new  MB_TI_manager();
  }
  return  m_Instance;
}

int MB_TI_manager::getNumbNodes()
{
  return nNodes_;
}

MB_TImpedanceWF* MB_TI_manager::getNode(int index)
{
  if( index < 0 || index >= nNodes_ ){
    std::cerr << "Class MB_TI_manager: method getNode:\n";
    std::cerr << "wrong index (should be < nNodes )="<< index <<"\n";
    std::cerr << "nNodes ="<<nNodes_<<"\n";
    std::cerr << "Stop.\n";
    exit(1);
  }
  return refArr[index];
}

int MB_TI_manager::addNode(int nBins, int nImpElements)
{
  if( nNodes_ == 0){
    refArr     = new MB_TImpedanceWF* [1];
    tmp_refArr = new MB_TImpedanceWF* [1];
    refArr[0]  = new MB_TImpedanceWF(nBins,nImpElements);
    tmp_refArr[0] = refArr[0];
    nNodes_ = 1;
  }
  else{
    delete [] refArr;
    refArr     = new MB_TImpedanceWF* [nNodes_+1];
    for( int i=0; i < nNodes_; i++){
      refArr[i]=tmp_refArr[i];
    }
    refArr[nNodes_] = new MB_TImpedanceWF(nBins,nImpElements);
    delete []  tmp_refArr;
    tmp_refArr = new MB_TImpedanceWF* [nNodes_+1];
    for( int i=0; i < (nNodes_+1); i++){
      tmp_refArr[i]= refArr[i];
    }
    nNodes_++;
  }
  return (nNodes_-1);
}

//=============== MB_TI_manager class          ============stop============
//=========================================================================
