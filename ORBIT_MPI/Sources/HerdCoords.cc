#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include "HerdCoords.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    HerdCoords
//
// INHERITANCE RELATIONSHIPS
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for containing Herd particle coordinates as doubles.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

#define nPartDim 200000

using namespace std;

HerdCoords::HerdCoords()
{
  _nMacros = 0;
}

void HerdCoords::setCoord(double &x_map, double &xp_map,
                          double &y_map, double &yp_map,
                          double &phi_map, double &deltaE_map)
{
  _x_map[_nMacros] = x_map;
  _xp_map[_nMacros] = xp_map;
  _y_map[_nMacros] = y_map;
  _yp_map[_nMacros] = yp_map;
  _phi_map[_nMacros] = phi_map;
  _deltaE_map[_nMacros] = deltaE_map;
  _nMacros++;
}
