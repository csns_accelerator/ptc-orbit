#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include "NodeDat.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    NodeDat
//
// INHERITANCE RELATIONSHIPS
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for containing node information.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

NodeDat::NodeDat(){}
