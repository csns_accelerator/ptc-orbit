/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Diagnostic.mod
//
// AUTHOR
//    Jeff Holmes,
//    John Galambos,  ORNL, jdg@ornl.gov
//
// CREATED
//    12/21/98
//
//  DESCRIPTION
//    Module descriptor file for the Diagnostic module. 
//    This module contains diagnostic features
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Diagnostic - "Diagnostic Module."
                 - "Written by John Galambos, Jeff Holmes, ORNL"
{

  Inherits Consts, MathLib, Particles, Ring, TransMap;

Errors

  badHerdNo    - "You asked to diagnose a nonexistent herd",
  badPMTMoment  - "I can't do a PMT with moment order > 10",
  tuneNotCalcd - "The tune appears to be in calculation now"
               - "Please finish it first",
  tuneNotCalcd2 - "The tune appears to have not been calculated yet";  


public:

  Void ctor() 
                  - "Constructor for the Diagnostic  module."
                  - "Initializes build values.";

// Canonical coordinates:

  Void dumpCanonicalCoords(const Integer &herd, Ostream &os)
                  - "Routine to dump the canonical coordinates"
                  - "of a herd to a stream";

// Sigma Coordinate Averages and Matrices:

  Void dumpSigMat(const Integer &herd, Ostream &os)
                  - "Routine to dump the coordinate averages and"
                  - "sigma matrix of a herd to a stream";

// Emittance:

  Real
    xEmitRMS      - "The horizontal RMS emittance of a Herd [pi-mm-mrad]",
    yEmitRMS      - "The vertical RMS emittance of a Herd [pi-mm-mrad]",
    xEmitSliceRMS - "The horizontal RMS emittance of a longitudinal slice"
                  - " of a Herd [pi-mm-mrad]",
    yEmitSliceRMS - "The vertical RMS emittance of a longitudinal slice of"
	          - "a Herd [pi-mm-mrad]",
    xEmitMax      - "The maximum horizontal emittance of a Herd [pi-mm-mrad]",
    yEmitMax      - "The maximum vertical emittance of a Herd [pi-mm-mrad]",
    xEmitSliceMax - "The maximum horizontal emittance of a longitudinal slice"
		  - "of a Herd [pi-mm-mrad]",
    yEmitSliceMax - "The maximum vertical emittance of longitudinal slice of"
		  -"a Herd [pi-mm-mrad]",

    xEmitMaxco    - "The maximum horizontal emittance of a Herd [pi-mm-mrad]"
		  - "relative to closed orbit",
    yEmitMaxco    - "The maximum vertical emittance of a Herd [pi-mm-mrad]"
	          - "relative to closed orbit",
    xEmitAveco    - "The average horizontal emittance of a Herd [pi-mm-mrad]"
		  - "relative to closed orbit",
    yEmitAveco    - "The average vertical emittance of a Herd [pi-mm-mrad]"
	          - "relative to closed orbit";


  RealVector
    xEmit       - "Hor. emittance of each particle of a herd [pi-mm-mrad]",
    yEmit       - "Vertical emittance of each particle of a herd [pi-mm-mrad]",
    xEmitSlice  - "Hor. emittance of each particle of a herd inside a"
		- "a particular longitudinal slice [pi-mm-mrad]",
    yEmitSlice  - "Vertical emittance of each particle of a herd inside a"
		- "particular longitudinal slice [pi-mm-mrad]",
    xEmitFrac   - "Binned X emittance distribution [%]",
    yEmitFrac   - "Binned Y emittance distribution [%]",
    xoyEmitFrac  - "Binned X or Y emittance distribution [%]",
    xpyEmitFrac  - "Binned X plus Y emittance distribution [%]",

    xEmitco	- "Hor. emittance of each particle of a herd [pi-mm-mrad]"
		- "relative to the closed orbit.",
    yEmitco	- "Vert. emittance of each particle of a herd [pi-mm-mrad]"
		- "relative to the closed orbit.",
    xEmitFracco - "Binned X emittance distribution [%], rel. to closed orbit",
    yEmitFracco - "Binned Y emittance distribution [%], rel. to closed orbit",
    xoyEmitFracco - "Binned X or Y emittance distribution [%], rel. to"
	          - "closed orbit",
    xpyEmitFracco - "Binned X plus Y emittance distribution [%], rel. to"
		  - "closed orbit",
    XPhase_old    - "previous X phases for fractional tune node",
    YPhase_old    - "previous Y phases for fractional tune node",
    XPhase        - "present X phases for fractional tune node",
    YPhase        - "present Y phases for fractional tune node",
    XFrac_Tune    - "X fractional tune",
    YFrac_Tune    - "Y fractional tune";

// Longitudinal Emittance:

  Real
    longEmitRMS      - "The horizontal RMS emittance of a Herd [pi-mm-mrad]",
    longEmitMax      - "The maximum horizontal emittance of a" 
		     - "Herd [pi-mm-mrad]";

  RealVector
    longEmit       - "Hor. emittance of each particle of a herd [pi-mm-mrad]",
    longEmitFrac   - "Binned X emittance distribution [%]";

  Integer
    nEmitBins         - "Number of emittance bins to use for binned "
		      - "distributions";
  Real 
   deltaEmitBin       - "Emittance bin length [pi-mm-mrad]";

  Void showEmitBase(const Integer &herd, Ostream &os)
                  - "Routine to dump the emittances"
                  - "of a herd to a stream";

  Void showEmitPercent(const Integer &herdNo, Ostream &sout,
                       const Integer &Turn, Real &percent)
                  - "Routine to dump a turn number and"
                  - "percent emittance of a herd to a stream";

  Void showEmitBaseco(const Integer &herd, Ostream &os)
                  - "Routine to dump the emittance coordinates"
                  - "of a herd to a stream.  Emittance relative to"
  		  - "closed orbit";

  Void showEmit(Ostream &os)
                  - "Routine to dump the emittances"
                  - "of the main herd to a stream";
  Void showEmitSlice(const Integer &herd, Ostream &os, const Real &phiMin, 
		    const Real &phiMax)
                  - "Routine to dump the RMS and maximum emittances"
                  - "of a longitudinal slice of the main herd to a stream";
  Void calcEmitBase(const Integer &herd)
                  - "Routine that calls the emittance calculators for a herd";
  Void binEmit()  - "Calculate the emittance distribution of a herd";
  Void binEmitco() - "Calculate the emittance distribution of a herd" 
		   - "relative to closed orbit";
	
  Void dumpEmitco(const Integer &herd, Ostream &os)
		  - "Routine to dump individual particle emittances (relative"
		  - "to closed orbit for a prescribed herd to a stream";
  Void dumpEmitAveco(const Integer &herd, Ostream &os)
		  - "Routine to dump average and Maximum individual particle" 
		  - "emittances (relative to closed orbit) for a prescribed" 
		  - "herd to a stream";
  


// Actions

  Void dumpActions(const Integer &herd, Ostream &os)
                  - "Routine to calculate and dump the action variables"
                  - "of a herd to a stream";
  Void calcActions(const Integer &herd)
                  - "Routine to calculate the action variables"
                  - "of a herd to a stream";
  RealVector
       yAction    - "The vertical actions of a herd [] ",
       xAction    - "The horizontal actions of a herd []";

//  Fractional Tunes:

  Integer
    oldSize           - "Number of macroparticles at previous " 
                      - "FracTune evaluation";  
  Void addFracTuneNode(const String &name, const Integer &order,
                       const String &fn)
                  - "Adds a fractional tune diagnostic node to the Ring";
  Void dumpFracTunes(Ostream &sout)
                  - "dumps fractional tunes to a stream";

//  Moments:

  Void showMoments(const Integer &herd, const Integer &ord, Ostream &os)
                  - "Routine to calculate and dump the moments of herd up"
                  - " to order (ord) to a stream (os)";
  Void addLongDistNode(const String &name, const Integer &order,
                       const  Integer &nBins, const String &fn)
                  - "Adds a beam longitudinal distribution node";
  Void addMomentNode(const String &name,
     const Integer &order, const  Integer &mo, const String &fn)
                  - "Adds a beam moment diagnostic node to the Ring";
  Void addMomentNodeSet(const  Integer &mo, const String &fn,
                        const String &choice)
                  - "Adds a beam moment diagnostic node set to the Ring";

  Void addMomentSliceNode(const String &name, const Integer &order, 
                          const  Integer &mo, const String &fn,
                          const Real &phiMin, const Real &phiMax)
                  - "Adds a beam moment diagnostic node for a particular"
		  - "longitudinal slice of beam to the Ring";
  Void addMomentSliceNodeSet(const Integer &mo, const String &fn,
                             const String &choice,
                             const Real &phiMin, const Real &phiMax)
                  - "Adds a beam moment diagnostic node set for a particular"
		  - "longitudinal slice of beam to the Ring";
  Integer addBPMNode(const String &name, const Integer &order,
                     const  Integer &mo, const String &fn,
                     const Real &phiMin, const Real &phiMax)
                  - "Adds a BPMNode for a particular"
                  - "longitudinal slice of beam to the Ring";
  Real BPMSignal(const Integer &nodeIndex, const String &XorY)
                  - "Gets Horizontal or Vertical BPM Signal";
  Integer addBPMFreqNode(const String &n, const Integer &order,
                         const Real &phiMin, const Real &phiMax,
                         const Integer &nBins,
                         const Integer &freq0, const Integer &band)
                  - "Adds a BPMFreqNode";
  Void BPMFreqSignal(const Integer &nodeIndex, Ostream &sout,
                     const String &dist, RealVector &param)
                  - "Dumps BPM Signal";
  Integer addQuadBPMNode(const String &name, const Integer &order,
                         const  Integer &mo, const String &fn,
                         const Real &phiMin, const Real &phiMax)
                  - "Adds a QuadBPMNode for a particular"
                  - "longitudinal slice of beam to the Ring";
  Real QuadBPMSignal(const Integer &nodeIndex, const String &XorY)
                  - "Gets Horizontal or Vertical Quad BPM Signal";
// Statistical Lattice:

  Void addStatLatNode(const String &name, const Integer &order, 
                     const String &fn)
                  - "Adds a statistical lattice diagnostic node to the Ring";
  Void addStatLatNodeSet(const String &fn, const String &choice)
               - "Adds a statistical lattice diagnostic node set to the Ring";


// Poincare-Moment-Tracking:

  Void addPMTNode(const String &name, const Integer &order, 
                     const String &fn, const Integer &hi, const Integer &vi)
                  - "Adds a PMT diagnostic node to the Ring";
  Void addPMTNodeSet(const String &fn, const Integer &hi, const Integer &vi)
               - "Adds a PMT diagnostic node set to the Ring";

//  Tunes / Actions:
  Void dumpTAndA(const Integer &herdno, Ostream &sout)
               - "Dump T&A, i.e. tunes and actions of a herd to a stream";

  Void dumpTAndAi(const Integer &herdno, Ostream &sout, const Integer &index)
               - "Dump T&A, i.e. tunes and actions of a particle to a stream";

  Void showTunes( Ostream &sout)
               - "Show some statistical tune spread info to a stream";
// Lonitudinal Mountain Range node:
 Void addLongMountainNode(const String &name, 
     const Integer &order, const  Integer &nBins, const String &fn)
                  - "Adds a longitudinal mountain rangediagnostic node to the Ring";
  

//  Emittance / Canonical Coordinates:
  Void dumpEAndCC(const Integer &herdno, Ostream &sout)
               - "Dump emittances and canonical coordinates of a herd"
               - "to a stream";

// General diagnostic node information

  Integer
   nDiagnosticNodes - "Number of diagnostic nodes in the Ring";

 Void showDiagnostics(Ostream &os) 
                  - "Routine to print Diagnostic node setup to a stream";
 Void activateLongDistNodes()    - "Activates all longdist nodes";
 Void activateMomentNodes() - "Activates all moment nodes";
 Void activateMomentSliceNodes() - "Activates all moment slice nodes";
 Void activateBPMNodes() - "Activates all BPMNodes";
 Void activateBPMFreqNodes() - "Activates all BPMFreqNodes";
 Void activateQuadBPMNodes() - "Activates all QuadBPMNodes";
 Void activateFracTuneNodes() - "Activates fractional tune nodes";
 Void activateMomentNode(const Integer &i)
                           - "Activates a moment node i";
 Void deactivateFracTuneNodes() - "Deactivates fractional tune nodes";
 Void deactivateLongDistNodes()  - "De-activates all longdist nodes";
 Void deactivateMomentNodes() - "De-activates all moment nodes";
 Void deactivateMomentSliceNodes() - "De-activates all moment slice nodes";
 Void deactivateBPMNodes() - "De-activates all BPMNodes";
 Void deactivateBPMFreqNodes() - "De-activates all BPMFreqNodes";
 Void deactivateQuadBPMNodes() - "De-activates all QuadBPMNodes";
 Void activateStatLatNodes() - "Activates all StatLat nodes";
 Void activateStatLatNode(const Integer &i)  
                              - "Activates a StatLat node i"; 
 Void deactivateStatLatNodes() - "De-activates all StatLat nodes";
 Void activatePMTNodes() - "Activates all PMT nodes";
 Void deactivatePMTNodes() - "De-activates all PMT nodes";

  RealMatrix
     momOld, mom0, momNew;
  IntegerMatrix
     dumpTime;
}
