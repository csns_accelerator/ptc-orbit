/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Parallel_MPI.mod
//
//  DESCRIPTION
//    Provides interface for the MPI functions
//    
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Parallel_MPI - "Parallel_MPI Module."
                 - "ORNL"
{

  Inherits Consts;

public:

  Void ctor() 
                  - "Constructor for the Parallel_MPI  module.";

  Void dtor() 
                  - "Destructor for the Parallel_MPI  module.";

  Integer MPI_size() 
                  - "Return number of CPUs";

  Integer MPI_rank() 
                  - "Return a rank of CPU";

  Void MPI_Get_CPU_name(String& CPU_name) 
                  - "Return a name of CPU";

  Integer MPI_Allreduce_MAX_Integer(Integer& i_in0)
                  - "Finding MAX Integer among CPUs";

  Integer MPI_Allreduce_SUM_Integer(Integer& i_in0)
                  - "Finding SUM Integer among CPUs";

  Real MPI_Allreduce_MAX_Real(Real& d_in0)
                  - "Finding MAX Real among CPUs";

  Real MPI_Allreduce_SUM_Real(Real& d_in0)
                  - "Finding SUM Real among CPUs";

  Void MPI_Barrier0()
                  - "The MPI_Barrier function";

  Real MPI_Wtime0()
                  - "The MPI_Wtime0 function";

  Integer MPI_Initialized0() 
                  - "Return 1 if MPI_Init was called 0-otherwise";

  Void MPI_String_Add_Integer(String& S, Integer& iRank)
                  - "Adds Integer to the String variable";

  Void MPI_test_1_my()
                  - "My test some MPI routines (send and recive Vector in this case)";


}
