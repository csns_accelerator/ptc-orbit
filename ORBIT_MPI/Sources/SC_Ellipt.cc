/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   SC_Ellipt.cc
//
// AUTHOR
//   Jeff Holmes
//   ORNL, jzh@ornl.gov
//
// CREATED
//   10/2013
//
// DESCRIPTION
//   File for Fast Multipole Methods
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "SC_Ellipt.h"


///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS ElliptCalculator
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(ElliptCalculator, Object);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   ElliptCalculator
//
// INHERITANCE RELATIONSHIPS
//   ElliptCalculator -> CalculatorBase -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   Class for calculating space charge kicks
//   from a uniform elliptical charge distribution including
//   a conducting wall boundary
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------
void ElliptCalculator::_SetDistribution(MacroPart& mp)
{
  int i, n, zIndex, zIm, zIp;

  double dz = _ZLength / double(_nZBins);
  double zmax = mp._phi(1);
  double zmin = mp._phi(1);
  double zi, zfrac;

  _doBoundary = 0;
  _xc     = 0.0;
  _xsig   = 0.0;
  _pxc    = 0.0;
  _pxsig  = 0.0;
  _xpxcor = 0.0;
  _yc     = 0.0;
  _ysig   = 0.0;
  _pyc    = 0.0;
  _pysig  = 0.0;
  _ypycor = 0.0;

  for(n = 0; n < _nZBins; n++)
  {
    _ZDist[n] = 0;
  }

  for(i = 1; i <= mp._nMacros; i++)
  {
    _xc  += mp._x(i);
    _pxc += mp._xp(i);
    _yc  += mp._y(i);
    _pyc += mp._yp(i);
    if(mp._phi(i) > zmax) zmax = mp._phi(i);
    if(mp._phi(i) < zmin) zmin = mp._phi(i);
  }

  _xc  /= double(mp._nMacros);
  _pxc /= double(mp._nMacros);
  _yc  /= double(mp._nMacros);
  _pyc /= double(mp._nMacros);

  _ZCenter = (zmax + zmin) / 2.0;

  for(i = 1; i <= mp._nMacros; i++)
  {
    _xsig   += (mp._x(i)  - _xc)  * (mp._x(i)  - _xc);
    _pxsig  += (mp._xp(i) - _pxc) * (mp._xp(i) - _pxc);
    _xpxcor += (mp._x(i)  - _xc)  * (mp._xp(i) - _pxc);
    _ysig   += (mp._y(i)  - _yc)  * (mp._y(i)  - _yc);
    _pysig  += (mp._yp(i) - _pyc) * (mp._yp(i) - _pyc);
    _ypycor += (mp._y(i)  - _yc)  * (mp._yp(i) - _pyc);

    zi = mp._phi(i) - _ZCenter;
    if(zi >  _ZLength / 2.0) zi -= _ZLength;
    if(zi < -_ZLength / 2.0) zi += _ZLength;
    zi += _ZLength / 2.0;
    zIndex = int(zi / dz + 0.5);
    zfrac = zi / dz - double(zIndex);
    if(zIndex < 0) zIndex = zIndex = _nZBins - 1;
    if(zIndex > _nZBins - 1) zIndex = 0;
    zIm = zIndex - 1;
    if(zIm < 0) zIm = _nZBins - 1;
    zIp = zIndex + 1;
    if(zIp > _nZBins - 1)  zIp = 0;
    _ZDist[zIm]   += 0.5 * (0.5 - zfrac) * (0.5 - zfrac);
    _ZDist[zIndex] += 0.75 - zfrac * zfrac;
    _ZDist[zIp]   += 0.5 * (0.5 + zfrac) * (0.5 + zfrac);
  }

  _xsig   /= double(mp._nMacros);
  _pxsig  /= double(mp._nMacros);
  _xpxcor /= double(mp._nMacros);
  _ysig   /= double(mp._nMacros);
  _pysig  /= double(mp._nMacros);
  _ypycor /= double(mp._nMacros);

  _xemit  = pow(_xsig * _pxsig - _xpxcor * _xpxcor, 0.5);
  _xalpha = -_xpxcor / _xemit;
  _xbeta  = _xsig / _xemit;
  _yemit  = pow(_ysig * _pysig - _ypycor * _ypycor, 0.5);
  _yalpha = -_ypycor / _yemit;
  _ybeta  = _ysig / _yemit;

  if(_ysig > _xsig)
  {
    _b = 2.0 * pow(_xsig, 0.5);
    _a = 2.0 * pow(_ysig, 0.5);
  }
  else
  {
    _a = 2.0 * pow(_xsig, 0.5);
    _b = 2.0 * pow(_ysig, 0.5);
  }
  _asq = _a * _a;
  _bsq = _b * _b;
  _csq = (_a + _b) * (_a - _b);
  _c   = pow(_csq, 0.5);

/*
  cout << "_xc, _pxc, _yc, _pyc = "
       << _xc << "   " << _pxc << "   "
       << _yc << "   " << _pyc << "\n";
  cout << "_xsig, _pxsig, _xpxcor, _ysig, _pysig, _ypycor = "
       << _xsig << "   " << _pxsig << "   " << _xpxcor << "   "
       << _ysig << "   " << _pysig << "   " << _ypycor << "\n";
  cout << "_a, _b, _c, _asq, _bsq, _csq = "
       << _a   << "   " << _b   << "   " << _c   << "   "
       << _asq << "   " << _bsq << "   " << _csq << "\n";
  cout << "_xemit, _xalpha, _xbeta, _yemit, _yalpha, _ybeta = "
       << _xemit << "   " << _xalpha << "   " << _xbeta   << "   "
       << _yemit << "   " << _yalpha << "   " << _ybeta << "\n";
  cout << "_nZBins = " << _nZBins << "\n";
  for(n = 0; n < _nZBins; n++)
  {
  cout << "n, _ZDist[n] = " << n << "   " << _ZDist[n] << "\n";
  }
  cout << "\n\n";
*/
  }

//-------------------------------------------------------------------------
double ElliptCalculator::_getu(double x, double y)
{
  double um, up, du, u, difm, difp, dif, dmp, tol;
  double xsq, ysq, snh, csh, snhsq, cshsq;
  du = 1.0;
  tol = 1.0e-12;
  xsq = x * x;
  ysq = y * y;

  if((xsq / _asq + ysq / _bsq) < 1.0)
  {
    return 0.0;
  }

  um = 0.0;
  snh = sinh(um);
  csh = cosh(um);
  snhsq = snh * snh;
  cshsq = csh * csh;
  difm = xsq * snhsq + ysq * cshsq - _csq * snhsq * cshsq;

  up = um;
  difp = difm;
  dmp = 1.0;
  while(dmp > 0.0)
  {
    um = up;
    difm = difp;
    up = um + du;
    snh = sinh(up);
    csh = cosh(up);
    snhsq = snh * snh;
    cshsq = csh * csh;
    difp = xsq * snhsq + ysq * cshsq - _csq * snhsq * cshsq;
    dmp = difm * difp;
  }
  while(du > tol)
  {
    u = 0.5 * (um + up);
    snh = sinh(u);
    csh = cosh(u);
    snhsq = snh * snh;
    cshsq = csh * csh;
    dif = xsq * snhsq + ysq * cshsq - _csq * snhsq * cshsq;
    dmp = difm * dif;
    if(dmp > 0.0)
    {
      um = u;
      difm = dif;
    }
    else
    {
      up = u;
      difp = dif;
    }
    du = up - um;
  }
  return u;
}

//-------------------------------------------------------------------------
double ElliptCalculator::_gettheta(double x, double y)
{
  double thm, thp, dth, th, difm, difp, dif, dmp, tol;
  double xsq, ysq, sn, cs, snsq, cssq;

  double pi = 4.0 * atan2(1.0, 1.0);
  dth = pi / 20.0;
  tol = 1.0e-12;
  xsq = x * x;
  ysq = y * y;

  if((xsq / _asq + ysq / _bsq) < 1.0)
  {
    return 0.0;
  }

  thm = 0.0;
  sn = sin(thm);
  cs = cos(thm);
  snsq = sn * sn;
  cssq = cs * cs;
  difm = xsq * snsq - ysq * cssq - _csq * snsq * cssq;

  thp = thm;
  difp = difm;
  dmp = 1.0;
  while(dmp > 0.0)
  {
    thm = thp;
    difm = difp;
    thp = thm + dth;
    sn = sin(thp);
    cs = cos(thp);
    snsq = sn * sn;
    cssq = cs * cs;
    difp = xsq * snsq - ysq * cssq - _csq * snsq * cssq;
    dmp = difm * difp;
  }
  while(dth > tol)
  {
    th = 0.5 * (thm + thp);
    sn = sin(th);
    cs = cos(th);
    snsq = sn * sn;
    cssq = cs * cs;
    dif = xsq * snsq - ysq * cssq - _csq * snsq * cssq;
    dmp = difm * dif;
    if(dmp > 0.0)
    {
      thm = th;
      difm = dif;
    }
    else
    {
      thp = th;
      difp = dif;
    }
    dth = thp - thm;
  }
  if(x < 0.0)
  {
    if(y < 0.0)
    {
      th = pi + th;
    }
    else
    {
      th = pi - th;
    }
  }
  else
  {
    if(y < 0.0)
    {
      th = - th;
    }
  }
  return th;
}

//-------------------------------------------------------------------------
void ElliptCalculator::_BoundaryCoeffs()
{
  int n, i;
  double x, y, xc, yc, u, th;

  if(_ysig > _xsig)
  {
    xc =  _yc;
    yc = -_xc;
  }
  else
  {
    xc =  _xc;
    yc =  _yc;
  }

  double* BPPhi;
  BPPhi = new double[_BPPoints];
  for(i = 0; i < _BPPoints; i++)
  {
    BPPhi[i] = 0.0;
    if(_ysig > _xsig)
    {
      x =  _BPy[i];
      y = -_BPx[i];
    }
    else
    {
      x =  _BPx[i];
      y =  _BPy[i];
    }
    x -= xc;
    y -= yc;
    if((x * x / _asq + y * y / _bsq) < 1.0)
    {
      cout << "Beam Crosses Boundary, _asq, _bsq, _BPx, _BPy = "
           << _asq    << "   " << _bsq    << "   "
           << _BPx[i] << "   " << _BPy[i] << "\n";
      return;
    }
    u  = _getu(x, y);
    th = _gettheta(x, y);
    BPPhi[i] = -u - 0.5 * exp(-2.0 * u) * cos(2.0 * th);
  }

  double* RHS;
  RHS = new double[2 * _BPModes + 1];
  for(n = 0; n < 2 * _BPModes + 1; n++)
  {
    RHS[n] = 0.0;
    for(i = 0; i < _BPPoints; i++)
    {
      RHS[n] -= _BPPhiH1[i][n] * BPPhi[i];
    }
  }

  for(n = 0; n < 2 * _BPModes + 1; n++)
  {
    _BPCoeffs[n] = 0.0;
    for(i = 0; i < 2 * _BPModes + 1; i++)
    {
      _BPCoeffs[n] += _BPPhiH2[n][i] * RHS[i];
    }
  }

  double BPresi, BPresf;
  BPresi = 0.0;
  BPresf = 0.0;
  for(i = 0; i < _BPPoints; i++)
  {
    BPresi += BPPhi[i] * BPPhi[i];
    for(n = 0; n < 2 * _BPModes + 1; n++)
    {
      BPPhi[i] += _BPPhiH1[i][n] * _BPCoeffs[n];
    }
    BPresf += BPPhi[i] * BPPhi[i];
  }

/*
  double BPrat = 1.0;
  if(BPresi != 0.0) BPrat = BPresf / BPresi;
  cout << "Boundary residual BPresi, BPresf, BPrat = "
       << BPresi << "   " << BPresf << "   " << BPrat << "\n";
*/

  delete BPPhi;
  delete RHS;

  _doBoundary = 1;
}

//-------------------------------------------------------------------------
void ElliptCalculator::_ApplyForce(MacroPart& mp, double SCKick_Coeff)
{
  int n, i;
  double x, y, xc, yc, u, th;
  double apb, xapb, yapb;
  double expp, expm, csh, snh, cs, sn;
  double exp2, cs2, sn2, dudx, dudy, dthdx, dthdy, denom;

  int m, mc, ms;
  double r, rmc, rms, rsq, xcoeff, ycoeff;
  std::complex<double> z;
  std::complex<double> zm;

  double kickx, kicky;

  int zIndex, zIm, zIp;
  double dz = _ZLength / double(_nZBins);
  double zi, zfrac, zfactor;

  double pi = 4.0 * atan2(1.0, 1.0);

  apb  = _a + _b;
  xapb = 2.0 / (_a * apb);
  yapb = 2.0 / (_b * apb);

  if(_ysig > _xsig)
  {
    xc =  _yc;
    yc = -_xc;
  }
  else
  {
    xc =  _xc;
    yc =  _yc;
  }

  for(i = 1; i <= mp._nMacros; i++)
  {
    if(_ysig > _xsig)
    {
      x =  mp._y(i);
      y = -mp._x(i);
    }
    else
    {
      x =  mp._x(i);
      y =  mp._y(i);
    }
    x -= xc;
    y -= yc;
    if((x * x / _asq + y * y / _bsq) > 1.0)
    {
      u     = _getu(x, y);
      th    = _gettheta(x, y);
      expp  = exp(u);
      expm  = 1.0 / expp;
      csh   = 0.5 * (expp + expm);
      snh   = 0.5 * (expp - expm);
      cs    = cos(th);
      sn    = sin(th);
      exp2  = expm * expm;
      cs2   = cs * cs - sn * sn;
      sn2   = 2.0 * cs * sn;
      denom = _c * (snh * snh + sn * sn);
      dudx  =  snh * cs / denom;
      dudy  =  csh * sn / denom;
      dthdx = -csh * sn / denom;
      dthdy =  snh * cs / denom;
      kickx = (1.0 - exp2 * cs2) * dudx -exp2 * sn2 * dthdx;
      kicky = (1.0 - exp2 * cs2) * dudy -exp2 * sn2 * dthdy;
    }
    else
    {
      kickx = xapb * x;
      kicky = yapb * y;
    }

    if(_doBoundary != 0)
    {
      rsq = x * x + y * y;
      xcoeff = x / rsq;
      ycoeff = y / rsq;
      z = std::complex<double>(x / _BPrnorm, y / _BPrnorm);
      zm = std::complex<double>(1.0, 0.0);
      for(m = 1; m <= _BPModes; m++)
      {
        zm *= z;
        rmc = std::real(zm);
        rms = std::imag(zm);
        mc = 2 * m - 1;
        ms = 2 * m;
        kickx -= double(m) *
                (_BPCoeffs[mc] * (rmc * xcoeff + rms * ycoeff) +
                 _BPCoeffs[ms] * (rms * xcoeff - rmc * ycoeff));
        kicky -= double(m) *
                (_BPCoeffs[mc] * (rmc * ycoeff - rms * xcoeff) +
                 _BPCoeffs[ms] * (rms * ycoeff + rmc * xcoeff));
      }
    }

    zi = mp._phi(i) - _ZCenter;
    if(zi >  _ZLength / 2.0) zi -= _ZLength;
    if(zi < -_ZLength / 2.0) zi += _ZLength;
    zi += _ZLength / 2.0;
    zIndex = int(zi / dz + 0.5);
    zfrac = zi / dz - double(zIndex);
    if(zIndex < 0) zIndex = _nZBins - 1;
    if(zIndex > _nZBins - 1) zIndex = 0;
    zIm = zIndex - 1;
    if(zIm < 0) zIm = _nZBins - 1;
    zIp = zIndex + 1;
    if(zIp > _nZBins - 1)  zIp = 0;
    zfactor = 0.5 * (0.5 - zfrac) * (0.5 - zfrac) * _ZDist[zIm] +
              (0.75 - zfrac * zfrac) * _ZDist[zIndex] +
              0.5 * (0.5 + zfrac) * (0.5 + zfrac) * _ZDist[zIp];
    zfactor *= SCKick_Coeff;

    if(_ysig > _xsig)
    {
      mp._xp(i) += -zfactor * kicky;
      mp._yp(i) +=  zfactor * kickx;
    }
    else
    {
      mp._xp(i) +=  zfactor * kickx;
      mp._yp(i) +=  zfactor * kicky;
    }
  }
}

//-------------------------------------------------------------------------
void ElliptCalculator::_ClearEllipt()
{
  int k;

  for(k = 0; k < _nZBins; k++)
  {
    _ZDist[k] = 0.0;
  }
  _doBoundary = 0;
}

