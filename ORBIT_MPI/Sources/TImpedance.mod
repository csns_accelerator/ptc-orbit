/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TImpedance.mod
//
// AUTHOR
//   Slava Danilov, Jeff Holmes, John Galambos, ORNL, vux@ornl.gov
//
// CREATED
//   07/15/2000
//   Revisited by Jeff Holmes 6/2014
//
// DESCRIPTION
//   Module descriptor file for the TImpedance module
//   Contains information about transverse impedance
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module TImpedance    - "TImpedance Module."
                     - "Written by Slava Danilov, ORNL, vux@ornl.gov"
{
  Inherits Consts, Particles, Ring, Injection;

  Errors
    badTImpedance    - "Sorry, you asked for a non existent TImpedance",
    badImpedSize     - "The size of the impedance vector is too small",
    badFFTTImp       - "This routine only applies to FFTTImpedance",
    badHerdNo        - "Sorry, you asked for an undefined macro Herd",
    tooManyFFTTImpedances - "Can only use one FFT TImpedance node now";

  public:

    Void ctor()
                     - "Constructor for the TImpedance module."
                     - "Initializes build values.";

    Integer
      nTImpedance    - "Number of TImpedance Nodes in the Ring",
      nFFTTImpedance - "Number of FFTTImpedance Nodes in the Ring",
      nLBinsTImp     - "Number of longitudinal bins to use",
      useXdimension  - "If 1, X collective force is taken into account",
      useYdimension  - "If 1, Y collective force is taken into account",
      nturnCount     - "Turn counter, needed for phase of bet. harmonics";

    Real
      checkangle     - "Angle kick for dimensions check";

  Void addFFTTImpedance(const String &name, const Integer &order,
                        ComplexVector &Zxplus, ComplexVector &Zxminus,
                        ComplexVector &Zyplus,  ComplexVector &Zyminus,
                        Real &b_a, Integer &useAvg, Integer &nMacrosMin)
                     - "Routine to add an FFT TImpedance node";
  Void addFreqDepTImp(const String &name, const Integer &order,
                      Integer &nTable, RealVector &fTable,
                      ComplexVector &zxTable, ComplexVector &zyTable,
                      Real &b_a, Integer &useAvg, Integer &nMacrosMin)
                     - "Routine to add a freq dependent TImpedance node";
  Void addFreqBetDepTImp(const String &name, const Integer &order,
                         Integer &nbTable, RealVector &bTable,
                         Integer &nfTable, RealVector &fTable,
                         ComplexMatrix &zxTable, ComplexMatrix &zyTable,
                         Real &b_a, Integer &useAvg, Integer &nMacrosMin)
                     - "Routine to add a freq and bet dependent TImpedance node";
  Void showTImpedance(const Integer &n, Ostream &os)
                     - "Routine to show the TImpedance info";
  Real dumpKick(const Integer &n, Real &angle)
                     - "Routine to dump kick at an angle";
  Void dumpPhiCount(const Integer &n, RealVector &p)
                     - "Routine to dump phi Bin information to vector p";
  Void dumpFFTStuff(const Integer &n, const Integer &maxharm,
                    const Integer &nturn, Ostream &osharmonics,
                    const Integer &nparticles)
                     - "Routine to dump FFT information";
  Void dump_TImped(const Integer &n, Ostream &os)
                     - "Routine to dump impedances to output stream";
}
