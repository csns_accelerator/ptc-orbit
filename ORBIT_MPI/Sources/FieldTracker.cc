/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   FieldTracker.cc
//
// AUTHOR
//   Jeff Holmes
//
// CREATED
//   03/08/2007
//
// MODIFIED
//
// DESCRIPTION
//   Code for module for tracking through 3D magnetic fields
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////
//
// include files
//
///////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "StreamType.h"
#include "StrClass.h"
#include "TeaPot.h"
#include "ThinLens.h"
#include "FieldTracker.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include "Consts.h"
#include "Injection.h"
#include "Spline.h"


///////////////////////////////////////////////////////////////////////////
//
// Static Definitions
//
///////////////////////////////////////////////////////////////////////////

Array(ObjectPtr) FTPointers;
extern Array(ObjectPtr) syncP, mParts, nodes, tMaps;

// Granularity to allocate entries in work arrays

const Integer Node_Alloc_Chunk = 10;


///////////////////////////////////////////////////////////////////////////
//
// Global Variables for Subroutine:
//
///////////////////////////////////////////////////////////////////////////

Real  xField3D = 0.0,  yField3D = 0.0,  zField3D = 0.0,
     BxField3D = 0.0, ByField3D = 0.0, BzField3D = 0.0;
     
Real gradBx = 0.0,  gradBy = 0.0, gradBz = 0.0;

Real   phi3Dx,   phi3Dy,   phi3Dz,
      phi3Dxx,  phi3Dxy,  phi3Dxz,  phi3Dyy,  phi3Dyz,  phi3Dzz,
     phi3Dxxx, phi3Dxxy, phi3Dxxz, phi3Dxyy, phi3Dxyz, phi3Dxzz,
     phi3Dyyy, phi3Dyyz, phi3Dyzz, phi3Dzzz;

Integer      nXGrid, nYGrid, nZGrid;
Vector(Real)  XGrid,  YGrid,  ZGrid;
Array3(Real) BXGrid, BYGrid, BZGrid, BMagGrid;

///////////////////////////////////////////////////////////////////////////
//
// Local Functions:
//
///////////////////////////////////////////////////////////////////////////

Integer FTfactorial(Integer n)
{
   return (n>1) ? n*FTfactorial(n-1) :1;
}

Void FTfindPhases(MapBase &tm, MacroPart& mp, const Integer &i);
Void FTdoTunes(MapBase &tm, MacroPart &mp);

Real FTxPhase, FTyPhase;

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   FTBase
//
// INHERITANCE RELATIONSHIPS
//   FTBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a base class for storing FieldTracker Node information.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class FTBase : public MapBase
{
  Declare_Standard_Members(FTBase, MapBase);

public:
    FTBase(const String &n, const Integer &order,
           const Real &bx, const Real &by,
		   const Real &ax, const Real &ay,
           const Real &ex, const Real &epx,
           const Real &l, const String &et):
    MapBase(n, order, bx, by, ax, ay, ex, epx, l, et)
    {
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Void _showFT(Ostream &sout)=0;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FTBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(FTBase, MapBase);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   FT3D
//
// INHERITANCE RELATIONSHIPS
//   FT3D -> FTBase -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class to track in 3D magnetic field configurations.
//
// PUBLIC MEMBERS
//   n       (str) --> _name    : Name for this node
//   order   (int) --> _oindex  : node order index
//   bx     (real) --> _betaX   : The horizontal beta value at the
//                                beginning of the Node [m]
//   by     (real) --> _betaY   : The vertical beta value at the
//                                beginning of the Node [m]
//   ax     (real) --> _alphaX  : The horizontal alpha value at the
//                                beginning of the Node
//   ay     (real) --> _alphaY  : The horizontal alpha  value at the
//                                beginning of the Node
//   ex     (real) --> _etaX    : The horizontal dispersion [m]
//   epx    (real) --> _etaPX   : The horizontal dispersion prime
//   l      (real) --> _length  : The length of the node
//   et   (string) --> _et      : Element Type
//   sub     (sub) --> _sub     : Subroutine to acquire fields
//   zi     (real) --> _zi      : Initial tracking position [m]
//   zf     (real) --> _zf      : Final tracking position [m]
//   ds     (real) --> _ds      : Integration step size
//   niters  (int) --> _niters  : Number predictor-corrector iterations
//   resid  (real) --> _resid   : Predictor-corrector residual
//   xrefi  (real) --> _xrefi   : Initial reference particle x [mm]
//   yrefi  (real) --> _yrefi   : Initial reference particle y [mm]
//   eulerai(real) --> _eulerai : Initial reference Euler angle alpha [mr]
//   eulerbi(real) --> _eulerbi : Initial reference Euler angle beta [mr]
//   eulergi(real) --> _eulergi : Initial reference Euler angle gamma [mr]
//   xreff  (real) --> _xreff   : Final reference particle x [mm]
//   yreff  (real) --> _yreff   : Final reference particle y [mm]
//   euleraf(real) --> _euleraf : Final reference Euler angle alpha [mr]
//   eulerbf(real) --> _eulerbf : Final reference Euler angle beta [mr]
//   eulergf(real) --> _eulergf : Final reference Euler angle gamma [mr]
//   sref   (real) --> _sref    : Path length of reference particle [m]
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class FT3D : public FTBase
{
  Declare_Standard_Members(FT3D, FTBase);
  public:

  FT3D(const String &n, const Integer &order,
       const Real &bx, const Real &by,
       const Real &ax, const Real &ay,
       const Real &ex, const Real &epx,
       const Real &l, const String &et,
       const Subroutine sub,
       const Real &zi, const Real &zf,
       const Real &ds, const Integer &niters,
       const Real &resid,
       const Real &xrefi, const Real &yrefi,
       const Real &eulerai, const Real &eulerbi,
       const Real &eulergi, const Integer &apflag):
  FTBase(n, order, bx, by, ax, ay, ex, epx, l, et),
  _xrefi(xrefi), _yrefi(yrefi),
  _eulerai(eulerai), _eulerbi(eulerbi), _eulergi(eulergi),
  _zi(zi), _zf(zf), _sub(sub), _ds(ds),
  _niters(niters), _resid(resid), 
  _apflag(apflag)
  {
  }

  Void nameOut(String &wname) { wname = _name; }
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);
  Void _showFT(Ostream &sout);

  Subroutine _sub;
  Integer _niters;
  Integer _apflag; 
  Real  _zi, _zf, _ds, _resid;
  Real _xrefi, _yrefi, _eulerai, _eulerbi, _eulergi;
  Real _xreff, _yreff, _euleraf, _eulerbf, _eulergf, _sref;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS FT3D
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(FT3D, FTBase);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FT3D::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FT3D::_nodeCalculator(MacroPart &mp)
{
 
  // If tune calculation is on, check storage sizes:
  Integer oldSize, i;

  Ring::betaX  = _betaX;
  Ring::betaY  = _betaY;
  Ring::alphaX = _alphaX;
  Ring::alphaY = _alphaY;
  Ring::etaX   = _etaX;
  Ring::etaPX  = _etaPX;
  Ring::gammaX = (1. + Sqr(Ring::alphaX))/Ring::betaX;
  Ring::gammaY = (1. + Sqr(Ring::alphaY))/Ring::betaY;

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  Real pMomentum = Sqrt(Sqr(mp._syncPart._eTotal) - Sqr(mp._syncPart._e0));
  Real coeff = 1.e-09 * Consts::vLight * mp._syncPart._charge / pMomentum;
  Real ds, resid;
  Real  dx,  dy,  dz,  dxo,  dyo,  dzo;
  Real dpx, dpy, dpz, dpxo, dpyo, dpzo;

  Real eulera = 1.0e-03 * _eulerai;
  Real eulerb = 1.0e-03 * _eulerbi;
  Real eulerg = 1.0e-03 * _eulergi;

  Real  xref = 1.0e-03 * _xrefi;
  Real  yref = 1.0e-03 * _yrefi;
  Real  zref = _zi;
  Real pxref = sin(eulerb) * cos(eulera);
  Real pyref = sin(eulerb) * sin(eulera);
  Real pzref = cos(eulerb);

  _sref = 0.0;
  Integer iquit = 0;
  Real xwidth = FieldTracker::xFoilMax - FieldTracker::xFoilMin;
   
  OFstream fio("RefPath", std::ios::out);

  xField3D = xref;
  yField3D = yref;
  zField3D = zref;
  _sub();
  BxField3D *= FieldTracker::BScale;
  ByField3D *= FieldTracker::BScale;
  BzField3D *= FieldTracker::BScale;

  fio << _sref << "  " <<  xref << "  " <<  yref << "  " <<  zref << "  "
                       << pxref << "  " << pyref << "  " << pzref << "  "
                       << BxField3D << "  "
                       << ByField3D << "  "
                       << BzField3D << "\n";

  if(FieldTracker::doRefPath != 0) iquit = 1;


  Real  rref = pow(xref * xref + yref * yref, 0.5);

  while(iquit == 0)
  {
    ds = _ds;
    dz = ds * pzref;
    if(dz > (_zf - zref))
    {
      dz = _zf - zref;
      ds = dz / pzref;
      iquit = 1;
    }
    dx = ds * pxref;
    dy = ds * pyref;

    xField3D = xref + dx / 2.0;
    yField3D = yref + dy / 2.0;
    zField3D = zref + dz / 2.0;
    _sub();

    BxField3D *= FieldTracker::BScale;
    ByField3D *= FieldTracker::BScale;
    BzField3D *= FieldTracker::BScale;
    dpx = coeff * (dy * BzField3D - dz * ByField3D);
    dpy = coeff * (dz * BxField3D - dx * BzField3D);
    dpz = coeff * (dx * ByField3D - dy * BxField3D);


    for(i = 1; i < _niters; i++)
    {
      dxo = dx;
      dyo = dy;
      dzo = dz;
      dpxo = dpx;
      dpyo = dpy;
      dpzo = dpz;

      ds = _ds;
      dz = ds * (pzref + dpz / 2.0);
      if(dz > (_zf - zref))
      {
        dz = _zf - zref;
        ds = dz / (pzref + dpz / 2.0);
        iquit = 1;
      }
      dx = ds * (pxref + dpx / 2.0);
      dy = ds * (pyref + dpy / 2.0);

      xField3D = xref + dx / 2.0;
      yField3D = yref + dy / 2.0;
      zField3D = zref + dz / 2.0;
      _sub();
      BxField3D *= FieldTracker::BScale;
      ByField3D *= FieldTracker::BScale;
      BzField3D *= FieldTracker::BScale;
      dpx = coeff * (dy * BzField3D - dz * ByField3D);
      dpy = coeff * (dz * BxField3D - dx * BzField3D);
      dpz = coeff * (dx * ByField3D - dy * BxField3D);
      resid = Sqrt((dx  -  dxo) * (dx - dxo)   +
                   (dpx - dpxo) * (dpx - dpxo) +
                   (dy  -  dyo) * (dy - dyo)   +
                   (dpy - dpyo) * (dpy - dpyo) +
                   (dz  -  dzo) * (dz - dzo)   +
                   (dpz - dpzo) * (dpz - dpzo) );
      if(resid < _resid) break;
   
    }

    xref  +=  dx;
    pxref += dpx;
    yref  +=  dy;
    pyref += dpy;
    zref  +=  dz;
    pzref += dpz;
    _sref +=  ds;

    if(_sref > FieldTracker::sMax * _length) iquit = 1;
    rref = pow(xref * xref + yref * yref, 0.5);
    if(rref > FieldTracker::rMax) iquit = 1;

    fio << _sref << "  " <<  xref << "  " <<  yref << "  " <<  zref << "  "
                         << pxref << "  " << pyref << "  " << pzref << "  "
                         << BxField3D << "  "
                         << ByField3D << "  "
                         << BzField3D << "\n";
  }

  fio.close();

  _xreff = 1.e+03 * xref;
  _yreff = 1.e+03 * yref;

  eulera = atan2(pyref, pxref);
  eulerb = atan2(Sqrt(pyref * pyref + pxref * pxref), pzref);
  _euleraf = 1.0e+03 * eulera;
  _eulerbf = 1.0e+03 * eulerb;
  _eulergf = -_euleraf;

  if(!TransMap::tuneCalcOn) return;

  // Check to see if more macros have been created since we started
  // the tune calculation:

  oldSize = Particles::xPhaseOld.rows();

  if (mp._nMacros > oldSize)
  {
    Particles::xPhaseOld.resize(mp._nMacros);
    Particles::yPhaseOld.resize(mp._nMacros);
    Particles::xPhaseTot.resize(mp._nMacros);
    Particles::yPhaseTot.resize(mp._nMacros);
    Particles::xTune.resize(mp._nMacros);
    Particles::yTune.resize(mp._nMacros);
    Particles::lTuneTrack.resize(mp._nMacros);

    FT3D *tm = FT3D::safeCast(tMaps(TransMap::nTransMaps-1));

    for(i = oldSize + 1; i <= mp._nMacros; i++) // set phases of new guys
    {
      FTfindPhases(*tm, mp,i);
      Particles::xPhaseOld(i) = FTxPhase;
      Particles::yPhaseOld(i) = FTyPhase;
    }
  }

}

Void FT3D::_showFT(Ostream &sout)
{
  sout << " FT3D " << _name << "\n";
  sout << " Index in ring " << _oindex << "\n";
  sout << " Position in ring [meters] " << _position << "\n";
  sout << " Length of FT3D = " << _length << "\n";
  sout << " Initial reference particle x = " << _xrefi << "\n";
  sout << " Initial reference particle y = " << _yrefi << "\n";
  sout << " Initial reference particle Euler alpha = " << _eulerai << "\n";
  sout << " Initial reference particle Euler beta = " << _eulerbi << "\n";
  sout << " Initial reference particle Euler gamma = " << _eulergi << "\n";
  sout << " Initial tracking position = " << _zi << "\n";
  sout << " Final reference particle x = " << _xreff << "\n";
  sout << " Final reference particle y = " << _yreff << "\n";
  sout << " Final reference particle Euler alpha = " << _euleraf << "\n";
  sout << " Final reference particle Euler beta = " << _eulerbf << "\n";
  sout << " Final reference particle Euler gamma = " << _eulergf << "\n";
  sout << " Final reference particle path length = " << _sref << "\n";
  sout << " Final tracking position = " << _zf << "\n";
  sout << " Integration step size = " << _ds << "\n";
  sout << " Predictor corrector iterations = " << _niters << "\n";
  sout << " Predictor corrector residual =  " << _resid << "\n";

  Real eulera = _euleraf / 1.e+03;
  Real eulerb = _eulerbf / 1.e+03;
  Real eulerg = _eulergf / 1.e+03;

  Real pxref = 1.e+03 * sin(eulerb) * cos(eulera);
  Real pyref = 1.e+03 * sin(eulerb) * sin(eulera);
  Real pzref = cos(eulerb);

  sout << " Final reference particle px = " << pxref << "\n";
  sout << " Final reference particle py = " << pyref << "\n";
  sout << " Final reference particle pz = " << pzref << "\n";
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FT3D::updatePartAtNode
//
// DESCRIPTION
//   Takes the particles through a kicker
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FT3D::_updatePartAtNode(MacroPart& mp)
{
  Integer i, j;

  if(_length < 0.0) return;
  if(Abs(_length) <= Consts::tiny) return;

  Real ds, resid;
  Real dx,  dy,  dz,  dxo,  dyo,  dzo;
  Real dpx, dpy, dpz, dpxo, dpyo, dpzo;

  Real xrefi = 1.0e-03 * _xrefi;
  Real yrefi = 1.0e-03 * _yrefi;
  Real xreff = 1.0e-03 * _xreff;
  Real yreff = 1.0e-03 * _yreff;

  Real ca, sa, cb, sb, cg, sg;

  Real eulera = 1.0e-03 * _eulerai;
  Real eulerb = 1.0e-03 * _eulerbi;
  Real eulerg = 1.0e-03 * _eulergi;

  ca = cos(eulera);
  sa = sin(eulera);
  cb = cos(eulerb);
  sb = sin(eulerb);
  cg = cos(eulerg);
  sg = sin(eulerg);

  Real cxx   =  cb * ca * cg - sa * sg;
  Real cxy   = -cb * ca * sg - sa * cg;
  Real cxz   =  sb * ca;
  Real cyx   =  cb * sa * cg + ca * sg;
  Real cyy   = -cb * sa * sg + ca * cg;
  Real cyz   =  sb * sa;
  Real czx   = -sb * cg;
  Real czy   =  sb * sg;
  Real czz   =  cb;

  eulera = 1.0e-03 * _euleraf;
  eulerb = 1.0e-03 * _eulerbf;
  eulerg = 1.0e-03 * _eulergf;

  ca = cos(eulera);
  sa = sin(eulera);
  cb = cos(eulerb);
  sb = sin(eulerb);
  cg = cos(eulerg);
  sg = sin(eulerg);

  Real dxx =  cb * ca * cg - sa * sg;
  Real dxy =  cb * sa * cg + ca * sg;
  Real dxz = -sb * cg;
  Real dyx = -cb * ca * sg - sa * cg;
  Real dyy = -cb * sa * sg + ca * cg;
  Real dyz =  sb * sg;
  Real dzx =  sb * ca;
  Real dzy =  sb * sa;
  Real dzz =  cb;

  Real eTotal, pMomentum, coeff, pmag;
  Real Factor = 2.0 * Consts::pi * Ring::harmonicNumber / Ring::lRing;
  Real gamma2i = 1.0 / (mp._syncPart._gammaSync * mp._syncPart._gammaSync);
  

  Real mpx, mpy, mpz, mpxp, mpyp, mpzp;
  Real xj, yj, zj, pxj, pyj, pzj, sj;
  int lost = 0;

    OFstream fio("Path", std::ios::out);

  if(FieldTracker::getPath == 1)
  {
    fio << "#s  x   y   z  px   py  pz" << "\n";
  }

  for(j = 1; j <= mp._nMacros; j++)
  {
    //std::cerr << "Next particle" << "\n";
    if(FieldTracker::getPath == 1)
    {
      fio<<"\n\n\n\n";
    }

    eTotal = mp._syncPart._eTotal + mp._deltaE(j);
    pMomentum = Sqrt(Sqr(eTotal) - Sqr(mp._syncPart._e0));
    double e = 1.602e-19;
    double amu = 1.66053886e-27;
    double fac = e * 1.0e9 * pMomentum / Consts::vLight;
    coeff = 1.e-09 * Consts::vLight * mp._syncPart._charge / pMomentum;
    mpx  = 1.0e-03 * mp._x(j);
    mpy  = 1.0e-03 * mp._y(j);
    mpz  = -(czx * mpx + czy * mpy) / czz;
    mpxp = 1.0e-03 * mp._xp(j);
    mpyp = 1.0e-03 * mp._yp(j);
    mpzp = 1.0 + mp._dp_p(j);
    mpx  += mpxp * mpz / mpzp;
    mpy  += mpyp * mpz / mpzp;
   
    xj = xrefi + cxx * mpx + cxy * mpy + cxz * mpz;
    yj = yrefi + cyx * mpx + cyy * mpy + cyz * mpz;
    zj = _zi;
    if(FieldTracker::foilAngle != 0)
    {
      zj = _zi + (xj - xrefi) / tan(FieldTracker::foilAngle * Consts::twoPi / 360.0);
    }
    pxj = cxx * mpxp + cxy * mpyp + cxz * mpzp;
    pyj = cyx * mpxp + cyy * mpyp + cyz * mpzp;
    pzj = czx * mpxp + czy * mpyp + czz * mpzp;
    pmag = Sqrt(pxj * pxj + pyj * pyj + pzj * pzj);
    pxj /= pmag;
    pyj /= pmag;
    pzj /= pmag;

    sj = 0.0;
    lost = 0;

    Integer iquit = 0;
    Integer nreflections = 0;
    Real ppar0 = 0.0; Real pperp0 = 0.0; Real bmag0 = 0.0;
    Real firststep = 0;    

    Real  rj = pow(xj * xj + yj * yj, 0.5);
    
    while(iquit == 0)
    {
  
      ds = _ds;
      dz = ds * pzj; 
      if(dz > (_zf - zj))
      {
        dz = _zf - zj;
        ds = dz / pzj;
        iquit = 1;
      }
      dx = ds * pxj;
      dy = ds * pyj;

      xField3D = xj + dx / 2.0;
      yField3D = yj + dy / 2.0;
      zField3D = zj + dz / 2.0;
      _sub();
      BxField3D *= FieldTracker::BScale;
      ByField3D *= FieldTracker::BScale;
      BzField3D *= FieldTracker::BScale;
   
      dpx = coeff * (dy * BzField3D - dz * ByField3D);
      dpy = coeff * (dz * BxField3D - dx * BzField3D);
      dpz = coeff * (dx * ByField3D - dy * BxField3D);

      for(i = 1; i < _niters; i++)
      {
        dxo = dx;
        dyo = dy;
        dzo = dz;
        dpxo = dpx;
        dpyo = dpy;
        dpzo = dpz;
        ds = _ds;
        dz = ds * (pzj + dpz / 2.0);
        if(dz > (_zf - zj))
        {
          dz = _zf - zj;
          ds = dz / (pzj + dpz / 2.0);
          iquit = 1;
        }
        dx = ds * (pxj + dpx / 2.0);
        dy = ds * (pyj + dpy / 2.0);

        xField3D = xj + dx / 2.0;
        yField3D = yj + dy / 2.0;
        zField3D = zj + dz / 2.0;

        _sub();
        BxField3D *= FieldTracker::BScale;
        ByField3D *= FieldTracker::BScale;
        BzField3D *= FieldTracker::BScale;
        dpx = coeff * (dy * BzField3D - dz * ByField3D);
        dpy = coeff * (dz * BxField3D - dx * BzField3D);
        dpz = coeff * (dx * ByField3D - dy * BxField3D);
        resid = Sqrt((dx  -  dxo) * (dx - dxo)   +
                     (dpx - dpxo) * (dpx - dpxo) +
                     (dy  -  dyo) * (dy - dyo)   +
                     (dpy - dpyo) * (dpy - dpyo) +
                     (dz  -  dzo) * (dz - dzo)   +
                     (dpz - dpzo) * (dpz - dpzo) );
        if(resid < _resid) break;
      }

      xj  +=  dx;
      yj  +=  dy;
      zj  +=  dz;
      pxj += dpx;
      pyj += dpy;
      pzj += dpz;
      sj +=  ds;

      if(sj > FieldTracker::sMax * _length) iquit = 1;
      rj = pow(xj * xj + yj * yj, 0.5);
      if(rj > FieldTracker::rMax) iquit = 1;

      if(FieldTracker::getPath==1)
      {
        //Print out the particle coords
        fio <<  sj << "  " <<  xj << "  " <<  yj << "  " <<  zj << "  "
                           << pxj << "  " << pyj << "  " << pzj << "\n";
      }

      if(FieldTracker::aperture != 0)
      {
        Integer condition = FieldTracker::ApertureCondition(xj, yj, zj, xrefi, _zi, _ds);

        if(condition == 1)
        {
          if(!TransMap::tuneCalcOn)
          {
            //Lose particle, record coords in rect. system.
            mp._x(j)  = xj;
            mp._xp(j) = pxj;
            mp._y(j)  = yj;
            mp._yp(j) = pyj;
            mp._phi(j) = zj;
            mp._addLostMacro(j, _position);
            j--;
            iquit = 1;
            lost = 1;
          }
        }
        if(condition == 2)
        {
          if(!TransMap::tuneCalcOn)
          {
            Real energyfrac = FieldTracker::Scatter(pxj,pyj,pzj);

            if((energyfrac < 0.0) || _apflag == 0 || nreflections > 0)
            {
              //Lose particle, record coords in rect. system.
              mp._x(j)  = xj;
              mp._xp(j) = pxj;
              mp._y(j)  = yj;
              mp._yp(j) = pyj;
              mp._phi(j) = zj;
              mp._addLostMacro(j, _position);
              j--;
              iquit = 1;
              lost = 1;
            }
            else
            {
              nreflections += 1;

              Real energyloss = (mp._syncPart._eKinetic + mp._deltaE(j))*(1 - energyfrac);
              mp._deltaE(j) -= energyloss;
			  eTotal = mp._syncPart._eTotal + mp._deltaE(j);
			  pMomentum = Sqrt(Sqr(eTotal) - Sqr(mp._syncPart._e0));
			  coeff = 1.e-09 * Consts::vLight * mp._syncPart._charge / pMomentum;
				
              if(((mp._deltaE(j) + mp._syncPart._eKinetic) < 0.000001))
              {
                //Remove if less than 1 keV
                //Lose it.
                mp._x(j)  = xj;
                mp._xp(j) = pxj;
                mp._y(j)  = yj;
                mp._yp(j) = pyj;
                mp._phi(j) = zj;
                mp._addLostMacro(j, _position);
                j--;
                iquit = 1;
                lost = 1;
              }
            }
          }
        }
      }
    }

    if(lost == 0)
    {
      // If not lost, calc. final coords in ORBITs ref frame upon leaving 3D field.
      dpx  = dxx * pxj + dxy * pyj + dxz * pzj;
      dpy  = dyx * pxj + dyy * pyj + dyz * pzj;
      dpz  = dzx * pxj + dzy * pyj + dzz * pzj;
      dxo = xj - xreff;
      dyo = yj - yreff;
      dzo = dzx * dxo + dzy * dyo;

      dx = dxx * dxo + dxy * dyo - dpx * dzo / dpz;
      dy = dyx * dxo + dyy * dyo - dpy * dzo / dpz;
      mp._x(j)  = 1.0e+03 * dx;
      mp._xp(j) = 1.0e+03 * dpx * pmag;
      mp._y(j)  = 1.0e+03 * dy;
      mp._yp(j) = 1.0e+03 * dpy * pmag;
      mp._phi(j) += Factor * (sj * (1.0 - gamma2i * mp._dp_p(j)) -_sref);
      if(mp._phi(j) >  Consts::pi) mp._phi(j) -= Consts::twoPi;
      if(mp._phi(j) < -Consts::pi) mp._phi(j) += Consts::twoPi; 
    }
  }

  // particle tune calculation:

  if(!TransMap::tuneCalcOn) return; // return if tune calculation is off

  FTdoTunes(*this, mp);
  fio.close();

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::ApertureCondition
//
// DESCRIPTION
//   Check to see if a particle is going to be absorbed or reflected, or neither.
//
// PARAMETERS
//   x,y:      horizontal and vertical coordinates {mm}.
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Integer FieldTracker::ApertureCondition(Real &x,     Real &y,     Real &z,
                                        Real &xcent, Real &zcent, Real &step)
{
  Integer cond = 0;

  // For tilted foils, calculate the z position at particles x coord.

  if(FieldTracker::foilAngle != 0)
  {
    FieldTracker::zFoilMin = zcent + (x - xcent) / tan(FieldTracker::foilAngle * Consts::twoPi / 360.0);
    FieldTracker::zFoilMin -= 5.0 * step;
    FieldTracker::zFoilMax = FieldTracker::zFoilMin + step * 2.0;
    // Make it a few stepsize thick to ensure capture.
    FieldTracker::zBracketMin = zcent + (x - xcent) / tan(FieldTracker::foilAngle * Consts::twoPi / 360.0);
    FieldTracker::zBracketMin -= 5.0 * step;
    FieldTracker::zBracketMax = FieldTracker::zBracketMin + step * 2.0;
    // Make it a few stepsize thick to ensure capture.
    FieldTracker::zBracketArmMin = zcent + (x - xcent) / tan(FieldTracker::foilAngle * Consts::twoPi / 360.0);
    FieldTracker::zBracketArmMin -= 5.0 * step;
    FieldTracker::zBracketArmMax = FieldTracker::zBracketArmMin + step * 2.0;
    // Make it a few stepsize thick to ensure capture.
  }

  if(x > FieldTracker::xMax ||
     x < FieldTracker::xMin ||
     y > FieldTracker::yMax)
  {
    //Outside of aperture
    cond = 1;
    return cond;
  }

  if(y < FieldTracker::yMin)
  {
    //Below foil catcher
    cond = 2;
    return cond;
  }

  // Particle intercepts foil.
  if((x > FieldTracker::xFoilMin) &&
     (x < FieldTracker::xFoilMax) &&
     (y > FieldTracker::yFoilMin) &&
     (y < FieldTracker::yFoilMax) &&
     (z > FieldTracker::zFoilMin) &&
     (z < FieldTracker::zFoilMax))
  { 
    cond = 1;
    return cond;
  }

  // Particle intercepts foil bracket
  if((x > FieldTracker::xBracketMin) &&
     (x < FieldTracker::xBracketMax) &&
     (y > FieldTracker::yBracketMin) &&
     (y < FieldTracker::yBracketMax) &&
     (z > FieldTracker::zBracketMin) &&
     (z < FieldTracker::zBracketMax))
  {
    cond = 1;
    return cond;
  }

  //Particle intercepts foil bracket arm
  if((x > FieldTracker::xBracketArmMin) &&
     (x < FieldTracker::xBracketArmMax) &&
     (y > FieldTracker::yBracketArmMin) &&
     (y < FieldTracker::yBracketArmMax) &&
     (z > FieldTracker::zBracketArmMin) &&
     (z < FieldTracker::zBracketArmMax))
  {
    cond = 1;
    return cond;
  }

  //Particle intercepts catcher
  if(FieldTracker::useCatcher > 0)
  {
    if((y > FieldTracker::yCatcherMin) &&
       (y < FieldTracker::yCatcherMax) &&
       (z > FieldTracker::zCatcherMin) &&
       (z < FieldTracker::zCatcherMax))
    {
     cond = 1;
     return cond;
    }
  }
 return cond;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::Scatter
//
// DESCRIPTION
//   Scatter a particle.
//
// PARAMETERS
//   x,y:      horizontal and vertical coordinates {mm}.
//
// RETURNS
//   Real number with fractional energy value of particle after scatter event.
//   -1 means particle absorbed, not scattered. 
//
///////////////////////////////////////////////////////////////////////////

Real FieldTracker::Scatter(Real &pxj, Real &pyj, Real &pzj)
{ 
  double px = pxj;
  double py = pyj; 
  double pz = pzj;

  double cosphi = px/sqrt(px*px + pz*pz);
  double sinphi = pz/sqrt(px*px + pz*pz);
	      
  double pxlocal = px * cosphi + pz * sinphi;
  double pzlocal = pz * cosphi - px * sinphi;
  double pylocal = py;
  
  double frac = 0;	      
  double inangle = atan2(pylocal, pxlocal)*(360.0/Consts::twoPi);
  inangle = -inangle;

  if(inangle < 10){
    frac = 0.0;
  }
  else if(inangle > 30){
    frac = 1.0;
  }
  else{
    frac = (inangle - 10.0) / (20.0);
  }
  

  // ** Part 1 - In plane Scattering ** //
  Real totalbedo10 = 0.568; 
  Real totalbedo30 = 0.253;
  Real totalbedo = (1-frac) * totalbedo10 + frac * totalbedo30;
  //totalbedo = 1;
  Real rollalbedo = MathLib::ran1(Injection::randSeed);
  Real inplaneshift = 0.0;

  if(rollalbedo > totalbedo){
    return -1; //Absorb the particle
  }
  else{
    Vector(Real) degree, inalb10, inalb30;
    degree.resize(89); inalb10.resize(89), inalb30.resize(89);

    //Integrated albedo (normalized to 100) distributions for incoming angle
    //of 10 and 30 degrees, as measured counterclockwise from 
    //the -180 degree axis (in other words these distributions are for
    //incoming angles of are 150 and 170 degrees in a normal coordinate ref frame). 
    
    degree(1)=4-inplaneshift; inalb30(1) = 0.001795379; inalb10(1)= 0.000915977;
    degree(2)=6-inplaneshift; inalb30(2) = 0.005548168; inalb10(2)= 0.002781583;
    degree(3)=8-inplaneshift; inalb30(3) = 0.011872897; inalb10(3)= 0.005999395;
    degree(4)=10-inplaneshift; inalb30(4) = 0.021702231; inalb10(4) = 0.01093112;
    degree(5)=12-inplaneshift; inalb30(5) = 0.035877545; inalb10(5) = 0.018108829;
    degree(6)=14-inplaneshift; inalb30(6) = 0.055786768; inalb10(6) = 0.027850888;
    degree(7)=16-inplaneshift; inalb30(7) = 0.082244797; inalb10(7) = 0.040930387;
    degree(8)=18-inplaneshift; inalb30(8) = 0.116404817; inalb10(8) = 0.057912524;
    degree(9)=20-inplaneshift; inalb30(9) = 0.159652386;  inalb10(9) = 0.079277574;
    degree(10)=22-inplaneshift; inalb30(10) = 0.213444991; inalb10(10) = 0.10575705;
    degree(11)=24-inplaneshift; inalb30(11) = 0.278970198; inalb10(11) = 0.13798627;
    degree(12)=26-inplaneshift; inalb30(12) = 0.357928935; inalb10(12) = 0.176537653;
    degree(13)=28-inplaneshift; inalb30(13) = 0.451915819; inalb10(13) = 0.222165969;
    degree(14)=30-inplaneshift; inalb30(14) = 0.562017247; inalb10(14) = 0.275532081;
    degree(15)=32-inplaneshift; inalb30(15) = 0.69002346;  inalb10(15) = 0.337765325;
    degree(16)=34-inplaneshift; inalb30(16) = 0.837639338; inalb10(16) = 0.408956788;
    degree(17)=36-inplaneshift; inalb30(17) = 1.006123977; inalb10(17) = 0.490117233;
    degree(18)=38-inplaneshift; inalb30(18) = 1.197582591; inalb10(18) = 0.582167923;
    degree(19)=40-inplaneshift; inalb30(19) = 1.41306166; inalb10(19) = 0.685844248;
    degree(20)=42-inplaneshift; inalb30(20) = 1.654528474; inalb10(20) = 0.801997877;
    degree(21)=44-inplaneshift; inalb30(21) = 1.923910408; inalb10(21) = 0.931021876;
    degree(22)=46-inplaneshift; inalb30(22) = 2.222907994; inalb10(22) = 1.074279199;
    degree(23)=48-inplaneshift; inalb30(23) = 2.554065113; inalb10(23) = 1.232642483;
    degree(24)=50-inplaneshift; inalb30(24) = 2.917358054; inalb10(24) = 1.406587598;
    degree(25)=52-inplaneshift; inalb30(25) = 3.315329116; inalb10(25) = 1.59732369;
    degree(26)=54-inplaneshift; inalb30(26) = 3.74981241; inalb10(26) = 1.805815541;
    degree(27)=56-inplaneshift; inalb30(27) = 4.222523088; inalb10(27) = 2.032535321;
    degree(28)=58-inplaneshift; inalb30(28) = 4.73456375; inalb10(28) = 2.278504893;
    degree(29)=60-inplaneshift; inalb30(29) = 5.289420031; inalb10(29) = 2.544839499;
    degree(30)=62-inplaneshift; inalb30(30) = 5.887376473; inalb10(30) = 2.832446482;
    degree(31)=64-inplaneshift; inalb30(31) = 6.530693602; inalb10(31) = 3.142462225;
    degree(32)=66-inplaneshift; inalb30(32) = 7.22005906; inalb10(32) = 3.475841642;
    degree(33)=68-inplaneshift; inalb30(33) = 7.958515862; inalb10(33) = 3.833985391;
    degree(34)=70-inplaneshift; inalb30(34) = 8.748229686; inalb10(34) = 4.217703914;
    degree(35)=72-inplaneshift; inalb30(35) = 9.589489027; inalb10(35) = 4.627860511;
    degree(36)=74-inplaneshift; inalb30(36) = 10.48479153; inalb10(36) = 5.066032021;
    degree(37)=76-inplaneshift; inalb30(37) = 11.43683243; inalb10(37) = 5.533407684;
    degree(38)=78-inplaneshift; inalb30(38) = 12.44526001; inalb10(38) = 6.031426916;
    degree(39)=80-inplaneshift; inalb30(39) = 13.51361128; inalb10(39) = 6.560905447;
    degree(40)=82-inplaneshift; inalb30(40) = 14.64198108; inalb10(40) = 7.123404261;
    degree(41)=84-inplaneshift; inalb30(41) = 15.83402894; inalb10(41) = 7.719652757;
    degree(42)=86-inplaneshift; inalb30(42) = 17.08962444; inalb10(42) = 8.352811665;
    degree(43)=88-inplaneshift; inalb30(43) = 18.41054597; inalb10(43) = 9.022350672;
    degree(44)=90-inplaneshift; inalb30(44) = 19.79965475; inalb10(44) = 9.731055238;
    degree(45)=92-inplaneshift; inalb30(45) = 21.25676505; inalb10(45) = 10.47970938;
    degree(46)=94-inplaneshift; inalb30(46) = 22.78515698; inalb10(46) = 11.27122188;
    degree(47)=96-inplaneshift; inalb30(47) = 24.38421406; inalb10(47) = 12.10614243;
    degree(48)=98-inplaneshift; inalb30(48) = 26.05546963; inalb10(48) = 12.98739216;
    degree(49)=100-inplaneshift; inalb30(49) = 27.80038198; inalb10(49) = 13.91858987;
    degree(50)=102-inplaneshift; inalb30(50) = 29.62052399; inalb10(50) = 14.90163481;
    degree(51)=104-inplaneshift; inalb30(51) = 31.51595889; inalb10(51) = 15.93967891;
    degree(52)=106-inplaneshift; inalb30(52) = 33.48536672; inalb10(52) = 17.0310449;
    degree(53)=108-inplaneshift; inalb30(53) = 35.53282197; inalb10(53) = 18.17672117;
    degree(54)=110-inplaneshift; inalb30(54) = 37.6539656; inalb10(54) = 19.37771724;
    degree(55)=112-inplaneshift; inalb30(55) = 39.84968683; inalb10(55) = 20.63768717;
    degree(56)=114-inplaneshift; inalb30(56) = 42.11983546; inalb10(56) = 21.95890724;
    degree(57)=116-inplaneshift; inalb30(57) = 44.45995368; inalb10(57) = 23.34401139;
    degree(58)=118-inplaneshift; inalb30(58) = 46.86978461; inalb10(58) = 24.7957798;
    degree(59)=120-inplaneshift; inalb30(59) = 49.34627339; inalb10(59) = 26.31783127;
    degree(60)=122-inplaneshift; inalb30(60) = 51.88581581; inalb10(60) = 27.91574553;
    degree(61)=124-inplaneshift; inalb30(61) = 54.4799191; inalb10(61) =  29.59334047;
    degree(62)=126-inplaneshift; inalb30(62) = 57.12034736; inalb10(62) = 31.35702387;
    degree(63)=128-inplaneshift; inalb30(63) = 59.80077743; inalb10(63) = 33.2148949; 
    degree(64)=130-inplaneshift; inalb30(64) = 62.51109624; inalb10(64) = 35.17053007;
    degree(65)=132-inplaneshift; inalb30(65) = 65.2404991;  inalb10(65) = 37.23585698;
    degree(66)=134-inplaneshift; inalb30(66) = 67.97499209; inalb10(66) = 39.41739444;
    degree(67)=136-inplaneshift; inalb30(67) = 70.69999246; inalb10(67) = 41.72636531;
    degree(68)=138-inplaneshift; inalb30(68) = 73.40296456; inalb10(68) = 44.17362249;
    degree(69)=140-inplaneshift; inalb30(69) = 76.06125175; inalb10(69) = 46.77243258;
    degree(70)=142-inplaneshift; inalb30(70) = 78.65959551; inalb10(70) = 49.5321157;
    degree(71)=144-inplaneshift; inalb30(71) = 81.17565929; inalb10(71) = 52.46396518;
    degree(72)=146-inplaneshift; inalb30(72) = 83.58936315; inalb10(72) = 55.57560976;
    degree(73)=148-inplaneshift; inalb30(73) = 85.8800383; inalb10(73) =  58.87124262;
    degree(74)=150-inplaneshift; inalb30(74) = 88.02522176; inalb10(74) = 62.34806243;
    degree(75)=152-inplaneshift; inalb30(75) = 90.00947713; inalb10(75) = 65.99553343;
    degree(76)=154-inplaneshift; inalb30(76) = 91.81583073; inalb10(76) = 69.78659387;
    degree(77)=156-inplaneshift; inalb30(77) = 93.43200773; inalb10(77) = 73.67829023;
    degree(78)=158-inplaneshift; inalb30(78) = 94.84769744; inalb10(78) = 77.60256289;
    degree(79)=160-inplaneshift; inalb30(79) = 96.06283665; inalb10(79) = 81.47487908;
    degree(80)=162-inplaneshift; inalb30(80) = 97.08156306; inalb10(80) = 85.19069161;
    degree(81)=164-inplaneshift; inalb30(81) = 97.91250776; inalb10(81) = 88.63951588;
    degree(82)=166-inplaneshift; inalb30(82) = 98.56810366; inalb10(82) = 91.7047714;
    degree(83)=168-inplaneshift; inalb30(83) = 99.06798016; inalb10(83) = 94.30257725;
    degree(84)=170-inplaneshift; inalb30(84) = 99.4327543;  inalb10(84) = 96.3772595;
    degree(85)=172-inplaneshift; inalb30(85) = 99.68476575; inalb10(85) = 97.9207842;
    degree(86)=174-inplaneshift; inalb30(86) = 99.84760619; inalb10(86) = 98.97336342;
    degree(87)=176-inplaneshift; inalb30(87) = 99.94200487; inalb10(87) = 99.60615763;
    degree(88)=178-inplaneshift; inalb30(88) = 99.98731853; inalb10(88) = 99.91611347;
    degree(89)=180-inplaneshift; inalb30(89) = 99.99952893; inalb10(89) = 99.99975722;

    Spline alb30(inalb30, degree);
    Spline alb10(inalb10, degree);
 
    Real outplaneshift = 0;
    Real random = 100.0 * MathLib::ran1(Injection::randSeed);
    Real newangle30 = alb30(random);
    Real newangle10 = alb10(random);
  
    Real newangle = (1-frac) * newangle10 + frac * newangle30;
    
    double theta = 180 - newangle; //New angle referenced from 0 degrees.
    
    theta *= Consts::twoPi/360.0;
    pxlocal = cos(theta);
    pylocal = sin(theta);
    pzlocal = pzlocal;
    
    // ** Part 2 - Out of plane Scattering ** //
  
    Vector(Real) outdegree, outalb10, outalb30;
    outdegree.resize(46); outalb10.resize(46), outalb30.resize(46);

    //Integrated out of plane albedo for positive scatter angle; negative
    //scatter angle dist. is symmetric, so will just randomly choose sign. 
    
    outdegree(1)=90-outplaneshift; outalb30(1) = 50.00; outalb10(1) = 50.00;
    outdegree(2)=88-outplaneshift; outalb30(2) = 49.999; outalb10(2) = 49.999;
    outdegree(3)=86-outplaneshift; outalb30(3) = 49.99; outalb10(3) = 49.99;
    outdegree(4)=84-outplaneshift; outalb30(4) = 49.98; outalb10(4) = 49.98;
    outdegree(5)=82-outplaneshift; outalb30(5) = 49.95; outalb10(5) = 49.96;
    outdegree(6)=80-outplaneshift; outalb30(6) = 49.91; outalb10(6) = 49.93;
    outdegree(7)=78-outplaneshift; outalb30(7) = 49.86; outalb10(7) = 49.89;
    outdegree(8)=76-outplaneshift; outalb30(8) = 49.78; outalb10(8) = 49.83;
    outdegree(9)=74-outplaneshift; outalb30(9) = 49.67; outalb10(9) = 49.75;
    outdegree(10)=72-outplaneshift; outalb30(10) = 49.53; outalb10(10) = 49.65;
    outdegree(11)=70-outplaneshift; outalb30(11) = 49.35; outalb10(11) = 49.52;
    outdegree(12)=68-outplaneshift; outalb30(12) = 49.14; outalb10(12) = 49.37;
    outdegree(13)=66-outplaneshift; outalb30(13) = 48.88; outalb10(13) = 49.18;
    outdegree(14)=64-outplaneshift; outalb30(14) = 48.57; outalb10(14) = 48.96;
    outdegree(15)=62-outplaneshift; outalb30(15) = 48.20; outalb10(15) = 48.70;
    outdegree(16)=60-outplaneshift; outalb30(16) = 47.78; outalb10(16) = 48.40;
    outdegree(17)=58-outplaneshift; outalb30(17) = 47.288; outalb10(17) = 48.051;
    outdegree(18)=56-outplaneshift; outalb30(18) = 46.732; outalb10(18) = 47.658;
    outdegree(19)=54-outplaneshift; outalb30(19) = 46.105; outalb10(19) = 47.216;
    outdegree(20)=52-outplaneshift; outalb30(20) = 45.403; outalb10(20) = 46.719;
    outdegree(21)=50-outplaneshift; outalb30(21) = 44.623; outalb10(21) = 46.166;
    outdegree(22)=48-outplaneshift; outalb30(22) = 43.765; outalb10(22) = 45.553;
    outdegree(23)=46-outplaneshift; outalb30(23) = 42.824; outalb10(23) = 44.873;
    outdegree(24)=44-outplaneshift; outalb30(24) = 41.801; outalb10(24) = 44.123;
    outdegree(25)=42-outplaneshift; outalb30(25) = 40.693; outalb10(25) = 43.301;
    outdegree(26)=40-outplaneshift; outalb30(26) = 39.499; outalb10(26) = 42.400;
    outdegree(27)=38-outplaneshift; outalb30(27) = 38.218; outalb10(27) = 41.415;
    outdegree(28)=36-outplaneshift; outalb30(28) = 36.850; outalb10(28) = 40.338;
    outdegree(29)=34-outplaneshift; outalb30(29) = 35.395; outalb10(29) = 39.167;
    outdegree(30)=32-outplaneshift; outalb30(30) = 33.855; outalb10(30) = 37.891;
    outdegree(31)=30-outplaneshift; outalb30(31) = 32.230; outalb10(31) = 36.504;
    outdegree(32)=28-outplaneshift; outalb30(32) = 30.521; outalb10(32) = 35.001;
    outdegree(33)=26-outplaneshift; outalb30(33) = 28.729; outalb10(33) = 33.373;
    outdegree(34)=24-outplaneshift; outalb30(34) = 26.857; outalb10(34) = 31.612;
    outdegree(35)=22-outplaneshift; outalb30(35) = 24.913; outalb10(35) = 29.712;
    outdegree(36)=20-outplaneshift; outalb30(36) = 22.897; outalb10(36) = 27.669;
    outdegree(37)=18-outplaneshift; outalb30(37) = 20.811; outalb10(37) = 25.477;
    outdegree(38)=16-outplaneshift; outalb30(38) = 18.664; outalb10(38) = 23.138;
    outdegree(39)=14-outplaneshift; outalb30(39) = 16.461; outalb10(39) = 20.649;
    outdegree(40)=12-outplaneshift; outalb30(40) = 14.207; outalb10(40) = 18.017;
    outdegree(41)=10-outplaneshift; outalb30(41) = 11.911; outalb10(41) = 15.252;
    outdegree(42)=8-outplaneshift; outalb30(42) = 9.576; outalb10(42) = 12.363;
    outdegree(43)=6-outplaneshift; outalb30(43) = 7.209; outalb10(43) = 9.371;
    outdegree(44)=4-outplaneshift; outalb30(44) = 4.816; outalb10(44) = 6.294;
    outdegree(45)=2-outplaneshift; outalb30(45) = 2.411; outalb10(45) = 3.163;
    outdegree(46)=0-outplaneshift; outalb30(46) = 0.0; outalb10(46) = 0;    
   
    Spline outofplane30(outalb30,outdegree);
    Spline outofplane10(outalb10,outdegree);
 
    random = 50.0 * MathLib::ran1(Injection::randSeed);
     
    Real outplaneangle30 = outofplane30(random);
    Real outplaneangle10 = outofplane10(random);
    
    newangle = (1-frac) * outplaneangle10 + frac * outplaneangle30;

    //Roll the sign of the angle
    random = MathLib::ran1(Injection::randSeed);
    double sign = 1;
    if(random < 0.5) sign *= -1;
    theta = newangle * sign * Consts::twoPi/360.0;
    
    double pxlocalfinal = pxlocal * cos(theta) + pzlocal * sin(theta);
    double pzlocalfinal = pzlocal * cos(theta) - pxlocal * sin(theta);
    double pylocalfinal = pylocal;

    //Translate everything back to the original frame.
    pxj = pxlocalfinal * cosphi - pzlocalfinal * sinphi;
    pzj = pzlocalfinal * cosphi + pxlocalfinal * sinphi;
    pyj = pylocalfinal;

    //Inelastic component, lose energy
    Vector(Real) enfrac, enalb30, enalb10;
    enfrac.resize(62); enalb30.resize(62), enalb10.resize(62);

    enfrac(1)=0.0; enalb30(1)=0.0; enalb10(1)=0.0;
    enfrac(2)=0.002058716; enalb10(2)=0.04284709; enalb30(2)=0.029424911;
    enfrac(3)=0.002309908; enalb10(3)=0.089292401; enalb30(3)=0.061100023;
    enfrac(4)=0.002591743; enalb10(4)=0.13817458; enalb30(4)=0.094372484;
    enfrac(5)=0.002908073; enalb10(5)=0.189894252; enalb30(5)=0.129680955;
    enfrac(6)=0.003262936; enalb10(6)=0.245911917; enalb30(6)=0.167842683;
    enfrac(7)=0.003661101; enalb10(7)=0.305541897; enalb30(7)=0.208501209;
    enfrac(8)=0.004107706; enalb10(8)=0.368739443; enalb30(8)=0.251451036;
    enfrac(9)=0.004608991; enalb10(9)=0.437694777; enalb30(9)=0.298447588;
    enfrac(10)=0.005171376; enalb10(10)=0.510804344; enalb30(10)=0.348321899;
    enfrac(11)=0.005802385; enalb10(11)=0.589035704; enalb30(11)=0.401605892;
    enfrac(12)=0.006510275; enalb10(12)=0.674003686; enalb30(12)=0.459934066;
    enfrac(13)=0.007304771; enalb10(13)=0.764337288; enalb30(13)=0.521922472;
    enfrac(14)=0.008195963; enalb10(14)=0.861871208; enalb30(14)=0.588800934;
    enfrac(15)=0.009196147; enalb10(15)=0.968424993; enalb30(15)=0.661601684;
    enfrac(16)=0.010318165; enalb10(16)=1.082470492; enalb30(16)=0.739804654;
    enfrac(17)=0.011577248; enalb10(17)=1.205456929; enalb30(17)=0.824486338;
    enfrac(18)=0.012989908; enalb10(18)=1.340001224; enalb30(18)=0.916873397;
    enfrac(19)=0.014574862; enalb10(19)=1.483429728; enalb30(19)=1.01674848;
    enfrac(20)=0.016353211; enalb10(20)=1.639029889; enalb30(20)=1.124130555;
    enfrac(21)=0.018348624; enalb10(21)=1.809041609; enalb30(21)=1.242426142;
    enfrac(22)=0.020587156; enalb10(22)=1.990497729; enalb30(22)=1.369153462;
    enfrac(23)=0.023099083; enalb10(23)=2.187302314; enalb30(23)=1.508358448;
    enfrac(24)=0.025917431; enalb10(24)=2.401572999; enalb30(24)=1.661446391;
    enfrac(25)=0.029080734; enalb10(25)=2.629867304; enalb30(25)=1.826699802;
    enfrac(26)=0.032629358; enalb10(26)=2.876623104; enalb30(26)=2.00892891;
    enfrac(27)=0.036611009; enalb10(27)=3.143926326; enalb30(27)=2.211039929;
    enfrac(28)=0.041077064; enalb10(28)=3.427002595; enalb30(28)=2.431300353;
    enfrac(29)=0.046089908; enalb10(29)=3.731369749; enalb30(29)=2.676339068;
    enfrac(30)=0.051713761; enalb10(30)=4.057942142; enalb30(30)=2.951242933;
    enfrac(31)=0.058023853; enalb10(31)=4.401358734; enalb30(31)=3.253502504;
    enfrac(32)=0.065102752; enalb10(32)=4.767385769; enalb30(32)=3.59452449;
    enfrac(33)=0.073047706; enalb10(33)=5.156400264; enalb30(33)=3.983070998;
    enfrac(34)=0.081959633; enalb10(34)=5.561915416; enalb30(34)=4.417983338;
    enfrac(35)=0.091961468; enalb10(35)=5.993800609; enalb30(35)=4.917615624;
    enfrac(36)=0.103181651; enalb10(36)=6.454679107; enalb30(36)=5.497961114;
    enfrac(37)=0.115772477; enalb10(37)=6.938539779; enalb30(37)=6.160181663;
    enfrac(38)=0.129899083; enalb10(38)=7.458934095; enalb30(38)=6.939259314;
    enfrac(39)=0.145748624; enalb10(39)=8.025865333; enalb30(39)=7.861387132;
    enfrac(40)=0.16353211; enalb10(40)=8.636962163; enalb30(40)=8.939697214;
    enfrac(41)=0.183486239; enalb10(41)=9.319191864; enalb30(41)=10.23385505;
    enfrac(42)=0.198917431; enalb10(42)=9.846254445; enalb30(42)=11.2925559;
    enfrac(43)=0.215651376; enalb10(43)=10.42320834; enalb30(43)=12.50122666;
    enfrac(44)=0.233779817; enalb10(44)=11.06007269; enalb30(44)=13.88426229;
    enfrac(45)=0.253431193; enalb10(45)=11.76876229; enalb30(45)=15.46504211;
    enfrac(46)=0.274752294; enalb10(46)=12.56181734; enalb30(46)=17.28537701;
    enfrac(47)=0.297853211; enalb10(47)=13.45731176; enalb30(47)=19.37856554;
    enfrac(48)=0.322899083; enalb10(48)=14.47340226; enalb30(48)=21.78439919;
    enfrac(49)=0.350055046; enalb10(49)=15.63489091; enalb30(49)=24.55715703;
    enfrac(50)=0.379486239; enalb10(50)=16.9673814; enalb30(50)=27.74730852;
    enfrac(51)=0.411412844; enalb10(51)=18.50802573; enalb30(51)=31.41238118;
    enfrac(52)=0.446; enalb10(52)=20.30111107; enalb30(52)=35.62753584;
    enfrac(53)=0.483504587; enalb10(53)=22.40650382; enalb30(53)=40.46617867;
    enfrac(54)=0.524165138; enalb10(54)=24.90688343; enalb30(54)=46.01849158;
    enfrac(55)=0.568238532; enalb10(55)=27.93535097; enalb30(55)=52.37457743;
    enfrac(56)=0.616036697; enalb10(56)=31.67740228; enalb30(56)=59.60928486;
    enfrac(57)=0.667834862; enalb10(57)=36.41935879; enalb30(57)=67.72866023;
    enfrac(58)=0.724; enalb10(58)=42.64598671; enalb30(58)=76.60027602;
    enfrac(59)=0.784880734; enalb10(59)=51.27844053; enalb30(59)=85.7034325;
    enfrac(60)=0.850880734; enalb10(60)=64.19394729; enalb30(60)=93.75582349;
    enfrac(61)=0.922422018; enalb10(61)=84.90767869; enalb30(61)=98.70914989;
    enfrac(62)=1; enalb10(62)=100; enalb30(62)=100;

    Spline enspline30(enalb30,enfrac);
    Spline enspline10(enalb10,enfrac);
 
    random = 100*MathLib::ran1(Injection::randSeed);
     
    Real energy30 = enspline30(random);
    Real energy10 = enspline10(random);

    Real energyfrac = (1-frac) * energy10 + frac * energy30;

    return energyfrac; //Particle has been reflected.
    }
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FieldTracker::ctor
//
// DESCRIPTION
//   Constructor for the FieldTracker Module
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::ctor()
{
// set some initial values

  nFTNodes = 0;
  zsymmetry = 0;
  doRefPath = 0;
  aperture = 0;
  useCatcher = 0;
  getPath = 0;
  BScale = 1.0;
  xMin = -1.0e6;
  xMax =  1.0e6;
  yMin = -1.0e6;
  yMax =  1.0e6;
  xFoilMin = 0.0;
  xFoilMax = 0.0;
  yFoilMin = 0.0;
  yFoilMax = 0.0;
  zFoilMin = 0.0;
  zFoilMax = 0.0;
  foilAngle = 0.0;
  RMaglich = 1.0;
  B0Maglich = 1.0;
  kMaglich = 0.0;
  sMax = 10.0;
  rMax = 1.0e+10;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::nullOut
//
// DESCRIPTION
//   Zeroes the length of a TransMap Node
//   and grabs original length and lattice functions for substitute
//   node
//
// PARAMETERS
//   n    (str) --> _name   : Name for this node
//   order (int)--> _oindex : node order index
//   transmapNumber         : number of matrix to be nulled
//   bx                     : betax
//   by                     : betay
//   ax                     : alphax
//   ay                     : alphay
//   ex                     : etax
//   epx                    : etapx
//   length                 : length
//   et (str)   --> _et     : The type of element added
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::nullOut(const String &n, const Integer &order,
                           Integer &transmapNumber,
                           Real &bx, Real &by,
                           Real &ax, Real &ay,
                           Real &ex, Real &epx,
                           Real &length, const String &et)
{
  //  Set the nearest transfer map to negative length

  Integer i;
  MapBase *tm;

  transmapNumber = -1;

  for(i = 1; i <= nTransMaps; i++)
  {
    tm = MapBase::safeCast(tMaps(i - 1));
    if(tm->_oindex == order) transmapNumber = i;
  }

  if(transmapNumber < 0) except(badNode);
  if(transmapNumber > nTransMaps) except(badNode);

  tm = MapBase::safeCast(tMaps(transmapNumber - 1));

  bx   = tm -> _betaX;
  by   = tm -> _betaY;
  ax   = tm -> _alphaX;
  ay   = tm -> _alphaY;
  ex   = tm -> _etaX;
  epx  = tm -> _etaPX;
  length = tm -> _length;

  tm -> _length = -Consts::tiny;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::replaceFT3D
//
// DESCRIPTION
//   Replaces an existing transMap with an FT3D node
//
// PARAMETERS
//   n      (str)  --> _name    : Name for this node
//   order  (int)  --> _oindex  : node order index
//   et  (string)  --> _et      : Element Type
//   sub    (sub)  --> _sub     : Subroutine to acquire fields
//   zi    (real)  --> _zi      : Initial tracking position [m]
//   zf    (real)  --> _zf      : Final tracking position [m]
//   ds    (real)  --> _ds      : Integration step size
//   niters (int)  --> _niters  : Number predictor-corrector iterations
//   resid (real)  --> _resid   : Predictor-corrector residual
//   xrefi (real)  --> _xrefi   : Initial reference particle x [mm]
//   yrefi (real)  --> _yrefi   : Initial reference particle y [mm]
//   eulerai(real) --> _eulerai : Initial reference Euler angle alpha [mr]
//   eulerbi(real) --> _eulerbi : Initial reference Euler angle beta [mr]
//   eulergi(real) --> _eulergi : Initial reference Euler angle gamma [mr]
//   apflag(int)   --> _apflag  : Flag for use an aperture or not.
//                                0 for always absorb on apertures
//                                1 for reflect at -y aperture
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::replaceFT3D(const String &n, const Integer &order,
                               const String &et,
                               const Subroutine sub,
                               const Real &zi, const Real &zf,
                               const Real &ds, const Integer &niters,
                               const Real &resid,
                               const Real &xrefi, const Real &yrefi,
                               const Real &eulerai, const Real &eulerbi,
                               const Real &eulergi,
			       const Integer &apflag)
{
  Real bx, by, ax, ay, ex, epx, l;

  //  Set the nearest transmap length negative

  Integer i;
  Integer transmapNumber;

  FieldTracker::nullOut(n, order, transmapNumber,
                        bx, by, ax, ay, ex, epx,
                        l, et);

  // Add the FT3D node to the empty space

  if(nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nFTNodes == FTPointers.size())
    FTPointers.resize(nFTNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new FT3D(n, order,
                               bx, by, ax, ay, ex, epx,
                               l, et,
                               sub, zi, zf, ds,
                               niters, resid,
                               xrefi, yrefi,
                               eulerai, eulerbi, eulergi,
			       apflag);
  nTransMaps++;
  nFTNodes++;

  for (i = nTransMaps - 1; i > transmapNumber; i--)
  {
    tMaps(i) = tMaps(i - 1);
  }
  tMaps(transmapNumber) = nodes(nNodes - 1);
  FTPointers(nFTNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::addFT3D
//
// DESCRIPTION
//   Adds a FieldTracker node
//
// PARAMETERS
//   n      (str) --> _name   : Name for this node
//   order  (int) --> _oindex : node order index
//   bx    (real) --> _betaX  : The horizontal beta value at the
//                              beginning of the Node [m]
//   by    (real) --> _betaY  : The vertical beta value at the
//                              beginning of the Node [m]
//   ax    (real) --> _alphaX : The horizontal alpha value at the
//                              beginning of the Node
//   ay    (real) --> _alphaY : The horizontal alpha  value at the
//                              beginning of the Node
//   ex    (real) --> _etaX   : The horizontal dispersion [m]
//   epx   (real) --> _etaPX  : The horizontal dispersion prime
//   l     (real) --> _length : The length of the node
//   et  (string) --> _et     : Element Type
//   sub    (sub)  --> _sub     : Subroutine to acquire fields
//   zi    (real)  --> _zi      : Initial tracking position [m]
//   zf    (real)  --> _zf      : Final tracking position [m]
//   ds    (real)  --> _ds      : Integration step size
//   niters (int)  --> _niters  : Number predictor-corrector iterations
//   resid (real)  --> _resid   : Predictor-corrector residual
//   xrefi (real)  --> _xrefi   : Initial reference particle x [mm]
//   yrefi (real)  --> _yrefi   : Initial reference particle y [mm]
//   eulerai(real) --> _eulerai : Initial reference Euler angle alpha [mr]
//   eulerbi(real) --> _eulerbi : Initial reference Euler angle beta [mr]
//   eulergi(real) --> _eulergi : Initial reference Euler angle gamma [mr]
//   apflag(int)   --> _apflag  : Flag for use an aperture or not.
//                                0 for always absorb, 1 for reflect at -y
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::addFT3D(const String &n, const Integer &order,
                           const Real &bx, const Real &by,
                           const Real &ax, const Real &ay,
                           const Real &ex, const Real &epx,
                           const Real &l, const String &et,
                           const Subroutine sub,
                           const Real &zi, const Real &zf,
                           const Real &ds, const Integer &niters,
                           const Real &resid,
                           const Real &xrefi, const Real &yrefi,
                           const Real &eulerai, const Real &eulerbi,
                           const Real &eulergi,
			   const Integer &apflag)
{
  if (nNodes == nodes.size())
    nodes.resize(nNodes + Node_Alloc_Chunk);
  if(nTransMaps == tMaps.size())
    tMaps.resize(nTransMaps + Node_Alloc_Chunk);
  if(nFTNodes == FTPointers.size())
    FTPointers.resize(nFTNodes + Node_Alloc_Chunk);

  nNodes++;

  nodes(nNodes - 1) = new FT3D(n, order,
                               bx, by, ax, ay, ex, epx,
                               l, et,
                               sub, zi, zf, ds,
                               niters, resid,
                               xrefi, yrefi,
                               eulerai, eulerbi, eulergi,
                               apflag);
  nTransMaps++;
  nFTNodes++;
  tMaps(nTransMaps - 1) = nodes(nNodes - 1);

  FTPointers(nFTNodes - 1) = nodes(nNodes - 1);
  nodesInitialized = 0;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//   FieldTracker::showFT
//
// DESCRIPTION
//   Prints out the settings of the active FieldTracker Nodes
//
// PARAMETERS
//   sout - The output stream
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::showFT(Ostream &sout)
{
  FTBase *tlp;

  if (nFTNodes == 0)
  {
    sout << "\n No FieldTracker Nodes are present \n\n";
      return;
  }

  sout << "\n\n FieldTracker Nodes present in ring:\n\n";

  for (Integer i = 1; i <= nFTNodes; i++)
  {
    tlp = FTBase::safeCast(FTPointers(i-1));
    tlp->_showFT(sout);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::ParseTest3D
//
// DESCRIPTION
//   Routine to parse 3D test field file.
//
// PARAMETERS
//   fileName -> File containing input field information.
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::ParseTest3D(const String &fileName)
{
  IFstream fio(fileName, std::ios::in);
  if(fio.good() != 1) except(badBFile);

  String Dummy;
  fio >> Dummy >> phi3Dx
      >> Dummy >> phi3Dy
      >> Dummy >> phi3Dz
      >> Dummy >> phi3Dxx
      >> Dummy >> phi3Dxy
      >> Dummy >> phi3Dxz
      >> Dummy >> phi3Dyy
      >> Dummy >> phi3Dyz
      >> Dummy >> phi3Dxxx
      >> Dummy >> phi3Dxxy
      >> Dummy >> phi3Dxxz
      >> Dummy >> phi3Dxyy
      >> Dummy >> phi3Dxyz
      >> Dummy >> phi3Dyyy
      >> Dummy >> phi3Dyyz;

  phi3Dzz  = -phi3Dxx  - phi3Dyy;
  phi3Dxzz = -phi3Dxxx - phi3Dxyy;
  phi3Dyzz = -phi3Dxxy - phi3Dyyy;
  phi3Dzzz = -phi3Dxxz - phi3Dyyz;

  fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::ParseMults3D
//
// DESCRIPTION
//   Routine to parse 3D multipole field file.
//
// PARAMETERS
//   fileName -> File containing input field information.
//   zmin     -> Minimum z for data.
//   zmax     -> Maximum z for data.
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::ParseMults3D(const String &fileName,
                                const Real &zmin, const Real &zmax)
{
  IFstream fio(fileName, std::ios::in);
  if(fio.good() != 1) except(badBFile);

  fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::ParseGrid3D
//
// DESCRIPTION
//   Routine to parse 3D grid field file.
//
// PARAMETERS
//   fileName -> File containing input field information.
//   xmin     -> Minimum x for data.
//   xmax     -> Maximum x for data.
//   ymin     -> Minimum y for data.
//   ymax     -> Maximum y for data.
//   zmin     -> Minimum z for data.
//   zmax     -> Maximum z for data.
//   skipX    -> Determines number of x gridpoints used.
//   skipY    -> Determines number of y gridpoints used.
//   skipZ    -> Determines number of z gridpoints used.
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::ParseGrid3D(const String &fileName,
                               const Real &xmin, const Real &xmax,
                               const Real &ymin, const Real &ymax,
                               const Real &zmin, const Real &zmax,
                               const Integer &skipX,
                               const Integer &skipY,
                               const Integer &skipZ)
{
  Integer nXTab, nYTab, nZTab;
  Integer iDummy, i, j, k;
  Integer xindex, yindex, zindex;
  String Dummy;
  Real x, y, z, Bx, By, Bz;

  IFstream fio(fileName, std::ios::in);
  if(fio.good() != 1) except(badBFile);

  fio >> nXTab >> nYTab >> nZTab >> iDummy;
  fio >> iDummy >> Dummy >> Dummy;
  fio >> iDummy >> Dummy >> Dummy;
  fio >> iDummy >> Dummy >> Dummy;
  fio >> iDummy >> Dummy >> Dummy;
  fio >> iDummy >> Dummy >> Dummy;
  fio >> iDummy >> Dummy >> Dummy;
  fio >> iDummy;

  nXGrid = 0;
  for(i = 0; i < nXTab; i++)
  {
    nYGrid = 0;
    for(j = 0; j < nYTab; j++)
    {
      nZGrid = 0;
      for(k = 0; k < nZTab; k++)
      {
        fio >> x >> y >> z >> Bx >> By >> Bz;
        if((z >= zmin) && (z <= zmax))
        {
          if((k == (k / skipZ) * skipZ) || (k == nZTab - 1))
          {
            nZGrid++;
          }
        }
      }
      if((y >= ymin) && (y <= ymax))
      {
        if((j == (j / skipY) * skipY) || (j == nYTab - 1))
        {
          nYGrid++;
        }
      }
    }
    if((x >= xmin) && (x <= xmax))
    {
      if((i == (i / skipX) * skipX) || (i == nXTab - 1))
      {
        nXGrid++;
      }
    }
  }
  fio.close();

  XGrid.resize(nXGrid);
  YGrid.resize(nYGrid);
  ZGrid.resize(nZGrid);
  BXGrid.resize(nXGrid, nYGrid, nZGrid);
  BYGrid.resize(nXGrid, nYGrid, nZGrid);
  BZGrid.resize(nXGrid, nYGrid, nZGrid);
  BMagGrid.resize(nXGrid, nYGrid, nZGrid);

  IFstream fio2(fileName, std::ios::in);
  if(fio2.good() != 1) except(badBFile);

  fio2 >> nXTab >> nYTab >> nZTab >> iDummy;
  fio2 >> iDummy >> Dummy >> Dummy;
  fio2 >> iDummy >> Dummy >> Dummy;
  fio2 >> iDummy >> Dummy >> Dummy;
  fio2 >> iDummy >> Dummy >> Dummy;
  fio2 >> iDummy >> Dummy >> Dummy;
  fio2 >> iDummy >> Dummy >> Dummy;
  fio2 >> iDummy;

  xindex = 1;
  for(i = 0; i < nXTab; i++)
  {
    yindex = 1;
    for(j = 0; j < nYTab; j++)
    {
      zindex = 1;
      for(k = 0; k < nZTab; k++)
      {
        fio2 >> x >> y >> z >> Bx >> By >> Bz;

        if((z >= zmin) && (z <= zmax))
        {
          if((k == (k / skipZ) * skipZ) || (k == nZTab - 1))
          {
            if((y >= ymin) && (y <= ymax))
            {
              if((j == (j / skipY) * skipY) || (j == nYTab - 1))
              {
                if((x >= xmin) && (x <= xmax))
                {
                  if((i == (i / skipX) * skipX) || (i == nXTab - 1))
                  {
                    XGrid(xindex) = x / 100.0;
                    YGrid(yindex) = y / 100.0;
                    ZGrid(zindex) = z / 100.0;
                    BXGrid(xindex, yindex, zindex) = Bx / 10000.0;
                    BYGrid(xindex, yindex, zindex) = By / 10000.0;
                    BZGrid(xindex, yindex, zindex) = Bz / 10000.0;
		    BMagGrid(xindex,yindex,zindex) = sqrt(Bx*Bx + By*By + Bz*Bz)/10000.0;
                  }
                }
              }
            }
            zindex++;
          }
        }
      }
      if((y >= ymin) && (y <= ymax))
      {
        if((j == (j / skipY) * skipY) || (j == nYTab - 1))
        {
          yindex++;
        }
      }
    }
    if((x >= xmin) && (x <= xmax))
    {
      if((i == (i / skipX) * skipX) || (i == nXTab - 1))
      {
        xindex++;
      }
    }
  }

  fio2.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::BTest3D
//
// DESCRIPTION
//   Routine to test various aspects of 3D Tracker.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::BTest3D()
{
  Real xF, yF, zF;

  xF = xField3D;
  yF = yField3D;
  zF = zField3D;

  BxField3D = phi3Dx +
              phi3Dxx * xF +
              phi3Dxy * yF +
              phi3Dxz * zF +
              phi3Dxxx * xF * xF / 2.0 +
              phi3Dxxy * xF * yF +
              phi3Dxxz * xF * zF +
              phi3Dxyy * yF * yF / 2.0 +
              phi3Dxyz * yF * zF +
              phi3Dxzz * zF * zF / 2.0;

  ByField3D = phi3Dy +
              phi3Dxy * xF +
              phi3Dyy * yF +
              phi3Dyz * zF +
              phi3Dxxy * xF * xF / 2.0 +
              phi3Dxyy * xF * yF +
              phi3Dxyz * xF * zF +
              phi3Dyyy * yF * yF / 2.0 +
              phi3Dyyz * yF * zF +
              phi3Dyzz * zF * zF / 2.0;

  BzField3D = phi3Dz +
              phi3Dxz * xF +
              phi3Dyz * yF +
              phi3Dzz * zF +
              phi3Dxxz * xF * xF / 2.0 +
              phi3Dxyz * xF * yF +
              phi3Dxzz * xF * zF +
              phi3Dyyz * yF * yF / 2.0 +
              phi3Dyzz * yF * zF +
              phi3Dzzz * zF * zF / 2.0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::BMults3D
//
// DESCRIPTION
//   Routine to use 3D multipole expansion in 3D Tracker.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::BMults3D()
{
  Real xF, yF, zF;

  xF = xField3D;
  yF = yField3D;
  zF = zField3D;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::BGrid3D
//
// DESCRIPTION
//   Routine to interpolate B from 3D grid for 3D Tracker.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::BGrid3D()
{
  Real xF, yF, zF;

  xF = xField3D;
  yF = yField3D;
  zF = zField3D;
 
  if((zsymmetry != 0) && (zF < ZGrid(1)))
  {
    zF = ZGrid(1) - zF + ZGrid(1);
  }

  Integer ig, jg, kg, igp, jgp, kgp;
  Real dx, dy, dz, xfrac, yfrac, zfrac, xfracc, yfracc, zfracc;

  dx = (XGrid(nXGrid) - XGrid(1)) / Real(nXGrid - 1);
  dy = (YGrid(nYGrid) - YGrid(1)) / Real(nYGrid - 1);
  dz = (ZGrid(nZGrid) - ZGrid(1)) / Real(nZGrid - 1);
 
  ig = Integer((xF - XGrid(1)) / dx) + 1;
  jg = Integer((yF - YGrid(1)) / dy) + 1;
  kg = Integer((zF - ZGrid(1)) / dz) + 1;

  if(ig < 1) ig = 1;
  if(jg < 1) jg = 1;
  if(kg < 1) kg = 1;
  if(ig > nXGrid - 1) ig = nXGrid - 1;
  if(jg > nYGrid - 1) jg = nYGrid - 1;
  if(kg > nZGrid - 1) kg = nZGrid - 1;

  igp = ig + 1;
  jgp = jg + 1;
  kgp = kg + 1;

  xfrac = (xF - XGrid(ig)) / dx;
  yfrac = (yF - YGrid(jg)) / dy;
  zfrac = (zF - ZGrid(kg)) / dz;

  xfracc = 1.0 - xfrac;
  yfracc = 1.0 - yfrac;
  zfracc = 1.0 - zfrac;

  BxField3D =   BXGrid(ig , jg , kg ) * xfracc * yfracc * zfracc +
              + BXGrid(ig , jg , kgp) * xfracc * yfracc * zfrac  +
              + BXGrid(ig , jgp, kg ) * xfracc * yfrac  * zfracc +
              + BXGrid(ig , jgp, kgp) * xfracc * yfrac  * zfrac  +
              + BXGrid(igp, jg , kg ) * xfrac  * yfracc * zfracc +
              + BXGrid(igp, jg , kgp) * xfrac  * yfracc * zfrac  +
              + BXGrid(igp, jgp, kg ) * xfrac  * yfrac  * zfracc +
              + BXGrid(igp, jgp, kgp) * xfrac  * yfrac  * zfrac;

  ByField3D =   BYGrid(ig , jg , kg ) * xfracc * yfracc * zfracc +
              + BYGrid(ig , jg , kgp) * xfracc * yfracc * zfrac  +
              + BYGrid(ig , jgp, kg ) * xfracc * yfrac  * zfracc +
              + BYGrid(ig , jgp, kgp) * xfracc * yfrac  * zfrac  +
              + BYGrid(igp, jg , kg ) * xfrac  * yfracc * zfracc +
              + BYGrid(igp, jg , kgp) * xfrac  * yfracc * zfrac  +
              + BYGrid(igp, jgp, kg ) * xfrac  * yfrac  * zfracc +
              + BYGrid(igp, jgp, kgp) * xfrac  * yfrac  * zfrac;

  BzField3D =   BZGrid(ig , jg , kg ) * xfracc * yfracc * zfracc +
              + BZGrid(ig , jg , kgp) * xfracc * yfracc * zfrac  +
              + BZGrid(ig , jgp, kg ) * xfracc * yfrac  * zfracc +
              + BZGrid(ig , jgp, kgp) * xfracc * yfrac  * zfrac  +
              + BZGrid(igp, jg , kg ) * xfrac  * yfracc * zfracc +
              + BZGrid(igp, jg , kgp) * xfrac  * yfracc * zfrac  +
              + BZGrid(igp, jgp, kg ) * xfrac  * yfrac  * zfracc +
              + BZGrid(igp, jgp, kgp) * xfrac  * yfrac  * zfrac;
  if((zsymmetry != 0) && (zF < ZGrid(1)))
  {
    BzField3D = -BzField3D;
  }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::BGradGrid3D
//
// DESCRIPTION
//   Routine to interpolate B Grad from 3D grid for 3D Tracker.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::BGradGrid3D()
{
  Real xF, yF, zF;

  xF = xField3D;
  yF = yField3D;
  zF = zField3D;
  if((zsymmetry != 0) && (zF < ZGrid(1)))
  {
    zF = ZGrid(1) - zF + ZGrid(1);
  }

  Integer ig, jg, kg, igp, jgp, kgp;
  Real dx, dy, dz, xfrac, yfrac, zfrac, xfracc, yfracc, zfracc;
 
  dx = (XGrid(nXGrid) - XGrid(1)) / Real(nXGrid - 1);
  dy = (YGrid(nYGrid) - YGrid(1)) / Real(nYGrid - 1);
  dz = (ZGrid(nZGrid) - ZGrid(1)) / Real(nZGrid - 1);

  ig = Integer((xF - XGrid(1)) / dx) + 1;
  jg = Integer((yF - YGrid(1)) / dy) + 1;
  kg = Integer((zF - ZGrid(1)) / dz) + 1;

  if(ig < 1) ig = 1;
  if(jg < 1) jg = 1;
  if(kg < 1) kg = 1;
  if(ig > nXGrid - 1) ig = nXGrid - 1;
  if(jg > nYGrid - 1) jg = nYGrid - 1;
  if(kg > nZGrid - 1) kg = nZGrid - 1;

  igp = ig + 1;
  jgp = jg + 1;
  kgp = kg + 1;
  
  Real gradBx_ig_jg_kg = (BMagGrid(ig+1,jg,kg) - BMagGrid(ig-1,jg,kg))/dx; 
  Real gradBy_ig_jg_kg = (BMagGrid(ig,jg+1,kg) - BMagGrid(ig,jg-1,kg))/dy;
  Real gradBz_ig_jg_kg = (BMagGrid(ig,jg,kg+1) - BMagGrid(ig,jg,kg-1))/dz;
  
  Real gradBx_ig_jgp_kgp = (BMagGrid(ig+1,jgp,kgp) - BMagGrid(ig-1,jgp,kgp))/dx; 
  Real gradBy_ig_jgp_kgp = (BMagGrid(ig,jgp+1,kgp) - BMagGrid(ig,jgp-1,kgp))/dy;
  Real gradBz_ig_jgp_kgp = (BMagGrid(ig,jgp,kgp+1) - BMagGrid(ig,jgp,kgp-1))/dz;

  Real gradBx_ig_jgp_kg = (BMagGrid(ig+1,jgp,kg) - BMagGrid(ig-1,jgp,kg))/dx; 
  Real gradBy_ig_jgp_kg = (BMagGrid(ig,jgp+1,kg) - BMagGrid(ig,jgp-1,kg))/dy;
  Real gradBz_ig_jgp_kg = (BMagGrid(ig,jgp,kg+1) - BMagGrid(ig,jgp,kg-1))/dz;

  Real gradBx_ig_jg_kgp = (BMagGrid(ig+1,jg,kgp) - BMagGrid(ig-1,jg,kgp))/dx; 
  Real gradBy_ig_jg_kgp = (BMagGrid(ig,jg+1,kgp) - BMagGrid(ig,jg-1,kgp))/dy;
  Real gradBz_ig_jg_kgp = (BMagGrid(ig,jg,kgp+1) - BMagGrid(ig,jg,kgp-1))/dz;

  Real gradBx_igp_jg_kg = (BMagGrid(igp+1,jg,kg) - BMagGrid(igp-1,jg,kg))/dx; 
  Real gradBy_igp_jg_kg = (BMagGrid(igp,jg+1,kg) - BMagGrid(igp,jg-1,kg))/dy;
  Real gradBz_igp_jg_kg = (BMagGrid(igp,jg,kg+1) - BMagGrid(igp,jg,kg-1))/dz;

  Real gradBx_igp_jgp_kg = (BMagGrid(igp+1,jgp,kg) - BMagGrid(igp-1,jgp,kg))/dx; 
  Real gradBy_igp_jgp_kg = (BMagGrid(igp,jgp+1,kg) - BMagGrid(igp,jgp-1,kg))/dy;
  Real gradBz_igp_jgp_kg = (BMagGrid(igp,jgp,kg+1) - BMagGrid(igp,jgp,kg-1))/dz;
 
  Real gradBx_igp_jg_kgp = (BMagGrid(igp+1,jg,kgp) - BMagGrid(igp-1,jg,kgp))/dx; 
  Real gradBy_igp_jg_kgp = (BMagGrid(igp,jg+1,kgp) - BMagGrid(igp,jg-1,kgp))/dy;
  Real gradBz_igp_jg_kgp = (BMagGrid(igp,jg,kgp+1) - BMagGrid(igp,jg,kgp-1))/dz;
  
  Real gradBx_igp_jgp_kgp = (BMagGrid(igp+1,jgp,kgp) - BMagGrid(igp-1,jgp,kgp))/dx; 
  Real gradBy_igp_jgp_kgp = (BMagGrid(igp,jgp+1,kgp) - BMagGrid(igp,jgp-1,kgp))/dy;
  Real gradBz_igp_jgp_kgp = (BMagGrid(igp,jgp,kgp+1) - BMagGrid(igp,jgp,kgp-1))/dz;


  xfrac = (xF - XGrid(ig)) / dx;
  yfrac = (yF - YGrid(jg)) / dy;
  zfrac = (zF - ZGrid(kg)) / dz;

  xfracc = 1.0 - xfrac;
  yfracc = 1.0 - yfrac;
  zfracc = 1.0 - zfrac;


  gradBx   =   gradBx_ig_jg_kg * xfracc * yfracc * zfracc +
              + gradBx_ig_jg_kgp * xfracc * yfracc * zfrac  +
              + gradBx_ig_jgp_kg * xfracc * yfrac  * zfracc +
              + gradBx_ig_jgp_kgp * xfracc * yfrac  * zfrac  +
              + gradBx_igp_jg_kg * xfrac  * yfracc * zfracc +
              + gradBx_igp_jg_kgp * xfrac  * yfracc * zfrac  +
              + gradBx_igp_jgp_kg * xfrac  * yfrac  * zfracc +
              + gradBx_igp_jgp_kgp * xfrac  * yfrac  * zfrac;

  gradBy   =   gradBy_ig_jg_kg * xfracc * yfracc * zfracc +
              + gradBy_ig_jg_kgp * xfracc * yfracc * zfrac  +
              + gradBy_ig_jgp_kg  * xfracc * yfrac  * zfracc +
              + gradBy_ig_jgp_kgp * xfracc * yfrac  * zfrac  +
              + gradBy_igp_jg_kg  * xfrac  * yfracc * zfracc +
              + gradBy_igp_jg_kgp * xfrac  * yfracc * zfrac  +
              + gradBy_igp_jgp_kg * xfrac  * yfrac  * zfracc +
              + gradBy_igp_jgp_kgp * xfrac  * yfrac  * zfrac;

  gradBz    =   gradBz_ig_jg_kg * xfracc * yfracc * zfracc +
              + gradBz_ig_jg_kgp * xfracc * yfracc * zfrac  +
              + gradBz_ig_jgp_kg  * xfracc * yfrac  * zfracc +
              + gradBz_ig_jgp_kgp * xfracc * yfrac  * zfrac  +
              + gradBz_igp_jg_kg * xfrac  * yfracc * zfracc +
              + gradBz_igp_jg_kgp * xfrac  * yfracc * zfrac  +
              + gradBz_igp_jgp_kg  * xfrac  * yfrac  * zfracc +
              + gradBz_igp_jgp_kgp * xfrac  * yfrac  * zfrac;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FieldTracker::BMaglich3D
//
// DESCRIPTION
//   Routine to use Bogdan Maglich 3D Migma-type field..
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FieldTracker::BMaglich3D()
{
  Real xF, yF, zF;

  xF = xField3D;
  yF = yField3D;
  zF = zField3D;

  Real rF = pow(xF * xF + yF * yF, 0.5);
  Real thF = atan2(yF, xF);
  Real BrField3D = -B0Maglich * 2.0 * kMaglich * rF * zF / (RMaglich * RMaglich);
  
  BxField3D = BrField3D * cos(thF);
  ByField3D = BrField3D * sin(thF);
  BzField3D = B0Maglich * (1.0 +
                        kMaglich * (2.0 * zF * zF - rF * rF)
                        / (RMaglich * RMaglich));
}


///////////////////////////////////////////////////////////////////////////
//
// LOCAL FUNCTIONS
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FTfindPhases
//
// DESCRIPTION
//   Finds the betatron X and Y phases
//
// PARAMETERS
//   tm = reference to the TransMap member
//   mp = reference to the macro-particle main herd
//   i  = index to  the herd member
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FTfindPhases(MapBase &tm, MacroPart &mp, const Integer &i)
{
    Real angle, xval, xpval, yval, ypval;

    // Find normalized coordinates on ellipse transformed to circle:

    xval = (mp._x(i) - 1000. * tm._etaX * mp._dp_p(i)) / sqrt(tm._betaX);
    xpval = (mp._xp(i) - 1000. * tm._etaPX * mp._dp_p(i)) * sqrt(tm._betaX)
             + xval * tm._alphaX;

    yval = mp._y(i) / sqrt(tm._betaY);
    ypval = mp._yp(i) * sqrt(tm._betaY) + yval * tm._alphaY;

    angle = atan2(xpval, xval);
    if(angle < 0.) angle += Consts::twoPi;
    FTxPhase = angle;

    angle = atan2(ypval, yval);
    if(angle < 0.) angle += Consts::twoPi;
    FTyPhase = angle;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FTdoTunes
//
// DESCRIPTION
//   Increments the tunes of a macroParticle herd. It is used as
//   the herd is stepped through transfer matrices.
//
// PARAMETERS
//   tm = reference to the TransMatrix1 member
//   mp = reference to the macro-particle herd
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void FTdoTunes(MapBase &tm, MacroPart &mp)
{
  Integer i;
  Real dPhase;
  Real phiCrit = Consts::pi;

  for(i = 1; i <= mp._nMacros; i++)
  {
    FTfindPhases(tm, mp, i);   // find new betatron phase

    // Horizontal Phase:

    dPhase = Particles::xPhaseOld(i) - FTxPhase;
    if(dPhase < (-Consts::pi)) dPhase += Consts::twoPi;
    //  Subtract out false 0 angle crossings from s.c. "kick-backs":
    if(dPhase > Consts::pi && Particles::xPhaseOld(i) > phiCrit)
      dPhase -= Consts::twoPi;
    Particles::xPhaseTot(i) += dPhase;
    Particles::xPhaseOld(i) = FTxPhase;

    // Vertical phase:

    dPhase = Particles::yPhaseOld(i) - FTyPhase;
    if(dPhase < (-Consts::pi)) dPhase += Consts::twoPi;
    //  Subtract out fake 0 angle crossings from s.c. "kick-backs"
    if(dPhase > Consts::pi && Particles::yPhaseOld(i) > phiCrit)
      dPhase -= Consts::twoPi;
    Particles::yPhaseTot(i) += dPhase;
    Particles::yPhaseOld(i) = FTyPhase;

    // Tune tracking length:

    Particles::lTuneTrack(i) += tm._length;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   FindScatterAngle
//
// DESCRIPTION
//   Finds the surface backscattering angle
//
// PARAMETERS
//
// RETURNS
//   The angle
//
///////////////////////////////////////////////////////////////////////////

Real FindScatterAngle()
{
  Real angle;
  Real errorlow = 10000;
  Real errorhigh = 10000;
  Real error = 10000;
  Real tol = 0.01;
  Real thetalow = -Consts::pi;
  Real thetahigh = Consts::pi;
  Real random;

  while(error > tol){
    random = MathLib::ran1(Injection::randSeed);

    errorlow = Consts::twoPi * random - Consts::pi - (thetalow + sin(thetalow));
    errorhigh = Consts::twoPi * random - Consts::pi - (thetahigh + sin(thetahigh));

    if(errorlow > errorhigh){
      angle = thetahigh;
      error = errorhigh;
      thetalow = thetahigh - (thetahigh - thetalow)/2.0;
    }
    else if (errorhigh > errorlow){
      angle = thetalow;
      error = errorlow;
      thetahigh = thetalow + (thetahigh - thetalow)/2.0;
    }
    
    std::cerr<<" high, low, angle, error "<<thetahigh<<" "<<thetalow<<" "<<angle<<" "<<error<<"\n";
  }

  return angle;
}
