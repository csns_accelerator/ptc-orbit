/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Aperture.cc
//
// AUTHOR
//    John Galambos
//    ORNL, jdg@ornl.gov
//    Sarah Cousineau, ORNL, scousine@sns.gov
//    Jeff Holmes, ORNL, jzh@sns.gov
//
// CREATED
//    12/4/98
//
//  DESCRIPTION
//    Module descriptor file for the Aperture module. This module 
//    contains source for the Aperture related info, which is used
//    to be able to mark spots where particles intersect.
//
//  REVISION HISTORY
//    04/22/02
//    Circular, Elliptical, and RaceTrack apertures added,
//    as well circular, elliptical, racetrack, and rectangular
//    aperture sets.
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Aperture.h"
#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "StreamType.h"
#include "MapBase.h"
#include "TransMapHead.h"
#include "Collimator.h"
#include <iostream>
#include <iomanip>
#include <cmath>

//MPI header file for parallel stuff ==MPI==
#include "mpi.h"

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

Array(ObjectPtr) AperturePointers;
extern Array(ObjectPtr) syncP, mParts, nodes, tMaps;


///////////////////////////////////////////////////////////////////////////
//
// Local routines:
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    ApertureBase 
//
// INHERITANCE RELATIONSHIPS
//    ApertureBase  -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a base class for storing Aperture information. 
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//    _transparent - switch to either let the particle pass or not.
//                   if transparent is false,(==0) the particle is lost,
//                   and removed from the herd.
//    _nHits       - number of hits.
//    _showAperture - "Routine to print out the aperture parameters"
//    
// PRIVATE MEMBERS
//    None.
//
// 
///////////////////////////////////////////////////////////////////////////

class ApertureBase : public Node {
  Declare_Standard_Members(ApertureBase, Node);
public:
    ApertureBase(const String &n, const Integer &order, const Integer &trans):
            Node(n, 0., order), _transparent(trans)
            
        {
          _nHits = 0;
          _nHitsGlobal = 0;
        }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Void _showAperture(Ostream &sout)=0;
    static Void _syncHits(Integer &nLoc, Integer &nGlob);
    Integer _transparent;
    Integer _nHits;
    Integer _nHitsGlobal;
    
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS ApertureBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(ApertureBase, Node);

Void ApertureBase::_syncHits( Integer &nLoc, Integer &nGlob)
{
  nGlob = nLoc;
  if( Aperture::nMPIsize > 1 ){
          MPI_Allreduce(&nLoc,&nGlob,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  }
}


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   RectAperture 
//
// INHERITANCE RELATIONSHIPS
//    RectAperture  -> ApertureBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for rectangular aperture information. 
//
// PUBLIC MEMBERS
//    Real _xMin   - minimum x for aperture (mm)
//    Real _xMax   - maximum x for aperture (mm)
//    Real _yMin   - minimum y for aperture (mm)
//    Real _yMax   - maximum y for aperture (mm)
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class RectAperture : public ApertureBase {
  Declare_Standard_Members(RectAperture, ApertureBase);
public:
    RectAperture(const String &n, const Integer &order, 
          const Real &xmin, const Real  &xmax, 
          const Real &ymin, const Real  &ymax, const Integer &transp):
            ApertureBase(n, order, transp), _xMin(xmin), _xMax(xmax), 
            _yMin(ymin), _yMax(ymax)
            
        { 
	  _nHits=0;
	  _nHitsGlobal=0;
        }

    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showAperture(Ostream &sout);

    const Real _xMin, _xMax, _yMin, _yMax;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS RectAperture
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(RectAperture, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    RectAperture::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void RectAperture::_nodeCalculator(MacroPart &mp)
{    
 
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    RectAperture::updatePartAtNode
//
// DESCRIPTION
//    Check to see if any particles hit this aperture
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void RectAperture::_updatePartAtNode(MacroPart& mp)
{
    Real rTrans2;
    Vector(Real) lostParts;
    Integer nLost =  0;

    Integer j=1;
     
    while(j <= mp._nMacros)
    {
 
      if( mp._x(j) > _xMax ||
          mp._x(j) < _xMin ||
          mp._y(j) > _yMax ||
          mp._y(j) < _yMin )
	{
          _nHits++;
          if((!_transparent) && (!TransMap::tuneCalcOn))
	    {
               mp._addLostMacro(j, _position);
               j--;
	    }
	}
       j++;
    }
  
}

Void RectAperture::_showAperture(Ostream &sout)
{ 
  _syncHits(_nHits,_nHitsGlobal);

  if( Aperture::nMPIrank == 0){
   sout << "Rectangular Aperture:"  << "\n";
   sout << " Min x [mm] =  " << _xMin << " Max x [mm] =  " << _xMax <<"\n";
   sout << " Min y [mm] =  " << _yMin << " Max y [mm] =  " << _yMax <<"\n";
   sout << " Position in ring [m] " << _position << "\n"; 
   sout << " Number of macroparticle hits = " << _nHitsGlobal << "\n";
  }
}


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   CircAperture 
//
// INHERITANCE RELATIONSHIPS
//    CircAperture  -> ApertureBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for circular aperture information. 
//
// PUBLIC MEMBERS
//    Real _radius - minimum radius for aperture (mm)
//    Real _xoffset - x center offset (mm)
//    Real _yoffset - y center offset (mm)
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class CircAperture : public ApertureBase {
  Declare_Standard_Members(CircAperture, ApertureBase);
public:
    CircAperture(const String &n, const Integer &order, 
          const Real &radius, const Real &xoffset, 
          const Real &yoffset, const Integer &transp):
      ApertureBase(n, order, transp), _radius(radius),
      _xoffset(xoffset), _yoffset(yoffset)
        { 
	  _nHits=0;
	  _nHitsGlobal=0;
        }

    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showAperture(Ostream &sout);
    const Real _radius, _xoffset, _yoffset;

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS CircAperture
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(CircAperture, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    CircAperture::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void CircAperture::_nodeCalculator(MacroPart &mp)
{    
 
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    CircAperture::updatePartAtNode
//
// DESCRIPTION
//    Check to see if any particles hit this aperture
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void CircAperture::_updatePartAtNode(MacroPart& mp)
{
    Real rTrans2;
    Vector(Real) lostParts;
    Integer nLost =  0;

    Integer j=1;

   
    
    while( j <= mp._nMacros)
    {
 
      if( (Sqr(mp._x(j)-_xoffset)+Sqr(mp._y(j)-_yoffset)) > Sqr(_radius))
      	{
          _nHits++;
          if((!_transparent) && (!TransMap::tuneCalcOn))
	    {
               mp._addLostMacro(j, _position);
               j--;
	    }
	}
       j++;
    }
  
}

Void CircAperture::_showAperture(Ostream &sout)
{ 
  _syncHits(_nHits,_nHitsGlobal);

  if( Aperture::nMPIrank == 0){ 
   sout << "Circular Aperture:"  << "\n";
   sout << " Radius [mm] =  " << _radius<<"\n";
   sout << " x center offset [mm] = "<< _xoffset <<"\n";
   sout << " y center offset [mm] = "<< _yoffset <<"\n";
   sout << " Position in ring [m] " << _position << "\n";
   sout << " Number of macroparticle hits = " << _nHitsGlobal << "\n";
  }
}


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   ElliptAperture 
//
// INHERITANCE RELATIONSHIPS
//    ElliptAperture  -> ApertureBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for elliptical aperture information. 
//
// PUBLIC MEMBERS
//    Real _appx - x for aperture (mm)
//    Real _appy - y for aperture (mm)
//    Real _xoffset - x center offset (mm)
//    Real _yoffset - y center offset (mm)
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class ElliptAperture : public ApertureBase {
  Declare_Standard_Members(ElliptAperture, ApertureBase);
public:
    ElliptAperture(const String &n, const Integer &order, 
          const Real &appx, const Real &appy, const Real &xoffset,
	  const Real &yoffset, const Integer &transp):
      ApertureBase(n, order, transp), _appx(appx), _appy(appy),
      _xoffset(xoffset), _yoffset(yoffset)
        { 
	  _nHits=0;
	  _nHitsGlobal=0;
        }

    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showAperture(Ostream &sout);

    const Real _appx, _xoffset;
    const Real _appy, _yoffset;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS CircAperture
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(ElliptAperture, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    ElliptAperture::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void ElliptAperture::_nodeCalculator(MacroPart &mp)
{    
 
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    ElliptAperture::updatePartAtNode
//
// DESCRIPTION
//    Check to see if any particles hit this aperture
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void ElliptAperture::_updatePartAtNode(MacroPart& mp)
{
    Real rTrans2;
    Vector(Real) lostParts;
    Integer nLost =  0;

    Integer j=1;
     
    while( j <= mp._nMacros)
    {
 
      if( (Sqr((mp._x(j)-_xoffset)/_appx)+
	   Sqr((mp._y(j)-_yoffset)/_appy)) > 1.)
      	{
          _nHits++;
          if((!_transparent) && (!TransMap::tuneCalcOn))
	    {
               mp._addLostMacro(j, _position);
               j--;
	    }
	}
       j++;
    }
  
}

Void ElliptAperture::_showAperture(Ostream &sout)
{    
  _syncHits(_nHits,_nHitsGlobal);

  if( Aperture::nMPIrank == 0){ 
   sout << " Elliptical Aperture:"  << "\n";
   sout << " semimajor (x) axis [mm] =  " << _appx << "\n";
   sout << " semiminor (y) axis [mm] =  " << _appy << "\n";
   sout << " x center offset [mm] = " << _xoffset << "\n";
   sout << " y center offset [mm] = " << _yoffset << "\n";
   sout << " Position in ring [m] " << _position << "\n";
   sout << " Number of macroparticle hits = " << _nHitsGlobal << "\n";
  }
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   RaceTrackAperture 
//
// INHERITANCE RELATIONSHIPS
//    RaceTrackAperture  -> ApertureBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for racetrack aperture information. 
//
// PUBLIC MEMBERS
//    Real _appx - x for aperture (mm)
//    Real _appy - y for aperture (mm)
//    Real _xappstraight - straight section length for x aperture (mm)
//    Real _yappstraight - straight section length for y aperture (mm)
//    Real _xoffset - x center offset (mm)
//    Real _yoffset - y center offset (mm)
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class RaceTrackAperture : public ApertureBase {
  Declare_Standard_Members(RaceTrackAperture, ApertureBase);
public:
  RaceTrackAperture(const String &n, const Integer &order,
		    const Real &appx, const Real &appy, 
		    const Real &xappstraight,const Real &yappstraight, 
		    const Real &xoffset, const Real &yoffset,  
		    const Integer &transp):
    ApertureBase(n, order, transp), _appx(appx), _appy(appy),
    _xappstraight(xappstraight), _yappstraight(yappstraight),
    _xoffset(xoffset), _yoffset(yoffset)
  {
    _nHits=0;
    _nHitsGlobal=0;
  }
  
  virtual Void _updatePartAtNode(MacroPart &mp);
  virtual Void _nodeCalculator(MacroPart &mp);
  virtual Void _showAperture(Ostream &sout);
  
  const Real _appx;
  const Real _appy;
  const Real _xappstraight, _yappstraight;
  const Real _xoffset, _yoffset;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS RaceTrackAperture
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(RaceTrackAperture, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   RaceTrackAperture::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//   None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void RaceTrackAperture::_nodeCalculator(MacroPart &mp)
{    
 
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    RaceTrackAperture::updatePartAtNode
//
// DESCRIPTION
//    Check to see if any particles hit this aperture
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void RaceTrackAperture::_updatePartAtNode(MacroPart& mp)
{
    Real rTrans2;
    Vector(Real) lostParts;
    Integer nLost = 0;
    Real xval, yval;

    Integer j=1;

    while( j <= mp._nMacros)
    {
      xval = 0.0;
      yval = 0.0;
      if ((mp._x(j)-_xoffset) > _xappstraight / 2.0) 
	xval = (mp._x(j)-_xoffset) - _xappstraight / 2.0;
      if ((mp._x(j)-_xoffset) < -_xappstraight / 2.0) 
	xval = (mp._x(j)-_xoffset) + _xappstraight / 2.0;
      if ((mp._y(j)-_yoffset) > _yappstraight / 2.0) 
	yval = (mp._y(j)-_yoffset) - _yappstraight / 2.0;
      if ((mp._y(j)-_yoffset) < -_yappstraight / 2.0) 
	yval = (mp._y(j)-_yoffset) + _yappstraight / 2.0;

      if( (Sqr(xval/_appx)+Sqr(yval/_appy)) > 1.)
      	{
          _nHits++;
          if((!_transparent) && (!TransMap::tuneCalcOn))
	    {
               mp._addLostMacro(j, _position);
               j--;
	    }
	}
       j++;
    }
}

Void RaceTrackAperture::_showAperture(Ostream &sout)
{    
  _syncHits(_nHits,_nHitsGlobal);

  if( Aperture::nMPIrank == 0){ 
   sout << " Race Track Aperture:"  << "\n";
   sout << " semimajor (x) axis [mm] =  " << _appx << "\n";
   sout << " semiminor (y) axis [mm] =  " << _appy << "\n";
   sout << " length of x straight section [mm] =  " << _xappstraight << "\n";
   sout << " length of y straight section [mm] =  " << _yappstraight << "\n";
   sout << " x center offset [mm] = " << _xoffset <<"\n";
   sout << " y center offset [mm] = " << _yoffset <<"\n";
   sout << " Position in ring [m] " << _position << "\n";
   sout << " Number of macroparticle hits = " << _nHitsGlobal << "\n";
  }
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   MomentumAperture 
//
// INHERITANCE RELATIONSHIPS
//    MomentumAperture  -> ApertureBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for rectangular aperture information. 
//
// PUBLIC MEMBERS
//    Real       _dp_pMax   - maximum dp/p allowed.
//    Integer    _calcFreq  - turn frequency to do the aperture calcultion.
//
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class MomentumAperture : public ApertureBase {
  Declare_Standard_Members(MomentumAperture, ApertureBase);
public:
    MomentumAperture(const String &n, const Integer &order, 
          const Real &dp_pMax, const Integer  &calcFreq):
            ApertureBase(n, order, 0), _dp_pMax(dp_pMax), _calcFreq(calcFreq)
        { 
	  _nHits=0;
	  _nHitsGlobal=0;
        }

    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showAperture(Ostream &sout);

    const Real _dp_pMax;
    const Integer _calcFreq;

};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS MomentumAperture
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(MomentumAperture, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MomentumAperture::NodeCalculator
//
// DESCRIPTION
//   empty
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MomentumAperture::_nodeCalculator(MacroPart &mp)
{    
 
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    MomentumAperture::updatePartAtNode
//
// DESCRIPTION
//    Check to see if any particles hit this aperture
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void MomentumAperture::_updatePartAtNode(MacroPart& mp)
{
  if( ( (Ring::nTurnsDone-1) % _calcFreq) != 0) return;

    Integer nLost =  0;

    Integer j=1;
     
    while( j <= mp._nMacros)
    {
 
      if( Abs(mp._dp_p(j))  > _dp_pMax )
	{
          _nHits++;
          if((!_transparent) && (!TransMap::tuneCalcOn))
	    {
               mp._addLostMacro(j, _position);
               j--;
	    }
	}
       j++;
    }
  
}

Void MomentumAperture::_showAperture(Ostream &sout)
{    
  _syncHits(_nHits,_nHitsGlobal);

  if( Aperture::nMPIrank == 0){ 
   sout << "Momentumangular Aperture:"  << "\n";
   sout << " Max dp/p =  " << _dp_pMax << "\n";
   sout << " turn freqency =  " << _calcFreq << "\n";
   sout << " Position in ring [m] " << _position << "\n";
   sout << " Number of macroparticle hits = " << _nHitsGlobal << "\n";
  }
}

///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE Aperture
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Aperture::ctor
//
// DESCRIPTION
//    Initializes the various Aperture related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::ctor()
{

  // set some initial values

    nApertures = 0;

  // parallel stuff  ==MPI==  ==start===
    iMPIini=0;
    MPI_Initialized(&iMPIini);
    nMPIsize = 1;
    nMPIrank = 0;
    if(iMPIini > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank);
    }
  // parallel stuff  ==MPI==  ==stop===
    
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addRectAperture
//
// DESCRIPTION
//    Adds a rectangular aperture.
//
// PARAMETERS
//    name:    Name for this node (string)
//    order:   node order index (integer)
//    xmin:    minumim horizontal extent of the aperture [mm]
//    xmax:    maximum horizontal eextent of the aperture [mm]
//    ymin:    minimum vertical extent of the aperture [mm]
//    ymax:    maximum vertical extent of the aperture [mm]
//    transp:  integer switch for transparency (1 == transparent 
//             and simply tally hits, != 1 means remove particle).
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addRectAperture(const String &name, 
          const Integer &order,const  Real &xmin, const Real &xmax,
          const  Real &ymin, const Real &ymax, const Integer &transp)
{
  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nApertures == AperturePointers.size())
      AperturePointers.resize(nApertures + 1);
   nNodes++;
  
   nodes(nNodes-1) = new RectAperture(name, order, xmin, xmax, ymin, ymax,
                     transp);
   nApertures++;
   
   AperturePointers(nApertures -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addCircAperture
//
// DESCRIPTION
//    Adds a circular aperture.
//
// PARAMETERS
//    name:    Name for this node (string)
//    order:   node order index (integer)
//    radius:  radius of the circular aperture (mm)
//    xoffset: x center offset (mm)
//    yoffset: y center offset (mm)
//    transp:  integer switch for transparency (1 == transparent 
//             and simply tally hits, != 1 means remove particle).
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addCircAperture(const String &name, 
          const Integer &order, const Real &radius, const Real &xoffset,
	  const Real &yoffset, const Integer &transp)

{

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nApertures == AperturePointers.size())
      AperturePointers.resize(nApertures + 1);
   nNodes++;
  
   nodes(nNodes-1) = new CircAperture(name, order, radius, xoffset,
				      yoffset, transp);
   nApertures++;
   
   AperturePointers(nApertures -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addElliptAperture
//
// DESCRIPTION
//    Adds an elliptical aperture.
//
// PARAMETERS
//    name:    Name for this node (string)
//    order:   node order index (integer)
//    appx:    length of x aperture axis (semimajor axis) (mm)
//    appy:    length of y aperture axis (semiminor axis) (mm)
//    xoffset: x center offset (mm)
//    yoffset: y center offset (mm)
//    transp:  integer switch for transparency (1 == transparent 
//             and simply tally hits, != 1 means remove particle).
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addElliptAperture(const String &name, 
          const Integer &order, const Real &appx, const Real &appy,
				 const Real &xoffset, const Real &yoffset,
				 const Integer &transp)

{

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nApertures == AperturePointers.size())
      AperturePointers.resize(nApertures + 1);
   nNodes++;
  
   nodes(nNodes-1) = new ElliptAperture(name, order, appx, appy, 
					xoffset, yoffset, transp);
   nApertures++;
   
   AperturePointers(nApertures -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addRaceTrackAperture
//
// DESCRIPTION
//    Adds a racetrack aperture.
//
// PARAMETERS
//    name:    Name for this node (string)
//    order:   node order index (integer)
//    appx:    length of x aperture axis (semimajor axis) (mm)
//    appy:    length of y aperture axis (semiminor axis) (mm)
//    xappstraight: x length of straight section (mm)
//    yappstraight: y length of straight section (mm)
//    xoffset: x center offset (mm)
//    yoffset: y center offset (mm)
//    transp:  integer switch for transparency (1 == transparent 
//             and simply tally hits, != 1 means remove particle).
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addRaceTrackAperture(const String &name,
          const Integer &order, const Real &appx, const Real &appy,
          const Real &xappstraight, const Real &yappstraight,
	  const Real &xoffset, const Real &yoffset, const Integer &transp)
{

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nApertures == AperturePointers.size())
      AperturePointers.resize(nApertures + 1);
   nNodes++;

   nodes(nNodes-1) = new RaceTrackAperture(name, order, appx, appy,
                                           xappstraight, yappstraight,
					   xoffset, yoffset, transp);
   nApertures++;

   AperturePointers(nApertures -1) = nodes(nNodes-1);

   nodesInitialized = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addMomentumAperture
//
// DESCRIPTION
//    Adds a mommentum aperture.
//
// PARAMETERS
//    name:     Name for this node (string)
//    order:    node order index (integer)
//    dp_pMax:  maximim dp/p allowed
//    calcFreq: turn frequency to perform the calculation
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addMomentumAperture(const String &name, 
          const Integer &order, const  Real &dp_pMax, const Integer &calcFreq)

{

  if (nNodes == nodes.size())  nodes.resize(nNodes + 1);
  if(nApertures == AperturePointers.size())
      AperturePointers.resize(nApertures + 1);
   nNodes++;
  
   nodes(nNodes-1) = new MomentumAperture(name, order, dp_pMax, calcFreq);
   nApertures++;
   
   AperturePointers(nApertures -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
// Aperture::addRectApertureSet
//
// DESCRIPTION
//    Adds a rectangular aperture node set to the ring, within a specified
//    range of node indices.
//
// PARAMETERS
//   
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addRectApertureSet(const Real &xmin, const Real &xmax, 
		const Real &ymin, const Real &ymax, const Integer &iindex, 
		const Integer &findex, const Integer &transp)

{
    String wname;
    //char num2[10];
    String cons("RectApp. Node ");
    //MapBase *tm;
    
    for (Integer n=iindex; n<= findex; n+=10)
    {
      //tm = MapBase::safeCast(tMaps(n-1));
      //sprintf(num2, "(%d)", n); 
        wname = cons; 
        addRectAperture(wname, (n+5), xmin, xmax, ymin,
                        ymax, transp);

    }

}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addCircApertureSet
//
// DESCRIPTION
//    Adds a circular aperture node set to the ring, within a specified
//    range of node indices.
//
// PARAMETERS
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addCircApertureSet(const Real &radius, const Real &xoffset,
				  const Real &yoffset, const Integer &iindex,
				  const Integer &findex, const Integer &transp)

{
    String wname;
    //char num2[10];
    String cons("CircApp. Node ");
    // MapBase *tm;
    

    for (Integer n=iindex; n <= findex; n+=10)
    {
      //tm = MapBase::safeCast(tMaps(n-1));
      //sprintf(num2, "(%d)", n); 
        wname = cons; 
        addCircAperture(wname, (n+5), radius, xoffset, yoffset, transp);

    }

}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addElliptApertureSet
//
// DESCRIPTION
//    Adds an elliptical aperture node set to the ring, within a specified
//    range of node indices.
//
// PARAMETERS
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addElliptApertureSet(const Real &appx, const Real &appy,
				    const Real &xoffset, const Real &yoffset,
				    const Integer &iindex, 
				    const Integer &findex,
				    const Integer &transp)

{
    String wname;
    //char num2[10];
    String cons("ElliptApp. Node ");
    //  MapBase *tm;
    

    for (Integer n=iindex; n <= findex; n+=10)
    {
      //tm = MapBase::safeCast(tMaps(n-1));
      //sprintf(num2, "(%d)", n); 
        wname = cons; 
        addElliptAperture(wname, (n+5), appx, appy, xoffset, yoffset, transp);
    }

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addRaceTrackApertureSet
//
// DESCRIPTION
//    Adds a racetrack aperture node set to the ring, within a specified
//    range of node indices.
//
// PARAMETERS
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addRaceTrackApertureSet(const Real &appx,
				       const Real &appy,
				       const Real &xappstraight,
				       const Real &yappstraight,
				       const Real &xoffset,
				       const Real &yoffset,
				       const Integer &iindex,
				       const Integer &findex,
				       const Integer &transp)
{
    String wname;
    //char num2[10];
    String cons("RaceTrackApp. Node ");
    //  MapBase *tm;
    
    for (Integer n=iindex; n <= findex; n+=10)
    {
      //tm = MapBase::safeCast(tMats(n-1));
      //sprintf(num2, "(%d)", n);
        wname = cons;
        addRaceTrackAperture(wname, (n+5), appx, appy,
                             xappstraight, yappstraight, 
			     xoffset, yoffset, transp);
    }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::addMomentumApertureSet
//
// DESCRIPTION
//    Adds an momentum aperture node set to the ring, within a specified
//    range of node indices.
//
// PARAMETERS
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::addMomentumApertureSet(const Real &dp_pMax, 
				      const Integer &calcFreq,
				      const Integer &iindex, 
				      const Integer &findex)
{
    String wname;
    //char num2[10];
    String cons("MomentumApp. Node ");
    //MapBase *tm;
    

    for (Integer n=iindex; n <= findex; n+=10)
    {
      //  tm = MapBase::safeCast(tMaps(n-1));
      // sprintf(num2, "(%d)", n); 
        wname = cons; 
        addMomentumAperture(wname, (n+5), dp_pMax, calcFreq);
  
    }

}




///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Aperture::showApertures
//
// DESCRIPTION
//    Prints out the settings of the active apertures
//
// PARAMETERS
//    sout - The stream to send output to.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::showApertures(Ostream &sout)
{

  ApertureBase *tlp;

  if (nApertures == 0)
    {
      if( Aperture::nMPIrank == 0) sout << "\n No Apertures \n\n";
      return;
    }

   for (Integer i=1; i<= nApertures; i++)
     {

        tlp = ApertureBase::safeCast(AperturePointers(i-1));
        tlp->_showAperture(sout);
     }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::dumpTurnHist(const Integer &herdNo, Ostream &sout)
//
// DESCRIPTION
//  Calculate and dump histogram of number of particles lost to
//  all aperatures (including collimators) in every turn.
//
// PARAMETERS
//    herdNo - Reference to a macro-particle herd
//    sout  - Reference to a stream to dump the info.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::dumpTurnHist(const Integer &herdNo, Ostream &sout)
{
    MacroPart *mp;
    
    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);
   
    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1)); 

    Integer nTurnsDone = mp->_nTurnsDone;
    Integer nLostMacros = mp->_lostOnes._nLostMacros;
    Integer nLostMacrosGlobal = nLostMacros;
    Integer nMacros = mp->_nMacros;
    Integer nMacrosGlobal = nMacros;
    if( Aperture::nMPIsize > 1){
      MPI_Allreduce(&nLostMacros,&nLostMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
      MPI_Allreduce(&nMacros,&nMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    }


    Vector(Real) _nTurnLost(nTurnsDone);
    Vector(Real) _nTotalLost(nTurnsDone);
    Vector(Real) _nPercentLost(nTurnsDone);

    for(Integer k=1; k<= mp->_nTurnsDone; k++)
      {
	_nTurnLost(k)=0;
      }
    
    for (Integer i=1; i<= mp->_lostOnes._nLostMacros; i++)
      { 
	_nTurnLost((mp->_lostOnes._turnLost(i))+1)++;       
      }
 
    //gather data across all CPUs   ======start=========   
    if( Aperture::nMPIsize > 1 && nTurnsDone > 0 ){
      Vector(Real) _nTurnLostGlobal(nTurnsDone);
      MPI_Allreduce((double *) &_nTurnLost.data(),
                    (double *) &_nTurnLostGlobal.data(),
                     nTurnsDone,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      for(int i=1; i <= nTurnsDone; i++){
	_nTurnLost(i) = _nTurnLostGlobal(i);
      }
    }    
    //gather data across all CPUs   ======stop==========

    _nTotalLost(1)=_nTurnLost(1);
    _nPercentLost(1)=_nTotalLost(1)/(nLostMacrosGlobal + nMacrosGlobal);

    for(Integer j=2; j<=mp->_nTurnsDone; j++)
      {
	_nTotalLost(j)=_nTotalLost(j-1)+_nTurnLost(j);
	_nPercentLost(j)=_nTotalLost(j)/(nLostMacrosGlobal + nMacrosGlobal) ;
      }

    for(Integer j=1; j<=mp->_nTurnsDone; j++)
      {
	if( Aperture::nMPIrank == 0) sout << setw(4) << j << setw(12) << _nTurnLost(j);
	if( Aperture::nMPIrank == 0) sout << setw(12) << _nTotalLost(j);
	if( Aperture::nMPIrank == 0) sout << setw(12) << _nPercentLost(j) <<"\n";
      }   
}





///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::dumpApertureHits(const Integer &herdNo, Ostream &sout)
//
// DESCRIPTION
//  Calculate and dump histogram of number of particles lost to
//  each aperture node.
//
// PARAMETERS
//    herdNo - Reference to a macro-particle herd
//    sout  - Reference to a stream to dump the info.
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////



Void Aperture::dumpApertureHits(const Integer &herdNo, Ostream &sout)
{

  ApertureBase *tlp;
  MacroPart *mp;
  Node *n1;
  String wname;
  Real totPerc=0.;

  sout.setf(ios::left, ios::adjustfield);

  if ( (herdNo & All_Mask) > Particles::nHerds)
    except(badHerdNo);
   
  SyncPart *sp = SyncPart::safeCast(syncP(0));
  mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1)); 

  Integer nLostMacros = mp->_lostOnes._nLostMacros;
  Integer nLostMacrosGlobal = nLostMacros;
  if( Aperture::nMPIsize > 1){
      MPI_Allreduce(&nLostMacros,&nLostMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  }

  if (nApertures == 0)
  {
      if( Aperture::nMPIrank == 0) sout << "\n No Apertures \n";
      return;
  }

  for (Integer i=1; i<= nApertures; i++)
     {
        tlp = ApertureBase::safeCast(AperturePointers(i-1));
        tlp->_syncHits(tlp->_nHits,tlp->_nHitsGlobal);
        tlp->nameOut(wname);
	if (wname.length() > 16) wname = wname.before(15) + "*";
	if( Aperture::nMPIrank == 0) sout<<setw(17) << wname <<setw(0);
	if( Aperture::nMPIrank == 0) sout<< setw(5) << tlp->_oindex;
	if( Aperture::nMPIrank == 0) sout<< setw(8) << tlp->_position << setw(5) << tlp->_nHitsGlobal;
	if(mp->_lostOnes._nLostMacros == 0)
	{
	    if( Aperture::nMPIrank == 0) sout<< setw(15) <<"0\n";
	}
	else
	{
	  totPerc+=float(tlp->_nHitsGlobal)/
	    float(nLostMacrosGlobal)*100;
	    if( Aperture::nMPIrank == 0) {
                sout<< setw(15) << (float(tlp->_nHitsGlobal)/
		    float(nLostMacrosGlobal))*100<<"\n";
	    }
	}
     }

}



///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Aperture::binImpact(const Integer &herdNo, Ostream &sout, 
//                      const Integer &numbins)
//
// DESCRIPTION
//  Create histogram of lost particle x and y impact values at aperture.
//
// PARAMETERS
//    herdNo - Reference to a macro-particle herd
//    numbins - number of bins
//
// RETURNS
//     Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Aperture::binImpact(const Integer &herdNo, Ostream &sout, 
		    const Integer &numbins)
{
    
    MacroPart *mp;

    if ( (herdNo & All_Mask) > Particles::nHerds)
         except(badHerdNo);
   
    SyncPart *sp = SyncPart::safeCast(syncP(0));
    mp = MacroPart::safeCast(mParts((herdNo & All_Mask)-1)); 

    class Bins
    {
      public:
      Double _minVal;
      Double _maxVal;
      Vector(Real) _binVal;
      Vector(Real) _binCount;
    };

    Real binsize;
    
    //Do the X Histogram 
    
    Bins _xbin;
    _xbin._maxVal=0.;

    _xbin._binVal.resize(numbins);
    _xbin._binCount.resize(numbins);

    for(Integer j=0; j<= mp->_lostOnes._nLostMacros; j++)
    {
      if(mp->_lostOnes._xLost(j) > _xbin._maxVal)
	_xbin._maxVal=mp->_lostOnes._xLost(j);
    }
       
    _xbin._minVal= _xbin._maxVal;

    for(Integer j=0; j<= mp->_lostOnes._nLostMacros; j++)
    {
    if(mp->_lostOnes._xLost(j) < _xbin._minVal)
          _xbin._minVal=mp->_lostOnes._xLost(j);
    }
 
    if( Aperture::nMPIsize > 1){
      double buf_loc[2], buf_glb[2] ;
      buf_loc[0] =   _xbin._maxVal;
      buf_loc[1] = - _xbin._minVal;
      MPI_Allreduce(buf_loc,buf_glb,2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      _xbin._maxVal =   buf_glb[0];
      _xbin._minVal = - buf_glb[1];
    }

    (Real) numbins;
    binsize=(_xbin._maxVal - _xbin._minVal)/numbins;
     
    _xbin._binVal(1)=_xbin._minVal + binsize/2.;

    for(Integer j=2; j<=numbins; j++)
    {
    _xbin._binVal(j)=_xbin._binVal(j-1)+binsize;
    }

    for(Integer i=1; i<=mp->_lostOnes._nLostMacros; i++)
      {
	for(Integer k=1; k<=numbins; k++)
	  {
	    if(mp->_lostOnes._xLost(i) >= 
	       (_xbin._binVal(k) - (binsize/2.) - .001) &
	       mp->_lostOnes._xLost(i) <= 
	       (_xbin._binVal(k) + (binsize/2.) + .001))
	      {
		_xbin._binCount(k)++;
	      }
	  }
      }

    //gather data across all CPUs   ======start=========   
    if( Aperture::nMPIsize > 1 && numbins > 0 ){
      Vector(Real) temp_bin(numbins);
      MPI_Allreduce((double *) &_xbin._binCount.data(),
                    (double *) &temp_bin.data(),
                     numbins ,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      for(int i=1; i <= numbins ; i++){
	_xbin._binCount(i) = temp_bin(i);
      }
    }    
    //gather data across all CPUs   ======stop==========

    //Do the Y Histogram 
    
    Bins _ybin;
    _ybin._maxVal=0.;

    _ybin._binVal.resize(numbins);
    _ybin._binCount.resize(numbins);

    for(Integer j=0; j<= mp->_lostOnes._nLostMacros; j++)
    {
      if(mp->_lostOnes._yLost(j) > _ybin._maxVal)
	_ybin._maxVal=mp->_lostOnes._yLost(j);
    }
       
    _ybin._minVal= _ybin._maxVal;

    for(Integer j=0; j<= mp->_lostOnes._nLostMacros; j++)
    {
    if(mp->_lostOnes._yLost(j) < _ybin._minVal)
          _ybin._minVal=mp->_lostOnes._yLost(j);
    }

    if( Aperture::nMPIsize > 1){
      double buf_loc[2], buf_glb[2] ;
      buf_loc[0] =   _ybin._maxVal;
      buf_loc[1] = - _ybin._minVal;
      MPI_Allreduce(buf_loc,buf_glb,2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      _ybin._maxVal =   buf_glb[0];
      _ybin._minVal = - buf_glb[1];
    }
    
    binsize=(_ybin._maxVal - _ybin._minVal)/numbins;
     
    _ybin._binVal(1)=_ybin._minVal + binsize/2.;
 
    for(Integer j=2; j<=numbins; j++)
      {
	_ybin._binVal(j)=_ybin._binVal(j-1)+binsize;
      }

    for(Integer i=1; i<=mp->_lostOnes._nLostMacros; i++)
      {
	for(Integer k=1; k<=numbins; k++)
	  {
	    if(mp->_lostOnes._yLost(i) >= 
	       (_ybin._binVal(k) - (binsize/2.)-.001) &
	       mp->_lostOnes._yLost(i) <= 
	       (_ybin._binVal(k) + (binsize/2.)+.001))
	      {
		_ybin._binCount(k)++;
	      }
	  }
      }

    //gather data across all CPUs   ======start=========   
    if( Aperture::nMPIsize > 1 && numbins > 0 ){
      Vector(Real) temp_bin(numbins);
      MPI_Allreduce((double *) &_ybin._binCount.data(),
                    (double *) &temp_bin.data(),
                     numbins ,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      for(int i=1; i <= numbins ; i++){
	_ybin._binCount(i) = temp_bin(i);
      }
    }    
    //gather data across all CPUs   ======stop==========

    //Output Both

    for(Integer j=1; j<=numbins; j++)
      {
    	if( Aperture::nMPIrank == 0) sout<<setw(10)<<_xbin._binVal(j)<<setw(5)<<_xbin._binCount(j);
	if( Aperture::nMPIrank == 0) sout<<setw(10)<<_ybin._binVal(j)<<setw(5)<<_ybin._binCount(j)<<"\n";
      }
    

}










