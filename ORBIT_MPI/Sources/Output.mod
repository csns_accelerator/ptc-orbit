/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Output.mod
//
// AUTHOR
//    John GalambosORNL, jdg@ornl.gov
//
// CREATED
//    11/2/97
//
//  DESCRIPTION
//    Module descriptor file for the Output module
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Output - "Output Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Sys, Consts, Particles, Ring, Injection, Bump, Diagnostic,
           TransMap, ThinLens;

public:

  Void ctor() 
                  - "Constructor for the Output module."
                  - "Initializes build values.";

  Void showNodes(Ostream &os)
			- "Routine to show all Node info to stream os";
  Void showTransMaps(Ostream &os)
			- "Routine to show TransferMatrix info to stream os";
  Void showRing(Ostream &os)
                  - "Routine to show all the ring information to stream os";
  Void showTurnInfo(Ostream &os)
                  - "Routine to show misc. general turn information";
  Void showInject(Ostream &os)
               - "Sends Injection information to an Ostream.";
  Void showFoil(Ostream &os)
               - "Sends Foil information to an Ostream.";
  Void showTiming(Ostream &os, const Real &et)
               - "Writes CPU timing info to an Ostream.";
  Void showStart(Ostream &os)
               - "Writes basic run input info to an Ostream.";
  Void clear()
               - "Clears the screen.";
  Void showTurn()
               - "Shows turn info.";

  String runName;

private:
  // parallel stuff  ==MPI==  ==start===
  Integer
    iMPIini       - "inf. about MPI 0-non initialized 1-initialized";
  Integer
    nMPIrank      - "rank of this CPU";
  Integer
    nMPIsize      - "number of CPUs for Parallel run";
  // parallel stuff  ==MPI==  ==stop===
}

