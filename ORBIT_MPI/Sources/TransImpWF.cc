////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   TransImpWF.cc
//
// CREATED
//   07/23/2002
//
// DESCRIPTION
//   Source code for the  the Transverse Impedance (Wake Function) module.
//   Uses TImpedanceWF.cc source code.
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Particles.h"
#include "MacroPart.h"
#include "Node.h"
#include "MapBase.h"
#include "TransMapHead.h"

#include "TImpedanceWF.h"
#include "TransImpWF.h"

#include <mpi.h>

#include <iostream>
#include <cstdlib>

using namespace std;

extern Array(ObjectPtr) mParts;
extern Array(ObjectPtr) nodes;
extern Array(ObjectPtr) tMaps;
extern Array(ObjectPtr) syncP;

//==============   defenition of TrImpWF class     ====start====
//==============as interface of TImpedanceWF class =============
///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME :  TrImpWF
//
// INHERITANCE RELATIONSHIPS
//     TrImpWF -> Node* -> Object
//
// DESCRIPTION
//   This is a wrapper class for the TImpedanceWF class to provide
//   this class with methods _updatePartAtNode and _nodeCalculator, and
//   data about index of instance of the TImpedanceWF class.
//
// DATA:
//   _indexTI_WF_instance      index of instance of the TImpedanceWF class .
//
/////////////////////////////////////////////////////////////////////////////

class TrImpWF : public Node {

  public:
  TrImpWF(const String &n, const Integer &order,
          Integer &nBins, Integer &nImpElements): Node(n, 0., order)
  {
    MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
    _indexTI_WF_instance = manager->addNode(nBins, nImpElements);
  };

  ~TrImpWF(){};

  Void nameOut(String &wname) { wname = _name; };
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);

  protected:
  Integer  _indexTI_WF_instance;
};
//==============   defenition of TrImpWF class     ====start====

Void TrImpWF::_nodeCalculator(MacroPart &mp)
{
}

Void TrImpWF::_updatePartAtNode(MacroPart &mp)
{
   MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
   MB_TImpedanceWF* timpNode = manager->getNode(_indexTI_WF_instance);

   Real T = timpNode->getPeriod();

   if(T < 0.){
     double beta = mp._syncPart._betaSync;
     T =  Ring::lRing/(MB_CLIGHT*beta);
   }

   timpNode->propagate(mp,T);
};

//==============   defenition of TrImpWF class     ====start====
//==============================================================

////////////////////////////////////////////////////////////////////////////////
//Source code for   TransImpWF module    ========START===============
////////////////////////////////////////////////////////////////////////////////

Void TransImpWF::ctor()
{
// set some initial values
    iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    nMPIsize_ = 1;
    nMPIrank_ = 0;
    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }
}

Void TransImpWF::dtor()
{
  MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
  delete manager;
}


Integer TransImpWF::addTImpedanceWF(const Integer &posIndex_in, const Integer &nBins_in)
{
	Integer posIndex = posIndex_in;
	Integer nBins = nBins_in;

   Integer nImpElements = 10;

   Integer order = posIndex;
   if (nNodes == nodes.size()) nodes.resize(nNodes + 100);
   nNodes++;

   MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
   Integer ind = manager->getNumbNodes();

   char buf[10];
   sprintf(buf,"%d",ind);

   String wname = "TrImpWF";
   wname =wname +buf;

   TrImpWF* TrImpWF_node;
   TrImpWF_node = new TrImpWF(wname, order, nBins, nImpElements);

   nodes(nNodes-1) = TrImpWF_node;

   return ind;
}

Void TransImpWF::addResElemToTImpedanceWF(const Integer &TImpWFelem_in, const String &axis, Real &R, Real &Q, Real &Freq)
{
	Integer TImpWFelem = TImpWFelem_in;
  Integer ind_xy = -1;
  MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
  MB_TImpedanceWF* timpNode = manager->getNode(TImpWFelem);
  if(axis == "x") ind_xy = 0;
  if(axis == "y") ind_xy = 1;
  if(ind_xy < 0 ){
    std::cerr <<" Method TransImpWF::addResElemToTImpedanceWF:\n";
    std::cerr <<" You should define axis as x or y, but you have ="<<axis<<"\n";
    std::cerr <<" Stop.\n";
    if(iMPIini_ > 0){
          MPI_Finalize();
     }
    exit(1);
  }
  timpNode->addResonantElement(ind_xy,R, Q, Freq);
}

Void TransImpWF::addElemToTImpedanceWF( const Integer &TImpWFelem_in, const String &axis,
                                        Real &W0re, Real &W0im,
                                        Real &ETAre, Real &ETAim)
{
	Integer TImpWFelem = TImpWFelem_in;
  Integer ind_xy = -1;
  MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
  MB_TImpedanceWF* timpNode = manager->getNode(TImpWFelem);
  if(axis == "x") ind_xy = 0;
  if(axis == "y") ind_xy = 1;
  if(ind_xy < 0 ){
    std::cerr <<" Method TransImpWF::addElemToTImpedanceWF:\n";
    std::cerr <<" You should define axis as x or y, but you have ="<<axis<<"\n";
    std::cerr <<" Stop.\n";
    if(iMPIini_ > 0){
          MPI_Finalize();
     }
    exit(1);
  }
  timpNode->addElement(ind_xy,W0re,W0im,ETAre,ETAim);
}

//sets the revolution period to the particular TWF node. The value in seconds.
//If the user will not set up this value - it will be calculated automatically
Void TransImpWF::setRevPeriodToTImpedanceWF( const Integer &TImpWFelem_in, const Real &tRev_in){
	Integer TImpWFelem = TImpWFelem_in;
	MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
  MB_TImpedanceWF* timpNode = manager->getNode(TImpWFelem);
	double tRev = tRev_in;
	timpNode->setPeriod(tRev);
}


////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========START===============
////////////////////////////////////////////////////////////////////////////////

Void TransImpWF::TransImpWFshowXY(Integer &TImpWFelem, String &file_name)
{
  MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
  MB_TImpedanceWF* timpNode = manager->getNode(TImpWFelem);
  char fn[256];
  strncpy(fn,(const char*) file_name,256);
  timpNode->showXY(fn);
}


Void TransImpWF::TransImpWFshowTKick(Integer &TImpWFelem, String &file_name)
{
  MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
  MB_TImpedanceWF* timpNode = manager->getNode(TImpWFelem);
  char fn[256];
  strncpy(fn,(const char*) file_name,256);
  timpNode->showTKick(fn);
}


Void TransImpWF::TransImpWFpropagate(Integer &TImpWFelem, Real &time)
{
  MB_TI_manager* manager = MB_TI_manager::GetMB_TI_manager();
  MB_TImpedanceWF* timpNode = manager->getNode(TImpWFelem);
  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));
  timpNode->propagate(*mp, time);
}

////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========STOP================
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//Source code for   TransImpWF module    ========START===============
////////////////////////////////////////////////////////////////////////////////
