#if !defined(__TransferMap__)
#define __TransferMap__

#include "Object.h"
#include "RealArr3.h"
#include "FNALMaps.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    TransMatrix1
//
// INHERITANCE RELATIONSHIPS
//    TransMatrix1 -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing 1st order transfer matrix information.
//
// PUBLIC MEMBERS
// Real Matrix:
//   _R      The 6x6 R first order transfer matrix
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

class TransMatrix1 : public MapBase {
    Declare_Standard_Members(TransMatrix1, MapBase);
    public:
    TransMatrix1(const String &n, const Integer &order,
                 const Matrix(Real) &R,
                 const Real &bx, const Real &by,
                 const Real &ax, const Real &ay,
                 const Real &ex, const Real &epx,
                 const Real &l, const String &et,
                 const Integer &matrep);

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);

    Integer _matrep;
    Real _detX, _detY;

    Integer _nRTVals, _nRLVals;
    Vector(Real) _RTVals, _RLVals;
    Vector(Integer) _jRTVals, _kRTVals, _kRLVals;
    
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    TransMatrix2
//
// INHERITANCE RELATIONSHIPS
//    TransMatrix2 -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing 2nd order transfer matrix information.
//
// PUBLIC MEMBERS
// Real Array3D
//   _R      The 6x6 1st order transfer matrix
//   _T      The 6x6x6 2nd order transfer matrix
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

class TransMatrix2 : public MapBase {
    Declare_Standard_Members(TransMatrix2, MapBase);
    public:
    TransMatrix2(const String &n, const Integer &order,
                 const Matrix(Real) &R, const Array3(Real) &T,
                 const Real &bx, const Real &by,
                 const Real &ax, const Real &ay,
                 const Real &ex, const Real &epx,
                 const Real &l, const String &et,
                 const Integer &matrep);

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);

    Integer _matrep;
    Real _detX, _detY;

    Integer _nRTVals, _nRLVals, _nTTVals, _nTLVals;
    Vector(Real) _RTVals, _RLVals, _TTVals, _TLVals;
    Vector(Integer) _jRTVals, _kRTVals, _kRLVals, _jTTVals, _kTTVals,
            _lTTVals, _kTLVals, _lTLVals;
    
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    FNALMap
//
// INHERITANCE RELATIONSHIPS
//    FNALMap -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing FNAL Beamline transport maps.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

class FNALMap : public MapBase {
    Declare_Standard_Members(FNALMap, MapBase);
    public:
    FNALMap(const String &n, const Integer &order,
            const Integer &maporder,
            FNALTMs *BeamLine,
            const Integer &BmLIndex,
            const Real &bx, const Real &by,
            const Real &ax, const Real &ay,
            const Real &ex, const Real &epx,
            const Real &l, const String &et);

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);

    Integer _maporder;
    FNALTMs *_BeamLine;
    Integer _BmLIndex;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    PTC_Map
//
// INHERITANCE RELATIONSHIPS
//    PTC_Map -> MapBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class intrfacing PTC code with respect of traking.
//
//
///////////////////////////////////////////////////////////////////////////

class PTC_Map : public MapBase {
    Declare_Standard_Members(PTC_Map, MapBase);
    public:
    PTC_Map(const String &n, const Integer &order,
            const Integer &orbit_ptc_node_index_in,
            const Real &bx, const Real &by,
            const Real &ax, const Real &ay,
            const Real &ex, const Real &epx,
            const Real &l, const String &et);

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);

    Integer orbit_ptc_node_index;
};




#endif   // __TransferMap__
