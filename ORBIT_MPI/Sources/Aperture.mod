/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    ThinLens.mod
//
// AUTHOR
//    John Galambos,  ORNL, jdg@ornl.gov
//    Sarah Cousineau, ORNL, scousine@sns.gov
//    Jeff Holmes, ORNL, jzh@sns.gov
//
// CREATED
//    12/8/98
//
//  DESCRIPTION
//    Module descriptor file for the Aperture module. 
//    This module contains information about Apertres which can
//    limit the allowable space for beam traversal.
//
//
//  REVISION HISTORY
//    04/22/02
//    Circular, Elliptical, and RaceTrack apertures added,
//    as well circular, elliptical, racetrack, and rectangular
//    aperture sets.
/////////////////////////////////////////////////////////////////////////////

module Aperture - "Aperture Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, Particles, Ring, TransMap;

Errors
  badHerdNo        - "Sorry, you asked for an undefined macro Herd";

public:

  Void ctor() 
                  - "Constructor for the Aperture  module."
                  - "Initializes build values.";

  Integer
   nApertures - "Number of apertures in the Ring";

  Void addRectAperture(const String &name, const Integer &o,
         const Real &xmin, const Real &xmax, const Real &ymin, 
         const Real &ymax, const Integer &transp)
                  - "Routine to add a rectagular aperture node ";
  Void addCircAperture(const String &name, const Integer &o,
         const Real &radius, const Real &xoffset, const Real &yoffset,
	 const Integer &transp)
                  - "Routine to add a circular aperture node ";
  Void addElliptAperture(const String &name, const Integer &o,
 	 const Real &appx, const Real &appy, const Real &xoffset,
	 const Real &yoffset, const Integer &transp)
                  - "Routine to add an elliptical aperture node ";
  Void addRaceTrackAperture(const String &name, const Integer &o,
         const Real &appx, const Real &appy, const Real &xappstraight,
	 const Real &yappstraight, const Real &xoffset,
	 const Real &yoffset, const Integer &transp)
                  - "Routine to add a racetrack aperture node ";
  Void addMomentumAperture(const String &name, const Integer &o,
         const Real &dp_pMax, const Integer &calcFreq)
                  - "Routine to add a momentum aperture node ";
  Void addRectApertureSet(const Real &xmin, const Real &xmax, 
		const Real &ymin, const Real &ymax, const Integer &iindex, 
		const Integer &findex, const Integer &transp)
                  - "Routine to add a set of rectagular aperture nodes ";
  Void addCircApertureSet(const Real &radius, const Real &xoffset,
		const Real &yoffset, const Integer &iindex, 
		const Integer &findex, const Integer &transp)
                  - "Routine to add a set of circular aperture nodes ";
  Void addElliptApertureSet(const Real &appx, const Real &appy,
			const Real &xoffset, const Real &yoffset, 
			const Integer &iindex, const Integer &findex,
			const Integer &transp)
                  - "Routine to add an elliptical aperture node set ";
  Void addRaceTrackApertureSet(const Real &appx, const Real &appy,
                        const Real &xappstraight,const Real &yappstraight,
			const Real &xoffset, const Real &yoffset,
			const Integer &iindex, const Integer &findex,
			const Integer &transp)
                  - "Routine to add a racetrack aperture node set ";
  Void addMomentumApertureSet(const Real &dp_pMax, const Integer &calcFreq, 
			const Integer &iindex, const Integer &findex)
                  - "Routine to add a momentum aperture node set ";
  Void showApertures(Ostream &os) 
                  - "Routine to print the aperture setup to a stream";
  Void dumpTurnHist(const Integer &n, Ostream &os)
		  - "Routine to dump histogram values of number of particles"
		  - "hitting all apertures for every turn";
  Void dumpApertureHits(const Integer &n, Ostream &os)
		  - "Routine to dump histogram values of number of particles"
		  - "hitting each aperture node";
  Void binImpact(const Integer &n, Ostream &sout, const Integer &numbins)
		  - "Routine create histogram of x and y impact parameters at"
                  - "aperture";

public:

  // parallel stuff  ==MPI==  ==start===
  Integer
    iMPIini       - "inf. about MPI 0-non initialized 1-initialized";
  Integer
    nMPIrank      - "rank of this CPU";
  Integer
    nMPIsize      - "number of CPUs for Parallel run";
  // parallel stuff  ==MPI==  ==stop===

}







