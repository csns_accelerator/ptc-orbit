/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   LorentzTracker.mod
//
// AUTHOR
//   Jeff Holmes
//
// CREATED
//   03/03/2009
//
// MODIFIED
//
// DESCRIPTION
//   Module for tracking through time-dependent electromagnetic fields.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module LorentzTracker - "LorentzTracker Module."
                      - "Written by Jeff Holmes"
{

  Inherits Consts, Particles, Ring, TransMap, TeaPot;

  Errors
  badHerdNo     - "Sorry, you asked for an undefined macro Herd",
  badMADFile    - "Sorry, I can't find the MAD file you specified",
  EOFTwiss      - "Oops - I'm at the end of the Twiss file. More TMs"
                - "than !0 length twiss sets. Check MAD deck for 0"
                - "length elements",
  badNode       - "Sorry, you asked for an undefined Node",
  noNode        - "Sorry, you asked for a nonexistent Node",
  badEBFile     - "Something wrong with the file you gave";

  public:

  Integer
    nLZNodes    - "Number of LorentzTracker Nodes in the Ring";

  Real
    EBScale     - "Multiplication factor for E and B fields",
    OmegaRes    - "Resonant frequency of time-dependent field",
    PhRes       - "Phase of time-dependent field";

  Void ctor() - "Constructor for the LorentzTracker module.";

  Void nullOut(const String &n, const Integer &order,
               Integer &transmapNumber,
               Real &bx, Real &by,
               Real &ax, Real &ay,
               Real &ex, Real &epx,
               Real &length, const String &et)
               - "Routine to remove a transmap node";

  Void replaceLZ3DT(const String &n, const Integer &order,
                    const String &et,
                    const Subroutine sub,
                    const Real &zi, const Real &zf,
                    const Real &ds, const Integer &niters,
                    const Real &resid,
                    const Real &xrefi, const Real &yrefi,
                    const Real &eulerai, const Real &eulerbi,
                    const Real &eulergi)
                    - "Routine to replace transmap by LZ3DT";

  Void addLZ3DT(const String &n, const Integer &order,
                const Real &bx, const Real &by,
                const Real &ax, const Real &ay,
                const Real &ex, const Real &epx,
                const Real &l, const String &et,
                const Subroutine sub,
                const Real &zi, const Real &zf,
                const Real &ds, const Integer &niters,
                const Real &resid,
                const Real &xrefi, const Real &yrefi,
                const Real &eulerai, const Real &eulerbi,
                const Real &eulergi)
                - "Routine to add an LZ3DT";

  Void showLZ(Ostream &os)
              - "Routine to print LZ Lattice to a stream";

  Void ParseTest3DT(const String &fileName)
                    - "Routine to parse 3DT test field file";

  Void ParseMults3DT(const String &fileName,
                     const Real &zmin, const Real &zmax)
                     - "Routine to parse 3DT multipole field file";

  Void ParseGrid3DT(const String &fileName,
                    const Real &xmin, const Real &xmax,
                    const Real &ymin, const Real &ymax,
                    const Real &zmin, const Real &zmax,
                    const Integer &skipX,
                    const Integer &skipY,
                    const Integer &skipZ)
                    - "Routine to parse 3DT grid field file";

  Void EBTest3DT()
                - "Routine to provide 3DT test fields";

  Void EBMults3DT()
                 - "Routine to provide 3DT multipole fields";

  Void EBGrid3DT()
                - "Routine to provide 3DT grid fields";

}
