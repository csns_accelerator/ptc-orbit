/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   Accelerate.cc
//
// AUTHOR
//   John Galambos
//   ORNL, jdg@ornl.gov
//
// CREATED
//   12/4/98
//   08/2006 Reviewed by Jeff Holmes
//   11/2008 Modified by Dean Adams to include ISIS fixed RF rf frequency injection
// DESCRIPTION
//   Module descriptor file for the Accelerate module. This module
//   contains source for the Accelerate related info.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Accelerate.h"
#include "Node.h"
#include "MacroPart.h"
#include "RealMat.h"
#include "StreamType.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include "DiagClass.h"

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

extern Array(ObjectPtr) syncP;
Array(ObjectPtr) RFPointers;
extern Array(ObjectPtr) mParts, nodes;
Integer nRFH;
Real deltaEA, deltaECharge;

static const char *accelHeader1 =
" Turn    Time   n-Macros  T-Sync   B_f   beta    phase  Volts  w_sync E_bck  E_bnch  A_bck  A_bnch dp/p  dp/p\n";

static const char *accelHeader2 =
"         (msec)           (GeV)                  (deg)   (kV)   (Hz)  (MeV)  (MeV)  (eV-s)  (eV-s) max-%  RMS-%\n";

static const char *accelLine =
" ----    ------   -----  ------   -----   -----   -----  -----  -----  ----  ------  -----  -----  -----  -----\n";

///////////////////////////////////////////////////////////////////////////
//
// Local routines:
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   rtbi
//
// DESCRIPTION
//   Finds the zero (i.e. root) of a function of one variable,
//   within prescribed bounds. The function must have a single Real
//   argument, which is the variable.
//
//   Taken from Numerical Recipes by Press, Teukolsky, Flannery, and
//   Vetterling. See secttion 9.1
//
// PARAMETERS
//   x_res = The root of the function
//   func  = The function to evaluate
//   x1    = lower bound
//   x2    = upper bound
//   tol   = relative tolerance for solution
//
// RETURNS
//   1 if successful
//   0 otherwise
//
///////////////////////////////////////////////////////////////////////////
#define JMAX 40

Real rtbi(Real &x_res, Real func(Real &), Real &x1, Real &x2, Real &tol)
{
  Integer j;
  Real xh, fh, xl, fl, xmid, fmid, dx, rtb;

  xh = x2;
  fh = func(xh);
  xl = x1;
  fl = func(xl);
  if(fh * fl > 0.) return 0.; // fail
  if(Abs(fh) < Consts::tiny)
  {
    x_res = xh;
    return 1.0;
  }
  if(Abs(fl) < Consts::tiny)
  {
    x_res = xl;
    return 1.0;
  }

  for(j = 0; j < JMAX; j++)
  {
    dx = xh - xl;
    xmid = 0.5 * (xh + xl);
    fmid = func(xmid);
    rtb = xmid;
    if((Abs(dx) < tol) || (Abs(fmid) < Consts::tiny))
    {
      x_res = rtb;
      return 1.0;
    }
    if(fmid * fh < 0.0)
    {
      xl = xmid;
      fl = fmid;
    }
    else if(fmid * fl < 0.0)
    {
      xh = xmid;
      fh = fmid;
    }
  }
  return 0.;  // fail
}

Real dEResid(Real &sp)
// use global data here to work around egcs 1.02 problem
{
  Integer i;
  Real dESum = 0.;

  for(i = 1; i <= nRFH; i++)
  {
    dESum += deltaECharge * 1.e-06 * Accelerate::RFVolts(i) *
             sin(Accelerate::RFHarmNum(i) * sp + Accelerate::RFPhase(i));
  }
  return  deltaEA - dESum;
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   AccelerateBase
//
// INHERITANCE RELATIONSHIPS
//   AccelerateBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a base class for storing Accelerate information.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   _BRho
//   _findAccelParms
//   _showAccelerate - "Routine to print out the Accelerate parameters"
//
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class AccelerateBase : public Node
{
  Declare_Standard_Members(AccelerateBase, Node);
  public:
    AccelerateBase(const String &n, const Integer &order,
                   const Subroutine &sub):
                   Node(n, 0., order), _findAccelParms(sub)
    {
    }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Void _showAccelerate(Ostream &sout)=0;
    const Subroutine _findAccelParms;
    Real _BRho, _deltaEAccel;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS AccelerateBase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(AccelerateBase, Node);

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   RampBAccel
//
// INHERITANCE RELATIONSHIPS
//   RampBAccel -> AccelerateBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class for doing the acceleration based on an input of the
//   dipole B field (time).
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class RampBAccel : public AccelerateBase
{
  Declare_Standard_Members(RampBAccel, AccelerateBase);
  public:
  RampBAccel(const String &name, const Integer &order,
             const Subroutine &sub,
             const Integer &nh, Vector(Real) &RFVolts,
             Vector(Real) &RFHarmNum, Vector(Real) &RFPhase):
             AccelerateBase(name, order, sub),
             _nRFHarmonics(nh), _RFVolts(RFVolts),
             _RFHarmNum(RFHarmNum), _RFPhase(RFPhase)
  {
    _nRFHarmonics = nh;
    _RampBAccel_Init = 0;
  }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showAccelerate(Ostream &sout);
    Real _BSynch;
    Real _gammaOld;
    Real _betaOld;
    Integer _nRFHarmonics;
    Integer _RampBAccel_Init;
    Vector(Real) &_RFVolts, &_RFHarmNum, &_RFPhase;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS RampBAccel
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(RampBAccel, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   RampBAccel::NodeCalculator
//
// DESCRIPTION
//   Get the updated B field value.
//   Set up the sync part parameters based on the new B field.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void RampBAccel::_nodeCalculator(MacroPart &mp)
{

  if((mp._herdName != "mainHerd") && (_RampBAccel_Init == 1)) return;

  Real minGuess, maxGuess;
  Real tol = 1.e-04;
  Integer i;

  _findAccelParms();  // find _BRho & RF values
  _BSynch = Accelerate::BSynch;
  for(i = 1; i <= _nRFHarmonics; i++)
  {
    _RFVolts(i)   = Accelerate::RFVolts(i);
    _RFHarmNum(i) = Accelerate::RFHarmNum(i);
    _RFPhase(i)   = Accelerate::RFPhase(i);
  }

// Data for voltage are initialized
// This will work only first time - to fix the call to Ring::calcBucket

  _RampBAccel_Init = 1;

// set new synch part energy:

  _gammaOld = mp._syncPart._gammaSync;
  _betaOld = mp._syncPart._betaSync;
  Real EOld = mp._syncPart._eTotal;

  Real pMomentum;
  Real eTotal;
  Real gammaSync;
  Real betaSync;
  Real eKinetic;
  Real dppFac;

  if(Accelerate::AccelChoice == 0)
  {
    _BRho =  Accelerate::rhoBend * _BSynch;
    pMomentum = 1.e-09 * Consts::vLight * _BRho * mp._syncPart._charge;
    eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pMomentum));
    gammaSync = eTotal / mp._syncPart._e0;
    betaSync = Sqrt(1. - 1. / Sqr(gammaSync));
    eKinetic = eTotal - mp._syncPart._e0;
    dppFac = 1. / (Sqr(betaSync) * eTotal);
    _deltaEAccel = eTotal - EOld;
  }
  if(Accelerate::AccelChoice == 1)
  {
    _deltaEAccel = 0.0;
    for(i = 1; i <= _nRFHarmonics; i++)
    {
      _deltaEAccel += _RFVolts(i) * sin(_RFPhase(i));
    }
    _deltaEAccel *= mp._syncPart._charge * 1.e-06;
    eTotal = EOld + _deltaEAccel;
    pMomentum = Sqrt(Sqr(eTotal) - Sqr(mp._syncPart._e0));
    _BRho =  pMomentum / (1.e-09 * Consts::vLight * mp._syncPart._charge);
    gammaSync = eTotal / mp._syncPart._e0;
    betaSync = Sqrt(1. - 1. / Sqr(gammaSync));
    eKinetic = eTotal - mp._syncPart._e0;
    dppFac = 1. / (Sqr(betaSync) * eTotal);
  }

  mp._syncPart._eTotal = eTotal;
  mp._syncPart._gammaSync = gammaSync;
  mp._syncPart._betaSync = betaSync;
  mp._syncPart._eKinetic = eKinetic;
  mp._syncPart._dppFac = dppFac;

  if(Ring::gammaTrans < 0.0)
  {
    Ring::etaCompaction = -Sqr(1. / Ring::gammaTrans) -
                           Sqr(1. / mp._syncPart._gammaSync);
  }
  else
  {
    Ring::etaCompaction = Sqr(1. / Ring::gammaTrans) -
                          Sqr(1. / mp._syncPart._gammaSync);
  }

  if(Accelerate::AccelChoice == 1)
  {
      mp._syncPart._phase = 0.;
      return;
  }

  // find the sync phase required to give _deltaEAccel:
 
  Integer maxHarmInd = 1;

  double phase_max = Accelerate::RFPhase(maxHarmInd);
  double harm_max = Accelerate::RFHarmNum(maxHarmInd);

  minGuess = -Consts::pi/(2*harm_max);
  maxGuess = Consts::pi/(2*harm_max);

  // work around to get dEResid right

  nRFH = _nRFHarmonics;
  deltaEA = _deltaEAccel;
  deltaECharge = mp._syncPart._charge;

  // fine tune the guess range

  int nSteps = 200;
  Real step = (maxGuess - minGuess)/nSteps; 
  Real x0 = 0.0;
  Real x1 = 0.0;	
  Real f0 = 0.0;
  Real f1 = 0.0;
  Real x_start = minGuess;
  Real x_min = maxGuess;
  for(i = 0; i < nSteps; i++)
  {
    x0 = x_start + i*step;
    x1 = x0 + step;
    f0 = dEResid(x0);
    f1 = dEResid(x1);
    if(f0 * f1 <= 0.)
    {
      if(Abs(x1) < x_min)
      {
	minGuess = x0;
	maxGuess = x1;
	x_min = Abs(x1);
      }
    }
  }

  Real phase = 0.0;
  if((Abs(dEResid(phase)) < Consts::tiny) &&
     (phase < maxGuess) && (phase> minGuess))
  {
  }
  else
  {
    if(rtbi(phase, dEResid, minGuess, maxGuess, tol) == 0.)
    {
      _nodeError = 1;
      _errMessage = "Bad prescribed voltage / B-field combo in Acceleration";
      return;
    }
  }
  mp._syncPart._phase = phase;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   RampBAccel::updatePartAtNode
//
// DESCRIPTION
//   Update the individual macroparticle relative energy and phases
//   to the new  synch. part energy and phase.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void RampBAccel::_updatePartAtNode(MacroPart& mp)
{
  Real dESum, deltaERF;
  Integer i, j;

  for(j = 1; j <= mp._nMacros; j++)
  {
    if(Accelerate::PhaseChoice == 1) mp._phi(j) += mp._syncPart._phase;
    dESum = 0.;
    for(i = 1; i <= _nRFHarmonics; i++)
    {
      dESum += _RFVolts(i) * sin(_RFHarmNum(i) * mp._phi(j) + _RFPhase(i));
    }
    deltaERF = mp._syncPart._charge * 1.e-06 * dESum;
    if(Accelerate::PhaseChoice == 1) mp._phi(j) -= mp._syncPart._phase;

    mp._deltaE(j) += deltaERF - _deltaEAccel;
    mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

    if(Ring::longTrackOnly)
    {
      mp._phi(j) += Ring::harmonicNumber * Consts::twoPi *
                    Ring::etaCompaction * mp._dp_p(j) / Ring::nLongUpdates;
    }
    else
    {
      mp._xp(j) *= _gammaOld * _betaOld /
                   (mp._syncPart._gammaSync * mp._syncPart._betaSync);
      mp._yp(j) *= _gammaOld * _betaOld /
                   (mp._syncPart._gammaSync * mp._syncPart._betaSync);
    }

    if(mp._phi(j) >  Consts::pi) mp._phi(j) -= Consts::twoPi;
    if(mp._phi(j) < -Consts::pi) mp._phi(j) += Consts::twoPi;
  }
}

Void RampBAccel::_showAccelerate(Ostream &sout)
{
//jdg set up output
}



///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   ISISRampBAccel
//
// INHERITANCE RELATIONSHIPS
//   ISISRampBAccel -> AccelerateBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class for doing the acceleration based on an input of the
//   dipole B field (time).  If the field is falling then the rf frequency is assumed fixed
//   This is to simulate the ISIS injection system
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class ISISRampBAccel : public AccelerateBase
{
  Declare_Standard_Members(ISISRampBAccel, AccelerateBase);
  public:
  ISISRampBAccel(const String &name, const Integer &order,
             const Subroutine &sub,
             const Integer &nh, Vector(Real) &RFVolts,
             Vector(Real) &RFHarmNum, Vector(Real) &RFPhase):
             AccelerateBase(name, order, sub),
             _nRFHarmonics(nh), _RFVolts(RFVolts),
             _RFHarmNum(RFHarmNum), _RFPhase(RFPhase)
  {
    _nRFHarmonics = nh;
    _ISISRampBAccel_Init = 0;
  }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showAccelerate(Ostream &sout);
    Real _BSynch;
    Real _gammaOld;
    Real _betaOld;
    Integer _nRFHarmonics;
    Integer _ISISRampBAccel_Init;
    Vector(Real) &_RFVolts, &_RFHarmNum, &_RFPhase;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS RampBAccel
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(ISISRampBAccel, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   ISISRampBAccel::NodeCalculator
//
// DESCRIPTION
//   Get the updated B field value.
//   Set up the sync part parameters based on the new B field.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void ISISRampBAccel::_nodeCalculator(MacroPart &mp)
{

  //cout << "inside ISISRampBAccel" << "\n";


  if((mp._herdName != "mainHerd") && (_ISISRampBAccel_Init == 1)) return;

  Real minGuess, maxGuess;
  Real tol = 1.e-04;
  Integer i;

  _findAccelParms();  // find _BRho & RF values
  _BSynch = Accelerate::BSynch;
  for(i = 1; i <= _nRFHarmonics; i++)
  {
    _RFVolts(i)   = Accelerate::RFVolts(i);
    _RFHarmNum(i) = Accelerate::RFHarmNum(i);
    _RFPhase(i)   = Accelerate::RFPhase(i);
  }

// Data for voltage are initialized
// This will work only first time - to fix the call to Ring::calcBucket

  _ISISRampBAccel_Init = 1;

// set new synch part energy:

  _gammaOld = mp._syncPart._gammaSync;
  _betaOld = mp._syncPart._betaSync;
  Real EOld = mp._syncPart._eTotal;

  Real pMomentum;
  Real eTotal;
  Real gammaSync;
  Real betaSync;
  Real eKinetic;
  Real dppFac;

  /*
    cout << " _gammaOld = " << _gammaOld << "\n";
    cout << " _betaOld = " << _betaOld << "\n";
    cout << " Eold = " << EOld << "\n";
  */

  if(Accelerate::AccelChoice == 0)
  {
    _BRho =  Accelerate::rhoBend * _BSynch;
    pMomentum = 1.e-09 * Consts::vLight * _BRho * mp._syncPart._charge;
    eTotal = Sqrt(Sqr(mp._syncPart._e0) + Sqr(pMomentum));
    gammaSync = eTotal / mp._syncPart._e0;
    betaSync = Sqrt(1. - 1. / Sqr(gammaSync));
    eKinetic = eTotal - mp._syncPart._e0;
    dppFac = 1. / (Sqr(betaSync) * eTotal);
    _deltaEAccel = eTotal - EOld;

    
    //cout << " _BRho = " << _BRho << "\n";
    //cout << " _BSynch = " << _BSynch << "\n";
    //cout << " eTotal = " << eTotal << "\n";
    //cout << " betaSync = " << betaSync << "\n";
    //cout << " eKinetic = " << eKinetic << "\n";
    //cout << " _deltaEAccel = " << _deltaEAccel << "\n";
    //cout << " Accelerate::FixedInjPeriod = " << Accelerate::FixedInjPeriod << "\n";
    

  }

  if(Accelerate::AccelChoice == 1)
  {
    _deltaEAccel = 0.0;
    for(i = 1; i <= _nRFHarmonics; i++)
    {
      _deltaEAccel += _RFVolts(i) * sin(_RFPhase(i));
    }
    _deltaEAccel *= mp._syncPart._charge * 1.e-06;
    eTotal = EOld + _deltaEAccel;
    pMomentum = Sqrt(Sqr(eTotal) - Sqr(mp._syncPart._e0));
    _BRho =  pMomentum / (1.e-09 * Consts::vLight * mp._syncPart._charge);
    gammaSync = eTotal / mp._syncPart._e0;
    betaSync = Sqrt(1. - 1. / Sqr(gammaSync));
    eKinetic = eTotal - mp._syncPart._e0;
    dppFac = 1. / (Sqr(betaSync) * eTotal);
  }


  mp._syncPart._eTotal = eTotal;
  mp._syncPart._gammaSync = gammaSync;
  mp._syncPart._betaSync = betaSync;
  mp._syncPart._eKinetic = eKinetic;
  mp._syncPart._dppFac = dppFac;

  if(Ring::gammaTrans < 0.0)
  {
    Ring::etaCompaction = -Sqr(1. / Ring::gammaTrans) -
                           Sqr(1. / mp._syncPart._gammaSync);
  }
  else
  {
    Ring::etaCompaction = Sqr(1. / Ring::gammaTrans) -
                          Sqr(1. / mp._syncPart._gammaSync);
  }

  if(Accelerate::AccelChoice == 1)
  {
      mp._syncPart._phase = 0.;
      return;
  }

  // find the sync phase required to give _deltaEAccel:
 
  if (_deltaEAccel < 0.0 ) 
    {
       mp._syncPart._phase =0.0;          
             
       Real syncRevTime= Ring::lRing/(Consts::vLight*betaSync);
       Real Phaseslip=((Accelerate::FixedInjPeriod-syncRevTime)/Accelerate::FixedInjPeriod)*
	              2*Consts::pi*Ring::harmonicNumber;
       Real deltaSyncPhase=(-1.0)*Phaseslip;
       //cout << "phase slip = " << deltaSyncPhase << "\n";
       //update particle phases
       for (Integer j=1;j<=mp._nMacros;j++) 
       {
        mp._phi(j) += deltaSyncPhase;
       }
       return;

    }

  // it is assumed that _deltaEAccel is positive for the following code.


  Integer maxHarmInd = 1;

  double phase_max = Accelerate::RFPhase(maxHarmInd);
  double harm_max = Accelerate::RFHarmNum(maxHarmInd);

  minGuess = -Consts::pi/(2*harm_max);
  maxGuess = Consts::pi/(2*harm_max);

  // work around to get dEResid right

  nRFH = _nRFHarmonics;
  deltaEA = _deltaEAccel;
  deltaECharge = mp._syncPart._charge;

  // fine tune the guess range

  int nSteps = 200;
  Real step = (maxGuess - minGuess)/nSteps; 
  Real x0 = 0.0;
  Real x1 = 0.0;	
  Real f0 = 0.0;
  Real f1 = 0.0;
  Real x_start = minGuess;
  Real x_min = maxGuess;
  for(i = 0; i < nSteps; i++)
  {
    x0 = x_start + i*step;
    x1 = x0 + step;
    f0 = dEResid(x0);
    f1 = dEResid(x1);
    if(f0 * f1 <= 0.)
    {
      if(Abs(x1) < x_min)
      {
	minGuess = x0;
	maxGuess = x1;
	x_min = Abs(x1);
      }
    }
  }

  Real phase = 0.0;
  if((Abs(dEResid(phase)) < Consts::tiny) &&
     (phase < maxGuess) && (phase> minGuess))
  {
  }
  else
  {
    if(rtbi(phase, dEResid, minGuess, maxGuess, tol) == 0.)
    {
      _nodeError = 1;
      _errMessage = "Bad prescribed voltage / B-field combo in Acceleration";
      return;
    }
  }
  mp._syncPart._phase = phase;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   ISISRampBAccel::updatePartAtNode
//
// DESCRIPTION
//   Update the individual macroparticle relative energy and phases
//   to the new  synch. part energy and phase.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void ISISRampBAccel::_updatePartAtNode(MacroPart& mp)
{
  Real dESum, deltaERF;
  Integer i, j;

  //cout << "Volts = " << _RFVolts(1) << "\n";
  //cout << "Phase = " << _RFPhase(1) << "\n";


  for(j = 1; j <= mp._nMacros; j++)
  {
    if(Accelerate::PhaseChoice == 1) mp._phi(j) += mp._syncPart._phase;
    dESum = 0.;
    for(i = 1; i <= _nRFHarmonics; i++)
    {
      dESum += _RFVolts(i) * sin(_RFHarmNum(i) * mp._phi(j) + _RFPhase(i));
    }
    deltaERF = mp._syncPart._charge * 1.e-06 * dESum;
    if(Accelerate::PhaseChoice == 1) mp._phi(j) -= mp._syncPart._phase;

    mp._deltaE(j) += deltaERF - _deltaEAccel;
    mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

    if(Ring::longTrackOnly)
    {
      mp._phi(j) += Ring::harmonicNumber * Consts::twoPi *
                    Ring::etaCompaction * mp._dp_p(j) / Ring::nLongUpdates;
    }
    else
    {
      mp._xp(j) *= _gammaOld * _betaOld /
                   (mp._syncPart._gammaSync * mp._syncPart._betaSync);
      mp._yp(j) *= _gammaOld * _betaOld /
                   (mp._syncPart._gammaSync * mp._syncPart._betaSync);
    }

    if(mp._phi(j) >  Consts::pi) mp._phi(j) -= Consts::twoPi;
    if(mp._phi(j) < -Consts::pi) mp._phi(j) += Consts::twoPi;
  }

}

Void ISISRampBAccel::_showAccelerate(Ostream &sout)
{
//jdg set up output
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   AccelbyPhase
//
// INHERITANCE RELATIONSHIPS
//   AccelbyPhase -> AccelerateBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class for doing the acceleration based on an input of the
//   synchronous phase vs time.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class AccelbyPhase : public AccelerateBase
{
  Declare_Standard_Members(AccelbyPhase, AccelerateBase);
  public:
  AccelbyPhase(const String &name, const Integer &order,
               const Subroutine &sub, const String &table):
               AccelerateBase(name, order, sub)
  {
    _AccelbyPhase_Init = 0;
    _readPoints(table);
  }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showAccelerate(Ostream &sout);
    Integer _AccelbyPhase_Init;
    Void _readPoints(const String &fileName);
    Integer _nTimePoints, _nRFHarmonics;
    Vector(Real) _timePoints, _syncPhasePoints, _RFHarmNum;
    Matrix(Real) _RFVoltPoints, _RFPhasePoints;
    Void _interpolate(Void);
    Vector(Real) _RFVolts, _RFPhase;
    Real _syncPhase, _BSynch;
    Real _gammaOld;
    Real _betaOld;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS AccelbyPhase
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(AccelbyPhase, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   AccelbyPhase::NodeCalculator
//
// DESCRIPTION
//   Get the updated B field value.
//   Set up the sync part parameters based on the new B field.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void AccelbyPhase::_nodeCalculator(MacroPart &mp)
{

  if((mp._herdName != "mainHerd") && (_AccelbyPhase_Init == 1)) return;

  _interpolate();

// Phase and voltage data are initialized
// This will work only first time - to fix the call to Ring::calcBucket

  _AccelbyPhase_Init = 1;

// set new synch part energy:

  _gammaOld = mp._syncPart._gammaSync;
  _betaOld = mp._syncPart._betaSync;
  Real EOld = mp._syncPart._eTotal;

  Real pMomentum;
  Real eTotal;
  Real gammaSync;
  Real betaSync;
  Real eKinetic;
  Real dppFac;

  _deltaEAccel = 0.0;
  for(int nH = 1; nH <= _nRFHarmonics; nH++)
  {
    _deltaEAccel += _RFVolts(nH) *
                    sin(_RFHarmNum(nH) * _syncPhase + _RFPhase(nH));
  }
  _deltaEAccel *= mp._syncPart._charge * 1.e-06;
  eTotal = EOld + _deltaEAccel;
  pMomentum = Sqrt(Sqr(eTotal) - Sqr(mp._syncPart._e0));
  _BRho =  pMomentum / (1.e-09 * Consts::vLight * mp._syncPart._charge);
  gammaSync = eTotal / mp._syncPart._e0;
  betaSync = Sqrt(1. - 1. / Sqr(gammaSync));
  eKinetic = eTotal - mp._syncPart._e0;
  dppFac = 1. / (Sqr(betaSync) * eTotal);
  _BSynch = _BRho / Accelerate::rhoBend;

  mp._syncPart._eTotal = eTotal;
  mp._syncPart._gammaSync = gammaSync;
  mp._syncPart._betaSync = betaSync;
  mp._syncPart._eKinetic = eKinetic;
  mp._syncPart._dppFac = dppFac;
  mp._syncPart._phase = _syncPhase;

  if(Ring::gammaTrans < 0.0)
  {
    Ring::etaCompaction = -Sqr(1. / Ring::gammaTrans) -
                           Sqr(1. / mp._syncPart._gammaSync);
  }
  else
  {
    Ring::etaCompaction = Sqr(1. / Ring::gammaTrans) -
                          Sqr(1. / mp._syncPart._gammaSync);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   AccelbyPhase::updatePartAtNode
//
// DESCRIPTION
//   Update the individual macroparticle relative energy and phases
//   to the new  synch. part energy and phase.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void AccelbyPhase::_updatePartAtNode(MacroPart& mp)
{
  Real dESum, deltaERF;
  Integer nH, j;

  for(j = 1; j <= mp._nMacros; j++)
  {
    dESum = 0.;
    for(nH = 1; nH <= _nRFHarmonics; nH++)
    {
      dESum += _RFVolts(nH) * sin(_RFHarmNum(nH) * mp._phi(j) + _RFPhase(nH));
    }
    deltaERF = mp._syncPart._charge * 1.e-06 * dESum;

    mp._deltaE(j) += deltaERF - _deltaEAccel;
    mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

    if(Ring::longTrackOnly)
    {
      mp._phi(j) += Ring::harmonicNumber * Consts::twoPi *
                    Ring::etaCompaction * mp._dp_p(j) / Ring::nLongUpdates;
    }
    else
    {
      mp._xp(j) *= _gammaOld * _betaOld /
                   (mp._syncPart._gammaSync * mp._syncPart._betaSync);
      mp._yp(j) *= _gammaOld * _betaOld /
                   (mp._syncPart._gammaSync * mp._syncPart._betaSync);
    }

    if(mp._phi(j) >  Consts::pi) mp._phi(j) -= Consts::twoPi;
    if(mp._phi(j) < -Consts::pi) mp._phi(j) += Consts::twoPi;
  }
}

Void AccelbyPhase::_showAccelerate(Ostream &sout)
{
//jdg set up output
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   AccelbyPhase::_readPoints
//
// DESCRIPTION
//   Gets synchronous phase and voltage time wave forms from a file.
//   The first line is the number of RF harmonics.
//   The next line lists the harmonic numbers, with data separated
//   by whitespace. The rest of the file
//   data is expected to be in the following form (one data set / line),
//   again with data separated by whitespace:
//
//   time [msec], Synchronous Phase [deg],
//                       Volt(1) [kV], Phase(1) [deg], ...
//                       Volt(n) [kV], Phase(n) [deg]
//   where n is the number of RF harmonics and Volts is the ith harmonic
//   voltage, and phase is the absolute phase of the harmonic.
//
// PARAMETERS
//   fileName - The name of the file to open
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void AccelbyPhase::_readPoints(const String &fileName)
{
  Integer nH;
  IFstream fio(fileName, ios::in);
  if(fio.good() != 1)
  {
    cout << "/n/n" << "AccelbyPhase::_readPoints -> Bad File Name"
         << "/n/n";
    return;
  }

  _nTimePoints = 0;
  fio >> _nRFHarmonics;
  _RFHarmNum.resize(_nRFHarmonics);
  _RFVolts.resize(_nRFHarmonics);
  _RFPhase.resize(_nRFHarmonics);

  for(nH = 1; nH <= _nRFHarmonics; nH++)
  {
    fio  >> _RFHarmNum(nH);
  }

  Real timeDummy;
  while(!fio.eof())
  {
    if(fio >> timeDummy)
    {
      _nTimePoints ++;
      _timePoints.resize(_nTimePoints);
      _syncPhasePoints.resize(_nTimePoints);
      _RFVoltPoints.resize(_nRFHarmonics, _nTimePoints);
      _RFPhasePoints.resize(_nRFHarmonics, _nTimePoints);

      _timePoints(_nTimePoints) = timeDummy;
      fio >> _syncPhasePoints(_nTimePoints);
      for(nH = 1; nH <= _nRFHarmonics; nH++)
        fio >> _RFVoltPoints(nH, _nTimePoints)
            >> _RFPhasePoints(nH,_nTimePoints);
    }
  }
    fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   AccelbyPhase::_interpolate
//
// DESCRIPTION
//   Gets synchronous particle phase and RF voltages and phases
//   by interpolation
//
// PARAMETERS
//   None
//
// RETURNS
//   Particle phase and RF voltages and phases at given time
//
///////////////////////////////////////////////////////////////////////////

Void AccelbyPhase::_interpolate()
{
  if((Ring::time < _timePoints(1)) ||
     (Ring::time > _timePoints(_nTimePoints)))
  {
    cout << "/n/n" << "AccelbyPhase::_interpolate -> Time out of range"
         << "/n/n";
    return;
  }

  Integer i = 0;
  while((Ring::time - _timePoints(i + 1)) >= 0.0) i++;

  if(i < 1) i = 1;
  if(i > _nTimePoints - 1) i = _nTimePoints - 1;

  Real frac = (Ring::time - _timePoints(i)) /
              (_timePoints(i + 1) - _timePoints(i));

  _syncPhase = (_syncPhasePoints(i) +
		frac * (_syncPhasePoints(i + 1) - _syncPhasePoints(i)))
                * Consts::pi/180.;

  for(Integer nH = 1; nH <= _nRFHarmonics; nH++)
  {
    _RFVolts(nH) = _RFVoltPoints(nH, i) +
                   frac * (_RFVoltPoints(nH, i + 1) - _RFVoltPoints(nH, i));
    _RFPhase(nH) = (_RFPhasePoints(nH, i) +
		    frac * (_RFPhasePoints(nH, i + 1) - _RFPhasePoints(nH, i)))
	           * Consts::pi/180.;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   RFCav
//
// INHERITANCE RELATIONSHIPS
//   RFCav -> AccelerateBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class for storing non accelerating RF Cavity information.
//   Each cavity can have an independent set of voltages components.
//
// PUBLIC MEMBERS
//   Integer:
//     &_nHarm    Reference to the number of harmonics to use
//   Real Vector:
//     &_voltage  Reference to Peak RF voltage vector [kV]
//     &_harmNum  Reference to the vector containing the harmonic number
//     &_phase    Reference to the vector containg the phase [rad]
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class RFCav : public AccelerateBase
{
  Declare_Standard_Members(RFCav, AccelerateBase);
  public:
  RFCav(const String &n, const Integer &order,
        Integer &nHarms, Vector(Real) &voltage,
	Vector(Real) &harmNum, Vector(Real) &phase, Subroutine sub):
        AccelerateBase(n, order, sub), _nHarms(nHarms), _voltage(voltage),
                                       _harmNum(harmNum), _phase(phase)
  {
  }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);
    virtual Void _showAccelerate(Ostream &sout);

    Integer &_nHarms;
    Vector(Real) &_voltage;
    Vector(Real) &_harmNum;
    Vector(Real) &_phase;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS RFCav
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(RFCav, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   RFCav::NodeCalculator
//
// DESCRIPTION
//   Calls routine to (optionally)
//   set the RF voltage / frequencies as a function of time.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void RFCav::_nodeCalculator(MacroPart &mp)
{
  _findAccelParms();

  Integer i;
  if(Accelerate::nTimePoints > 0)
  {
    for(i = 1; i <= Accelerate::nRFHarmonics; i++)
    {
      _voltage(i) = Accelerate::RFVolts(i);
      _phase(i) = Accelerate::RFPhase(i);
    }
  }

  if(Ring::gammaTrans < 0.0)
  {
    Ring::etaCompaction =
      -Sqr(1. / Ring::gammaTrans) - Sqr(1. / mp._syncPart._gammaSync);
  }
  else
  {
    Ring::etaCompaction =
       Sqr(1. / Ring::gammaTrans) - Sqr(1. / mp._syncPart._gammaSync);
  }
}

Void RFCav::_showAccelerate(Ostream &os)
{
  // add some stuff:
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   RFCav::updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle with an RFCav
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void RFCav::_updatePartAtNode(MacroPart& mp)
{
  Real volts;
  Integer i, j;
  Real deltaERF, phase0, phasej;

  phase0 = 0.0;
  if(Abs(Accelerate::deltaFreq) > Consts::tiny)
  {
    phase0 = Consts::twoPi * Accelerate::deltaFreq * mp._nTurnsDone;
  }

  for(j = 1; j <= mp._nMacros; j++)
  {
    phasej = mp._phi(j);
    if(Abs(Accelerate::deltaFreq) > Consts::tiny)
      phasej = phase0 + (1.0 + Accelerate::deltaFreq) * mp._phi(j);
    volts = 0.;

    if(_nHarms >= 1)
    {
      for(i = 1; i <= _nHarms; i++)
      {
        volts += _voltage(i) *
                 sin(_harmNum(i) * phasej + _phase(i));
      }
      deltaERF = mp._syncPart._charge * 1.e-06 * volts;
      mp._deltaE(j) += deltaERF;
    }
    else if(_nHarms == -100)
    {
      volts += _voltage(1) * (_harmNum(1) * phasej + _phase(1));
      deltaERF = mp._syncPart._charge * 1.e-06 * volts;
      mp._deltaE(j) += deltaERF;
    }
    else if(_nHarms == 0)
    {
      if(phasej <= _phase(1)) mp._deltaE(j) = -mp._deltaE(j);
      if(phasej >= _phase(2)) mp._deltaE(j) = -mp._deltaE(j);
    }

    mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

    if(Ring::longTrackOnly)
    {
      mp._phi(j) += Ring::harmonicNumber * Consts::twoPi *
                    Ring::etaCompaction * mp._dp_p(j) /
                    Ring::nLongUpdates;

      if(mp._phi(j) >  Consts::pi) mp._phi(j) -= Consts::twoPi;
      if(mp._phi(j) < -Consts::pi) mp._phi(j) += Consts::twoPi;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   BarrierCav
//
// INHERITANCE RELATIONSHIPS
//   BarrierCav -> AccelerateBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class for storing RF Barrier Cavity information.
//   Each cavity can have an independent set of voltage parameters.
//
// PUBLIC MEMBERS
//   Integer:
//     &_nHarm    Reference to the number of harmonics to use
//   Real Vector:
//     &_voltage  Reference to Peak RF voltage vector [kV].
//                The sign is important, and flips at opposite ends of
//                the bunch.
//     &_harmNum  Reference to the vector containing the harmonic number
//     &_phase    Reference to the vector containing the barrier center [deg]
//     &_dphase   Reference to the vector containing the barrier width [deg]
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class BarrierCav : public AccelerateBase
{
  Declare_Standard_Members(BarrierCav, AccelerateBase);

  public:
  BarrierCav(const String &n, const Integer &order,
             Integer &nHarms, Vector(Real) &voltage,
	     Vector(Real) &harmNum, Vector(Real) &phase,
             Vector(Real) &dphase,
             Subroutine sub):
             AccelerateBase(n, order, sub),
             _nHarms(nHarms), _voltage(voltage),
             _harmNum(harmNum), _phase(phase),
             _dphase(dphase)
  {
  }

  virtual Void nameOut(String &wname) { wname = _name; }
  virtual Void _updatePartAtNode(MacroPart &mp);
  virtual Void _nodeCalculator(MacroPart &mp);
  virtual Void _showAccelerate(Ostream &sout);

  Integer &_nHarms;
  Vector(Real) &_voltage;
  Vector(Real) &_harmNum;
  Vector(Real) &_phase;
  Vector(Real) &_dphase;
};

///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS BarrierCav
//
///////////////////////////////////////////////////////////////////////////

Define_Standard_Members(BarrierCav, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   BarrierCav::NodeCalculator
//
// DESCRIPTION
//   Calls routine to (optionally)
//   set the RF voltage / frequencies as a function of time
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BarrierCav::_nodeCalculator(MacroPart &mp)
{
  _findAccelParms();

  if(Ring::gammaTrans < 0.0)
  {
    Ring::etaCompaction =
      -Sqr(1. / Ring::gammaTrans) - Sqr(1. / mp._syncPart._gammaSync);
  }
  else
  {
    Ring::etaCompaction =
       Sqr(1. / Ring::gammaTrans) - Sqr(1. / mp._syncPart._gammaSync);
  }
}

Void BarrierCav::_showAccelerate(Ostream &os)
{
// add some stuff:
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   BarrierCav::updatePartAtNode
//
// DESCRIPTION
//   Calls the specified local calculator for an operation on
//   a MacroParticle in a BarrierCav
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void BarrierCav::_updatePartAtNode(MacroPart& mp)
{

  Real volts, phrad, dphrad, phradp, phpp, phpm, phradm, phmp, phmm;
  Integer i, j, on;

  for(j = 1; j <= mp._nMacros; j++)
  {
    volts = 0.;

    for(i = 1; i <= _nHarms; i++)
    {
      phrad  =  _phase(i) * Consts::pi / 180.0;
      dphrad = _dphase(i) * Consts::pi / 180.0;

      if(phrad >= 0.0)
      {
        phradp = phrad;
        phradm = phrad - Consts::twoPi;
      }
      if(phrad <  0.0)
      {
        phradp = phrad + Consts::twoPi;
        phradm = phrad;
      }

      phpp = phradp + dphrad;
      phpm = phradp - dphrad;
      phmp = phradm + dphrad;
      phmm = phradm - dphrad;

      on = 0;
      if((mp._phi(j) > phmm) && (mp._phi(j) < phmp))
      {
        phrad = phradm;
        on = 1;
      }
      if((mp._phi(j) > phpm) && (mp._phi(j) < phpp))
      {
        phrad = phradp;
        on = 1;
      }

      if(on == 0)
      {
        volts += 0.0;
      }
      else
      {
        volts += _voltage(i) *
                 sin(_harmNum(i) * Consts::pi  *
                     (mp._phi(j) - phrad) / dphrad);
      }
    }

    mp._deltaE(j) += mp._syncPart._charge * 1.e-06 * volts;
    mp._dp_p(j) = mp._deltaE(j) * mp._syncPart._dppFac;

    if(Ring::longTrackOnly)
    {
      mp._phi(j) += Ring::harmonicNumber * Consts::twoPi *
                    Ring::etaCompaction * mp._dp_p(j) /
                    Ring::nLongUpdates;

      if(mp._phi(j) >  Consts::pi) mp._phi(j) -= Consts::twoPi;
      if(mp._phi(j) < -Consts::pi) mp._phi(j) += Consts::twoPi;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE Accelerate
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::ctor
//
// DESCRIPTION
//   Initializes the various Accelerate related constants.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::ctor()
{
// set some initial values

  nRFCavities = 0;
  nAccelerates = 0;
  BSynch = 0.;
  rhoBend = 7.;
  AccelChoice = 0;
  PhaseChoice = 0;
  etaCompaction = 0.;
  syncFreq = 0.;
  BSync0 = 0.;
  BSync1 = 0.;
  BSyncFreq = 0.;
  deltaFreq = 0.;
  bunchArea  = 0.;
  orbitFreq  = 0.;
  nRFHarmonics = 0;
  nTimePoints = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::addRampedBAccel
//
// DESCRIPTION
//   Adds a ramped B accelerating Node
//
// PARAMETERS
//   name:    Name for this node
//   order:   node order index
//   sub:     The subroutine to call to provide the synch. phase
//   nh:      Number of harmonics
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::addRampedBAccel(const String &name, const Integer &order,
                                 const Subroutine sub,
                                 const Integer &nh, Vector(Real) &RFV,
                                 Vector(Real) &harmNum, Vector(Real) &RFP)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nRFCavities == RFPointers.size())
    RFPointers.resize(nRFCavities + 1);
  nNodes++;

  nodes(nNodes - 1) = new RampBAccel(name, order, sub, nh, RFV,
                                     harmNum, RFP);
  nRFCavities++;
  nAccelerates++;
  if((nAccelerates > 1) && (AccelChoice == 0))
  {
    Accelerate::except(badAccelChoice);
  }

  RFPointers(nRFCavities - 1) = nodes(nNodes - 1);

  nodesInitialized = 0;
  Ring::nLongUpdates++;
}




///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::addISISRampedBAccel
//
// DESCRIPTION
//   Adds a ramped B accelerating Node for ISIS
//
// PARAMETERS
//   name:    Name for this node
//   order:   node order index
//   sub:     The subroutine to call to provide the synch. phase
//   nh:      Number of harmonics
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::addISISRampedBAccel(const String &name, const Integer &order,
                                 const Subroutine sub,
                                 const Integer &nh, Vector(Real) &RFV,
                                 Vector(Real) &harmNum, Vector(Real) &RFP)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nRFCavities == RFPointers.size())
    RFPointers.resize(nRFCavities + 1);
  nNodes++;

  nodes(nNodes - 1) = new ISISRampBAccel(name, order, sub, nh, RFV,
                                     harmNum, RFP);
  nRFCavities++;
  nAccelerates++;
  if((nAccelerates > 1) && (AccelChoice == 0))
  {
    Accelerate::except(badAccelChoice);
  }

  RFPointers(nRFCavities - 1) = nodes(nNodes - 1);

  nodesInitialized = 0;
  Ring::nLongUpdates++;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::addAccelbyPhase
//
// DESCRIPTION
//   Adds a phase-specified accelerating Node
//
// PARAMETERS
//   name:    Name for this node
//   order:   node order index
//   sub:     Not needed
//   table:   File name containing node data
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::addAccelbyPhase(const String &name, const Integer &order,
                                 const Subroutine sub, const String &table)
{
  if (nNodes == nodes.size()) nodes.resize(nNodes + 1);
  if(nRFCavities == RFPointers.size())
    RFPointers.resize(nRFCavities + 1);
  nNodes++;

  nodes(nNodes - 1) = new AccelbyPhase(name, order, sub, table);
  nRFCavities++;
  nAccelerates++;

  RFPointers(nRFCavities - 1) = nodes(nNodes - 1);

  nodesInitialized = 0;
  Ring::nLongUpdates++;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::showAccelNodes
//
// DESCRIPTION
//   Prints out the settings of the active Accelerates
//
// PARAMETERS
//   sout - The output stream
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::showAccelNodes(Ostream &sout)
{

  AccelerateBase *tlp;

  if (nRFCavities == 0)
  {
    sout << "\n No Accelerates \n\n";
    return;
  }

  for(Integer i=1; i<= nRFCavities; i++)
  {
    tlp = AccelerateBase::safeCast(RFPointers(i-1));
    tlp->_showAccelerate(sout);
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::getUFixedPoint
//
// DESCRIPTION
//   Finds the unstable fixed point
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::getUFixedPoint()
{
  Real minGuess, maxGuess;
  Real tol = 1.e-04;
  SyncPart *sp;
  sp= SyncPart::safeCast(syncP(0));

//jdg: put in exception for gamma at transition

  if(Abs(deltaEA) < Consts::tiny)  // no acceleration
  {
    if((sp->_gammaSync > Ring::gammaTrans) &&
       (Ring::gammaTrans > 0.0))
    {
      uFixedPoint = 0.0;
    }
    else
    {
      uFixedPoint = Consts::pi;
    }
    return;
  }

  if(deltaEA < 0.0) // negative acceleration
  {
    if((sp->_gammaSync > Ring::gammaTrans) &&
       (Ring::gammaTrans > 0.0))
    {
      minGuess = -Consts::pi / 2.;
      maxGuess = 0.0;
    }
    else
    {
      minGuess = -Consts::pi;
      maxGuess = -Consts::pi / 2.;
    }
  }

  if(deltaEA > 0.0) // positive acceleration
  {
    if((sp->_gammaSync > Ring::gammaTrans) &&
       (Ring::gammaTrans > 0.0))
    {
      minGuess = 0.0;
      maxGuess = Consts::pi / 2;
    }
    else
    {
      minGuess = Consts::pi / 2.;
      maxGuess = Consts::pi;
    }
  }

  deltaECharge = sp->_charge;

  uFixedPoint = 0.0;
  if(Abs(dEResid(uFixedPoint)) < Consts::tiny)
  {
  }
  else
  {
    if(rtbi(uFixedPoint, dEResid, minGuess, maxGuess, tol) == 0.)
    {
      except(badUFPSolve);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::getRampedBV
//
// DESCRIPTION
//   Gets ramped B field and voltage time wave forms from a file.
//   The first line is the number of RF harmonics.
//   The next line lists the harmonic numbers, with data separated
//   by whitespace. The rest of the file
//   data is expected to be in the following form (one data set / line),
//   again with data separated by whitespace:
//
//   time [msec], B [T], Volt(1) [kV], Phase(1) [deg], ...
//                       Volt(n) [kV], Phase(n) [deg]
//   where n is the number of RF harmonics and Volts is the ith harmonic
//   voltage, and phase is the absolute phase of the harmonic.
//
// PARAMETERS
//   fileName - The name of the file to open
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::getRampedBV(const String &fileName)
{
  Integer i;
  IFstream fio(fileName, ios::in);
  if(fio.good() != 1) except(badRampedBFile);

  nTimePoints = 0;
  fio >> nRFHarmonics;
  RFVolts.resize(nRFHarmonics);
  RFHarmNum.resize(nRFHarmonics);
  RFPhase.resize(nRFHarmonics);

  for(i = 1; i <= nRFHarmonics; i++)
  {
    fio  >> RFHarmNum(i);
  }

  Real timeDummy;
  while(!fio.eof())
  {
    if(fio >> timeDummy)
    {
      nTimePoints ++;
      RFVoltPoints.resize(nRFHarmonics, nTimePoints);
      RFPhasePoints.resize(nRFHarmonics, nTimePoints);
      BSynchPoints.resize(nTimePoints);
      timePoints.resize(nTimePoints);

      timePoints(nTimePoints) = timeDummy;
      fio >> BSynchPoints(nTimePoints);
      for(i = 1; i <= nRFHarmonics; i++)
        fio  >> RFVoltPoints(i, nTimePoints)
             >> RFPhasePoints(i,nTimePoints);
    }
  }
    fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::getRampedV
//
// DESCRIPTION
//   Gets ramped voltage info from a file.
//   The first line is the number of RF harmonics.
//   The next line lists the harmonic numbers, with data separated
//   by whitespace. The rest of the file
//   data is expected to be in the following form (one data set / line),
//   again with data separated by whitespace:
//
//   time [msec], Volt(1) [kV], Phase(1) [deg], ...
//                Volt(n) [kV], Phase(n) [deg]
//   where n is the number of RF harmonics and Volts is the ith harmonic
//   voltage, and phase is the absolute phase of the harmonic.
//
// PARAMETERS
//   fileName - The name of the file to open
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::getRampedV(const String &fileName)
{
  Integer i;
  IFstream fio(fileName, ios::in);
  if(fio.good() != 1) except(badRampedBFile);

  nTimePoints = 0;
  fio >> nRFHarmonics;
  RFVolts.resize(nRFHarmonics);
  RFHarmNum.resize(nRFHarmonics);
  RFPhase.resize(nRFHarmonics);

  for(i = 1; i <= nRFHarmonics; i++)
  {
    fio  >> RFHarmNum(i);
  }

  Real timeDummy;
  while(!fio.eof())
  {
    if(fio >> timeDummy)
    {
      nTimePoints ++;
      RFVoltPoints.resize(nRFHarmonics, nTimePoints);
      RFPhasePoints.resize(nRFHarmonics, nTimePoints);
      timePoints.resize(nTimePoints);

      timePoints(nTimePoints) = timeDummy;
      for(i = 1; i <= nRFHarmonics; i++)
        fio  >> RFVoltPoints(i, nTimePoints)
             >> RFPhasePoints(i,nTimePoints);
    }
  }
    fio.close();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::InterpolateBV
//
// DESCRIPTION
//   Gets the ramped B field and RF voltages and phases
//   by interpolation
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::InterpolateBV()
{
  if((time < Accelerate::timePoints(1)) ||
     (time > Accelerate::timePoints(nTimePoints)))
    Accelerate::except(badTimePoint);

  Integer i = 0;
  while((Ring::time - timePoints(i + 1)) >= 0.0) i++;
		
  if(i < 1) i = 1;
  if(i > nTimePoints - 1) i = nTimePoints - 1;

  Real frac = (time - timePoints(i)) /
              (timePoints(i + 1) - timePoints(i));

  BSynch = Accelerate::BSynchPoints(i) +
           frac * (Accelerate::BSynchPoints(i + 1) -
                   Accelerate::BSynchPoints(i));

  for(Integer j = 1; j <= nRFHarmonics; j++)
  {
    RFVolts(j) = Accelerate::RFVoltPoints(j, i) +
                 frac * (Accelerate::RFVoltPoints(j, i + 1) -
                         Accelerate::RFVoltPoints(j, i));
    RFPhase(j) = (Accelerate::RFPhasePoints(j, i) +
                  frac * (Accelerate::RFPhasePoints(j, i + 1) -
		          Accelerate::RFPhasePoints(j, i)))
	         * Consts::pi/180.;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::InterpolateV
//
// DESCRIPTION
//   Gets the ramped RF voltages and phases
//   by interpolation
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::InterpolateV()
{
  if((time < Accelerate::timePoints(1)) ||
     (time > Accelerate::timePoints(nTimePoints)))
    Accelerate::except(badTimePoint);

  BSynch = BSync0 - BSync1 * cos(Consts::twoPi * time * BSyncFreq * 1.e-03);

  Integer i = 0;
  while((Ring::time - timePoints(i + 1)) >= Consts::tiny) i++;
  if(i < 1) i = 1;
  if(i > nTimePoints - 1) i = nTimePoints - 1;

  Real frac = (time - timePoints(i)) /
              (timePoints(i+1) - timePoints(i));

  for(Integer j = 1; j <= nRFHarmonics; j++)
  {
    RFVolts(j) = Accelerate::RFVoltPoints(j, i) +
                 frac * (Accelerate::RFVoltPoints(j, i + 1) -
                         Accelerate::RFVoltPoints(j, i));
    RFPhase(j) = (Accelerate::RFPhasePoints(j, i) +
                  frac * (Accelerate::RFPhasePoints(j, i + 1) -
		          Accelerate::RFPhasePoints(j, i)))
	         * Consts::pi/180.;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::addRFCavity
//
// DESCRIPTION
//   Adds an RF Cavity object, which uses a constant RF voltage
//   scenario, with an arbitrary number of harmonics.
//   V = sum over i (voltage(i) * sin(harmNum(i) * (phi + phase(i)))
//
// PARAMETERS
//   name:    Name for this node
//   order:   node order index
//   nHarm    Number of harmonic components to use in the voltage waveform
//   voltage  Peak voltage vector [kV]
//   harmNum  Harmonic number vector for RF Voltage
//   phase    phase vector for RF voltage [rad]
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::addRFCavity(const String &name, const Integer &order,
                             Integer &nHarm, Vector(Real) &voltage,
                             Vector(Real) &harmNum, Vector(Real) &phase)
{
  if((voltage.rows() < nHarm) ||
     (phase.rows() < nHarm) ||
     (harmNum.rows() < nHarm))
    except(badVoltageSize);

  if(nNodes == nodes.size())
    nodes.resize(nNodes + 1);
  if(nRFCavities == RFPointers.size())
    RFPointers.resize(nRFCavities + 1);

  nNodes++;
  nodes(nNodes - 1) = new RFCav(name, order, nHarm, voltage,
                                harmNum, phase, constVolts);
  nRFCavities ++;
  RFPointers(nRFCavities - 1) = nodes(nNodes - 1);

  nodesInitialized = 0;
  Ring::nLongUpdates++;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::addBarrierCavity
//
// DESCRIPTION
//   Adds an RF Barrier Cavity object, which uses a constant RF voltage
//   scenario, with an arbitrary number of harmonics
//
// PARAMETERS
//   name:    Name for this node
//   order:   node order index
//   nHarm    Number of harmonic components to use in the voltage waveform
//   voltage  Peak voltage vector [kV]
//   harmNum  Harmonic number vector for RF Voltage
//   phase    phase vector for barrier centers [deg]
//   dphase   phase vector for barrier widths [deg]
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::addBarrierCavity(const String &name, const Integer &order,
                                  Integer &nHarm, Vector(Real) &voltage,
                                  Vector(Real) &harmNum, Vector(Real) &phase,
                                  Vector(Real) &dphase)
{
  if((voltage.rows() < nHarm) ||
     ( phase.rows() < nHarm) ||
     ( harmNum.rows() < nHarm))
    except(badVoltageSize);

  if(nNodes == nodes.size())
    nodes.resize(nNodes + 1);
  if(nRFCavities == RFPointers.size())
    RFPointers.resize(nRFCavities + 1);

  nNodes++;
  nodes(nNodes - 1) = new BarrierCav(name, order, nHarm, voltage,
                                     harmNum, phase, dphase, constVolts);
  nRFCavities ++;
  RFPointers(nRFCavities - 1) = nodes(nNodes - 1);

  nodesInitialized = 0;
  Ring::nLongUpdates++;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::addRampedRFCavity
//
// DESCRIPTION
//   Adds an RF Cavity object, which uses a ramped RF voltage
//   scenario, with an arbitrary number of harmonics.
//   V = sum over i (voltage(i) * sin(harmNum(i) * (phi + phase(i)))
//
// PARAMETERS
//   name:    Name for this node
//   order:   node order index
//   nHarm    Number of harmonic components to use in the voltage waveform
//   voltage  Peak voltage vector [kV]
//   harmNum  Harmonic number vector for RF Voltage
//   phase    phase vector for RF voltage [rad]
//   sub      subroutine that scales the above voltages with time.
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::addRampedRFCavity(const String &name, const Integer &order,
                                   Integer &nHarm, Vector(Real) &voltage,
                                   Vector(Real) &harmNum,
                                   Vector(Real) &phase,
                                   Subroutine sub)
{
  if((voltage.rows() < nHarm) ||
     ( phase.rows() < nHarm) ||
     ( harmNum.rows() < nHarm))
    except(badVoltageSize);

  if(nNodes == nodes.size())
    nodes.resize(nNodes + 1);
  if(nRFCavities == RFPointers.size())
    RFPointers.resize(nRFCavities + 1);

  nNodes++;
  nodes(nNodes - 1) = new RFCav(name, order, nHarm, voltage,
                                 harmNum, phase, sub);
  nRFCavities ++;
  RFPointers(nRFCavities -1) = nodes(nNodes-1);

  nodesInitialized = 0;
  Ring::nLongUpdates++;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Accelerate::constVolts
//
// DESCRIPTION
//   Dummy routine for a constant RF voltage scenario.
//   Does nothing, no scaling needed.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::constVolts()
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Accelerate::showAccelerate
//
// DESCRIPTION
//   Prints out some acceleration parameters to a stresm
//
// PARAMETERS
//   os - the stream to print to
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Accelerate::showAccelerate(Ostream &os)
{
  SyncPart *sp;
  Integer i;
  Real dpSig;

  sp = SyncPart::safeCast(syncP(0));
  MacroPart *mp = MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));

  Vector(Real) temp(mp->_nMacros);

  static Integer first = 1;
  if(first)
  {
    first=0;
    os << accelHeader1;
    os << accelHeader2;
    os << accelLine;
  }

  orbitFreq = Consts::vLight  * sp->_betaSync/Ring::lRing;

  mp->_findXYPExtrema();
  mp->_findDEExtrema();

  syncFreq = Consts::vLight / Ring::lRing *
             Sqrt(1.e-06 * Ring::harmonicNumber * Abs(Ring::etaCompaction)
                  * RFVolts(1) / (sp->_eTotal * Consts::twoPi));
  Real bunchHeight = Max(mp->_dEMax, Abs(mp->_dEMin));

  Real factor = 1.e+09 * Ring::lRing / (2. * Consts::vLight *
                                        mp->_syncPart._betaSync *
                                        Ring::harmonicNumber);

// Bunch area assumes simple ellipse:

  temp = mp->_deltaE;
  MathLib::hpSort(temp);
  Real delE = temp(Integer(mp->_nMacros * 0.999)) -
              temp(Integer(mp->_nMacros * 0.001 + 1));

  temp = mp->_phi;
  MathLib::hpSort(temp);
  Real delPhi = temp(Integer(mp->_nMacros * 0.999)) -
                temp(Integer(mp->_nMacros * 0.001 + 1));
  bunchArea = factor * delE * delPhi/4.;

  LongEmittance le(*mp);
  le._diagCalculator();
  dpSig = le._dESig * mp->_syncPart._dppFac;

  getUFixedPoint();
  Real ang = sp->_phase + Ring::bucketphi0 * (uFixedPoint - sp->_phase);

  if(nRFCavities > 0)  calcBucket(bucketdE0, ang);

  Real dppMax = Max(mp->_dEMax, Abs(mp->_dEMin));
  dppMax *= mp->_syncPart._dppFac;

  long dummy = os.setf(ios::right, ios::adjustfield);

  os.setf(ios::right,ios::adjustfield);

  os << setw(5) << nTurnsDone << "   "
     << setw(8) << time <<  " "
     << setw(7) << nMacroParticles << "  "
     << setw(5) << setprecision(4) << sp->_eKinetic << "  "
     << setw(4) << setprecision(4) << mp->_bunchFactor  << "  "
     << setw(4) << setprecision(4) << sp->_betaSync << "  "
     << setw(5) << setprecision(4) << 180./Consts::pi * sp->_phase << "  "
     << setw(6) << setprecision(4) << RFVolts(1) << "  "
     << setw(6) << setprecision(4) << syncFreq << "  "
     << setw(5) << setprecision(4) << 1.e3 * bucketHeight << "  "
     << setw(5) << setprecision(4) << 1.e3 * bunchHeight << "  "
     << setw(5) << bucketArea << "  "
     << setw(5) << bunchArea << "  "
     << setw(5) << dppMax*100. << "  "
     << setw(5) << dpSig*100. << "\n";
}
