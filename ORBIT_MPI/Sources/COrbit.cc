/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    COrbit.cc
//
// AUTHOR
//    Steven Bunch
//    University of Tennessee, bunchsc@sns.gov
//
// CREATED
//    7/3/02
//
//  DESCRIPTION
//    Module descriptor file for the Closed Orbit module. This module
//    contains source for the Closed Orbit related info.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "COrbit.h"
#include "Solver.h"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

Void turn();
Void con1();

Integer testHerd;
Real xi, xpi, yi, ypi, dE;
Real xf, xpf, yf, ypf, cx, cxp, cy, cyp, tol;
Integer ix, ixp, iy, iyp, icl1, icl2, ic1, ic2, ic3, ic4;

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    COrbit::ctor
//
// DESCRIPTION
//    Initializes the various Closed Orbit related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void COrbit::ctor()
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//    COrbit::closedOrbit
//
// DESCRIPTION
//    Calculates Closed Orbit Solution
//
// PARAMETERS
//    xi
//    xpi
//    yi
//    ypi
//    dE
//    ctol
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void COrbit::closedOrbit(Real &axi, Real &axpi, Real &ayi,
                         Real &aypi, Real &adE, const Real &ctol)
{
  xi = axi;
  xpi = axpi;
  yi = ayi;
  ypi = aypi;
  dE = adE;

  testHerd = Particles::addMacroHerdBase(1, "testHerd"); // small test herd
  setHerdFeelLevel(testHerd, 1); // this herd only feels herds.
  addMacroPartBase2(testHerd, xi, xpi, yi, ypi, dE, 0.);

  ix = Solver::addVariable("xi", xi, 1.0, 0, 0, 0.0, 0.0, 1);
  ixp = Solver::addVariable("xpi", xpi, 1.0, 0, 0, 0.0, 0.0, 1);
  iy = Solver::addVariable("yi", yi, 1.0, 0, 0, 0.0, 0.0, 1);
  iyp = Solver::addVariable("ypi", ypi, 1.0, 0, 0, 0.0, 0.0, 1);

  ic1 = Solver::addConstraint("cx", cx, 1.0, 1, 1);
  ic2 = Solver::addConstraint("cxp", cxp, 1.0, 1, 1);
  ic3 = Solver::addConstraint("cy", cy, 1.0, 1, 1);
  ic4 = Solver::addConstraint("cyp", cyp, 1.0, 1, 1);

  icl1 = Solver::addCalculator("Do Turn", turn, 1, 1);
  icl2 = Solver::addCalculator("Solution", con1, 2, 1);

  tol = ctol;

  Solver::solve(1);

  Solver::showEqnSetBase(cout,0);

  axi = xi;
  axpi = xpi;
  ayi = yi;
  aypi = ypi;
  adE = dE;
}

Void turn()
{
  Particles::setPartValsBase(1,testHerd,xi,xpi,yi,ypi,dE,0.);

  Ring::doTurnBase(testHerd,1);

  xf = Particles::xValBase(1,testHerd);
  xpf = Particles::xpValBase(1,testHerd);
  yf = Particles::yValBase(1,testHerd);
  ypf = Particles::ypValBase(1,testHerd);
}

Void con1()
{
  cx = xf - xi;
  cxp = xpf - xpi;
  cy = yf - yi;
  cyp = ypf - ypi;
}
