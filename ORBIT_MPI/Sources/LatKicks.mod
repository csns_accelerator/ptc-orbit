/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//  LatKicks.mod
//
// AUTHOR
//  Jeff Holmes,  ORNL, jzh@ornl.gov
//  John Galambos, ORNL, jdg@ornl.gov
//
// CREATED
//  5/03/99
//
// DESCRIPTION
//  Module descriptor file for the LatKicks module. This module contains
//  information about symplectic kicks.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module LatKicks      - "LatKicks Module."
                     - "Written by Jeff Holmes, ORNL, jzh@ornl.gov"
{

   Inherits Consts, Particles, Ring, TransMap;

Errors
   badKickFile       - "Sorry, you asked for a non existent "
                     - "or non matching kick file",
   tooFewTransMaps   - "Sorry, you need more Transfer Matrices before you "
                     - "add symplectic kicks",
   badNodeNum        - "You asked for the data from a nonexistent node";

public:

   Void ctor() 
                     - "Constructor for the LatKicks module."
                     - "Initializes build values.";

   Integer
   nLatKickNodes     - "Number of symplectic kick "
                     - "nodes in the ring.";

   Void addTKickPair(const String &n, const Integer &o,
                     const Integer &xkicks,
                     const IntegerVector &x_xpow,
                     const IntegerVector &xp_xpow,
                     const IntegerVector &y_xpow,
                     const IntegerVector &yp_xpow,
                     const IntegerVector &del_xpow,
                     const RealVector &xcflow,
                     const RealVector &xcfhi,
                     const Integer &xpkicks,
                     const IntegerVector &x_xppow,
                     const IntegerVector &xp_xppow,
                     const IntegerVector &y_xppow,
                     const IntegerVector &yp_xppow,
                     const IntegerVector &del_xppow,
                     const RealVector &xpcflow,
                     const RealVector &xpcfhi,
                     const Integer &ykicks,
                     const IntegerVector &x_ypow,
                     const IntegerVector &xp_ypow,
                     const IntegerVector &y_ypow,
                     const IntegerVector &yp_ypow,
                     const IntegerVector &del_ypow,
                     const RealVector &ycflow,
                     const RealVector &ycfhi,
                     const Integer &ypkicks,
                     const IntegerVector &x_yppow,
                     const IntegerVector &xp_yppow,
                     const IntegerVector &y_yppow,
                     const IntegerVector &yp_yppow,
                     const IntegerVector &del_yppow,
                     const RealVector &ypcflow,
                     const RealVector &ypcfhi
                    )
                     - "Routine to add a pair of "
                     - "symplectic kick nodes.";

   Void addTKickSet(const String &fileName)
                     - "Routine to add a set of "
                     - "symplectic kick nodes.";

   Void showTKickNode(const Integer &n, Ostream &os)
                     - "Routine to show symplectic "
                     - "kick node info. for single node";

   Void dumpTKickNodes(Ostream &os)
                     - "Dump the symplectic "
                     - "kick node info. for all the nodes";

}
