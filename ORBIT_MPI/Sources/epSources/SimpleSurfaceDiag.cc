//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    SimpleSurfaceDiag.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/23/2003
//
// DESCRIPTION
//   This class is the subclass of the AbstractSurfaceDiag class, and it 
//   collects information about energy spectrum of the macro-particles 
//   hitting the surface of the wall, and macro-size distribution of 
//   the same particles. 
//
///////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include <cmath>

#include "SimpleSurfaceDiag.hh"
#include "epModuleConstants.hh"

//costructor parameters timeStartIn and timeStopIn in [ns]
SimpleSurfaceDiag::SimpleSurfaceDiag(int nTurnIn, 
				     double timeStartIn, 
				     double timeStopIn, 
				     char* fOutNameIn) :
  AbstractSurfaceDiag("SIMPLE SURFACE DIAGNOSTICS")
{
  fOutName = (char *) malloc (sizeof(char) * (NAME_LENGTH_MAX));
  strncpy(fOutName,fOutNameIn,NAME_LENGTH_MAX);
  nTurn = nTurnIn;
  ctimeStart = timeStartIn*1.0e-9*epModuleConstants::c;
  ctimeStop  = timeStopIn*1.0e-9*epModuleConstants::c;
  ctimeStartActual = 0.;
  ctimeStopActual = 0.;
  inf_start_done = 0;
  inf_stop_done = 0;
  eH_ = new histogram(100);
  mH_ = new histogram(100); 
  turnCurr = 0;
}

SimpleSurfaceDiag::~SimpleSurfaceDiag()
{
  free(fOutName);
  delete eH_;
  delete mH_;
}

void SimpleSurfaceDiag::init()
{
  ctimeStartActual = 0.;
  ctimeStopActual = 0.;
  inf_start_done = 0;
  inf_stop_done = 0;
  eH_->clean();
  mH_->clean();
}

void SimpleSurfaceDiag::setZeroTurn()
{
  init();
  turnCurr = 0;
}

void SimpleSurfaceDiag::startNewTurn()
{
  init();
  turnCurr++;
}

void SimpleSurfaceDiag::accountTime(double ctime)
{
  if(nTurn != turnCurr) return;

  if(inf_start_done == 0 && inf_stop_done == 0 && ctime >= ctimeStart){
    inf_start_done = 1;
    ctimeStartActual = ctime;
  }

  if(inf_start_done == 1 && inf_stop_done == 0 && ctime >= ctimeStop){
    //stop analysis and print result
    inf_stop_done = 1;
    ctimeStopActual = ctime;
    print();
  }  
}


void SimpleSurfaceDiag::analyze(int indHit, int nOut, eBunch* eb, double* n_v)
{
  if(inf_start_done == 1 && inf_stop_done == 0){
    //analyze 
    double enrg, macro;
    enrg = eb->getEnergy(indHit);
    macro = eb->macroSize(indHit);
    eH_->add(enrg,macro);
    mH_->add(macro);
  }
}


void SimpleSurfaceDiag::print(){
  print(fOutName);
}

void SimpleSurfaceDiag::print(std::ostream& Out)
{
  eH_->analyze();
  mH_->analyze();
  
  if(rank_MPI == 0){
    Out<<"========================================="<<std::endl;
    Out<<getName()<<std::endl;
    Out<<"========================================="<<std::endl;
    Out<<"Start time [ns] = "<< ctimeStartActual * 1.0e+9/epModuleConstants::c<<std::endl;
    Out<<"Stop  time [ns] = "<< ctimeStopActual * 1.0e+9/epModuleConstants::c<<std::endl;
  }

  if(rank_MPI == 0){
    Out<<"========ENERGY DISTRIBUTION======="<<std::endl;
  }
  eH_->print(Out);
  if(rank_MPI == 0){
    Out<<"========MACRO-SIZE DISTRIBUTION======="<<std::endl;
  }
  mH_->print(Out);
}

void SimpleSurfaceDiag::print(char* fileName)
{
    ofstream F_dump;
    if(rank_MPI == 0)F_dump.open (fileName, ios::out);
    print(F_dump);
    if(rank_MPI == 0){F_dump.close();}
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
