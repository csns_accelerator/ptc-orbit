//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    numericalFunctions.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    09/22/2003
//
// DESCRIPTION
//    Keeps static numerical functions for ep Module
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <mpi.h>
#include <iostream> 
#include <fstream>
using namespace std;

#ifndef NUMERICAL_FUNCTIONS_H
#define NUMERICAL_FUNCTIONS_H

class  numericalFunctions
{
public:

  numericalFunctions();
  ~numericalFunctions();

  //-------------------------------------------------------------
  //functions
  //-------------------------------------------------------------
  static double incompleteGamma(double p,double x);
  static double incompleteBeta(double x,double mu,double nu);

  //-------------------------------------------------------------
  //functions from numerical recipes
  //-------------------------------------------------------------
  static double gammln(double xx);
  static double factrl(int n);
  static double factln(int n);
  static double bico(int n,int k);

  //for incomplete gamma function
  static void gser(double& gamser, double a, double x, double& gln);
  static void gcf(double& gammcf, double a, double x, double& gln);
  static double gammp(double a,double x);

  //for incomplete beta function
  static double betacf(double a, double b, double x);
  static double betai(double a, double b, double x);

private:
  const static int ITMAX;
  const static int MAXIT;
  const static double EPS;
  const static double FPMIN;

private:
  static double* gammlnCoeff;
  static double* setGammlnCoeffArray();
public:
  static double getGammlnCoeffArray(int i);
private:
  static double* factrlInit;
  static double* setFactrlInitArray();

  //-------------------------------------------------------------
  //operation tool
  //-------------------------------------------------------------
private:
  static void FinalizeExecution(); 

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
