//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    RhoGrid3D.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    06/09/2003
//
// DESCRIPTION
//    The subclass of Grid3D class to handle 
//    the potential calculations
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef RHO_GRID3D_HH
#define RHO_GRID3D_HH

#include "Grid3D.hh"
using namespace std;

class RhoGrid3D : public Grid3D
{
public:
  //--------------------------------------
  //the public methods of the RhoGrid3D class
  //--------------------------------------
  
  RhoGrid3D(int nZ, EP_Boundary* boundaryIn);
  virtual ~RhoGrid3D();

  //calculates potential on Grid3D by using boundary's method
  void findPotential(Grid3D* phiGrid);

  //bins macro-particles (electrons)
  void binParticles(eBunch* eb);

};
#endif
///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
