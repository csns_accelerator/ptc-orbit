//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    UniformField.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/25/2004
//
// DESCRIPTION
//   The source of the electrostatic and magnetic uniform fields
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////
#include "BaseFieldSource.hh"

#ifndef UNIFORM_FIELD_HH
#define UNIFORM_FIELD_HH

class UniformField : public BaseFieldSource
{
public:

  UniformField();
  virtual ~UniformField();

  //set components of electric field in Volt/m
  void setElectricField(double EX_in, double EY_in, double EZ_in);

  //set components of magnetic field in Tesla
  void setMagneticField(double HX_in, double HY_in, double HZ_in);

  //set the spatial limits
  void setX_MinMax(double xMin_in, double xMax_in);
  void setY_MinMax(double yMin_in, double yMax_in);
  void setZ_MinMax(double zMin_in, double zMax_in);

  double getAbsElectricField();
  double getAbsMagneticField();  

  //--------------------------------------------------------
  //overridden methods
  //---------------------------------------------------------

  //returns the H in Gauss/(electron charge in CGS)
  void getMagneticField(double x,    double y,    double z, 
                        double& comp_x, double& comp_y, double& comp_z);

  //returns the electric field (for point like charge 1/(r*r))
  void getElectricField(double x,    double y,    double z, 
                        double& e_x, double& e_y, double& e_z);

protected:
  double EX,EY,EZ;
  double COMP_X,COMP_Y,COMP_Z;

  double xMin,xMax;
  double yMin,yMax;
  double zMin,zMax;

};
#endif

