//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    baseParticleTracker.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    06/17/2003
//
// DESCRIPTION
//   the base class to move macro-particle in the electromagnetic field
//   This class needs one or more BaseFieldSource or 
//   BaseFieldSource.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef BASE_PARTICLE_TRACKER_HH
#define BASE_PARTICLE_TRACKER_HH

#include "eBunch.hh"
#include "baseSurface.hh"
#include "EP_Boundary.hh"
#include "Grid3D.hh"
#include "BaseFieldSource.hh"
#include "missingPartsDiagnostic.hh"
#include "CompositeSurfaceDiag.hh"

class baseParticleTracker
{
public:

  baseParticleTracker();
  virtual ~baseParticleTracker();

  void setMissingPartsDiagnostics(missingPartsDiagnostic* missingDiagIn);
  missingPartsDiagnostic* getMissingPartsDiagnostics();

  void setSurfaceDiagnostics(CompositeSurfaceDiag* surfaceDiagIn);
  CompositeSurfaceDiag* getSurfaceDiagnostics();

  void setNumberOfSteps(int nSteps);

  void addMagneticFieldSource(BaseFieldSource* fs);

  void addElectricFieldSource(BaseFieldSource* fs);

  void removeMagneticFieldSource(BaseFieldSource* fs);

  void removeElectricFieldSource(BaseFieldSource* fs);

  void moveParticles(double ctime, 
                    eBunch* eb, 
                    Grid3D* rhoGrid3D,
                    baseSurface* surface);

  void moveParticlesSimplectic(double ctime, 
			       eBunch* eb, 
			       Grid3D* rhoGrid3D,
			       baseSurface* surface);

  double getDeadMacroSize();
  double getAbsorbedEnergy();

private:

  //returns the H in Gauss/(electron charge in CGS)
  void getMagneticField(double x,    double y,    double z, 
                        double& h_x, double& h_y, double& h_z);

  //returns the electric field (for point like charge 1/(r*r))
  void getElectricField(double x,    double y,    double z, 
                        double& e_x, double& e_y, double& e_z);

private:

  int i_s;

  BaseFieldSource** electricSources;
  int nElSources;
  BaseFieldSource** magneticSources;
  int nMgSources;

  double e_x_sum, e_y_sum, e_z_sum;
  double h_x_sum, h_y_sum, h_z_sum;

  double e_x_tmp, e_y_tmp, e_z_tmp;
  double h_x_tmp, h_y_tmp, h_z_tmp;

  //missing particles diagnostics
  missingPartsDiagnostic* missingDiag;

  //surface diagnostics composite
  CompositeSurfaceDiag* surfaceDiag;

  //number of steps for integration of the motion laws
  int nSteps_;

  //for electron detector 0v
  double macroSize_dead;
  double energy_absorbed;

};
#endif

