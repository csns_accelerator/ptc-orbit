//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    histogram.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    08/07/2003
//
// DESCRIPTION
//    Source code for the class used to make histogram from set of data
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include "histogram.hh"

histogram::histogram(int sizeIn)
{
  inf_fixedLimits = 0;
  size = sizeIn;
  init();
}

histogram::histogram(double xMinIn,double xMaxIn, int sizeIn)
{
  inf_fixedLimits = 1;
  size = sizeIn;
  xMin = xMinIn;
  xMax = xMaxIn;
  init();
  for(int i = 0; i <= size; i++){
    gridX[i] = xMin + i*(xMax - xMin)/(size);
  }
  gridX[0]    = xMin;
  gridX[size] = xMax;
  for(int i = 0; i <= size; i++){
    gridX_MPI[i] = gridX[i];
  }
}

histogram::histogram(double* xGridIn, int nPoint)
{
  inf_fixedLimits = 1;
  size = nPoint-1;
  xMin = xGridIn[0];
  xMax = xGridIn[size];
  init();
  for(int i = 0; i <= size; i++){
    gridX[i] = xGridIn[i];
    gridX_MPI[i] = xGridIn[i];
  }
}

void histogram::init()
{  
  gridX     = (double *) malloc (sizeof(double) * (size+1));
  tmp_gridX = (double *) malloc (sizeof(double) * (size+1));
  histY     = (double *) malloc (sizeof(double) * size);
  histY2    = (double *) malloc (sizeof(double) * size);
  histN     = (int *) malloc (sizeof(int) * size);

  nTotalCount = 0;

  gridX_MPI  = (double *) malloc (sizeof(double) * (size+1));
  histY_MPI  = (double *) malloc (sizeof(double) * size);
  histY2_MPI = (double *) malloc (sizeof(double) * size);
  histN_MPI  = (int *) malloc (sizeof(int) * size);

  buffMaxSize = 1000;
  buffSize = 0;
  xArr = (double *) malloc (sizeof(double) * buffMaxSize);
  wArr = (double *) malloc (sizeof(double) * buffMaxSize);

  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  } 

  //set to 0 histograms and values
  clean();

}

histogram::~histogram()
{
  free(gridX);
  free(tmp_gridX);
  free(histY);
  free(histY2);
  free(histN);

  free(gridX_MPI);
  free(histY_MPI);
  free(histY2_MPI);
  free(histN_MPI);

  free(xArr);
  free(wArr);
}

void histogram::setBufferSize(int buffSizeMax)
{
  free(xArr);
  free(wArr);
  buffMaxSize = buffSizeMax;
  buffSize = 0;
  xArr = (double *) malloc (sizeof(double) * buffMaxSize);
  wArr = (double *) malloc (sizeof(double) * buffMaxSize);
}

void histogram::add(double x)
{
  inf_dataConsumed = 0;
  xArr[buffSize] = x;
  wArr[buffSize] = 1.0;
  buffSize++;
  if(buffSize == buffMaxSize){
    consumeBuffer();
  }
}

void histogram::add(double x,double weight)
{
  inf_dataConsumed = 0;
  xArr[buffSize] = x;
  wArr[buffSize] = weight;
  buffSize++;
  if(buffSize == buffMaxSize){
    consumeBuffer();
  }
}


int histogram::getIndex(double* grd,double x)
{
  if(x < grd[0]) return -1;
  if(x > grd[size]) return size;
  int ind = 0;
  while( x > grd[ind+1] && ind < (size-1)){
    ind++;
  }
  return ind;
}

void histogram::transformGrid(double* fromGrid, double* toGrid)
{
      for(int i = 0; i < size; i++){
        histY_MPI[i] = 0.;
        histY2_MPI[i] = 0.;
        histN_MPI[i] = 0;
      }
      int ind0,ind1;
      double coeff;
      for(int i = 0; i < size; i++){
        ind0 = getIndex(toGrid,fromGrid[i]);
        ind1 = getIndex(toGrid,fromGrid[i+1]);
        if(ind0 == ind1){
          histY_MPI[ind0]  += histY[i];
          histY2_MPI[ind0] += histY2[i];
	  histN_MPI[ind0]  += histN[i];
	}
        else{
	  coeff = (fromGrid[i+1] - toGrid[ind1])/(fromGrid[i+1] - fromGrid[i]);
	  if(coeff < 0. || coeff > 1.0){
            finalizeExecution("During redistribution coeff outside 0-1 region.");
	  }
	  histY_MPI[ind0]  += (1.0 - coeff)*histY[i];
          histY2_MPI[ind0] += (1.0 - coeff)*histY2[i];
	  histN_MPI[ind0]  += (int) ((1.0 - coeff)*histN[i]);
	  histY_MPI[ind1]  += coeff*histY[i];
          histY2_MPI[ind1] += coeff*histY2[i];
	  histN_MPI[ind1]  += histN[i] - ((int) ((1.0 - coeff)*histN[i]));
	}
      }
      for(int i = 0; i < size; i++){
	histY[i]  = histY_MPI[i];
	histY2[i] = histY2_MPI[i];
	histN[i]  = histN_MPI[i];
      }
      for(int i = 0; i <= size; i++){
	fromGrid[i] = toGrid[i];
      }

}


void histogram::consumeBuffer()
{
  if(buffSize == 0){
     inf_dataConsumed = 1;
     return;
  }
  int ind = 0;
  if(inf_fixedLimits == 1){
    for(int i = 0; i < buffSize; i++){
      nTotalCount++;
      ind = getIndex(gridX,xArr[i]);
      if(ind < 0 || ind == size){
	if(ind == -1){
          outLowCount++;
          outLowY += wArr[i];
	}
	else{
          outUppCount++;
          outUppY += wArr[i];
	}
      }
      else{
        histY[ind]  += wArr[i];
        histY2[ind] += wArr[i]*wArr[i];
        histN[ind]++;
      }
    }
  }
  else{
    //non fixed limits
    int i_change = 0;
    for(int i = 0; i < buffSize; i++){
      if(xMin > xArr[i]) {
	xMin = xArr[i];
	i_change = 1;
      }
      if(xMax < xArr[i]) {
	xMax = xArr[i];
	i_change = 1;
      }
    }
    if(nTotalCount > 0 && xMin < xMax && i_change != 0){
      //redistribution

      for(int i = 0; i <= size; i++){
	tmp_gridX[i] = xMin + i*(xMax - xMin)/(size);
      }
      tmp_gridX[0] = xMin;
      tmp_gridX[size] = xMax;
      transformGrid(gridX,tmp_gridX);
    }
    else{
      if(nTotalCount == 0){
	for(int i = 0; i <= size; i++){
	  gridX[i] = xMin + i*(xMax - xMin)/(size);
	}
	gridX[0] = xMin;
	gridX[size] = xMax;
      }
    }

    //let's account data
    for(int i = 0; i < buffSize; i++){
      nTotalCount++;
      ind = getIndex(gridX,xArr[i]);
      histY[ind]  += wArr[i];
      histY2[ind] += wArr[i]*wArr[i];
      histN[ind]++;
    }

  }
  buffSize = 0;
}

void histogram::analyze(){

  consumeBuffer();

  if(size_MPI > 1){
    //parallel case
    if(inf_fixedLimits == 0){
      double xMin_MPI = xMin;
      double xMax_MPI = xMax;
      MPI_Allreduce(&xMin,&xMin_MPI,1,MPI_DOUBLE,MPI_MIN,MPI_COMM_WORLD);
      MPI_Allreduce(&xMax,&xMax_MPI,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      if(xMin_MPI != xMin || xMax_MPI != xMax){
        xMin = xMin_MPI;
	xMax = xMax_MPI;
	for(int i = 0; i <= size; i++){
	  tmp_gridX[i] = xMin + i*(xMax - xMin)/(size);
	}
	tmp_gridX[0] = xMin;
	tmp_gridX[size] = xMax;
	transformGrid(gridX,tmp_gridX);
      }
    }

    for(int i = 0; i <= size; i++){
      gridX_MPI[i] = gridX[i];
    }

    MPI_Allreduce(&nTotalCount,&nTotalCount_MPI,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(&outLowCount,&outLowCount_MPI,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(&outUppCount,&outUppCount_MPI,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

    MPI_Allreduce(&outLowY,&outLowY_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(&outUppY,&outUppY_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

    MPI_Allreduce(histN,histN_MPI,size,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(histY,histY_MPI,size,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(histY2,histY2_MPI,size,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

    integral = 0.;
    for(int i = 0; i < size; i++){
      if(gridX[i+1] != gridX[i]){
	histY_MPI[i] = histY_MPI[i]/(gridX[i+1]-gridX[i]);
	histY2_MPI[i] = histY2_MPI[i]/(gridX[i+1]-gridX[i]);
      }
      integral += histY[i];
    }

    MPI_Allreduce(&integral,&integral_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

  }
  else{
    //single CPU case
    nTotalCount_MPI = nTotalCount;
    outLowCount_MPI = outLowCount;
    outUppCount_MPI = outUppCount;
    outLowY_MPI = outLowY;
    outUppY_MPI = outUppY;
    for(int i = 0; i <= size; i++){
      gridX_MPI[i] = gridX[i];
    }
    integral = 0.;
    for(int i = 0; i < size; i++){
      histY_MPI[i]  =  histY[i];
      histY2_MPI[i] = histY2[i];
      histN_MPI[i]  = histN[i];
      if(gridX[i+1] != gridX[i]){
	histY_MPI[i] = histY_MPI[i]/(gridX[i+1]-gridX[i]);
	histY2_MPI[i] = histY2_MPI[i]/(gridX[i+1]-gridX[i]);
      }
      integral += histY[i];
    }
    integral_MPI = integral;
  }

  //analysis done
  inf_dataConsumed = 1;
}


void histogram::clean()
{
  for(int i = 0; i < size; i++){
    histN[i] = 0;
    histY[i] = 0.;
    histY2[i] = 0.;
    histN_MPI[i] = 0;
    histY_MPI[i] = 0.;
    histY2_MPI[i] = 0.;
  }

  nTotalCount = 0;
  integral = 0.;
  outLowCount = 0;
  outUppCount = 0;
  outLowY = 0.;
  outUppY = 0.;

  nTotalCount_MPI = 0;
  integral_MPI = 0.;
  outLowCount_MPI = 0;
  outUppCount_MPI = 0;
  outLowY_MPI = 0.;
  outUppY_MPI = 0.;

  buffSize = 0;

  inf_dataConsumed = 0;

  if(inf_fixedLimits == 0){
    xMin =  1.0e+307;
    xMax = -1.0e+307;
    for(int i = 0; i <= size; i++){
      gridX[i] = xMin + i*(xMax - xMin)/(size);
    }
    gridX[0]    = xMin;
    gridX[size] = xMax;
  }
}

void histogram::normalize()
{
  if(integral_MPI > 0.){
    for(int i = 0; i < size; i++){
      histY_MPI[i] =  histY_MPI[i]/integral_MPI;
      histY2_MPI[i] = histY2_MPI[i]/(integral_MPI*integral_MPI);
    }
    integral_MPI = 1.0;
  }
}

//multiplied distribution by coeff
void histogram::multiply(double coeff)
{
  for(int i = 0; i < size; i++){
    histY_MPI[i] =  histY_MPI[i]*coeff;
    histY2_MPI[i] = histY2_MPI[i]*coeff;
    histY[i] =  histY[i]*coeff;
    histY2[i] = histY2[i]*coeff;
  }
  integral = integral * coeff;
  integral_MPI = integral_MPI * coeff;

  outLowY = outLowY * coeff;
  outUppY = outUppY * coeff;

  outLowY_MPI = outLowY_MPI * coeff; 
  outUppY_MPI =outUppY_MPI * coeff;
}

int histogram::getSize(){ return size;}

double histogram::getMinX(){ return xMin;}
double histogram::getMaxX(){ return xMax;}

double histogram::getMinX(int index){ return gridX[index];}
double histogram::getMaxX(int index){ return gridX[index+1];}

double histogram::getValue(int index){ return histY_MPI[index];}

double histogram::getError(int index)
{
  if(histN_MPI[index] < 2) return 0.;
  double xAvg2N =  histY_MPI[index]*histY_MPI[index]/histN_MPI[index];
  double Dx = histY2_MPI[index] - xAvg2N;
  return sqrt(fabs(Dx)/(histN_MPI[index]-1));
}

int histogram::getCount(int index){ return histN_MPI[index];}

int histogram::getCount(){ return nTotalCount_MPI;}
double histogram::getIntegral(){ return integral_MPI;}

int histogram::getOutsideCountLow(){ return outLowCount_MPI;}
int histogram::getOutsideCountUpp(){ return outUppCount_MPI;}

double histogram::getOutsideSumLow(){ return outLowY_MPI;}
double histogram::getOutsideSumUpp(){ return outUppY_MPI;}

void histogram::print(std::ostream& Out)
{
  if(rank_MPI == 0){
    Out<<"% n cells = "<< getSize() <<"\n"
       <<"% minX = "<< getMinX() <<"\n"
       <<"% maxX = "<< getMaxX() <<"\n"
       <<"% total N = "<< getCount() <<"\n"
       <<"% integral = "<< getIntegral() <<"\n"
       <<"% outside lower N = "<< getOutsideCountLow() <<"\n"
       <<"% outside upper N = "<< getOutsideCountUpp() <<"\n"
       <<"% outside lower sum = "<< getOutsideSumLow() <<"\n"
       <<"% outside upper sum = "<< getOutsideSumUpp() <<"\n";

    Out<<"% #i      fromX           toX           avgX          N          sumW       dispersionW \n";

        for(int i = 0; i < size; i++){
          Out<<" "<< i
	     <<"   \t"<< gridX_MPI[i]
	     <<"   \t"<< gridX_MPI[i+1]
	     <<"   \t"<< (gridX_MPI[i] + gridX_MPI[i+1])/2.0
             <<"   \t"<< getCount(i)
             <<"   \t"<< getValue(i)
             <<"   \t"<< getError(i)
             <<" \n";
	  if(i % 1000 == 0) Out.flush();
	}
  }
}

void histogram::print(char* fileName)
{
  if(size_MPI == 1){
    ofstream F_out;
    F_out.open (fileName, ios::out);
    print(F_out);
    F_out.close();    
  }
  else{
    ofstream F_dump;
    if(rank_MPI == 0)F_dump.open (fileName, ios::out);
    print(F_dump);
    if(rank_MPI == 0){F_dump.close();}
    return;
  }
}

void histogram::finalizeExecution(char* message)
{
  if(iMPIini > 0){
    MPI_Finalize();
  }
  std::cerr<<message<<"\n";
  std::cerr<<"Stop from histogram class.\n";
  exit(1);  
}
