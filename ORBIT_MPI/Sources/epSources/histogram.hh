//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    histogram.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    08/07/2003
//
// DESCRIPTION
//    Specification and inline functions for a class used to 
//    make histogram from set of data
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include <iostream> 
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;

//MPI staff
#include "mpi.h"

#ifndef HISTOGRAM_H
#define HISTOGRAM_H
  
///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    histogram
//
// CLASS RELATIONSHIPS
//    independent
//
// DESCRIPTION
//    A class for making histogram from two types of data:
//    (double value) or (double value, double weight).
//    It has multiple CPU capability.     
//
// PUBLIC METHODS
//    histogram(int):                 Constructor without hard low and upper limits
//    histogram(double,double,int):   Constructor with hard low and upper limits
//    histogram(double*,int):         Constructor with an array defining a grid
//    ~histogram:                     Destructor for the histogram class
//    setBufferSize(int):             sets the inner buffer size ( by default it is 1000);
//    add(double):                    put data value into histogram
//    add(double,double):             put data value into histogram with weight
//    consumeData():                  carry out data analysis
//    normalize():                    make spectrum from histogram
//    getSize():                      returns number of intervals
//    getMinX():                      returns low value of the grid
//    getMaxX():                      returns upper value of the grid 
//    getMinX(int):                   returns low value for i-th cell of grid
//    getMaxX(int):                   returns upper value for i-th cell of grid
//    getValue(int):                  returns an indexed histogram value 
//    getError(int):                  returns an indexed histogram value error
//    getCount(int):                  returns an indexed histogram count for grid cell
//    getCount():                     returns the total histogram count
//    getIntegral():                  returns the integral over histogram
//    print(std::ostream& Out):       print with ostream
//    print(char* fileName):          print method which requests filename to print
//
// PRIVATE METHODS
//    init():                         initialize all arrays it called from constructor
//
// PRIVATE MEMBERS   
//    iMPIini        non0 means MPI_Initialized, and 0 means uninitialized
//    rank_MPI       rank of current CPU
//    size_MPI       number of whole CPU
//
// PROTECTED MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class  histogram 
{
public:
  //-----------------------------------------
  //the public methods of the histogram class
  //-----------------------------------------
  histogram(int sizeIn);
  histogram(double xMinIn,double xMaxIn, int sizeIn);
  histogram(double* xGrid, int nPoint);

  virtual ~histogram();

  void setBufferSize(int buffSizeMax);

  void add(double x);
  void add(double x,double weight);

  //-------------------------------------
  //this method should be called before 
  //any request for information or printing
  //-------------------------------------
  void analyze();

  void clean();

  void normalize();

  //multiplied distribution by coeff
  void multiply(double coeff);

  int getSize();
  double getMinX();
  double getMaxX();
  double getMinX(int index);
  double getMaxX(int index);

  double getValue(int index);
  double getError(int index);
  int getCount(int index);

  int getCount();
  double getIntegral();

  int getOutsideCountLow();
  int getOutsideCountUpp();

  double getOutsideSumLow();
  double getOutsideSumUpp();

  void print(std::ostream& Out);
  void print(char* fileName);

private:
  //------------------------------------------
  //the private methods of the histogram class
  //------------------------------------------
  void init();
  int getIndex(double* grd, double x);
  void transformGrid(double* fromGrid, double* toGrid);
  void consumeBuffer();
  void finalizeExecution(char* message);

private:
  //------------------------------------------
  //the private members of the histogram class
  //------------------------------------------

  //-----------------------
  //info variables
  //-----------------------

  //inf_fixedLimits = 1 if limits are fixed 0 - otherwise
  int inf_fixedLimits;

  //inf_ dataConsumed = 1 yes, 0 - otherwise
  int inf_dataConsumed;

  //number of cells in the grid
  int size;

  //min and max values
  double xMin, xMax;

  //gridX array with (size+1) element
  double* gridX;
  double* tmp_gridX;

  //histograms arrays keep sum of values
  double* histY;
  double* histY2;
  int*    histN;

  int nTotalCount;
  double integral;

  //outside values
  int outLowCount;
  int outUppCount;
  double outLowY;
  double outUppY;

  //this is used for parallel and non parallel case !!!
  double* gridX_MPI;
  double* histY_MPI;
  double* histY2_MPI;
  int*    histN_MPI;

  int nTotalCount_MPI;
  double integral_MPI;

  int outLowCount_MPI;
  int outUppCount_MPI;
  double outLowY_MPI;
  double outUppY_MPI;

  //buffer
  int buffMaxSize;
  int buffSize;
  double* xArr;
  double* wArr;

  int iMPIini; 
  int rank_MPI; 
  int size_MPI;
};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
