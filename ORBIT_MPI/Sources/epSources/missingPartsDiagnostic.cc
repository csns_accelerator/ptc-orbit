//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    missingPartsDiagnostic.cc
//
// AUTHOR
//    Y.Sato, A. Shishlo
//
// CREATED
//    12/15/2003
//
// DESCRIPTION
//    This class is supposed to be used from baseParticleTracker class to
//    collect information about particles that can not be treated properly
//    and will be considered as missing because we can not define an 
//    interaction point on the border.
//    This class will be used for debugging purpose mostly.
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "missingPartsDiagnostic.hh"
#include "epModuleConstants.hh"

#include "mpi.h"

missingPartsDiagnostic::missingPartsDiagnostic()
{

  //define the size of the arrays and increasing step (chunk)
  nStepsMax = 1000;
  nStepsMaxChunk = 1000;

  nStrikesArr = (int *) malloc (sizeof(int) * nStepsMax);
  nMissingArr = (int *) malloc (sizeof(int) * nStepsMax);
  strikesMacroSizeTotalArr = (double *) malloc (sizeof(double) * nStepsMax);
  missingMacroSizeTotalArr = (double *) malloc (sizeof(double) * nStepsMax);
  ctimeArr = (double *) malloc (sizeof(double) * nStepsMax);

  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0 ){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  }

  nStrikesArr_MPI = (int *) malloc (sizeof(int) * nStepsMax);
  nMissingArr_MPI = (int *) malloc (sizeof(int) * nStepsMax);
  strikesMacroSizeTotalArr_MPI = (double *) malloc (sizeof(double) * nStepsMax);
  missingMacroSizeTotalArr_MPI = (double *) malloc (sizeof(double) * nStepsMax);

  initilize();


}

missingPartsDiagnostic::~missingPartsDiagnostic()
{
  free(nStrikesArr);
  free(nMissingArr);
  free(strikesMacroSizeTotalArr);
  free(missingMacroSizeTotalArr);
  free(ctimeArr);

  free(nStrikesArr_MPI);
  free(nMissingArr_MPI);
  free(strikesMacroSizeTotalArr_MPI);
  free(missingMacroSizeTotalArr_MPI);

}


void missingPartsDiagnostic::initilize()
{
  //initial number of steps
  nSteps =   0;
  nStrikes = 0;
  nMissing = 0;
  strikesMacroSizeTotal = 0.;
  missingMacroSizeTotal = 0.;
  ctimeTotal = 0.;

  for(int i = 0; i < nStepsMax; i++){
    nStrikesArr[i] = 0;
    nMissingArr[i] = 0;
    strikesMacroSizeTotalArr[i] = 0.;
    missingMacroSizeTotalArr[i] = 0.;
    ctimeArr[i] = 0.;
  }

  for(int i = 0; i < nStepsMax; i++){
    nStrikesArr_MPI[i] = 0;
    nMissingArr_MPI[i] = 0;
    strikesMacroSizeTotalArr_MPI[i] = 0.;
    missingMacroSizeTotalArr_MPI[i] = 0.;
  }

}


//this method should be called at the beginning of tracking step
void missingPartsDiagnostic::accountTime(double ctimeStep)
{
  nSteps++;
  if(nSteps == nStepsMax){
    resize(nStepsMax + nStepsMaxChunk);
  }
  ctimeTotal += ctimeStep;
  ctimeArr[nSteps] = ctimeTotal;
}



void missingPartsDiagnostic::resize(int newStepN)
{
  int* tmp_int = (int *) malloc (sizeof(int) * newStepN);
  double* tmp_dbl = (double *) malloc (sizeof(double) * newStepN);

  //-------------------------------------------
  for(int i = 0; i < nStepsMax; i++){
    tmp_int[i] = nStrikesArr[i];
  }
  free(nStrikesArr);
  nStrikesArr = (int *) malloc (sizeof(int) * newStepN);
  for(int i = 0; i < nStepsMax; i++){
    nStrikesArr[i] = tmp_int[i];
  }
  for(int i = nStepsMax; i < newStepN; i++){
    nStrikesArr[i] = 0;
  }

  //-------------------------------------------
  for(int i = 0; i < nStepsMax; i++){
    tmp_int[i] = nMissingArr[i];
  }
  free(nMissingArr);
  nMissingArr = (int *) malloc (sizeof(int) * newStepN);
  for(int i = 0; i < nStepsMax; i++){
    nMissingArr[i] = tmp_int[i];
  }
  for(int i = nStepsMax; i < newStepN; i++){
    nMissingArr[i] = 0;
  }

  //-------------------------------------------
  for(int i = 0; i < nStepsMax; i++){
    tmp_dbl[i] = strikesMacroSizeTotalArr[i];
  }
  free(strikesMacroSizeTotalArr);
  strikesMacroSizeTotalArr = (double *) malloc (sizeof(double) * newStepN);
  for(int i = 0; i < nStepsMax; i++){
    strikesMacroSizeTotalArr[i] = tmp_dbl[i];
  }
  for(int i = nStepsMax; i < newStepN; i++){
    strikesMacroSizeTotalArr[i] = 0.;
  }

  //-------------------------------------------
  for(int i = 0; i < nStepsMax; i++){
    tmp_dbl[i] = missingMacroSizeTotalArr[i];
  }
  free(missingMacroSizeTotalArr);
  missingMacroSizeTotalArr = (double *) malloc (sizeof(double) * newStepN);
  for(int i = 0; i < nStepsMax; i++){
    missingMacroSizeTotalArr[i] = tmp_dbl[i];
  }
  for(int i = nStepsMax; i < newStepN; i++){
    missingMacroSizeTotalArr[i] = 0.;
  }

  //-------------------------------------------
  for(int i = 0; i < nStepsMax; i++){
    tmp_dbl[i] = ctimeArr[i];
  }
  free(ctimeArr);
  ctimeArr = (double *) malloc (sizeof(double) * newStepN);
  for(int i = 0; i < nStepsMax; i++){
    ctimeArr[i] = tmp_dbl[i];
  }
  for(int i = nStepsMax; i < newStepN; i++){
    ctimeArr[i] = 0.;
  }

  //-------------------------------------------
  free(nStrikesArr_MPI);
  free(nMissingArr_MPI);
  nStrikesArr_MPI = (int *) malloc (sizeof(int) * newStepN);
  nMissingArr_MPI = (int *) malloc (sizeof(int) * newStepN);
  for(int i = 0; i < newStepN; i++){
    nStrikesArr_MPI[i] = 0;
    nMissingArr_MPI[i] = 0;
  }

  //-------------------------------------------
  free(strikesMacroSizeTotalArr_MPI);
  free(missingMacroSizeTotalArr_MPI);
  strikesMacroSizeTotalArr_MPI = (double *) malloc (sizeof(double) * newStepN);
  missingMacroSizeTotalArr_MPI = (double *) malloc (sizeof(double) * newStepN);
  for(int i = 0; i < newStepN; i++){
    strikesMacroSizeTotalArr_MPI[i] = 0.;
    missingMacroSizeTotalArr_MPI[i] = 0.;
  }

  nStepsMax = newStepN;

  free(tmp_dbl);
  free(tmp_int);
}


void missingPartsDiagnostic::accountStrike(int index, eBunch* eb)
{
  double macro = eb->macroSize(index);
  nStrikes++;
  nStrikesArr[nSteps]++;
  strikesMacroSizeTotal += macro;
  strikesMacroSizeTotalArr[nSteps] += macro;
}


void missingPartsDiagnostic::accountMissing(int index, eBunch* eb)
{
  double macro = eb->macroSize(index);
  nMissing++;
  missingMacroSizeTotal += macro;
  nMissingArr[nSteps]++;
  missingMacroSizeTotalArr[nSteps] += macro;
}


int missingPartsDiagnostic::getStrikes()
{
  return nStrikes;
}

int missingPartsDiagnostic::getMissing()
{
  return nMissing;
}

double missingPartsDiagnostic::getStrikesMacroTotal()
{
  return strikesMacroSizeTotal;
}

double missingPartsDiagnostic::getMissingMacroTotal()
{
  return missingMacroSizeTotal;
}

int missingPartsDiagnostic::getStepsN()
{
  return nSteps;
}

double missingPartsDiagnostic::getTimeTotal()
{
  return ctimeTotal;
}

double missingPartsDiagnostic::getTime(int iStep)
{
  return ctimeArr[iStep];
}

int missingPartsDiagnostic::getStrikes(int iStep)
{
  return nStrikesArr[iStep];
}

int missingPartsDiagnostic::getMissing(int iStep)
{
  return nMissingArr[iStep];
}

double missingPartsDiagnostic::getStrikesMacro(int iStep)
{
  return strikesMacroSizeTotalArr[iStep];
}

double missingPartsDiagnostic::getMissingMacro(int iStep)
{
  return missingMacroSizeTotalArr[iStep];
}
void missingPartsDiagnostic::print(char* fileName)
{
  ofstream F_dump;
  if(rank_MPI == 0) F_dump.open (fileName, ios::out);
  print(F_dump);
  if(rank_MPI == 0) F_dump.close();
}

void missingPartsDiagnostic::print(std::ostream& Out)
{
   if(size_MPI > 1){
     int  nStepsAvg = 0;
     MPI_Allreduce(&nSteps,&nStepsAvg,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
     nStepsAvg = nStepsAvg/size_MPI;
     int nStepsDev = abs(nStepsAvg - nSteps);
     int nStepsDev_MPI = 0;
     MPI_Allreduce(&nStepsDev,&nStepsDev_MPI,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
     if(nStepsDev_MPI != 0){
       finalizeExecution("Size of the arrays are different for CPUs!!!");
     }
   }

   int nStrikes_MPI = nStrikes;
   int nMissing_MPI = nMissing;
   double strikesMacroSizeTotal_MPI = strikesMacroSizeTotal;
   double missingMacroSizeTotal_MPI = missingMacroSizeTotal;

   if(size_MPI > 1){
     MPI_Allreduce(nStrikesArr,nStrikesArr_MPI,nSteps,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
     MPI_Allreduce(nMissingArr,nMissingArr_MPI,nSteps,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

     MPI_Allreduce(strikesMacroSizeTotalArr,strikesMacroSizeTotalArr_MPI,nSteps,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);   
     MPI_Allreduce(missingMacroSizeTotalArr,missingMacroSizeTotalArr_MPI,nSteps,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

     MPI_Allreduce(&nStrikes,&nStrikes_MPI,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
     MPI_Allreduce(&nMissing,&nMissing_MPI,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
     MPI_Allreduce(&strikesMacroSizeTotal,&strikesMacroSizeTotal_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD); 
     MPI_Allreduce(&missingMacroSizeTotal,&missingMacroSizeTotal_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD); 

   }
   else{
       for(int i = 0; i < nStepsMax; i++){
	 nStrikesArr_MPI[i] = nStrikesArr[i];
	 nMissingArr_MPI[i] = nMissingArr[i];
	 strikesMacroSizeTotalArr_MPI[i] = strikesMacroSizeTotalArr[i];
	 missingMacroSizeTotalArr_MPI[i] = missingMacroSizeTotalArr[i];
       }
   }
   
   if(rank_MPI == 0){
     Out <<"% nStrikes = "<<nStrikes_MPI<<" macroSize="<<strikesMacroSizeTotal_MPI<<"\n";
     Out <<"% nMissing = "<<nMissing_MPI<<" macroSize="<<missingMacroSizeTotal_MPI<<"\n";
     Out <<"% ======================================================================\n";
     Out <<"% #i  time[ns]   nStrikes    MacroSize    nMissing   Macrosize \n";
     for(int i = 0; i < nSteps; i++){
       Out <<" "<<i
	     <<" "<<ctimeArr[i]*1.0e9/epModuleConstants::c
	     <<" "<<nStrikesArr_MPI[i]
	     <<" "<<strikesMacroSizeTotalArr_MPI[i]
	     <<" "<<nMissingArr_MPI[i]
	     <<" "<<missingMacroSizeTotalArr_MPI[i]
	     <<"\n";
       if(i % 1000 == 0) Out.flush();
     }
   }
   


}


void missingPartsDiagnostic::finalizeExecution(char* message)
{
  if(iMPIini > 0){
    MPI_Finalize();
  }
  std::cerr<<message<<"\n";
  std::cerr<<"Stop from missingPartsDiagnostic class.\n";
  exit(1);  
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
