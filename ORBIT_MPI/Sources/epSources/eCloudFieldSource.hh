//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    eCloudFieldSource.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    07/02/2003
//
// DESCRIPTION
//   The source of the electrostatic and magnetic fields. 
//   originated from the electron cloud. Keeps auxiliary Grid3Ds
//   for potential and space charge density.
//
//   The subclass of the BaseFieldSource.
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include "BaseFieldSource.hh"
#include "RhoGrid3D.hh"
#include "PhiGrid3D.hh"
#include "eBunch.hh"

#ifndef E_CLOUD_FIELD_HH
#define E_CLOUD_FIELD_HH

class eCloudFieldSource: public BaseFieldSource
{
public:

  eCloudFieldSource(int nZ,
                    double zGridMinIn,double zGridMaxIn, 
                    EP_Boundary* boundaryIn);

  virtual ~eCloudFieldSource();

  void createPotential(eBunch* eb);

  void addBoundaryPotential();

  Grid3D* getRhoGrid3D();
  Grid3D* getPhiGrid3D();
 
  void setMinMaxZ(double zGridMinIn,double zGridMaxIn);

  //--------------------------------------------------------
  //overridden method
  //---------------------------------------------------------

  //returns the electric field (for point like charge 1/(r*r))
  void getElectricField(double x,    double y,    double z, 
                        double& e_x, double& e_y, double& e_z);


private:

  //reference to the boundary
  EP_Boundary* boundary_;

  //local phiGrid (should be created and deleted inside)
  PhiGrid3D* phiGridLocal_;
  RhoGrid3D* rhoGridLocal_;

  int nZ_;

  double zGridMin_, zGridMax_;

  
};
#endif

