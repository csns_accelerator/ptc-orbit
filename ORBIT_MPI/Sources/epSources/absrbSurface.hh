//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    absrbSurface.hh
//
// AUTHOR
//    A. Shishlo, Y. Sato
//
// CREATED
//    04/01/2003
//
// DESCRIPTION
//    Specification and inline functions for a subclass used to 
//    absorb macro particles that hit the surface in the class EPnode
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "stdlib.h"
#include "baseSurface.hh"

#ifndef ABSORPTION_SURFACE_H
#define ABSORPTION_SURFACE_H

///////////////////////////////////////////////////////////////////////////
//
// SUBCLASS NAME
//    absrbSurface
//
// CLASS RELATIONSHIPS
// 
//    subclass absrbSurface -> class baseSurface
//    class baseSurface class EP_Boundary, class eBunch -> class EPnode
//
// DESCRIPTION
//    A subclass to absorb macro particles which hit the surface. 
//    Actually, it removes those macroparticles from the bunch of 
//    electrons in the EPnode
//
// PUBLIC METHODS
//    absrbSurface:    Constructor for making the subclass objects
//    ~absrbSurface:   Destructor for this subclass
//    impact:          remove macroparticles crossing the surface
//                     from the list of electron bunch
//
// PRIVATE/PROTECTED METHODS/MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class absrbSurface: public  baseSurface
{
public:

  absrbSurface();
  virtual ~absrbSurface();

  void impact(int index, eBunch* eb, double* r_v,double* n_v);
};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
