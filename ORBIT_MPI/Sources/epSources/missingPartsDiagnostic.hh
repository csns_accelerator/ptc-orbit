//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    missingPartsDiagnostic.hh
//
// AUTHOR
//    Y.Sato, A. Shishlo
//
// CREATED
//    12/15/2003
//
// DESCRIPTION
//    This class is supposed to be used from baseParticleTracker class to
//    collect information about particles that can not be treated properly
//    and will be considered as missing because we can not define an 
//    interaction point on the border. 
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "stdlib.h"

#ifndef MISSING_PARTS_DIAGNOSTICS_H
#define MISSING_PARTS_DIAGNOSTICS_H
  
#include "eBunch.hh"

class  missingPartsDiagnostic
{
public:

  //constructor
  missingPartsDiagnostic();

  //destructor
  virtual ~missingPartsDiagnostic();

  void initilize();
  void accountStrike(int index, eBunch* eb);
  void accountTime(double ctimeStep);
  virtual void accountMissing(int index, eBunch* eb);

  int getStrikes();
  int getMissing();
  double getStrikesMacroTotal();
  double getMissingMacroTotal();
  double getTimeTotal();

  int getStepsN();
  double getTime(int iStep);
  int getStrikes(int iStep);
  int getMissing(int iStep);
  double getStrikesMacro(int iStep);
  double getMissingMacro(int iStep);

  void print(char* fname);
  void print(std::ostream& Out);

private:

  void resize(int newStepN);

  void finalizeExecution(char* message);

  //max number of steps
  int nStepsMax;
  int nStepsMaxChunk;


  //number of steps that have been done
  int nSteps;

  int nStrikes;
  int nMissing;
  double strikesMacroSizeTotal;
  double missingMacroSizeTotal;
  double ctimeTotal;

  int* nStrikesArr;
  int* nMissingArr;
  double* strikesMacroSizeTotalArr;
  double* missingMacroSizeTotalArr;
  double* ctimeArr;

  //temporary used array to print information
  int* nStrikesArr_MPI;
  int* nMissingArr_MPI;
  double* strikesMacroSizeTotalArr_MPI;
  double* missingMacroSizeTotalArr_MPI;

  //MPI staff
  int rank_MPI;
  int size_MPI;
  int iMPIini;

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
