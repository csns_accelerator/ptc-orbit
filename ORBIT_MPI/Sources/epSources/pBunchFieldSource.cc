//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    pBunchFieldSource.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    07/02/2003
//
// DESCRIPTION
//   The source of the electrostatic and magnetic fields. 
//   originated from the proton bunch. Keeps auxilary Grid3D
//   for potential to produce E = gamma*grad(phi_in_the_pBunch_rest)
//   and Hz = 0, Hx = beta*Ey Hy=-betaEx.
//
//   The actual definition of the inner phi-grid should be done
//   by SpaceCharge3D class from ORBIT package or by pBunch.
//
//   The subclass of the BaseFieldSource
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <cstdio>

using namespace std;

#include "pBunchFieldSource.hh"
#include "epModuleConstants.hh"

pBunchFieldSource::pBunchFieldSource(int nZ,
                                     double zGridMinIn,double zGridMaxIn, 
                                     EP_Boundary* boundaryIn):
                   BaseFieldSource() 
{
  boundary_ = boundaryIn;
  nZ_ = nZ;
  zGridMin_ = zGridMinIn;
  zGridMax_ = zGridMaxIn;
  beta_  = 0.0;
  
  phiGridLocal_ = new Grid3D(nZ_,boundary_);
  phiGridLocal_->setMinMaxZ(zGridMin_,zGridMax_); 
  phiGridLocal_->clean();
}

pBunchFieldSource::~pBunchFieldSource()
{
  delete phiGridLocal_;
}

void pBunchFieldSource::setBeta(double betaIn){beta_ = betaIn;}

double pBunchFieldSource::getBeta(){return beta_;}

Grid3D* pBunchFieldSource::getPhiGrid3D(){return phiGridLocal_;}

void pBunchFieldSource::setMinMaxZ(double zGridMinIn,double zGridMaxIn)
{
  zGridMin_ = zGridMinIn;
  zGridMax_ = zGridMaxIn;  
  phiGridLocal_->setMinMaxZ(zGridMin_,zGridMax_);
}

//returns the H in Gauss/(electron charge in CGS)
void pBunchFieldSource::getMagneticField(double x,    double y,    double z, 
                                           double& h_x, double& h_y, double& h_z)
{
  phiGridLocal_->calcGradient(x,ex,y,ey,z,ez);
  h_x = -ey*beta_; h_y = ex*beta_; h_z = 0.;
}

//returns the electric field (for point like charge 1/(r*r))
void pBunchFieldSource::getElectricField(double x,    double y,    double z, 
                                           double& e_x, double& e_y, double& e_z)
{
  phiGridLocal_->calcGradient(x,e_x,y,e_y,z,e_z);
  e_x = - e_x; e_y = - e_y; e_z = - e_z;
}

