//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    CntrlSecEmSurface.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/31/2003
//
// DESCRIPTION
//    This is a subclass of the baseSurface class.
//    The controlled surface model with a secondary emission as 
//    specified by :
//    Ref.: PRST-AB5, 124404(2002)
//    Controlled surface means that this surface class can control
//    production and deleting of the new macro-electrons according to
//    the two function and three parameters
//    p_death(energy) - this function specifies the probability of 
//                      the death of the incoming macro-electrons.
//                      If this function was not specified the parameter
//                      p_death_default (0. by default) will be used.  
//    n_born(energy)  - number of the macro-particles that will be born
//                      as a function of the energy of the incoming 
//                      particle. If this function was not specified 
//                      number of the new macro-particles is 
//                      n_new_born_default (1 - by default).
//    p_el  - relative probability factor for elastic scattering process
//    p_rd  - relative probability factor for rediffusing process
//    p_ts  - relative probability factor for true secondary emission process
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <mpi.h>

#include "Function.hh"
#include "baseSurface.hh"

#ifndef CONTROLLED_SECONDARY_EMISSION_SURFACE_H
#define CONTROLLED_SECONDARY_EMISSION_SURFACE_H

class  CntrlSecEmSurface: public baseSurface
{
  ////////////////////////////////
  //PUBLIC MEMBERS
  ////////////////////////////////

public:

  CntrlSecEmSurface();
  CntrlSecEmSurface(int surfaceType);
  virtual ~CntrlSecEmSurface();

  void impact(int index, eBunch* eb, double* r_v, double* n_v);

  void setDeathFunction(Function* func);
  void setDeathProbability(double p_death_defaultIn);  
  
  void setNewBornNumberFunction(Function* func);
  void setNewBornNumber(int n_new_born_defaultIn);

  ///////////////////////
  //convenient methods
  ///////////////////////

  void setRelativeProbability(double p_elIn,double p_rdIn,double p_tsIn);

  void setMacroPartsHardLimitN(int macroPartsMaxNIn);

  int getMacroPartsHardLimitN();

protected:

  //0 for Copper, 1 for Stainless Steal
  void init(int typeIn);

  //calculates all arrays
  void init();

  //stop program with message
  void finalize(const char* message);

  ////////////////////////////////
  //PHYSICS FUNCTIONS
  ////////////////////////////////

  double calc_del_e(double ene0, double cos_theta0);
  double calc_del_r(double ene0, double cos_theta0);
  double calc_del_ts(double ene0, double cos_theta0);

  double randomEmissEnergy_e(double ene0);
  double randomEmissEnergy_r(double ene0);
  double randomEmissEnergy_ts(double dlt_ts, double ene0);

  //ANGLES and RESULTING MOMENTUM CALCULATION
  void calcEmissMomentum(double* r_v,double* n_v, double emissEnergy, double* pout_v);

  //chooses index according Monte-Carlo procedure 
  //with weights[0-n_point] that are not normalized
  int chooseIndex(int n_point, double* weights);

protected:

  ////////////////////////////////
  //MODEL PARAMETERS
  ////////////////////////////////

  //mass and charge (mass in MeV)
  double mass;
  double charge;

  //maximal number of emitted macro-electrons
  int N_ELECTRONS_MAX;

  //emitted angular spectrum
  double alpha_;

  //Backscattered electrons - parameters
  double P_1_e_infiniteE_, P_1_e_hat_, E_e_hat_, W_, p_, sigma_e_, e1_, e2_;

  //Rediffused electrons
  double P_1_r_infiniteE_, E_r_, r_, q_, r1_, r2_;

  //True-secondary electrons
  double delta_ts_hat_, E_ts_hat_, s_, t1_, t2_, t3_, t4_;

  //Total SEY
  double E_t_hat_, delta_t_hat_;

  //additional model parameters for the true-secondary component
  double* p_n_; 
  double* eps_n_;

  /////////////////////////////////////////////
  //AUXILIARY MEMBERS for PHYSICS
  /////////////////////////////////////////////

  //hard limit on number of macro-particles for all CPUs
  int macroPartsMaxN;

  Function* p_death_func;
  double p_death_default;

  Function* n_born_func;
  int  n_new_born_default;

  //p_el  - relative probability factor for elastic scattering process
  //p_rd  - relative probability factor for rediffusing process
  //p_ts  - relative probability factor for true secondary emission process  
  double  p_el;
  double  p_rd;
  double  p_ts;

  //cumulative distribution function for Gauss distribution
  Function* gaussInverseDistrFunc;

private:

  /////////////////////////////////////////////
  //AUXILIARY MATHEMATICAL METHODS and MEMBERS
  /////////////////////////////////////////////

  //binomial finction n1!/((n2!)*((n1-n2)!))
  double binomial(int n1,int n2);

  //this is the array with n! where n-is the array index
  double* factorial;

  //The commulative distribution functions for true secondary emission
  //for different number of emitted electrons from 1 up to N_ELECTRONS_MAX
  Function** distrFunc_ts;

  /////////////////////////////////////////////
  //AUXILIARY TEMPORARY MEMBERS
  /////////////////////////////////////////////
  //probability array (max size from 0-N_ELECTRONS_MAX)
  double* prob_array_tmp;

protected:
  //---------------------------------------
  //the private for MPI
  //---------------------------------------
  int iMPIini; int rank_MPI; int size_MPI;

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
