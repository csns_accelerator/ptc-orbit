/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   PBunchDiagNodeStore.cc
//
// AUTHORS
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/29/2004
//
// DESCRIPTION
//    PBunchDiagNodeStore class - singleton to keep references 
//    to the PBunchDiagNode  instances.
//
///////////////////////////////////////////////////////////////////////////// 

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "PBunchDiagNodeStore.hh"

#include <cstdlib>
#include <iostream>

#include "mpi.h"

PBunchDiagNodeStore* PBunchDiagNodeStore::pbdStore = NULL;

PBunchDiagNodeStore::PBunchDiagNodeStore()
{
  nPBunchDiagNodes= 0;
  pbdNodes = NULL;
}

PBunchDiagNodeStore::~PBunchDiagNodeStore()
{
  if(pbdNodes) delete [] pbdNodes;
}

PBunchDiagNodeStore* PBunchDiagNodeStore::getPBunchDiagNodeStore()
{

  if(!pbdStore){
     pbdStore= new  PBunchDiagNodeStore();
    return  pbdStore;
  }
  return pbdStore;
}

int PBunchDiagNodeStore::addPBunchDiagNode(PBunchDiagNode* pdN)
{
  PBunchDiagNode** eps_tmp = new PBunchDiagNode*[nPBunchDiagNodes+1];
  for(int i = 0; i < nPBunchDiagNodes; i++){
    eps_tmp[i] = pbdNodes[i];
  }
  eps_tmp[nPBunchDiagNodes] = pdN;
  if(nPBunchDiagNodes > 0){
    delete [] pbdNodes;
  }
  pbdNodes = eps_tmp;
  nPBunchDiagNodes++;

  return (nPBunchDiagNodes-1);
}


PBunchDiagNode* PBunchDiagNodeStore::getPBunchDiagNode(int index)
{
  if(index < nPBunchDiagNodes){
    return pbdNodes[index];
  }

  int iMPIini  = 0;
  int rank_MPI = 0;
  MPI_Initialized( &iMPIini);
  if (iMPIini) {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  }

  if( iMPIini > 0){
    MPI_Finalize();
  }
  if(rank_MPI == 0){
    std::cerr<<"Stop from PBunchDiagNodeStore class.\n";
    std::cerr<<"There is no node with index ="<<index<<std::endl;
    std::cerr<<"The Store has only PBunchDiagNodes ="<<nPBunchDiagNodes<<std::endl;
  }
  exit(1); 
  return NULL; 
}

int PBunchDiagNodeStore::getSize()
{
  return nPBunchDiagNodes;
}
