//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    SimpleEBunchDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/23/2003
//
// DESCRIPTION
//   This is a simple class for e-bunch diagnostics. It calculates:
//   1. radial distribution of the bunch
//   2. energy spectrum
//   3. macro-size distribution
//   4. longitudianal distribution
//   5. longitudianal momentum distribution
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef SIMPLE_EBUNCH_DIAGNISTICS_HH
#define SIMPLE_EBUNCH_DIAGNISTICS_HH

#include <iostream> 
#include <fstream>

#include "eBunch.hh"
#include "AbstractEBunchDiag.hh"
#include "histogram.hh"

class SimpleEBunchDiag : public AbstractEBunchDiag
{
public:

  //costructor parameters timeIn in [ns]
  SimpleEBunchDiag(int nTurnIn, double timeIn, char* fOutNameIn);
  virtual ~SimpleEBunchDiag();

  void init();
  void setZeroTurn();
  void startNewTurn();
  void analyze(int turn, double ctimeIn, eBunch* eb);
 
  void print();
  void print(std::ostream& Out);
  void print(char* fileName);

  //histogram methods
  void setEnergyHistogram(histogram* eH);
  void setRadialHistogram(histogram* rH);
  void setMacroSizeHistogram(histogram* mH);
  void setLongPositionHistogram(histogram* zH);
  void setLongMomentumHistogram(histogram* pzH);
  const histogram* getEnergyHistogram();
  const histogram* getRadialHistogram();
  const histogram* getMacroSizeHistogram();
  const histogram* getLongPositionHistogram();
  const histogram* getLongMomentumHistogram();


private:

  int nTurn;
  double ctime;

  double cCurrentTime;
  
  char* fOutName;


  //information about completion
  //inf_done - 0 - not yet 1 - done
  int inf_done;

  //histograms
  histogram* eH_;
  histogram* rH_;
  histogram* mH_;
  histogram* zH_;
  histogram* pzH_;

};

#endif

