//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    AbstractSurfaceDiag.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/22/2003
//
// DESCRIPTION
//   the abstract class for surface diagnostics. It is supposed to deal with
//   characteristics of particles hitting the surface and the heat production 
//   from them. This is the abstract-component class in a composite
//   pattern for surface diagnostic structure.
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "AbstractSurfaceDiag.hh"

const int AbstractSurfaceDiag::NAME_LENGTH_MAX = 100;

AbstractSurfaceDiag::AbstractSurfaceDiag()
{
  diagName = (char *) malloc (sizeof(char) * (NAME_LENGTH_MAX));
  strncpy(diagName,"NO NAME SURFACE DIAGNOSTICS",NAME_LENGTH_MAX);

  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  } 
}

AbstractSurfaceDiag::AbstractSurfaceDiag(char* diagNameIn)
{
  diagName = (char *) malloc (sizeof(char) * (NAME_LENGTH_MAX));
  strncpy(diagName,diagNameIn,NAME_LENGTH_MAX);

  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  } 
}

AbstractSurfaceDiag::~AbstractSurfaceDiag()
{
  free(diagName);
}

void AbstractSurfaceDiag::setName(char* newName)
{
  strncpy(diagName,newName,NAME_LENGTH_MAX);
}

const char* AbstractSurfaceDiag::getName()
{
  return diagName;
}


///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
