//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    CompositeEBunchDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/22/2003
//
// DESCRIPTION
//   It is a composite class for abstractEBunchDiag class. It registers 
//   the e-bunch diagnostics instances and delegates the analysis to them. 
//   The methods print(...) were implemented just for case. In general, 
//   each diagnostics should manages its own print method
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "CompositeEBunchDiag.hh"

CompositeEBunchDiag::CompositeEBunchDiag() : 
        AbstractEBunchDiag("COMPOSITE E-BUNCH DIAGNOSTICS")
{
  nComps = 0;
  nCompsMax = 10;
  nTurns = 0;
  components = new AbstractEBunchDiag*[nCompsMax];
}

CompositeEBunchDiag::~CompositeEBunchDiag()
{
  for(int i = 0; i < nComps; i++){
    delete components[i];
  }
  delete [] components;
}

void CompositeEBunchDiag::init(){
  for(int i = 0; i < nComps; i++){
    components[i]->init();
  }
}

void CompositeEBunchDiag::setZeroTurn()
{
  nTurns = 0;
}

void CompositeEBunchDiag::startNewTurn(){
  nTurns++;
  for(int i = 0; i < nComps; i++){
    components[i]->startNewTurn();
  }
}

void CompositeEBunchDiag::analyze(double ctime, eBunch* eb){
  for(int i = 0; i < nComps; i++){
    components[i]->analyze(nTurns,ctime,eb);
  }
}

void CompositeEBunchDiag::analyze(int nTurn, double ctime, eBunch* eb){
  //empty here
}

void CompositeEBunchDiag::print(ostream& Out){
  for(int i = 0; i < nComps; i++){
    components[i]->print(Out);
  }
}

void CompositeEBunchDiag::print(char* fileName){
  ofstream F_dump;
  if(rank_MPI == 0)F_dump.open (fileName, ios::out);
  if(rank_MPI == 0)F_dump<<"====="<<getName()<<"======="<<std::endl;
  if(rank_MPI == 0)F_dump<<"turn # = "<< nTurns <<std::endl;
  for(int i = 0; i < nComps; i++){
    if(rank_MPI == 0) {
      F_dump <<"= "<<components[i]->getName()
	     <<" ="<<std::endl;
    }
    components[i]->print(F_dump);
  }  
  if(rank_MPI == 0){F_dump.close();}
}

void CompositeEBunchDiag::addDiagnostics(AbstractEBunchDiag* diag)
{
  if(nComps == nCompsMax){
    AbstractEBunchDiag** tmp_comps = new AbstractEBunchDiag*[nCompsMax+1];
    for(int i = 0; i < nComps; i++){
      tmp_comps[i] = components[i];
    }
    delete [] components;
    components = new AbstractEBunchDiag*[nCompsMax+1];
    for(int i = 0; i < nComps; i++){
      components[i] = tmp_comps[i];
    }
    delete [] tmp_comps;
    nCompsMax++;
  }
  
  components[nComps] = diag;
  nComps++;
}

int CompositeEBunchDiag::getNumberOfDiagnostics(){ return nComps;}

AbstractEBunchDiag* CompositeEBunchDiag::getDiagnostics(int index)
{
  if(index < nComps && index >= 0){
    return components[index];
  }
  return NULL;
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
