/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   PBunchDiagNode.cc
//
// AUTHORS
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/29/2004
//
// DESCRIPTION
//     PBunchDiagNode class
//
///////////////////////////////////////////////////////////////////////////// 

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Particles.h"
#include "MacroPart.h"


#include "PBunchDiagNode.hh"

#include "epModuleConstants.hh"

#include "mpi.h"

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>

using namespace std;

Define_Standard_Members(PBunchDiagNode, Node);

PBunchDiagNode::PBunchDiagNode(String &n, 
		 int order, 
		 int zSlices_In,
		 int  nRecordsMax_In,
		 int diagType_In, 
		 const char* typeName_In,
		 const char* fName_In) :
  Node(n, 0., order)
{

  nTurn = 0;
	
	bunch_factor = 1.0;

  //default no recording
  infRecording = 0;

  nRecordsMax = nRecordsMax_In;
  nRecords = 0;
  
  zSlices = zSlices_In;
  diagType = diagType_In;

  int MAX_LENGTH_STRING = 256;
  typeName = (char *) malloc (sizeof(char) * (MAX_LENGTH_STRING));
  fName    = (char *) malloc (sizeof(char) * (MAX_LENGTH_STRING));
  strncpy(typeName,typeName_In,MAX_LENGTH_STRING);
  strncpy(fName,fName_In,MAX_LENGTH_STRING);

  //number of column to print with max values
  nPrintColumn = 10;
  if(nPrintColumn > zSlices) nPrintColumn = zSlices;
  iPrintColumn = (int*) malloc (sizeof(int) * (nPrintColumn));


  nRecColumn = zSlices;
  if(diagType == 1 || diagType == 3 || 
     diagType == 5 || diagType == 7 || 
     diagType == 9 || diagType == 10 ||
     diagType == 11 || diagType == 14 || diagType == 15 ||
     diagType == 18 || diagType == 19)
    {
      nRecColumn = (zSlices/2) + 1;
    }

  records = (double**) malloc (sizeof(double*) * (nRecordsMax));
  nCountsArr = (int*) malloc (sizeof(int) * (zSlices));
  turnInf = (int*) malloc (sizeof(int) * (nRecordsMax));
  maxValuesArr = (double*) malloc (sizeof(double) * (zSlices));

  buff_dbl_MPI = (double*) malloc (sizeof(double) * (zSlices));
  buff_int_MPI = (int*) malloc (sizeof(int) * (zSlices));

  centArrX = (double*) malloc (sizeof(double) * (zSlices));
  centArrY = (double*) malloc (sizeof(double) * (zSlices));

  r2AvgArr =  (double*) malloc (sizeof(double) * (zSlices));

  X2Arr =  (double*) malloc (sizeof(double) * (zSlices));
  Y2Arr =  (double*) malloc (sizeof(double) * (zSlices));

  //define step
  phi_step = 2.0*epModuleConstants::PI/zSlices;

  //we are using the simpliest form of FFT complex to complex
  //but in this case we need only (zSlices/2 + 1) FFT coeficients
  //because the input data is real.

  inFFT  = (FFTW_COMPLEX *) new char[zSlices* sizeof(FFTW_COMPLEX)];
  outFFT = (FFTW_COMPLEX *) new char[zSlices* sizeof(FFTW_COMPLEX)];
  planForward = fftw_create_plan(zSlices, FFTW_FORWARD, FFTW_ESTIMATE);
  //planForward = fftw_create_plan(zSlices, FFTW_FORWARD, FFTW_ESTIMATE);
  //fftw_one(planForward,inFFT,outFFT);

  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  } 
}

PBunchDiagNode::~PBunchDiagNode()
{ 
  free(typeName);
  free(fName);
  free(iPrintColumn);
  free(nCountsArr);
  free(turnInf);
  free(maxValuesArr);

  free(buff_dbl_MPI);
  free(buff_int_MPI);

  free(centArrX);
  free(centArrY);

  free(r2AvgArr);

  free(X2Arr);
  free(Y2Arr);

  for(int i = 0; i < nRecords; i++){
    free(records[i]);
  }

  free(records);

  fftw_destroy_plan(planForward);
  delete [] inFFT;
  delete [] outFFT; 
}

Void PBunchDiagNode::nameOut(String &wname) { wname = _name; };


Void PBunchDiagNode::_nodeCalculator(MacroPart &mp)
{
}

Void PBunchDiagNode::_updatePartAtNode(MacroPart &mp)
{
  nTurn++;
  if(infRecording == 0) return;

  if(nRecords == nRecordsMax){
    //we will use the first record again as the latest
    double* tmp_arr = records[0];
    for(int i = 1; i < nRecords; i++){
      records[i-1] = records[i];
      turnInf[i-1] = turnInf[i];
    }
    records[nRecords-1] = tmp_arr;
    turnInf[nRecords-1] = nTurn; 
  }
  else{
    records[nRecords] = (double*) malloc (sizeof(double) * (zSlices));
    turnInf[nRecords] = nTurn;
    nRecords++;
  }


  //call appropriate diagnostics
  if(diagType == 0){
    makeLongDensityDiag(mp);
  }
  else if(diagType == 1){
    makeLongDensityFFTDiag(mp);
  }
  else if(diagType == 2){
    makeCentroidXDiag(mp);
  }
  else if(diagType == 3){
    makeCentroidXFFTDiag(mp);
  }
  else if(diagType == 4){
    makeCentroidYDiag(mp);
  }
  else if(diagType == 5){
    makeCentroidYFFTDiag(mp);
  }
  else if(diagType == 6){
    makeR2Diag(mp);
  }
  else if(diagType == 7){
    makeR2FFTDiag(mp);
  }
  else if(diagType == 8){
    makeQuadMomDiag(mp);
  }
  else if(diagType == 9){
    makeQuadMomFFTDiag(mp);
  }
  else if(diagType == 10){
     makeCentroidXWFFTDiag(mp);
  }
  else if(diagType == 11){
    makeCentroidYWFFTDiag(mp);
  } 
  else if(diagType == 12){
     makeX2Diag(mp);
  }
  else if(diagType == 13){
    makeY2Diag(mp);
  } 
  else if(diagType == 14){
     makeX2FFTDiag(mp);
  }
  else if(diagType == 15){
    makeY2FFTDiag(mp);
  }
  else if(diagType == 16){
     makeXplusYDiag(mp);
  }
  else if(diagType == 17){
    makeXminsYDiag(mp);
  } 
  else if(diagType == 18){
     makeXplusYFFTDiag(mp);
  }
  else if(diagType == 19){
    makeXminsYFFTDiag(mp);
  }

}

void PBunchDiagNode::startRecording()
{
  infRecording = 1;
}

void PBunchDiagNode::stopRecording()
{
   infRecording = 0; 
}

void PBunchDiagNode::dumpDiagnostics()
{
  if(rank_MPI == 0){
    std::ofstream F_dump;
    F_dump.open (fName, ios::out);

    F_dump<<"% type of diagnostic : "<<typeName<<std::endl;
    F_dump<<"% turn i= ";
    for(int i = 0; i < nRecColumn; i++){
      F_dump<<std::setw(11)<<i<<" ";
    }

    F_dump<<std::endl;

    for(int ir = 0; ir <  nRecords; ir++){
      F_dump<<std::setw(6)<<turnInf[ir]<<"       ";
      for(int i = 0; i < nRecColumn; i++){
	F_dump<<std::setw(11)<<std::setprecision(3)
	      <<std::setiosflags(ios::scientific)
	      <<records[ir][i]<<" ";
      }
      F_dump<<std::endl;
    } 

    F_dump.close();
  }
}

void PBunchDiagNode::dumpMaxDiagnostics()
{
  //find first nPrintColumn maximal column
  for(int i = 0; i < nRecColumn; i++){
    maxValuesArr[i] = 0.;
  }

  double tmp_max_global = 0.;
  int    ind_max_global = -1;  
  for(int i = 0; i < nRecColumn; i++){
    double tmp_max = 0.;
    for(int ir = 0; ir <  nRecords; ir++){
      if(tmp_max < fabs(records[ir][i])) tmp_max = fabs(records[ir][i]);
    }
    maxValuesArr[i] = tmp_max;
    if(tmp_max_global < tmp_max){
      tmp_max_global = tmp_max;
      ind_max_global = i;
    }
  }

  std::ofstream F_dump;

  if(ind_max_global < 0){
    if(rank_MPI == 0){
      F_dump.open (fName, ios::out);
      F_dump<<"% type of diagnostic:"<<typeName<<std::endl;
      F_dump<<"Nothing to show."<<std::endl;
      F_dump.close();
    }
  }

  iPrintColumn[0] = ind_max_global;

  int nPrintColumn_local = 1;

  for(int ip = 1; ip < nPrintColumn; ip++){
    tmp_max_global = 0.;
    ind_max_global = -1;
    
    for(int i = 0; i < nRecColumn; i++){
      int inf = 1;
      for(int ic = 0; ic < nPrintColumn_local; ic++){
        if(iPrintColumn[ic] == i) inf = -1;
      }
      if(inf == -1) continue;
      if(tmp_max_global < maxValuesArr[i]){
        tmp_max_global = maxValuesArr[i];
        ind_max_global = i;
      }
    }

    if(ind_max_global < 0) continue;
    iPrintColumn[nPrintColumn_local] = ind_max_global;
    nPrintColumn_local++;
  }

  if(rank_MPI == 0){
    F_dump.open (fName, ios::out);
    F_dump<<"% type of diagnostic : "<<typeName<<std::endl;
    F_dump<<"% turn i= ";
    for(int i = 0; i < nPrintColumn_local; i++){
      F_dump<<std::setw(11)<<iPrintColumn[i]<<" ";
    }

    F_dump<<std::endl;

    for(int ir = 0; ir <  nRecords; ir++){
      F_dump<<std::setw(6)<<turnInf[ir]<<"       ";
      for(int i = 0; i < nPrintColumn_local; i++){
	F_dump<<std::setw(11)<<std::setprecision(3)
	      <<std::setiosflags(ios::scientific)
	      <<records[ir][iPrintColumn[i]]<<" ";
      }
      F_dump<<std::endl;
    } 

    F_dump.close();
  }
}


//it will work only for longitudinal diagnostics
double PBunchDiagNode::getBunchFactor(){
	return bunch_factor;
}

int PBunchDiagNode::getIndex(double phi)
{
  int ind = (int) ((phi + epModuleConstants::PI)/phi_step);
  if(ind >= zSlices) ind = zSlices - 1;
  if(ind < 0) ind = 0;
  return ind;
}

void PBunchDiagNode::setRecordToZero()
{
  for(int i = 0; i < zSlices; i++){
    records[nRecords-1][i] = 0.;
    nCountsArr[i] = 0;
  }
}

void PBunchDiagNode::allReduceRecord()
{
  if(size_MPI > 1){
    double* tmp_arr = records[nRecords-1];
    MPI_Allreduce(tmp_arr,buff_dbl_MPI,zSlices,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    for(int i = 0; i < zSlices; i++){
      tmp_arr[i] = buff_dbl_MPI[i];
    }

    MPI_Allreduce(nCountsArr,buff_int_MPI,zSlices,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    for(int i = 0; i < zSlices; i++){
      nCountsArr[i] = buff_int_MPI[i];
    }
  }
}

void PBunchDiagNode::makeRecordFFT()
{
  double* record =  records[nRecords-1];
  for(int i = 0; i < zSlices; i++){
    c_re(inFFT[i]) = record[i];
    c_im(inFFT[i]) = 0.;      
  }

  fftw_one(planForward,inFFT,outFFT);

  for(int i = 0; i < zSlices; i++){
    record[i] = sqrt( (double) (fabs(c_re(outFFT[i])*c_re(outFFT[i]) + 
				     c_im(outFFT[i])*c_im(outFFT[i]))));
    record[i] /= zSlices;     
  } 
}


//Calculates <x>*LongDensWeight centroid as function of phi
void PBunchDiagNode::makeCentroidXWDiag(MacroPart &mp)
{
  double* record =  records[nRecords-1];

  setRecordToZero();

  int bunchSize = mp._nMacros; 

  int ind;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    record[ind] += mp._x(ip);
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  int nCountMax = 0;
  for(int i = 0; i < zSlices; i++){
    if(nCountMax < nCountsArr[i]) nCountMax = nCountsArr[i];
  }

  if(nCountMax == 0){
    for(int i = 0; i < zSlices; i++){
      record[i] = 0.;
    }
  }
  else{
    for(int i = 0; i < zSlices; i++){
      record[i] /= nCountMax;
    }
  }
}

//Calculates <y>*LongDensWeight centroid as function of phi
void PBunchDiagNode::makeCentroidYWDiag(MacroPart &mp)
{
  double* record =  records[nRecords-1];

  setRecordToZero();

  int bunchSize = mp._nMacros;

  int ind;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    record[ind] += mp._y(ip);
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  int nCountMax = 0;
  for(int i = 0; i < zSlices; i++){
    if(nCountMax < nCountsArr[i]) nCountMax = nCountsArr[i];
  }

  if(nCountMax == 0){
    for(int i = 0; i < zSlices; i++){
      record[i] = 0.;
    }
  }
  else{
    for(int i = 0; i < zSlices; i++){
      record[i] /= nCountMax;
    }
  }
}

//=============================================================
//DIAGNOSTICS METHODS
//=============================================================


//diagType = 0 - longitudinal density
void PBunchDiagNode::makeLongDensityDiag(MacroPart &mp)
{
  double* record =  records[nRecords-1];

  setRecordToZero();

  int bunchSize = mp._nMacros;

  int ind;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    record[ind] += 1.;
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  int bunchSizeTotal = 0;
  for(int i = 0; i < zSlices; i++){ 
    bunchSizeTotal += nCountsArr[i];
  }
	
	int max_record = 0;
	for(int i = 0; i < zSlices; i++){
		if(max_record < nCountsArr[i]){
			max_record = nCountsArr[i];
		}
	}

	bunch_factor = 1.0;
	if(max_record != 0){
		bunch_factor = (1.0*bunchSizeTotal/zSlices)/max_record;
	}

  if(bunchSizeTotal == 0) return;
  
  for(int i = 0; i < zSlices; i++){
    record[i] /= bunchSizeTotal;
  }
}

//diagType = 1 - longitudinal density FFT
void PBunchDiagNode::makeLongDensityFFTDiag(MacroPart &mp)
{
  makeLongDensityDiag(mp);
  makeRecordFFT();
}

//diagType = 2 - <x> centroid as function of phi
void PBunchDiagNode::makeCentroidXDiag(MacroPart &mp)
{
  double* record =  records[nRecords-1];

  setRecordToZero();

  int bunchSize = mp._nMacros;

  int ind;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    record[ind] += mp._x(ip);
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  for(int i = 0; i < zSlices; i++){
    if( nCountsArr[i] != 0 ){
      record[i] /= nCountsArr[i];
    }
    else{
      record[i] = 0.;
    }
  }
}

//diagType = 3 - FFT of <x> centroid as function of phi
void PBunchDiagNode::makeCentroidXFFTDiag(MacroPart &mp)
{
  makeCentroidXDiag(mp);
  makeRecordFFT();
}

//diagType = 4 - <y> centroid as function of phi
void PBunchDiagNode::makeCentroidYDiag(MacroPart &mp)
{
  double* record =  records[nRecords-1];

  setRecordToZero();

  int bunchSize = mp._nMacros;

  int ind;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    record[ind] += mp._y(ip);
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  for(int i = 0; i < zSlices; i++){
    if( nCountsArr[i] != 0 ){
      record[i] /= nCountsArr[i];
    }
    else{
      record[i] = 0.;
    }
  }
}

//diagType = 5 - FFT of <y> centroid as function of phi
void PBunchDiagNode::makeCentroidYFFTDiag(MacroPart &mp)
{
  makeCentroidYDiag(mp);
  makeRecordFFT();
}

//diagType = 6 - sqrt(<(x-<x>)^2+(y-<y>)^2>) as function of phi
void PBunchDiagNode::makeR2Diag(MacroPart &mp)
{

  double* record =  records[nRecords-1];

  makeCentroidXDiag(mp);
  for(int i = 0; i < zSlices; i++){
    centArrX[i] = record[i];
  }

  makeCentroidYDiag(mp);
  for(int i = 0; i < zSlices; i++){
    centArrY[i] = record[i];
  }

  setRecordToZero();

  int bunchSize = mp._nMacros;

  int ind;
  double x,y;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    x = mp._x(ip) - centArrX[ind];
    y = mp._y(ip) - centArrY[ind];
    record[ind] += (x*x+y*y);
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  for(int i = 0; i < zSlices; i++){
    if( nCountsArr[i] != 0 ){
      record[i] /= nCountsArr[i];
      record[i] = sqrt(record[i]);
    }
    else{
      record[i] = 0.;
    }
  }
}

//diagType = 7 - FFT of sqrt(<(x-<x>)^2+(y-<y>)^2>) as function of phi
void PBunchDiagNode::makeR2FFTDiag(MacroPart &mp)
{
  makeR2Diag(mp);
  makeRecordFFT();
}

//diagType = 8 - quad moment<(x-<x>)^2>-<(y-<y>)^2> as function of phi
void PBunchDiagNode::makeQuadMomDiag(MacroPart &mp)
{
  double* record =  records[nRecords-1];

  makeCentroidXDiag(mp);
  for(int i = 0; i < zSlices; i++){
    centArrX[i] = record[i];
  }

  makeCentroidYDiag(mp);
  for(int i = 0; i < zSlices; i++){
    centArrY[i] = record[i];
  }

  makeR2Diag(mp);
  for(int i = 0; i < zSlices; i++){
    r2AvgArr[i] = record[i]*record[i];
  }

  setRecordToZero();

  int bunchSize = mp._nMacros;

  int ind;
  double x,y;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    x = mp._x(ip) - centArrX[ind];
    y = mp._y(ip) - centArrY[ind];
    record[ind] += (x*x-y*y);
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  for(int i = 0; i < zSlices; i++){
    if( nCountsArr[i] != 0 && r2AvgArr[i] > 0.){
      record[i] /= (r2AvgArr[i]*nCountsArr[i]);
    }
    else{
      record[i] = 0.;
    }
  }
}

//diagType = 9 - FFT of quad moment <(x-<x>)^2>-<(y-<y>)^2> as function of phi
void PBunchDiagNode::makeQuadMomFFTDiag(MacroPart &mp)
{
  makeQuadMomDiag(mp);
  makeRecordFFT();
}


//diagType = 10 - FFT of <x>*longDensWeight centroid as function of phi
void PBunchDiagNode::makeCentroidXWFFTDiag(MacroPart &mp)
{
  makeCentroidXWDiag(mp);
  makeRecordFFT();
}

//diagType = 11 - FFT of <y>*longDensWeight centroid as function of phi
void PBunchDiagNode::makeCentroidYWFFTDiag(MacroPart &mp)
{
  makeCentroidYWDiag(mp);
  makeRecordFFT();
}

//diagType = 12 - sqrt(<(x-<x>)^2) as function of phi
void PBunchDiagNode::makeX2Diag(MacroPart &mp)
{

  double* record =  records[nRecords-1];

  makeCentroidXDiag(mp);
  for(int i = 0; i < zSlices; i++){
    centArrX[i] = record[i];
  }

  setRecordToZero();

  int bunchSize = mp._nMacros;

  int ind;
  double x;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    x = mp._x(ip) - centArrX[ind];
    record[ind] += (x*x);
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  for(int i = 0; i < zSlices; i++){
    if( nCountsArr[i] != 0 ){
      record[i] /= nCountsArr[i];
      record[i] = sqrt(record[i]);
    }
    else{
      record[i] = 0.;
    }
  }
}

//diagType = 13 - sqrt(<(y-<y>)^2) as function of phi
void PBunchDiagNode::makeY2Diag(MacroPart &mp)
{

  double* record =  records[nRecords-1];

  makeCentroidYDiag(mp);
  for(int i = 0; i < zSlices; i++){
    centArrY[i] = record[i];
  }

  setRecordToZero();

  int bunchSize = mp._nMacros;

  int ind;
  double y;
  for(int ip = 1; ip <= bunchSize; ip++){
    ind = getIndex(mp._phi(ip));
    y = mp._y(ip) - centArrY[ind];
    record[ind] += (y*y);
    nCountsArr[ind] += 1;
  }

  allReduceRecord();

  for(int i = 0; i < zSlices; i++){
    if( nCountsArr[i] != 0 ){
      record[i] /= nCountsArr[i];
      record[i] = sqrt(record[i]);
    }
    else{
      record[i] = 0.;
    }
  }
}

//diagType = 14 - FFT of sqrt(<(x-<x>)^2) as function of phi
void PBunchDiagNode::makeX2FFTDiag(MacroPart &mp)
{
  makeX2Diag(mp);
  makeRecordFFT();
}

//diagType = 15 - FFT of sqrt(<(y-<y>)^2) as function of phi
void PBunchDiagNode::makeY2FFTDiag(MacroPart &mp)
{
  makeY2Diag(mp);
  makeRecordFFT();
}

//diagType = 16 - (sqrt(<(x-<x>)^2)+sqrt(<(y-<y>)^2)) as function of phi
void PBunchDiagNode::makeXplusYDiag(MacroPart &mp)
{
  double* record =  records[nRecords-1];

  makeX2Diag(mp);
  for(int i = 0; i < zSlices; i++){
    X2Arr[i] = record[i];
  }
  makeY2Diag(mp);
  for(int i = 0; i < zSlices; i++){
    Y2Arr[i] = record[i];
  }  
  setRecordToZero();

  for(int i = 0; i < zSlices; i++){
    record[i] = X2Arr[i]+Y2Arr[i];
  }

}

//diagType = 17 - (sqrt(<(x-<x>)^2)-sqrt(<(y-<y>)^2)) as function of phi
void PBunchDiagNode::makeXminsYDiag(MacroPart &mp)
{
  double* record =  records[nRecords-1];

  makeX2Diag(mp);
  for(int i = 0; i < zSlices; i++){
    X2Arr[i] = record[i];
  }
  makeY2Diag(mp);
  for(int i = 0; i < zSlices; i++){
    Y2Arr[i] = record[i];
  }  
  setRecordToZero();

  for(int i = 0; i < zSlices; i++){
    record[i] = X2Arr[i]-Y2Arr[i];
  }

}

//diagType = 18 - FFT of (sqrt(<(x-<x>)^2)+sqrt(<(y-<y>)^2)) as function of phi
void PBunchDiagNode::makeXplusYFFTDiag(MacroPart &mp)
{
  makeXplusYDiag(mp);
  makeRecordFFT();
}

//diagType = 19 - FFT of (sqrt(<(x-<x>)^2)-sqrt(<(y-<y>)^2)) as function of phi
void PBunchDiagNode::makeXminsYFFTDiag(MacroPart &mp)
{
  makeXminsYDiag(mp);
  makeRecordFFT();
}
