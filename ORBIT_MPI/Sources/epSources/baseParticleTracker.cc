//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    baseParticleTracker.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    06/17/2003
//
// DESCRIPTION
//   the base class to move macro-particle in the electromagnetic field
//   This class needs one or more BaseFieldSource or 
//   BaseFieldSource.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <cstdio>

#include "baseParticleTracker.hh"

using namespace std;

baseParticleTracker::baseParticleTracker(){

  nElSources = 0;
  nMgSources = 0;

  nSteps_ = 1;

  missingDiag = NULL;
  surfaceDiag = NULL;

  macroSize_dead = 0.0;
  energy_absorbed = 0.0;

}

baseParticleTracker::~baseParticleTracker(){
  if(nElSources > 0){
    delete [] electricSources;
  }
  if(nMgSources > 0){
    delete [] magneticSources;
  }
}

void baseParticleTracker::setMissingPartsDiagnostics(missingPartsDiagnostic* missingDiagIn)
{
  missingDiag = missingDiagIn;
}

missingPartsDiagnostic* baseParticleTracker::getMissingPartsDiagnostics()
{
  return missingDiag;
}

void baseParticleTracker::setSurfaceDiagnostics(CompositeSurfaceDiag* surfaceDiagIn)
{
  surfaceDiag =  surfaceDiagIn;
}

CompositeSurfaceDiag* baseParticleTracker::getSurfaceDiagnostics()
{
  return surfaceDiag;
}

void baseParticleTracker::setNumberOfSteps(int nSteps)
{
  nSteps_ = nSteps;
}

void baseParticleTracker::addMagneticFieldSource(BaseFieldSource* fs)
{
  BaseFieldSource** fss_tmp = new BaseFieldSource*[nMgSources+1];
  for(int i = 0; i < nMgSources; i++){
    fss_tmp[i] = magneticSources[i];
  }
  fss_tmp[nMgSources] = fs;
  if(nMgSources > 0){
    delete [] magneticSources;
  }
  magneticSources = fss_tmp;
  nMgSources++;
}

void baseParticleTracker::addElectricFieldSource(BaseFieldSource* fs)
{
  BaseFieldSource** fss_tmp = new BaseFieldSource*[nElSources+1];
  for(int i = 0; i < nElSources; i++){
    fss_tmp[i] = electricSources[i];
  }
  fss_tmp[nElSources] = fs;
  if(nElSources > 0){
    delete [] electricSources;
  }
  electricSources = fss_tmp;
  nElSources++;
}

void baseParticleTracker::removeMagneticFieldSource(BaseFieldSource* fs)
{
  int remInd = -1;
  for(int i = 0; i < nMgSources; i++){
    if(fs == magneticSources[i]) {
      remInd = i;
      break;
    }
  }

  if(remInd < 0) return;

  if(nMgSources == 1){
    delete [] magneticSources;
    magneticSources = NULL;
    nMgSources = 0;
    return;
  }

  BaseFieldSource** fss_tmp = new BaseFieldSource*[nMgSources-1];
  int count = 0;
  for(int i = 0; i < nMgSources; i++){
    if(i != remInd ){
      fss_tmp[count] = magneticSources[i];
      count++;
    }
  }

  delete [] magneticSources;
  magneticSources = fss_tmp;
  nMgSources--;
}

void baseParticleTracker::removeElectricFieldSource(BaseFieldSource* fs)
{
  int remInd = -1;
  for(int i = 0; i < nElSources; i++){
    if(fs == electricSources[i]) {
      remInd = i;
      break;
    }
  }

  if(remInd < 0) return;

  if(nElSources == 1){
    delete [] electricSources;
    electricSources = NULL;
    nElSources = 0;
    return;
  }

  BaseFieldSource** fss_tmp = new BaseFieldSource*[nElSources-1];
  int count = 0;
  for(int i = 0; i < nElSources; i++){
    if(i != remInd ){
      fss_tmp[count] = electricSources[i];
      count++;
    }
  }

  delete [] electricSources;
  electricSources = fss_tmp;
  nElSources--;
}

void baseParticleTracker::moveParticlesSimplectic(double ctime, 
						  eBunch* eb, 
						  Grid3D* rhoGrid3D,
						  baseSurface* surface)
{
  if(surfaceDiag){
    surfaceDiag->accountTime(ctime);
  }

  EP_Boundary* boundary = rhoGrid3D->getBoundary();

  //variables declaration
  int nPart = eb->getSize();
  int nStep = 1;
  double ctStep = 0.;

  //mass in MeV
  double mass = eb->getMass();
  double charge = eb->getCharge();
  double classicalR = eb->getClassicalRadius();

  //velocity in a.u v/c=beta
  double vx   = 0., vy   = 0., vz   = 0.;
  double d_vx = 0., d_vy = 0., d_vz = 0.;

  //coordinates of the particle
  double x = 0., y = 0., z = 0.;
  double d_x =0., d_y =0., d_z =0.;

  double z_min = rhoGrid3D->getzGridMin();
  double z_max = rhoGrid3D->getzGridMax();
  double z_length = z_max - z_min;

  //fields
  double fieldE = 0., e_x = 0., e_y = 0., e_z = 0.;
  double fieldH = 0., h_x = 0., h_y = 0., h_z = 0.;


  //xy-grid cell size
  //double d_xy = sqrt(boundary->getStepX()*boundary->getStepX()+
  //                   boundary->getStepY()*boundary->getStepY());
 
  //vectors
  double r_v[3], n_v[3];

  //variable defined state of particle INSIDE, OUTSIDE of boundary
  //or to be killed
  int isInside = 1;  

  //auxiliary variables
  double nx[3], ny[3], nz[3];
  double nx_0,  ny_0,  nz_0;
  double rad = 0., w = 0.;
  double beta_x_0, beta_y_0, beta_z_0;
  double beta_x, beta_y, beta_z;
  double cos_phi_0, sin_phi_0;
  double cos_phi, sin_phi;
  double shift_x,shift_y,shift_z;
  double Ey_over_H;
  int min_ort_ind = 0;

  double min_ort_val = 0.;
  nStep = nSteps_;
  ctStep = ctime/nStep;

  double ctStepInner = ctStep;

  macroSize_dead = 0.0;
  energy_absorbed = 0.0;

  //start of loop upon macro-particles
  for(int ind = 0; ind < nPart; ind++){
    vx = eb->px(ind)/mass;
    vy = eb->py(ind)/mass;
    vz = eb->pz(ind)/mass;
    x = eb->x(ind);
    y = eb->y(ind);
    z = eb->z(ind);
    //moving particle    ============start==========
    for(int i_step = 0; i_step < nStep; i_step++){
      d_vx = 0.; d_vy = 0.; d_vz = 0.;
      d_x  = 0.; d_y  = 0.; d_z  = 0.;
      for(int iter = 0; iter < 3; iter++){

        if(iter == 0) ctStepInner = ctStep*0.5;
        if(iter == 1) ctStepInner = ctStep;
        if(iter == 2) ctStepInner = ctStep*0.5;

	getElectricField(x,y,z,e_x,e_y,e_z);
	getMagneticField(x,y,z,h_x,h_y,h_z);
	fieldE = sqrt(e_x*e_x+e_y*e_y+e_z*e_z);
	fieldH = sqrt(h_x*h_x+h_y*h_y+h_z*h_z);
	if(fieldH > 0.){
	  //presence of magnetic field
	  //cout <<"debug start magn field tracking! H="<< fieldH <<" fieldE="<< fieldE <<"\n";
	  nz[0] = h_x/fieldH;
	  nz[1] = h_y/fieldH;
	  nz[2] = h_z/fieldH;
	  nz_0  = 1.0;
	  nx[0] = e_y*nz[2]-e_z*nz[1];
	  nx[1] = e_z*nz[0]-e_x*nz[2];
	  nx[2] = e_x*nz[1]-e_y*nz[0]; 
	  nx_0 = sqrt(nx[0]*nx[0]+nx[1]*nx[1]+nx[2]*nx[2]);
          if(nx_0 == 0.){
	    //reason why nx_0 = 0 may be E = 0 or E || H
            //but we need orts
            min_ort_ind = 0;
            min_ort_val = fabs(nz[0]);
            if(min_ort_val > fabs(nz[1])){
	      min_ort_ind = 1;
	      min_ort_val = fabs(nz[1]);
	    }
            if(min_ort_val > fabs(nz[2])){
	      min_ort_ind = 2;
	      min_ort_val = fabs(nz[2]);
	    }
            if(min_ort_ind == 0){
	      nx[0] =  0.;
	      nx[1] = -nz[2];
	      nx[2] =  nz[1]; 
	    }
            if(min_ort_ind == 1){
              nx[0] =  nz[2];
	      nx[1] =  0.;
	      nx[2] = -nz[0];
	    }
            if(min_ort_ind == 2){
              nx[0] = -nz[1];
	      nx[1] =  nz[0];
	      nx[2] =  0.;
	    } 
	    nx_0 = sqrt(nx[0]*nx[0]+nx[1]*nx[1]+nx[2]*nx[2]);
	  }
	  nx[0] = nx[0]/nx_0;
	  nx[1] = nx[1]/nx_0;
	  nx[2] = nx[2]/nx_0; 
	  nx_0 = 1.0;
	  ny[0] = nz[1]*nx[2]-nz[2]*nx[1];
	  ny[1] = nz[2]*nx[0]-nz[0]*nx[2];
	  ny[2] = nz[0]*nx[1]-nz[1]*nx[0];
	  ny_0  = sqrt(ny[0]*ny[0]+ny[1]*ny[1]+ny[2]*ny[2]);
	  ny[0] = ny[0]/ny_0;
	  ny[1] = ny[1]/ny_0;
	  ny[2] = ny[2]/ny_0;
	  ny_0  = 1.0;

	  //we have got orts, and can do physics
	  beta_x_0 = vx*nx[0]+vy*nx[1]+vz*nx[2];
	  beta_y_0 = vx*ny[0]+vy*ny[1]+vz*ny[2];
	  beta_z_0 = vx*nz[0]+vy*nz[1]+vz*nz[2];

	  Ey_over_H = (e_x*ny[0]+e_y*ny[1]+e_z*ny[2])/fieldH;
          rad = sqrt((beta_x_0-Ey_over_H)*(beta_x_0-Ey_over_H) + beta_y_0*beta_y_0);
          w = fieldH*classicalR*charge;

          if(rad > 0.){
	    cos_phi_0 =  (beta_x_0 - Ey_over_H)/rad;
	    sin_phi_0 = -beta_y_0/rad;
	  }
	  else{
            cos_phi_0 = 0.0;
            sin_phi_0 = 0.0;
	  }

	  cos_phi   = cos(w*ctStepInner);
	  sin_phi   = sin(w*ctStepInner);
	  beta_x =  rad*(cos_phi*cos_phi_0 - sin_phi*sin_phi_0) + Ey_over_H - beta_x_0;
	  beta_y = -rad*(sin_phi*cos_phi_0 + cos_phi*sin_phi_0) - beta_y_0;  
	  beta_z = classicalR*charge*(e_x*nz[0]+e_y*nz[1]+e_z*nz[2])*ctStepInner;
	  d_vx = beta_x*nx[0]+beta_y*ny[0]+beta_z*nz[0];
	  d_vy = beta_x*nx[1]+beta_y*ny[1]+beta_z*nz[1];
	  d_vz = beta_x*nx[2]+beta_y*ny[2]+beta_z*nz[2];
	  shift_x = (rad/w)*(sin_phi*cos_phi_0 + cos_phi*sin_phi_0 - sin_phi_0)+Ey_over_H*ctStepInner;
	  shift_y = (rad/w)*(cos_phi*cos_phi_0 - sin_phi*sin_phi_0 - cos_phi_0);
	  shift_z = (beta_z_0 + 0.5*beta_z)*ctStepInner;
	  d_x = shift_x*nx[0]+shift_y*ny[0]+shift_z*nz[0];
	  d_y = shift_x*nx[1]+shift_y*ny[1]+shift_z*nz[1];
	  d_z = shift_x*nx[2]+shift_y*ny[2]+shift_z*nz[2];
	}
	else{
	  if(fieldE > 0.){
	    //only electric field
	    d_vx = classicalR*charge*e_x*ctStepInner;
	    d_vy = classicalR*charge*e_y*ctStepInner;
	    d_vz = classicalR*charge*e_z*ctStepInner;
	    d_x  = vx*ctStepInner;
	    d_y  = vy*ctStepInner;
	    d_z  = vz*ctStepInner;        
	  }
	  else{
	    d_vx = 0.;        d_vy = 0.;        d_vz = 0.;
	    d_x  = vx*ctStepInner; d_y  = vy*ctStepInner; d_z  = vz*ctStepInner;
	  }
	}

	if(iter == 0 || iter == 2){
	  x = x+d_x;
	  y = y+d_y;
	  z = z+d_z;        
	}
	if(iter == 1){
	  vx = vx+d_vx;
	  vy = vy+d_vy;
	  vz = vz+d_vz;
	}

      }


      eb->px(ind) = vx*mass;      
      eb->py(ind) = vy*mass;      
      eb->pz(ind) = vz*mass;  
      eb->x(ind) = x;
      eb->y(ind) = y;
      eb->z(ind) = z;
      //provide the z limits =====start====
      if( z_length > 0. ){
	if(z < z_min ){
	  z = z_min + fabs(z-z_min);
	  if(z > z_max) {
             z = z_max;
             eb->pz(ind) = - fabs(eb->pz(ind));
	  }
	  else{
	    eb->z(ind) = z;
            eb->pz(ind) = fabs(eb->pz(ind));
	  }
	}
	if(z > z_max){
	  z = z_max - fabs(z-z_max);
	  if( z < z_min) {
	    z = z_min;
	    eb->pz(ind) = fabs(eb->pz(ind));
	  }
	  else{
	    eb->z(ind) = z;
	    eb->pz(ind) = - fabs(eb->pz(ind));
	  }
	}
      }
      //provide the z limits =====stop=====
      isInside = boundary->impactPoint_straightMotion(ind, eb, r_v, n_v);
      if(isInside == boundary->IS_OUTSIDE){
	if(missingDiag){
	  missingDiag->accountStrike(ind,eb);
	}

	int ebTotalCount_0 = eb->getTotalCount();
	macroSize_dead += eb->macroSize(ind);
	energy_absorbed += eb->getEnergy(ind)*eb->macroSize(ind);

	//absorbed or secondary reaction particle to be killed
	surface->impact(ind, eb, r_v, n_v);

	int ebTotalCount_1 = eb->getTotalCount();
        int nNewBorn = ebTotalCount_1 - ebTotalCount_0;
        if(surfaceDiag){
          surfaceDiag->analyze(ind,nNewBorn,eb,n_v);
	}

	if(nNewBorn>0){
	  for(int newInd = ebTotalCount_0; newInd < ebTotalCount_1; newInd++){
	    energy_absorbed -= eb->getEnergy(newInd)*eb->macroSize(newInd);
	  }
	}

	break;
      }
      if(isInside == boundary->TO_BE_KILLED){
	if(missingDiag){
	  missingDiag->accountMissing(ind,eb);
	}
	eb->deleteParticle(ind);
	break;
      }
    }
    //moving particle    ============stop===========
  }
  macroSize_dead /= ctime;
  energy_absorbed /= ctime;

  //-----------------------------------
  //end of loop upon macro-particles
  //-----------------------------------

  if(missingDiag){
    missingDiag->accountTime(ctime);
  }

  eb->compress();
}

void baseParticleTracker::moveParticles(double ctime, 
                    eBunch* eb, 
                    Grid3D* rhoGrid3D,
                    baseSurface* surface)
{
  if(surfaceDiag){
    surfaceDiag->accountTime(ctime);
  }
  EP_Boundary* boundary = rhoGrid3D->getBoundary();

  //variables declaration
  int nPart = eb->getSize();
  int nStep = 1;
  double ctStep = 0.;

  //mass in MeV
  double mass = eb->getMass();
  double charge = eb->getCharge();
  double classicalR = eb->getClassicalRadius();

  //velocity in a.u v/c=beta
  double vx   = 0., vy   = 0., vz   = 0.;
  double d_vx = 0., d_vy = 0., d_vz = 0.;

  //coordinates of the particle
  double x = 0., y = 0., z = 0.;
  double d_x =0., d_y =0., d_z =0.;

  double z_min = rhoGrid3D->getzGridMin();
  double z_max = rhoGrid3D->getzGridMax();
  double z_length = z_max - z_min;

  //fields
  double fieldE = 0., e_x = 0., e_y = 0., e_z = 0.;
  double fieldH = 0., h_x = 0., h_y = 0., h_z = 0.;


  //xy-grid cell size
  //double d_xy = sqrt(boundary->getStepX()*boundary->getStepX()+
  //                   boundary->getStepY()*boundary->getStepY());
 
  //vectors
  double r_v[3], n_v[3];

  //variable defined state of particle INSIDE, OUTSIDE of boundary
  //or to be killed
  int isInside = 1;  

  //auxiliary variables
  double nx[3], ny[3], nz[3];
  double nx_0,  ny_0,  nz_0;
  double rad = 0., w = 0.;
  double beta_x_0, beta_y_0, beta_z_0;
  double beta_x, beta_y, beta_z;
  double cos_phi_0, sin_phi_0;
  double cos_phi, sin_phi;
  double shift_x,shift_y,shift_z;
  double Ey_over_H;
  int min_ort_ind = 0;

  double min_ort_val = 0.;
  nStep = nSteps_;
  ctStep = ctime/nStep;

  macroSize_dead = 0.0;
  energy_absorbed = 0.0;

  //start of loop upon macro-particles
  for(int ind = 0; ind < nPart; ind++){
    vx = eb->px(ind)/mass;
    vy = eb->py(ind)/mass;
    vz = eb->pz(ind)/mass;
    x = eb->x(ind);
    y = eb->y(ind);
    z = eb->z(ind);
    //moving particle    ============start==========
    for(int i_step = 0; i_step < nStep; i_step++){
      d_vx = 0.; d_vy = 0.; d_vz = 0.;
      d_x  = 0.; d_y  = 0.; d_z  = 0.;
      for(int iter = 0; iter < 2; iter++){
	getElectricField(x+0.5*d_x,y+0.5*d_y,z+0.5*d_z,e_x,e_y,e_z);
	getMagneticField(x+0.5*d_x,y+0.5*d_y,z+0.5*d_z,h_x,h_y,h_z);
	fieldE = sqrt(e_x*e_x+e_y*e_y+e_z*e_z);
	fieldH = sqrt(h_x*h_x+h_y*h_y+h_z*h_z);
	if(fieldH > 0.){
	  //presence of magnetic field
	  //cout <<"debug start magn field tracking! H="<< fieldH <<" fieldE="<< fieldE <<"\n";
	  nz[0] = h_x/fieldH;
	  nz[1] = h_y/fieldH;
	  nz[2] = h_z/fieldH;
	  nz_0  = 1.0;
	  nx[0] = e_y*nz[2]-e_z*nz[1];
	  nx[1] = e_z*nz[0]-e_x*nz[2];
	  nx[2] = e_x*nz[1]-e_y*nz[0]; 
	  nx_0 = sqrt(nx[0]*nx[0]+nx[1]*nx[1]+nx[2]*nx[2]);
          if(nx_0 == 0.){
	    //reason why nx_0 = 0 may be E = 0 or E || H
            //but we need orts
            min_ort_ind = 0;
            min_ort_val = fabs(nz[0]);
            if(min_ort_val > fabs(nz[1])){
             min_ort_ind = 1;
             min_ort_val = fabs(nz[1]);
	    }
            if(min_ort_val > fabs(nz[2])){
             min_ort_ind = 2;
             min_ort_val = fabs(nz[2]);
	    }
            if(min_ort_ind == 0){
	      nx[0] =  0.;
	      nx[1] = -nz[2];
	      nx[2] =  nz[1]; 
	    }
            if(min_ort_ind == 1){
              nx[0] =  nz[2];
	      nx[1] =  0.;
	      nx[2] = -nz[0];
	    }
            if(min_ort_ind == 2){
              nx[0] = -nz[1];
	      nx[1] =  nz[0];
	      nx[2] =  0.;
	    } 
	    nx_0 = sqrt(nx[0]*nx[0]+nx[1]*nx[1]+nx[2]*nx[2]);
	  }
	  nx[0] = nx[0]/nx_0;
	  nx[1] = nx[1]/nx_0;
	  nx[2] = nx[2]/nx_0; 
	  nx_0 = 1.0;
	  ny[0] = nz[1]*nx[2]-nz[2]*nx[1];
	  ny[1] = nz[2]*nx[0]-nz[0]*nx[2];
	  ny[2] = nz[0]*nx[1]-nz[1]*nx[0];
	  ny_0  = sqrt(ny[0]*ny[0]+ny[1]*ny[1]+ny[2]*ny[2]);
	  ny[0] = ny[0]/ny_0;
	  ny[1] = ny[1]/ny_0;
	  ny[2] = ny[2]/ny_0;
	  ny_0  = 1.0;

	  //we have got orts, and can do physics
	  beta_x_0 = vx*nx[0]+vy*nx[1]+vz*nx[2];
	  beta_y_0 = vx*ny[0]+vy*ny[1]+vz*ny[2];
	  beta_z_0 = vx*nz[0]+vy*nz[1]+vz*nz[2];

	  Ey_over_H = (e_x*ny[0]+e_y*ny[1]+e_z*ny[2])/fieldH;
          rad = sqrt((beta_x_0-Ey_over_H)*(beta_x_0-Ey_over_H) + beta_y_0*beta_y_0);
          w = fieldH*classicalR*charge;
          if(rad > 0.){
	    cos_phi_0 =  (beta_x_0 - Ey_over_H)/rad;
	    sin_phi_0 = -beta_y_0/rad;
	  }
	  else{
            cos_phi_0 = 0.0;
            sin_phi_0 = 0.0;
	  }

	  cos_phi   = cos(w*ctStep);
	  sin_phi   = sin(w*ctStep);
	  beta_x =  rad*(cos_phi*cos_phi_0 - sin_phi*sin_phi_0) + Ey_over_H - beta_x_0;
	  beta_y = -rad*(sin_phi*cos_phi_0 + cos_phi*sin_phi_0) - beta_y_0;  
	  beta_z = classicalR*charge*(e_x*nz[0]+e_y*nz[1]+e_z*nz[2])*ctStep;
	  d_vx = beta_x*nx[0]+beta_y*ny[0]+beta_z*nz[0];
	  d_vy = beta_x*nx[1]+beta_y*ny[1]+beta_z*nz[1];
	  d_vz = beta_x*nx[2]+beta_y*ny[2]+beta_z*nz[2];
	  shift_x = (rad/w)*(sin_phi*cos_phi_0 + cos_phi*sin_phi_0 - sin_phi_0)+Ey_over_H*ctStep;
	  shift_y = (rad/w)*(cos_phi*cos_phi_0 - sin_phi*sin_phi_0 - cos_phi_0);
	  shift_z = (beta_z_0 + 0.5*beta_z)*ctStep;
	  d_x = shift_x*nx[0]+shift_y*ny[0]+shift_z*nz[0];
	  d_y = shift_x*nx[1]+shift_y*ny[1]+shift_z*nz[1];
	  d_z = shift_x*nx[2]+shift_y*ny[2]+shift_z*nz[2];
	}
	else{
	  if(fieldE > 0.){
	    //only electric field
	    d_vx = classicalR*charge*e_x*ctStep;
	    d_vy = classicalR*charge*e_y*ctStep;
	    d_vz = classicalR*charge*e_z*ctStep;
	    d_x  = (vx + 0.5*d_vx)*ctStep;
	    d_y  = (vy + 0.5*d_vy)*ctStep;
	    d_z  = (vz + 0.5*d_vz)*ctStep;        
	  }
	  else{
	    d_vx = 0.;        d_vy = 0.;        d_vz = 0.;
	    d_x  = vx*ctStep; d_y  = vy*ctStep; d_z  = vz*ctStep;
	  }
	}
      }
      vx = vx+d_vx;
      vy = vy+d_vy;
      vz = vz+d_vz;
      x = x+d_x;
      y = y+d_y;
      z = z+d_z;
      eb->px(ind) = vx*mass;      
      eb->py(ind) = vy*mass;      
      eb->pz(ind) = vz*mass;  
      eb->x(ind) = x;
      eb->y(ind) = y;
      eb->z(ind) = z;
      //provide the z limits =====start====
      if( z_length > 0. ){
	if(z < z_min ){
	  z = z_min + fabs(z-z_min);
	  if(z > z_max) {
             z = z_max;
             eb->pz(ind) = - fabs(eb->pz(ind));
	  }
	  else{
	    eb->z(ind) = z;
            eb->pz(ind) = fabs(eb->pz(ind));
	  }
	}
	if(z > z_max){
	  z = z_max - fabs(z-z_max);
	  if( z < z_min) {
	    z = z_min;
	    eb->pz(ind) = fabs(eb->pz(ind));
	  }
	  else{
	    eb->z(ind) = z;
	    eb->pz(ind) = - fabs(eb->pz(ind));
	  }
	}
      }
      //provide the z limits =====stop=====
      isInside = boundary->impactPoint_straightMotion(ind, eb, r_v, n_v);
      if(isInside == boundary->IS_OUTSIDE){
	if(missingDiag){
	  missingDiag->accountStrike(ind,eb);
	}

	int ebTotalCount_0 = eb->getTotalCount();
	macroSize_dead += eb->macroSize(ind);
	energy_absorbed += eb->getEnergy(ind)*eb->macroSize(ind);

	//absorbed or secondary reaction particle to be killed
	surface->impact(ind, eb, r_v, n_v);

	int ebTotalCount_1 = eb->getTotalCount();
        int nNewBorn = ebTotalCount_1 - ebTotalCount_0;
        if(surfaceDiag){
          surfaceDiag->analyze(ind,nNewBorn,eb,n_v);
	}

	if(nNewBorn>0){
	  for(int newInd = ebTotalCount_0; newInd < ebTotalCount_1; newInd++){
	    energy_absorbed -= eb->getEnergy(newInd)*eb->macroSize(newInd);
	  }
	}

	break;
      }
      if(isInside == boundary->TO_BE_KILLED){
	if(missingDiag){
	  missingDiag->accountMissing(ind,eb);
	}
	eb->deleteParticle(ind);
	break;
      }
    }
    //moving particle    ============stop===========
  }

  macroSize_dead /= ctime;
  energy_absorbed /= ctime;
  //-----------------------------------
  //end of loop upon macro-particles
  //-----------------------------------

  if(missingDiag){
    missingDiag->accountTime(ctime);
  }

  eb->compress();
}

//returns total macroSize of dead electrons per ctime ---use after moveParticles
double baseParticleTracker::getDeadMacroSize()
{
  return macroSize_dead;
}
//returns total absorbed energy on surface per ctime ---use after moveParticles
double baseParticleTracker::getAbsorbedEnergy()
{
  return energy_absorbed;
}

//returns the H in Gauss/(electron charge in CGS)
void baseParticleTracker::getMagneticField(double x,    double y,    double z, 
                                           double& h_x, double& h_y, double& h_z)
{
  h_x_sum = 0.; h_y_sum = 0.; h_z_sum = 0.;
  for(i_s = 0; i_s < nMgSources; i_s++){
    magneticSources[i_s]->getMagneticField(x,y,z,h_x_tmp,h_y_tmp,h_z_tmp);
    h_x_sum += h_x_tmp; h_y_sum += h_y_tmp; h_z_sum += h_z_tmp; 
  } 
  h_x = h_x_sum; h_y = h_y_sum; h_z = h_z_sum; 
}

//returns the electric field (for point like charge 1/(r*r))
void baseParticleTracker::getElectricField(double x,    double y,    double z, 
                                           double& e_x, double& e_y, double& e_z)
{
  e_x_sum = 0.; e_y_sum = 0.; e_z_sum = 0.;
  for(i_s = 0; i_s < nElSources; i_s++){
    electricSources[i_s]->getElectricField(x,y,z,e_x_tmp,e_y_tmp,e_z_tmp);
    e_x_sum += e_x_tmp; e_y_sum += e_y_tmp; e_z_sum += e_z_tmp; 
  } 
  e_x = e_x_sum; e_y = e_y_sum; e_z = e_z_sum; 
}
