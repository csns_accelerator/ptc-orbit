/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   EP_Node.cc
//
// AUTHORS
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/21/2004
//
// DESCRIPTION
//    EP_Node class
//
///////////////////////////////////////////////////////////////////////////// 

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Particles.h"
#include "MacroPart.h"
#include "Node.h"
#include "MapBase.h"
#include "TransMapHead.h"

#include "EP_Node.hh"

#include <iostream>
#include <cstdlib>

Define_Standard_Members(EP_Node, Node);

EP_Node::EP_Node(String &n, Integer &order, Real &length_In) :
  Node(n, 0., order), length(length_In)
{
  eb = new eBunch();

  eBunchMainDiag = new CompositeEBunchDiag();
  eBunchMainDiag->setZeroTurn();

  surfMainDiag = new CompositeSurfaceDiag();
  surfMainDiag->setZeroTurn();

  //surfaceType = 0 for Copper, 1 for Stainless Steal, 2 for Titanium nitride coated 
  int surfaceType=2;
  surface = new CntrlSecEmSurface(surfaceType);

  //death probability on the surface
  //it is table (electrons energy in eV) - Probability
  //destructor of this death_func is called by surface
  Function* death_func = getDeathFunction();
  surface->setDeathFunction(death_func);

  baseFileds = NULL;
  nBaseFileds = 0;


  //probabilities for surface and electron production
  surfProb = 1.0;
  volProb = 0.;

  newMacroPerBunch = 50000;
  electronsPerProton = 0.;
  pLossRate = 0.;

  //use boundary conditions for fields or not ( 1-yes 0-no)
  addBoundaryPotentials = 1;

  //set the effective length coefficient 
  //it will affect on p-Bunch kick ( by default it is 1.0)
  effectiveLengthCoeff = 1.0;

  //profile function
  //i = 0 - p-bunch line density
  //i = 1 - e-bunch line density
  //i = 2 - x-centroid of the e-bunch
  //i = 3 - y-centroid of the e-bunch
  //i = 4 - size of the e-bunch
  //i = 5 - electron, hitting surface, current [A/m]
  //i = 6 - energy absorbed by surface [MeV/s/m]
  nProfileF = 7;
  profileF_arr = new Function*[nProfileF];
  for(int i = 0; i < nProfileF; i++){
    profileF_arr[i] = NULL;
  }

}

EP_Node::~EP_Node()
{    
  delete eb;
  delete eBunchMainDiag;
  delete surfMainDiag;
  delete surface;

  if(baseFileds){
    for(int i = 0; i < nBaseFileds; i++){
      delete baseFileds[i];
    }
    delete [] baseFileds;
  }

  for(int i = 0; i < nProfileF; i++){
    if(profileF_arr[i]) delete profileF_arr[i];
  }
  delete [] profileF_arr;
};

Void EP_Node::nameOut(String &wname) { wname = _name; };

Void EP_Node::_nodeCalculator(MacroPart &mp)
{
}

Void EP_Node::_updatePartAtNode(MacroPart &mp)
{
  EP_NodeCalculator* epCalc = EP_NodeCalculator::getNodeCalculator();

  eBunchMainDiag->startNewTurn();
  surfMainDiag->startNewTurn();

  if(eBunchMainDiag->getNumberOfDiagnostics() > 0){
    epCalc->setEBunchDiagnostics(eBunchMainDiag);
  }
  else{
    epCalc->setEBunchDiagnostics(NULL);
  }

  if(surfMainDiag->getNumberOfDiagnostics() > 0){
    epCalc->getTracker()->setSurfaceDiagnostics(surfMainDiag);
  }
  else{
    epCalc->getTracker()->setSurfaceDiagnostics(NULL);
  }

  epCalc->setElectronGenerationProbab(surfProb,volProb);

  epCalc->setElectronGenerationParams(newMacroPerBunch,
				      electronsPerProton,
				      pLossRate);

  for(int i = 0; i < nBaseFileds; i++){
    epCalc->getTracker()->addMagneticFieldSource(baseFileds[i]);
    epCalc->getTracker()->addElectricFieldSource(baseFileds[i]);       
  }

  //add boundary potaential or not : addBoundaryPotentials = 1 yes 0 -no
  epCalc->setAddBoundaryPotentialsInfo(addBoundaryPotentials);

  //set the effective length coefficient 
  epCalc->setEffLengthCoeff(effectiveLengthCoeff);

  //MAIN line - propagation of the p-bunch through the e-cloud region
  epCalc->propagate(length, mp, eb, surface, profileF_arr);

  for(int i = 0; i < nBaseFileds; i++){
    epCalc->getTracker()->removeMagneticFieldSource(baseFileds[i]);
    epCalc->getTracker()->removeElectricFieldSource(baseFileds[i]);       
  }

  epCalc->setEBunchDiagnostics(NULL);
  epCalc->getTracker()->setSurfaceDiagnostics(NULL);

}

void EP_Node::resetSurface(baseSurface* srf)
{
  if(srf){
    delete surface;
    surface = srf;
    Function* death_func = getDeathFunction();
    surface->setDeathFunction(death_func);
  }
}


baseSurface* EP_Node::getSurface()
{
  return surface;
}


void EP_Node::setElectronGenerationProbab(double surfProb_In, double volProb_In)
{
  surfProb =  surfProb_In;
  volProb = volProb_In;
}

void EP_Node::setElectronGenerationParams(int newMacroPerBunch_In,
				 double electronsPerProton_In,
				 double pLossRate_In)
{
  newMacroPerBunch   =  newMacroPerBunch_In;
  electronsPerProton = electronsPerProton_In;
  pLossRate          = pLossRate_In;
}


CompositeEBunchDiag* EP_Node::getEBunchDiag()
{
  return eBunchMainDiag;
}

CompositeSurfaceDiag* EP_Node::getSurfDiag()
{
  return surfMainDiag;
}


void EP_Node::addFieldSource(BaseFieldSource* bf)
{
  if(bf){
    if(nBaseFileds == 0){
      baseFileds = new BaseFieldSource*[1];
      baseFileds[0] = bf;
      nBaseFileds = 1;
    } 
    else{
      BaseFieldSource** tmp = new BaseFieldSource*[nBaseFileds+1];
      for(int i = 0; i < nBaseFileds; i++){
	tmp[i] = baseFileds[i];
      }
      tmp[nBaseFileds] = bf;
      delete [] baseFileds;
      baseFileds = tmp;
      nBaseFileds++;
    }
  }
}

void EP_Node::clearEBunch()
{
  eb->deleteAllParticles();
}

void EP_Node::readEBunch(char* fileName)
{
 eb->readParticles(fileName);
}

void EP_Node::dumpEBunch(char* fileName)
{
  eb->print(fileName);
}

void EP_Node::setAddBoundaryPotentialsInfo(int addBoundaryPotentialsIn)
{
  addBoundaryPotentials = addBoundaryPotentialsIn;
}

//set the effective length coefficient 
//it will affect on p-Bunch kick ( by default it is 1.0)
void EP_Node::setEffLengthCoeff(double effectiveLengthCoeffIn)
{
  effectiveLengthCoeff = effectiveLengthCoeffIn;
}

//profile functions array
//i = 0 - p-bunch line density
//i = 1 - e-bunch line density
//i = 2 - x-centroid of the e-bunch
//i = 3 - y-centroid of the e-bunch
//i = 4 - size of the e-bunch
//i = 5 - electron, hitting surface, current [A/m]
//i = 6 - energy absorbed by surface [MeV/s/m]
void EP_Node::startRecordingProfiles(int profileF_index)
{
  if(profileF_index < nProfileF && profileF_index >= 0){
    if(profileF_arr[profileF_index]) delete profileF_arr[profileF_index];
    profileF_arr[profileF_index] = new Function();
    profileF_arr[profileF_index]->clean();
    profileF_arr[profileF_index]->add(0.,0.);
  }  
}

//profile functions array
//i = 0 - b-bunch line density
//i = 1 - e-bunch line density
//i = 2 - x-centroid of the e-bunch
//i = 3 - y-centroid of the e-bunch
//i = 4 - size of the e-bunch
//i = 5 - electron, hitting surface, current [A/m]
//i = 6 - energy absorbed by surface [MeV/s/m]
void EP_Node::dumpRecordingProfiles(int profileF_index, char* fileName)
{
  if(profileF_index < nProfileF && profileF_index >= 0){
    if(profileF_arr[profileF_index]) profileF_arr[profileF_index]->print(fileName);
  }
}

//profile functions array
//i = 0 - b-bunch line density
//i = 1 - e-bunch line density
//i = 2 - x-centroid of the e-bunch
//i = 3 - y-centroid of the e-bunch
//i = 4 - size of the e-bunch
//i = 5 - electron, hitting surface, current [A/m]
//i = 6 - energy absorbed by surface [MeV/s/m]
void EP_Node::stopRecordingProfiles(int profileF_index)
{
  if(profileF_index < nProfileF && profileF_index >= 0){
    if(profileF_arr[profileF_index]) delete profileF_arr[profileF_index];
    profileF_arr[profileF_index] = NULL;
  }
}

eBunch* EP_Node::getEBunch()
{
  return eb;
}

Function* EP_Node::getDeathFunction()
{
  //death probability on the surface
  //it is table (electrons energy in eV) - Probability
  Function* death_func = new Function();
  death_func->add(0.  , 0.8);
  death_func->add(1.  , 0.5);
  death_func->add(2.  , 0.3);
  death_func->add(3.  , 0.1);
  death_func->add(4.  , 0. );
  death_func->setConstStep(1);
  return death_func;
}
