//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    QuadMagField.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/06/2005
//
// DESCRIPTION
//   The source of quadrupole magnetic fields  B=(b*y, b*x, 0): b=gradient
//
//   In ECloud.mod there is the related method to add a quadrupole magnetic field 
//   to the EP_Node. Magnetic field gradient in Tesla/mm.
//   ECloudAddQuadMagnField(const Integer& nodeIndex, 
//                          const Real& zMin, const Real& zMax, 
//   		            const Real& quad_mag_gradient)
//      
//   In futer we are going to add the fringe field also
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <cstdio>
#include <cmath>

#include "QuadMagField.hh"
#include "epModuleConstants.hh"

QuadMagField::QuadMagField():
  BaseFieldSource() 
{
  QuadMagGradient=0.;

  xMin = - 1.0e+307; xMax = + 1.0e+307;
  yMin = - 1.0e+307; yMax = + 1.0e+307;
  zMin = - 1.0e+307; zMax = + 1.0e+307;

}

QuadMagField::~QuadMagField()
{
}

//set magnetic field gradient in [Tesla/mm]
void QuadMagField::setMagneticField(double QuadMagGradient_in)
{
  double coeff = epModuleConstants::coeff_Tesla_to_inner;
  QuadMagGradient = QuadMagGradient_in * coeff;
}


double QuadMagField::getQuadrupoleMagneticGradient()
{
  return QuadMagGradient;
}

void QuadMagField::setX_MinMax(double xMin_in, double xMax_in)
{
  xMin = xMin_in; xMax = xMax_in;
}

void QuadMagField::setY_MinMax(double yMin_in, double yMax_in)
{
  yMin = yMin_in; yMax = yMax_in;
}

void QuadMagField::setZ_MinMax(double zMin_in, double zMax_in)
{
  zMin = zMin_in; zMax = zMax_in;
}

//returns the H in Gauss/(electron charge in CGS)
void QuadMagField::getMagneticField(double x,    double y,    double z, 
                                    double& comp_x, double& comp_y, double& comp_z)
{
  if( x > xMin && x < xMax && 
      y > yMin && y < yMax && 
      z > zMin && z < zMax){
    comp_x = QuadMagGradient * y; 
    comp_y = QuadMagGradient * x; 
    comp_z = 0.;
  }
  else{
    comp_x = 0.; comp_y = 0.; comp_z = 0.;
  }
}

