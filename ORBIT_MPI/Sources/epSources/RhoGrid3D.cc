//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    RhoGrid3D.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    06/09/2003
//
// DESCRIPTION
//    The subclass of Grid3D class to handle 
//    the potential calculations
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include "RhoGrid3D.hh"

RhoGrid3D::RhoGrid3D(int nZ, EP_Boundary* boundaryIn) :
  Grid3D(nZ,boundaryIn)
{

}

RhoGrid3D::~RhoGrid3D()
{
}

//calculates potential on Grid3D by using boundary's method
void RhoGrid3D::findPotential(Grid3D* phiGrid)
{
  if(getBinsX() != phiGrid->getBinsX() || 
     getBinsY() != phiGrid->getBinsY() ||
     getBinsZ() != phiGrid->getBinsZ()){
       if(rank_MPI == 0){
	 std::cout << "RhoGrid3D::findPotential(Grid3D* phiGrid) \n";
	 std::cout << "getBinsX() = "<< getBinsX()<<" phiGrid->getBinsX() ="<<phiGrid->getBinsX()<<"\n";
	 std::cout << "getBinsY() = "<< getBinsY()<<" phiGrid->getBinsY() ="<<phiGrid->getBinsY()<<"\n";
	 std::cout << "getBinsZ() = "<< getBinsZ()<<" phiGrid->getBinsZ() ="<<phiGrid->getBinsZ()<<"\n";
	 std::cout << "Check the size of the arrays.\n";
	 std::cout << "Stop. \n";
       }
       FinalizeExecution();     
  }
  else{
    for(int i=0; i < nZ_; i++){
      boundary->findPotential(getSlice2D(i),phiGrid->getSlice2D(i));
    }
    if(nZ_ == 1){
      double z_width = getzGridMax() - getzGridMin();
      if(z_width > 0.0){
        phiGrid->multiply(1.0/z_width);
      }
    }
    if(nZ_ == 2){
      double z_width = getzGridMax() - getzGridMin();
      if(z_width > 0.0){
        phiGrid->multiply(0.5/z_width);
      }      
    }
    if(nZ_ > 2){
      double z_width = getStepZ();
      if(z_width > 0.0){
        phiGrid->multiply(1.0/z_width);
      }        
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//bins macro-particles (electrons)
///////////////////////////////////////////////////////////////////////////
void RhoGrid3D::binParticles(eBunch* eb)
{
  clean();
  eb->compress();
  int nMacros = eb->getSize();
  double charge = eb->getCharge();
  for(int i = 0; i < nMacros; i++){
    //if(boundary->isInside(eb->x(i),eb->y(i)) == EP_Boundary::IS_INSIDE) {
    binParticleIntoArr3D( charge*eb->macroSize(i), eb->x(i), eb->y(i), eb->z(i));
    //}
    //else{
    //  eb->deleteParticle(i);
    //}
  }
  //eb->compress();
  allReduce();
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
