//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    eBunch.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    05/01/2003
//
// DESCRIPTION
//    Specification and inline functions for a class used to 
//    arrange macro particles to be concerened in the class EPnode
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include <iostream> 
#include <fstream>
#include "mpi.h"
#include "stdlib.h"
#include <cmath>

using namespace std;

#ifndef E_BUNCH_H
#define E_BUNCH_H
  
///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    eBunch
//
// CLASS RELATIONSHIPS
//    class EPnode
//      <- class EP_Boundary, class eBunch, class Surface
//
// DESCRIPTION
//    A class for arranging macro particles of electrons which are moving
//    in a electron bunch. A macroparticle is dealed as a sigle particle
//    with charge (e * macroSize) and mass (mass * macroSize) in EPnode.
//    We make each macroparticle "alive", if it does not hit the provided 
//    surface, or make it "dead", if it hits the surface.
//
// PUBLIC METHODS
//    eBunch:        Constructor for making eBunch objects
//    ~eBunch:       Destructor for the eBunch class
//    macroSize:     returns MacroSize of each macroparticle
//    x: y: z:       returns coordinates of each macroparticle
//    px: py: pz:    returns momentum of non-drifting macroparticle
//    pp: pt:        returns momentum of drifting  macroparticle
//    flag:          returns the status of macroparticle; dead or alive
//                   flag(i)=0 means i-th macroparticle is dead
//                   flag(i)=1 means i-th macroparticle is alive
//    xBin:yBin:zBin:
//                   returns bin_index of each coordinate to a given grids
//    xFractBin:yFractBin:zFractBin:
//                   returns fraction of each coordinate at its bin_index 
//                   to a given grids
//    addParticle:   1. adds a non-drifting macroparticle
//                   2. adds a drifting macroparticle
//                   3. adds unclassified macroparticle
//    deleteParticle : deletes a macroparticle
//    compress:      compresses memory space just for alive macroparticles
//    getMass:       returns mass of an electron
//    getSize:       returns the number of alive macro_particles
//    getSizeGlobal  returns the number of macro_particles in all CPUs
//    getTotalCount():       returns total number of macro particles, alive and dead
//    print(ostream& Out):   print with ostream
//    print(char* fileName): print method which requests filename to print
//    readParticles(char* fileName, int nParts);
//    readParticles(char* fileName);
//
// PRIVATE METHODS
//    resize:        expands the total possible size 
//
// PRIVATE MEMBERS
//    mass         electron mass
//    nDim           the number of elements of an macroparticle:
//                   macroSize,x,y,z,px,py,pz,pp,pt
//    nTotalSize     the number of "possible" macro_particles
//    nSize          the number of "alive" macro_particles
//                   it is available after compress()
//    nNew           nSize + [the number of "new" macro_particles]
//                   nSize <= nNew < nTotalSize
//    nChunk         initial value of nTotalSize
//    arrFlag        * of [index] for flag
//    arrCoord       ** of [index][macroSize,x,y,z,px,py,pz,pp,pt]
//    arrBin         int** of [index][x,y,z]
//    arrFractBin    double** of [index][x,y,z]
//    iMPIini        non0 means MPI_Initialized, and 0 means uninitialized
//    rank_MPI       rank of current CPU
//    size_MPI       number of whole CPU
//
// PROTECTED MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class  eBunch 
{
public:
  //--------------------------------------
  //the public methods of the eBunch class
  //--------------------------------------
  
  eBunch();
  virtual ~eBunch();

  double& macroSize(int index);
  double& x(int index);  
  double& y(int index);  
  double& z(int index);
  double& px(int index);  
  double& py(int index);  
  double& pz(int index);
  double& pp(int index);   
  double& pt(int index);
  int& flag(int index);

  int& xBin(int index);
  int& yBin(int index); 
  int& zBin(int index);
  double& xFractBin(int index);
  double& yFractBin(int index);
  double& zFractBin(int index);

  void addParticle(double macroSize, double x,double y,double z,
		   double px,double py,double pz);
  void addParticle(double macroSize, double x,double y,double z,
		   double pp,double pt);
  void addParticle(double macroSize, double x,double y,double z,
		   double px,double py,double pz, double pp,double pt);

  void deleteParticle(int index);

  void compress();

  double getMass();                // MeV
  double getClassicalRadius();     // m
  double getCharge();              // sign only

  int getSize();
  int getSizeGlobal();
  int getSizeGlobalFromMemory(); 
  int getTotalCount();
  double getTotalMacroSize();
  double getTotalMacroSizeGlobal();
  double getTotalMacroSizeGlobalFromMemory();
  double getAveMacroSize();   
  double getAveMacroSizeFromMemory(); 
  void print(std::ostream& Out);  
  void print(char* fileName);
  void readParticles(char* fileName, int nParts);
  void readParticles(char* fileName);

  //convenience methods
 
  double getEnergy(int index);
  double getEnergyX(int index);
  double getEnergyY(int index);
  double getEnergyZ(int index);

  void deleteAllParticles();


private:
  //---------------------------------------
  //the private methods of the eBunch class
  //---------------------------------------

  void resize();
  void FinalizeExecution();

protected:

  double mass;     
  double charge;
  double classicalRadius;

private:
  //---------------------------------------
  //the private members of the eBunch class
  //---------------------------------------

  int nDim;
  int nTotalSize;  int nSize;     int nNew;   int nChunk;
  int    sizeGlobal; 
  double totalMacroSizeGlobal; 
  double aveMacroSizeGlobal; 

  int* arrFlag;        int* tmp_arrFlag;
  double** arrCoord;   double** tmp_arrCoord;

  int** arrBin; 
  double** arrFractBin;
  int** tmp_arrBin; 
  double** tmp_arrFractBin;

  //need of compress
  int needOfCompress;

  //for MPI
  int iMPIini; int rank_MPI; int size_MPI;
	int* nSizeArr;
	int* nSizeArr_MPI;
	double* dump_arr;
	int nSizeChank;
	
};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
