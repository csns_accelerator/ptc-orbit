//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    numericalFunctions.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    09/22/2003
//
// DESCRIPTION
//    Keeps static numerical functions for ep Module
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "numericalFunctions.hh"

double* numericalFunctions::gammlnCoeff = numericalFunctions::setGammlnCoeffArray();
double* numericalFunctions::factrlInit = numericalFunctions::setFactrlInitArray();

const int numericalFunctions::ITMAX = 100;
const int numericalFunctions::MAXIT = 100;
const double numericalFunctions::EPS = 3.0e-7;
const double numericalFunctions::FPMIN = 1.0e-30;


numericalFunctions::numericalFunctions()
{
}

numericalFunctions::~numericalFunctions()
{
}

///////////////////////////////////////////////////////////////////////////
// functions

//used in secondaryEmissSurface
double numericalFunctions::incompleteGamma(double p,double x){
  return gammp(p,x);
}
double numericalFunctions::incompleteBeta(double x,double mu,double nu){
  return betai(mu,nu,x);
}

///////////////////////////////////////////////////////////////////////////
// from numerical recipes (modified)

//---------------------------------------------------------------------
//returns log(fabs(gamma(xx>0)))
double numericalFunctions::gammln(double xx)
{
  double x,y,tmp,ser;
  y=x=xx;
  tmp=x+5.5;
  tmp -= (x+0.5)*log(tmp);
  ser=1.000000000190015;
  for (int j=0;j<=5;j++) ser += gammlnCoeff[j]/++y;
  return -tmp+log(2.5066282746310005*ser/x);
}
double* numericalFunctions::setGammlnCoeffArray(){
  static double cof[6]={76.18009172947146,-86.50532032941677,
			24.01409824083091,-1.231739572450155,
			0.1208650973866179e-2,-0.5395239384953e-5};
  return cof;
}
double numericalFunctions::getGammlnCoeffArray(int i){
  return gammlnCoeff[i]; //for debug use
}

//---------------------------------------------------------------------
//returns n!  with using gammln for large n
double numericalFunctions::factrl(int n){
  if (n<0) {
    std::cout<< "negative factorial in factrl\n";
    FinalizeExecution();
  }
  if (n>30) return exp(gammln(n+1.0));
  return factrlInit[n];
}
double* numericalFunctions::setFactrlInitArray(){
  static double arr[31];
  arr[0]=1.0;
  for(int i=0;i<30;i++){
    arr[i+1]=arr[i]*(i+1);
  }
  return arr;
}

//---------------------------------------------------------------------
//returns log(n!)
double numericalFunctions::factln(int n){
  static double a[101];
  if (n < 0) {
    std::cout<< "negative factorial in factln\n";
    FinalizeExecution();     
  }
  if (n <= 1) return 0.0;
  if (n <= 100) return a[n] ? a[n] : (a[n]=gammln(n+1.0));
  else return gammln(n+1.0);
}

//---------------------------------------------------------------------
//returns binomial coefficient n_C_k
double numericalFunctions::bico(int n,int k){
  return floor(0.5+exp(factln(n)-factln(k)-factln(n-k)));
}

//---------------------------------------------------------------------
//returns the incomplete gamma function P(a,x)
//  evaluated by its series representaion as gamser
//also returns log(Gamma(a)) as gln
//---USES gammln
void numericalFunctions::gser(double& gamser,double a,double x,double& gln){
  int n;
  double sum,del,ap;

  gln=gammln(a);
  if (x <= 0.0) {
    if (x < 0.0) {
      std::cout<< "debug: x < 0 in gser\n";
      FinalizeExecution();     
    }
    gamser=0.0;
    return;
  } else {
    ap=a;
    del=sum=1.0/a;
    for (n=1;n<=ITMAX;n++) {
      ++ap;
      del *= x/ap;
      sum += del;
      if (fabs(del) < fabs(sum)*EPS) {
	gamser=sum*exp(-x+a*log(x)-(gln));
	return;
      }
    }
    std::cout<< "debug: a too large, ITMAX too small in gser\n";
    FinalizeExecution();     
    return;
  }
}

//------
//returns the incomplete gamma function Q(a,x) 
//  evaluated by its continued fraction representation as gammcf
//also returns log(gamma(a)) as gln
//---USES gammln
void numericalFunctions::gcf(double& gammcf,double a,double x,double& gln){
  int i;
  double an,b,c,d,del,h;

  gln=gammln(a);
  b=x+1.0-a;
  c=1.0/FPMIN;
  d=1.0/b;
  h=d;
  for (i=1;i<=ITMAX;i++) {
    an = -i*(i-a);
    b += 2.0;
    d=an*d+b;
    if (fabs(d) < FPMIN) d=FPMIN;
    c=b+an/c;
    if (fabs(c) < FPMIN) c=FPMIN;
    d=1.0/d;
    del=d*c;
    h *= del;
    if (fabs(del-1.0) < EPS) break;
  }
  if (i > ITMAX) {
    std::cout<< "debug: a too large, ITMAX too small in gcf\n";
    FinalizeExecution();     
  }
  gammcf=exp(-x+a*log(x)-(gln))*h;
}

//------
//returns the incomplete gamma function P(a,x)
//USES gcf, gser
double numericalFunctions::gammp(double a,double x){
  double gamser,gammcf,gln;

  if (x < 0.0 || a <= 0.0) {
    std::cout<< "debug: invalid arguments in gammp\n";
    FinalizeExecution();     
  }

  if (x < (a+1.0)) {
    gser(gamser,a,x,gln);
    return gamser;
  } else {
    gcf(gammcf,a,x,gln);
    return 1.0-gammcf;
  }
}

//---------------------------------------------------------------------
//USED by betai: Evaluates continued fraction for incomplete beta function
//               by modified Lentz's method
double numericalFunctions::betacf(double a,double b,double x)
{
  int m,m2;
  double aa,c,d,del,h,qab,qam,qap;

  qab=a+b;
  qap=a+1.0;
  qam=a-1.0;
  c=1.0;
  d=1.0-qab*x/qap;
  if (fabs(d) < FPMIN) d=FPMIN;
  d=1.0/d;
  h=d;
  for (m=1;m<=MAXIT;m++) {
    m2=2*m;
    aa=m*(b-m)*x/((qam+m2)*(a+m2));
    d=1.0+aa*d;
    if (fabs(d) < FPMIN) d=FPMIN;
    c=1.0+aa/c;
    if (fabs(c) < FPMIN) c=FPMIN;
    d=1.0/d;
    h *= d*c;
    aa = -(a+m)*(qab+m)*x/((a+m2)*(qap+m2));
    d=1.0+aa*d;
    if (fabs(d) < FPMIN) d=FPMIN;
    c=1.0+aa/c;
    if (fabs(c) < FPMIN) c=FPMIN;
    d=1.0/d;
    del=d*c;
    h *= del;
    if (fabs(del-1.0) < EPS) break;
  }
  if (m > MAXIT) {
    std::cout<< "a or b too big, or MAXIT too small in betacf \n";
    FinalizeExecution();     
  }
  return h;
}

//------
//returns the incomplete beta function I_x(a,b)
//USES betacf, gammln 
double numericalFunctions::betai(double a, double b, double x)
{
  double bt;
  
  if (x < 0.0 || x > 1.0) {
    std::cout<< "Bad x in routine betai \n";
    FinalizeExecution();     
  } 

  if (x == 0.0 || x == 1.0) bt=0.0;
  else
    bt=exp(gammln(a+b)-gammln(a)-gammln(b)+a*log(x)+b*log(1.0-x));
  if (x < (a+1.0)/(a+b+2.0))
    return bt*betacf(a,b,x)/a;
  else
    return 1.0-bt*betacf(b,a,1.0-x)/b;
}

///////////////////////////////////////////////////////////////////////////
// operation tool
void numericalFunctions::FinalizeExecution()
{
  int iMPIini;
  MPI_Initialized(&iMPIini);
  if(iMPIini > 0){
    MPI_Finalize();
  }
  
  exit(1);
}


///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
