/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   EP_NodeStore.cc
//
// AUTHORS
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/21/2004
//
// DESCRIPTION
//    EP_NodeStore class - singleton to keep references 
//    to the EP_Node instances.
//
///////////////////////////////////////////////////////////////////////////// 

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cstdlib>

#include "EP_NodeStore.hh"

/////////////////////////////////////////////////////////////////////////////
//The Methods of the NodeLoadingManager class
/////////////////////////////////////////////////////////////////////////////

EP_NodeStore* EP_NodeStore::epNodeStore = NULL;

EP_NodeStore::EP_NodeStore()
{
  nEP_Nodes = 0;
  epNodes = NULL;
}

EP_NodeStore::~EP_NodeStore()
{
  if(epNodes) delete [] epNodes;
}

EP_NodeStore* EP_NodeStore::getEP_NodeStore()
{

  if(!epNodeStore){
    epNodeStore = new  EP_NodeStore();
    return  epNodeStore;
  }
  return epNodeStore;
}

int EP_NodeStore::addEP_Node(EP_Node* epN)
{
  EP_Node** eps_tmp = new EP_Node*[nEP_Nodes+1];
  for(int i = 0; i < nEP_Nodes; i++){
    eps_tmp[i] = epNodes[i];
  }
  eps_tmp[nEP_Nodes] = epN;
  if(nEP_Nodes > 0){
    delete [] epNodes;
  }
  epNodes = eps_tmp;
  nEP_Nodes++;

  return (nEP_Nodes-1);
}


EP_Node* EP_NodeStore::getEP_Node(int index)
{
  if(index < nEP_Nodes){
    return epNodes[index];
  }

  int iMPIini  = 0;
  int rank_MPI = 0;
  MPI_Initialized( &iMPIini);
  if (iMPIini) {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  }

  if( iMPIini > 0){
    MPI_Finalize();
  }
  if(rank_MPI == 0){
    std::cerr<<"Stop from EP_NodeStore class.\n";
    std::cerr<<"There is no node with index ="<<index<<std::endl;
    std::cerr<<"The Store has only N_NP_Nodes ="<<nEP_Nodes<<std::endl;
  }
  exit(1);
  return NULL;  
}

int EP_NodeStore::getSize()
{
  return nEP_Nodes;
}
