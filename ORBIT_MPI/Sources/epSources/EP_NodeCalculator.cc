//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//   EP_NodeCalculator.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/13/2003
//
// DESCRIPTION
//   This the EP_NodeCalculator class.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <cstdio>

#include "mpi.h"

#include "EP_NodeCalculator.hh"

extern Array(ObjectPtr) syncP;

EP_NodeCalculator* EP_NodeCalculator::instance = NULL;
int EP_NodeCalculator::iMPIini  = 0;
int EP_NodeCalculator::size_MPI = 1;
int EP_NodeCalculator::rank_MPI = 0;

EP_NodeCalculator* EP_NodeCalculator::getNodeCalculator()
{
  if(instance){
    return instance;
  }
  else{
    finalizeExecution("You have to create EP_NodeCalculator first.");
  }
  return NULL;
}

EP_NodeCalculator* EP_NodeCalculator::getNodeCalculator( 
		    int xBins_In, int yBins_In, int nZ_slices_In,
                    double xSize_In, double ySize_In,
                    int BPPoints, int BPShape, int BPModes, 
		    int minProtonBunchSize_In)
{
  if(instance){
    return instance;
  }
  else{
    instance = new EP_NodeCalculator(xBins_In,yBins_In,nZ_slices_In,
				     xSize_In,ySize_In,BPPoints,BPShape,BPModes,
				     minProtonBunchSize_In);
    return instance;
  }
}

EP_NodeCalculator::EP_NodeCalculator(int xBins_In, int yBins_In, int nZ_slices_In,
				     double xSize_In, double ySize_In,
				     int BPPoints, int BPShape, int BPModes,
				     int minProtonBunchSize_In)
{

  //MPI parameters
  iMPIini  = 0;
  size_MPI = 1;
  rank_MPI = 0;
  MPI_Initialized( &iMPIini);
  if (iMPIini) {
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  }

  //longitudinal intergration parameters
  nLongSteps = 1500;
  nPBunchFiledUpdateSteps = 1;

  //================================================
  //See the EP_Boundary.cc
  //BPShape= 1(Circle),2(Ellipse),3(Rectangle) 
  double length = 0.;
  xSize = xSize_In;
  ySize = ySize_In;
  xBins = xBins_In;
  yBins = yBins_In;
  nZ_slices = nZ_slices_In;

  boundary = new EP_Boundary(xBins,yBins,xSize,ySize,
		       BPPoints,BPShape,BPModes);

  //proton beam and ring parameters
  //they should be defined in the makeProtonBunchArrays(...) method
  beta = 0.;
  gamma = 1.;
  ring_length = 0.;
  frequency = 0.;
  cTimeTotal = 0.;
  harmonicNumber = 1.;
  minProtonBunchSize = minProtonBunchSize_In;
  coeffPhiToZ = 0.;

  //pBunch rho longitudinal profile
  nPointsPBunchProfile = 200;
  pBunchProfile     =  (double *) malloc (sizeof(double) * nPointsPBunchProfile);
  pBunchProfile_MPI =  (double *) malloc (sizeof(double) * nPointsPBunchProfile);
  pBunchProfileF = new Function();
  pBunchCommProfileF = new Function();

  //Tracker and field sources
  nTrackerSteps = 20;
  pbFields = new pBunchFieldSource(nZ_slices,z_min_ep,z_max_ep,boundary);
  ebFields = new eCloudFieldSource(nZ_slices,z_min_ep,z_max_ep,boundary);
  tracker = new baseParticleTracker();
  tracker->setNumberOfSteps(nTrackerSteps);
  tracker->addElectricFieldSource(pbFields);
  tracker->addElectricFieldSource(ebFields);
  pBunchRhoGrid = NULL;
  pBunchPhiGrid = NULL;
  useSimplecticTracker = 0;

  //use boundary conditions for fields or not ( 1-yes 0-no)
  addBoundaryPotentials = 1;

  //proton bunch x,y,z kick's grids
  pBunchKickX = NULL;
  pBunchKickY = NULL;
  pBunchKickZ = NULL;

  //electron production general parameters
  //probability surface and volume production
  eGenSurfProb = 1.0;
  eGenVolProb = 0.;

  //surface electron production parameters
  nNewEletronsPerBunch  = 50000;
  newElectronsPerProton = 0.;
  pBunchLossRate = 0.;
  nNewGenElectronsArr =  (int *) malloc (sizeof(int) * nLongSteps);
  nNewGen_MPI = (int *) malloc (sizeof(int) * size_MPI);

  //volume electron production parameters
  eVolumeProdGrid = new Grid3D(1,boundary);

  //main e-bunch diagnostics
  eBunchMainDiag = NULL;

  //maximal e-cloud linear density
  maxEDensity = 0.;

  //info - enable or suppress applying kicks to the p-bunch
  infoPBunchKicksApplying = 1;

  //set the effective length coefficient 
  //it will affect on p-Bunch kick ( by default it is 1.0)
  effectiveLengthCoeff = 1.0;

  //debug output
  infDebugOutput = 0;

  //transverse modulation function for p-bunch in x-direction
  pBunchTransModulationXF = NULL;

  //transverse modulation function for p-bunch in y-direction
  pBunchTransModulationYF = NULL;

  //density modulation function for p-bunch in phi-direction
  pBunchDensityModulationF = NULL;

}

EP_NodeCalculator::~EP_NodeCalculator()
{
  if(instance){
    if(boundary) delete boundary;
    if(tracker) delete tracker ;
    if(pbFields) delete pbFields;
    if(ebFields) delete ebFields;

    if(pBunchRhoGrid) delete pBunchRhoGrid;
    if(pBunchPhiGrid) delete pBunchPhiGrid;
    if(pBunchKickX) delete pBunchKickX;
    if(pBunchKickY) delete pBunchKickY;
    if(pBunchKickZ) delete pBunchKickZ;

    free(pBunchProfile);
    free(pBunchProfile_MPI);
    delete pBunchProfileF;
    delete pBunchCommProfileF;

    free(nNewGenElectronsArr);
    free(nNewGen_MPI);

    delete eVolumeProdGrid;

    if(pBunchTransModulationXF) delete pBunchTransModulationXF;
    if(pBunchTransModulationYF) delete pBunchTransModulationYF;
    if(pBunchDensityModulationF) delete pBunchDensityModulationF;

  }
  instance = NULL;
}

void EP_NodeCalculator::finalizeExecution(char* message)
{
  if( iMPIini > 0){
    MPI_Finalize();
  }
  std::cerr<<message<<"\n";
  std::cerr<<"Stop from EP_NodeCalculator class.\n";
  exit(1);  
}

//MAIN method 
//profileF_arr - profile functions array
//i = 0 - p-bunch line density
//i = 1 - e-bunch line density
//i = 2 - x-centroid of the e-bunch
//i = 3 - y-centroid of the e-bunch
//i = 4 - size of the e-bunch
//i = 5 - electron, hitting surface, current [A/m]
//i = 6 - energy absorbed by surface [MeV/s/m]
void EP_NodeCalculator::propagate(double length,
				  MacroPart &mp, 
				  eBunch* eb, 
				  baseSurface* surface,
				  Function** profileF_arr)
{

  double eCloudDensity;
  maxEDensity = 0.;

  //set the length parameters
  if(length == 0.) return;
  z_min_ep = - length/2.0;
  z_max_ep =   length/2.0;  
  pbFields->setMinMaxZ(z_min_ep,z_max_ep);
  ebFields->setMinMaxZ(z_min_ep,z_max_ep);

  //set parameters of the ring
  SyncPart *sp = SyncPart::safeCast
                 (syncP((Particles::syncPart & Particles::All_Mask)-1));
  beta = sp->_betaSync;
  gamma = sp->_gammaSync;
  ring_length = 1000.0*Ring::lRing;
  cTimeTotal = ring_length/beta;
  frequency = epModuleConstants::c/cTimeTotal;
  harmonicNumber = Ring::harmonicNumber;
  pBunchMacroSize = Injection::nReals_Macro;

  //distribute particles
  MacroPartDistributor* PartDistr;
  PartDistr = MacroPartDistributor::GetMacroPartDistributor();
  PartDistr->distributeParticles(mp);

  int nProtonsAll = mp._globalNMacros;

  pBunchTotalMacroSize = nProtonsAll*pBunchMacroSize;

  if(infDebugOutput == 1){
    if(rank_MPI == 0) std::cout<<"debug beta = "<< beta <<std::endl;  
    if(rank_MPI == 0) std::cout<<"debug gamma = "<< gamma <<std::endl;  
    if(rank_MPI == 0) std::cout<<"debug ring_length = "<< ring_length <<std::endl;
    if(rank_MPI == 0) std::cout<<"debug cTimeTotal [ns] = "<< 1.0e+9*cTimeTotal/epModuleConstants::c <<std::endl;   
    if(rank_MPI == 0) std::cout<<"debug frequency = "<< frequency <<std::endl;  
    if(rank_MPI == 0) std::cout<<"debug harmonicNumber = "<< harmonicNumber <<std::endl;  
    if(rank_MPI == 0) std::cout<<"debug pBunchMacroSize = "<< pBunchMacroSize <<std::endl;  
    if(rank_MPI == 0) std::cout<<"debug pBunchTotalMacroSize = "<< pBunchTotalMacroSize <<std::endl; 
    if(rank_MPI == 0) std::cout<<"debug z_max_ep = "<< z_max_ep <<std::endl;  
    if(rank_MPI == 0) std::cout<<"debug z_min_ep = "<< z_min_ep <<std::endl;
  }


  if( nProtonsAll < minProtonBunchSize ){ return;}

  //Loading Manager definition (this is singleton)
  //Loading Manager timing start
  NodeLoadingManager* LoadManager = NodeLoadingManager::GetNodeLoadingManager();
  LoadManager->measureStart();

  makeProtonBunchArrays(LoadManager,PartDistr,mp);

  //----------------------------------------------------
  //MOVING THE E-CLOUD region          START
  //----------------------------------------------------

  //file for dumping information on the disk
  std::ofstream F_out;
  if(rank_MPI == 0 && infDebugOutput == 1) F_out.open ("ep_tracker_results.dat", std::ios::app);

  pBunchKickX->clean();
  pBunchKickY->clean();
  pBunchKickZ->clean();

  int eBunchSize;
  double eBunchTotalMacroSize;

  double s, s_min ,sStep,ctime, ctime_min,ctStep;

  s = -(ring_length/2.0 + (z_max_ep - z_min_ep)/2.0);
  s_min = s;
  sStep = (ring_length)/nLongSteps;
  ctime = 0.;
  ctime_min = ctime;
  ctStep = sStep/beta;

  double ct_internal_step = ctStep/nPBunchFiledUpdateSteps;
  double s_internal_step  = sStep/nPBunchFiledUpdateSteps;

  //-------------------------------------------------------------------------------------------
  //debug special e-bunch field -----START----
  //eb->addParticle(0.,0.,0.,0.,0.,0.);
  //Grid3D* ebPhiGrid = ebFields->getPhiGrid3D();
  //double fixedE = (1000.0/epModuleConstants::coeff_Phi_to_Volts)*(ebPhiGrid->getyGridMax() - ebPhiGrid->getyGridMin())/1000.0;
  ////double fixedV = 1000.0/epModuleConstants::coeff_Phi_to_Volts;
  //for(int i_z = 0; i_z < ebPhiGrid->getBinsZ(); i_z++){
  //  for(int i_x = 0; i_x < ebPhiGrid->getBinsX(); i_x++){
  //    for(int i_y = 0; i_y < ebPhiGrid->getBinsY(); i_y++){
  //	ebPhiGrid->getArr3D()[i_z][i_x][i_y] = (i_y*fixedE)/(ebPhiGrid->getBinsY() - 1);
  //	//ebPhiGrid->getArr3D()[i_z][i_x][i_y] = (i_z*fixedV)/(ebPhiGrid->getBinsZ() - 1);
  //   }
  // }
  //}
  //debug special e-bunch field -----STOP-----
  //-------------------------------------------------------------------------------------------

  for(int iS = 0; iS < nLongSteps; iS++){

    generateElectrons(iS,s,sStep,ebFields->getRhoGrid3D(),eb,PartDistr);

    //eBunchSize = eb->getSizeGlobal();
    //eBunchTotalMacroSize = eb->getTotalMacroSizeGlobal();
    eb->getAveMacroSize();  //---060904
    eBunchSize = eb->getSizeGlobalFromMemory(); //---060904
    eBunchTotalMacroSize = eb->getTotalMacroSizeGlobalFromMemory(); //---060904

    //calculate fileds for e-cloud
    if(eBunchSize != 0){
      ebFields->createPotential(eb);
      if(addBoundaryPotentials == 1) ebFields->addBoundaryPotential();
    }
    else{
      ebFields->getPhiGrid3D()->clean();
    }

    double deadMacroSize = 0.0;
    double absorbedEnergy = 0.0;

    for(int i_inernal = 0; i_inernal < nPBunchFiledUpdateSteps; i_inernal++){
      
      //sets the p-bunch field to the pBunchFieldSource
      if(eBunchSize != 0){
	setPBunchFiledSource(s+i_inernal*s_internal_step,pbFields,PartDistr);
      }
      else{
	pbFields->getPhiGrid3D()->clean();
      }

      //track electrons 
      //we have to provide the access to this point 
      //to account time in the surface diagnostics
      if(useSimplecticTracker == 1){
	tracker->moveParticlesSimplectic(ct_internal_step,eb,ebFields->getRhoGrid3D(),surface);
      }
      else{
	tracker->moveParticles(ct_internal_step,eb,ebFields->getRhoGrid3D(),surface);
      }

      deadMacroSize += tracker->getDeadMacroSize();
      absorbedEnergy += tracker->getAbsorbedEnergy();

      //debug printing of the one electron coordinates   ====START====
      //if(eb->getSize() > 0 && rank_MPI == 0){
      //	std::cout<<" "<<eb->x(0)
      //		 <<" "<<eb->y(0)
      //		 <<" "<<eb->z(0)
      //		 <<" "<<eb->px(0)
      //		 <<" "<<eb->py(0)
      //		 <<" "<<eb->pz(0)
      //		 <<std::endl;
      //}
      //debug printing of the one electron coordinates   ====START====
    }

    //for electron detector 0v
    deadMacroSize /= nPBunchFiledUpdateSteps;
    absorbedEnergy /= nPBunchFiledUpdateSteps;
    double deadMacroSizeGlobal = 0.0;
    double absorbedEnergyGlobal = 0.0;
    if(size_MPI == 1) {
      deadMacroSizeGlobal = deadMacroSize;
      absorbedEnergyGlobal = absorbedEnergy;
    }
    else{
      MPI_Allreduce(&deadMacroSize,&deadMacroSizeGlobal,1, 
		    MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      MPI_Allreduce(&absorbedEnergy,&absorbedEnergyGlobal,1, 
		    MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    }
    //electrons hitting surface per unit time
    double electronHitSurface = deadMacroSizeGlobal*epModuleConstants::c*1.60218e-16/(z_max_ep - z_min_ep);
    //energy absorbed by surface [MeV/s/m]
    double absorbedEnergySurface = absorbedEnergyGlobal*epModuleConstants::c*1.e3/(z_max_ep - z_min_ep);

    //sum x,y,z kicks into the grid for p-bunch 
    if(eBunchSize != 0){
      accountPBunchKick(s,sStep,PartDistr,mp);
    }

    eCloudDensity = eBunchTotalMacroSize*1.6e-7/(z_max_ep - z_min_ep);

    if(eBunchMainDiag) eBunchMainDiag->analyze(ctime,eb);

    if(rank_MPI == 0 && infDebugOutput == 1) {
      std::cout << "debug i="<<iS<<" s[m]= "<<s/1000.
		<<" t [ns]="<< 1.0e+9*ctime/epModuleConstants::c
		<<" pLDens[nC/m]="<<pBunchProfileF->getY(s)
		<<" bSize="<< eBunchSize
		<<" eBTMacS="<<eBunchTotalMacroSize 
		<<" eLDens[nC/m]="<<eCloudDensity
		<<" ED_0V[A/m]="<<electronHitSurface
		<<" eneAbsSurf[MeV/s/m]="<<absorbedEnergySurface
		<<std::endl;
     
      F_out << iS <<" "<< s/1000.
	    <<" "<< (ctime/epModuleConstants::c)*1.0e+9
	    <<" "<< eBunchSize
	    <<" "<< eBunchTotalMacroSize 
	    <<" "<< pBunchProfileF->getY(s)
	    <<" "<< eCloudDensity
	    <<" "<< electronHitSurface
	    <<" "<< absorbedEnergySurface
	    <<"\n";
    }

    if(maxEDensity < eCloudDensity) maxEDensity = eCloudDensity;

    //record profiles into collectors
    if(profileF_arr){
      if(profileF_arr[0]){
	profileF_arr[0]->add(profileF_arr[0]->getMaxX()+
			     ctStep*1.0e+9/epModuleConstants::c,
			     pBunchProfileF->getY(s));
      }
      if(profileF_arr[1]){
	profileF_arr[1]->add(profileF_arr[1]->getMaxX()+
			     ctStep*1.0e+9/epModuleConstants::c,
			     eCloudDensity);
      }
      if(profileF_arr[2]){
	eBunchSize = eb->getSizeGlobal();
	double x_sum;
	double x_sum_MPI;
	x_sum = 0.;

	for(int j = 0; j < eb->getSize(); j++){
	  x_sum += eb->x(j);
	}
    
	MPI_Allreduce(&x_sum,&x_sum_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

	if(eBunchSize > 0){
	  x_sum = x_sum_MPI/eBunchSize;
	}

	profileF_arr[2]->add(profileF_arr[2]->getMaxX()+
			     ctStep*1.0e+9/epModuleConstants::c,
			     x_sum);
      }
      if(profileF_arr[3]){
	eBunchSize = eb->getSizeGlobal();
	double y_sum;
	double y_sum_MPI;
	y_sum = 0.;

	for(int j = 0; j < eb->getSize(); j++){
	  y_sum += eb->y(j);
	}
    
	MPI_Allreduce(&y_sum,&y_sum_MPI,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

	if(eBunchSize > 0){
	  y_sum = y_sum_MPI/eBunchSize;
	}

	profileF_arr[3]->add(profileF_arr[3]->getMaxX()+
			     ctStep*1.0e+9/epModuleConstants::c,
			     y_sum);
      }
      if(profileF_arr[4]){
	eBunchSize = eb->getSizeGlobal();
 	profileF_arr[4]->add(profileF_arr[4]->getMaxX()+
 			     ctStep*1.0e+9/epModuleConstants::c,
 			     eBunchSize);
      }
      if(profileF_arr[5]){
 	profileF_arr[5]->add(profileF_arr[5]->getMaxX()+
 			     ctStep*1.0e+9/epModuleConstants::c,
 			     electronHitSurface);
      }
      if(profileF_arr[6]){
 	profileF_arr[6]->add(profileF_arr[6]->getMaxX()+
 			     ctStep*1.0e+9/epModuleConstants::c,
 			     absorbedEnergySurface);
      }    
    }


    s = s_min + sStep*(iS+1);
    ctime = ctime_min + ctStep*(iS+1);
  }

  if(rank_MPI == 0 && infDebugOutput == 1) F_out.close();

  //----------------------------------------------------
  //MOVING THE E-CLOUD region          STOP
  //----------------------------------------------------

  //apply kicks to the protons
  if(infoPBunchKicksApplying == 1){
    applyPBunchKick(mp);
  }

  //Loading Manager timing stop
  LoadManager->measureStop();
}

void EP_NodeCalculator::makeProtonBunchArrays(NodeLoadingManager* LoadManager,
					      MacroPartDistributor* PartDistr, 
					      MacroPart &mp)
{

  //remember that we need 2 additional slices - left and right

  pBunchTotalSlices = LoadManager->getTotalNumberOfSlices();
  pBunchLocalSlices = LoadManager->getNumberOfSlices();
  phiMin = PartDistr->getPhiMin();
  phiMax = PartDistr->getPhiMax();
  phiStep = PartDistr->getPhiStep();
  coeffPhiToZ = ring_length/(2.0* epModuleConstants::PI);
  zMinGlobal = phiMin * coeffPhiToZ;
  zMaxGlobal = phiMax * coeffPhiToZ;

  //this is min and max slice's indexes that belongs to this CPU
  int indSL = PartDistr->getSliceIndMin();
  int indSR = PartDistr->getSliceIndMax();
  double z_min = (phiMin + phiStep*indSL)*coeffPhiToZ;
  double z_max = (phiMin + phiStep*indSR)*coeffPhiToZ;

  //if(rank_MPI == 0) std::cout<<"debug phiMin = "<< phiMin <<" Z_MIN="<< zMinGlobal <<std::endl;  
  //if(rank_MPI == 0) std::cout<<"debug phiMax = "<< phiMax <<" Z_MAX="<< zMaxGlobal <<std::endl;  
  //if(rank_MPI == 0) std::cout<<"debug phiStep = "<< phiStep <<std::endl;
  //if(rank_MPI == 0) std::cout<<"debug pBunchTotalSlices = "<< pBunchTotalSlices <<std::endl;  
  //std::cout<<"debug rank="<<rank_MPI <<" pBunchLocalSlices = "<< pBunchLocalSlices <<std::endl; 
  //std::cout<<"debug rank="<<rank_MPI <<" z_min = "<< z_min - phiStep*coeffPhiToZ/2.0 <<" ind0="<<indSL  <<std::endl;  
  //std::cout<<"debug rank="<<rank_MPI <<" z_max = "<< z_max + phiStep*coeffPhiToZ/2.0 <<" ind1="<<indSR  <<std::endl;  


  //------------------------------------------------------------------
  //Grid3D definition for rho and phi grids for proton bunch
  // and the grid for x,y,z kicks of protons                  START
  //------------------------------------------------------------------

  if(pBunchRhoGrid){
    if(pBunchRhoGrid->getBinsZ() != (pBunchLocalSlices + 2)){
      delete pBunchRhoGrid;
      pBunchRhoGrid = new RhoGrid3D(pBunchLocalSlices+2,boundary);
    }
  }
  else{
    pBunchRhoGrid = new RhoGrid3D(pBunchLocalSlices+2,boundary);
  }
  pBunchRhoGrid->clean();


  if(pBunchPhiGrid){
    if(pBunchPhiGrid->getBinsZ() != (pBunchLocalSlices + 2)){
      delete pBunchPhiGrid;
      pBunchPhiGrid = new PhiGrid3D(pBunchLocalSlices+2,boundary);
    }
  }
  else{
    pBunchPhiGrid = new PhiGrid3D(pBunchLocalSlices+2,boundary);
  }
  pBunchPhiGrid->clean();

  if(pBunchKickX){
    if(pBunchKickX->getBinsZ() != (pBunchLocalSlices + 2)){
      delete pBunchKickX;
      pBunchKickX = new Grid3D(pBunchLocalSlices+2,boundary);
    }
  }
  else{
    pBunchKickX = new Grid3D(pBunchLocalSlices+2,boundary);
  }
  pBunchKickX->clean();

  if(pBunchKickY){
    if(pBunchKickY->getBinsZ() != (pBunchLocalSlices + 2)){
      delete pBunchKickY;
      pBunchKickY = new Grid3D(pBunchLocalSlices+2,boundary);
    }
  }
  else{
    pBunchKickY = new Grid3D(pBunchLocalSlices+2,boundary);
  }
  pBunchKickY->clean();

  if(pBunchKickZ){
    if(pBunchKickZ->getBinsZ() != (pBunchLocalSlices + 2)){
      delete pBunchKickZ;
      pBunchKickZ = new Grid3D(pBunchLocalSlices+2,boundary);
    }
  }
  else{
    pBunchKickZ = new Grid3D(pBunchLocalSlices+2,boundary);
  }
  pBunchKickZ->clean();

  //longitudinal coordinates definition
  pBunchRhoGrid->setMinMaxZ(z_min-phiStep*coeffPhiToZ,z_max+phiStep*coeffPhiToZ);
  pBunchPhiGrid->setMinMaxZ(z_min-phiStep*coeffPhiToZ,z_max+phiStep*coeffPhiToZ);
  pBunchKickX->setMinMaxZ(z_min-phiStep*coeffPhiToZ,z_max+phiStep*coeffPhiToZ);
  pBunchKickY->setMinMaxZ(z_min-phiStep*coeffPhiToZ,z_max+phiStep*coeffPhiToZ);
  pBunchKickZ->setMinMaxZ(z_min-phiStep*coeffPhiToZ,z_max+phiStep*coeffPhiToZ);

  //------------------------------------------------------------------
  //Grid3D definition for rho and phi grids for proton bunch
  // and the grid for x,y,z kicks of protons                  START
  //------------------------------------------------------------------

  //bin protons and calculate fields
  int bunchSize = mp._nMacros;
  double y_shift = 0.;
  double x_shift = 0.;
  double phi_local;
  double macroSize_local = pBunchMacroSize;

  for(int ip = 1; ip <= bunchSize; ip++){
    phi_local = mp._phi(ip);
    if(pBunchTransModulationXF) x_shift = pBunchTransModulationXF->getY(phi_local);
    if(pBunchTransModulationYF) y_shift = pBunchTransModulationYF->getY(phi_local);
    if(pBunchDensityModulationF) macroSize_local = pBunchDensityModulationF->getY(phi_local)*pBunchMacroSize;
    pBunchRhoGrid->binParticleIntoArr3D(macroSize_local, mp._x(ip)+x_shift, mp._y(ip)+y_shift, mp._phi(ip)*coeffPhiToZ);
  }

  //-----------------------------------------------------
  //We prefer to make makePeriodicRing() instead of 
  //makeRing() , because we want to treat coasting beam 
  //correctly
  //-----------------------------------------------------
  //pBunchRhoGrid->makeRing();
  pBunchRhoGrid->makePeriodicRing();

  pBunchRhoGrid->findPotential(pBunchPhiGrid);
  if(addBoundaryPotentials == 1) pBunchPhiGrid->addBoundaryPotential();

  //debug part START
  //for(int i = 0; i < pBunchRhoGrid->getBinsZ(); i++){
  //  std::cout<<"debug z-sum rank="<<rank_MPI
  //	     <<" i="<<i 
  //	     <<" z ="<<pBunchRhoGrid->getGridValueZ(i)
  //	     <<" rho="<<pBunchRhoGrid->getSliceSum(i)*1.6e-7/(phiStep*coeffPhiToZ)
  //	     <<std::endl;
  //}
  //
  //debug part STOP

  //------------------------------------------------------
  //create longitudinal proton bunch profile  START
  //------------------------------------------------------
  double z;
  double z_min_local = z_min - phiStep*coeffPhiToZ/2.0;
  double z_max_local = z_max + phiStep*coeffPhiToZ/2.0;
  double z_step = ring_length/(nPointsPBunchProfile-1);
  for(int i = 0; i < nPointsPBunchProfile; i++){
    z = -ring_length/2.0 + i*z_step;
    if( z >= z_min_local && z <= z_max_local && z > zMinGlobal && z < zMaxGlobal){
      pBunchProfile[i] = fabs(pBunchRhoGrid->getSliceSum(z));
    }
    else{
      pBunchProfile[i] = 0.;
    }
  }

  if(size_MPI > 1){
    MPI_Allreduce(pBunchProfile,pBunchProfile_MPI,nPointsPBunchProfile, 
		  MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  }
  else{
    for(int i = 0; i < nPointsPBunchProfile; i++){
      pBunchProfile_MPI[i] = pBunchProfile[i];
    }
  }

  pBunchProfileF->clean();
  for(int i = 0; i < nPointsPBunchProfile; i++){
    z = -ring_length/2.0 + i*z_step;
    pBunchProfileF->add(z,pBunchProfile_MPI[i]*1.6e-7/(phiStep*coeffPhiToZ));
  }
  pBunchProfileF->setConstStep(1);

  double sum = 0.;
  for(int i = 1; i < nPointsPBunchProfile; i++){
    z = -ring_length/2.0 + i*z_step;
    sum += (pBunchProfile_MPI[i-1]+pBunchProfile_MPI[i])*z_step/2.;
  }

  if(sum != 0.){
    for(int i = 0; i < nPointsPBunchProfile; i++){
      pBunchProfile_MPI[i] /= sum;
    }
  }

  pBunchCommProfileF->clean();
  pBunchCommProfileF->add(-ring_length/2.0,0.);
  sum = 0.;
  for(int i = 1; i < nPointsPBunchProfile; i++){
    z = -ring_length/2.0 + i*z_step;
    sum += (pBunchProfile_MPI[i-1]+pBunchProfile_MPI[i])*z_step/2.;
    pBunchCommProfileF->add(z,sum);
  }
  pBunchCommProfileF->setConstStep(1);

  //------------------------------------------------------
  //debug part START
  //pBunchCommProfileF->print("rho_comm_pBunch.dat");
  //pBunchProfileF->print("rho_pBunch.dat");
  //debug part STOP
  //------------------------------------------------------

  //------------------------------------------------------
  //create longitudinal proton bunch profile  STOP
  //------------------------------------------------------

  //--------------------------------------------------------------
  //create array with the number of new generated electrons  START
  //--------------------------------------------------------------
  z_step = ring_length/(nLongSteps);
  int n_non_0 = 0;
  double rho;
  for(int i = 0; i < nLongSteps; i++){
    z = -ring_length/2.0 + (i+0.5)*z_step;
    rho = pBunchProfileF->getY(z);
    if(rho != 0.){
      n_non_0++;
      nNewGenElectronsArr[i] = 1;
    }
    else{
      nNewGenElectronsArr[i] = 0;
    }
  }
  if(n_non_0 != 0){
    int n_avg = nNewEletronsPerBunch/n_non_0;
    int n_rest = nNewEletronsPerBunch % n_non_0;
    int n_count = 0;
    for(int i = 0; i < nLongSteps; i++){
      if(nNewGenElectronsArr[i] != 0){
	if(n_count < n_rest){
	  nNewGenElectronsArr[i] = n_avg + 1;
	}
	else{
	  nNewGenElectronsArr[i] = n_avg;
	}
	n_count++; 
      }
    }
  }
  else{
    for(int i = 0; i < nLongSteps; i++){
      nNewGenElectronsArr[i] = 0;
    } 
  }
  //--------------------------------------------------------------
  //create array with the number of new generated electrons  STOP
  //--------------------------------------------------------------

}

//============================================================
//This method calls surface and volume generators
//with probabilities eGenSurfProb and eGenVolProb
//============================================================
void EP_NodeCalculator::generateElectrons(int iStep_In,
					  double s_In, 
					  double sStep, 
					  Grid3D* rhoGrid, 
					  eBunch* eb,
					  MacroPartDistributor* PartDistr)
{
  if(eGenSurfProb == 0. && eGenVolProb == 0.) return;
  double g = randGen::getRandU1();
  if(size_MPI > 1){
     MPI_Bcast(&g, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  }
  if(g < eGenSurfProb){
    generateElectronsOnSurface(iStep_In,s_In,sStep,rhoGrid,eb,PartDistr);
  }
  else{
    generateElectronsInVolume(iStep_In,s_In,sStep,rhoGrid,eb,PartDistr);
  }
}

//============================================================
//This method generates electrons on the surface and 
//uses data prepared by the makeProtonBunchArrays(...) method
//The PartDistr is not used here, but it is needed to create
//the same interface as for "in Volume electron generation".
//============================================================
void EP_NodeCalculator::generateElectronsOnSurface(int iStep_In,
						   double s_In, 
						   double sStep, 
						   Grid3D* rhoGrid, 
						   eBunch* eb,
						   MacroPartDistributor* PartDistr)
{

  double s = s_In;
  int iStep = iStep_In;
  if(s_In < (-ring_length/2.0)){
    s = ring_length + s_In;
    iStep = (int) ((s - ring_length/2.0)/sStep);
    if(iStep < 0) iStep = 0;
    if(iStep > (nLongSteps-1)) iStep = nLongSteps-1;
  }

  int nNew = nNewGenElectronsArr[iStep];
  int nNew_Global = nNew;
  if(nNew_Global == 0) {
    eb->compress();
    return;
  }
  int iInd;
  if(size_MPI > 1){
    for(int i = 0; i < size_MPI; i++){
      nNewGen_MPI[i] = 0;
    }
    for(int i = 0; i < nNew_Global; i++){
      iInd = (int)( size_MPI*randGen::getRandU1());
      nNewGen_MPI[iInd]++;
    }
    MPI_Bcast(nNewGen_MPI, size_MPI, MPI_INT, 0, MPI_COMM_WORLD);
  }
  else{
    nNewGen_MPI[rank_MPI] = nNew_Global;
  }

  nNew = nNewGen_MPI[rank_MPI];
  
  if(nNew == 0) {
    eb->compress();
    return;
  }
  
  double generatedMacroSize = pBunchTotalMacroSize*pBunchLossRate;
  generatedMacroSize *= newElectronsPerProton;
  generatedMacroSize *= (rhoGrid->getzGridMax() - rhoGrid->getzGridMin())/ring_length;
  generatedMacroSize *= (pBunchCommProfileF->getY(s+sStep) - pBunchCommProfileF->getY(s));
  generatedMacroSize /= nNew_Global;

  if(generatedMacroSize <= 0.){
    eb->compress();
    return;
  }

  int nP,iP;
  double r,x,y,z;
  double z_min = rhoGrid->getzGridMin();
  double z_max = rhoGrid->getzGridMax();
  for(int j = 0; j < nNew; j++){
    z = z_min + randGen::getRandU1()*(z_max - z_min);
    nP = boundary->getNumbBPoints();
    iP = (int) (nP*randGen::getRandU1());
    r = randGen::getRandU1();
    if(iP == (nP-1)) {
      x = boundary->getBoundPointX(0) + (boundary->getBoundPointX(iP) - boundary->getBoundPointX(0))*r;
      y = boundary->getBoundPointY(0) + (boundary->getBoundPointY(iP) - boundary->getBoundPointY(0))*r;
    }
    else{
      x = boundary->getBoundPointX(iP) + (boundary->getBoundPointX(iP+1) - boundary->getBoundPointX(iP))*r;
      y = boundary->getBoundPointY(iP) + (boundary->getBoundPointY(iP+1) - boundary->getBoundPointY(iP))*r;
    }
    eb->addParticle(generatedMacroSize,x,y,z,0.,0.,0.);
  }   
  eb->compress();
}


//============================================================
//This method generates electrons in the volume and 
//uses data prepared by the makeProtonBunchArrays(...) method
//============================================================
void EP_NodeCalculator::generateElectronsInVolume(int iStep_In,
						  double s_In, 
						  double sStep, 
						  Grid3D* rhoGrid, 
						  eBunch* eb,
						  MacroPartDistributor* PartDistr)
{

  double s = s_In;
  int iStep = iStep_In;
  if(s_In < (-ring_length/2.0)){
    s = ring_length + s_In;
    iStep = (int) ((s - ring_length/2.0)/sStep);
    if(iStep < 0) iStep = 0;
    if(iStep > (nLongSteps-1)) iStep = nLongSteps-1;
  }

  int nNew = nNewGenElectronsArr[iStep];
  int nNew_Global = nNew;
  if(nNew_Global == 0) {
    eb->compress();
    return;
  }

  int iInd;
  if(size_MPI > 1){
    for(int i = 0; i < size_MPI; i++){
      nNewGen_MPI[i] = 0;
    }
    for(int i = 0; i < nNew_Global; i++){
      iInd = (int)( size_MPI*randGen::getRandU1());
      nNewGen_MPI[iInd]++;
    }
    MPI_Bcast(nNewGen_MPI, size_MPI, MPI_INT, 0, MPI_COMM_WORLD);
  }
  else{
    nNewGen_MPI[rank_MPI] = nNew_Global;
  }

  nNew = nNewGen_MPI[rank_MPI];

  //prepare the transverse plane distribution
  //it uses MPI comm, so each CPU should have 
  //acess to this point of the code

  if(s <= zMinGlobal || s >= zMaxGlobal){
    return;
  }

  eVolumeProdGrid->clean();
  double sum = 0.;
  int iCPU = PartDistr->getNodeIndex(s/coeffPhiToZ);
  if(iCPU == rank_MPI){
    pBunchRhoGrid->setValuesForSlice(s,0,eVolumeProdGrid);

    for(int i_x = 0, i_x_max = eVolumeProdGrid->getBinsX(); i_x < i_x_max; i_x++){
      for(int i_y = 0, i_y_max = eVolumeProdGrid->getBinsY(); i_y < i_y_max; i_y++){
	eVolumeProdGrid->getArr3D()[0][i_x][i_y] = fabs(eVolumeProdGrid->getArr3D()[0][i_x][i_y]);
      }
    }

    sum = eVolumeProdGrid->getSum();
    if(sum > 0.){
      eVolumeProdGrid->multiply(1.0/sum);
    }

  }

  eVolumeProdGrid->broadcast(iCPU);
  sum = eVolumeProdGrid->getSum();
  if( sum == 0.) {
    return;
  }

  //debug - artifitially prepared distribution =====START===
  //double xd,yd;
  //eVolumeProdGrid->clean();
  //for(int i_x = 0, i_x_max = eVolumeProdGrid->getBinsX(); i_x < i_x_max; i_x++){
  //  for(int i_y = 0, i_y_max = eVolumeProdGrid->getBinsY(); i_y < i_y_max; i_y++){
  //    xd = eVolumeProdGrid->getGridValueX(i_x);
  //    yd = eVolumeProdGrid->getGridValueY(i_y);
  //    eVolumeProdGrid->getArr3D()[0][i_x][i_y] = exp(-(xd*xd+yd*yd)/2000.0);
  //  }
  //}
  //sum = eVolumeProdGrid->getSum();
  //eVolumeProdGrid->multiply(1.0/sum);
  //debug - artifitially prepared distribution =====STOP====

  //if this particular CPU should not produce electrons 
  //we have to leave
  if(nNew == 0) {
    eb->compress();
    return;
  }
  
  double generatedMacroSize = pBunchTotalMacroSize*pBunchLossRate;
  generatedMacroSize *= newElectronsPerProton;
  generatedMacroSize *= (rhoGrid->getzGridMax() - rhoGrid->getzGridMin())/ring_length;
  generatedMacroSize *= (pBunchCommProfileF->getY(s+sStep) - pBunchCommProfileF->getY(s));
  generatedMacroSize /= nNew_Global;

  if(generatedMacroSize <= 0.){
    eb->compress();
    return;
  }

  int indX, indY;
  double g;
  double x,y,z;
  double z_min = rhoGrid->getzGridMin();
  double z_max = rhoGrid->getzGridMax();
  for(int j = 0; j < nNew; j++){

    g = randGen::getRandU1();
    sum = 0.;
    indX = -1;
    indY = -1;
    for(int i_x = 0, i_x_max = eVolumeProdGrid->getBinsX(); i_x < i_x_max; i_x++){
      for(int i_y = 0, i_y_max = eVolumeProdGrid->getBinsY(); i_y < i_y_max; i_y++){
        sum += eVolumeProdGrid->getArr3D()[0][i_x][i_y];
        if( g < sum ){
	  indX = i_x;
          indY = i_y;
	  break;
	}
      }
      if( indX >= 0 && indY >= 0){
	break;
      }
    }

    //this should never happen, but ...
    if( indX < 0 || indY < 0) continue;

    z = z_min + randGen::getRandU1()*(z_max - z_min);
    x = eVolumeProdGrid->getGridValueX(indX) - eVolumeProdGrid->getStepX()*(0.5 - randGen::getRandU1());
    y = eVolumeProdGrid->getGridValueY(indY) - eVolumeProdGrid->getStepY()*(0.5 - randGen::getRandU1());
    if( EP_Boundary::IS_INSIDE ==  boundary->isInside(x,y)){
      eb->addParticle(generatedMacroSize,x,y,z,0.,0.,0.);
    }
  }   
  eb->compress();
}


//==========================================================================
//This method defines potential from the proton bunch in pBunchFieldSource
//==========================================================================
void EP_NodeCalculator::setPBunchFiledSource(double s, 
					     pBunchFieldSource* pbF,
					     MacroPartDistributor* PartDistr)
{

  //-----------------------------------------------------------------
  //this method works directly with PhiGrid3D in
  //the pBunchFieldSource* pbF. 
  //s could be outside the [-ring_length/2.0 , +ring_length/2.0]
  //limits, so we have to decide about each z - position.
  //Each slice of pBunchPhiGrid is considered locally (local CPU)
  //so we need broadcast all data for all CPUs.
  //----------------------------------------------------------------- 

  Grid3D* phiExtGr = pbF->getPhiGrid3D();
  phiExtGr->clean();

  double z_min = phiExtGr->getzGridMin();
  double z_max = phiExtGr->getzGridMax();
  double z_center = (z_max + z_min)/2.0;

  pbF->setBeta(beta);

  int nZ = phiExtGr->getBinsZ();
  double z,z_In;
  int iCPU;
  for(int i = 0; i < nZ; i++){

    z_In = s - (z_center - phiExtGr->getGridValueZ(i));
    z = z_In;

    if(z_In < (-ring_length/2.0)){
      z = z_In + ring_length;
    }

    else if(z_In > (ring_length/2.0)){
      z = z_In - ring_length;
    }    

    if(z < zMinGlobal || z > zMaxGlobal) continue;

    iCPU = PartDistr->getNodeIndex(z/coeffPhiToZ);
    if(iCPU == rank_MPI){
      pBunchPhiGrid->setValuesForSlice(z,i,phiExtGr);
    }
    phiExtGr->broadcast(iCPU,i);
  }

}

//============================================================
//This method adds the x,y,and z kicks to the momentum of 
//the protons in the pbunch. 
//The source for potential - ebFields->getPhiGrid3D()
//The recivers - pBunchKickX,pBunchKickY,pBunchKickZ
//============================================================
void EP_NodeCalculator::accountPBunchKick(double s,
					  double sStep, 
					  MacroPartDistributor* PartDistr, 
					  MacroPart &mp)
{

  //s variable is the position of the center of the e-cloud region

  double n_z_slices = pBunchKickX->getBinsZ();

  //we are going to consider only internal z-slices
  //(left and right are excluded)
  //z_start, z_stop - z-coordinates of the kick slice
  //in the coordinate system of  eCloudFieldSource* ebFields grid

  Grid3D* ebPhiGrid = ebFields->getPhiGrid3D();

  double z_start, z_stop;
  double zGr_min, zGr_max;
  zGr_min = ebPhiGrid->getzGridMin();
  zGr_max = ebPhiGrid->getzGridMax();

  //Left side motion.
  //We have to consider left and right 
  //side motion because of periodic conditions.

  double gradX,gradY,gradZ;

  double pCharge = mp._syncPart._charge;

  double coeff_to_ctE_long = pCharge*epModuleConstants::classicalRadius_proton/(beta*beta*gamma);

  //dp/p0 - in ORBIT this is in [mrad] , so we have to multiply by 1000
  double coeff_to_ctE_tr   = 1000.0 * coeff_to_ctE_long;

  double coeff_to_ctE_local_tr;
  double coeff_to_ctE_local_long;
  double** xKickSlice = NULL;
  double** yKickSlice = NULL;
  double** zKickSlice = NULL;

  double x,y,z;

  for(int i_z = 0; i_z < n_z_slices; i_z++){
    z_start = pBunchKickX->getGridValueZ(i_z) - s; 
    z_stop  = z_start + sStep;
    if(z_start >= zGr_max) continue;
    if(z_stop  <= zGr_min) continue;
    if(z_start <  zGr_min) z_start = zGr_min;
    if(z_stop  >  zGr_max) z_stop  = zGr_max;

    xKickSlice = pBunchKickX->getSlice2D(i_z);
    yKickSlice = pBunchKickY->getSlice2D(i_z);
    zKickSlice = pBunchKickZ->getSlice2D(i_z);

    coeff_to_ctE_local_tr   = coeff_to_ctE_tr *(z_start - z_stop);
    coeff_to_ctE_local_long = coeff_to_ctE_long*(z_start - z_stop);
    z = (z_start + z_stop)/2.0;
    for(int i_x = 0, i_x_max = pBunchKickX->getBinsX(); i_x < i_x_max; i_x++){
      x = ebPhiGrid->getGridValueX(i_x);
      for(int i_y = 0, i_y_max = pBunchKickX->getBinsY(); i_y < i_y_max; i_y++){
	y = ebPhiGrid->getGridValueY(i_y);
	ebPhiGrid->calcGradient(x,gradX,y,gradY,z,gradZ);
	xKickSlice[i_x][i_y] += coeff_to_ctE_local_tr*gradX;
	yKickSlice[i_x][i_y] += coeff_to_ctE_local_tr*gradY;
	zKickSlice[i_x][i_y] += coeff_to_ctE_local_long*gradZ;  
      }
    }
  }



  //Right side motion.
  //We have to consider left and right 
  //side motion because of periodic conditions.
  s = s + ring_length;

  for(int i_z = 0; i_z < n_z_slices; i_z++){
    z_start = pBunchKickX->getGridValueZ(i_z) - s; 
    z_stop  = z_start + sStep;
    if(z_start >= zGr_max) continue;
    if(z_stop  <= zGr_min) continue;
    if(z_start <  zGr_min) z_start = zGr_min;
    if(z_stop  >  zGr_max) z_stop  = zGr_max;

    xKickSlice = pBunchKickX->getSlice2D(i_z);
    yKickSlice = pBunchKickY->getSlice2D(i_z);
    zKickSlice = pBunchKickZ->getSlice2D(i_z);

    coeff_to_ctE_local_tr   = coeff_to_ctE_tr *(z_start - z_stop);
    coeff_to_ctE_local_long = coeff_to_ctE_long*(z_start - z_stop);
    z = (z_start + z_stop)/2.0;
    for(int i_x = 0, i_x_max = pBunchKickX->getBinsX(); i_x < i_x_max; i_x++){
      x = ebPhiGrid->getGridValueX(i_x);
      for(int i_y = 0, i_y_max = pBunchKickX->getBinsY(); i_y < i_y_max; i_y++){
	y = ebPhiGrid->getGridValueY(i_y);
	ebPhiGrid->calcGradient(x,gradX,y,gradY,z,gradZ);
	xKickSlice[i_x][i_y] += coeff_to_ctE_local_tr*gradX;
	yKickSlice[i_x][i_y] += coeff_to_ctE_local_tr*gradY;
	zKickSlice[i_x][i_y] += coeff_to_ctE_local_long*gradZ;        
      }
    }
  }
}

//============================================================
//This method applys the x,y,and z kicks to the momentum of 
//the protons in the pbunch. 
//The source - pBunchKickX,pBunchKickY,pBunchKickZ
//The macro-particles (protons) are already distributed, so
//we should not worry about the CPU rank.
//============================================================  
void EP_NodeCalculator::applyPBunchKick(MacroPart &mp)
{

  //tracks of first and last slice could be wrong
  if(rank_MPI == 0){
    for(int i_x = 0, i_x_max = pBunchKickX->getBinsX(); i_x < i_x_max; i_x++){
      for(int i_y = 0, i_y_max = pBunchKickX->getBinsY(); i_y < i_y_max; i_y++){
	pBunchKickX->getSlice2D(0)[i_x][i_y] = pBunchKickX->getSlice2D(1)[i_x][i_y];
	pBunchKickY->getSlice2D(0)[i_x][i_y] = pBunchKickY->getSlice2D(1)[i_x][i_y];
	pBunchKickZ->getSlice2D(0)[i_x][i_y] = pBunchKickZ->getSlice2D(1)[i_x][i_y];
      }
    }
  }

  if(rank_MPI == (size_MPI-1)){
    int ind_last = pBunchKickX->getBinsZ();
    for(int i_x = 0, i_x_max = pBunchKickX->getBinsX(); i_x < i_x_max; i_x++){
      for(int i_y = 0, i_y_max = pBunchKickX->getBinsY(); i_y < i_y_max; i_y++){
	pBunchKickX->getSlice2D(ind_last-1)[i_x][i_y] = pBunchKickX->getSlice2D(ind_last-2)[i_x][i_y];
	pBunchKickY->getSlice2D(ind_last-1)[i_x][i_y] = pBunchKickY->getSlice2D(ind_last-2)[i_x][i_y];
	pBunchKickZ->getSlice2D(ind_last-1)[i_x][i_y] = pBunchKickZ->getSlice2D(ind_last-2)[i_x][i_y];
      }
    }
  }

  //apply kicks
  int bunchSize = mp._nMacros;
  double x,y,z;
  int xInd,yInd,zInd;
  double xFrac,yFrac,zFrac;

  double kickX,kickY,kickZ;
  for(int ip = 1; ip <= bunchSize; ip++){
    x = mp._x(ip);
    y = mp._y(ip);
    z = mp._phi(ip)*coeffPhiToZ;
    pBunchKickX->getGridIndAndFrac(x,xInd,xFrac,
				   y,yInd,yFrac,
				   z,zInd,zFrac);
    kickX = effectiveLengthCoeff*pBunchKickX->calcValue(xInd,yInd,zInd,xFrac,yFrac,zFrac);
    kickY = effectiveLengthCoeff*pBunchKickY->calcValue(xInd,yInd,zInd,xFrac,yFrac,zFrac);
    kickZ = effectiveLengthCoeff*pBunchKickZ->calcValue(xInd,yInd,zInd,xFrac,yFrac,zFrac);

    mp._xp(ip)     += kickX;
    mp._yp(ip)     += kickY;
    mp._dp_p(ip)   += kickZ;
    mp._deltaE(ip)  = mp._dp_p(ip) / mp._syncPart._dppFac;
  }
}

//=====================================================
//convenience methods
//=====================================================

baseParticleTracker* EP_NodeCalculator::getTracker()
{
  return tracker;
}

void EP_NodeCalculator::setEBunchDiagnostics(CompositeEBunchDiag* eBunchMainDiag_In)
{
  eBunchMainDiag = eBunchMainDiag_In;
}

//=====================================================
//convenience methods with Shell interface
//=====================================================

//------------------------------------------------------------------------
//sets surface and volume probability production
//------------------------------------------------------------------------
void EP_NodeCalculator::setElectronGenerationProbab(double eGenSurfProb_In, 
						    double eGenVolProb_In)
{
  if(eGenSurfProb_In < 0. || eGenVolProb_In < 0.) return;
  eGenSurfProb = eGenSurfProb_In;
  eGenVolProb  = eGenVolProb_In;
  if(eGenSurfProb_In > 0. || eGenVolProb_In > 0.){
      eGenSurfProb = eGenSurfProb_In/(eGenSurfProb_In + eGenVolProb_In);
      eGenVolProb  = eGenVolProb_In/(eGenSurfProb_In + eGenVolProb_In);
  }
}

//------------------------------------------------------------------------
//sets electron production params
//------------------------------------------------------------------------
void EP_NodeCalculator::setElectronGenerationParams(int nNewEletronsPerBunch_In, 
						    double newElectronsPerProton_In,
						    double pBunchLossRate_In)
{
  nNewEletronsPerBunch = nNewEletronsPerBunch_In;
  newElectronsPerProton = newElectronsPerProton_In;
  pBunchLossRate = pBunchLossRate_In;
}

//------------------------------------------------------------------------
//sets the number of steps in longitudinal direction (default 1500)
//------------------------------------------------------------------------
void EP_NodeCalculator::setNumberLongSteps(int nLongSteps_In)
{
  free(nNewGenElectronsArr);
  nLongSteps = nLongSteps_In;
  nNewGenElectronsArr =  (int *) malloc (sizeof(int) * nLongSteps);
}

//------------------------------------------------------------------------
//sets the number of pBunch field updates (default 1)
//------------------------------------------------------------------------
void EP_NodeCalculator::setNumberPBunchFieldUpdates(int nPBunchFiledUpdateSteps_In)
{
  nPBunchFiledUpdateSteps = nPBunchFiledUpdateSteps_In;
}

//------------------------------------------------------------------------
//sets the number of steps of integration in the slice (default 20)
//------------------------------------------------------------------------
void EP_NodeCalculator::setNumberStepsInSlice(int nTrackerSteps_In)
{
  nTrackerSteps_In = nTrackerSteps;
}

//------------------------------------------------------------------------
//sets using simplectic or non-simplectic tracking (default 0)
//------------------------------------------------------------------------
void EP_NodeCalculator::setSimplecticTraking(int useSimplecticTracker_In)
{
  if(useSimplecticTracker_In == 1){
    useSimplecticTracker = 1;
  }
  else{
    useSimplecticTracker = 0;
  }
}

//------------------------------------------------------------------------
//define will it be there the debugging output (default 0)
//------------------------------------------------------------------------
void EP_NodeCalculator::setTrackerDebugOutput(int infDebugOutput_In)
{
  if(infDebugOutput_In == 1){
    infDebugOutput = 1;
  }
  else{
    infDebugOutput = 0;
  }
}

//------------------------------------------------------------------------
//defines should we apply kicks to p-bunch or not (default = 1 - yes)
//------------------------------------------------------------------------
void EP_NodeCalculator::setInfoPBunchKicksApply(int infoPBunchKicksApplying_In)
{
  if(infoPBunchKicksApplying_In == 0){
    infoPBunchKicksApplying = 0;
  }
  else{
    infoPBunchKicksApplying = 1;
  }
}

//set the effective length coefficient 
//it will affect on p-Bunch kick ( by default it is 1.0)
void EP_NodeCalculator::setEffLengthCoeff(double effectiveLengthCoeffIn)
{
  effectiveLengthCoeff = effectiveLengthCoeffIn;
}

//------------------------------------------------------------------------
//returns maximal e-cloud linear density
//------------------------------------------------------------------------
double EP_NodeCalculator::getMaxECloudDensity()
{
  return maxEDensity;
}

//------------------------------------------------------------------------
//add boundary potaential or not : addBoundaryPotentials = 1 yes 0 -no
//------------------------------------------------------------------------
void EP_NodeCalculator::setAddBoundaryPotentialsInfo(int addBoundaryPotentialsIn)
{
  addBoundaryPotentials = addBoundaryPotentialsIn;
}

//------------------------------------------------------------------------
//sets the transverse modulation function for p-bunch in x-direction
//this is mostly for debugging
//------------------------------------------------------------------------
void EP_NodeCalculator::setPBunchTransFunctionX(Function* xTransFunc_In)
{
  if(xTransFunc_In){
    if(pBunchTransModulationXF) delete pBunchTransModulationXF;
  }
  pBunchTransModulationXF = xTransFunc_In;
}

//------------------------------------------------------------------------
//sets the transverse modulation function for p-bunch in y-direction
//this is mostly for debugging
//------------------------------------------------------------------------
void EP_NodeCalculator::setPBunchTransFunctionY(Function* yTransFunc_In)
{
  if(yTransFunc_In){
    if(pBunchTransModulationYF) delete pBunchTransModulationYF;
  }
  pBunchTransModulationYF = yTransFunc_In;
}

//------------------------------------------------------------------------
//sets the density modulation function for p-bunch in phi-direction
//this is mostly for debugging
//------------------------------------------------------------------------
void EP_NodeCalculator::setPBunchDensityFunction(Function* densityFunc_In)
{
  if(densityFunc_In){
    if(pBunchDensityModulationF) delete pBunchDensityModulationF;
  }
  pBunchDensityModulationF = densityFunc_In;
}
