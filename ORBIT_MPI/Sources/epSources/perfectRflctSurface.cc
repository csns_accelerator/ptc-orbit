//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    perfectRflctSurface.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    04/01/2003
//
// DESCRIPTION
//    Source code for the subclass "perfectRflctSurface" used to ramove
//    macroparticles acrossing the surface and reproduce the perfectly 
//    reflected new macro particles in the class EPnode
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "perfectRflctSurface.hh"
#include "math.h"

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   perfectRflctSurface::perfectRflctSurface()
//   perfectRflctSurface::~perfectRflctSurface()
//
// DESCRIPTION
//   Constructor and Desctructor
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

perfectRflctSurface::perfectRflctSurface()
{
}

perfectRflctSurface::~perfectRflctSurface()
{
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   perfectRflctSurface::impact
//
// DESCRIPTION
//   remove macropartices which cross the given surface at position r_v 
//   and add the new macroparticles which has the perfectly reflected 
//   momentum at the posirion r_v to the list of macroparticles in eBunch
//
// RETURNS
//   Nothing.
//
// REMARKS
//   Please use "eb->compress();" AFTER the LOOP usage of this method to eBunch
//   not to reorder the index of eBunch inside the loop
///////////////////////////////////////////////////////////////////////////

void perfectRflctSurface::impact(int index, eBunch* eb, 
				 double* r_v,double* n_v)
{
  double macroSize= eb->macroSize(index);
  double px= eb->px(index), py= eb->py(index), pz= eb->pz(index);
  double pp= eb->pp(index), pt= eb->pt(index);
  eb->deleteParticle(index);

  double nx=n_v[0], ny=n_v[1], nz=n_v[2];
  double pout_x = px +2.0*fabs(px*nx+py*ny+pz*nz)*nx;
  double pout_y = py +2.0*fabs(px*nx+py*ny+pz*nz)*ny;
  double pout_z = pz +2.0*fabs(px*nx+py*ny+pz*nz)*nz;

  eb->addParticle(macroSize,r_v[0],r_v[1],r_v[2], 
		  pout_x,pout_y,pout_z,pp,pt);

  // x != r_v[0] for (x,y,z) cannot be the exact point of the surface
  // with condidering time steps
  //new pp, pt also along with the magnetic field
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
