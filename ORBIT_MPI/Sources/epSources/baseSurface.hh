//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    baseSurface.hh
//
// AUTHOR
//    A. Shishlo
//
// CREATED
//    03/31/2003
//
// DESCRIPTION
//    Specification and inline functions for a class used to manage 
//    macro particles which hit the surface in the class EPnode
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "stdlib.h"

#ifndef BASE_SURFACE_H
#define BASE_SURFACE_H
  
#include "eBunch.hh"
#include "Function.hh"

///////////////////////////////////////////////////////////////////////////
//
// SUBCLASS NAME
//    absrbSurface
//
// CLASS RELATIONSHIPS
// 
//    subclass absrbSurface -> class baseSurface
//    class baseSurface class EP_Boundary, class eBunch -> class EPnode
//
// DESCRIPTION
//    A subclass to absorb macro particles which hit the surface. 
//    Actually, it removes those macroparticles from the bunch of 
//    electrons in the EPnode
//
// PUBLIC METHODS
//    baseSurface:    Constructor for making the subclass objects
//    ~baseSurface:   Destructor for this subclass
//    impact:         removes the macroparticles which hit the surface
//                    and add newly produced macro particles at the hitted
//                    points. The process to make new macro particles is
//                    defind in each subclasses
//
// PRIVATE/PROTECTED METHODS/MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class  baseSurface
{
public:

  baseSurface();
  virtual ~baseSurface();

  virtual void impact(int index, eBunch* eb, double* r_v,double* n_v);
 
  virtual void setDeathFunction(Function* func);
  virtual void setDeathProbability(double p_death_defaultIn);  
  
  virtual void setNewBornNumberFunction(Function* func);
  virtual void setNewBornNumber(int n_new_born_defaultIn);
  virtual void setMacroPartsHardLimitN(int macroPartsMaxNIn);
};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
