//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    randGen.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    08/06/2003
//
// DESCRIPTION
//   The source of random number
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include "randGen.hh"
#include <cstdlib>


randGen::randGen()
{
}

randGen::~randGen()
{
}

//returns uniform random number between [0,1]
double randGen::getRandU()
{
  return rand()/(0.0+RAND_MAX);
}

//returns uniform random number between [0,1]
double randGen::getRandU1()
{
  return rand()/(1.0+RAND_MAX);
}

void randGen::setInitialState(int ini)
{
  srand((unsigned int) ini);
}
