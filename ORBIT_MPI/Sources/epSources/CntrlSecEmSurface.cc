//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    CntrlSecEmSurface.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/31/2003
//
// DESCRIPTION
//    This is a subclass of the baseSurface class.
//    The controlled surface model with a secondary emission as 
//    specified by :
//    Ref.: PRST-AB5, 124404(2002)
//    Controlled surface means that this surface class can control
//    production and deleting of the new macro-electrons according to
//    the two function and three parameters
//    p_death(energy) - this function specifies the probability of 
//                      the death of the incoming macro-electrons.
//                      If this function was not specified the parameter
//                      p_death_default (0. by default) will be used.  
//    n_born(energy)  - number of the macro-particles that will be born
//                      as a function of the energy of the incoming 
//                      particle. If this function was not specified 
//                      number of the new macro-particles is 
//                      n_new_born_default (1 - by default).
//    p_el  - relative probability factor for elastic scattering process
//    p_rd  - relative probability factor for rediffusing process
//    p_ts  - relative probability factor for true secondary emission process
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "CntrlSecEmSurface.hh"

#include "randGen.hh"
#include "epModuleConstants.hh"
#include "numericalFunctions.hh"

CntrlSecEmSurface::CntrlSecEmSurface()
{
  //0 for Copper, 1 for Stainless Steal, 2 for Titanium nitride coated
  int surfaceType=1;
  init(surfaceType);
}

CntrlSecEmSurface::CntrlSecEmSurface(int surfaceType)
{
  if(surfaceType != 0 && surfaceType != 1 && surfaceType != 2){
    finalize("Constructor of the CntrlSecEmSurface class: type should 0, 1 or 2!");
  }
  init(surfaceType);
}

//surfaceType = 0 for Copper, 1 for Stainless Steal, 2 for Titanium nitride coated
void CntrlSecEmSurface::init(int surfaceType)
{
  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  }

  mass            = epModuleConstants::mass_electron; // in [MeV]
  charge          = epModuleConstants::charge_electron;

  //------------------------------------------------------------------
  //0 for Copper, 1 for Stainless Steal, 2 for Titanium nitride coated
  //------------------------------------------------------------------
  
  if(surfaceType == 0){
    //this is copper

    N_ELECTRONS_MAX = 10;

    alpha_=1.0;

    P_1_e_infiniteE_= 0.02; 
    P_1_e_hat_= 0.496; 
    E_e_hat_= 0.0; 
    W_= 60.86; 
    p_= 1.0; 
    sigma_e_= 0.5; 
    e1_=0.26; 
    e2_=2.0;             //sigma_e_=2.0;

    P_1_r_infiniteE_=0.2; 
    E_r_=0.041; r_=0.104; 
    q_=0.5; 
    r1_=0.26; 
    r2_=2.0;

    delta_ts_hat_=1.8848; 
    E_ts_hat_=276.8; 
    s_=1.54; 
    t1_=0.66; 
    t2_=0.8; 
    t3_=0.7; 
    t4_=1.0;

    E_t_hat_=271.0; 
    delta_t_hat_=2.1;

    p_n_ = new double[N_ELECTRONS_MAX];
    p_n_[0]=2.5;p_n_[1]=3.3;p_n_[2]=2.5;p_n_[3]=2.5;p_n_[4]=2.8;
    p_n_[5]=1.3;p_n_[6]=1.5;p_n_[7]=1.5;p_n_[8]=1.5;p_n_[9]=1.5;

    eps_n_ = new double[N_ELECTRONS_MAX];
    eps_n_[0]=1.5;eps_n_[1]=1.75;eps_n_[2]=1.0;eps_n_[3]=3.75;eps_n_[4]=8.5;
    eps_n_[5]=11.5;eps_n_[6]=2.5;eps_n_[7]=3.0;eps_n_[8]=2.5;eps_n_[9]=3.0;
  }
  else if(surfaceType == 1){
    //this is stainless steel

    N_ELECTRONS_MAX = 10;

    alpha_=1.0;

    P_1_e_infiniteE_=0.07; 
    P_1_e_hat_=0.5; 
    E_e_hat_=0.0; 
    W_=100.0; 
    p_=0.9; 
    sigma_e_=1.9; 
    e1_=0.26; 
    e2_=2.0;                            

    P_1_r_infiniteE_=0.74; 
    E_r_=40.0; 
    r_=1.0; 
    q_=0.4; 
    r1_=0.26; 
    r2_=2.0;          
    
    delta_ts_hat_=1.22; 
    E_ts_hat_=310.0; 
    s_=1.813; 
    t1_=0.66; 
    t2_=0.8; 
    t3_=0.7; 
    t4_=1.0;       
    
    E_t_hat_=292.0; 
    delta_t_hat_=2.05;

    p_n_ = new double[N_ELECTRONS_MAX];
    p_n_[0]=1.6;p_n_[1]=2.0;p_n_[2]=1.8;p_n_[3]=4.7;p_n_[4]=1.8;
    p_n_[5]=2.4;p_n_[6]=1.8;p_n_[7]=1.8;p_n_[8]=2.3;p_n_[9]=1.8;

    eps_n_ = new double[N_ELECTRONS_MAX];
    eps_n_[0]=3.9;eps_n_[1]=6.2;eps_n_[2]=13.0;eps_n_[3]=8.8; eps_n_[4]=6.25;
    eps_n_[5]=2.25;eps_n_[6]=9.2;eps_n_[7]=5.3;eps_n_[8]=17.8;eps_n_[9]=10.0;
  }
  else{
    //this is titanium nitride coated

    N_ELECTRONS_MAX = 10;

    alpha_=1.0;

    P_1_e_infiniteE_= 0.02; 
    P_1_e_hat_= 0.5; 
    E_e_hat_= 0.0; 
    W_= 60.0; 
    p_= 1.0; 
    sigma_e_= 0.5; 
    e1_=0.26; 
    e2_=2.0;             //sigma_e_=2.0;

    P_1_r_infiniteE_=0.19; 
    E_r_=0.04; r_=0.1; 
    q_=0.5; 
    r1_=0.26; 
    r2_=2.0;

    delta_ts_hat_=1.8; 
    E_ts_hat_=246.0; 
    s_=1.54; 
    t1_=0.66; 
    t2_=0.8; 
    t3_=0.7; 
    t4_=1.0;

    E_t_hat_=250.0; 
    delta_t_hat_=2.0;

    p_n_ = new double[N_ELECTRONS_MAX];
    p_n_[0]=2.5;p_n_[1]=3.3;p_n_[2]=2.5;p_n_[3]=2.5;p_n_[4]=2.8;
    p_n_[5]=1.3;p_n_[6]=1.5;p_n_[7]=1.5;p_n_[8]=1.5;p_n_[9]=1.5;

    eps_n_ = new double[N_ELECTRONS_MAX];
    eps_n_[0]=1.5;eps_n_[1]=1.75;eps_n_[2]=1.0;eps_n_[3]=3.75;eps_n_[4]=8.5;
    eps_n_[5]=11.5;eps_n_[6]=2.5;eps_n_[7]=3.0;eps_n_[8]=2.5;eps_n_[9]=3.0;
  }


  init();
}

//calculates all arrays
void CntrlSecEmSurface::init()
{
  //factorial definition
  factorial = new double[N_ELECTRONS_MAX+1];
  factorial[0] = 1.;
  factorial[1] = 1.;  
  for(int i = 2; i <= N_ELECTRONS_MAX; i++){
    factorial[i] = i*factorial[i-1];
  }

  //temporary members
  prob_array_tmp = new double[N_ELECTRONS_MAX];

  //default parameters
  macroPartsMaxN = 250000;
  p_death_func       = NULL;
  p_death_default    = 0.;
  n_born_func        = NULL;
  n_new_born_default = 1;

  p_el = 1.0;
  p_rd = 1.0;
  p_ts = 1.0; 
  
  double sum_p = p_el + p_rd + p_ts;
  p_el /= sum_p;
  p_rd /= sum_p;
  p_ts /= sum_p;

  //number of point for distribution function
  double nP = 300;
  double x_tmp,f_tmp;
  double x_max;
  Function tmp_func;

  //cumulative distribution function 
  //for Gauss distribution with sigma = 1.
  //this is intergral_from_0_to_x(exp(-t^2/2)*d(t))
  x_max = 5.0;
  for(int i = 0; i <= nP; i++){
    x_tmp = i*(x_max/nP);
    f_tmp = numericalFunctions::incompleteGamma(0.5,(x_tmp*x_tmp)/2.0);
    tmp_func.add(x_tmp,f_tmp);
  }
  tmp_func.normalize();
  gaussInverseDistrFunc = new Function();
  tmp_func.setInverse(gaussInverseDistrFunc);

  //The commulative distribution functions for true secondary emission
  //for different number of emitted electrons from 1 up to N_ELECTRONS_MAX
  distrFunc_ts = new Function*[N_ELECTRONS_MAX];
  double dlt_limit = 1.0e-5;
  for(int j = 0; j < N_ELECTRONS_MAX; j++){
    distrFunc_ts[j]  = new Function();
    tmp_func.clean();
    x_max = 0.;

    while(fabs(numericalFunctions::incompleteGamma(p_n_[j],x_max) - 1.0) >  dlt_limit){
      x_max += 1.0; 
    }

    for(int i = 0; i <= nP; i++){
       x_tmp = i*(x_max/nP);
       f_tmp = numericalFunctions::incompleteGamma(p_n_[j],x_tmp);
       tmp_func.add(x_tmp,f_tmp);
    }
    tmp_func.normalize();
    tmp_func.setInverse(distrFunc_ts[j]);
  }
}


//destructor
CntrlSecEmSurface::~CntrlSecEmSurface()
{
  delete [] p_n_;
  delete [] eps_n_;

  if(p_death_func){
    delete p_death_func;
  }

  if(n_born_func){
    delete n_born_func;
  }

  for(int i = 0; i < N_ELECTRONS_MAX; i++){
    delete distrFunc_ts[i];
  }
  delete [] distrFunc_ts;

  delete [] factorial;

  delete [] prob_array_tmp;

  delete gaussInverseDistrFunc;

}

void CntrlSecEmSurface::finalize(const char* message)
{
  if(iMPIini > 0){
    MPI_Finalize();
  }
  std::cerr<<message<<std::endl;
  std::cerr<<"Stop from CntrlSecEmSurface class."<<std::endl;
  exit(1); 
}

void CntrlSecEmSurface::setMacroPartsHardLimitN(int macroPartsMaxNIn)
{
  macroPartsMaxN = macroPartsMaxNIn;
}

int CntrlSecEmSurface::getMacroPartsHardLimitN()
{
  return macroPartsMaxN;
}

void CntrlSecEmSurface::setDeathFunction(Function* func)
{
  if(p_death_func) delete p_death_func;
  p_death_func = func;
}

void CntrlSecEmSurface::setDeathProbability(double p_death_defaultIn)
{
  p_death_default = p_death_defaultIn;
}

void CntrlSecEmSurface::setNewBornNumberFunction(Function* func)
{
  if(n_born_func) delete n_born_func;
  n_born_func = func;
}

void CntrlSecEmSurface::setNewBornNumber(int n_new_born_defaultIn)
{
  n_new_born_default = n_new_born_defaultIn;
}

void CntrlSecEmSurface::setRelativeProbability(double p_elIn,double p_rdIn,double p_tsIn)
{
  p_el = p_elIn;
  p_rd = p_rdIn;
  p_ts = p_tsIn; 
  
  double sum_p = p_el + p_rd + p_ts;

  p_el /= sum_p;
  p_rd /= sum_p;
  p_ts /= sum_p;
}

//binomial finction n1!/((n2!)*((n1-n2)!))
double CntrlSecEmSurface::binomial(int n1,int n2)
{
  return factorial[n1]/(factorial[n2]*factorial[n1-n2]);
}

//chooses index according Monte-Carlo procedure 
//with weights[0-n_point] that are not normalized
int CntrlSecEmSurface::chooseIndex(int n_point, double* weights)
{
  double total = 0.;
  for(int i = 0; i < n_point; i++){
    total += weights[i];
  }

  double g = randGen::getRandU();

  double sum = 0.;
  int ind = 0;
  for(int i = 0; i < n_point; i++){
    sum += weights[i]/total;
    if(g <= sum){
      ind = i;
      break;
    }
  }
  return ind;
}


///////////////////////////////////////////////////////////////////////////
//SEY CALCULATION: elastic, rediffused, true-secondaries
///////////////////////////////////////////////////////////////////////////
double CntrlSecEmSurface::calc_del_e(double ene0, double cos_theta0){
  double del_e = 
    (P_1_e_infiniteE_ 
     + (P_1_e_hat_ - P_1_e_infiniteE_)/exp( pow((fabs(ene0-E_e_hat_)/W_),p_)/p_ ))
    *(1.0 + e1_*(1.0-pow(cos_theta0,e2_)));
  return del_e;
}

double CntrlSecEmSurface::calc_del_r(double ene0, double cos_theta0){
  double del_r = 
    P_1_r_infiniteE_*(1.0 - 1.0/exp( pow(ene0/E_r_,r_)))
    *(1.0 + r1_*(1.0-pow(cos_theta0,r2_)));
  return del_r;
}

double CntrlSecEmSurface::calc_del_ts(double ene0, double cos_theta0){
  double del_hat0 = delta_ts_hat_*(1.0 + t1_*(1.0-pow(cos_theta0,t2_))); 
  double E_hat0 = E_ts_hat_*(1.0 + t3_*(1.0-pow(cos_theta0,t4_)));
  double del_ts = del_hat0 * s_ * (ene0/E_hat0) / ( s_ - 1.0 + pow((ene0/E_hat0),s_) );
  return del_ts;
}


///////////////////////////////////////////////////////////////////////////
//ENERGY distribution calculation: elastic, rediffused, true-secondary
///////////////////////////////////////////////////////////////////////////

double CntrlSecEmSurface::randomEmissEnergy_e(double ene0)
{
  double u = randGen::getRandU();
  double prob_limit   = gaussInverseDistrFunc->getX(ene0/sigma_e_);
  double energy = ene0 - sigma_e_*gaussInverseDistrFunc->getY(prob_limit*u);
  return energy;
}

double CntrlSecEmSurface::randomEmissEnergy_r(double ene0)
{
  double u = randGen::getRandU();
  double energy = ene0* pow(u,1.0/(1.0+q_));
  return energy;
}

double CntrlSecEmSurface::randomEmissEnergy_ts(double dlt_ts, double ene0)
{
  double p_par = dlt_ts/((double) N_ELECTRONS_MAX);

  //calculate binomial distribution coeff for n=1...N_ELECTRONS_MAX
  for(int i = 0; i < N_ELECTRONS_MAX; i++){
    prob_array_tmp[i] = (i+1.0) * pow(p_par,i+1)*pow((1.0-p_par),N_ELECTRONS_MAX-(i+1))*
      binomial(N_ELECTRONS_MAX,i+1);
  }
  
  int ind = chooseIndex(N_ELECTRONS_MAX,prob_array_tmp);
  Function* distr_func = distrFunc_ts[ind];
  
  double u = randGen::getRandU();
  double prob_limit   = distr_func->getX(ene0/eps_n_[ind]);
  double energy = eps_n_[ind]*distr_func->getY(prob_limit*u);  
  return energy;
}

///////////////////////////////////////////////////////////////////////////
//ANGLES and RESULTING MOMENTUM CALCULATION
///////////////////////////////////////////////////////////////////////////
void CntrlSecEmSurface::calcEmissMomentum(double* r_v,double* n_v, 
					      double emissEnergy, 
					      double* pout_v){

  //emissEnergy in eV but our momentum in MeV/c
  double pout = sqrt(2.0*(mass*1.0e-6)*emissEnergy);

  //arrange orthogonal system
  double e_1[3],e_2[3], a[3],aa[3]; double costheta1, abs_aa;

  a[0]=1.0; a[1]=0.0; a[2]=0.0;
  if( fabs(n_v[1])+fabs(n_v[2]) < 1.0e-8 ){
    a[0]=0.0; a[1]=1.0; a[2]=0.0;
  }

  costheta1 = a[0]*n_v[0] + a[1]*n_v[1] + a[2]*n_v[2]; 
  aa[0] = a[0] - costheta1 * n_v[0];
  aa[1] = a[1] - costheta1 * n_v[1];
  aa[2] = a[2] - costheta1 * n_v[2];

  abs_aa = sqrt( aa[0]*aa[0] + aa[1]*aa[1] + aa[2]*aa[2] );
  e_1[0] = aa[0]/abs_aa;
  e_1[1] = aa[1]/abs_aa;
  e_1[2] = aa[2]/abs_aa;

  e_2[0] = n_v[1]*e_1[2] - n_v[2]*e_1[1]; 
  e_2[1] = n_v[2]*e_1[0] - n_v[0]*e_1[2]; 
  e_2[2] = n_v[0]*e_1[1] - n_v[1]*e_1[0]; 

  //get angles theta and phi
  double cos_theta =  pow(randGen::getRandU(), 1.0/(1.0+alpha_));
  double sin_theta = sqrt(fabs(1.0 - cos_theta*cos_theta));
  double phi = 2.0*randGen::getRandU()*epModuleConstants::PI;  
  double sin_phi = sin(phi);
  double cos_phi = cos(phi);

  //get pou_v[3]
  pout_v[0] = pout*( cos_theta*n_v[0] + sin_theta*( cos_phi*e_1[0] + sin_phi*e_2[0] ) );
  pout_v[1] = pout*( cos_theta*n_v[1] + sin_theta*( cos_phi*e_1[1] + sin_phi*e_2[1] ) );
  pout_v[2] = pout*( cos_theta*n_v[2] + sin_theta*( cos_phi*e_1[2] + sin_phi*e_2[2] ) );
}


///////////////////////////////////////////////////////////////////////////
//MAIN METHOD FOR CALCULATION OF THE NEW_BORN ELECTRONS
///////////////////////////////////////////////////////////////////////////

void CntrlSecEmSurface::impact(int index, eBunch* eb, double* r_v, double* n_v)
{

  double u;

  double macroSize = eb->macroSize(index);
  //energy in eV, eb->getEnergy(index) returns in MeV
  double incEnergy = 1.0e+6*eb->getEnergy(index);
  eb->deleteParticle(index);

  if(incEnergy == 0.) return;

  //------------------------------------------
  //should we produce new electrons?   START
  //------------------------------------------
  double p_death = p_death_default;
  if(p_death_func){
    p_death = p_death_func->getY(incEnergy);
  }
  u = randGen::getRandU();
  if(u < p_death) {
    return;
  }
  macroSize = macroSize/(1.0 - p_death);
  //------------------------------------------
  //should we produce new electrons?   STOP
  //------------------------------------------

  //-------------------------------------------------------
  //Definition how many electrons we should produce   START
  //-------------------------------------------------------
  int nNewBorn = 1;
  int local_max = macroPartsMaxN/size_MPI;
  double sigma_n = local_max/5.0;
  int local_size = eb->getSize();
  double x_par = (local_max - local_size)/sigma_n;

  if(x_par < -20.) {
    nNewBorn = 1; 
  }
  else{
    if(n_born_func){
      nNewBorn = (int) n_born_func->getY(incEnergy);
    }
    else{
      nNewBorn = n_new_born_default;
    }
     
    x_par = 1.0/(1.0 + exp(-x_par));
    u = randGen::getRandU();
    if(u > x_par){
      nNewBorn = 1;
    }
  }

  if(nNewBorn <= 0){
    finalize("Number of new born electrons is equal to or less than 0!");
  }
  
  //----
  //temporary treatment of eBunch to reduce macroSize range   060904
  int splitterOn = 1;
  if(splitterOn==1){
    //double ave = eb->getTotalMacroSize()/eb->getSize();
    double ave = eb->getAveMacroSizeFromMemory();
    if(ave!=0.){
      int multipleN = (int)(macroSize/ave);
      if( multipleN == 3 || multipleN == 2) nNewBorn = 2;
      if( multipleN >  4 ) nNewBorn = multipleN/2;
    }
  }
  //----

  macroSize = macroSize/((int) nNewBorn);
  //-------------------------------------------------------
  //Definition how many electrons we should produce   STOP
  //-------------------------------------------------------

  //calculation of the incident angle and 
  //SEY for three processes (el,rd,ts)
  double px= eb->px(index);
  double py= eb->py(index);
  double pz= eb->pz(index);
  double pp = sqrt(px*px+py*py+pz*pz);
  double cos_theta0 = -(px*n_v[0]+py*n_v[1]+pz*n_v[2])/pp;

  if(cos_theta0 < 0.){
    finalize("Incident electron moves outside the material!");
  }

  double del_el = calc_del_e(incEnergy,cos_theta0);
  double del_rd = calc_del_r(incEnergy,cos_theta0);
  double del_ts = calc_del_ts(incEnergy,cos_theta0);

  double prob_el = p_el*del_el;
  double prob_rd = p_rd*del_rd;
  double prob_ts = p_ts*del_ts;

  double sum = prob_el + prob_rd + prob_ts;
  prob_el /= sum;
  prob_rd /= sum;
  prob_ts /= sum;

  //-------------------------------------------------------
  //The loop to produce new electrons   START
  //-------------------------------------------------------
  double macroSizeLocal = 0.;
  int indProc = -1;
  double energy = 0.;
  double pout_v[3];
  for(int nE = 0; nE < nNewBorn; nE++){
    macroSizeLocal = macroSize;

    //redifine prob_array_tmp that could be spoiled 
    //by randomEmissEnergy_ts method
    prob_array_tmp[0] = prob_el;
    prob_array_tmp[1] = prob_rd;
    prob_array_tmp[2] = prob_ts;

    //find index of the process (0 - el, 1 - rd; 2 - ts)
    indProc = chooseIndex(3,prob_array_tmp);

    //define the macrosize and energy of the electron
    //after this point we do not need prob_array_tmp values
    //they can be used by randomEmissEnergy_ts method
    macroSizeLocal = macroSizeLocal/prob_array_tmp[indProc];

    //elastic scattering
    if(indProc == 0){
      macroSizeLocal = macroSizeLocal * del_el;
      energy = randomEmissEnergy_e(incEnergy);
    }

    //rediffused scattering
    if(indProc == 1){
      macroSizeLocal = macroSizeLocal * del_rd;
      energy = randomEmissEnergy_r(incEnergy);
    }

    //true scattering
    if(indProc == 2){
      macroSizeLocal = macroSizeLocal * del_ts;
      energy = randomEmissEnergy_ts(del_ts, incEnergy);
    }

    if(indProc != 0 && indProc != 1 && indProc != 2){
      finalize("Can not choose the process indProc != (0,1,2)!");
    }

    //define momentum and add electrons to bunch
    calcEmissMomentum(r_v,n_v,energy,pout_v); 
    eb->addParticle(macroSizeLocal,r_v[0],r_v[1],r_v[2],
		    pout_v[0],pout_v[1],pout_v[2]);
  }
  //-------------------------------------------------------
  //The loop to produce new electrons   STOP
  //-------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
