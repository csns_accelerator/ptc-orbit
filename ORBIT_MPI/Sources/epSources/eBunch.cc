//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    eBunch.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    05/01/2003
//
// DESCRIPTION
//    Source code for the class "eBunch" used to arrange macro particles
//    to be concerened in the class EPnode
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include "eBunch.hh"
#include "epModuleConstants.hh"

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   eBunch::eBunch
//   eBunch::~eBunch()
//
// DESCRIPTION
//   Constructor and Desctructor
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

eBunch::eBunch()
{
  //[MeV/c^2] = electron mass =9.1094d-31[kg]
  //charge of the electron
  //classical radius in mm

  mass            = epModuleConstants::mass_electron;
  charge          = epModuleConstants::charge_electron;
  classicalRadius = epModuleConstants::classicalRadius_electron;

  nDim = 9;

  nChunk = 5000;
  nTotalSize = nChunk;
  nNew = nSize = 0;
  sizeGlobal = 0;                                //---060804
  totalMacroSizeGlobal = 0.0;                    //---060804
  aveMacroSizeGlobal = 0.0;                      //---060804

  arrFlag = new int[nTotalSize];
  arrCoord = new double*[nTotalSize];
  for(int i=0; i < nTotalSize; i++){
    arrCoord[i] = new double[nDim];
  }

  //Bin and Fraction at Bin for a given grids
  arrBin = new int*[nTotalSize];
  for(int i=0; i < nTotalSize; i++){
    arrBin[i] = new int[3]; //x,y,z
  }
  arrFractBin = new double*[nTotalSize];
  for(int i=0; i < nTotalSize; i++){
    arrFractBin[i] = new double[3]; //x,y,z
  }

  //for MPI
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  } 
	
	nSizeArr     = (int *) malloc (sizeof(int) *size_MPI);
  nSizeArr_MPI = (int *) malloc (sizeof(int) *size_MPI);
	
	nSizeChank = 1000;
  dump_arr = (double *) malloc (sizeof(double)*nSizeChank*(nDim+1));	

  //we do not need compress in the beginning
  needOfCompress = 0;
}

eBunch::~eBunch()
{
  for(int i=0; i < nTotalSize; i++){
    delete [] arrCoord[i];
    delete [] arrBin[i];
    delete [] arrFractBin[i];
  }
  delete [] arrFlag;
  delete [] arrCoord;
  delete [] arrBin;
  delete [] arrFractBin;
	
	free(dump_arr); free(nSizeArr); free(nSizeArr_MPI);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   eBunch::macroSize, eBunch::x, eBunch::y, eBunch::z,
//   eBunch::px, eBunch::py, eBunch::pz, eBunch::pp, eBunch::pt, 
//   eBunch::flag
//
// DESCRIPTION
//   Each returns an element of the given index-th macroparticle
//   
//   macroSize:     returns MacroSize of each macroparticle
//   x: y: z:       returns coordinates of each macroparticle
//   px: py: pz:    returns momentum of non-drifting macroparticle
//   pp: pt:        returns momentum of drifting  macroparticle
//   flag:          returns the status of macroparticle; dead or alive
//                  flag(index)=0 means index-th macroparticle is dead
//                  flag(index)=1 means index-th macroparticle is alive
//
// RETURNS
//   arrCoord[index][number of corresponding element]
//
///////////////////////////////////////////////////////////////////////////

double& eBunch::macroSize(int index){  return arrCoord[index][0];}
double& eBunch::x(int index){          return arrCoord[index][1];}
double& eBunch::y(int index){          return arrCoord[index][2];}
double& eBunch::z(int index){          return arrCoord[index][3];}
double& eBunch::px(int index){         return arrCoord[index][4];}
double& eBunch::py(int index){         return arrCoord[index][5];}
double& eBunch::pz(int index){         return arrCoord[index][6];}
double& eBunch::pp(int index){         return arrCoord[index][7];}
double& eBunch::pt(int index){         return arrCoord[index][8];}
int & eBunch::flag(int index){             return arrFlag[index];}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   eBunch::xBin, eBunch::yBin, eBunch::zBin,
//   eBunch::xFractBin, eBunch::yFractBin, eBunch::zFractBin
//
// DESCRIPTION
//   bin_index or fraction of each coordicate to a given grids
//
// RETURNS
//   xBin:      returns bin_index of x to a given grids
//   xFractBin: returns x fraction at its bin_index to a given grids
//
///////////////////////////////////////////////////////////////////////////

int& eBunch::xBin(int index){return arrBin[index][0];}
int& eBunch::yBin(int index){return arrBin[index][1];}
int& eBunch::zBin(int index){return arrBin[index][2];}
double& eBunch::xFractBin(int index){return arrFractBin[index][0];}
double& eBunch::yFractBin(int index){return arrFractBin[index][1];}
double& eBunch::zFractBin(int index){return arrFractBin[index][2];}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   eBunch::resize
//
// DESCRIPTION
//   Expands the size of arrFlag,arrCoord,arrBin and arrFractBin, 
//   when the number of macroparticles reaches nTotalSize. It should be 
//   used in eBunch::addParticle. 
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

void eBunch::resize()
{
  int nOldTotalSize = nTotalSize;
  nTotalSize = nTotalSize + nChunk;

  tmp_arrFlag     = new int    [nTotalSize];
  tmp_arrCoord    = new double*[nTotalSize];
  tmp_arrBin      = new int*   [nTotalSize];
  tmp_arrFractBin = new double*[nTotalSize];
  for(int i = nOldTotalSize; i < nTotalSize; i++){
    tmp_arrCoord[i]    = new double[nDim];
    tmp_arrBin[i]      = new int   [3];
    tmp_arrFractBin[i] = new double[3];
  }

  for(int i=0; i < nOldTotalSize; i++){
    tmp_arrFlag[i]     = arrFlag[i];
    tmp_arrCoord[i]    = arrCoord[i];
    tmp_arrBin[i]      = arrBin[i]; 
    tmp_arrFractBin[i] = arrFractBin[i]; 
  }

  delete [] arrFlag;
  delete [] arrCoord;
  delete [] arrBin;
  delete [] arrFractBin;

  arrFlag     = tmp_arrFlag;
  arrCoord    = tmp_arrCoord;
  arrBin      = tmp_arrBin;
  arrFractBin = tmp_arrFractBin;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::addParticle
//
// DESCRIPTION
//    adds a macro_particle of non-drift motion under week magnetic field
//    It is needed to call eBunch::compress to activate this new 
//    macroparticle in calculation
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

void eBunch::addParticle(double macroSize,
			 double x,double y,double z,
			 double px,double py,double pz)
{
  int n;
  n = nNew;
  if (n >= nTotalSize){
    resize();
  }

  //sign of the charge are regulated by "charge" variable

  arrCoord[n][0] = fabs(macroSize);
  arrCoord[n][1] = x;
  arrCoord[n][2] = y;
  arrCoord[n][3] = z;
  arrCoord[n][4] = px;
  arrCoord[n][5] = py;
  arrCoord[n][6] = pz;
  arrCoord[n][7] = 0.0;
  arrCoord[n][8] = 0.0;
  arrFlag[n] = 1; //alive
  
  nNew = nNew + 1;

  //we need compress in the future
  needOfCompress = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::addParticle
//
// DESCRIPTION
//    adds a macro_particle of drift motion under strong magnetic field
//    It is needed to call eBunch::compress to activate this new 
//    macroparticle in calculation
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

void eBunch::addParticle(double macroSize,
			 double x,double y,double z,
			 double pp,double pt)
{
  int n;
  n = nNew;
  if (n >= nTotalSize){
    resize();
  }

  arrCoord[n][0] = fabs(macroSize);
  arrCoord[n][1] = x;
  arrCoord[n][2] = y;
  arrCoord[n][3] = z;
  arrCoord[n][4] = 0.0;
  arrCoord[n][5] = 0.0;
  arrCoord[n][6] = 0.0;
  arrCoord[n][7] = pp;
  arrCoord[n][8] = pt;
  arrFlag[n] = 1; //alive

  nNew = nNew + 1;

  //we need compress in the future
  needOfCompress = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::addParticle
//
// DESCRIPTION
//    adds a macro_particle of unclassified motion under unknown magnetic field
//    It is needed to call eBunch::compress to activate this new 
//    macroparticle in calculation
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

void eBunch::addParticle(double macroSize,
			 double x,double y,double z,
			 double px,double py,double pz, 
			 double pp, double pt)
{
  int n;
  n = nNew;
  if (n >= nTotalSize){
    resize();
  }

  arrCoord[n][0] = fabs(macroSize);
  arrCoord[n][1] = x;
  arrCoord[n][2] = y;
  arrCoord[n][3] = z;
  arrCoord[n][4] = px;
  arrCoord[n][5] = py;
  arrCoord[n][6] = pz;
  arrCoord[n][7] = pp;
  arrCoord[n][8] = pt;
  arrFlag[n] = 1; //alive
  
  nNew = nNew + 1;

  //we need compress in the future
  needOfCompress = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::deleteParticle
//
// DESCRIPTION
//    deletes a macro_particle with declaring its flag is dead
//    It is needed to call eBunch::compress to remove this dead 
//    macroparticle from calculation
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

void eBunch::deleteParticle(int index)
{
  arrFlag[index] = 0; //dead

  //we need compress in the future
  needOfCompress = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::compress
//
// DESCRIPTION
//    compresses the workspace to the number of "alive" macroparticles
//    with removing "dead" flagged macroparticles and renumbering "alive"
//    flagged macroparticles
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

void eBunch::compress()
{

  if(needOfCompress == 0) return;

  int lowInd = 0;
  int uppInd = lowInd;
  int count = 0;

  double* tmp;
  int tmp_flag = 0;

  int lowIndChanged = 0;

  while( uppInd < nNew){

    lowIndChanged = 0;

    while( arrFlag[lowInd] != 0 && lowInd < nNew){
      count++;
      lowInd++;
      lowIndChanged = 1;
    }
    if(lowInd == (nNew -1)) break;

    if(lowIndChanged > 0){
      uppInd = lowInd + 1;
    }
    else{
      uppInd++; 
    }

    while( arrFlag[uppInd] == 0 && uppInd < nNew){
      uppInd++;
    }

    if(uppInd < nNew){
      count++;
      tmp = arrCoord[lowInd];
      arrCoord[lowInd] = arrCoord[uppInd];
      arrCoord[uppInd] = tmp;
      tmp_flag = arrFlag[lowInd];
      arrFlag[lowInd] = arrFlag[uppInd];
      arrFlag[uppInd] = tmp_flag;
      lowInd++;
    }
  }
  nSize=count;
  nNew=count;

  //compression is done
  needOfCompress = 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::getMass
//
// RETURNS
//    mass of an electron
//
///////////////////////////////////////////////////////////////////////////

double eBunch::getMass(){   return mass;}
double eBunch::getCharge(){ return charge;}
double eBunch::getClassicalRadius(){ return classicalRadius;}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::getSize
//
// DESCRIPTION
//    returns the number of macroparticles
//    In order to reflect the results of the latest addParticle() or 
//    deleteParticle(), you have to use this right after compress()
//
// RETURNS
//    the number of alive macro_particles
//
///////////////////////////////////////////////////////////////////////////

int eBunch::getSize(){  return nSize;}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::getSizeGlobal
//
// DESCRIPTION
//    returns the number of macroparticles
//    In order to reflect the results of the latest addParticle() or 
//    deleteParticle(), you have to use this right after compress()
//
// RETURNS
//    the number of alive macro_particles for all CPUs
//
///////////////////////////////////////////////////////////////////////////

int eBunch::getSizeGlobal()
{  
  if(size_MPI == 1) {
    sizeGlobal = nSize;
    return sizeGlobal;
  }
  else{
    MPI_Allreduce(&nSize,&sizeGlobal,1, 
		  MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    return sizeGlobal;    
  }
}
//returns the number of alive macro_particles for all CPUs from memory
//after eBunch::getSizeGlobal() called      
int eBunch::getSizeGlobalFromMemory()
{
  return sizeGlobal;
}


//returns total number of macro particles, alive and dead
int eBunch::getTotalCount()
{
  return nNew;
}

double eBunch::getTotalMacroSize()
{
  double totalMacroSize = 0.;
  for(int i = 0; i < nSize; i++){
    totalMacroSize +=arrCoord[i][0];
  }
  return totalMacroSize;
}

//returns the number of total macrosize of macroparticles among all CPUs 
double eBunch::getTotalMacroSizeGlobal()
{
  double totalMacroSize = getTotalMacroSize();
  if(size_MPI == 1) {
    totalMacroSizeGlobal = totalMacroSize;
    return totalMacroSizeGlobal;
  }
  else{
    MPI_Allreduce(&totalMacroSize,&totalMacroSizeGlobal,1, 
		  MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    return  totalMacroSizeGlobal;    
  }   
}
//returns the number of total macrosize of macroparticles among all CPUs 
//after eBunch::getTotalMacroSizeGlobal() called       
double eBunch::getTotalMacroSizeGlobalFromMemory()
{
  return totalMacroSizeGlobal; 
}

//returns average of macrosize among all CPUs
//after calling this you can use 
//         eBunch::getSizeGlobalFromMemory() 
//         eBunch::getTotalMacroSizeGlobalFromMemory()
//         eBunch::getAveMacroSizeFromMemory()
double eBunch::getAveMacroSize(){
  getSizeGlobal();
  getTotalMacroSizeGlobal();

  if(sizeGlobal == 0){
    aveMacroSizeGlobal = 0.;
  }else{
    aveMacroSizeGlobal = totalMacroSizeGlobal/sizeGlobal;
  }
  return aveMacroSizeGlobal;
}
//returns average of macrosize among all CPUs
//after eBunch::getAveMacroSize() called
double eBunch::getAveMacroSizeFromMemory(){          
  return aveMacroSizeGlobal;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::print
//
// DESCRIPTION
//    print every components of whole macroparticles 
//    with requesting a file name to print
//    for whole CPU in MPI
//
///////////////////////////////////////////////////////////////////////////

void eBunch::print(std::ostream& Out)
{
  int N_flush = 10000;

  //single CPU case
  if(rank_MPI == 0){
    for(int i = 0; i < nSize; i++){
      Out <<flag(i)<<"  "<< macroSize(i)<<"  " 
	  << x(i) <<"  "<< y(i) <<"  "<< z(i) <<"  " 
	  << px(i)<<"  "<< py(i)<<"  "<< pz(i)<<"  " 
	  << pp(i)<<"  "<< pt(i)<<"\n";

    if (i % N_flush == 0){Out.flush();}
    }
    Out.flush();
    if(size_MPI == 1) return;
  }


  //parallel case                                   ===== MPI start =====
  MPI_Status statusMPI ; 
  for(int i=0; i < size_MPI; i++){
    nSizeArr[i]=0;
    if(i==rank_MPI){nSizeArr[i]=nSize;}
  }
  MPI_Allreduce(nSizeArr,nSizeArr_MPI,size_MPI,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

  //sending and receiving coordinates and properties of macroparticles
  for( int i = 1; i < size_MPI; i++){
		int nRepeat = nSizeArr_MPI[i]/nSizeChank;
		if(nSizeArr_MPI[i] % nSizeChank != 0) nRepeat += 1;
    MPI_Bcast(&nRepeat,1,MPI_INT,0,MPI_COMM_WORLD);		
		for(int ir = 0; ir < nRepeat; ir++){
			int indS = ir*nSizeChank;
			int indE = indS + nSizeChank;
			if(indE >= nSizeArr_MPI[i]) indE = nSizeArr_MPI[i];
			if( i == rank_MPI ){
				for( int j = indS; j < indE; j++){
					dump_arr[(nDim+1)*(j-indS) + 0] = (double) flag(j);
					dump_arr[(nDim+1)*(j-indS) + 1] = macroSize(j);
					dump_arr[(nDim+1)*(j-indS) + 2] = x(j);
					dump_arr[(nDim+1)*(j-indS) + 3] = y(j);
					dump_arr[(nDim+1)*(j-indS) + 4] = z(j);
					dump_arr[(nDim+1)*(j-indS) + 5] = px(j);
					dump_arr[(nDim+1)*(j-indS) + 6] = py(j);
					dump_arr[(nDim+1)*(j-indS) + 7] = pz(j);
					dump_arr[(nDim+1)*(j-indS) + 8] = pp(j);
					dump_arr[(nDim+1)*(j-indS) + 9] = pt(j);
				}
				MPI_Send(dump_arr, (nDim+1)*(indE-indS), MPI_DOUBLE, 0, 1111, MPI_COMM_WORLD);  
			}
			if(rank_MPI == 0){
				MPI_Recv(dump_arr, (nDim+1)*(indE-indS), MPI_DOUBLE, i, 1111, MPI_COMM_WORLD, &statusMPI); 
				for( int j = 0; j < (indE-indS); j++){
					Out << (int) dump_arr[(nDim+1)*j + 0] << "  "
					<< dump_arr[(nDim+1)*j + 1] << "  "
					<< dump_arr[(nDim+1)*j + 2] << "  "
					<< dump_arr[(nDim+1)*j + 3] << "  "
					<< dump_arr[(nDim+1)*j + 4] << "  "
					<< dump_arr[(nDim+1)*j + 5] << "  "
					<< dump_arr[(nDim+1)*j + 6] << "  "
					<< dump_arr[(nDim+1)*j + 7] << "  "
					<< dump_arr[(nDim+1)*j + 8] << "  "
					<< dump_arr[(nDim+1)*j + 9] << "\n";
				}
			}
		}
	}
	if(rank_MPI == 0) Out.flush();
  //                                                  ===== MPI end =====
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::print
//
// DESCRIPTION
//    print every components of macroparticles with requesting a file name 
//    to print for rank0 CPU
//
// REMAEKS
//    In order to reflect the results of the latest addParticle() or 
//    deleteParticle(), you have to use this right after compress()
//
///////////////////////////////////////////////////////////////////////////

void eBunch::print(char* fileName)
{
  if(size_MPI == 1){
    ofstream F_out;
    F_out.open (fileName, ios::out);
    print(F_out);
    F_out.close();    
  }
  else{
    ofstream F_dump;
    if(rank_MPI == 0){
      F_dump.open (fileName, ios::out);
    }
    print(F_dump);
    if(rank_MPI == 0){F_dump.close();}

    return;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::readParticles(char* fileName, int nPart)
//
// DESCRIPTION
//    reads every components of the given number of macroparticles
//    with assignning them to the concerned processes almost 
//    (for the remindar) impartially.
//
// REMARK 
//    needs eBunch::compress(); see eBunch::addParticle
//
// RETURNS
//    Nothing
//    
///////////////////////////////////////////////////////////////////////////

void eBunch::readParticles(char* fileName, int nParts)
{
  int flag;
  double macroSize, x,y,z, px,py,pz, pp,pt;
  ifstream is(fileName, ios::in);

  int i=0; 
  int i_start = 0;
  int i_stop = nParts-1;

  if (is.bad()){ 
    std::cout << "The eBunch::readParticles(char* fileName, int nParts) \n";
    std::cout << "rank_MPI = "<<rank_MPI<<"\n";
    std::cout << "Can not create file:"<< fileName <<"\n";
    std::cout << "Stop. \n";
    FinalizeExecution();
  }

  //                       ----- MPI start -----
  int n_part_rank = nParts/size_MPI;
  int modulo = nParts%size_MPI;

  i_start = rank_MPI * n_part_rank;
  if(rank_MPI < modulo){
    i_start = i_start + rank_MPI;
    n_part_rank++;
  } 
  else{
    i_start = i_start + modulo;
  }

  i_stop = i_start + n_part_rank -1;
  //                       ----- MPI end -----

  while(!is.eof() && (i < nParts)){ 
    if(is >> flag>> macroSize>> x>>y>>z>> px>>py>>pz>> pp>>pt)
      {
	if( i >= i_start && i <= i_stop ){
	  addParticle(macroSize, x,y,z, px,py,pz, pp,pt);
	}      
	i++;
      }
    else
      if(!is.eof()){ 
	std::cout << "The eBunch::readParticles(char* fileName,int nParts) \n";
	std::cout << "rank_MPI = "<<rank_MPI<<"\n";
	std::cout << "Can not read from file:"<< fileName <<"\n";
	std::cout << "The number of macro-particles to read="<< nParts <<"\n";
	std::cout << "Please check the file's content.\n";
	std::cout << "Stop. \n";
	FinalizeExecution();	  
      } 
  }     
  is.close();
  compress();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::readParticles(char* fileName)
//
// DESCRIPTION
//    reads every components of the given number of macroparticles
//    with assignning them to the concerned processes almost 
//    (for the remindar) impartially.
//
// RETURNS
//    Nothing
//    
///////////////////////////////////////////////////////////////////////////

void eBunch::readParticles(char* fileName)
{
  int flag;
  double macroSize, x,y,z, px,py,pz, pp,pt;
  ifstream is(fileName, ios::in);

  if (is.bad()){ 
    std::cout << "The eBunch::readParticles(char* fileName) \n";
    std::cout << "rank_MPI = "<<rank_MPI<<"\n";
    std::cout << "Can not open file:"<< fileName <<"\n";
    std::cout << "Stop. \n";
    FinalizeExecution();
  }

  int accum_read_parts = 0;
  if(rank_MPI == 0){
    while(!is.eof()){
      if(is >> flag>> macroSize>> x>>y>>z>> px>>py>>pz>> pp>>pt)
	{
	  accum_read_parts++;
	}
      else
	if(!is.eof()){ 
	  std::cout << "The eBunch::readParticles(char* fileName) \n";
	  std::cout << "rank_MPI = "<<rank_MPI<<"\n";
	  std::cout << "Can not read from file:"<< fileName <<"\n";
	  std::cout << "The number of macro-particles have been read ="
		    << accum_read_parts <<"\n";
	  std::cout << "Please check the file's content.\n";
	  std::cout << "Stop. \n"; 
	  FinalizeExecution();	  
	} 
    } 
    is.close();
  }

  if(iMPIini > 0){
    MPI_Bcast(&accum_read_parts,1,MPI_INT,0,MPI_COMM_WORLD);
  }

  readParticles(fileName, accum_read_parts);
}


double eBunch::getEnergy(int index)
{
  double p2 = arrCoord[index][4]*arrCoord[index][4]+
    arrCoord[index][5]*arrCoord[index][5]+
    arrCoord[index][6]*arrCoord[index][6];
  return p2/(2.0*mass);
}

double eBunch::getEnergyX(int index)
{
  return arrCoord[index][4]*arrCoord[index][4]/(2.0*mass);
}

double eBunch::getEnergyY(int index)
{
  return arrCoord[index][5]*arrCoord[index][5]/(2.0*mass);
}

double eBunch::getEnergyZ(int index)
{
  return arrCoord[index][6]*arrCoord[index][6]/(2.0*mass);
}

void eBunch::deleteAllParticles()
{
  for(int i = 0; i < nNew; i++){
    arrFlag[i] = 0; //dead
  }

  nSize = 0;
  nNew = 0;

  //compression is not needed
  needOfCompress = 0;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    eBunch::FinalizeExecution(char* fileName)
//
// DESCRIPTION
//            Finalize MPI if it has been initialized
//
// RETURNS
//    Nothing
//    
///////////////////////////////////////////////////////////////////////////

void eBunch::FinalizeExecution()
{
  if(iMPIini > 0){
    MPI_Finalize();
  }
  exit(1);
}
