//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    EP_NodeCalculator.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/13/2004
//
// DESCRIPTION
//    Specification and inline functions for a class EP_NodeCalculator
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#ifndef EP_NODE_CALCULATOR_H
#define EP_NODE_CALCULATOR_H

//ORBIT headers
#include "Injection.h"
#include "Ring.h"
#include "Particles.h"
#include "MacroPart.h"
#include "PartsDistributor_MPI.h"

#include "eBunch.hh"
#include "epModuleConstants.hh"
#include "baseSurface.hh"
#include "baseParticleTracker.hh"
#include "eCloudFieldSource.hh"
#include "pBunchFieldSource.hh"
#include "RhoGrid3D.hh"
#include "PhiGrid3D.hh"
#include "Function.hh"
#include "randGen.hh"

//---diagnostics------------------
#include "missingPartsDiagnostic.hh"
#include "CompositeEBunchDiag.hh"
#include "CompositeSurfaceDiag.hh"

class  EP_NodeCalculator
{
public:

  static EP_NodeCalculator* getNodeCalculator();
  static EP_NodeCalculator* getNodeCalculator(
			      int xBins_In, int yBins_In, int nZ_slices_In,
			      double xSize_In, double ySize_In,
			      int BPPoints, int BPShape, int BPModes,
			      int minProtonBunchSize_In);

  virtual ~EP_NodeCalculator();

  //MAIN method 
  //profileF_arr - profile functions array
  //i = 0 - p-bunch line density
  //i = 1 - e-bunch line density
  //i = 2 - x-centroid of the e-bunch
  //i = 3 - y-centroid of the e-bunch
  //i = 4 - size of the e-bunch
  //i = 5 - electron, hitting surface, current [A/m]
  //i = 6 - energy absorbed by surface [MeV/s/m]
  void propagate(double length, 
		 MacroPart &mp, 
		 eBunch* eb, 
		 baseSurface* surface,
		 Function** profileF_arr);

  //-------------------------------------------------------
  //convenience methods
  //-------------------------------------------------------
  baseParticleTracker* getTracker();

  void setEBunchDiagnostics(CompositeEBunchDiag* eBunchMainDiag_In);

  //-------------------------------------------------------
  //convenience methods with Shell interface
  //-------------------------------------------------------

  //sets surface and volume probability production
  void setElectronGenerationProbab(double eGenSurfProb_In, 
				   double eGenVolProb_In);

  //sets electron production params
  void setElectronGenerationParams(int nNewEletronsPerBunch_In, 
				   double newElectronsPerProton_In,
				   double pBunchLossRate_In);

  //sets the number of steps in longitudinal direction (default 1500)
  void setNumberLongSteps(int nLongSteps_In);

  //sets the number of pBunch field updates (default 1)
  void setNumberPBunchFieldUpdates(int nPBunchFiledUpdateSteps_In);

  //sets the number of steps of integration in the slice (default 20)
  void setNumberStepsInSlice(int nTrackerSteps_In);

  //sets using simplectic or non-simplectic tracking
  void setSimplecticTraking(int useSimplecticTracker_In);

  //define will it be there the debugging output (default 0)
  void setTrackerDebugOutput(int infDebugOutput_In);

  //defines should we apply kicks to p-bunch or not (default = 1 - yes)
  void setInfoPBunchKicksApply(int infoPBunchKicksApplying_In);

  //set the effective length coefficient 
  //it will affect on p-Bunch kick ( by default it is 1.0)
  void setEffLengthCoeff(double effectiveLengthCoeffIn);

  //returns maximal e-cloud linear density
  double getMaxECloudDensity();

  //add boundary potaential or not : addBoundaryPotentials = 1 yes 0 -no
  void setAddBoundaryPotentialsInfo(int addBoundaryPotentialsIn);

  //sets the transverse modulation function for p-bunch in y-direction
  //this is mostly for debugging
  void setPBunchTransFunctionX(Function* xTransFunc_In);

  //sets the transverse modulation function for p-bunch in y-direction
  //this is mostly for debugging
  void setPBunchTransFunctionY(Function* yTransFunc_In);

  //sets the density modulation function for p-bunch in phi-direction
  //this is mostly for debugging
  void setPBunchDensityFunction(Function* densityFunc_In);

private:

  EP_NodeCalculator(int xBins_In, int yBins_In, int nZ_slices_In,
                    double xSize, double ySize,
                    int BPPoints, int BPShape, int BPModes,
		    int minProtonBunchSize_In);

  static void finalizeExecution(char* message);


private:

  void makeProtonBunchArrays(NodeLoadingManager* LoadManager,
			MacroPartDistributor* PartDistr, 
			MacroPart &mp);

  void generateElectrons(int iStep,
			 double s, 
			 double sStep, 
			 Grid3D* rhoGrid, 
			 eBunch* eb,
			 MacroPartDistributor* PartDistr);


  void generateElectronsOnSurface(int iStep,
				  double s, 
				  double sStep, 
				  Grid3D* rhoGrid, 
				  eBunch* eb,
				  MacroPartDistributor* PartDistr);

  void generateElectronsInVolume(int iStep,
				 double s, 
				 double sStep, 
				 Grid3D* rhoGrid, 
				 eBunch* eb,
				 MacroPartDistributor* PartDistr);

  void setPBunchFiledSource(double s, 
			    pBunchFieldSource* pbF,
			    MacroPartDistributor* PartDistr);


  void accountPBunchKick(double s,
			 double sStep, 
			 MacroPartDistributor* PartDistr,
			 MacroPart &mp);


  void applyPBunchKick(MacroPart &mp);


private:

  //singleton reference
  static EP_NodeCalculator* instance;

  //longitudinal integration parameters
  //the parameters nLongSteps defines others values
  //so be careful when you change it 
  int nLongSteps;
  int nPBunchFiledUpdateSteps;

  //boundary and longitudinal sizes
  EP_Boundary* boundary;
  int xBins;
  int yBins;
  int nZ_slices;
  double xSize;
  double ySize;
  double z_min_ep;
  double z_max_ep;

  //proton beam and ring parameters all distances in [mm]
  double beta;
  double gamma;
  double ring_length;
  double frequency;
  double cTimeTotal;
  double harmonicNumber;
  double pBunchMacroSize;
  double pBunchTotalMacroSize;
  int minProtonBunchSize;
  int pBunchTotalSlices;
  int pBunchLocalSlices;
  double phiMin;
  double phiMax;
  double phiStep;
  double zMinGlobal;
  double zMaxGlobal;
  double coeffPhiToZ;
  
  //pBunch rho longitudinal profile (and commulative one)
  //pBunchProfileF - linear density in [nC]/m
  int nPointsPBunchProfile;
  double* pBunchProfile;
  double* pBunchProfile_MPI;
  Function* pBunchProfileF;
  Function* pBunchCommProfileF;

  //proton bunch rho and phi arrays
  RhoGrid3D* pBunchRhoGrid;
  PhiGrid3D* pBunchPhiGrid;
  
  //proton bunch x,y,z kick's grids
  Grid3D* pBunchKickX;
  Grid3D* pBunchKickY;
  Grid3D* pBunchKickZ;

  //electron production general parameters
  //probability surface and volume production
  double eGenSurfProb;
  double eGenVolProb;

  //surface electron production parameters
  int nNewEletronsPerBunch;
  double newElectronsPerProton;
  double pBunchLossRate;
  //array with the number of new born electrons for each long. step 
  int* nNewGenElectronsArr;
  int* nNewGen_MPI;  

  //volume electron production parameters
  Grid3D* eVolumeProdGrid;

  //Tracker 
  baseParticleTracker* tracker;
  int useSimplecticTracker;
  int nTrackerSteps;
  pBunchFieldSource* pbFields;
  eCloudFieldSource* ebFields;

  //use boundary conditions for fields or not ( 1-yes 0-no)
  //default = 1 - yes
  int addBoundaryPotentials;

  //set the effective length coefficient 
  //it will affect on p-Bunch kick ( by default it is 1.0)
  double effectiveLengthCoeff;
  

  //main e-bunch diagnostics
  CompositeEBunchDiag* eBunchMainDiag;

  //maximal e-cloud linear density
  double maxEDensity;

  //info - enable or suppress applying kicks to the p-bunch
  int infoPBunchKicksApplying;

  //debug output
  int infDebugOutput;

  //transverse modulation function for p-bunch in x-direction
  Function* pBunchTransModulationXF;

  //transverse modulation function for p-bunch in y-direction
  Function* pBunchTransModulationYF;

  //density modulation function for p-bunch in phi-direction
  Function* pBunchDensityModulationF;

  //MPI variables
  static int iMPIini;
  static int size_MPI;
  static int rank_MPI;
};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
