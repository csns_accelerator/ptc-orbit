//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    CompositeEBunchDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/22/2003
//
// DESCRIPTION
//   It is a composite class for abstractEBunchDiag class. It registers 
//   the e-bunch diagnostics instances and delegates the analysis to them.
//   The methods print(...) were implemented just for case. In general, 
//   each diagnostics should manages its own print method. 
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef COMPOSITE_EBUNCH_DIAGNISTICS_HH
#define COMPOSITE_EBUNCH_DIAGNISTICS_HH

#include <iostream> 
#include <fstream>

#include "AbstractEBunchDiag.hh"

class CompositeEBunchDiag : public AbstractEBunchDiag
{
public:

  CompositeEBunchDiag();
  virtual ~CompositeEBunchDiag();

  void init();
  void setZeroTurn();
  void startNewTurn();
  void analyze(double ctime, eBunch* eb);
  void analyze(int nTurn, double ctime, eBunch* eb);
 
  void print(std::ostream& Out);
  void print(char* fileName);

  //composite pattern method
  void addDiagnostics(AbstractEBunchDiag* diag);

  //convenience methods
  int getNumberOfDiagnostics();
  AbstractEBunchDiag* getDiagnostics(int index);

private:

  int nComps;
  int nCompsMax;

  int nTurns;

  AbstractEBunchDiag** components;

};

#endif

