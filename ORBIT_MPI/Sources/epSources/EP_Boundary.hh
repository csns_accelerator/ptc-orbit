// This code is for EPnode.             
//method impactPoint_straightMotion()  - Y.Sato 04/04/2003

//# It is modified from the code:
//     # Library            : ORBIT_MPI
//     # File               : Boundary.h
//     # Original code      : Jeff Holmes, Slava Danilov, John Galambos 

#ifndef EP_BOUNDARY_H
#define EP_BOUNDARY_H

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <mpi.h>
#include "rfftw.h"
#include "fftw.h"
#include "eBunch.hh"
#include "epModuleConstants.hh"

using namespace std;

/** The Boundary class is used to define boundary condition.*/
    
class EP_Boundary
{
public:

  /// Constructor
  EP_Boundary(int xBins   , int yBins   , 
		 double xSize, double ySize,
		 int BPPoints, int BPShape, 
		 int BPModes );

  EP_Boundary(int xBins   , int yBins   , 
		 double xSize, double ySize,
		 int BPPoints, int BPShape, 
		 int BPModes, double eps);

  /// Destructor
  virtual ~EP_Boundary();

  ///----------------------------------------------------------------
  ///---added methods for EPnode ---by Y.Sato ---------------start---

  //  calculate impact position and its normal vector on surface
  //  derived from an outgoing (outside) particle in straight motion
  //
  // In case of no impact for particle that is inside particle, 
  // it returns the TO_BE_KILLED

  int impactPoint_straightMotion(int index, eBunch* eb,
				 double* r_v,double* n_v);

  //  calculate impact position and its normal vector on surface
  //  derived from an outgoing (outside) particle in straight motion
  //
  // In case of no impact for particle that is inside particle, 
  // it returns the TO_BE_KILLED

  int impactPoint_driftMotion(int index, eBunch* eb,
			      double* r_v,double* n_v);

  //Get xBins   i.e. grid size in x-direction
  int getnXBins(); 

  //Get yBins   i.e. grid size in y-direction
  int getnYBins();

  // get the index and the fraction of the grid's point 
  // for particular x or y
  // The index should be a central point in three point interpolation
  // schema so:  1 <= ind <= (nBins-2)
  // The fraction should be: 0 <= frac <= 1.0
  void getIndAndFracX(double x, int& ind, double& frac);
  void getIndAndFracY(double y, int& ind, double& frac);

  //returns xGridMax_, and so on  
  double getxGridMax();
  double getxGridMin();
  double getyGridMax();
  double getyGridMin();

  //returns steps on X and Y axes
  double getStepX();
  double getStepY();

  ///---added methods for EPnode ---by Y.Sato -----------------end---
  ///----------------------------------------------------------------

  /// Adds potential from the boundary to the grid
  void addBoundaryPotential(double** phisc, 
                           int iXmin, int iXmax, 
                           int iYmin, int iYmax);

  ///Finds the potential without boundary
  void findPotential(double** rhosc, double** phisc); 
 
  /// Returns the pointers to the FFT array of the Green Function
  FFTW_COMPLEX* getOutFFTGreenF();

  ///Defines is a particle into the boundary or not ( returns < 0 - it is not)
  int isInside(double x, double y);

  ///Define external parameters from inner 
  void defineExtXYgrid(double &xGridMin, double &xGridMax, 
                       double &yGridMin, double &yGridMax,
                       double &dx      , double &dy );

  ///Define external grid's index limits from inner 
  void defineExtXYlimits(int &ixMinB, int &ixMaxB, 
                         int &iyMinB, int &iyMaxB);

  ///Get X-grid
  double* getXgrid();

  ///Get Y-grid
  double* getYgrid();

  ///returns the index of the boundary shape
  int getBoundaryShape();

  ///Get the bounding curve limit for X-coordinate
  double getBoundaryXsize();

  ///Get the bounding curve limit for Y-coordinate
  double getBoundaryYsize();

  ///Get the number of boundary points
  int getNumbBPoints();

  ///Get the X and Y coordinates of the boundary points
  double getBoundPointX(int i);
  double getBoundPointY(int i);

  ///debug method
  void checkBoundary();

  ///public static members
public:
  const static int IS_INSIDE;
  const static int IS_OUTSIDE;
  const static int TO_BE_KILLED;

protected:

  ///INITIALIZATION
  void init(int xBins, int yBins, 
       double xSize, double ySize,
       int BPPoints, int BPShape, 
       int BPModes);

  /// Finalize MPI
  void  _finalize_MPI();

  /// Calculates inverse matrix
  void  _gaussjinv(double **a, int n);

  /// Calculates all of the LSQM functions at one point
  double* lsq_fuctions(double x, double y);

  /// Calculates additional potential phi at one point
  double calculatePhi(double x, double y);

  /// Defines the FFT of the Green Function
  void _defineGreenF();

  /// Defines LSQM matrix
  void _defineLSQMatrix();

  /// Defines LSQM coefficients
  void _defineLSQcoeff();

  ///Sets array with the interpolation coefficients for boundary points
  void _setInterpolationCoeff();


protected:

  //Grid size
  int xBins_;
  int yBins_;

  //Twice extended grid size to use convolution method
  int xBins2_;
  int yBins2_; 

  //Green function 
  double** greensF_;
  
  //FFT arrays
  FFTW_REAL* in_;
  FFTW_REAL* in_res_;
  FFTW_COMPLEX* out_green_;
  FFTW_COMPLEX* out_;
  FFTW_COMPLEX* out_res_;

  rfftwnd_plan planForward_;
  rfftwnd_plan planBackward_;

  //boundary points
  //BPPoints_ - number of boundary points
  //shape of the boundary  BPShape_ = 1 - Circle 2 - Ellipse 3 - Rectangle
  int BPPoints_;
  int BPShape_;

  //Size of the bounding curve from 0 to max [m] or [cm] or [mm] 
  //The center is the point with X,Y coordinates (xSize_/2.,ySize_/2.)
  double xSize_;
  double ySize_; 

  //R_cirle_                - radius of the circle, 
  //BPa_,BPb_               - ellipse parameters, 
  //BPx_length_,BPy_width_  - rectangle parameters
  double R_cirle_,BPa_,BPb_,BPx_length_,BPy_width_;
  double BPrnorm_;
  
  double* BPx_;
  double* BPy_;
  int* iBPx_;
  int* iBPy_;  
  double* theta_;
  double* BPphi_; //external potential at the boundary points   
  
  //Grid array and parameters
  double xGridMin_, xGridMax_, yGridMin_, yGridMax_;
  double  dx_  ,  dy_;

  //Min and max indexes of XY-plane's grid to operate with potential
  int ixMinB_,ixMaxB_,iyMinB_,iyMaxB_;
  
  double* xGrid_;
  double* yGrid_; 

  //LSQM array and matrix 
  //2*BPModes_+1 - number of functions in LSQM 
  int BPModes_; 
  double* func_vector_;
  double* cof_vector_;
  
  double** LSQ_matrix_;
  double** tmp_matrix_;

  //number of boundary points
  int nBpoints_;

  //array with the interpolation coefficients for boundary points
  //(former Wxm, Wx0, Wxp, Wym, Wy0, Wyp for 9-points scheme)
  double** W_coeff_;

  //PI = 3.141592653589793238462643383279502
  const static double PI;

};

#endif
