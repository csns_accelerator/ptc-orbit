//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    AbstractEBunchDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/22/2003
//
// DESCRIPTION
//   the abstract class for eBunch diagnostics. It is supposed to deal with
//   e-bunch characteristics, the heat production from e-bunch and energy
//   spectrum of the electrons hitting the wall should be handled by surface
//   diagnostics class. This is the abstract-component class in a composite
//   pattern for e-bunch diagnostic structure.
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "AbstractEBunchDiag.hh"

const int AbstractEBunchDiag::NAME_LENGTH_MAX = 256;

AbstractEBunchDiag::AbstractEBunchDiag()
{
  diagName = (char *) malloc (sizeof(char) * (NAME_LENGTH_MAX));
  strncpy(diagName,"NO NAME E-BUNCH DIAGNOSTICS",NAME_LENGTH_MAX);

  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  } 
}

AbstractEBunchDiag::AbstractEBunchDiag(char* diagNameIn)
{
  diagName = (char *) malloc (sizeof(char) * (NAME_LENGTH_MAX));
  strncpy(diagName,diagNameIn,NAME_LENGTH_MAX);

  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  } 
}

AbstractEBunchDiag::~AbstractEBunchDiag()
{
  free(diagName);
}

void AbstractEBunchDiag::setName(char* newName)
{
  strncpy(diagName,newName,NAME_LENGTH_MAX);
}

const char* AbstractEBunchDiag::getName()
{
  return diagName;
}


///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
