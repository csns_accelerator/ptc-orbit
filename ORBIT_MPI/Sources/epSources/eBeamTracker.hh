//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//   eBeamTracker.hh
//
// AUTHOR
//   S. Cousineau 
//
// CREATED
//   12/29/2004
//
// DESCRIPTION
//   the base class to move electron in the electromagnetic field
//   This class needs one or more BaseFieldSource or 
//   BaseFieldSource.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef E_BEAM_TRACKER_HH
#define E_BEAM_TRACKER_HH

#include "eBunch.hh"
#include "baseSurface.hh"
#include "EP_Boundary.hh"
#include "Grid3D.hh"
#include "BaseFieldSource.hh"
#include "missingPartsDiagnostic.hh"
#include "CompositeSurfaceDiag.hh"

class eBeamTracker
{
public:

  eBeamTracker();
  virtual ~eBeamTracker();

  void setMissingPartsDiagnostics(missingPartsDiagnostic* missingDiagIn);
  missingPartsDiagnostic* getMissingPartsDiagnostics();


  void setNumberOfSteps(int nSteps);

  void addMagneticFieldSource(BaseFieldSource* fs);

  void addElectricFieldSource(BaseFieldSource* fs);

  void removeMagneticFieldSource(BaseFieldSource* fs);

  void removeElectricFieldSource(BaseFieldSource* fs);

  void moveParticlesStrongH(double time,eBunch* eb);

  void moveParticlesWeakH(double& time, int& nstep, eBunch*& eb, double& T,
			  double& xfinal, double& yfinal, double& zfinal, 
			  double& pxfinal, double& pyfinal, 
			  double& pzfinal);
 
private:

  //returns the H in Gauss/(electron charge in CGS)
  void getMagneticField(double x,    double y,    double z, int inside,
                        double& h_x, double& h_y, double& h_z);

  //returns the electric field (for point like charge 1/(r*r))
  void getElectricField(double x,    double y,    double z, int inside, 
                        double& e_x, double& e_y, double& e_z);

private:

  int i_s;

  BaseFieldSource** electricSources;
  int nElSources;
  BaseFieldSource** magneticSources;
  int nMgSources;

  double e_x_sum, e_y_sum, e_z_sum;
  double h_x_sum, h_y_sum, h_z_sum;

  double e_x_tmp, e_y_tmp, e_z_tmp;
  double h_x_tmp, h_y_tmp, h_z_tmp;

  //missing particles diagnostics
  missingPartsDiagnostic* missingDiag;

  //surface diagnostics composite
  CompositeSurfaceDiag* surfaceDiag;

  //number of steps for integration of the motion laws
  int nSteps_;

};
#endif

