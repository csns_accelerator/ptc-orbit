//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    AbstractSurfaceDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/22/2003
//
// DESCRIPTION
//   the abstract class for surface diagnostics. It is supposed to deal with
//   characteristics of particles hitting the surface and the heat production 
//   from them. This is the abstract-component class in a composite
//   pattern for surface diagnostic structure.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef ABSTRACT_SURFACE_DIAGNISTICS_HH
#define ABSTRACT_SURFACE_DIAGNISTICS_HH

#include <iostream> 
#include <fstream>
#include <cstring>

#include "eBunch.hh"

class AbstractSurfaceDiag
{
public:

  AbstractSurfaceDiag();
  AbstractSurfaceDiag(char* diagNameIn);
  virtual ~AbstractSurfaceDiag();

  void setName(char* newName);
  const char* getName();

  virtual void init(){};
  virtual void setZeroTurn(){};
  virtual void startNewTurn(){};

  virtual void accountTime(double ctimeStep){};

  //n_v - normal vector to the surface
  virtual void analyze(int indHit, int nOut, eBunch* eb, double* n_v){};
 
  virtual void print(std::ostream& Out){};
  virtual void print(char* fileName){};
  virtual void print(){};

protected:

  char* diagName;
  const static int NAME_LENGTH_MAX;


  //MPI stuff
  int iMPIini; 
  int rank_MPI; 
  int size_MPI;

};

#endif

