//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    Grid3D.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    05/19/2003
//
// DESCRIPTION
//    Source code for the class "Grid3D" used to 
//    to arrange 3D array for 3D grid, 
//    to operate the arrays with MPI and 
//    to describe the motion of macro-particles in the grid 
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include "Grid3D.hh"

int Grid3D::nBuffSize_Inner = 0;
double* Grid3D::buffMPI_out = NULL;
double* Grid3D::buffMPI_in  = NULL;

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Grid3D::Grid3D     
//    Grid3D::~Grid3D     
//
// DESCRIPTION
//   Constructor and Desctructor
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Grid3D::Grid3D(int nZ, EP_Boundary* boundaryIn)
{
  boundary = boundaryIn;

  nZ_ = nZ;
  nX_ = boundary->getnXBins();
  nY_ = boundary->getnYBins();

  dx_ = boundary->getStepX();
  dy_ = boundary->getStepY();

  zGridMin_=0.0; zGridMax_=0.0;

  dz_=0.0;

  //Allocate memory for the 3D distribution
  Arr3D = new double**[nZ_];
  for(int iz=0 ; iz < nZ_; iz++){
    Arr3D[iz] = new double*[nX_];
    for(int ix=0 ; ix < nX_; ix++){
      Arr3D[iz][ix] = new double[nY_];
      for(int iy=0 ; iy < nY_; iy++){
	Arr3D[iz][ix][iy] = 0.0;
      }
    }
  }

  //MPI stuffs
  rank_MPI = 0;
  size_MPI = 1;
  iMPIini  = 0;  
  MPI_Initialized(&iMPIini);

  if(iMPIini > 0 ){
    MPI_Comm_size(MPI_COMM_WORLD, &size_MPI);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_MPI);
  }

  if(size_MPI  > 1 ){

    //creation MPI buffers
    if(nZ_*nX_*nY_ > nBuffSize_Inner){
      nBuffSize_Inner = nZ_*nX_*nY_;

      if(buffMPI_out != NULL){
	free(buffMPI_out);
      }
      if(buffMPI_in != NULL){
	free(buffMPI_in);
      }

      buffMPI_out = (double *) malloc (sizeof(double) * nBuffSize_Inner);
      buffMPI_in  = (double *) malloc (sizeof(double) * nBuffSize_Inner);
    }
  }
}

Grid3D::~Grid3D()
{
  for(int iz=0 ; iz < nZ_; iz++){
    for(int ix=0 ; ix < nX_; ix++){
      delete [] Arr3D[iz][ix];
    }   
    delete [] Arr3D[iz];
  }  
  delete [] Arr3D;
  ////////////////////////////////////////////////////////
  // this part of code results suspend of execution
  // for a parallel case. We should no have a lot of
  // memory leak, because buffers are static.
  ////////////////////////////////////////////////////////
  //if( size_MPI > 1 ){
  //  if(buffMPI_out != NULL){
  //    free(buffMPI_out);
  //  }
  //  if(buffMPI_in != NULL){
  //    free(buffMPI_in);
  //  }
  //  nBuffSize_Inner = 0;
  //}
  ////////////////////////////////////////////////////////

}

///////////////////////////////////////////////////////////////////////////
//returns Arr3D or Slice2D=Arr3D[zInd]
///////////////////////////////////////////////////////////////////////////
double*** Grid3D::getArr3D(){return Arr3D;}
double** Grid3D::getSlice2D(int zInd){return Arr3D[zInd];}

////////////////////////////////////////////////////////////////////////////
//set the range of 3D Grid 
//(;to be defined in main code after setting boundary
//                         and before getGridIndAndFrac)
////////////////////////////////////////////////////////////////////////////
void Grid3D::setMinMaxZ(double zGridMin,double zGridMax)
{
  zGridMin_ = zGridMin;
  zGridMax_ = zGridMax;

  if( nZ_ > 1){
    dz_ = (zGridMax_-zGridMin_)/( nZ_ -1);
  }
  else{
   dz_ = zGridMax_-zGridMin_;
  }

}
///////////////////////////////////////////////////////////////////////////
//convenience functions
///////////////////////////////////////////////////////////////////////////

int Grid3D::getBinsX(){ return nX_;}
int Grid3D::getBinsY(){ return nY_;}
int Grid3D::getBinsZ(){ return nZ_;}

double Grid3D::getStepX(){return boundary->getStepX();}
double Grid3D::getStepY(){return boundary->getStepX();}
double Grid3D::getStepZ(){return dz_;}

double Grid3D::getxGridMax(){ return boundary->getxGridMax();}
double Grid3D::getxGridMin(){ return boundary->getxGridMin();}
double Grid3D::getyGridMax(){ return boundary->getyGridMax();}
double Grid3D::getyGridMin(){ return boundary->getyGridMin();}
double Grid3D::getzGridMax(){ return zGridMax_;}
double Grid3D::getzGridMin(){ return zGridMin_;}

double Grid3D::getGridValueX(int ind){return boundary->getXgrid()[ind];}
double Grid3D::getGridValueY(int ind){return boundary->getYgrid()[ind];}
double Grid3D::getGridValueZ(int ind){return (zGridMin_ + dz_*ind);}

///////////////////////////////////////////////////////////////////////////
//clean the array - set all elements to zero
///////////////////////////////////////////////////////////////////////////
void Grid3D::clean()
{
  for(int k = 0, k_max = getBinsZ(); k < k_max; k++){
    for(int i = 0, i_max = getBinsX(); i < i_max; i++){
      for(int j = 0, j_max = getBinsY(); j < j_max; j++){
	Arr3D[k][i][j] = 0.;
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//sum a new grid to this one
///////////////////////////////////////////////////////////////////////////
void Grid3D::sum(Grid3D* grd)
{
  if(getBinsX() != grd->getBinsX() ||
     getBinsY() != grd->getBinsY() ||
     getBinsZ() != grd->getBinsZ()){
       if(rank_MPI == 0){
	 std::cout << "Grid3D::sum(Grid3D* grd) \n";
	 std::cout << "rank_MPI = "<<rank_MPI<<"\n";
	 std::cout << "getBinsX() = "<< getBinsX() 
	           << " grd->getBinsX()="<<grd->getBinsX()<<"\n";
	 std::cout << "getBinsY() = "<< getBinsY() 
		   << " grd->getBinsY()="<<grd->getBinsY()<<"\n";
	 std::cout << "getBinsZ() = "<< getBinsZ() 
		   << " grd->getBinsZ()="<<grd->getBinsZ()<<"\n";
	 std::cout << "Check the size of the arrays.\n";
	 std::cout << "Stop. \n";
       }
       FinalizeExecution();
  }

  double*** tmpArr = grd->getArr3D();

  for(int k = 0, k_max = getBinsZ(); k < k_max; k++){
    for(int i = 0, i_max = getBinsX(); i < i_max; i++){
      for(int j = 0, j_max = getBinsY(); j < j_max; j++){
	Arr3D[k][i][j] += tmpArr[k][i][j];
      }
    }
  }
  
}

///////////////////////////////////////////////////////////////////////////
//sum two grids
///////////////////////////////////////////////////////////////////////////
void Grid3D::sum(Grid3D* grd1, Grid3D* grd2)
{
  if(getBinsX() != grd1->getBinsX() || getBinsX() != grd2->getBinsX()|| 
     getBinsY() != grd1->getBinsY() || getBinsY() != grd2->getBinsY()|| 
     getBinsZ() != grd1->getBinsZ() || getBinsZ() != grd2->getBinsZ()){
       if(rank_MPI == 0){
	 std::cout << "Grid3D::sum(Grid3D* grd1, Grid3D* grd2) \n";
	 std::cout << "rank_MPI = "<<rank_MPI<<"\n";
	 std::cout << "getBinsX() = "<< getBinsX() 
		   <<" grd1->getBinsX()="<<grd1->getBinsX()
		   <<" grd2->getBinsX()="<<grd2->getBinsX()<<"\n";
	 std::cout << "getBinsY() = "<< getBinsY() 
		   <<" grd1->getBinsY()="<<grd1->getBinsY()
		   <<" grd2->getBinsY()="<<grd2->getBinsY()<<"\n";
	 std::cout << "getBinsZ() = "<< getBinsZ() 
		   <<" grd1->getBinsZ()="<<grd1->getBinsZ()
		   <<" grd2->getBinsZ()="<<grd2->getBinsZ()<<"\n";
	 std::cout << "Check the size of the arrays.\n";
	 std::cout << "Stop. \n";
       }
       FinalizeExecution();
  }

  double*** tmpArr1 = grd1->getArr3D();
  double*** tmpArr2 = grd2->getArr3D();
  for(int k = 0, k_max = getBinsZ(); k < k_max; k++){
    for(int i = 0, i_max = getBinsX(); i < i_max; i++){
      for(int j = 0, j_max = getBinsY(); j < j_max; j++){
	Arr3D[k][i][j] = tmpArr1[k][i][j]+tmpArr2[k][i][j];
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//multiply all elements of Grid3D by constant coefficient
///////////////////////////////////////////////////////////////////////////
void Grid3D::multiply(double coeff)
{
  for(int k = 0, k_max = getBinsZ(); k < k_max; k++){
    for(int i = 0, i_max = getBinsX(); i < i_max; i++){
      for(int j = 0, j_max = getBinsY(); j < j_max; j++){
	Arr3D[k][i][j] = coeff*Arr3D[k][i][j];
      }
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
//multiply all elements of the z-slice with index ind by constant coefficient
//////////////////////////////////////////////////////////////////////////////
void Grid3D::multiply(int ind_z, double coeff)
{
  if(ind_z < 0 || ind_z > (getBinsZ()-1)){
    if(rank_MPI == 0){
      std::cout << "Grid3D::multiply(int ind_z, double coeff) \n";
      std::cout << "rank_MPI   = "<<rank_MPI<<"\n";
      std::cout << "getBinsZ() = "<< getBinsZ() <<"\n";
      std::cout << "ind_z      = "<< ind_z <<"\n";
      std::cout << "Check the size of the arrays or ind_z.\n";
      std::cout << "Stop. \n";
    }
    FinalizeExecution();
  }   
  for(int i = 0, i_max = getBinsX(); i < i_max; i++){
    for(int j = 0, j_max = getBinsY(); j < j_max; j++){
      Arr3D[ind_z][i][j] = coeff*Arr3D[ind_z][i][j];
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//set values from current grid to the external - grd
//z_shift - shift between the zero-z of current grid and external's one
///////////////////////////////////////////////////////////////////////////
void Grid3D::setValuesForExternalGrid(double z_shift, Grid3D* grd)
{
  int nExtZ = grd->getBinsZ();
  double z_center = (grd->getzGridMax()+grd->getzGridMin())/2.0;
  double z;
  if(grd->getBinsZ() > 2){
    for(int i = 0; i < nExtZ; i++){
      z = z_shift + (grd->getGridValueZ(i) - z_center);
      setValuesForSlice(z,i,grd);
    }
  }
  else{
    if(grd->getBinsZ() == 1){
      z = z_shift;
      setValuesForSlice(z,0,grd);
    }
    else{
      z = z_shift + (grd->getGridValueZ(0) - z_center);
      setValuesForSlice(z,0,grd);
      z = z_shift + (grd->getGridValueZ(1) - z_center);
      setValuesForSlice(z,1,grd);
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//set values at the grid points of the ind_z-th slice of the the Grid3D grd
///////////////////////////////////////////////////////////////////////////
void Grid3D::setValuesForSlice(double z, int ind_z, Grid3D* grd)
{
  if(ind_z < 0 || ind_z > (grd->getBinsZ()-1) ||
     grd->getBinsX() != getBinsX() || 
     grd->getBinsY() != getBinsY()){
    if(rank_MPI == 0){
      std::cout << "Grid3D::setValuesForSlice(double z, int ind_z, Grid3D* grd) \n";
      std::cout << "rank_MPI        = "<<rank_MPI<<"\n";
      std::cout << "getBinsX()      = "<< getBinsX() <<"\n";
      std::cout << "getBinsY()      = "<< getBinsY() <<"\n";
      std::cout << "grd->getBinsX() = "<< grd->getBinsX() <<"\n";
      std::cout << "grd->getBinsY() = "<< grd->getBinsY() <<"\n";
      std::cout << "grd->getBinsZ() = "<< grd->getBinsZ() <<"\n";
      std::cout << "ind_z           = "<< ind_z <<"\n";
      std::cout << "Check the size of the arrays or ind_z.\n";
      std::cout << "Stop. \n";
    }
    FinalizeExecution();
  }   
  double** target = grd->getSlice2D(ind_z);
  if(getBinsZ() == 1){
    for(int i = 0; i < nX_; i++){
      for(int j = 0; j < nY_; j++){
	target[i][j] = Arr3D[0][i][j];
      }
    }
  } 
  else{
    int zInd  = int ( (z - zGridMin_)/dz_ + 0.5 );
    double fracZ = 0.0;
    double Wzm = 0., Wz0 = 0., Wzp = 0.;
    if(getBinsZ() > 2){
      //cut off edge for three point interpolation
      if(zInd <= 0) zInd = 1;
      if(zInd >=  nZ_ -1) zInd =  nZ_  - 2;
      fracZ = (z - (zGridMin_ + zInd * dz_))/dz_; 
      Wzm = 0.5 - fracZ;
      Wzm *=0.5*Wzm; 
      Wzp = 0.5 + fracZ;
      Wzp *=0.5*Wzp;
      Wz0 = 0.75 - fracZ*fracZ;
      for(int i = 0; i < nX_; i++){
	for(int j = 0; j < nY_; j++){
	  target[i][j] = Wzm*Arr3D[zInd-1][i][j]+Wz0*Arr3D[zInd][i][j]+Wzp*Arr3D[zInd+1][i][j];
	}
      }           
    }
    else{
      fracZ = (z - zGridMin_)/dz_;
      if(z < zGridMin_) fracZ = 0.0;
      if(z > zGridMax_) fracZ = 1.0;
      Wzm = 1.0 - fracZ;
      Wzp = fracZ;
      for(int i = 0; i < nX_; i++){
	for(int j = 0; j < nY_; j++){
	  target[i][j] = Wzm*Arr3D[0][i][j]+Wzp*Arr3D[1][i][j];
	}
      }
    }
  } 
}

///////////////////////////////////////////////////////////////////////////
//sum the 3D array through all CPUs        -MPI related method 
///////////////////////////////////////////////////////////////////////////
void Grid3D::allReduce()
{
  if( size_MPI > 1){
    int ind = 0;
    for(int i = 0; i < nZ_; i++){
      for(int k = 0; k < nX_; k++){
	for(int j = 0; j < nY_; j++){
          buffMPI_in[ ind ] = Arr3D[i][k][j];
          ind++;
	}
      }
    }

    MPI_Allreduce(buffMPI_in,buffMPI_out,ind, 
		  MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

    ind = 0;
    for(int i = 0; i < nZ_; i++){
      for(int k = 0; k < nX_; k++){
	for(int j = 0; j < nY_; j++){
          Arr3D[i][k][j]=buffMPI_out[ ind ];
          ind++;
	}
      }
    }  
  }
}


////////////////////////////////////////////////////////////////////////////
//MPI method to produce conformed parallel ring of matrices with left (right)
//slice equals of the second slice from the right (left)
//This method is used in parallel 3D space charge calculations.
////////////////////////////////////////////////////////////////////////////
void Grid3D::makeRing()
{

  if(nZ_ < 3){
    if(rank_MPI == 0){
      std::cout << "Grid3D::makeRing()"<<std::endl;
      std::cout << "This method can not be used if nZ_ < 3 here nZ_="<<nZ_<<std::endl;
      std::cout << "Check the size of the arrays."<<std::endl;
      std::cout << "Stop."<<std::endl;
    }
    FinalizeExecution();
  }

  if(size_MPI == 1) {
    return;
  }

  MPI_Status statusMPI;

  //clockwise sending - add right slice to the second left one at the next CPU
  int send_to_ID, recive_from_ID;
  int ind = 0;

  send_to_ID = rank_MPI +1;
  recive_from_ID = rank_MPI - 1;
  if( rank_MPI == (size_MPI-1)) { send_to_ID = 0; }
  if( rank_MPI == 0           ) { recive_from_ID =(size_MPI-1); } 

  //send slice nZ_-1
  int iSlice = nZ_-1;
  ind = 0;
  for(int k = 0; k < nX_; k++){
    for(int j = 0; j < nY_; j++){
      buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
      ind++;
    }
  }

  MPI_Sendrecv(buffMPI_in , nX_* nY_, MPI_DOUBLE, send_to_ID    , 1, 
               buffMPI_out, nX_* nY_, MPI_DOUBLE, recive_from_ID, 1,
               MPI_COMM_WORLD , &statusMPI);

  if(rank_MPI != 0){
    iSlice = 1;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] += buffMPI_out[ ind ];
	ind++;
      }
    }
  }

  //send slice nZ_-2
  iSlice = nZ_-2;
  ind = 0;
  for(int k = 0; k < nX_; k++){
    for(int j = 0; j < nY_; j++){
      buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
      ind++;
    }
  }

  MPI_Sendrecv(buffMPI_in , nX_* nY_, MPI_DOUBLE, send_to_ID    , 2, 
               buffMPI_out, nX_* nY_, MPI_DOUBLE, recive_from_ID, 2,
               MPI_COMM_WORLD , &statusMPI);

  if(rank_MPI != 0){
    iSlice = 0;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] += buffMPI_out[ ind ];
	ind++;
      }
    }
  }


  //anti-clockwise sending - add left slice to the second right one at the next CPU
  //send slice 0
  iSlice = 0;
  ind = 0;
  for(int k = 0; k < nX_; k++){
    for(int j = 0; j < nY_; j++){
      buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
      ind++;
    }
  }

  MPI_Sendrecv(buffMPI_in , nX_* nY_, MPI_DOUBLE, recive_from_ID, 3, 
               buffMPI_out, nX_* nY_, MPI_DOUBLE, send_to_ID,     3,
               MPI_COMM_WORLD , &statusMPI);

  if(rank_MPI != (size_MPI-1)){
    iSlice = nZ_ -2;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] = buffMPI_out[ ind ];
	ind++;
      }
    }
  }

  //send slice 1
  iSlice = 1;
  ind = 0;
  for(int k = 0; k < nX_; k++){
    for(int j = 0; j < nY_; j++){
      buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
      ind++;
    }
  }

  MPI_Sendrecv(buffMPI_in , nX_* nY_, MPI_DOUBLE, recive_from_ID, 4, 
               buffMPI_out, nX_* nY_, MPI_DOUBLE, send_to_ID,     4,
               MPI_COMM_WORLD , &statusMPI);

  if(rank_MPI != (size_MPI-1)){
    iSlice = nZ_ - 1;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] = buffMPI_out[ ind ];
	ind++;
      }
    }
  }

}

///////////////////////////////////////////////////////////////////////////
//This method does makeRing() and does periodic conditions
//these conditions necessary for coasting beam
///////////////////////////////////////////////////////////////////////////
void Grid3D::makePeriodicRing()
{
  if(size_MPI == 1) {
    //non-parallel case
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	//add right and left slices to the second right and left one
	//remember that we have auxilary (non-physical) left and right slices
	Arr3D[2][k][j] += Arr3D[nZ_-1][k][j];
	Arr3D[nZ_-1][k][j] = Arr3D[2][k][j];
	Arr3D[nZ_-3][k][j] += Arr3D[0][k][j];
	Arr3D[0][k][j] = Arr3D[nZ_-3][k][j];
	
        // Periodic boundary conditions
	Arr3D[1][k][j] += Arr3D[nZ_-2][k][j];
	Arr3D[nZ_-2][k][j] = Arr3D[1][k][j];
      }
    }
    return;
  }

  makeRing();

  MPI_Status statusMPI;

  int iSlice;
  int ind;
  //-----------------------------------------
  //Satisfy periodic conditions
  //-----------------------------------------

  //conform slices nZ_-2 and 1

  if(rank_MPI == (size_MPI-1)){
    iSlice = nZ_-2;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
	ind++;
      }
    }
    MPI_Send(buffMPI_in, nX_* nY_, MPI_DOUBLE, 0, 11, MPI_COMM_WORLD); 
  }

  if(rank_MPI == 0){
    MPI_Recv(buffMPI_out, nX_* nY_, MPI_DOUBLE,size_MPI-1, 11,MPI_COMM_WORLD, &statusMPI);
    iSlice = 1;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] += buffMPI_out[ ind ];
	buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
	ind++;
      }
    }
    MPI_Send(buffMPI_in, nX_* nY_, MPI_DOUBLE, size_MPI-1, 12, MPI_COMM_WORLD); 
  }

  if(rank_MPI == (size_MPI-1)){

    MPI_Recv(buffMPI_out, nX_* nY_, MPI_DOUBLE,0 , 12 ,MPI_COMM_WORLD, &statusMPI);

    iSlice = nZ_-2;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] = buffMPI_out[ ind ];
	ind++;
      }
    }
  }


  //conform slices nZ_-1 and 2

  if(rank_MPI == (size_MPI-1)){
    iSlice = nZ_-1;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
	ind++;
      }
    }
    MPI_Send(buffMPI_in, nX_* nY_, MPI_DOUBLE, 0, 13, MPI_COMM_WORLD); 
  }

  if(rank_MPI == 0){
    MPI_Recv(buffMPI_out, nX_* nY_, MPI_DOUBLE,size_MPI-1, 13,MPI_COMM_WORLD, &statusMPI);
    iSlice = 2;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] += buffMPI_out[ ind ];
	buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
	ind++;
      }
    }
    MPI_Send(buffMPI_in, nX_* nY_, MPI_DOUBLE, size_MPI-1, 14, MPI_COMM_WORLD); 
  }

  if(rank_MPI == (size_MPI-1)){

    MPI_Recv(buffMPI_out, nX_* nY_, MPI_DOUBLE,0 , 14 ,MPI_COMM_WORLD, &statusMPI);

    iSlice = nZ_-1;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] = buffMPI_out[ ind ];
	ind++;
      }
    }
  }

  //conform slices nZ_-3 and 0

  if(rank_MPI == (size_MPI-1)){
    iSlice = nZ_-3;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
	ind++;
      }
    }
    MPI_Send(buffMPI_in, nX_* nY_, MPI_DOUBLE, 0, 15, MPI_COMM_WORLD); 
  }

  if(rank_MPI == 0){
    MPI_Recv(buffMPI_out, nX_* nY_, MPI_DOUBLE,size_MPI-1, 15,MPI_COMM_WORLD, &statusMPI);
    iSlice = 0;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] += buffMPI_out[ ind ];
	buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
	ind++;
      }
    }
    MPI_Send(buffMPI_in, nX_* nY_, MPI_DOUBLE, size_MPI-1, 16, MPI_COMM_WORLD); 
  }

  if(rank_MPI == (size_MPI-1)){

    MPI_Recv(buffMPI_out, nX_* nY_, MPI_DOUBLE,0 , 16 ,MPI_COMM_WORLD, &statusMPI);

    iSlice = nZ_-3;
    ind = 0;
    for(int k = 0; k < nX_; k++){
      for(int j = 0; j < nY_; j++){
	Arr3D[iSlice][k][j] = buffMPI_out[ ind ];
	ind++;
      }
    }
  }
}
///////////////////////////////////////////////////////////////////////////
//broadcast the 3D array through all CPUs from CPU with rank=rank 
//  -MPI related method 
///////////////////////////////////////////////////////////////////////////
void Grid3D::broadcast(int rank)
{
 if( size_MPI > 1){
   if(rank < size_MPI){
     int ind = 0;
     for(int i = 0; i < nZ_; i++){
       for(int k = 0; k < nX_; k++){
	 for(int j = 0; j < nY_; j++){
	   buffMPI_in[ ind ] = Arr3D[i][k][j];
	   ind++;
	 }
       }
     }
     MPI_Bcast(buffMPI_in, ind, MPI_DOUBLE, rank, MPI_COMM_WORLD);
     ind = 0;
     for(int i = 0; i < nZ_; i++){
       for(int k = 0; k < nX_; k++){
	 for(int j = 0; j < nY_; j++){
	   Arr3D[i][k][j] = buffMPI_in[ ind ];
	   ind++;
	 }
       }
     }
   }
   else{
    if(rank_MPI == 0){
      std::cout << "Grid3D::broadcast(int rank) \n";
      std::cout << "rank     = "<<rank     <<"\n";
      std::cout << "size_MPI = "<<size_MPI <<"\n";
      std::cout << "Check the rank. Should be < size_MPI.\n";
      std::cout << "Stop. \n";
    }
    FinalizeExecution();     
   }
 }
}

///////////////////////////////////////////////////////////////////////////
//broadcast the one slice of the 3D array through all CPUs from CPU 
//with rank=rank 
//-MPI related method 
///////////////////////////////////////////////////////////////////////////
void Grid3D::broadcast(int rank,int iSlice)
{
  if( size_MPI > 1 && iSlice >= 0 && iSlice < nZ_){
    if(rank < size_MPI){
      int ind = 0;
      for(int k = 0; k < nX_; k++){
	for(int j = 0; j < nY_; j++){
	  buffMPI_in[ ind ] = Arr3D[iSlice][k][j];
	  ind++;
	}
      }
      MPI_Bcast(buffMPI_in, ind, MPI_DOUBLE, rank, MPI_COMM_WORLD);
      ind = 0;
      for(int k = 0; k < nX_; k++){
	for(int j = 0; j < nY_; j++){
	  Arr3D[iSlice][k][j] = buffMPI_in[ ind ];
	  ind++;
	}
      }
    }
   else{
    if(rank_MPI == 0){
      std::cout << "Grid3D::broadcast(int rank, int iSlice)"<<std::endl;
      std::cout << "rank     = "<<rank     <<std::endl;
      std::cout << "size_MPI = "<<size_MPI <<std::endl;
      std::cout << "iSlice   = "<<iSlice   <<std::endl;
      std::cout << "nZ_      = "<<nZ_      <<std::endl;
      std::cout << "Check the rank or iSlice. Should be < size_MPI."<<std::endl;
      std::cout << "Stop."<<std::endl;
    }
    FinalizeExecution();     
   }
 }
}

////////////////////////////////////////////////////////////////////////////
//Finalize MPI if it has been initialized and exit
////////////////////////////////////////////////////////////////////////////
void Grid3D::FinalizeExecution()
{
  if(iMPIini > 0){
    MPI_Finalize();
  }
  exit(1);
}

////////////////////////////////////////////////////////////////////////////
//returns the boundary instance of this Grid3D
////////////////////////////////////////////////////////////////////////////
EP_Boundary* Grid3D::getBoundary()
{
  return boundary;
}

////////////////////////////////////////////////////////////////////////////
// get the index and the fraction of the grid's point for particular (x,y,z)   
//   The index should be a central point in three point interpolation
//   schema so:  1 <= ind < nBins 
//   The fraction should be: 0 <= frac <= 1.0
// In z-direction, the above schema are appliable for getBinsZ>2.
////////////////////////////////////////////////////////////////////////////
void Grid3D::getGridIndAndFrac(double x, int& xInd, double& xFrac,
			       double y, int& yInd, double& yFrac,
			       double z, int& zInd, double& zFrac)
{
  boundary->getIndAndFracX( x, xInd, xFrac);
  boundary->getIndAndFracY( y, yInd, yFrac);
 
  //z direction
  if( nZ_ > 1){
    zInd  = int ( (z - zGridMin_)/dz_ + 0.5 );

    if( nZ_ > 2){
      //cut off edge for three point interpolation
      if(zInd <= 0) zInd = 1;
      if(zInd >=  nZ_ -1) zInd =  nZ_  - 2;
      zFrac = (z - (zGridMin_ + zInd * dz_))/dz_;
    }
    if( nZ_ == 2){
      zFrac = (z - zGridMin_)/dz_;
      if(zInd < 0) {
	zInd = 0;
	zFrac = 0.0;
      }
      else if(zInd > 1) {
	zInd = 1;
	zFrac = 1.0;
      }
    }
  }
  else{
    dz_=0.0; zInd = 0;zFrac = 0.0;
  }
}
////////////////////////////////////////////////////////////////////////////
// bin a macroparticle into Arr3D
// (add wights of the interpolation to Arr3D)
////////////////////////////////////////////////////////////////////////////
void Grid3D::binParticleIntoArr3D(double macroSize, double x, double y, double z)
{
  int iX, iY, iZ;
  double xFrac, yFrac, zFrac;
  getGridIndAndFrac(x, iX, xFrac, y, iY, yFrac, z, iZ, zFrac);
  binParticleIntoArr3D(macroSize, iX, xFrac, iY, yFrac, iZ, zFrac); 
}

////////////////////////////////////////////////////////////////////////////
//bin a macroparticle into Arr3D
// (add wights of the interpolation to Arr3D)
////////////////////////////////////////////////////////////////////////////
void Grid3D::binParticleIntoArr3D(double macroSize, 
				  int iX, double xFrac,
				  int iY, double yFrac,
				  int iZ, double zFrac)
{
  //Calculate interpolation weight
  double Wxm,Wx0,Wxp,Wym,Wy0,Wyp;
  double Wzm,Wz0,Wzp;
  Wzm = Wz0 = Wzp = 0.0;
 
  Wxm = 0.5 * (0.5 - xFrac) * (0.5 - xFrac);
  Wx0 = 0.75 - xFrac * xFrac;
  Wxp = 0.5 * (0.5 + xFrac) * (0.5 + xFrac);
  Wym = 0.5 * (0.5 - yFrac) * (0.5 - yFrac);
  Wy0 = 0.75 - yFrac * yFrac;
  Wyp = 0.5 * (0.5 + yFrac) * (0.5 + yFrac);

  if( nZ_ >= 3){
    Wzm = 0.5 * (0.5 - zFrac) * (0.5 - zFrac);
    Wz0 = 0.75 - zFrac * zFrac;
    Wzp = 0.5 * (0.5 + zFrac) * (0.5 + zFrac);
  }
  if( nZ_ == 2){
    Wzm = 1.0 - zFrac; // for zInd=0
    Wz0 = 0.0; 
    Wzp = zFrac;       // for zInd=1
  }

  //Add weight of particle to Arr3D

  double tmp;

  if( nZ_ >= 3){
    tmp = Wym * Wzm *macroSize;
    Arr3D[iZ-1][iX-1][iY-1] += Wxm * tmp;
    Arr3D[iZ-1][iX  ][iY-1] += Wx0 * tmp;
    Arr3D[iZ-1][iX+1][iY-1] += Wxp * tmp;
    tmp = Wy0 * Wzm *macroSize;
    Arr3D[iZ-1][iX-1][iY  ] += Wxm * tmp;
    Arr3D[iZ-1][iX  ][iY  ] += Wx0 * tmp;
    Arr3D[iZ-1][iX+1][iY  ] += Wxp * tmp;
    tmp = Wyp * Wzm *macroSize;
    Arr3D[iZ-1][iX-1][iY+1] += Wxm * tmp;
    Arr3D[iZ-1][iX  ][iY+1] += Wx0 * tmp;
    Arr3D[iZ-1][iX+1][iY+1] += Wxp * tmp;
    tmp = Wym * Wz0 *macroSize;
    Arr3D[iZ  ][iX-1][iY-1] += Wxm * tmp;
    Arr3D[iZ  ][iX  ][iY-1] += Wx0 * tmp;
    Arr3D[iZ  ][iX+1][iY-1] += Wxp * tmp;
    tmp = Wy0 * Wz0 *macroSize;
    Arr3D[iZ  ][iX-1][iY  ] += Wxm * tmp;
    Arr3D[iZ  ][iX  ][iY  ] += Wx0 * tmp;
    Arr3D[iZ  ][iX+1][iY  ] += Wxp * tmp;
    tmp = Wyp * Wz0 *macroSize;
    Arr3D[iZ  ][iX-1][iY+1] += Wxm * tmp;
    Arr3D[iZ  ][iX  ][iY+1] += Wx0 * tmp;
    Arr3D[iZ  ][iX+1][iY+1] += Wxp * tmp;
    tmp = Wym * Wzp *macroSize;
    Arr3D[iZ+1][iX-1][iY-1] += Wxm * tmp;
    Arr3D[iZ+1][iX  ][iY-1] += Wx0 * tmp;
    Arr3D[iZ+1][iX+1][iY-1] += Wxp * tmp;
    tmp = Wy0 * Wzp *macroSize;
    Arr3D[iZ+1][iX-1][iY  ] += Wxm * tmp;
    Arr3D[iZ+1][iX  ][iY  ] += Wx0 * tmp;
    Arr3D[iZ+1][iX+1][iY  ] += Wxp * tmp;
    tmp = Wyp * Wzp *macroSize;
    Arr3D[iZ+1][iX-1][iY+1] += Wxm * tmp;
    Arr3D[iZ+1][iX  ][iY+1] += Wx0 * tmp;
    Arr3D[iZ+1][iX+1][iY+1] += Wxp * tmp;  
  }
  if( nZ_ == 2){
    tmp = Wym * Wzm *macroSize;
    Arr3D[0][iX-1][iY-1] += Wxm * tmp;
    Arr3D[0][iX  ][iY-1] += Wx0 * tmp;
    Arr3D[0][iX+1][iY-1] += Wxp * tmp;
    tmp = Wy0 * Wzm *macroSize;
    Arr3D[0][iX-1][iY  ] += Wxm * tmp;
    Arr3D[0][iX  ][iY  ] += Wx0 * tmp;
    Arr3D[0][iX+1][iY  ] += Wxp * tmp;
    tmp = Wyp * Wzm *macroSize;
    Arr3D[0][iX-1][iY+1] += Wxm * tmp;
    Arr3D[0][iX  ][iY+1] += Wx0 * tmp;
    Arr3D[0][iX+1][iY+1] += Wxp * tmp;
    tmp = Wym * Wzp *macroSize;
    Arr3D[1][iX-1][iY-1] += Wxm * tmp;
    Arr3D[1][iX  ][iY-1] += Wx0 * tmp;
    Arr3D[1][iX+1][iY-1] += Wxp * tmp;
    tmp = Wy0 * Wzp *macroSize;
    Arr3D[1][iX-1][iY  ] += Wxm * tmp;
    Arr3D[1][iX  ][iY  ] += Wx0 * tmp;
    Arr3D[1][iX+1][iY  ] += Wxp * tmp;
    tmp = Wyp * Wzp *macroSize;
    Arr3D[1][iX-1][iY+1] += Wxm * tmp;
    Arr3D[1][iX  ][iY+1] += Wx0 * tmp;
    Arr3D[1][iX+1][iY+1] += Wxp * tmp;  
  }
  if( nZ_ == 1){
    tmp = Wym * macroSize;
    Arr3D[0][iX-1][iY-1] += Wxm * tmp;
    Arr3D[0][iX  ][iY-1] += Wx0 * tmp;
    Arr3D[0][iX+1][iY-1] += Wxp * tmp;
    tmp = Wy0 * macroSize;
    Arr3D[0][iX-1][iY  ] += Wxm * tmp;
    Arr3D[0][iX  ][iY  ] += Wx0 * tmp;
    Arr3D[0][iX+1][iY  ] += Wxp * tmp;
    tmp = Wyp * macroSize;
    Arr3D[0][iX-1][iY+1] += Wxm * tmp;
    Arr3D[0][iX  ][iY+1] += Wx0 * tmp;
    Arr3D[0][iX+1][iY+1] += Wxp * tmp;
  }
}

///////////////////////////////////////////////////////////////////////////
// calc gradient of Arr3D
// NOTE: gradX = gradient_x(Arr3D), and so on
///////////////////////////////////////////////////////////////////////////
void Grid3D::calcGradient(double x,double& gradX,
			  double y,double& gradY,
			  double z,double& gradZ)
{
  int iX, iY, iZ;
  double xFrac, yFrac, zFrac;
  getGridIndAndFrac(x, iX, xFrac, y, iY, yFrac, z, iZ, zFrac); 
  calcGradient(iX, xFrac, gradX, iY, yFrac, gradY, iZ, zFrac, gradZ); 
}

///////////////////////////////////////////////////////////////////////////
// calc gradient of Arr3D
// NOTE: gradX = gradient_x(Arr3D), and so on
///////////////////////////////////////////////////////////////////////////
void Grid3D::calcGradient(int iX,double xFrac,double& gradX,
			  int iY,double yFrac,double& gradY,
			  int iZ,double zFrac,double& gradZ)
{
  //coeff. of Arr3D
  double Wxm,Wx0,Wxp,Wym,Wy0,Wyp;
  double Wzm, Wz0, Wzp;
  Wzm = Wz0 = Wzp = 0.0;

  Wxm = 0.5 * (0.5 - xFrac) * (0.5 - xFrac);
  Wx0 = 0.75 - xFrac * xFrac;
  Wxp = 0.5 * (0.5 + xFrac) * (0.5 + xFrac);
  Wym = 0.5 * (0.5 - yFrac) * (0.5 - yFrac);
  Wy0 = 0.75 - yFrac * yFrac;
  Wyp = 0.5 * (0.5 + yFrac) * (0.5 + yFrac);
  Wzm = 0.0; Wz0 = 0.0; Wzp = 0.0;
  if( nZ_ >= 3){
    Wzm = 0.5 * (0.5 - zFrac) * (0.5 - zFrac);
    Wz0 = 0.75 - zFrac * zFrac;
    Wzp = 0.5 * (0.5 + zFrac) * (0.5 + zFrac);
  }
  else if( nZ_ == 2){
    Wzm = 1.0 - zFrac; // for zInd=0
    Wz0 = 0.0; 
    Wzp = zFrac;       // for zInd=1
  }

  //coeff. for grad(Arr3D) through the Gradient weights, dWs
  double dWxm,dWx0,dWxp,dWym,dWy0,dWyp;
  double dWzm, dWz0, dWzp; 
  dWzm = dWz0 = dWzp = 0.0;

  dWxm = (-1.0)*  (0.5 - xFrac)/dx_;
  dWx0 = (-1.0)*    2. * xFrac /dx_;
  dWxp = (-1.0)* -(0.5 + xFrac)/dx_;
  dWym = (-1.0)*  (0.5 - yFrac)/dy_;
  dWy0 = (-1.0)*    2. * yFrac /dy_;
  dWyp = (-1.0)* -(0.5 + yFrac)/dy_;
  if( nZ_ >= 3){
    dWzm = (-1.0)*  (0.5 - zFrac)/dz_;
    dWz0 = (-1.0)*    2. * zFrac /dz_;
    dWzp = (-1.0)* -(0.5 + zFrac)/dz_;
  }
  else if( nZ_ == 2){
    dWzm = (-1.0)*  1.0/dz_; // for zInd=0
    dWz0 =  0.0; 
    dWzp = (-1.0)* -1.0/dz_; // for zInd=1   
  }

  //calculate gradient
  if( nZ_ >= 3){
    gradX = 
      calcSheetGradient(iZ-1,iX,iY,dWxm,dWx0,dWxp,Wym,Wy0,Wyp) *Wzm +
      calcSheetGradient(iZ  ,iX,iY,dWxm,dWx0,dWxp,Wym,Wy0,Wyp) *Wz0 +
      calcSheetGradient(iZ+1,iX,iY,dWxm,dWx0,dWxp,Wym,Wy0,Wyp) *Wzp;
    gradY = 
      calcSheetGradient(iZ-1,iX,iY,Wxm,Wx0,Wxp,dWym,dWy0,dWyp) *Wzm +
      calcSheetGradient(iZ  ,iX,iY,Wxm,Wx0,Wxp,dWym,dWy0,dWyp) *Wz0 +
      calcSheetGradient(iZ+1,iX,iY,Wxm,Wx0,Wxp,dWym,dWy0,dWyp) *Wzp;
    gradZ = 
      calcSheetGradient(iZ-1,iX,iY,Wxm,Wx0,Wxp,Wym,Wy0,Wyp) *dWzm +
      calcSheetGradient(iZ  ,iX,iY,Wxm,Wx0,Wxp,Wym,Wy0,Wyp) *dWz0 +
      calcSheetGradient(iZ+1,iX,iY,Wxm,Wx0,Wxp,Wym,Wy0,Wyp) *dWzp;
  } 
  if( nZ_ == 2){
    gradX = 
      calcSheetGradient(0,iX,iY,dWxm,dWx0,dWxp,Wym,Wy0,Wyp) *Wzm +
      calcSheetGradient(1,iX,iY,dWxm,dWx0,dWxp,Wym,Wy0,Wyp) *Wzp;
    gradY = 
      calcSheetGradient(0,iX,iY,Wxm,Wx0,Wxp,dWym,dWy0,dWyp) *Wzm +
      calcSheetGradient(1,iX,iY,Wxm,Wx0,Wxp,dWym,dWy0,dWyp) *Wzp;
    gradZ = 
      calcSheetGradient(0,iX,iY,Wxm,Wx0,Wxp,Wym,Wy0,Wyp) *dWzm +
      calcSheetGradient(1,iX,iY,Wxm,Wx0,Wxp,Wym,Wy0,Wyp) *dWzp;
  }
  if( nZ_ == 1){
    gradX = 
      calcSheetGradient(0,iX,iY,dWxm,dWx0,dWxp,Wym,Wy0,Wyp);
    gradY = 
      calcSheetGradient(0,iX,iY,Wxm,Wx0,Wxp,dWym,dWy0,dWyp);
    gradZ = 0.0;
  }

}

///////////////////////////////////////////////////////////////////////////
// calc gradient of Arr3D
// NOTE: gradX = gradient_x(Arr3D), and so on
//calculates gradient at grid point, does not do interpolation.
//This method is dangerous because it does not includes checking
//of the indixes.
///////////////////////////////////////////////////////////////////////////
void Grid3D::calcGradient(int xInd, double& gradX,
			  int yInd, double& gradY,
			  int zInd, double& gradZ)
{
  if(xInd < 1){
    gradX = -1.5*Arr3D[zInd][xInd][yInd]+2.0*Arr3D[zInd][xInd+1][yInd]-0.5*Arr3D[zInd][xInd+2][yInd];
  }
  else{
    if(xInd < (nX_-1)){
      gradX = 0.5*(Arr3D[zInd][xInd+1][yInd] - Arr3D[zInd][xInd-1][yInd]);
    }
    else{
      gradX = 0.5*Arr3D[zInd][xInd-2][yInd]-2.0*Arr3D[zInd][xInd-1][yInd]+1.5*Arr3D[zInd][xInd][yInd];
    }
  }
  gradX /= dx_;

  if(yInd < 1){
    gradY = -1.5*Arr3D[zInd][xInd][yInd]+2.0*Arr3D[zInd][xInd][yInd+1]-0.5*Arr3D[zInd][xInd][yInd+2];
  }
  else{
    if(yInd < (nY_-1)){
      gradY = 0.5*(Arr3D[zInd][xInd][yInd+1] - Arr3D[zInd][xInd][yInd-1]);
    }
    else{
      gradY = 0.5*Arr3D[zInd][xInd][yInd-2]-2.0*Arr3D[zInd][xInd][yInd-1]+1.5*Arr3D[zInd][xInd][yInd];
    }
  }
  gradY /= dy_;

  if(nZ_ == 1){
    gradZ = 0.;
    return;
  }
  else{
    if(nZ_ == 2){
      gradZ = (Arr3D[1][xInd][yInd] - Arr3D[0][xInd][yInd])/dz_;
      return;
    }
    
    //the Z-case is the same as x and y.
    if(zInd < 1){
      gradZ = -1.5*Arr3D[zInd][xInd][yInd]+2.0*Arr3D[zInd+1][xInd][yInd]-0.5*Arr3D[zInd+2][xInd][yInd];
    }
    else{
      if(zInd < (nZ_-1)){
	gradZ = 0.5*(Arr3D[zInd+1][xInd][yInd] - Arr3D[zInd-1][xInd][yInd]);
      }
      else{
	gradZ = 0.5*Arr3D[zInd-2][xInd][yInd]-2.0*Arr3D[zInd-1][xInd][yInd]+1.5*Arr3D[zInd][xInd][yInd];
      }
    }
    gradZ /= dz_;
  }
}

///////////////////////////////////////////////////////////////////////////
// calculates value at the point with coordinates x,y,z
///////////////////////////////////////////////////////////////////////////
double Grid3D::calcValue(double x,double y,double z)
{
  int xInd,yInd,zInd;
  double xFrac,yFrac,zFrac;
  getGridIndAndFrac( x, xInd, xFrac, y, yInd, yFrac, z, zInd, zFrac);
  return calcValue(xInd,yInd,zInd,xFrac,yFrac,zFrac);
}

///////////////////////////////////////////////////////////////////////////
// calculates value at the point with coordinates x,y,z
///////////////////////////////////////////////////////////////////////////
double Grid3D::calcValue(int    iX,   int    iY,   int    iZ,
			 double fracX,double fracY,double fracZ)
{

  double value = 0.0; 

  double Wxm, Wx0, Wxp, Wym, Wy0, Wyp;
  double Wzm = 0.0, Wz0 = 0.0, Wzp = 0.0;

  Wxm = 0.5 - fracX;
  Wxm *=0.5*Wxm; 
  Wxp = 0.5 + fracX;
  Wxp *=0.5*Wxp;
  Wx0 = 0.75 - fracX*fracX;

  Wym = 0.5 - fracY;
  Wym *=0.5*Wym; 
  Wyp = 0.5 + fracY;
  Wyp *=0.5*Wyp;
  Wy0 = 0.75 - fracY*fracY;

  if( nZ_ > 2 ){
    Wzm = 0.5 - fracZ;
    Wzm *=0.5*Wzm; 
    Wzp = 0.5 + fracZ;
    Wzp *=0.5*Wzp;
    Wz0 = 0.75 - fracZ*fracZ;
  } 
  else if( nZ_ == 2 ){
    Wzm = 1.0 - fracZ;
    Wz0 = 0.0;
    Wzp = fracZ;
  }
  else if( nZ_ == 1 ){
    Wzm = 0.0;
    Wz0 = 1.0;
    Wzp = 0.0;    
  }

  double Vm, V0, Vp;
  double Vzm, Vz0, Vzp;

  if( nZ_ > 2 ){
    Vm = calcValueOnX(iX,iY-1,iZ-1,Wxm,Wx0,Wxp);
    V0 = calcValueOnX(iX,iY  ,iZ-1,Wxm,Wx0,Wxp);
    Vp = calcValueOnX(iX,iY+1,iZ-1,Wxm,Wx0,Wxp);
    Vzm = Wym*Vm + Wy0*V0 + Wyp*Vp;

    Vm = calcValueOnX(iX,iY-1,iZ,Wxm,Wx0,Wxp);
    V0 = calcValueOnX(iX,iY  ,iZ,Wxm,Wx0,Wxp);
    Vp = calcValueOnX(iX,iY+1,iZ,Wxm,Wx0,Wxp);
    Vz0 = Wym*Vm + Wy0*V0 + Wyp*Vp;

    Vm = calcValueOnX(iX,iY-1,iZ+1,Wxm,Wx0,Wxp);
    V0 = calcValueOnX(iX,iY  ,iZ+1,Wxm,Wx0,Wxp);
    Vp = calcValueOnX(iX,iY+1,iZ+1,Wxm,Wx0,Wxp);
    Vzp = Wym*Vm + Wy0*V0 + Wyp*Vp;

    value = Wzm*Vzm + Wz0*Vz0 + Wzp*Vzp;   
  }
  else if(nZ_ == 2){
    Vm = calcValueOnX(iX,iY-1,0,Wxm,Wx0,Wxp);
    V0 = calcValueOnX(iX,iY  ,0,Wxm,Wx0,Wxp);
    Vp = calcValueOnX(iX,iY+1,0,Wxm,Wx0,Wxp);
    Vzm = Wym*Vm + Wy0*V0 + Wyp*Vp; 

    Vm = calcValueOnX(iX,iY-1,1,Wxm,Wx0,Wxp);
    V0 = calcValueOnX(iX,iY  ,1,Wxm,Wx0,Wxp);
    Vp = calcValueOnX(iX,iY+1,1,Wxm,Wx0,Wxp);
    Vzp = Wym*Vm + Wy0*V0 + Wyp*Vp; 

    value = Wzm*Vzm + Wzp*Vzp; 
  }
  else if(nZ_ == 1){
    Vm = calcValueOnX(iX,iY-1,0,Wxm,Wx0,Wxp);
    V0 = calcValueOnX(iX,iY  ,0,Wxm,Wx0,Wxp);
    Vp = calcValueOnX(iX,iY+1,0,Wxm,Wx0,Wxp);

    value = Wym*Vm + Wy0*V0 + Wyp*Vp; 
  }

  return value;
}

///////////////////////////////////////////////////////////////////////////
//returns the sum of all grid points
///////////////////////////////////////////////////////////////////////////
double Grid3D::getSum()
{
  double sum = 0.;
  for(int iZ = 0; iZ < nZ_; iZ++){
    for(int i = 0; i < nX_; i++){
      for(int j = 0; j < nY_; j++){
	sum += Arr3D[iZ][i][j];
      }
    }
  }
  return sum;
}

///////////////////////////////////////////////////////////////////////////
//returns the sum of all grid points in the slice with index iZ
///////////////////////////////////////////////////////////////////////////
double Grid3D::getSliceSum(int iZ)
{
  double sum = 0.;
  if( iZ <   0 ) return sum;
  if( iZ > (nZ_-1)) return sum;
  for(int i = 0; i < nX_; i++){
    for(int j = 0; j < nY_; j++){
      sum += Arr3D[iZ][i][j];
    }
  }
  return sum;
}

///////////////////////////////////////////////////////////////////////////
//returns the sum of all grid points in the slice with  position z
///////////////////////////////////////////////////////////////////////////
double Grid3D::getSliceSum(double z)
{
  double sum = 0.;
  if(z > getzGridMax()) return sum;
  if(z < getzGridMin()) return sum;
  int zInd = 0;
  double zFrac = 0.;
  if(nZ_ > 2){
    double sumM,sum0,sumP;
    zInd  = int ( (z - zGridMin_)/dz_ + 0.5 );
    //cut off edge for three point interpolation
    if(zInd <= 0) zInd = 1;
    if(zInd >=  nZ_ -1) zInd =  nZ_  - 2;
    zFrac = (z - (zGridMin_ + zInd * dz_))/dz_;
    sumM = getSliceSum(zInd - 1);
    sum0 = getSliceSum(zInd);
    sumP = getSliceSum(zInd + 1);
    sum = 0.5*(0.5 - zFrac)*(0.5 - zFrac)*sumM + 
          (0.75 - zFrac*zFrac)*sum0 +
          0.5*(0.5 + zFrac)*(0.5 + zFrac)*sumP;
    return sum;
  }
  if(nZ_ == 2){
   zFrac = (z - zGridMin_)/dz_;
   sum = (1.0 - zFrac)*getSliceSum(0) + zFrac*getSliceSum(1);
   return sum;
  }
  if(nZ_ == 1){
    sum = getSliceSum(0);
    return sum;
  }
  return sum;
}

///////////////////////////////////////////////////////////////////////////
// private:
// calculates interpolated value along x-axis for fixed y and z
///////////////////////////////////////////////////////////////////////////
double Grid3D::calcValueOnX(int iX, int iY, int iZ, double Wxm,double Wx0,double Wxp)
{
  return ( Wxm*Arr3D[iZ][iX-1][iY]+Wx0*Arr3D[iZ][iX][iY]+Wxp*Arr3D[iZ][iX+1][iY]);
}

///////////////////////////////////////////////////////////////////////////
// private:
// calculates interpolated value along y-axis for fixed x and z
///////////////////////////////////////////////////////////////////////////
double Grid3D::calcValueOnY(int iX, int iY, int iZ, double Wym,double Wy0,double Wyp)
{
  return ( Wym*Arr3D[iZ][iX][iY-1]+Wy0*Arr3D[iZ][iX][iY]+Wyp*Arr3D[iZ][iX][iY+1]);
}


///////////////////////////////////////////////////////////////////////////
//private:
// calculates Gradient from each z-sheet without interpolating in z
///////////////////////////////////////////////////////////////////////////
double Grid3D::calcSheetGradient(int iZ,int iX,int iY,
				 double xm,double x0,double xp,
				 double ym,double y0,double yp)		     
{
  double sheetGradient = 
    xm * ym * Arr3D[iZ][iX-1][iY-1] +
    x0 * ym * Arr3D[iZ][iX  ][iY-1] +
    xp * ym * Arr3D[iZ][iX+1][iY-1] +
    xm * y0 * Arr3D[iZ][iX-1][iY  ] +
    x0 * y0 * Arr3D[iZ][iX  ][iY  ] +
    xp * y0 * Arr3D[iZ][iX+1][iY  ] +
    xm * yp * Arr3D[iZ][iX-1][iY+1] +
    x0 * yp * Arr3D[iZ][iX  ][iY+1] +
    xp * yp * Arr3D[iZ][iX+1][iY+1];
  return sheetGradient;
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
