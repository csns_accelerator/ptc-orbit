//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    CompositeSurfaceDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/22/2003
//
// DESCRIPTION
//   It is a composite class for abstractSurfceDiag class. It registers 
//   the surface diagnostics instances and delegates the analysis to them.
//   The methods print(...) were implemented just for case. In general, 
//   each diagnostics should manages its own print method.
//   This method override  analyze() method of the abstract class and 
//   uses the first input parameter of this method (ctimeStep) to calculate
//   total time after start of the new turn and provide it as input parameter
//   for surface diagnostics instances.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef COMPOSITE_SURFACE_DIAGNISTICS_HH
#define COMPOSITE_SURFACE_DIAGNISTICS_HH

#include <iostream> 
#include <fstream>

#include "AbstractSurfaceDiag.hh"

class CompositeSurfaceDiag : public AbstractSurfaceDiag
{
public:

  CompositeSurfaceDiag();
  virtual ~CompositeSurfaceDiag();

  void init();
  void setZeroTurn();
  void startNewTurn();

  //time should be accounted here and passed to the components
  void accountTime(double ctimeStep);

  //indHit index of macro-particle that hit the surface and will die
  //nOut number of macro-particles that were produced
  //the method getTotalCount() of the eBunch should be used.
  //n_v - normal vector to the surface
  void analyze(int indHit, int nOut, eBunch* eb, double* n_v);
 
  void print(std::ostream& Out);
  void print(char* fileName);
  virtual void print();

  //composite pattern method
  void addDiagnostics(AbstractSurfaceDiag* diag);

  //convenience methods
  int getNumberOfDiagnostics();
  AbstractSurfaceDiag* getDiagnostics(int index);

private:

  int nComps;
  int nCompsMax;

  int nTurns;

  double ctimeTotal;

  AbstractSurfaceDiag** components;

};

#endif

