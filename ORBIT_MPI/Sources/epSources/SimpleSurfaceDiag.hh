//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    SimpleSurfaceDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/23/2003
//
// DESCRIPTION
//   This class is the subclass of the AbstractSurfaceDiag class, and it 
//   collects information about energy spectrum of the macro-particles 
//   hitting the surface of the wall, and macro-size distribution of 
//   the same particles. 
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef SIMPLE_SURFACE_DIAGNISTICS_HH
#define SIMPLE_SURFACE_DIAGNISTICS_HH

#include <iostream> 
#include <fstream>

#include "eBunch.hh"
#include "AbstractSurfaceDiag.hh"
#include "histogram.hh"

class SimpleSurfaceDiag : public AbstractSurfaceDiag
{
public:

  //costructor parameters timeStartIn and timeStopIn in [ns]
  SimpleSurfaceDiag(int nTurnIn, 
		    double timeStartIn, 
		    double timeStopIn, 
		    char* fOutNameIn);

  virtual ~SimpleSurfaceDiag();

  void init();
  void setZeroTurn();
  void startNewTurn();

  void accountTime(double ctime);

  //n_v - normal vector to the surface
  void analyze(int indHit, int nOut, eBunch* eb, double* n_v);
 
  void print();
  void print(std::ostream& Out);
  void print(char* fileName);

  //histogram methods
  void setEnergyHistogram(histogram* eH);
  void setMacroSizeHistogram(histogram* mH);
  const histogram* getEnergyHistogram();
  const histogram* getMacroSizeHistogram();

protected:

  int nTurn;
  double ctimeStart;
  double ctimeStop;

  //current turnCurr;
  int turnCurr;

  double ctimeStartActual;
  double ctimeStopActual;
  
  char* fOutName;

  //information about completion
  //inf_start(stop)_done - 0 - not yet 1 - done
  int inf_start_done;
  int inf_stop_done;

  //histograms - energy and macro-size
  histogram* eH_;
  histogram* mH_;
};

#endif

