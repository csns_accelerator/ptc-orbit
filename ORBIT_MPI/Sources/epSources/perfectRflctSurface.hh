//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    perfectRflctSurface.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    04/01/2003
//
// DESCRIPTION
//    Specification and inline functions for a subclass used to 
//    absorb macro particles that hit the surface and reproduce
//    the perfectly reflected new macro particles in the class EPnode
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "stdlib.h"
#include "baseSurface.hh"

#ifndef PERFECT_REFLECTION_SURFACE_H
#define PERFECT_REFLECTION_SURFACE_H
  
///////////////////////////////////////////////////////////////////////////
//
// SUBCLASS NAME
//    perfectRflctSurface
//
// CLASS RELATIONSHIPS
// 
//    subclass perfectRflctSurface -> class baseSurface
//    class baseSurface class EP_Boundary, class eBunch -> class EPnode
//
// DESCRIPTION
//    A subclass to absorb macro particles which across the surface and
//    reproduce new macro particles which reflect at the cross point of 
//    the surface with the same size of momentum but with the difference
//    of in-coming-momentum vector and out-going-momentum vector is parallel 
//    to the normal vector on the surface (perfect reflection). 
//
// PUBLIC METHODS
//    perfectRflctSurface:    Constructor for making the subclass objects
//    ~perfectRflctSurface:   Destructor for this subclass
//    impact:          remove macroparticles crossing the surface
//                     from the list of electron bunch and add the perfecly
//                     reflected macroparticles
//
// PRIVATE/PROTECTED METHODS/MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class  perfectRflctSurface: public baseSurface
{
public:

  perfectRflctSurface();
  virtual ~perfectRflctSurface();

  void impact(int index, eBunch* eb, double* r_v,double* n_v);

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif
