/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   PBunchDiagNode.hh
//
// AUTHORS
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/29/2004
//
// DESCRIPTION
//     PBunchDiagNode class
//
///////////////////////////////////////////////////////////////////////////// 
#ifndef PBUNCH_DIAGNOSTIC_NODE_H
#define PBUNCH_DIAGNOSTIC_NODE_H


/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Node.h"

//FFTW headers
#include "fftw.h"

class PBunchDiagNode : public Node {
  Declare_Standard_Members(PBunchDiagNode, Node);

public:

  PBunchDiagNode(String &n, 
		 int order, 
		 int zSlices_In,
		 int  nRecordsMax_In, 
		 int diagType_In, 
		 const char* typeName_In, 
		 const char* fName_In);

  ~PBunchDiagNode();

  Void nameOut(String &wname);
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);

  //specific methods

  void startRecording();

  void stopRecording();

  void dumpDiagnostics();

  void dumpMaxDiagnostics();
	
	//it will work only for longitudinal diagnostics
  double getBunchFactor();

private:

  int getIndex(double phi);

  void setRecordToZero();

  void allReduceRecord();

  void makeRecordFFT();

  //Calculates <x>*LongDensWeight centroid as function of phi
  void makeCentroidXWDiag(MacroPart &mp);

  //Calculates <x>*LongDensWeight centroid as function of phi
  void makeCentroidYWDiag(MacroPart &mp);

  //diagType = 0 - longitudinal density
  void makeLongDensityDiag(MacroPart &mp);

  //diagType = 1 - longitudinal density FFT
  void makeLongDensityFFTDiag(MacroPart &mp);

  //diagType = 2 - <x> centroid as function of phi
  void makeCentroidXDiag(MacroPart &mp);

  //diagType = 3 - FFT of <x> centroid as function of phi
  void makeCentroidXFFTDiag(MacroPart &mp);

  //diagType = 4 - <y> centroid as function of phi
  void makeCentroidYDiag(MacroPart &mp);

  //diagType = 5 - FFT of <y> centroid as function of phi
  void makeCentroidYFFTDiag(MacroPart &mp);

  //diagType = 6 - sqrt(<(x-<x>)^2+(y-<y>)^2>) as function of phi
  void makeR2Diag(MacroPart &mp);

  //diagType = 7 - FFT of sqrt(<(x-<x>)^2+(y-<y>)^2>) as function of phi
  void makeR2FFTDiag(MacroPart &mp);

  //diagType = 8 - quad moment <(x-<x>)^2>-<(y-<y>)^2> as function of phi
  void makeQuadMomDiag(MacroPart &mp);

  //diagType = 9 - FFT of quad moment <(x-<x>)^2>-<(y-<y>)^2> as function of phi
  void makeQuadMomFFTDiag(MacroPart &mp);

  //diagType = 10 - FFT of <x>*longDensWeight centroid as function of phi
  void makeCentroidXWFFTDiag(MacroPart &mp);

  //diagType = 11 - FFT of <y>*longDensWeight centroid as function of phi
  void makeCentroidYWFFTDiag(MacroPart &mp);

  //diagType = 12 - sqrt(<(x-<x>)^2) as function of phi
  void makeX2Diag(MacroPart &mp);

  //diagType = 13 - sqrt(<(y-<y>)^2) as function of phi
  void makeY2Diag(MacroPart &mp);

  //diagType = 14 - FFT of sqrt(<(x-<x>)^2) as function of phi
  void makeX2FFTDiag(MacroPart &mp);

  //diagType = 15 -FFT of  sqrt(<(y-<y>)^2) as function of phi
  void makeY2FFTDiag(MacroPart &mp);

  //diagType = 16 - (sqrt(<(x-<x>)^2)+sqrt(<(y-<y>)^2)) as function of phi
  void makeXplusYDiag(MacroPart &mp);

  //diagType = 17 - (sqrt(<(x-<x>)^2)-sqrt(<(y-<y>)^2)) as function of phi
  void makeXminsYDiag(MacroPart &mp);

  //diagType = 18 - FFT of (sqrt(<(x-<x>)^2)+sqrt(<(y-<y>)^2)) as function of phi
  void makeXplusYFFTDiag(MacroPart &mp);

  //diagType = 19 - FFT of (sqrt(<(x-<x>)^2)-sqrt(<(y-<y>)^2)) as function of phi
  void makeXminsYFFTDiag(MacroPart &mp); 

protected:

  int nTurn;
	
	//bunch factor
	double bunch_factor;

  //default no recording = 0
  int infRecording;

  int nRecordsMax;

  int nRecords;
  
  int zSlices;
  double phi_step;

  int diagType;

  char* typeName;

  char* fName;
  int  nPrintColumn;
  int* iPrintColumn;

  //data arrays
  double** records;
  int nRecColumn;

  int* nCountsArr;

  fftw_plan planForward;
  FFTW_COMPLEX* inFFT;
  FFTW_COMPLEX* outFFT;

  int* turnInf;

  double* maxValuesArr;

  double* buff_dbl_MPI;
  int*    buff_int_MPI;

  double* centArrX;
  double* centArrY;

  double* r2AvgArr;  

  double* X2Arr;
  double* Y2Arr;

  //MPI members
  int iMPIini; 
  int rank_MPI; 
  int size_MPI;

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif   // EP_NODE_H


