//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    SimpleEBunchDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/23/2003
//
// DESCRIPTION
//   This is a simple class for e-bunch diagnostics. It calculates:
//   1. radial distribution of the bunch
//   2. energy spectrum
//   3. macro-size distribution
//   4. longitudianal distribution
//   5. longitudianal momentum distribution
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include <cmath>

#include "epModuleConstants.hh"
#include "SimpleEBunchDiag.hh"

//costructor parameters timeIn in [ns]
SimpleEBunchDiag::SimpleEBunchDiag(int nTurnIn, double timeIn, char* fOutNameIn) : 
     AbstractEBunchDiag("SIMPLE E-BUNCH DIAGNOSTICS")
{
  fOutName = (char *) malloc (sizeof(char) * (NAME_LENGTH_MAX));
  strncpy(fOutName,fOutNameIn,NAME_LENGTH_MAX);
  nTurn = nTurnIn;
  ctime = timeIn*1.0e-9*epModuleConstants::c;
  cCurrentTime = 0.;
  inf_done = 0;
  eH_ = new histogram(100);
  rH_ = new histogram(100);
  mH_ = new histogram(100);
  zH_ = new histogram(100);
  pzH_ = new histogram(100);
 
}

SimpleEBunchDiag::~SimpleEBunchDiag()
{
  free(fOutName);
  delete eH_;
  delete rH_;
  delete mH_;
  delete zH_;
  delete pzH_;
}

void SimpleEBunchDiag::init()
{
  inf_done = 0;
  eH_->clean();
  rH_->clean();
  mH_->clean();
  zH_->clean();
  pzH_->clean();
}

void SimpleEBunchDiag::setZeroTurn()
{
  init();
}

void SimpleEBunchDiag::startNewTurn()
{
  init();
}

void SimpleEBunchDiag::analyze(int turn, double ctimeIn, eBunch* eb)
{
  double px,py,pz, p2, enrg,macro, x,y,z, r;

  if(turn == nTurn && ctime <= ctimeIn && inf_done == 0){

    cCurrentTime = ctimeIn;

    eH_->clean();
    rH_->clean();
    mH_->clean();
    zH_->clean();
    pzH_->clean();

    //energy, radial, and macro-size histograms
    int nMacroPart = eb->getSize();
    for(int i = 0; i < nMacroPart; i++){
      px = eb->px(i);
      py = eb->py(i);
      pz = eb->pz(i);
      p2 = px*px+py*py+pz*pz;
      //energy will be in eV
      enrg = eb->getEnergy(i);
      macro = eb->macroSize(i);
      x = eb->x(i);
      y = eb->y(i);
      z = eb->z(i);
      r = sqrt(x*x+y*y);
      eH_->add(enrg,macro);
      rH_->add(r,macro);
      mH_->add(macro);
      zH_->add(z,macro);
      pzH_->add(pz,macro);
    }    
    //end of analysis
    inf_done = 1;
    print();
  }
}

void SimpleEBunchDiag::print(){
  print(fOutName);
}

void SimpleEBunchDiag::print(std::ostream& Out)
{
  eH_->analyze();
  rH_->analyze();
  mH_->analyze();
  zH_->analyze();
  pzH_->analyze();
  
  if(rank_MPI == 0){
    Out<<"========================================="<<std::endl;
    Out<<getName()<<std::endl;
    Out<<"========================================="<<std::endl;
    Out<<"time [ns] = "<< cCurrentTime * 1.0e+9/epModuleConstants::c<<std::endl;
  }

  if(rank_MPI == 0){
    Out<<"========ENERGY DISTRIBUTION======="<<std::endl;
  }
  eH_->print(Out);
  if(rank_MPI == 0){
    Out<<"========RADIAL DISTRIBUTION======="<<std::endl;
  }
  rH_->print(Out);
  if(rank_MPI == 0){
    Out<<"========MACRO-SIZE DISTRIBUTION======="<<std::endl;
  }
  mH_->print(Out);

  if(rank_MPI == 0){
    Out<<"========LONGITUDINAL DISTRIBUTION======="<<std::endl;
  }
  zH_->print(Out);
  if(rank_MPI == 0){
    Out<<"========LONGITUDINAL-MOMENTUM DISTRIBUTION======="<<std::endl;
  }
  pzH_->print(Out);
}

void SimpleEBunchDiag::print(char* fileName)
{
    ofstream F_dump;
    if(rank_MPI == 0)F_dump.open (fileName, ios::out);
    print(F_dump);
    if(rank_MPI == 0){F_dump.close();}
}

void SimpleEBunchDiag::setEnergyHistogram(histogram* eH)
{
  if(eH){
    delete eH_;
    eH_ = eH;
  }
}

void SimpleEBunchDiag::setRadialHistogram(histogram* rH)
{
  if(rH){
    delete rH_;
    rH_ = rH;
  } 
}

void SimpleEBunchDiag::setMacroSizeHistogram(histogram* mH)
{
   if(mH){
    delete mH_;
    mH_ = mH;
  }  
}

void SimpleEBunchDiag::setLongPositionHistogram(histogram* zH)
{
   if(zH){
    delete zH_;
    zH_ = zH;
  }  
}

void SimpleEBunchDiag::setLongMomentumHistogram(histogram* pzH)
{
   if(pzH){
    delete pzH_;
    pzH_ = pzH;
  }  
}

const histogram* SimpleEBunchDiag::getEnergyHistogram()
{
  return eH_;
}


const histogram* SimpleEBunchDiag::getRadialHistogram()
{
  return rH_;
}

const histogram* SimpleEBunchDiag::getMacroSizeHistogram()
{
  return mH_;
}

const histogram* SimpleEBunchDiag::getLongPositionHistogram()
{
  return zH_;
}

const histogram* SimpleEBunchDiag::getLongMomentumHistogram()
{
  return pzH_;
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
