//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    pBunchFieldSource.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    07/02/2003
//
// DESCRIPTION
//   The source of the electrostatic and magnetic fields. 
//   originated from the proton bunch. Keeps auxilary Grid3D
//   for potential to produce E = gamma*grad(phi_in_the_pBunch_rest)
//   and Hz = 0, Hx = beta*Ey Hy=-betaEx.
//
//   The actual definition of the inner phi-grid should be done
//   by SpaceCharge3D class from ORBIT package or by pBunch.
//
//   The subclass of the BaseFieldSource.
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include "BaseFieldSource.hh"
#include "Grid3D.hh"

#ifndef P_BUNCH_FIELD_HH
#define P_BUNCH_FIELD_HH

class pBunchFieldSource: public BaseFieldSource
{
public:

  pBunchFieldSource(int nZ,
                    double zGridMinIn,double zGridMaxIn, 
                    EP_Boundary* boundaryIn);

  virtual ~pBunchFieldSource();

  void setBeta(double betaIn);

  double getBeta();

  Grid3D* getPhiGrid3D();

  void setMinMaxZ(double zGridMinIn,double zGridMaxIn);
 
  //--------------------------------------------------------
  //overridden methods
  //---------------------------------------------------------

  //returns the H in Gauss/(electron charge in CGS) = 0.0
  void getMagneticField(double x,    double y,    double z, 
                        double& h_x, double& h_y, double& h_z);

  //returns the electric field (for point like charge 1/(r*r))
  void getElectricField(double x,    double y,    double z, 
                        double& e_x, double& e_y, double& e_z);


private:
  //reference to the boundary
  EP_Boundary* boundary_;

  //local phiGrid (should be created and deleted inside)
  Grid3D* phiGridLocal_;

  int nZ_;

  double zGridMin_, zGridMax_;

  //beta for pBunch
  double beta_;

  //temporary variables
  double ex,ey,ez;
  
};
#endif

