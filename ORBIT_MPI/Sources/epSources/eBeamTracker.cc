//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    eBeamTracker.cc
//
// AUTHOR
//    S. Cousineau
//
// CREATED
//    12/28/2004
//
// DESCRIPTION
//   the base class to move electrons in the electromagnetic field
//   This class needs one or more BaseFieldSource or 
//   BaseFieldSource.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <cstdio>

#include "eBeamTracker.hh"

using namespace std;

eBeamTracker::eBeamTracker(){

  nElSources = 0;
  nMgSources = 0;
  nSteps_ = 1;

  missingDiag = NULL;
  surfaceDiag = NULL;

}


eBeamTracker::~eBeamTracker(){
  if(nElSources > 0){
    delete [] electricSources;
  }
  if(nMgSources > 0){
    delete [] magneticSources;
  }
}



void eBeamTracker::setNumberOfSteps(int nSteps)
{
  nSteps_ = nSteps;
}

void eBeamTracker::addMagneticFieldSource(BaseFieldSource* fs)
{
  BaseFieldSource** fss_tmp = new BaseFieldSource*[nMgSources+1];
  for(int i = 0; i < nMgSources; i++){
    fss_tmp[i] = magneticSources[i];
  }
  fss_tmp[nMgSources] = fs;
  if(nMgSources > 0){
    delete [] magneticSources;
  }
  magneticSources = fss_tmp;
  nMgSources++;
}

void eBeamTracker::addElectricFieldSource(BaseFieldSource* fs)
{
  BaseFieldSource** fss_tmp = new BaseFieldSource*[nElSources+1];
  for(int i = 0; i < nElSources; i++){
    fss_tmp[i] = electricSources[i];
  }
  fss_tmp[nElSources] = fs;
  if(nElSources > 0){
    delete [] electricSources;
  }
  electricSources = fss_tmp;
  nElSources++;
}

void eBeamTracker::removeMagneticFieldSource(BaseFieldSource* fs)
{
  int remInd = -1;
  for(int i = 0; i < nMgSources; i++){
    if(fs == magneticSources[i]) {
      remInd = i;
      break;
    }
  }

  if(remInd < 0) return;

  if(nMgSources == 1){
    delete [] magneticSources;
    magneticSources = NULL;
    nMgSources = 0;
    return;
  }

  BaseFieldSource** fss_tmp = new BaseFieldSource*[nMgSources-1];
  int count = 0;
  for(int i = 0; i < nMgSources; i++){
    if(i != remInd ){
      fss_tmp[count] = magneticSources[i];
      count++;
    }
  }

  delete [] magneticSources;
  magneticSources = fss_tmp;
  nMgSources--;
}

void eBeamTracker::removeElectricFieldSource(BaseFieldSource* fs)
{
  int remInd = -1;
  for(int i = 0; i < nElSources; i++){
    if(fs == electricSources[i]) {
      remInd = i;
      break;
    }
  }

  if(remInd < 0) return;

  if(nElSources == 1){
    delete [] electricSources;
    electricSources = NULL;
    nElSources = 0;
    return;
  }

  BaseFieldSource** fss_tmp = new BaseFieldSource*[nElSources-1];
  int count = 0;
  for(int i = 0; i < nElSources; i++){
    if(i != remInd ){
      fss_tmp[count] = electricSources[i];
      count++;
    }
  }

  delete [] electricSources;
  electricSources = fss_tmp;
  nElSources--;
}


/*
void eBeamTracker::moveParticlesStrongH(double ctime, eBunch* eb)
{

  //Warning!!!  This method has never been benchmarked!
  if(surfaceDiag){
    surfaceDiag->accountTime(ctime);
  }
 
  //variables declaration
  int nPart = eb->getSize();
  int nStep = 1;
  double ctStep = 0.;

  //mass in MeV
  double mass = eb->getMass();
  double charge = eb->getCharge();
  double classicalR = eb->getClassicalRadius();

  //velocity in a.u v/c=beta
  double vx   = 0., vy   = 0., vz   = 0.;
  double d_vx = 0., d_vy = 0., d_vz = 0.;

  //coordinates of the particle
  double x = 0., y = 0., z = 0.;
  double d_x =0., d_y =0., d_z =0.;

  //fields
  double fieldE = 0., e_x = 0., e_y = 0., e_z = 0.;
  double fieldH = 0., h_x = 0., h_y = 0., h_z = 0.;

  //vectors
  double r_v[3], n_v[3];

  //variable defined state of particle INSIDE, OUTSIDE of boundary
  //or to be killed
  int isInside = 1;  

  //auxiliary variables
  double nx[3], ny[3], nz[3];
  double nx_0,  ny_0,  nz_0;
  double rad = 0., w = 0.;
  double beta_x_0, beta_y_0, beta_z_0;
  double gamma_0;
  double e_x_0, e_y_0, e_z_0;
  double beta_dot_E;
  double beta_x, beta_y, beta_z;
  double gamma;
  double cos_phi_0, sin_phi_0;
  double cos_phi, sin_phi;
  double shift_x,shift_y,shift_z;
  //double Ey_over_H;
  double xfac, yfac, zfac;
  int min_ort_ind = 0;

  double min_ort_val = 0.;
  nStep = nSteps_;
  ctStep = ctime/nStep;

  //start of loop upon macro-particles
  for(int ind = 0; ind < nPart; ind++){
    vx = eb->px(ind)/mass;
    vy = eb->py(ind)/mass;
    vz = eb->pz(ind)/mass;
    x = eb->x(ind);
    y = eb->y(ind);
    z = eb->z(ind);
    //moving particle    ============start==========
    for(int i_step = 0; i_step < nStep; i_step++){
      d_vx = 0.; d_vy = 0.; d_vz = 0.;
      d_x  = 0.; d_y  = 0.; d_z  = 0.;
      for(int iter = 0; iter < 2; iter++){
	getElectricField(x+0.5*d_x,y+0.5*d_y,z+0.5*d_z,e_x,e_y,e_z);
	getMagneticField(x+0.5*d_x,y+0.5*d_y,z+0.5*d_z,h_x,h_y,h_z);
	fieldE = sqrt(e_x*e_x+e_y*e_y+e_z*e_z);
	fieldH = sqrt(h_x*h_x+h_y*h_y+h_z*h_z);
	if(fieldH > 0.){
	  //presence of magnetic field
	  //cout <<"debug start magn field tracking! H="<< fieldH <<" fieldE="<< fieldE <<"\n";
	  nz[0] = h_x/fieldH;
	  nz[1] = h_y/fieldH;
	  nz[2] = h_z/fieldH;
	  nz_0  = 1.0;
	  nx[0] = e_y*nz[2]-e_z*nz[1];
	  nx[1] = e_z*nz[0]-e_x*nz[2];
	  nx[2] = e_x*nz[1]-e_y*nz[0]; 
	  nx_0 = sqrt(nx[0]*nx[0]+nx[1]*nx[1]+nx[2]*nx[2]);
          if(nx_0 == 0.){
	    //reason why nx_0 = 0 may be E = 0 or E || H
            //but we need orts
            min_ort_ind = 0;
            min_ort_val = fabs(nz[0]);
            if(min_ort_val > fabs(nz[1])){
             min_ort_ind = 1;
             min_ort_val = fabs(nz[1]);
	    }
            if(min_ort_val > fabs(nz[2])){
             min_ort_ind = 2;
             min_ort_val = fabs(nz[2]);
	    }
            if(min_ort_ind == 0){
	      nx[0] =  0.;
	      nx[1] = -nz[2];
	      nx[2] =  nz[1]; 
	    }
            if(min_ort_ind == 1){
              nx[0] =  nz[2];
	      nx[1] =  0.;
	      nx[2] = -nz[0];
	    }
            if(min_ort_ind == 2){
              nx[0] = -nz[1];
	      nx[1] =  nz[0];
	      nx[2] =  0.;
	    } 
	    nx_0 = sqrt(nx[0]*nx[0]+nx[1]*nx[1]+nx[2]*nx[2]);
	  }
	  nx[0] = nx[0]/nx_0;
	  nx[1] = nx[1]/nx_0;
	  nx[2] = nx[2]/nx_0; 
	  nx_0 = 1.0;
	  ny[0] = nz[1]*nx[2]-nz[2]*nx[1];
	  ny[1] = nz[2]*nx[0]-nz[0]*nx[2];
	  ny[2] = nz[0]*nx[1]-nz[1]*nx[0];
	  ny_0  = sqrt(ny[0]*ny[0]+ny[1]*ny[1]+ny[2]*ny[2]);
	  ny[0] = ny[0]/ny_0;
	  ny[1] = ny[1]/ny_0;
	  ny[2] = ny[2]/ny_0;
	  ny_0  = 1.0;

	  //we have got orts, and can do physics
	  beta_x_0 = vx*nx[0]+vy*nx[1]+vz*nx[2];
	  beta_y_0 = vx*ny[0]+vy*ny[1]+vz*ny[2];
	  beta_z_0 = vx*nz[0]+vy*nz[1]+vz*nz[2];
	  gamma_0 = 1/sqrt(1-(beta_x_0*beta_x_0 + beta_y_0+beta_y_0 + beta_z_0*beta_z_0));
	  e_x_0 = e_x*nx[0]+e_y*nx[1]+e_z*nx[2];
	  e_y_0 = e_x*ny[0]+e_y*ny[1]+e_z*ny[2];
	  e_z_0 = e_x*nz[0]+e_y*nz[1]+e_z*nz[2];
	  beta_dot_E = beta_y_0*e_y_0 + beta_z_0*e_z_0;
	  //Ey_over_H = (e_x*ny[0]+e_y*ny[1]+e_z*ny[2])/fieldH;

	  xfac = (e_y_0 - beta_y_0*beta_dot_E)/fieldH;
	  yfac = beta_x_0*beta_dot_E/fieldH;
	  zfac = (e_z_0 - beta_z_0*beta_dot_E)/fieldH;
          //rad = sqrt((beta_x_0-Ey_over_H)*(beta_x_0-Ey_over_H) + beta_y_0*beta_y_0);
	  rad = sqrt((beta_x_0 - xfac)*(beta_x_0 - xfac) 
		     + (beta_y_0 - yfac)*(beta_y_0 - yfac));
          w = fieldH*classicalR*charge;
          if(rad > 0.){
	    cos_phi_0 =  (beta_x_0 - xfac)/rad;
	    sin_phi_0 = -(beta_y_0 - yfac)/rad;
	    //cos_phi_0 =  (beta_x_0 - Ey_over_H)/rad;
	    //sin_phi_0 = -beta_y_0/rad;
	  }
	  else{
            cos_phi_0 = 0.0;
            sin_phi_0 = 0.0;
	  }
       
	  cos_phi   = cos(w*ctStep);
	  sin_phi   = sin(w*ctStep);
	  //Get new betas, subtract of initial betas to get delta beta.
	  beta_x =  rad*(cos_phi*cos_phi_0 - sin_phi*sin_phi_0) + xfac - beta_x_0;
	  beta_y = -rad*(sin_phi*cos_phi_0 + cos_phi*sin_phi_0) + yfac - beta_y_0;  
	  beta_z = classicalR/gamma_0*charge*zfac*ctStep;
	  //Transfer to original coordinate system.
	  d_vx = beta_x*nx[0]+beta_y*ny[0]+beta_z*nz[0];
	  d_vy = beta_x*nx[1]+beta_y*ny[1]+beta_z*nz[1];
	  d_vz = beta_x*nx[2]+beta_y*ny[2]+beta_z*nz[2];
	  //Recalculate factors with new betas
	  beta_dot_E = beta_y*e_y_0 + beta_z*e_z_0;
	  xfac = (e_y_0 - beta_y*beta_dot_E)/fieldH;
	  yfac = beta_x*beta_dot_E/fieldH;
	  zfac = (e_z_0 - beta_z*beta_dot_E)/fieldH;
	  //Calculate shift in position
	  shift_x = (rad/w)*(sin_phi*cos_phi_0 + cos_phi*sin_phi_0 - sin_phi_0)+xfac*ctStep;
	  shift_y = (rad/w)*(cos_phi*cos_phi_0 - sin_phi*sin_phi_0 - cos_phi_0)+yfac*ctStep;
	  shift_z = (beta_z_0 + 0.5*beta_z)*ctStep;
	  //Transfer to original coord system
	  d_x = shift_x*nx[0]+shift_y*ny[0]+shift_z*nz[0];
	  d_y = shift_x*nx[1]+shift_y*ny[1]+shift_z*nz[1];
	  d_z = shift_x*nx[2]+shift_y*ny[2]+shift_z*nz[2];
	}
	else{
	  if(fieldE > 0.){
	    //only electric field
	    d_vx = classicalR/gamma_0*charge*e_x*ctStep;
	    d_vy = classicalR/gamma_0*charge*e_y*ctStep;
	    d_vz = classicalR/gamma_0*charge*e_z*ctStep;
	    d_x  = (vx + 0.5*d_vx)*ctStep;
	    d_y  = (vy + 0.5*d_vy)*ctStep;
	    d_z  = (vz + 0.5*d_vz)*ctStep;        
	  }
	  else{
	    d_vx = 0.;        d_vy = 0.;        d_vz = 0.;
	    d_x  = vx*ctStep; d_y  = vy*ctStep; d_z  = vz*ctStep;
	  }
	}
      }
      vx = vx+d_vx;
      vy = vy+d_vy;
      vz = vz+d_vz;
      x = x+d_x;
      y = y+d_y;
      z = z+d_z;
      eb->px(ind) = vx*mass;      
      eb->py(ind) = vy*mass;      
      eb->pz(ind) = vz*mass;  
      eb->x(ind) = x;
      eb->y(ind) = y;
      eb->z(ind) = z;
     

      //Right now, lets just add a fixed circ. aperature, for testing purposes.
      double position = 0;
      double radius = 100.0;
      position = sqrt((eb->x(ind))*(eb->x(ind)) + (eb->y(ind))*(eb->y(ind)));
	if(position > radius){
	  cout<<"Ok, lost it at x="<<eb->x(ind)<<" y="<<eb->y(ind)<<" and z="<<eb->z(ind)<<"\n";
	  eb->deleteParticle(ind);
	  break;
	}
      //moving particle    ============stop===========
    }
  //-----------------------------------
  //end of loop upon macro-particles
  //-----------------------------------


  }

}
*/



void eBeamTracker::moveParticlesWeakH(double& ctime, int& nstep, eBunch*& eb, 
				      double& T, double& xfinal, 
				      double& yfinal,
				      double& zfinal, double& pxfinal, 
				      double& pyfinal, double& pzfinal)
{
  
  //variables declaration
  int nPart = eb->getSize();
  int nStep = 1;
  int condition_changed = 0;
  int ebeam_inside = 0;
  double ctStep = 0.;

  //mass in MeV
  double mass = eb->getMass();
  double charge = eb->getCharge();
  double classicalR = eb->getClassicalRadius();
  double E;

  //velocity in a.u v/c=beta
  double vx   = 0., vy   = 0., vz   = 0.;
  double d_vx = 0., d_vy = 0., d_vz = 0.;

  //coordinates of the particle
  double x = 0., y = 0., z = 0.;
  double d_x =0., d_y =0., d_z =0.;

  //fields
  double fieldE = 0., e_x = 0., e_y = 0., e_z = 0.;
  double fieldH = 0., h_x = 0., h_y = 0., h_z = 0.;

  //auxilary variables
  double beta_x_0, beta_y_0, beta_z_0;
  double gamma;
  double beta_dot_E;
  double beta_x, beta_y, beta_z;
  double position = 0;
  double radius = 100.0;
  nStep = nstep;
  ctStep = ctime/nStep;
  gamma = T/mass + 1;

  //start of loop upon macro-particles
  for(int ind = 0; ind < nPart; ind++){
    vx = eb->px(ind)/mass/gamma; //this is really intended as beta, not v
    vy = eb->py(ind)/mass/gamma;
    vz = eb->pz(ind)/mass/gamma;
    x = eb->x(ind);
    y = eb->y(ind);
    z = eb->z(ind);

    cout<<"Initial: "<<x<<" "<<y<<" "<<z<<" "<<vx<<" "<<vy<<" "<<vz<<"\n";
    //moving particle    ============start==========
    for(int i_step = 0; i_step < nStep; i_step++){
    // while(condition_changed < 2){
      d_vx = 0.; d_vy = 0.; d_vz = 0.;
      d_x  = 0.; d_y  = 0.; d_z  = 0.;

      position = sqrt((eb->x(ind))*(eb->x(ind)) + (eb->y(ind))*(eb->y(ind)));
      if(position < radius){
	ebeam_inside = 1;
      }
      else{
	ebeam_inside = 0;
      }
      getElectricField(x,y,z,ebeam_inside,e_x,e_y,e_z); //Field (units of 1/mm)
      getMagneticField(x,y,z,ebeam_inside,h_x,h_y,h_z);
      
      gamma = 1/sqrt(1-(vx*vx + vy*vy + vz*vz));

      beta_dot_E = (vx*e_x + vy*e_y + vz*e_z);
      
      d_vx = charge*classicalR/gamma
	*(e_x - vx*beta_dot_E + vy*h_z - vz*h_y)*ctStep;
      d_vy = charge*classicalR/gamma
	*(e_y - vy*beta_dot_E + vz*h_x - vx*h_z)*ctStep;
      d_vz = charge*classicalR/gamma
	*(e_z - vz*beta_dot_E + vx*h_z - vz*h_x)*ctStep;

      vx = vx+d_vx;
      vy = vy+d_vy;
      vz = vz+d_vz;
      d_x  = vx*ctStep;
      d_y  = vy*ctStep; 
      d_z  = vz*ctStep;

      x = x+d_x;
      y = y+d_y;
      z = z+d_z;
      //cout<<x<<" "<<y<<" "<<z<<" "<<vx<<" "<<vy<<" "<<vz<<"\n";
      eb->px(ind) = vx*mass*gamma;      
      eb->py(ind) = vy*mass*gamma;      
      eb->pz(ind) = vz*mass*gamma;  
      eb->x(ind) = x;
      eb->y(ind) = y;
      eb->z(ind) = z;
           
      //Test to see if we are outside the beam pipe.
      position = sqrt((eb->x(ind))*(eb->x(ind)) + (eb->y(ind))*(eb->y(ind)));
      if(ebeam_inside == 0){
	if(position < radius){
	  ebeam_inside = 1;
	  condition_changed++;
	}
      }
      else{
	if(position > radius){
	    ebeam_inside = 0;
	    condition_changed++;
       
	}
      }
      if(condition_changed >= 2){
	cout<<"Ok, lost it at x="<<eb->x(ind)<<" y="<<eb->y(ind)<<" and z="
	  <<eb->z(ind)<<"\n";
	xfinal = eb->x(ind); 
	yfinal = eb->y(ind); 
	zfinal = eb->z(ind);
	pxfinal = eb->px(ind);
	pyfinal = eb->py(ind);
	pzfinal = eb->pz(ind);
	eb->deleteParticle(ind);
	break;
      }
      //moving particle    ============stop===========
      
    }
	xfinal = eb->x(ind); 
	yfinal = eb->y(ind); 
	zfinal = eb->z(ind);
	pxfinal = eb->px(ind);
	pyfinal = eb->py(ind);
	pzfinal = eb->pz(ind);
  //-----------------------------------
  //end of loop upon macro-particles
  //-----------------------------------

  }

}


//returns the H in Gauss/(electron charge in CGS)
void eBeamTracker::getMagneticField(double x, double y,double z, int inside, 
                                    double& h_x, double& h_y, double& h_z)
{
  h_x_sum = 0.; h_y_sum = 0.; h_z_sum = 0.;
  for(i_s = 0; i_s < nMgSources; i_s++){
    magneticSources[i_s]->getMagneticField(x,y,z,h_x_tmp,h_y_tmp,h_z_tmp);
    h_x_sum += h_x_tmp; h_y_sum += h_y_tmp; h_z_sum += h_z_tmp; 
  } 
  if(inside == 1){
    h_x = h_x_sum; h_y = h_y_sum; h_z = h_z_sum; 
  }
  else{
    h_x = 0; h_y = 0; h_z = 0;
  } 
}

//returns the electric field (for point like charge 1/(r*r))
void eBeamTracker::getElectricField(double x, double y, double z, int inside, 
                                    double& e_x, double& e_y, double& e_z)
{
  e_x_sum = 0.; e_y_sum = 0.; e_z_sum = 0.;
  for(i_s = 0; i_s < nElSources; i_s++){
    electricSources[i_s]->getElectricField(x,y,z,e_x_tmp,e_y_tmp,e_z_tmp);
    e_x_sum += e_x_tmp; e_y_sum += e_y_tmp; e_z_sum += e_z_tmp; 
  }
  if(inside==1){
    e_x = e_x_sum; e_y = e_y_sum; e_z = e_z_sum; 
  }
  else{
    e_x = 0; e_y = 0; e_z = 0;
  }
}
