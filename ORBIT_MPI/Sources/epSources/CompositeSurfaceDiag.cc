//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    CompositeSurfaceDiag.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/22/2003
//
// DESCRIPTION
//   It is a composite class for abstractSurfceDiag class. It registers 
//   the surface diagnostics instances and delegates the analysis to them.
//   The methods print(...) were implemented just for case. In general, 
//   each diagnostics should manages its own print method.
//   This method override  analyze() method of the abstract class and 
//   uses the first input parameter of this method (ctimeStep) to calculate
//   total time after start of the new turn and provide it as input parameter
//   for surface diagnostics instances.
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "epModuleConstants.hh"
#include "CompositeSurfaceDiag.hh"

CompositeSurfaceDiag::CompositeSurfaceDiag() : 
        AbstractSurfaceDiag("COMPOSITE SURFACE DIAGNOSTICS")
{
  nComps = 0;
  nCompsMax = 10;
  nTurns = 0;
  ctimeTotal = 0.;
  components = new AbstractSurfaceDiag*[nCompsMax];
}

CompositeSurfaceDiag::~CompositeSurfaceDiag()
{
  for(int i = 0; i < nComps; i++){
    delete components[i];
  }
  delete [] components;
}

void CompositeSurfaceDiag::init(){
  ctimeTotal = 0.;
  for(int i = 0; i < nComps; i++){
    components[i]->init();
  }
}

void CompositeSurfaceDiag::setZeroTurn()
{
  nTurns = 0;
  ctimeTotal = 0.;
}

void CompositeSurfaceDiag::startNewTurn(){
  nTurns++;
  ctimeTotal = 0.;
  for(int i = 0; i < nComps; i++){
    components[i]->startNewTurn();
  }
}

void CompositeSurfaceDiag::accountTime(double ctimeStep)
{
  ctimeTotal += ctimeStep;
  for(int i = 0; i < nComps; i++){
    components[i]->accountTime(ctimeTotal);
  }
}

void CompositeSurfaceDiag::analyze(int indHit, int nOut, eBunch* eb, double* n_v){
  for(int i = 0; i < nComps; i++){
    components[i]->analyze(indHit,nOut,eb,n_v);
  }
}

void CompositeSurfaceDiag::print(ostream& Out){
  for(int i = 0; i < nComps; i++){
    components[i]->print(Out);
  }
}

void CompositeSurfaceDiag::print(char* fileName){
  ofstream F_dump;
  if(rank_MPI == 0)F_dump.open (fileName, ios::out);
  if(rank_MPI == 0)F_dump<<"====="<<getName()<<"======="<<std::endl;
  if(rank_MPI == 0)F_dump<<"turn #   = "<< nTurns <<std::endl;
  if(rank_MPI == 0)F_dump<<"time [ns]= "<< ctimeTotal*1.0e+9/epModuleConstants::c <<std::endl;
  for(int i = 0; i < nComps; i++){
    if(rank_MPI == 0) {
      F_dump <<"= "<<components[i]->getName()
	     <<" ="<<std::endl;
    }
    components[i]->print(F_dump);
  }  
  if(rank_MPI == 0){F_dump.close();}
}

void CompositeSurfaceDiag::print(){
  print(std::cout);
}

void CompositeSurfaceDiag::addDiagnostics(AbstractSurfaceDiag* diag)
{
  if(nComps == nCompsMax){
    AbstractSurfaceDiag** tmp_comps = new AbstractSurfaceDiag*[nCompsMax+1];
    for(int i = 0; i < nComps; i++){
      tmp_comps[i] = components[i];
    }
    delete [] components;
    components = new AbstractSurfaceDiag*[nCompsMax+1];
    for(int i = 0; i < nComps; i++){
      components[i] = tmp_comps[i];
    }
    delete [] tmp_comps;
    nCompsMax++;
  }
  
  components[nComps] = diag;
  nComps++;
}

int CompositeSurfaceDiag::getNumberOfDiagnostics(){ return nComps;}

AbstractSurfaceDiag* CompositeSurfaceDiag::getDiagnostics(int index)
{
  if(index < nComps && index >= 0){
    return components[index];
  }
  return NULL;
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
