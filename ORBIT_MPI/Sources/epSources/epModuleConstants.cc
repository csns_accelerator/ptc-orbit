//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    epModuleConstants.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    06/20/2003
//
// DESCRIPTION
//    Keeps all physical constants for ep Module
//
///////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "epModuleConstants.hh"

const double epModuleConstants::PI = 3.14159265358979323;
const double epModuleConstants::c = 2.99792458e+11;
const double epModuleConstants::elementary_charge_CGS = 4.8032068e-10;
const double epModuleConstants::coeff_Tesla_to_inner = (1.0e+4/4.8032068e-10)*0.01;
const double epModuleConstants::coeff_VoltsPerM_to_inner = 1.0/((1.6021773e-19)*(8.98755179e+9)*1.0e+6);
const double epModuleConstants::coeff_Phi_to_Volts = (1.6021773e-19)*(8.98755179e+9)*1000.;
const double epModuleConstants::mass_electron = 0.51099906;
const double epModuleConstants::classicalRadius_electron = 2.8179409e-12;
const double epModuleConstants::charge_electron = -1.0; 
const double epModuleConstants::mass_proton = 938.2723;
const double epModuleConstants::classicalRadius_proton = 1.534698e-15;
const double epModuleConstants::charge_proton = +1.0;

epModuleConstants::epModuleConstants()
{
}

epModuleConstants::~epModuleConstants()
{
}

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
