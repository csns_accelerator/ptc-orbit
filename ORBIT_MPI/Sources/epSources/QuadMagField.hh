//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    QuadMagField.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/06/2005
//
// DESCRIPTION
//   The source of quadrupole magnetic fields  B=(b*y, b*x, 0): b=gradient 
//
//   In ECloud.mod there is the related method to add a quadrupole magnetic field 
//   to the EP_Node. Magnetic field gradient in Tesla/mm.
//   ECloudAddQuadMagnField(const Integer& nodeIndex, 
//                          const Real& zMin, const Real& zMax, 
//   		            const Real& quad_mag_gradient)
//      
//   In futer we are going to add the fringe field also
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////
#include "BaseFieldSource.hh"

#ifndef QUADMAG_FIELD_HH
#define QUADMAG_FIELD_HH

class QuadMagField : public BaseFieldSource
{
public:

  QuadMagField();
  virtual ~QuadMagField();

  //set magnetic field gradient in [Tesla/mm]
  void setMagneticField(double H1_in);

  //set the spatial limits
  void setX_MinMax(double xMin_in, double xMax_in);
  void setY_MinMax(double yMin_in, double yMax_in);
  void setZ_MinMax(double zMin_in, double zMax_in);

  double getQuadrupoleMagneticGradient(); // [Gauss/mm/(electron charge in CGS)]

  //--------------------------------------------------------
  //overridden methods
  //---------------------------------------------------------

  //returns the H in Gauss/(electron charge in CGS)
  void getMagneticField(double x,    double y,    double z, 
                        double& comp_x, double& comp_y, double& comp_z);

protected:
  double QuadMagGradient;  //unit [Gauss/mm/(electron charge in CGS)]

  double xMin,xMax; //unit [mm]
  double yMin,yMax;
  double zMin,zMax;

};
#endif


