//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    Grid3D.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    05/19/2003
//
// DESCRIPTION
//    Specification and inline functions for a class used 
//    to arrange 3D array for 3D grid, 
//    to operate the arrays with MPI and 
//    to describe the motion of macroparticles in the grid 
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include <iostream> 
#include <cstdlib>

using namespace std;

#ifndef GRID3D_HH
#define GRID3D_HH

//MPI staff
#include "mpi.h"

#include "EP_Boundary.hh"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    Grid3D
//
// CLASS RELATIONSHIPS
//    class Grid3D, Boundary, eBunch, Surface
//
// DESCRIPTION
//
// PUBLIC METHODS
// -TO SET ARRAY
//    Grid3D(int,int,int);    Constructor for Grid objects 
//                            (the number of grids = nZ*nX*nY)
//    ~Grid3D();              Destructor for the Grid3D class
//    getArr3D();             returns Arr3D
//    getSlice2D(int);        returns Arr3D[zInd]
//    getBinsX();getBinsY();getBinsZ();
//                            return size of bins in x,y,z
//
// -TO OPERATE ARRAYS
//    clean();                clean the array - set all elements to zero
//    sum(Grid3D*);           sums a new grid to this one
//    sum(Grid3D*,Grid3D*);   sums two grids
//    allReduce();            sums the 3D array trough all CPUs
//
// -TO OPERATE A MACROPARTICLE
//    setMinMaxXYZ(double,double,double,double,double,double);
//                            sets Min and Max range in x,y,z and 
//                            calculates dx_,dy_,dz_
//    getStepX();getStepY();getStepZ();
//                            return the steps dx_,dy_,dz_
//    getGridIndAndFrac(double,int&,double&, double,int&,double&,
//                      double,int&,double&);
//                            gets the index and fraction of (x,y,z)
//    binParticleIntoArr3D(double, int,double, int,double, int,double);
//                            bins a macroparticle into Arr3D
//    calcGradient(int,double,double&, int,double,double&, 
//                 int,double,double&);
//                            calculates gradient of Arr3D
//
// PRIVATE METHODS
//    calcSheetGradient(int,int,int,double,double,double,double,double,double);
//                            calculates gradient from each z-sheet
//    FinalizeExecution();    finalizes MPI if it has been initialized and 
//                            exits
//
// PRIVATE MEMBERS
//    Arr3D;           holds set of double on each 3D grid points  
//    nZ_,nX_,nY_;     size of bins in x,y,z
//    xGridMin_,xGridMax_,yGridMin_,yGridMax_,zGridMin_,zGridMax_;
//                     the range of 3D Grid
//    dx_,dy_,dz_;     the grid steps in x,y,z
//
///////////////////////////////////////////////////////////////////////////

class Grid3D
{
public:
  //--------------------------------------
  //the public methods of the Grid3D class
  //--------------------------------------
  
  Grid3D(int nZ, EP_Boundary* boundaryIn);
  virtual ~Grid3D();
  double*** getArr3D();
  double**  getSlice2D(int zInd);

  void setMinMaxZ(double zGridMin,double zGridMax);

  int getBinsX();
  int getBinsY();
  int getBinsZ();

  double getStepX();
  double getStepY();
  double getStepZ();

  double getxGridMax();
  double getxGridMin();
  double getyGridMax();
  double getyGridMin();
  double getzGridMax();
  double getzGridMin();

  double getGridValueX(int ind);
  double getGridValueY(int ind);
  double getGridValueZ(int ind);

  //define all elements as 0.0
  void clean();

  //add grd Grid3D to this Grid3D
  void sum(Grid3D* grd);

  //replace this Grid3D by sum of grd1 and grd2 
  void sum(Grid3D* grd1, Grid3D* grd2);

  //multiply all elements of Grid3D by constant coefficient
  void multiply(double coeff);

  //multiply all elements of the z-slice with index ind
  //by constant coefficient
  void multiply(int ind_z, double coeff);

  //set values from current grid to the external - grd
  //z_shift - shift between the zero-z of current grid
  //and external's one
  void setValuesForExternalGrid(double z_shift, Grid3D* grd);

  //set values at the grid points of the ind_z-th slice
  //of the the Grid3D - grd
  void setValuesForSlice(double z, int ind_z, Grid3D* grd);

  //MPI related method
  void allReduce();

  //MPI method to produce conformed parallel ring of matrices with left (right)
  //slice equals of the second slice from the right (left)
  //This method is used in parallel 3D space charge calculations
  void makeRing();

  //This method does makeRing() and does periodic conditions
  //these conditions necessary for coasting beam
  void makePeriodicRing();

  void broadcast(int rank);
  void broadcast(int rank,int iSlice);

  //returns the boundary instance of this Grid3D
  EP_Boundary* getBoundary();

  void getGridIndAndFrac(double x, int& xInd, double& xFrac,
			 double y, int& yInd, double& yFrac,
			 double z, int& zInd, double& zFrac);

  void binParticleIntoArr3D(double macroSize, double x, double y, double z);

  void binParticleIntoArr3D(double macroSize, 
			    int xInd,double xFrac,
			    int yInd,double yFrac,
			    int zInd,double zFrac);

  void calcGradient(double x,double& gradX,
	       	    double y,double& gradY,
		    double z,double& gradZ);

  void calcGradient(int xInd,double xFrac,double& gradX,
		    int yInd,double yFrac,double& gradY,
		    int zInd,double zFrac,double& gradZ);

  void calcGradient(int xInd, double& gradX,
		    int yInd, double& gradY,
		    int zInd, double& gradZ);

  double calcValue(double x,double y,double z);

  double calcValue(int iX, int iY, int iZ,
		   double fracX,double fracY,double fracZ); 


  //returns the sum of all grid points
  double getSum();

  //returns the sum of all grid points in the slice with index iZ
  double getSliceSum(int iZ);

  //returns the sum of all grid points in the slice with position z
  double getSliceSum(double z);  

protected:
  //---------------------------------------
  //the private methods of the Grid3D class
  //---------------------------------------

  double calcValueOnX(int iX, int iY, int iZ, 
                      double Wxm,double Wx0,double Wxp);

  double calcValueOnY(int iX, int iY, int iZ, 
                      double Wym,double Wy0,double Wyp);

  double calcSheetGradient(int iZ,int iX,int iY,
			   double xm,double x0,double xp,
			   double ym,double y0,double yp);

  void FinalizeExecution(); 

protected:
  //---------------------------------------
  //the private members of the Grid3D class
  //---------------------------------------

  double*** Arr3D;

  EP_Boundary* boundary;

  int nZ_,nX_,nY_;
  double zGridMin_,zGridMax_;
  double dx_, dy_, dz_;

  int rank_MPI;
  int size_MPI;
  int iMPIini;

private:
  //---------------------------------------
  //the static members
  //---------------------------------------
  static double* buffMPI_out;
  static double* buffMPI_in;

  static int nBuffSize_Inner;

};
#endif
///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////
