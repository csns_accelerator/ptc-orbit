//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    UniformField.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/25/2004
//
// DESCRIPTION
//   The source of the electrostatic and magnetic uniform fields
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <cstdio>
#include <cmath>

#include "UniformField.hh"
#include "epModuleConstants.hh"

UniformField::UniformField():
  BaseFieldSource() 
{
  EX = 0.; EY = 0.; EZ = 0.;
  COMP_X = 0.; COMP_Y = 0.; COMP_Z = 0.;

  xMin = - 1.0e+307; xMax = + 1.0e+307;
  yMin = - 1.0e+307; yMax = + 1.0e+307;
  zMin = - 1.0e+307; zMax = + 1.0e+307;

}

UniformField::~UniformField()
{
}

//set components of electric field in Volt/m
void UniformField::setElectricField(double EX_in, double EY_in, double EZ_in)
{
  double coeff = epModuleConstants::coeff_VoltsPerM_to_inner;
  EX = EX_in*coeff;
  EY = EY_in*coeff;
  EZ = EZ_in*coeff;
}

//set components of magnetic field in Tesla
void UniformField::setMagneticField(double COMP_X_in, double COMP_Y_in, double COMP_Z_in)
{
  double coeff = epModuleConstants::coeff_Tesla_to_inner;
  COMP_X = COMP_X_in * coeff;
  COMP_Y = COMP_Y_in * coeff;
  COMP_Z = COMP_Z_in * coeff;
}

double UniformField::getAbsElectricField()
{
  return sqrt(EX*EX+EY*EY+EZ*EZ);
}

double UniformField::getAbsMagneticField()
{
  return sqrt(COMP_X*COMP_X+COMP_Y*COMP_Y+COMP_Z*COMP_Z);
}

void UniformField::setX_MinMax(double xMin_in, double xMax_in)
{
  xMin = xMin_in; xMax = xMax_in;
}

void UniformField::setY_MinMax(double yMin_in, double yMax_in)
{
  yMin = yMin_in; yMax = yMax_in;
}

void UniformField::setZ_MinMax(double zMin_in, double zMax_in)
{
  zMin = zMin_in; zMax = zMax_in;
}

//returns the H in Gauss/(electron charge in CGS)
void UniformField::getMagneticField(double x,    double y,    double z, 
                                    double& comp_x, double& comp_y, double& comp_z)
{
  if( x > xMin && x < xMax && 
      y > yMin && y < yMax && 
      z > zMin && z < zMax){
    comp_x = COMP_X; comp_y = COMP_Y; comp_z = COMP_Z;
  }
  else{
    comp_x = 0.; comp_y = 0.; comp_z = 0.;
  }
}

//returns the electric field (for point like charge 1/(r*r))
void UniformField::getElectricField(double x,    double y,    double z, 
                                    double& e_x, double& e_y, double& e_z)
{
  if( x > xMin && x < xMax && 
      y > yMin && y < yMax && 
      z > zMin && z < zMax){
    e_x = EX; e_y = EY; e_z = EZ;
  }
  else{
    e_x = 0.; e_y = 0.; e_z = 0.;
  }
}

