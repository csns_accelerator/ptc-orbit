//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    eCloudFieldSource.cc
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    07/02/2003
//
// DESCRIPTION
//   The source of the electrostatic and magnetic fields. 
//   originated from the electron cloud. Keeps auxiliary Grid3Ds
//   for potential and space charge density.
//
//   The subclass of the BaseFieldSource
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <cstdio>

using namespace std;

#include "eCloudFieldSource.hh"

eCloudFieldSource::eCloudFieldSource(int nZ,
                                     double zGridMinIn,double zGridMaxIn, 
                                     EP_Boundary* boundaryIn):
                   BaseFieldSource() 
{
  boundary_ = boundaryIn;
  nZ_ = nZ;
  zGridMin_ = zGridMinIn;
  zGridMax_ = zGridMaxIn;
  
  rhoGridLocal_ = new RhoGrid3D(nZ_,boundary_);
  rhoGridLocal_->setMinMaxZ(zGridMin_,zGridMax_); 
  rhoGridLocal_->clean();

  phiGridLocal_ = new PhiGrid3D(nZ_,boundary_);
  phiGridLocal_->setMinMaxZ(zGridMin_,zGridMax_); 
  phiGridLocal_->clean();
}

eCloudFieldSource::~eCloudFieldSource()
{
  delete phiGridLocal_;
  delete rhoGridLocal_;
}

void eCloudFieldSource::createPotential(eBunch* eb)
{
  rhoGridLocal_->binParticles(eb);
  rhoGridLocal_->findPotential(phiGridLocal_);
}

void eCloudFieldSource::addBoundaryPotential()
{
  phiGridLocal_->addBoundaryPotential();
}

Grid3D* eCloudFieldSource::getRhoGrid3D(){return rhoGridLocal_;}

Grid3D* eCloudFieldSource::getPhiGrid3D(){return phiGridLocal_;}

void eCloudFieldSource::setMinMaxZ(double zGridMinIn,double zGridMaxIn)
{
  zGridMin_ = zGridMinIn;
  zGridMax_ = zGridMaxIn; 
  rhoGridLocal_->setMinMaxZ(zGridMin_,zGridMax_); 
  phiGridLocal_->setMinMaxZ(zGridMin_,zGridMax_);
}

//returns the electric field (for point like charge 1/(r*r))
void eCloudFieldSource::getElectricField(double x,    double y,    double z, 
                                           double& e_x, double& e_y, double& e_z)
{
  phiGridLocal_->calcGradient(x,e_x,y,e_y,z,e_z);
  e_x = - e_x; e_y = - e_y; e_z = - e_z;
}

