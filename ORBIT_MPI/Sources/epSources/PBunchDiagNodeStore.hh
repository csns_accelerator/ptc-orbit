/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   PBunchDiagNodeStore.hh
//
// AUTHORS
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/29/2004
//
// DESCRIPTION
//    PBunchDiagNodeStore class - singleton to keep references 
//    to the PBunchDiagNode instances.
//
///////////////////////////////////////////////////////////////////////////// 
#ifndef PBUNCH_DIAGNOSTIC_NODE_STORE_H
#define PBUNCH_DIAGNOSTIC_NODE_STORE_H
/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "PBunchDiagNode.hh"

class PBunchDiagNodeStore
{
 public:

  static PBunchDiagNodeStore* getPBunchDiagNodeStore();

  int addPBunchDiagNode(PBunchDiagNode* epN);

  PBunchDiagNode* getPBunchDiagNode(int index);

  int getSize();

  ~PBunchDiagNodeStore();

 protected:

  //constructor
  PBunchDiagNodeStore();

 private:

  static  PBunchDiagNodeStore* pbdStore;

  int nPBunchDiagNodes;
  PBunchDiagNode** pbdNodes;
};


///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif   // PBUNCH_DIAGNOSTIC_NODE_STORE_H


