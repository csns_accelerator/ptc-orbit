/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   EP_Node.hh
//
// AUTHORS
//    Y. Sato, A. Shishlo
//
// CREATED
//    01/21/2004
//
// DESCRIPTION
//    EP_Node class
//
///////////////////////////////////////////////////////////////////////////// 
#ifndef EP_NODE_H
#define EP_NODE_H


/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Node.h"

#include "EP_NodeCalculator.hh"
#include "CntrlSecEmSurface.hh"
#include "BaseFieldSource.hh"
#include "SimpleEBunchDiag.hh"
#include "SimpleSurfaceDiag.hh"

class EP_Node : public Node {
  Declare_Standard_Members(EP_Node, Node);

public:

   EP_Node(String &n, Integer &order, Real &length_In);

  ~EP_Node();

  Void nameOut(String &wname);
  Void _updatePartAtNode(MacroPart &mp);
  Void _nodeCalculator(MacroPart &mp);

  //specific methods

  void resetSurface(baseSurface* srf);
  baseSurface* getSurface();

  void setElectronGenerationProbab(double surfProb_In, double volProb_In);
  void setElectronGenerationParams(int newMacroPerBunch_In,
				   double electronsPerProton_In,
				   double pLossRate_In);

  CompositeEBunchDiag* getEBunchDiag();
  CompositeSurfaceDiag* getSurfDiag();

  void addFieldSource(BaseFieldSource* bf);

  void clearEBunch();

  void readEBunch(char* fileName);

  void dumpEBunch(char* fileName);

  void setAddBoundaryPotentialsInfo(int addBoundaryPotentialsIn);

  //set the effective length coefficient 
  //it will affect on p-Bunch kick ( by default it is 1.0)
  void setEffLengthCoeff(double effectiveLengthCoeffIn);

  //profile functions array
  //i = 0 - p-bunch line density
  //i = 1 - e-bunch line density
  //i = 2 - x-centroid of the e-bunch
  //i = 3 - y-centroid of the e-bunch
  //i = 4 - size of the e-bunch
  //i = 5 - electron, hitting surface, current [A/m]
  //i = 6 - energy absorbed by surface [MeV/s/m]

  void startRecordingProfiles(int profileF_index);

  void dumpRecordingProfiles(int profileF_index, char* fileName);

  void stopRecordingProfiles(int profileF_index);

  eBunch* getEBunch();

protected: 

  Function* getDeathFunction();

protected:

  eBunch* eb;

  CompositeEBunchDiag* eBunchMainDiag;

  CompositeSurfaceDiag* surfMainDiag;

  baseSurface* surface;
  
  double length;

  BaseFieldSource** baseFileds;
  int nBaseFileds;

  //probabilities for surface and electron production
  double surfProb;
  double volProb;

  int newMacroPerBunch;
  double electronsPerProton;
  double pLossRate;


  //use boundary conditions for fields or not ( 1-yes 0-no)
  //default = 1 - yes
  int addBoundaryPotentials;

  //set the effective length coefficient 
  //it will affect on p-Bunch kick ( by default it is 1.0)
  double effectiveLengthCoeff;
 
  //profile functions array
  //i = 0 - p-bunch line density
  //i = 1 - e-bunch line density
  //i = 2 - x-centroid of the e-bunch
  //i = 3 - y-centroid of the e-bunch
  //i = 4 - size of the e-bunch
  //i = 5 - electron, hitting surface, current [A/m]
  //i = 6 - energy absorbed by surface [MeV/s/m]
  int nProfileF;
  Function** profileF_arr;  

};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif   // EP_NODE_H


