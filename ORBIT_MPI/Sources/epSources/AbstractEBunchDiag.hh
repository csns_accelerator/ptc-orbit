//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    AbstractEBunchDiag.hh
//
// AUTHOR
//    Y. Sato, A. Shishlo
//
// CREATED
//    12/22/2003
//
// DESCRIPTION
//   the abstract class for eBunch diagnostics. It is supposed to deal with
//   e-bunch characteristics, the heat production from e-bunch and energy
//   spectrum of the electrons hitting the wall should be handled by surface
//   diagnostics class. This is the am abstract-component class in a composite
//   pattern for e-bunch diagnostic structure.
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#ifndef ABSTRACT_EBUNCH_DIAGNISTICS_HH
#define ABSTRACT_EBUNCH_DIAGNISTICS_HH

#include <iostream> 
#include <fstream>
#include <cstring>

#include "eBunch.hh"

class AbstractEBunchDiag
{
public:

  AbstractEBunchDiag();
  AbstractEBunchDiag(char* diagNameIn);
  virtual ~AbstractEBunchDiag();

  void setName(char* newName);
  const char* getName();

  virtual void init(){};
  virtual void setZeroTurn(){};
  virtual void startNewTurn(){};
  virtual void analyze(int nTurn, double ctime, eBunch* eb){};
 
  virtual void print(std::ostream& Out){};
  virtual void print(char* fileName){};


protected:

  char* diagName;
  const static int NAME_LENGTH_MAX;


  //MPI stuff
  int iMPIini; 
  int rank_MPI; 
  int size_MPI;

};

#endif

