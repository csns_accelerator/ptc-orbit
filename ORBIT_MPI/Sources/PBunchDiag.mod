/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    PBunchDiag.mod
//
// AUTHOR
//   Y. Sato, A. Shishlo
//   
//
// CREATED
//   01/14/2004
//
// DESCRIPTION
//   Module descriptor file for the PBunchDiag module. This module contains
//   diagnostic methods for p-bunch diagnostics.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module PBunchDiag - "PBunchDiagnostic module."
                         - "Written by  Y.Sato, A.Shishlo"
{

Inherits Consts;

Errors
  badHerdNo             - "Sorry, you asked for an undefined macro Herd",
  badpBunchDiagType     - "Sorry, I do not know about this diagnostic type.";

public:

  Void ctor() 
                        - "Constructor for the PBunchDiag module."
                        - "Initializes build values.";

  Void dtor() 
                        - "Destructor for the PBunchDiag module."
                        - "Finalize the PBunchDiag module";

  Integer PBunchDiagMakeNode(const Integer& order,
                             const Integer&  zSlices,
                             const Integer&  nRecordsMax, 
                             const String& diagTypeName,
                             const String& fileName)

                        - "Creates pBunchDiag Node and returns its index.";


  Void PBunchDiagStart(const Integer& nodeIndex)
                       - "Starts recording for specific node with index nodeIndex.";

  Void PBunchDiagStop(const Integer& nodeIndex)
                       - "Stops recording for specific node with index nodeIndex.";

  Void PBunchDiagDump(const Integer& nodeIndex)
                       - "Dumps diagnostics for specific node with index nodeIndex.";

  Void PBunchDiagDumpMax(const Integer& nodeIndex)
                       - "Dumps maximal values for specific node with index nodeIndex.";

//-------------debugging methods-----------start-------------------------

  Void PBunchDiagPerform(const Integer& nodeIndex)
                       - "Performs diagnostics immediately and dump results.";
											 
  Real PBunchDiagGetBunchFactor(const Integer& nodeIndex)		
	                     - "Returns the bunch factor for longitudinal diagnostics.";

//-------------debugging methods-----------stop-------------------------

}

