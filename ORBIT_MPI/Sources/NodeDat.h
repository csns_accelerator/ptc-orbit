#if !defined(__NodeDat__)
#define __NodeDat__

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    NodeDat
//
// INHERITANCE RELATIONSHIPS
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for containing node information.
//
// PUBLIC MEMBERS
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//  None.
//
///////////////////////////////////////////////////////////////////////////

class NodeDat
{
  public:
  NodeDat();
  ~NodeDat(){};

  char _eltType[80];
  char _eltName[80];
  double _beta_X;
  double _beta_Y;
  double _alpha_X;
  double _alpha_Y;
  double _eta_x;
  double _etap_x;
  double _length;
};

#endif   // __NodeDat__
