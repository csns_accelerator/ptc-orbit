/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   SpaceCharge3D 
//
// AUTHOR
//   Slava Danilov, Jeff Holmes, John Galambos,  ORNL, vux@ornl.gov
//   
//
// CREATED
//   07/13/2001
//
// DESCRIPTION
//   Module descriptor file for the 3D SpaceCharge module. This module contains
//   information about 3D Space Charge.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module SpaceCharge3D - "SpaceCharge 3D Module."
                     - "Written by Slava Danilov and enhanced by Jeff Holmes"
{

Inherits Consts, Particles, Ring, TransMap, Injection, LSpaceCharge, Parallel;

Errors
  badSpaceCharge3D      - "Sorry, you asked for a non existent SpaceCharge3D node",
  bad3Dforce            - "This routine only applies to SpaceCharge3D force",
  badHerdNo             - "Sorry, you asked for an undefined macro Herd",
  tooManyFFTTImpedances - "Can only use one SpaceCharge3D node now",
  tooFewTransMaps       - "Sorry, you need more Transfer Matricies before you"
                        - "Add transverse space charge kicks";
public:

  Void ctor() 
                        - "Constructor for the SpaceCharge3D module."
                        - "Initializes build values.";

  Integer
    nSpaceCharge3D      - "Number of  Space Charge 3D Nodes in the Ring",
    nSpChpot3D          - "Number of  Space Charge Potentials Nodes in the Ring",
    _nXBins             - "Number of horizontal bins to use",
    _nYBins             - "Number of vertical bins to use",
    _nPhiBins           - "Number of longitudinal bins to use";


  Real    
    _radVChamber        - "Radius of the vacuum chamber(in mm)";
 
   

  Void addSpCh3D_BF(const String &n, const Integer &order,   
                    Integer &nXBins, Integer  &nYBins, Integer &nPhiBins,
                    Real &eps, Real &length, const Integer &nMacrosMin)
                        - "Routine to add an Space Charge 3D node";

  Void addSpCh3D_BFSet(Integer &nxb, Integer &nyb, Integer &nphi, Real &eps, 
                       const Integer &nMacrosMin)
                        - "Routine to build a set of 3DSCSet";

  Void addSpCh3D_FFT(const String &n, const Integer &o,
                     Integer &nXBins, Integer  &nYBins, Integer &nPhiBins,
                     Real &eps, Real &length,
                     const String &BPShape, Real &BP1,
                     Real &BP2, Real &BP3, Real &BP4,
                     Integer &BPPoints, Integer &BPModes,
                     Real &Gridfact,
                     const Integer &nMacrosMin)
                        - "Routine to add an FFT PIC Transverse Space"
                        - "Charge node with a conducting wall beam pipe";

  Void addSpCh3D_FFTSet(Integer &nxb, Integer &nyb, Integer &nphi, Real &eps,
                        const String &BPShape, Real &BP1,
                        Real &BP2, Real &BP3, Real &BP4,
                        Integer &BPPoints, Integer &BPModes,
                        Real &Gridfact,
                        const Integer &nMacrosMin)
                        - "Routine to build a set of PotentialTransSC"
                        - "nodes for each transfer matrix element present";

  Void showSpCh3D(const Integer &n, Ostream &os)
                        - "Routine to show the Space Charge 3D info";

  Void dumpPotential(const Integer &n, const Integer &nphi , Ostream &osharmonics)
                        - "Routine to dump the potential";

  Void dumpPhiCount(const Integer &n, RealVector &p)
                        - "Routine to dump phi Bin  information  to vector p";

}

