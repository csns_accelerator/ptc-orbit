/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   Accelerate.mod
//
// AUTHOR
//   John Galambos,  ORNL, jdg@ornl.gov
//   A. Luccio, J. Beebe-Wang, BNL
//
// CREATED
//   12/15/98
//   08/2006 Reviewed by Jeff Holmes
//   11/2008 Modified by Dean Adams 
// DESCRIPTION
//   Module descriptor file for the Accelerate module.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module Accelerate - "Accelerate Module."
                  - "Written by John Galambos, ORNL, jdg@ornl.gov"
                  - "Contact Jeff Holmes, ORNL, jzh@ornl.gov"
{

  Inherits Consts, Particles, Ring, Diagnostic, MathLib;

  public:

  Errors
    badUFPSolve    - "I can't find the unstable fixed point",
    badSynchPart1  - "I couldn't find a sync. part. phase for the given RF "
                   - "and dB/dt you gave",
    badRampedBFile - "Something wrong with the file you gave",
    badTimePoint   - "The time is outside the given data point range",
    badVoltageSize - "The vector size used for RF voltage/phase is bad",
    badAccelChoice - "AccelChoice != 0 with more than one accelerating cavity";

  Void ctor()
                   - "Constructor for the Accelerate  module."
                   - "Initializes build values.";
  Integer
    nRFCavities   - "Number of RF Cavities in the Ring";
  Integer
    nAccelerates   - "Number of Accelerates in the Ring";

  Void addRampedBAccel(const String &name, const Integer &o,
                       const Subroutine sub,
                       const Integer &nh, RealVector &rfv,
                       RealVector &hn, RealVector &rfp)
                   - "Routine to add a ramped B accelerating node ";
 
  Void addISISRampedBAccel(const String &name, const Integer &o,
                       const Subroutine sub,
                       const Integer &nh, RealVector &rfv,
                       RealVector &hn, RealVector &rfp)
                    - "Routine to add a ramped B accelerating node ";

  Void addAccelbyPhase(const String &name, const Integer &o,
                       const Subroutine sub, const String &table)
                   - "Routine to add a phase based accelerating node ";
  Void showAccelNodes(Ostream &os)
                   - "Routine to print acceleration nodes to a stream";
  Void showAccelerate(Ostream &os)
                   - "Routine to print acceleration info to a stream";

// Stuff for specifying ramp rates of B / V:

  Void getRampedBV(const String &fileName)
                   - "Routine to parse input for ramped B and V  vs. time";
  Void getRampedV(const String &fileName)
                   - "Routine to parse input for ramped Voltages vs. time";
  Void InterpolateBV()
                   - "Routine to use interpolation to provide ramped B "
                   - "acceleration parameters BSync, RFVolts and RFPhase";
  Void InterpolateV()
                   - "Routine to use interpolation to provide ramped B "
                   - "acceleration parameters RFVolts and RFPhase";

  Real
    BSync0         - "Fixed field component [T]",
    BSync1         - "Max. Ramped  field component [T]",
    BSyncFreq      - "Field ramping frequency during acceleration  [Hz]";

// Non-accelerating RF cavities:

  Real
     deltaFreq     - "Relative RF Frequency Shift";

  Void addRFCavity(const String &n, const Integer &o, Integer &nh,
                   RealVector &v, RealVector &hn, RealVector &p)
                   - "Routine to add an RF Cavity node with constant voltage";
  Void addBarrierCavity(const String &n, const Integer &o, Integer &nh,
                        RealVector &v, RealVector &hn, RealVector &p,
                        RealVector &dp)
                   - "Routine to add a Barrier Cavity node with constant voltage";
  Void addRampedRFCavity(const String &n, const Integer &o, Integer &nh,
                         RealVector &v, RealVector &hn, RealVector &p,
                         Subroutine sub)
                   - "Routine to add a time ramped RF Cavity node";
  Void constVolts()
                   - "Dummy routine used when not scaling volts(t)";

// Finding fixed points:

  Void getUFixedPoint()
                   - "Routine to find the unstable fixed point";
  Real uFixedPoint
                   - "The unstable fixed point for seperatrix [rad]";

// Ramped B stuff:

  Integer 
    nRFHarmonics   - "Number of RF Harmonics",
    nTimePoints    - "Number of time data points read in";
  RealMatrix
    RFVoltPoints   - "RF volts(harmonic, time) [kV]",
    RFPhasePoints  - "RF phase(harmonic, time) [deg]";
  RealVector
    BSynchPoints   - "Dipole field [T]",
    timePoints     - "The time points [msec]",
    RFVolts        - "RF volts for each harmonic [kV]",
    RFHarmNum      - "RF harmonic number for each harmonic",
    RFPhase        - "RF phases for each harmonic [rad]";
  Real
    bunchArea      - "The bunch area (V-sec)",
    syncFreq       - "The synchrotron frequency [Hz]",
    orbitFreq      - "The frequency about the ring [Hz]",
    BSynch         - "The B field that is used to scale the synchronous energy [T]",
    rhoBend        - "The bend radius of the ring dipoles, as in B-rho [m]",
    FixedInjPeriod - "Fixed injection Period (Period of RF) in mic secs ";
  Integer
    AccelChoice    - "0 -> rhoB determines synchronous energy and phase"
                   - "1 -> phase determines synchronous energy and rhoB",
    PhaseChoice    - "0 -> macroparticle phases relative to RF"
                   - "1 -> macroparticle phases relative to synchronous particle";
}
