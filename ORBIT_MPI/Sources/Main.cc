//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    Main.cc $Revision: 1.9 $
//
// AUTHOR
//    Scott Haney, LLNL, (510) 423-6308
//
// COPYRIGHT
//    Copyright (C) 1990-1996, The Regents of the University of California
//    and Lawrence Livermore National Laboratory.
//
// CREATED
//    March 20, 1991
//    Modified by J. Galambos to be the main for ORBIT
//    modules.
//
// DESCRIPTION
//    Simple main program for the ORBIT code
//    Add modules to start() as you develop them.
//
///////////////////////////////////////////////////////////////////////////

#include "ShellTool.h"
#include <errno.h>

extern String applVersionStr;

using namespace std;

class Track: public ShellTool {
  Declare_Standard_Members(Track, ShellTool);
public:
  Track(int argc, char **argv, unsigned int rdSize = 2048)
    : ShellTool(argc,argv,rdSize) { }
  ~Track() {cerr << "bye\n"; }
  virtual void start();
};

// Creates the member functions that adds other capabilities to Track
Define_Standard_Members(Track, ShellTool);

void Track::start() {

    applVersionStr="1.01";

  // Do Link_Module() on each module in the order you want the constructors
  // to be called.
    
  Link_Module(Particles);
  Link_Module(Ring);
  Link_Module(TransMap);
  Link_Module(Injection);
  Link_Module(Bump);  
  //  Link_Module(RFCavity);
  Link_Module(LSpaceCharge);  
  Link_Module(TSpaceCharge); 
  Link_Module(ThinLens);
  Link_Module(Diagnostic);
  Link_Module(Aperture);
  Link_Module(Accelerate);
  Link_Module(Plots);
  Link_Module(Output);
  Link_Module(Parallel);
  Link_Module(TransImpWF);
}

int main(int argc, char **argv)
{

  StartMain(argc, argv);

  Track sc(argc, argv);

  sc.run();

  EndMain();

  return 0;
}
