/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Particles.mod
//
// AUTHOR
//    John GalambosORNL, jdg@ornl.gov
//
// CREATED
//    10/2/97
//
//  DESCRIPTION
//    Module descriptor file for the Particles module
//
//  REVISION HISTORY
//    7/3/02 - Added setPartValsBase function
//
/////////////////////////////////////////////////////////////////////////////

module Particles - "Particles Module."
                 - "Written by John Galambos, ORNL, jdg@ornl.gov"
{

  Inherits Consts, MathLib, Parallel;
Errors
  badSyncPIndex   - "You've entered a bad Synchronous Particle identifier"
		  - "for a Macro Particle",
 badMacroPartNum  - "You asked for an undefined MacroParticle",
 badHerdNumber    - "Sorry, you used a bad herd ID here",
 tooManySyncPart  - "Sorry, I've already got a synchronous particle defined",
 tooManyBuckParts - "Sorry, I've already got a bucket particle defined",
 tooManyMains     - "Sorry, I've already got amain herd  defined"
                  - "try using addMacroHerdBase instead",
 badHerdNo        - "Sorry, you asked for an undefined macro Herd",
 badReadMacroFile - "Sorry, something's wrong with the file you specified"
                  - "to read macros from";

public:

  Void ctor()
                  - "Constructor for the Particle module."
                  - "Initializes build values.";
// Macro Particle Stuff:

  Integer

    bucketHerd       - "Identifier for a longitudinal bucket herd"
                     - "used to find the bucket",
    mainHerd         - "Identifier for the main macro particle herd",

    nMacroParticles  - "Number of injected macro-particles",
    nBucketParticles - "Number of special bucket particles",
    syncPart         - "Identifier for the synchronous particle",
    partChunkSize    - "Chunk size to allocate space for macro particles",

    nHerds            - "Number of macro Particle herds";

// Tune stuff:

  RealVector
    xPhaseOld     - "Record of horizontal betatron phase at previous transfer "
                  - "matrix [rad]",
    yPhaseOld     - "Record of vertical betatron phase at previous transfer "
                  - "matrix [rad]",
    xPhaseTot     - "Total horizontalbetatron  phase advance accumulated  "
                  - "during tune tracking [rad]",
    yPhaseTot     - "Total vertical betatron  phase advance accumulated "
                  - "during tune tracking [rad]",
    xTune         - "The horizontal tunes of the macroparticles",
    yTune         - "The vertical tunes of the macroparticles",
    lTuneTrack    - "Longitudinal advance during  tune tracking [m]";


// Routines:


// Particle formation routines:

  Integer getHerdSize(const Integer &herd)
                 - "return the number of macro-particles in the herd.";
  Integer getHerdSizeGlobal(const Integer &herd)
                 - "return the number of macro-particles in all CPUs' herds.";
  Void setHerdCenterXY(const Integer &herd,  const Real &x_c, const Real &y_c)
                 - "Sets the geometric center of the herd in the new position in the (x,y) plane";
  Integer  addMacroHerd(const Integer &chunkSize)
                 - "Routine to make a main herd of macro paricles";
  Integer addMainHerd(const Integer &chunkSize)
                 - "Routine to make a main herd of macro paricles";
  Integer addMacroHerdBase(const Integer &chunkSize,const String &name)
                 - "Routine to make a herd of macro paricles";
  Void
  addSyncPart(const Real &m, const Integer &c, Real &E)
		  - "Routine to add a synchronous particle";
  Void
  updateSyncPart(const Real &m, const Integer &c, Real &E)
		  - "Routine to update a synchronous particle";

  Void addMacroPartBase2(const Integer &herd, const Real &x, const Real &xP,
               const Real &y, const Real &yP, const Real &E, const Real & phi)
                  - "Routine to insert a macro particle into a herd";
  Void addMacroPartBase(const Integer &herd, const Real &x, const Real &xP,
                     const Real &y, const Real &yP);
  Void addMacroPart2(const Real &x, const Real &xP,
               const Real &y, const Real &yP, const Real &E, const Real & phi)
                  - "Routine to insert a macro particle into the main herd";
  Void addMacroPart(const Real &x, const Real &xP,
                     const Real &y, const Real &yP);
  Void addBucketPart(const Real &dE, const Real &phi)
                  - "Routine to make a bucket herd and add a particle to it";

  Void setHerdFeelLevel(Integer &h, const Integer &fl)
                   - "Routine to set the herd feel level _feelsHerds";

  Void setPartValsBase(const Integer &i, const Integer &herd,
                                const Real &nx, const Real &nxp,
                                const Real &ny, const Real &nyp,
                                const Real &ndeltaE, const Real &nphi)
                   - "Routine to update particle values";

// Infor to extract info about specific particles:
  Real xVal(const Integer &i)
                  - "Routine do dump x value of MacoPart i for the mainHerd";
  Real xpVal(const Integer &i);
  Real yVal(const Integer &i);
  Real ypVal(const Integer &i);
  Real deltaEVal(const Integer &i);
  Real phiVal(const Integer &i);

  Real xValBase(const Integer &i, const Integer &herd)
                  - "Routine do dump x value of MacoPart i for a herd";
  Real xpValBase(const Integer &i, const Integer &herd);
  Real yValBase(const Integer &i, const Integer &herd);
  Real ypValBase(const Integer &i, const Integer &herd);
  Real deltaEValBase(const Integer &i, const Integer &herd);
  Real dp_pValBase(const Integer &i, const Integer &herd);
  Real phiValBase(const Integer &i, const Integer &herd);

// Routines to extract vector info about particles:

  Void xVals(RealVector &x)   - "Dump x values of mainHerd  to a vector";
  Void xpVals(RealVector &x)  - "Dump x-prime values of main herd to a vector";
  Void yVals(RealVector &x)   - "Dump y values of main herd to a vector";
  Void ypVals(RealVector &x)  - "Dump y-prime values of main herd to a vector";
  Void deltaEVals(RealVector &x)- "Dump delta-E values of main herd to a vector";
  Void phiVals(RealVector &x)  - "Dump phi values of main herd to a vector";
  Void absPhiVals(RealVector &x)  - "Dump |phi| values of main herd to a vector";

// Routines to read/write to stream:

  Void dumpPart(const Integer &n, Ostream &os, const Integer &index)
                             - "Dump selected particle coordinates to a stream";
  Void dumpParts(const Integer &n, Ostream &os)
                             - "Dump particle coordinates to a stream";
  Void dumpPartsGlobal(const Integer &n, Ostream &os)
                             - "Dump particle coordinates to a stream for all CPUs";
  Void showExtrema(const Integer &n, Ostream &os)
                             - "Dump particle extrema from a herd to a stream";
  Void showHerd(const Integer &n,Ostream &os)
			- "Routine to show MacroParticle info to stream os";
  Void showHerdGlobal(const Integer &n,Ostream &os)
			- "Routine to show MacroParticle info to stream os for all CPUs";
  Void showSyncPart(Ostream &os)
			- "Routine to show Sync Particle info to stream os";
  Void readParts(const Integer &n, const String &fName,
                 const Integer &nParts)
                             - "Read particle coordinates from a stream";
  Void readParts2(const Integer &n, const String &fName,
                  const Integer &nParts)
                          - "Read particle coordinates from a stream"
                          - "in ACCSIM units";

  Void readPartsParallel(const Integer &n, const String &fName,
	                 const Integer &nParts, const Integer &type_accsim)
			- "Read particle coordinates from a stream in parallel";

  Void dumpLostParts(const Integer &n,Ostream &os)
			- "Routine to dump lost MacroParticle info to stream os";

  Void dumpLostPartsGlobal(const Integer &n,Ostream &os)
			- "Routine to dump lost MacroParticle info for all CPUs to stream os";


// Misc stuff:
  Integer
  All_Mask  - "Integer used as mask for macroPart identifiers";

// Parallel stuff:

  Void syncNMacs(Integer &n)
                  - "Gets the total number of macros from all parallel "
                  - "jobs to the parent";

	Real getSyncPartEnergy()
                        - "Returns the kinetic energy of the sync. particle.";
//-------------debugging methods-----------start-------------------------


  Void bumpXYcoords(Real &x_bump, Real &y_bump)
                        - "Debugging method."
                        - "Adds constant value to the x and y coordinates.";


//-------------debugging methods-----------stop--------------------------


private:

  Integer
    nSyncP       - "Number of Specie available";

  // parallel stuff  ==MPI==  ==start===
  Integer
    iMPIini       - "inf. about MPI 0-non initialized 1-initialized";
  Integer
    nMPIrank      - "rank of this CPU";
  Integer
    nMPIsize      - "number of CPUs for Parallel run";
  // parallel stuff  ==MPI==  ==stop===
}
