////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   SpaceCharge3D_MPI.cc
//
// AUTHOR
//   Slava Danilov, Jeff Holmes, John Galambos
//   ORNL, vux@ornl.gov
//
// CREATED
//   4/10/2001
//
// DESCRIPTION
//   Source code for the 3D spacecharge kick (MPI parallel implementation).
//   This module contains source for the SpaceCharge3D.cc  related info.
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Particles.h"
#include "MacroPart.h"
#include "Node.h"
#include "MapBase.h"
#include "TransMapHead.h"


#include "SpaceCharge3D_MPI.h"
#include "SpCh3D_MPI.h"
#include "PartsDistributor_MPI.h"
#include "FFT3DSpaceCharge_MPI.h"
#include "Boundary.h"

#include <iostream>
#include <cstdlib>

extern Array(ObjectPtr) mParts;
extern Array(ObjectPtr) nodes;
extern Array(ObjectPtr) tMaps;

ORBIT_Boundary* boundary_;

Define_Standard_Members(SpCh3D_MPI, Node);

Void SpaceCharge3D_MPI::ctor()
{
// set some initial values
    iMPIini_=0;
    MPI_Initialized(&iMPIini_);
    nMPIsize_ = 1;
    nMPIrank_ = 0;
    if(iMPIini_ > 0){
      MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
      MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);
    }    
}

Void SpaceCharge3D_MPI::dtor()
{
    
}

Void SpCh3D_MPI::_nodeCalculator(MacroPart &mp)
{
}

Void SpCh3D_MPI::_updatePartAtNode(MacroPart &mp)
{
   //Loading Manager definition (this is singleton)
   NodeLoadingManager* LM_ = NodeLoadingManager::GetNodeLoadingManager();

   //Loading Manager timing stop
   LM_->measureStop();

  _FFT3DSpaceCharge_MPI->propagate( mp, *_boundary, _lkick, 0.0 , 0.0);

  //Loading Manager timing start
   LM_->measureStart();
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  SpaceCharge3D_MPI::addSpCh3D_FFT_MPI
//
// DESCRIPTION
//    Adds a 3DSC_FFT Space Charge Node
//
// PARAMETERS
//    name           :  Name for the TM 
//    order          :  Order index
//    n(X,Y,Phi)Bins :  Number of bins to use
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D_MPI::addSpCh3D_FFT_MPI(String &name, Integer &order,
                    Integer &nXBins, Integer  &nYBins, Integer &nPhiBins,
                    Real &length, Integer &nMacrosMin)
{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + 100);

   nNodes++;
   SpCh3D_MPI* Ch3D_node;
   Ch3D_node = new SpCh3D_MPI(name, order,
                                    nXBins, nYBins, nPhiBins,
                                    length,nMacrosMin);

   Ch3D_node->_setBoundary(boundary_);

   nodes(nNodes-1) = Ch3D_node;

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  SpaceCharge3D_MPI::addSpCh3D_FFTSet_MPI
//
// DESCRIPTION
//    Adds a set of 3D Space Charge Nodes, 
//    one for every Matrix element present. 
//    Adds the set to be second order in the
//    integration. I.e., use a "central" kick over the 1/2 length
//    of the previous matrix element + 1/2 length of the present matrix
//    element.
//
// PARAMETERS
//
//    nxb       Number of X   bins to use
//    nyb       Number of Y   bins to use
//    nphi      Number of Phi bins to use
//    eps:      The smoothing parameter to use (fraction of a bin length)
//    BP...     Conducting wall beam pipe parameters
//    BP1       -x-maximal size of the beam pipe 
//    BP2       -y-maximal size of the beam pipe
//    Gridfact  Transverse grid size compared to problem extent
//    nMacrosMin The minimum number of macros to have before doing
//              the kick
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D_MPI::addSpCh3D_FFTSet_MPI(
                        Integer &nxb, Integer &nyb, Integer &nphi0,
                        Real &eps,
                        String &BPShape, Real &BP1, Real &BP2,
                        Integer &BPPoints, Integer &BPModes,
                        Real &Gridfact,
                        Integer &nMacrosMin)
{

    Integer nphi = nphi0+1;

    //Loading Manager repetition rate definition
    // It means that after nCountLM measurements the longitudinal grid
    // will be revised
    Integer nCountLM = 1000;

    MacroPartDistributor* PartDistr;
    PartDistr = MacroPartDistributor::GetMacroPartDistributor(nCountLM,nphi);

    Node *n1;
    MapBase *tm, *tm2;
    Integer n;
    String wname;
    Real avLength;
    Integer o_ind;

    if( nTransMaps < 2) except(tooFewTransMaps);

    Integer BPShape_ind;
    if( BPShape != "Circle" && BPShape != "Ellipse" && BPShape != "Rectangle"){
      except(badPipeShape);
    }

    if(BPShape == "Circle") BPShape_ind=1;
    if(BPShape == "Ellipse") BPShape_ind=2;
    if(BPShape == "Rectangle") BPShape_ind=3;

    boundary_ = new ORBIT_Boundary(nxb, nyb, 
                                   BP1, BP2,
                                   BPPoints, BPShape_ind, 
                                   BPModes , eps );


    char num2[14];
    String cons("3DSC_MPI");
    
    // Add kick for 1st half of TM number 1:

    wname = cons+"0";   
    tm = MapBase::safeCast(tMaps(0));
    avLength = tm->_length/2.;
    o_ind = tm->_oindex-5;
    addSpCh3D_FFT_MPI(wname, o_ind, nxb, nyb, nphi, avLength, nMacrosMin);
	      
    // Add central kicks :

    for (n=1; n<=nTransMaps-1; n++)
    {
        
        tm = MapBase::safeCast(tMaps(n-1));
        tm2 = MapBase::safeCast(tMaps(n));
        sprintf(num2, "(%d)", n); 
        wname = cons+num2;        
        avLength = ( tm->_length + tm2->_length)/2.; 
        o_ind = tm->_oindex+5;    
        addSpCh3D_FFT_MPI(wname, o_ind, nxb, nyb, nphi, avLength, nMacrosMin);
     }

    // Add kick for last half of last TM:

    sprintf(num2, "(%d)", nTransMaps); 
    wname = cons+num2;  
    tm = MapBase::safeCast(tMaps(nTransMaps-1));
    avLength = tm->_length/2.;
    o_ind = tm->_oindex+5;
    addSpCh3D_FFT_MPI(wname, o_ind, nxb, nyb, nphi, avLength, nMacrosMin);

   //Loading Manager definition
   NodeLoadingManager* LM_ = NodeLoadingManager::GetNodeLoadingManager();
   //timing shishlo
   LM_->switchOn();
   LM_->measureStart();
}

///////////////////////////////////////////////////////////////////////////
//  Switches Load Manager On
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D_MPI::switchLoadManagerOn()
{
   //Loading Manager definition
   NodeLoadingManager* LM_ = NodeLoadingManager::GetNodeLoadingManager();
   LM_->switchOn();
}

///////////////////////////////////////////////////////////////////////////
//  Switches Load Manager Off
///////////////////////////////////////////////////////////////////////////

Void SpaceCharge3D_MPI::switchLoadManagerOff()
{
   //Loading Manager definition
   NodeLoadingManager* LM_ = NodeLoadingManager::GetNodeLoadingManager();
   LM_->switchOff();
}

////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========START===============
////////////////////////////////////////////////////////////////////////////////


Void SpaceCharge3D_MPI::createPartDistr(Integer &nCountLM,Integer &nSlices_total)
{
 //debug method
 MacroPartDistributor* PartDistr;
 PartDistr = MacroPartDistributor::GetMacroPartDistributor(nCountLM,nSlices_total);
}

Void SpaceCharge3D_MPI::setMaximalNumberOfDistrParts(Integer &nMax)
{
 //debug method
 MacroPartDistributor* PartDistr;
 PartDistr = MacroPartDistributor::GetMacroPartDistributor();
 PartDistr->setPartNumbChunk(nMax);
}

Void SpaceCharge3D_MPI::distributeParticle()
{
  //debug method
   MacroPartDistributor* PartDistr;
   PartDistr = MacroPartDistributor::GetMacroPartDistributor();

   MacroPart *mp;
   mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));

   PartDistr->distributeParticles(*mp);


   int nMPIsize_,nMPIrank_;
   MPI_Comm_size(MPI_COMM_WORLD,&nMPIsize_);
   MPI_Comm_rank(MPI_COMM_WORLD,&nMPIrank_);

   //if(nMPIrank_ == 0){
   //
   //  std::cout<<"debug ==Distributor phi( min max ) ="
   //           <<PartDistr->getPhiMin()<<" "
   //           <<PartDistr->getPhiMax()<<"\n";
   //
   //  int i,i_max;
   //  double phi;
   //  i_max =  nMPIsize_*5;
   //  for(i=0; i< i_max; i++){
   //    phi = PartDistr->getPhiMin()+i*(PartDistr->getPhiMax()-PartDistr->getPhiMin())/i_max;
   //    std::cout<<"phi ="<<phi
   //             <<" cpu id="<<PartDistr->getNodeIndex(phi)
   //             <<" phi ind="<<PartDistr->getPhiIndex(phi)
   //             <<"\n";
   //  }
   //}
}

Void SpaceCharge3D_MPI::distributeParticleEvenly()
{
  //debug method
   MacroPartDistributor* PartDistr;
   PartDistr = MacroPartDistributor::GetMacroPartDistributor();

   MacroPart *mp;
   mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1));

   PartDistr->distributeParticlesEvenly(*mp);
}


Real SpaceCharge3D_MPI::rand_MPI()
{
  //debug method
  double r;
  r = ((double) rand())/RAND_MAX;
  return r;
}


Void SpaceCharge3D_MPI::createBoundary(
                      Integer &xBins, Integer &yBins, 
                      Real &xSize, Real &ySize,
                      Integer &BPPoints, Integer &BPShape,
                      Integer &BPModes, Real &eps)
{

    boundary_ = new ORBIT_Boundary(xBins, yBins, 
                                   xSize, ySize,
                                   BPPoints, BPShape , 
                                   BPModes , eps );
}

Void SpaceCharge3D_MPI::createFFT3DSpaceCharge_MPI(Integer &nXBins, Integer &nYBins, 
                                                   Integer &nZBins, Integer &minBunchSize)
{
  ORBIT_FFT3DSpaceCharge* FFT3DSpaceCharge;
  FFT3DSpaceCharge = ORBIT_FFT3DSpaceCharge::getORBIT_FFT3DSpaceCharge(
				    nXBins, nYBins, nZBins, minBunchSize);
}

Void SpaceCharge3D_MPI::propagateFFT3DSpaceCharge_MPI(Real &length)
{
  ORBIT_FFT3DSpaceCharge* FFT3DSpaceCharge;
  FFT3DSpaceCharge = ORBIT_FFT3DSpaceCharge::getORBIT_FFT3DSpaceCharge();

  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1)); 
  FFT3DSpaceCharge->propagate( *mp, *boundary_, length , 0.0 , 0.0); 

}

Integer SpaceCharge3D_MPI::getTotalNumbOfMacros_MPI()
{

  MacroPart *mp;
  mp= MacroPart::safeCast(mParts((Particles::mainHerd & Particles::All_Mask) - 1)); 
  Integer totalNumb;
  int iMPIini_=0;
  MPI_Initialized(&iMPIini_);
  int nMacrosGlobal;
  int nMacros = mp->_nMacros;
  nMacrosGlobal = nMacros;
  if(iMPIini_ > 0){
    MPI_Allreduce(&nMacros,&nMacrosGlobal,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
  }
  mp->_globalNMacros = nMacrosGlobal;

  totalNumb = mp->_globalNMacros;
  return totalNumb;
}
////////////////////////////////////////////////////////////////////////////////
//Debugging methods        ========STOP================
////////////////////////////////////////////////////////////////////////////////
