/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   Ring.cc
//
// AUTHOR
//   John Galambos
//   ORNL, jdg@ornl.gov
//
// CREATED
//   10/2/97
//
// DESCRIPTION
//   Module descriptor file for the Ring module. This module contains
//   general information/manipulations common to all Node types.
//   Source file for the Ring module. This module organizes the Ring nodes.
//
// REVISION HISTORY
//   Modification to keep running time in ring.
//   04/30/2007
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Ring.h"
#include "MacroPart.h"
#include "Node.h"
#include "MapBase.h"
#include "StrClass.h"
#include "StreamType.h"
#include "RealMat.h"
#include "IntegerMat.h"
#include <cmath>
#if defined(_WIN32)
  #include <strstrea.h>
#else
  #include <sstream>
#endif

#include <iomanip>

using namespace std;
///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS AND LOCAL ROUTINES & VARIABLES
//
///////////////////////////////////////////////////////////////////////////

// Lists of calculators, constraints, variables, and objective functions.

Array(ObjectPtr) nodes, ONodes;

extern Array(ObjectPtr) mParts, syncP, tMaps;

extern const Integer All_Mask;

// Granularity to allocate entries in work arrays

const Integer Node_Alloc_Chunk = 10;

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   turnKernel(MacroPart *mp, SyncPart *sp,
//              const Integer &startNode, const Integer &endNode,
//              const Integer &stepTime)
//
// DESCRIPTION
//   Pushes macro-particles of a herd from one node to another
//   Can also update the running time of the beam.
//
// PARAMETERS
//   mp        = pointer to the macroParticle herd to advance
//   sp        = synchronous particle used in updating time
//   startNode = the starting  node number
//   endNode   = the end node number
//   stepTime  = 0: does not advance time
//             = 1: advances time
//
// RETURNS
//   0           - for successful push
//   node number - if an error occured at a node.
//
///////////////////////////////////////////////////////////////////////////

Integer turnKernel(MacroPart *mp, SyncPart *sp,
                   const Integer &startNode,
                   const Integer &endNode,
                   const Integer &stepTime, 
		           const Real &globalAperture)
{
  Node *node;
  Integer i;

  if(Ring::longTrackOnly && stepTime && (startNode == 1))
  {
    Ring::time += 1000. * Ring::lRing / (sp->_betaSync * Consts::vLight);
  }

  for(i = startNode; i <= endNode; i++)
  {
    mp->_currentNode = i;
    node = Node::safeCast(ONodes(mp->_currentNode - 1));

    if(!Ring::longTrackOnly && stepTime) // Advance time only when desired
    {
      if(node -> isKindOf(MapBase::desc()))
      {
        Ring::time += 500. * node->_length /
                      (sp->_betaSync * Consts::vLight);
      }
    }

    node->_nodeCalculator(*mp);
    if(node->_nodeError == 1)
    {
      cerr << node->_errMessage << "\n";
      return i;
    }

    node->_updatePartAtNode(*mp);

    if(!Ring::longTrackOnly && stepTime) // Advance time only when desired
    {
      if(node -> isKindOf(MapBase::desc()))
      {
        Ring::time += 500. * node->_length /
                      (sp->_betaSync * Consts::vLight);
      }
    }

    if(globalAperture > 0.0 )
    {
      Integer j = 1;
      while( j <= mp->_nMacros) //Remove particles outside global aperture.
      {
        if((Sqr(mp->_x(j))+Sqr(mp->_y(j))) > Sqr(globalAperture))
        {
          mp->_addLostMacro(j, node->_position);
          j--;
        }
        j++;
      }
    }
  }

  mp->_currentNode++;
  return 0;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Ring::ctor
//
// DESCRIPTION
//   Initializes the various Ring related constants.
//
// PARAMETERS
//   None.
//
// RETURNS
//   Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Ring::ctor()
{
  // set some initial values

  nodesInitialized = 0;
  ringInitialized = 0;
  nTurnsDone = 0;
  nNodes = 0;

  alphaX = 0.0;
  alphaY = 0.0;
  betaX = 10.0;
  betaY = 10.0;
  betaXAvg = 10.0;
  betaYAvg = 10.0;
  chromaticityX = 0.0;
  chromaticityY = 0.0;
  etaX = 0.0;
  etaPX = 0.0;
  etaY = 0.0;
  etaPY = 0.0;
  gammaX = (1.0 + alphaX * alphaX) / betaX;
  gammaY = (1.0 + alphaY * alphaY) / betaY;

  lRing = 0.0;
  compactionFact = 0.0;
  etaCompaction = 0.0;
  gammaTrans = 1.0e+10;
  nuX = 6.23;
  nuY = 6.20;
  globalAperture = -1.0;

  harmonicNumber = 1.0;
  nLongUpdates = 0;
  longTrackOnly = 0;
  nPartTurnsDone = 0;

  time = 0.;

  bucketdE0 = 0.0;
  bucketphi0 = 0.99;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Ring::initNodes
//
// DESCRIPTION
//   Initialize nodes. Basically it just sorts all the nodes which
//   have been instantiated. Note: Only the node pointers are sorted,
//   and these are stored in the array ONodes. The specific Node type
//   pointers (e.g. TransMap) are not sorted.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::initNodes()
{
  Integer i, j;
  Integer an1, an2;
  Node    *n1, *n2;
  Real pos;

  ONodes.resize(nNodes);
  numNodes.resize(nNodes);

  // Order the nodes by the assigned index:

  for (i = 1; i <= nNodes; i++)
  {
    n1 = Node::safeCast(nodes(i - 1));
    ONodes(i - 1) = n1;
    numNodes(i) = i;
  }

  for (j = 2; j <= nNodes; j++)
  {
    n1 = Node::safeCast(ONodes(j - 1));
    an1 = numNodes(j);

    for (i = j - 1; i >= 0; i--)
    {
      if (i == 0) break;

      n2 = Node::safeCast(ONodes(i - 1));
      an2 = numNodes(i);
      if (n2->_oindex <= n1->_oindex) break;

      ONodes(i) = n2;
      numNodes(i + 1) = an2;
    }
    ONodes(i) = n1;
    numNodes(i + 1) = an1;
  }

  // Find and assign node longitudinal positions:

  pos = 0.;
  for (i = 1; i <= nNodes; i++)
  {
    n1 = Node::safeCast(ONodes(i - 1));
    pos += n1->_length;
    n1->_position = pos;
  }

  if(!longTrackOnly) lRing = pos;

  nodesInitialized = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Ring::initRing
//
// DESCRIPTION
//   Does Ring initializations
//
// PARAMETERS
//   none
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::initRing()
{
  initNodes();  // Initialize the nodes

  // Other initializations:

  // Set the synchronous particle info that depends on lRing:

  if(syncP.size()==0) except(noSyncPart);
  SyncPart *sp = SyncPart::safeCast(syncP(0));
  sp->_phiCoef = 2. * pi * harmonicNumber / (lRing * 1000.);

  ringInitialized = 1;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Ring::doTurnBase(Integer &herdNo, const Integer &nTurns)
//
// DESCRIPTION
//   Pushes macro-particles of a specified herd around the ring
//
// PARAMETERS
//   herdNo  - Integer identifier of the herd.
//   nTurns  - Number of turns to do
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::doTurnBase(Integer &herdNo, const Integer &nTurns)
{
  Integer i, error, stepTime;
  MacroPart *mp;

  if((herdNo & All_Mask) > Particles::nHerds) except (badHerdNo);
  if (nTurns < 1) except(badNTurns);
  if (nNodes == 0) except(noRingNodes);

  if(!nodesInitialized) initRing();
  if(!ringInitialized) initRing();

  // Get the pointer to the herd and set stepTime:

  SyncPart *sp = SyncPart::safeCast(syncP(0));
  mp = MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
  stepTime = 0;
  if(herdNo == mainHerd) stepTime = 1;

  // Push herd around the ring:

  for (i = 1; i <= nTurns; i++)
  {
    error = turnKernel(mp, sp, 1, nNodes, stepTime, globalAperture);
    if(error) Ring::except(Ring::nodeError);

    // Turn(s) done, update some tallys.

    mp->_nTurnsDone++;
    mp->_nPartTurnsDone += mp->_nMacros;
    if(herdNo == mainHerd) // special tallies for the main herd
    {
      nMacroParticles = mp->_nMacros;
      nTurnsDone = mp->_nTurnsDone;
      nPartTurnsDone = mp->_nPartTurnsDone;
    }
    mp->_currentNode = 1;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//   Ring::doTurn(const Integer &nTurns)
//
// DESCRIPTION
//   Pushes macro-particles of the main herd around the ring
//
// PARAMETERS
//   nTurns - Number of turns to do
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::doTurn(const Integer &nTurns)
{
  if(mainHerd == 0) except(noHerd);

  // do some turns with the mainHerd:

  doTurnBase(mainHerd, nTurns);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Ring::turnHerds(const Integer &nTurns)
//
// DESCRIPTION
//   Pushes macro-particles of all herds around the ring for
//   a specified number of turns. The herds are pushed in sync
//   around the Ring.
//
// PARAMETERS
//   nTurn   - Number of turns to do
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::turnHerds(const Integer &nTurns)
{
  Integer i, j, k, error, stepTime;
  Node *node;
  MacroPart *mp;

  if(nHerds == 0) except(badHerdNo);
  if (nTurns < 1) except(badNTurns);
  if (nNodes == 0) except(noRingNodes);

  if(!nodesInitialized) initRing();
  if(!ringInitialized) initRing();    

  // Make sure all herds are set to go at the start of the Ring:

  for(k = 1; k <= nHerds; k++)
  {
     mp = MacroPart::safeCast(mParts(k - 1));
     if(mp->_currentNode != 1) except(badHerdPos);
  }

  SyncPart *sp = SyncPart::safeCast(syncP(0));

  // Push herds around the ring:

  for(i = 1; i <= nTurns; i++)
  {
    for(j = 1; j <= nNodes; j++)
    {
      for(k = 1; k <= nHerds; k++)
      {
        stepTime = 0;
        if(k == 1) stepTime = 1;
        mp = MacroPart::safeCast(mParts(k - 1));
        error = turnKernel(mp, sp, j, j, stepTime, globalAperture);
        if(error) Ring::except(Ring::nodeError);
      }
    }

    // Turn done, update some tallys.
    for(k = 1; k <= nHerds; k++)
    {
      mp = MacroPart::safeCast(mParts(k - 1));
      mp->_nTurnsDone++;
      mp->_nPartTurnsDone += mp->_nMacros;
      mp->_currentNode = 1;
    }
    mp = MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));
    nTurnsDone = mp->_nTurnsDone;
    nPartTurnsDone = mp->_nPartTurnsDone;
    nMacroParticles = mp->_nMacros;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Ring::turnToNode(Integer &herdNo, Integer &nodeStop)
//
// DESCRIPTION
//   Pushes macro-particles of herd "herd" to a node of the ring
//   Execute the node calculators up to and including those of node
//   "nodeStop". Also, advances time for herdNo = mainHerd.
//
// PARAMETERS
//   herdNo  - Integer identifier of the herd.
//   nodeStop   - Node to turn to
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::turnToNode(Integer &herdNo, const Integer &nodeStop)
{
  Integer error, stepTime;
  Node *node;
  MacroPart *mp;

  if((herdNo & All_Mask) > Particles::nHerds) except (badHerdNo);
  if(nNodes == 0) except(noRingNodes);

  if(!nodesInitialized) initRing();
  if(!ringInitialized) initRing();

  // Get the pointer to the herd and set stepTime:

  SyncPart *sp = SyncPart::safeCast(syncP(0));
  mp = MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
  stepTime = 0;
  if(herdNo == mainHerd) stepTime = 1;

  if (nodeStop < mp->_currentNode) except(badNodeNum1);
  if (nodeStop > nNodes) except(badNodeNum2);

  // Push herd the ring to the specified node:

  error = turnKernel(mp, sp, mp->_currentNode, nodeStop, stepTime, globalAperture);
  if(error) Ring::except(Ring::nodeError);

  if(nodeStop == nNodes) // Turn done, update some tallys.
  {
    mp->_nTurnsDone++;
    mp->_nPartTurnsDone += mp->_nMacros;
    mp->_currentNode = 1;
    if(herdNo == mainHerd)
    {
      nTurnsDone = mp->_nTurnsDone;
      nPartTurnsDone = mp->_nPartTurnsDone;
      nMacroParticles = mp->_nMacros;
    }
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Ring::finishTurn(Integer &herdNo)
//
// DESCRIPTION
//   Pushes macro-particles of herd "herd" through the last node of the ring,
//   from wherever they happen to be.
//   Execute the node calculators up to and including those of node
//   nNodes (last node in Ring).
//
// PARAMETERS
//    herdNo  - Integer identifier of the herd.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::finishTurn(Integer &herdNo)
{
  Integer error, stepTime;
  Node *node;
  MacroPart *mp;

  if((herdNo & All_Mask) > Particles::nHerds) except (badHerdNo);
  if(nNodes == 0) except(noRingNodes);

  // Get the pointer to the herd and set stepTime:

  SyncPart *sp = SyncPart::safeCast(syncP(0));
  mp = MacroPart::safeCast(mParts((herdNo & All_Mask) - 1));
  stepTime = 0;
  if(herdNo == mainHerd) stepTime = 1;

  if (mp->_currentNode == 1) return;  // already at the beginning

  // Push herd the ring to nNodes:

  error = turnKernel(mp, sp, mp->_currentNode, nNodes, stepTime, globalAperture);
  if(error) Ring::except(Ring::nodeError);

  // Turn done, update some tallys.

  mp->_nTurnsDone++;
  mp->_nPartTurnsDone += mp->_nMacros;
  mp->_currentNode = 1;
  if(herdNo == mainHerd)
  {
    nTurnsDone = mp->_nTurnsDone;
    nPartTurnsDone = mp->_nPartTurnsDone;
  }
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Ring::calcBucket(const Real &dE, const Real &angle)
//
// DESCRIPTION
//   This is a special routine to 
//   find the longitudinal bucket. Uses a seperate bucket herd to push
//   around the ring, tracing out the separatrix. The idea is to start this
//   test particle near the end point of the seperatrix.
//
// PARAMETERS
//   dE - the initial delta E for bucket particle calculation (GeV)
//   angle - the initial  angle to start the bucket particle with (deg).
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::calcBucket(const Real &dE, const Real &angle)
{
  Integer j, k;
  Node *node;
  MacroPart *bp;
  MacroPart *mp = MacroPart::safeCast(mParts((mainHerd & All_Mask) - 1));

  if(nNodes == 0) except(noRingNodes);
  if(!ringInitialized) initRing();

  SyncPart *sp = SyncPart::safeCast(syncP(0));

  bucketHeight = 0.;
  bucketArea = 0.;

  /*
    move calc of u-fixed point to Accelerate::getUFixedPoint
    do this calc first, and pass in absolute angle to this routine now.

    Real angleRad = angle * pi/180.;
    Real angle2 = Consts::pi - sp->_phase - angleRad;
  */

  //  Make a test particle "bucket-herd":

  if(nBucketParticles == 0)
  {
    addBucketPart(dE, angle);
    bp = MacroPart::safeCast(mParts((bucketHerd & All_Mask) - 1));
  }
  else
  {
    bp = MacroPart::safeCast(mParts((bucketHerd & All_Mask) - 1));
    bp->_deltaE(1) = dE;
    bp->_phi(1) = angle;
  }

  Real phiDirection = -1.;
  Real phiOld, dEOld, dESign;
  dESign = 1.;
  k = 0;

  /*
    Calculate the seperatrix:
    while (Abs(bp->_phi(1)) > 1.e-2 && dESign > 0.
    && Abs(bp->_phi(1)) < Consts::pi )
  */

  while ((k < 5) || ((dESign > 0.) && (Abs(bp->_phi(1)) < Consts::pi )))
  {
    k++;

    phiOld = bp->_phi(1);
    dEOld = bp->_deltaE(1);
        
    for(j = 1; j <= nNodes; j++)
    {
      node = Node::safeCast(ONodes(j - 1));
      node->_nodeCalculator(*bp);
      node->_updatePartAtNode(*bp);
    }
    phiDirection = bp->_phi(1) - phiOld;
    if(Abs(phiDirection) > Consts::pi) break; // catch "2pi" jump

    dESign = bp->_deltaE(1)/dEOld;

    /*
      cerr << "\n\n" << k << "\n"
           << "phiDirection,dESign = "
           << phiDirection << "   " << dESign << "\n"
           << "phiOld,dEOld = "
           << phiOld << "   " << dEOld << "\n"
           << "phi,dE = "
           << bp->_phi(1) <<  "   " << bp->_deltaE(1)
           << "\n\n";
    */

    bucketArea +=  Abs(phiDirection) * Abs(bp->_deltaE(1));
    if(bp->_deltaE(1) > bucketHeight) bucketHeight = bp->_deltaE(1);

    if(k >= bucketphi.rows())
    {
      bucketphi.resize(k + 100);
      bucketdE.resize(k + 100);
    }
    bucketphi(k) = bp->_phi(1);
    bucketdE(k) = bp->_deltaE(1);
  }

  nBucketTurns = k - 1;
  /*
    if(k == 1) except(badBucketGuess);
  */

  bucketphi.resize(2 * nBucketTurns - 1);
  bucketdE.resize(2 * nBucketTurns - 1);
  for (k = 1; k <= nBucketTurns - 1; k++)
  {
    bucketdE(nBucketTurns + k) = -bucketdE(nBucketTurns - k);
    bucketphi(nBucketTurns + k) = bucketphi(nBucketTurns - k);
  }

  bucketArea *= 2.e9 * Ring::lRing / (2.* Consts::pi *
                Consts::vLight  * sp->_betaSync * Ring::harmonicNumber);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Ring::clearNodes()
//
// DESCRIPTION
//   Clears all the nodes in the Ring. Presently it just clears
//   the array of node pointers, and sets nNodes = 0. It does NOT
//   call the destructors of the nodes! As such, it should be used
//   sparingly. It may be useful to clear the Ring in order to
//   subsequently transport an accumulated beam to a detector after
//   a run, etc.
//
// PARAMETERS
//   None
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Ring::clearNodes()
{
  nNodes = 0;
  ringInitialized = 0;
  nodesInitialized = 0;
  lRing = 0.;
}

