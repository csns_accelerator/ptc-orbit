/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//   FieldTracker.mod
//
// AUTHOR
//   Jeff Holmes
//
// CREATED
//   03/08/2007
//
// MODIFIED
//
// DESCRIPTION
//   Module for tracking through 3D magnetic fields
//
// REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

module FieldTracker - "FieldTracker Module."
              - "Written by Jeff Holmes"
{

  Inherits Consts, Particles, Ring, TransMap, TeaPot;

  Errors
  badHerdNo     - "Sorry, you asked for an undefined macro Herd",
  badMADFile    - "Sorry, I can't find the MAD file you specified",
  EOFTwiss      - "Oops - I'm at the end of the Twiss file. More TMs"
                - "than !0 length twiss sets. Check MAD deck for 0"
                - "length elements",
  badNode       - "Sorry, you asked for an undefined Node",
  noNode        - "Sorry, you asked for a nonexistent Node",
  badBFile      - "Something wrong with the file you gave";

  public:

  Integer
    nFTNodes    - "Number of FieldTracker Nodes in the Ring",
    zsymmetry   - "Symmetry of element B field data",
    doRefPath	- "Flag for doing reference particle calculation. 0 is do it (default)",
    aperture    - "Flag for checking aperture. 0 is don't do it (default)",
    useCatcher  - "Flag to use the catcher aperture",
    getPath     - "Flag to print the entire path of a particle";

  Real
    BScale      - "Multiplication factor for B field",
    xMin        - "Right side X  aperture",
    xMax 	- "Left side X apeture",
    yMin	- "Bottom Y aperture",
    yMax	- "Top Y aperture",
    xFoilMin	- "Foil right edge",
    xFoilMax	- "Foil left edge",
    yFoilMin	- "Foil bottom edge",
    yFoilMax 	- "Foil top edge",
    zFoilMin	- "Min Z foil",
    zFoilMax    - "Max Z foil",
    xBracketMin	- "Bracket right edge",
    xBracketMax	- "Bracket left edge",
    yBracketMin	- "Bracket bottom edge",
    yBracketMax - "Bracket top edge",
    zBracketMin	- "Min Z bracket",
    zBracketMax - "Max Z bracket",   
    xBracketArmMin - "Bracket arm right edge",
    xBracketArmMax - "Bracket arm left edge",
    yBracketArmMin - "Bracket arm bottom edge",
    yBracketArmMax - "Bracket arm top edge",
    zBracketArmMin - "Min Z bracket arm",
    zBracketArmMax - "Max Z bracket arm",   
    zCatcherMin - "Min Z electron catcher",
    zCatcherMax - "Max Z electron catcher",
    yCatcherMin - "Min Y electron catcher", 
    yCatcherMax - "Max Y electron catcher", 
    foilAngle   - "Y-Z tilt angle of foil. Default is no-tilt (0)",
    RMaglich    - "Radius for Maglich field",
    B0Maglich   - "Magnitude of Maglich field",
    kMaglich    - "Focusing constant for Maglich field",
    sMax        - "Maximum distance to track / node length",
    rMax        - "Maximum radius to track";

  Void ctor() - "Constructor for the FieldTracker module.";

  Void nullOut(const String &n, const Integer &order,
               Integer &transmapNumber,
               Real &bx, Real &by,
               Real &ax, Real &ay,
               Real &ex, Real &epx,
               Real &length, const String &et)
               - "Routine to remove a transmap node";

  Void replaceFT3D(const String &n, const Integer &order,
                   const String &et,
                   const Subroutine sub,
                   const Real &zi, const Real &zf,
                   const Real &ds, const Integer &niters,
                   const Real &resid,
                   const Real &xrefi, const Real &yrefi,
                   const Real &eulerai, const Real &eulerbi,
                   const Real &eulergi,
		   const Integer &apflag)
                   - "Routine to replace transmap by FT3D";

  Void addFT3D(const String &n, const Integer &order,
               const Real &bx, const Real &by,
               const Real &ax, const Real &ay,
               const Real &ex, const Real &epx,
               const Real &l, const String &et,
               const Subroutine sub,
               const Real &zi, const Real &zf,
               const Real &ds, const Integer &niters,
               const Real &resid,
               const Real &xrefi, const Real &yrefi,
               const Real &eulerai, const Real &eulerbi,
               const Real &eulergi,
  	       const Integer &apflag)
               - "Routine to add an FT3D";

  Void showFT(Ostream &os)
              - "Routine to print FT Lattice to a stream";

  Void ParseTest3D(const String &fileName)
               - "Routine to parse 3D test field file";

  Void ParseMults3D(const String &fileName,
                    const Real &zmin, const Real &zmax)
               - "Routine to parse 3D multipole field file";

  Void ParseGrid3D(const String &fileName,
                   const Real &xmin, const Real &xmax,
                   const Real &ymin, const Real &ymax,
                   const Real &zmin, const Real &zmax,
                   const Integer &skipX,
                   const Integer &skipY,
                   const Integer &skipZ)
               - "Routine to parse 3D grid field file";

  Void BTest3D()
               - "Routine to provide 3D test fields";

  Void BMults3D()
               - "Routine to provide 3D multipole fields";

  Void BGrid3D()
               - "Routine to provide 3D grid fields";

  Void BGradGrid3D()
               - "Routine to provide 3D gradient fields";

  Void BMaglich3D()
               - "Routine to provide 3D Maglich fields";
 
  Integer ApertureCondition(Real &x, Real &y,
               Real &z, Real &ycent, Real &zcent, Real &step)
               - "Check the particle for apertures";
  Real Scatter(Real &px, Real &py, Real &pz)
  	       - "Scatter from the surface, check for absorption";
		
}
