/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    Bump.cc
//
// AUTHOR
//    John Galambos
//    ORNL, jdg@ornl.gov
//
// CREATED
//    12/11/97
//
//  DESCRIPTION
//    Module descriptor file for the Bump module. This module contains
//    Source for the closed orbit Bump related info.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////
#include "Bump.h"
#include "Node.h"
#include "MacroPart.h"
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

///////////////////////////////////////////////////////////////////////////
//
// STATIC DEFINITIONS
//
///////////////////////////////////////////////////////////////////////////

Array(ObjectPtr) idealBumpPointers;
extern Array(ObjectPtr) mParts, nodes;

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    IdealBump
//
// INHERITANCE RELATIONSHIPS
//    IdealBump -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None.
//
// DESCRIPTION
//    This is a class for storing ideal bump information. A bump simply 
//    adds a given amount of x, xp, y, yp to each particle.
//
// PUBLIC MEMBERS
//   _xBumpVal   The X position ideal bump (mm)
//   _xPBumpVal  The X prime ideal bump (mrad)
//   _yBumpVal   The Y position ideal bump (mm)
//   _yPBumpVal  The Y prime ideal bump (mrad)
//   _upDown     Indicator for bumping up (==1) or down (!=1)
//   _sub        Routine that provides the bump values as a function of time
//     
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class IdealBump : public Node {
  Declare_Standard_Members(IdealBump, Node);
public:
    IdealBump(const String &n, const Integer &order, 
                 const Integer &ud, const Subroutine sub)  :
            Node(n, 0., order), _upDown(ud), _sub(sub)
        {           
        }

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp);
    virtual Void _nodeCalculator(MacroPart &mp);

    Real _xBumpVal,_xPBumpVal,_yBumpVal,_yPBumpVal;
    Integer _upDown;
    Subroutine _sub;
    
};



///////////////////////////////////////////////////////////////////////////
//
// NON-INLINE PROTECTED MEMBER FUNCTIONS FOR CLASS IdealBump
//
///////////////////////////////////////////////////////////////////////////
Define_Standard_Members(IdealBump, Node);

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    IdealBump::NodeCalculator
//
// DESCRIPTION
//    Calls the routine to get the bump values.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void IdealBump::_nodeCalculator(MacroPart &mp)
{
        // Call the routine to get ideal bump values:
    _sub();
    Real factor=1;
    Bump::bumpOn = 1;
    
    if(_upDown != 1)
        {
            factor = -1.;
            Bump::bumpOn=0;
        }
    

        // set the object members to the ideal bump values
    _xBumpVal  = factor * Bump::xIdealBump;
    _xPBumpVal = factor * Bump::xPIdealBump;
    _yBumpVal  = factor * Bump::yIdealBump;
    _yPBumpVal = factor * Bump::yPIdealBump;        
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//    IdealBump::updatePartAtNode
//
// DESCRIPTION
//    Calls the specified local calculator for an operation on
//     a MacroParticle with an IdealBump
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void IdealBump::_updatePartAtNode(MacroPart& mp)
{
    Integer i;

    for(i=1; i<=mp._nMacros; i++)
    {
      mp._x(i)  += _xBumpVal;
      mp._xp(i) += _xPBumpVal;    
      mp._y(i)  += _yBumpVal;
      mp._yp(i) += _yPBumpVal;
    }
    
}

///////////////////////////////////////////////////////////////////////////
//
// PUBLIC MEMBER FUNCTIONS FOR MODULE Bump
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// NAME
//    Bump::ctor
//
// DESCRIPTION
//    Initializes the various Bump related constants.
//
// PARAMETERS
//    None.
//
// RETURNS
//    Nothing.
//
///////////////////////////////////////////////////////////////////////////

Void Bump::ctor()
{

// set some initial values

    nIdealBumps = 0;
    xIdealBump = 0.;
    xPIdealBump = 0.;
    yIdealBump = 0.;
    yPIdealBump = 0.;
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Bump::addIdealBump
//
// DESCRIPTION
//    Adds a transfer matrix object
//
// PARAMETERS
//    name:    Name for the TM 
//    order:   Order index
//    upDown   Switch indicating whether to bump up (==1) or down (!=1)
//    sub      Subroutine name that will provide bump values as a
//             function of time
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Bump::addIdealBump(const String &name, const Integer &order, 
    const Integer &upDown,  const Subroutine sub)

{
  if (nNodes == nodes.size())
     nodes.resize(nNodes + 1);
  if(nIdealBumps == idealBumpPointers.size())
      idealBumpPointers.resize(nIdealBumps + 1);
  
   nNodes++;
  
   nodes(nNodes-1) = new IdealBump(name, order, upDown, sub);
   nIdealBumps ++;
   
   idealBumpPointers(nIdealBumps -1) = nodes(nNodes-1);
   
   nodesInitialized = 0;

}
 
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Bump::eFoldBump
//
// DESCRIPTION
//    Applies an exponential decay set of bumps.
//    Inputs are the initial x, x', y, y',
//    Final x and y, and the e-folding times for x and y.
//    The y' and x' coordinates are scaled by assuming motion along
//    the line with slope alpha/beta (at the foil)
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////
Void Bump::eFoldBump()
{


  Real denomX, denomY;
  Real delt = tBumpF - tBump0;
  Real angleX = 0.5 * atan(2*alphaX/(gammaX - betaX));
  Real angleY = 0.5 * atan(2*alphaY/(gammaY - betaY));  

  if (time > tBumpF)
    {
       xIdealBump = xBumpF;
       xPIdealBump = xPBump0 + (xBumpF - x0Inj)* tan(angleX);
       yIdealBump = yBumpF;
       yPIdealBump = yPBump0 + (yBumpF - y0Inj)* tan(angleY);
       return;
    }
       
  denomX =(1. - exp(-eFoldTimeX));
  denomX = Max(denomX, tiny); 
  xIdealBump = xBump0 - (xBump0-xBumpF)*(1. - exp(-eFoldTimeX*time/delt))/
                                         denomX;
  xPIdealBump = xPBump0 + (xIdealBump - x0Inj)* tan(angleX);
  
  denomY =(1. - exp(-eFoldTimeY));
  denomY = Max(denomY, tiny);
  yIdealBump = yBump0 - (yBump0-yBumpF)*(1. - exp(-eFoldTimeY*time/delt))/
                                         denomY;
  yPIdealBump = yPBump0 + (yIdealBump - y0Inj)* tan(angleY);
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Bump::eFoldBump2
//
// DESCRIPTION
//    Applies an exponential decay set of bumps.
//    Inputs are the initial constant x, x', y, y', values,
//    coefficients for the bumped x, x', y, y' values and
//    the e-folding times for x and y.
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////

Void Bump::eFoldBump2()
{

  xIdealBump = xBump0 + xBump1 * exp(-time/eFoldTimeX) ;
  yIdealBump = yBump0 + yBump1 *exp(-time/eFoldTimeY) ;
  xPIdealBump = xPBump0 + xPBump1 * exp(-time/eFoldTimeX) ;
  yPIdealBump = yPBump0 + yPBump1 * exp(-time/eFoldTimeY) ;
  
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Bump::interpolateBump
//
// DESCRIPTION
//    Applies bumps interpolated from input sets of data. Interpolation
//    is performed between the first point  in bumpTimes to the last
//    point in bumpTimes (see below). 
//     If "time" is beyond the extrema of bumpTimes, the lower( upper) value
//     of this vector is used. Note, all the  Vectors below  must be sized
//     to at least that of bumpTimes.
//     If you only want to bump only X or Y, jsut leave the appropriate set of
//     "X" or "Y" vectors set to the default initializtion of 0.
//
//    Inputs are the following Real Vectors:
//       bumpTimes - contains the times for the bumps (msec)
//       xBumpPoints - contains the x points (mm)
//       xPBumpPoints- contains the x' points (mrad)
//       yBumpPoints - contains the y points (mm)
//       yPBumpPoints- contains the y' points (mrad)
//
//
// PARAMETERS
//    None
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////
Void Bump::interpolateBumps()
{

Integer nt, nx, nxp, ny, nyp, i;
Real frac;

  nt = bumpTimes.rows();
  nx = xBumpPoints.rows();
  nxp = xPBumpPoints.rows();
  ny = yBumpPoints.rows();
  nyp = yPBumpPoints.rows();  

  if(nt < 2) except(badInterpSizes);
  if(bumpTimes(nt) < bumpTimes(1)) except(badBumpTimes);
  if ( nx < nt || nxp < nt || ny < nt || nyp < nt) except(badInterpSizes); 
 
  if (time <= bumpTimes(1))
    {
      xIdealBump = xBumpPoints(1);
      xPIdealBump = xPBumpPoints(1);
      yIdealBump = yBumpPoints(1);
      yPIdealBump = yPBumpPoints(1);
      return;
    }


  if (time >= bumpTimes(nt))
    {
      xIdealBump = xBumpPoints(nt);
      xPIdealBump = xPBumpPoints(nt);
      yIdealBump = yBumpPoints(nt);
      yPIdealBump = yPBumpPoints(nt);
      return;
    }

   i = 1;
   while (bumpTimes(i) <= time) i++;
   

   frac = (bumpTimes(i) - time)/(bumpTimes(i) - bumpTimes(i-1));

   xIdealBump = xBumpPoints(i-1)*frac  + (1.-frac)* xBumpPoints(i);
   xPIdealBump = xPBumpPoints(i-1)*frac  + (1.-frac)* xPBumpPoints(i);
   yIdealBump = yBumpPoints(i-1)*frac  + (1.-frac)* yBumpPoints(i);
   yPIdealBump = yPBumpPoints(i-1)*frac  + (1.-frac)* yPBumpPoints(i);

}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//  Bump::sizeBumpPoints
//
// DESCRIPTION
//    
//    Sizes vectors used in the interpolation bump scheme - 
//    intepolateBumps
//
// PARAMETERS
//    
//     nPoints - The number of Bump points to use for interpolation.
//
// RETURNS
//    Nothing
//
///////////////////////////////////////////////////////////////////////////
Void Bump::sizeBumpPoints(const Integer &nPoints)
{
   xBumpPoints.resize(nPoints); 
   xPBumpPoints.resize(nPoints);
   yBumpPoints.resize(nPoints); 
   yPBumpPoints.resize(nPoints);
   bumpTimes.resize(nPoints);
}

