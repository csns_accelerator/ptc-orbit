//=========================================================================
//  TImpedanceWF.h - header file of the Tr. Imp. Module (Wake Function)
//=========================================================================
//Transverse Impedance Module on the base of Mike Blaskiewicz's approach
//=========================================================================
// This Module includes the following classes:
// MB_TImpedanceWF - main class that performs calculation
// CmplxVector     - class is a class to operate with vectors of
//                   complex variables satisfying phasor condition
// MB_TI_manager   - class to keep references to the MB_TImpedanceWF class
//                   instances. The singleton pattern.
//=========================================================================

#ifndef MB_TImpedanceWF_H
#define MB_TImpedanceWF_H

//headers related to the original ORBIT
#include "Injection.h"
#include "Ring.h"
#include "Particles.h"
#include "MacroPart.h"

//=========================================================================
//============DEFINITION for MB_TImpedance module ============start========

#ifndef PI
#define PI   3.1415926535898
#endif

#define MB_INFINITY 1.e+21

#define MB_PROTON_MASS   0.93827231     // GeV
#define MB_PROTON_RADIUS 1.534698e-18   // m
#define MB_CLIGHT        2.99792458e+8  // m/c

#define MB_Z0            377.           // Ohms

//============DEFINITION for MB_TImpedance module ============stop=========
//=========================================================================
//===============header for TImpedanceWF class ============start===========
/** The  TImpedanceWF class uses the wake fields (WF)
    for the transverse impedance implementation.
*/

class MB_CmplxVector;

class MB_TImpedanceWF
{
public:

  /// Constructor
  MB_TImpedanceWF(int nBins, int nImpElements);

  /// Destructor
  virtual ~MB_TImpedanceWF();

  /// Set the parameters of the impedance element's wake field
  void addElement( int i_xy,
                   double wake_zero_re,
                   double wake_zero_im,
                   double eta_coff_re,
                   double eta_coff_im );

  /** Set the parameters of the resonant impedance element's wake field.
          r  - resistance parameter [ohms/m^2]
          q  - quality factor
          fr - resonant frequency [Hz]
  */
  void addResonantElement( int i_xy,
                           double r,
                           double q,
                           double fr);

  /**Set the range of operation for CmplxVector's arrays.
     There should be placed all CmplxVector variables.
  */
  void setRange_();

  ///Get wake function for x or y (i_xy) and an WF element with index j at the moment of time t [sec].
  double getWF(int i_xy, int j, double t);


  ///Restore initial element's state
  void restore();

  ///Memorize the state of the transverse impedance element
  void memorizeState();

  ///Restore the memorized state of the transverse impedance element
  void restoreState();

  /// Print out the <x> and <y> momenta of bunch
  void showXY(char* f);

  /// Print out the x and y kicks for bunch
  void showTKick(char* f);

  ///Propagate the bunch through the transverse impedance element
  void propagate( MacroPart &mp , double t);

  /// Print the parameters of the impedance element's wake fields
  void printElementParameters();

  /// Set period for ring calculation. This value will not be used inside this class.
	/// We will use in the ORBIT node
	void setPeriod(double T_in);

  /// Get period for ring calculation.
	/// We will use in the ORBIT node.
	double getPeriod();


protected:

  /// Transverse Kick calculation
  void _tKickCalc( double t);

  /// Bin the macro particles longitudinally
  void  _defineLongBin(const MacroPart &mp);

  /// Synchronize longitudinal grid size for parallel calculations
   void _grid_synchronize(double& ct_min, double& ct_max);

  /// Sum longitudinal distribution for parallel calculations
   void _sum_distribution();

  ///Resize arrays for bins index and fraction coefficient for every particle
   void _resizeFractCoeff( int bunchSizeLocal);

  /// Finalize MPI
   void _finalize_MPI();

protected:

  //coefficient (r0*4*pi/(Z0*beta**2*gamma))
  double coefficient_;

  // time_min_prev_ - minimal time for previous bunch
  double time_min_prev_;

  // Integrator's containers:

  int nBins_;
  int nElem_;

  //local CPU maximal banch size
  int maxBunchSize_;

  int* nElemExist_;
  int* xy_mask_;

  double**  xyM_;
  double**  tSumKick_;

  MB_CmplxVector***  tKick_ ;

  MB_CmplxVector**  tfKick_ ;

  MB_CmplxVector**  tFFKick_;

  MB_CmplxVector**  tFFmemory_;

  MB_CmplxVector** wake_zero_;

  MB_CmplxVector** eta_fact_;

  MB_CmplxVector** eta_coff_;

  // (size: maxBunchSize_)
  int* bins_;

  // (size: maxBunchSize_)
  double* fractBins_;

  // actual BunchSize for one CPU and for all CPUs
  int bunchSize_;
  int bunchSizeGlobal_;

  // time spread
  double t_min_,t_max_,t_step_;

	//The time period for ring calculation.[sec]
	double T;

  //MPI variables
  int iMPIini_;
  int nMPIsize_;
  int nMPIrank_;

  //buffer to data exchange for parallel version
  double* buff;
  double* buff_MPI;

};
//===============header for TImpedanceWF class ============stop============
//=========================================================================
//===============header for CmplxVector class =============start===========
/** The CmplxVector class is a class to operate
    with vectors of complex variable satisfying
    phasor condition.
*/

class MB_CmplxVector
{
public:

  /// Constructor
  MB_CmplxVector(int size);

  /// Destructor
  virtual ~MB_CmplxVector();

  ///Set the range of operation
  void  setRange(int i_min, int i_max);

  ///Returns the min range of operation
    int  getMinRange();

  ///Returns the max range of operation
    int  getMaxRange();

  ///Set the real and imaginary parts equal to zero
    void  zero();

  ///Set the real part of one element
    void  setRe(int j, double value);

  ///Set the imaginary part of one element
    void  setIm(int j, double value);

  ///Get the real part of one element
    double getRe(int j);

  ///Get the imaginary part of one element
    double getIm(int j);

  ///Sum two complex vectors
    void  sum( MB_CmplxVector& cv );

  ///Retirns the real part of the sum all components of the complex vector
    double sumRe();

  ///Retirns the Im part of the sum all components of the complex vector
    double sumIm();

  ///Multiply two complex vectors
    void  mult( MB_CmplxVector& cv );

   ///Multiply by real value
    void  multR( double x );

  ///Copy operator
    void  copy( MB_CmplxVector& cv );

  ///Defines this complex vector as shift one (exp(eta*time))
    void  defShift( MB_CmplxVector& eta , double time_shift );

  ///Shifts this complex vector by Multiplying by (exp(eta*time))
    void  shift(MB_CmplxVector& eta , double time_shift );

  ////Print vector
    void  print_();

protected:

  // Size of the complex vector
  int size_;

  //Re and Im values of complex vector
  double* re_;
  double* im_;

private:

  //Range for operation (static)
  int i_min_;
  int i_max_;

};
//===============header for CmplxVector class =============stop============
//=========================================================================
//===============header for MB_TI_manager class  =============start===========
//===============   singleton pattern         =============================
class MB_TI_manager
{
 public:
  static MB_TI_manager* GetMB_TI_manager();

  //returns number of nodes
  int getNumbNodes();

  //returns refference to the a MB_TImp node for index=index
  MB_TImpedanceWF* getNode(int index);

  //creates a MB_TImp node and returns its index
  int addNode(int nBins, int nImpElements);

  //destructor
  ~MB_TI_manager();

 protected:
  //constuctor
  MB_TI_manager();

 private:

  static MB_TI_manager* m_Instance;

  //number of nodes
  int nNodes_;
  MB_TImpedanceWF** refArr;
  MB_TImpedanceWF** tmp_refArr;

};
//===============header for MB_TI_manager class  =============stop============
//=========================================================================

#endif
