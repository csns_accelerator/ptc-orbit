//# Library            : ORBIT
//# File               : FFT3DSpaceCharge.hh
//# Original code      : Jeff Holmes, Slava Danilov, John Galambos

#ifndef ORBIT_FFTSC3D_H
#define ORBIT_FFTSC3D_H

#include <mpi.h>
#include "rfftw.h"
#include "fftw.h"

#include "Injection.h"
#include "Ring.h"
#include "Particles.h"
#include "MacroPart.h"

#include "PartsDistributor_MPI.h"
#include "Boundary.h"

//Consts.h - header from SuperCode with Consts::pi
#include "Consts.h"

/** The ORBIT_FFT3DSpaceCharge class is used to perform FFT of the SC distribution,
    to convolute SC FFT and Green's functions, to calculate forces, and to apply
    the SC kick to the particles in the bunch.
*/
    
class  ORBIT_FFT3DSpaceCharge
{
public:

  static ORBIT_FFT3DSpaceCharge* getORBIT_FFT3DSpaceCharge(int nXBins, int nYBins, int nZBins, int minBuchSize);
  static ORBIT_FFT3DSpaceCharge* getORBIT_FFT3DSpaceCharge();

  /// Destructor
  virtual ~ORBIT_FFT3DSpaceCharge();

  ///Propagates particles through 3DSC element
  void propagate(MacroPart &mp,
                 ORBIT_Boundary& boundary,
                 double element_length,
                 double shift_X, double shift_Y);


protected:

  /// Constructor
  ORBIT_FFT3DSpaceCharge(int nXBins, int nYBins, int nZBins, int minBuchSize); 

  /// Finalize MPI
  void  _finalize_MPI();

  ///Bin macroparticles
  void _binMacroParticles(MacroPart &mp,
                          ORBIT_Boundary& boundary,
                          double shift_X, double shift_Y);

  ///Conforms the space charge distribution on all of CPUs
  void _conformSCDistribution(); 

  ///Calculate FFT of the SC distribution and the potential
  void _claculatePotential(ORBIT_Boundary& boundary); 

  ///Conforms SC potential across all CPUs
  void _conformSCPotential();

  ///Resize inner arrays if the longitudinal size has been changed
  void _resizeLongAndPartArrays(int nPart);

protected:

  //static reference to itself
  static ORBIT_FFT3DSpaceCharge* m_Instance;

  //longitudinal size of bunches
  double phi_min_,phi_max_,phi_step_;

  //grid size
  int nXBins_,nYBins_,nZBins_;

  //XY-plane's grid array and parameters
  double xGridMin_, xGridMax_, yGridMin_, yGridMax_;
  double  dx_  ,  dy_;

  //Define region to operate with the potential and particles 
  //(there are particles into this region)
  int ixMin_,ixMax_,iyMin_,iyMax_;
  int ixMin_old_,ixMax_old_,iyMin_old_,iyMax_old_;

  
  double* xGrid_;
  double* yGrid_; 


  //min and max Bunch's size
  int minBunchSize_;
  int maxBunchSize_;

  //number of the live particles in all CPU
  int nPartAll_;

  //MPI initialized - 1, not - 0
  int iMPIini;

  //size MPI
  int size_MPI;

  //rank MPI
  int rank_MPI;

  //array of particle's position and fraction coefficient
  int bunchSize_;
  int* xBin_ ;
  int* yBin_ ;
  int* LPositionIndex_;
  double* xFractBin_;
  double* yFractBin_;
  double* fractLPosition_;

  //Longitudinal position indexes belonged to this CPU
  int longIndex_min_;
  int longIndex_max_;
  
  //Number of the longitudinal slices belonged to this CPU
  int nSlices_;

  //The 3D charge distrubution
  double*** rho3D_;

  //The 3D potential
  double*** phiSC_;  

  //Allocate memory for exchange buffer for intiger variables
  int* buff_int;
  int* buff_int_MPI;

  //Allocate memory for exchange buffer for XY-plane's SC distribution
  int buff_double_size_;
  double* buff_double;
  double* buff_double_MPI;


  //FFT arrays 
  FFTW_COMPLEX* out_green_;
  FFTW_REAL * in_;
  FFTW_REAL * in1_;
  FFTW_COMPLEX * out1_;
  FFTW_COMPLEX * out_;
  rfftwnd_plan planForward_;
  rfftwnd_plan planBackward_;
 
};

#endif
