/////////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    longSC.h
//
// AUTHOR
//    John Galambos
//    ORNL, jdg@ornl.gov
//
// CREATED
//    6/15/98
//
//  DESCRIPTION
//    Header file for the longitudinal space charge class.
//    Seprated from LSpaceCharge.cc to use elsewhere.
//
//  REVISION HISTORY
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(__LongSC__)
#define __LongSC__

/////////////////////////////////////////////////////////////////////////////
//
// include files
//
/////////////////////////////////////////////////////////////////////////////

#include "Node.h"
#include "fftw.h"
#include "RealMat.h"
#include "ComplexMat.h"
#include "SCLTypes.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   LongSC
//
// INHERITANCE RELATIONSHIPS
//   LongSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a base class for storing longitudinal space charge information.
//
// PUBLIC MEMBERS
//   Integer:
//   _nBins         Reference to the number of bins.
//   _nParts_Bin    Integer Vector containing the particles/bin
//   _nMacrosMin    Minimum number of macro particles before using this node.
//
// Real
//   _deltaPhiBin   The phi bin width (rad).//
//   _phiCount      Vector of the number of macros in each phi bin.
//
// Void:
//   nameOut - dumps node name to a string
//
// PROTECTED MEMBERS
//   _forceCalculated - switch indicating that the force has been calculated.
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class LongSC : public Node
{
  Declare_Standard_Members(LongSC, Node);

  public:
    LongSC(const String &n, const Integer &order, Integer &nBins,
           Integer &nMacrosMin);
    ~LongSC();

    virtual Void nameOut(String &wname) { wname = _name; }
    virtual Void _updatePartAtNode(MacroPart &mp)=0;
    virtual Void _nodeCalculator(MacroPart &mp)=0;
    virtual Real _kick(Real &angle)=0;
    virtual Void _longBin(MacroPart &mp);

    virtual Integer syncLongDist(Vector(Real) &count, Integer &nBins,
                                 Integer &nMacros, Real &phiMin,
                                 Real &phiMax);

    Vector(Real) _phiCount;

    //parallel stuff ===MPI===
    Vector(Real) _local_MPI_Phi;

    Real _deltaPhiBin;

    Integer _nBins;
    Integer _nMacrosMin;

  protected:
    Real _charge2Current;
    Integer _forceCalculated;
};


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   FFTLongSC
//
// INHERITANCE RELATIONSHIPS
//   FFTLongSC -> LongSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class for storing longitudinal space charge information,
//   done with the FFT implementation.
//   This case also can include the wall impedance effects.
//
// PUBLIC MEMBERS
//   _useAverage     Calculate kick at bins, rather than for each particle,
//                   if  == 1. Interpolate back to particles.
//   _useSpaceCharge Calculate kick with or without space charge,
//                   if  == 1. Include space charge.
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class FFTLongSC : public LongSC
{
  Declare_Standard_Members(FFTLongSC, LongSC);
  public:
    FFTLongSC(const String &n, const Integer &order, Integer &nBins,
              Vector(Complex) &Z, Real &b, Integer &useAvg,
              Integer &nMacrosMin,
           Integer &useSpaceCharge);
    ~FFTLongSC();

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
    Real _kick(Real &angle);

    Vector(Complex) _zImped_n;
    Vector(Real) _FFTMagnitude;
    Vector(Real) _FFTPhase;
    Real _z_0;
    Real _b_a;
    Integer _useAverage;
    Integer _useSpaceCharge;
    Vector(Real) _z, _chi;

 private:
    Vector(Real) _deltaEGrid;
    fftw_plan _plan;
    FFTW_COMPLEX *_in;
    FFTW_COMPLEX *_out;
};


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   FreqDepLimp
//
// INHERITANCE RELATIONSHIPS
//   FreqDepLimp -> LongSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class for storing longitudinal impedance information
//   including space charge, done with the FFT implementation.
//   Impedance is stored as a function of beam frequency, which can
//   depend on time.
//
// PUBLIC MEMBERS
//   _useAverage     Calculate kick at bins, rather than for each particle,
//                   if  == 1. Interpolate back to particles.
//   _useSpaceCharge Calculate kick with or without space charge,
//                   if  == 1. Include space charge.
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class FreqDepLimp : public LongSC
{
  Declare_Standard_Members(FreqDepLimp, LongSC);
  public:
    FreqDepLimp(const String &name, const Integer &order,
                Integer &nBins, Integer &nTable,
                Vector(Real) &fTable, Vector(Complex) &zTable,
                Real &b_a, Integer &useAvg, Integer &nMacrosMin,
                Integer &useSpaceCharge);
    ~FreqDepLimp();

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
    Real _kick(Real &angle);

    Integer _nTable;
    Vector(Real) _fTable;
    Vector(Complex) _zTable;

    Vector(Complex) _zImped_n;
    Vector(Real) _FFTMagnitude;
    Vector(Real) _FFTPhase;
    Real _z_0;
    Real _b_a;
    Integer _useAverage;
    Integer _useSpaceCharge;
    Vector(Real) _z, _chi;

 private:
    Vector(Real) _deltaEGrid;
    fftw_plan _plan;
    FFTW_COMPLEX *_in;
    FFTW_COMPLEX *_out;
};


///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//   FreqBetDepLimp
//
// INHERITANCE RELATIONSHIPS
//   FreqBetDepLimp -> LongSC -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//   None
//
// DESCRIPTION
//   This is a class for storing longitudinal impedance information
//   including space charge, done with the FFT implementation.
//   Impedance is stored as a function of beam frequency, which can
//   depend on time. Impedance is also a function of relativistic
//   beta to accomodate sub-relativistic particles.
//
// PUBLIC MEMBERS
//   _useAverage     Calculate kick at bins, rather than for each particle,
//                   if  == 1. Interpolate back to particles.
//   _useSpaceCharge Calculate kick with or without space charge,
//                   if  == 1. Include space charge.
//
// PROTECTED MEMBERS
//   None
//
// PRIVATE MEMBERS
//   None
//
///////////////////////////////////////////////////////////////////////////

class FreqBetDepLimp : public LongSC
{
  Declare_Standard_Members(FreqBetDepLimp, LongSC);
  public:
    FreqBetDepLimp(const String &name, const Integer &order,
                   Integer &nBins,
                   Integer &nbTable, Vector(Real) &bTable,
                   Integer &nfTable, Vector(Real) &fTable,
                   Matrix(Complex) &zTable,
                   Real &b_a, Integer &useAvg, Integer &nMacrosMin,
                   Integer &useSpaceCharge);
    ~FreqBetDepLimp();

    Void _updatePartAtNode(MacroPart &mp);
    Void _nodeCalculator(MacroPart &mp);
    Real _kick(Real &angle);

    Integer _nbTable, _nfTable;
    Vector(Real) _bTable, _fTable;
    Matrix(Complex) _zTable;

    Vector(Complex) _zImped_n;
    Vector(Real) _FFTMagnitude;
    Vector(Real) _FFTPhase;
    Real _z_0;
    Real _b_a;
    Integer _useAverage;
    Integer _useSpaceCharge;
    Vector(Real) _z, _chi;

 private:
    Vector(Real) _deltaEGrid;
    fftw_plan _plan;
    FFTW_COMPLEX *_in;
    FFTW_COMPLEX *_out;
};


///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif   // __LongSC__
