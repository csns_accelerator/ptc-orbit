//////////////////////////////// -*- C++ -*- //////////////////////////////
//
// FILE NAME
//    MacroPart.h
//
// AUTHOR
//    J. Galambos
//
// CREATED
//    8/7/97
//
//
// DESCRIPTION
//    Specification and inline functions for a class used to 
//    store macro particle info
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// INCLUDE FILES
//
///////////////////////////////////////////////////////////////////////////

#if !defined(__MacroPart__)
#define __MacroPart__

#include "Object.h"
#include "StrClass.h"
#include "RealMat.h"
#include "IntegerMat.h"

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    SyncPart
//
// INHERITANCE RELATIONSHIPS
//    SyncPart -> Object -> IOSystem
//
// USING/CONTAINING RELATIONSHIPS
//    Object (U)
//
// DESCRIPTION
//    A class for storing SyncPart info
//
//
// PUBLIC MEMBERS
//    SyncPart:      Constructor for making SyncPart objects
//    ~SyncPart:     Destructor for the SyncPart class.
//    _mass          Atomic Mass (AMU)
//    _charge        atomic charge number
//    _eKinetic      kinetic energy (GeV)
//    _e0            rest mass (GeV)
//    _eTotal        total energy (GeV)
//    _betaSync      v/v_light
//    _gammaSync     E/E_0
//    _dppFac        conversion factor from dE to dp/p
//    _phiCoef       conversion from longitudnal length (m) to phase (rad)
//    _phase         synchronous particle phase, = 0 for no accelerate [rad]
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class SyncPart : public Object {
  Declare_Standard_Members(SyncPart, Object);
public:
  
    SyncPart(const Real& m, const Integer& charge, Real &E);
   ~SyncPart();

    Void update(const Real& m, const Integer& charge, Real &E);

    Real _mass;
    Real _charge;
    Real _eKinetic;
    Real _e0;
    Real _eTotal;
    Real _betaSync;
    Real _gammaSync;
    Real _dppFac;
    Real _phiCoef;
    Real _phase;
    
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    LostMacroParts
//
// INHERITANCE RELATIONSHIPS
//    LostMacroParts -> Object -> IOSystem
//
// USING/CONTAINING RELATIONSHIPS
//    Object (U)
//
// DESCRIPTION
//    A class for storing lost macro  particle info. Lost macros
//    are removed from the herd.
//
// PUBLIC MEMBERS
//    LostMacroParts:        Constructor for making MacroPart objects
//    ~LostMacroParts:       Destructor for the MacroPart class.
// Real Vector:
//    _x                 x  (horizontal) position (mm)
//    _xp                x prime position (mrad)
//    _y                 y  (vertical) position (mm)
//    _yp                y prime position (mrad)
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class LostMacroParts : public Object {
  Declare_Standard_Members(LostMacroParts, Object);
public:  
  LostMacroParts();

  ~LostMacroParts();

   Integer _nLostMacros;
   Vector(Real) _xLost;
   Vector(Real) _xpLost;
   Vector(Real) _yLost;
   Vector(Real) _ypLost;
   Vector(Real) _phiLost;
   Vector(Real) _deltaELost;
   Vector(Real) _LPosLost;
   Vector(Integer) _nodeLost;

   Vector(Integer) _turnLost;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    MacroPart
//
// INHERITANCE RELATIONSHIPS
//    MacroPart -> Object -> IOSystem
//
// USING/CONTAINING RELATIONSHIPS
//    Object (U)
//
// DESCRIPTION
//    A class for storing macro  particle info. The macro particles 
//    abstraction is for a "herd" of macroparticles. Note, we puposely
//    to not store all atributes, statistics etc. about the macropartcles
//    in this class to keep the size as small as possible. Only the basic 
//    proprties are stored here. The hope is that keeping the size small
//    will increase the likelyhood of the more-often used stuff getting
//    into cache.
//
// PUBLIC MEMBERS
//    MacroPart:        Constructor for making MacroPart objects
//    ~MacroPart:       Destructor for the MacroPart class.
// Real Vector:
//    _x                 x  (horizontal) position (mm)
//    _xp                x prime position (mrad)
//    _y                 y  (vertical) position (mm)
//    _yp                y prime position (mrad)
//    _phi               phase angle relative to synchronous particle (rad)
//    _deltaE            energy offset (GeV)
//    _dp_p              momentum dp/p
//    _fractLPosition    Fractional position in longitudinal bin
//    _LPosFactor        Longitudinal weighting factor = local line density
//                       over the average line density.
// Integer Vector
//    _LPositionIndex    Longitudinal bin index
//    _foilHits          Number of foil traversals.
//    _xBin              Integer vector storing the horizontal bin location
//    _yBin              Integer vector storing the vertical bin location
// Real Vector
//    _xFractBin         Fractional postion within a horizontal bin
//    _yFractBin         Fractional postion within a vertical bin
// Real 
//    _xMin              Min x of herd (mm)
//    _xMax              Max x of herd (mm)
//    _yMin              Min y of herd (mm)
//    _yMax              Max y of herd (mm)
//    _phiMin            Minimum longitudinal phase of herd (rad)
//    _phiMax            Maximum longitudinal phase of herd (rad)
//    _dEMin             Minimum deltaE of herd (GeV)
//    _dEMax             Maximum deltaE of herd (GeV)
//    _bunchFactor       longitudinal (average/peak) density ratio
// Integer
//    _nMacros           Number of macro particles presently in herd.
//    _globalNMacros     Global number of macro particles presently in herd,
//                         used in parallel runs.
//    _feelsHerds        Switch to control interaction with other herds
//                       0 (default) = no interaction with other herds
//                       1 = feels, but doesn't push other herds
//    _longBinningDone   flag to indicate if longitudinally binned yet
//
// String
//   _herdName
//  
// Node Info for the herd:
//
//    _currentNode       the current Ring node the herd is at
//    _nTurnsDone        number of turns the herd has completed
//    _nPartTurnsDone    integral of the number of turns * number of particles
//
//  Void routines:
//    _reSize           - resize (increase) the herd vectors to new size
//    _findXYPExtrema   - find the min/max _x,_y,_phi,extents
//    _findDEExtrema    - find the min/max  deltaE extents
//    _insertMacroPart  - routines to add a macroparticle to a herd.
//    _addLostMacro     - routine to move a single particle from the herd to
//                      - the LostMacroParts object.
//
// SyncPart &
//    _syncPart          Reference to the synchronous particle object
//
// LostMacroParts 
//    _lostOnes         - place to store lost macroparticle information.
//
// PROTECTED MEMBERS
//  None
// PRIVATE MEMBERS
//    None.
//
///////////////////////////////////////////////////////////////////////////

class MacroPart : public Object {
  Declare_Standard_Members(MacroPart, Object);
public:
  
  MacroPart(SyncPart &s, Integer &chunkSize, const String &name);
  MacroPart(SyncPart &s, const Integer &chunkSize, const String &name);

  ~MacroPart();

    Integer _nMacros, _globalNMacros;
    Integer _feelsHerds;
    Integer _longBinningDone;
    SyncPart &_syncPart;      // not const since acceleration changes it

    Integer _currentNode, _nTurnsDone, _nPartTurnsDone;

    Vector(Real) _x;
    Vector(Real) _xp;
    Vector(Real) _y;
    Vector(Real) _yp;
    Vector(Real) _phi;
    Vector(Real) _deltaE, _dp_p;
    Vector(Integer) _LPositionIndex;
    Vector(Real) _fractLPosition, _LPosFactor;    
    Vector(Integer) _foilHits;
    Void _resize(const Integer &chunk);
    Void _findXYPExtrema();
    Void _findDEExtrema();

    Void _insertMacroPart(const Real& x, const Real& xp, const Real& y, 
            const Real& yp);            
    Void _insertMacroPart(const Real& x, const Real& xp, const Real& y,
             const Real& yp, const Real &dE, const Real &phi);

    Vector(Real) _xFractBin, _yFractBin;
    Vector(Integer) _xBin, _yBin;
    Real _xMin, _xMax, _yMin, _yMax, _phiMin, _phiMax, _dEMin, _dEMax,
         _bunchFactor;
   Void _addLostMacro(const Integer &i, const Real &LPos);
   LostMacroParts _lostOnes;
    String _herdName;
};

///////////////////////////////////////////////////////////////////////////
//
// END OF FILE
//
///////////////////////////////////////////////////////////////////////////

#endif




