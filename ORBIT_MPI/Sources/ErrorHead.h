#if !defined(__Errhead__)
#define __Errhead__

#include "Object.h"
///////////////////////////////////////////////////////////////////////////
//
// FILE NAME
//    ErrorHead.h
//
// AUTHOR
//    Steven Bunch, University of Tennessee, bunchsc@sns.gov
//
// CREATED
//    6/17/02
//
//  DESCRIPTION
//    Header file for Error class
//
//  REVISION HISTORY
//
///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    CoordDisplacement
//
// INHERITANCE RELATIONSHIPS
//    CoordDisplacement -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing generalized coordinate displacements
//
// PUBLIC MEMBERS
//    _dx
//    _dxp
//    _dy
//    _dyp
//    _dphi
//    _ddeltaE
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class CoordDisplacement : public ErrorBase
{
    Declare_Standard_Members(CoordDisplacement, ErrorBase);
  public:
    CoordDisplacement(const String &n, const Integer &order,
                      const Real &dx, const Real &dxp,
                      const Real &dy, const Real &dyp,
                      const Real &dphi, const Real &ddeltaE);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _dx;
    Real _dxp;
    Real _dy;
    Real _dyp;
    Real _dphi;
    Real _ddeltaE;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    QuadKicker
//
// INHERITANCE RELATIONSHIPS
//    QuadKicker -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing quadrupole kicks
//
// PUBLIC MEMBERS
//    _k
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class QuadKicker : public ErrorBase
{
    Declare_Standard_Members(QuadKicker, ErrorBase);
  public:
    QuadKicker(const String &n, const Integer &order,
               const Real &k);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _k;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    QuadKickerOsc
//
// INHERITANCE RELATIONSHIPS
//    QuadKickerOsc -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing oscillating quadrupole kicks
//
// PUBLIC MEMBERS
//    _k
//    _freq
//    _phase
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class QuadKickerOsc : public ErrorBase
{
    Declare_Standard_Members(QuadKickerOsc, ErrorBase);
  public:
    QuadKickerOsc(const String &n, const Integer &order,
                   const Real &k, const Real &freq,
                   const Real &phase);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _k;
    Real _freq;
    Real _phase;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    DipoleKickerOsc
//
// INHERITANCE RELATIONSHIPS
//    DipoleKickerOsc -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing oscillating dipole kicks
//
// PUBLIC MEMBERS
//    _k
//    _freq
//    _phase
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class DipoleKickerOsc : public ErrorBase
{
    Declare_Standard_Members(DipoleKickerOsc, ErrorBase);
  public:
    DipoleKickerOsc(const String &n, const Integer &order,
                   const Real &k, const Real &freq,
                   const Real &phase);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _k;
    Real _freq;
    Real _phase;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    LongDisplacement
//
// INHERITANCE RELATIONSHIPS
//    LongDisplacement -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Longitudinal Displacement
//
// PUBLIC MEMBERS
//    _ds
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class LongDisplacement : public ErrorBase {
    Declare_Standard_Members(LongDisplacement, ErrorBase);
  public:
    LongDisplacement(const String &n, const Integer &order,
                     const Real &ds);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _ds;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationXY
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationXY -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X-Y coordinate rotations of straight elements
//
// PUBLIC MEMBERS
//    _anglexy
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class StraightRotationXY : public ErrorBase {
    Declare_Standard_Members(StraightRotationXY, ErrorBase);
  public:
    StraightRotationXY(const String &n, const Integer &order,
                       const Real &anglexy);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _anglexy;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationXSI
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationXSI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X-S initial coordinate rotations
//    of straight elements
//
// PUBLIC MEMBERS
//    _anglexsi
//    _length
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class StraightRotationXSI : public ErrorBase {
    Declare_Standard_Members(StraightRotationXSI, ErrorBase);
  public:
    StraightRotationXSI(const String &n, const Integer &order,
                        const Real &anglexsi, const Real &length);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _anglexsi;
    Real _length;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationXSF
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationXSF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X-S final coordinate rotations
//    of straight elements
//
// PUBLIC MEMBERS
//    _anglexsf
//    _length
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class StraightRotationXSF : public ErrorBase {
    Declare_Standard_Members(StraightRotationXSF, ErrorBase);
  public:
    StraightRotationXSF(const String &n, const Integer &order,
                        const Real &anglexsf, const Real &length);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _anglexsf;
    Real _length;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationYSI
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationYSI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Y-S initial coordinate rotations
//    of straight elements
//
// PUBLIC MEMBERS
//    _angleysi
//    _length
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class StraightRotationYSI : public ErrorBase {
    Declare_Standard_Members(StraightRotationYSI, ErrorBase);
  public:
    StraightRotationYSI(const String &n, const Integer &order,
                        const Real &angleysi, const Real &length);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _angleysi;
    Real _length;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    StraightRotationYSF
//
// INHERITANCE RELATIONSHIPS
//    StraightRotationYSF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Y-S final coordinate rotations
//    of straight elements
//
// PUBLIC MEMBERS
//    _angleysf
//    _length
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class StraightRotationYSF : public ErrorBase {
    Declare_Standard_Members(StraightRotationYSF, ErrorBase);
  public:
    StraightRotationYSF(const String &n, const Integer &order,
                        const Real &angleysf, const Real &length);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _angleysf;
    Real _length;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendFieldI
//
// INHERITANCE RELATIONSHIPS
//    BendFieldI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Bend initial field errors
//
// PUBLIC MEMBERS
//    _drho
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class BendFieldI : public ErrorBase {
    Declare_Standard_Members(BendFieldI, ErrorBase);
  public:
    BendFieldI(const String &n, const Integer &order,
               const Real &drho);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _drho;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendFieldF
//
// INHERITANCE RELATIONSHIPS
//    BendFieldF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Bend final field errors
//
// PUBLIC MEMBERS
//    _drho
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class BendFieldF : public ErrorBase {
    Declare_Standard_Members(BendFieldF, ErrorBase);
  public:
    BendFieldF(const String &n, const Integer &order,
               const Real &drho);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _drho;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementXI
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementXI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X initial Bend displacements
//
// PUBLIC MEMBERS
//    _anglexi
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class BendDisplacementXI : public ErrorBase {
    Declare_Standard_Members(BendDisplacementXI, ErrorBase);
  public:
    BendDisplacementXI(const String &n, const Integer &order,
                       const Real &anglexi, const Real &disp);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _anglexi;
    Real _disp;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementXF
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementXF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing X final Bend displacements
//
// PUBLIC MEMBERS
//    _anglexf
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class BendDisplacementXF : public ErrorBase {
    Declare_Standard_Members(BendDisplacementXF, ErrorBase);
  public:
    BendDisplacementXF(const String &n, const Integer &order,
                       const Real &anglexf, const Real &disp);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _anglexf;
    Real _disp;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementYI
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementYI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Y initial Bend displacements
//
// PUBLIC MEMBERS
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class BendDisplacementYI : public ErrorBase {
    Declare_Standard_Members(BendDisplacementYI, ErrorBase);
  public:
    BendDisplacementYI(const String &n, const Integer &order,
                       const Real &disp);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _disp;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementYF
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementYF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Y final Bend displacements
//
// PUBLIC MEMBERS
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class BendDisplacementYF : public ErrorBase {
    Declare_Standard_Members(BendDisplacementYF, ErrorBase);
  public:
    BendDisplacementYF(const String &n, const Integer &order,
                       const Real &disp);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _disp;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementLI
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementLI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Longitudinal Bend initial displacements
//
// PUBLIC MEMBERS
//    _angleli
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class BendDisplacementLI : public ErrorBase {
    Declare_Standard_Members(BendDisplacementLI, ErrorBase);
  public:
    BendDisplacementLI(const String &n, const Integer &order,
                       const Real &angleli, const Real &disp);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _angleli;
    Real _disp;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    BendDisplacementLF
//
// INHERITANCE RELATIONSHIPS
//    BendDisplacementLF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing Longitudinal Bend final displacements
//
// PUBLIC MEMBERS
//    _anglelf
//    _disp
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class BendDisplacementLF : public ErrorBase {
    Declare_Standard_Members(BendDisplacementLF, ErrorBase);
  public:
    BendDisplacementLF(const String &n, const Integer &order,
                       const Real &anglelf, const Real &disp);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _anglelf;
    Real _disp;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    RotationI
//
// INHERITANCE RELATIONSHIPS
//    RotationI -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing initial coordinate rotations
//
// PUBLIC MEMBERS
//    _anglei
//    _rhoi
//    _theta
//    _length
//    _et
//    _type
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class RotationI : public ErrorBase {
    Declare_Standard_Members(RotationI, ErrorBase);
  public:
    RotationI(const String &n, const Integer &order,
              const Real &anglei, const Real &rhoi,
              const Real &theta, const Real &length,
              const String &et, const String &type);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _anglei;
    Real _rhoi;
    Real _theta;
    Real _length;
    String _et;
    String _type;
};

///////////////////////////////////////////////////////////////////////////
//
// CLASS NAME
//    RotationF
//
// INHERITANCE RELATIONSHIPS
//    RotationF -> ErrorBase -> Node* -> Object
//
// USING/CONTAINING RELATIONSHIPS
//    None
//
// DESCRIPTION
//    Class for performing final coordinate rotations
//
// PUBLIC MEMBERS
//    _anglef
//    _rhoi
//    _theta
//    _length
//    _et
//    _type
//
// PROTECTED MEMBERS
//    None
// PRIVATE MEMBERS
//    None
//
///////////////////////////////////////////////////////////////////////////

class RotationF : public ErrorBase {
    Declare_Standard_Members(RotationF, ErrorBase);
  public:
    RotationF(const String &n, const Integer &order,
              const Real &anglef, const Real &rhoi,
              const Real &theta, const Real &length,
              const String &et, const String &type);
    Void nameOut(String &wname) { wname = _name; }
    Void _nodeCalculator(MacroPart &mp);
    Void _showError(Ostream &sout);
    Void _updatePartAtNode(MacroPart &mp);

    Real _anglef;
    Real _rhoi;
    Real _theta;
    Real _length;
    String _et;
    String _type;
};

#endif   // __Errhead__
